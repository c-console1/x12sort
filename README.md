# X12SortC

This sorts X12 files for coexistence with HRP/Axiom in conjunction with a few Automate tasks.

## Prerequisites

[DAM (Data Access Manager)](http://phcwapqa04/docs/datalayer/dam/index.html) commands for configuring a server, remove --machine parameter to configure your local machine

If a machine already has DAM configuration for the job, use `dam info app X12Sort` to review the configuration.

## QA
```powershell
dam new app X12Sort stdtemplate --type batch --desc="Coexistence X12 Sorting" --machine="phcwapqa04"
dam new var X12Sort SiteDestRootOut "\\phsi.promedica.org\promedica\phc_common\Automate File Transfer\HealthEdge Coexistence Testing\Cerberus\SI" --desc="Destination for inbound files from Axiom that need to go to trading partners" --machine="phcwapqa04"
dam new var X12Sort SiteDestRootIn "\\phsi.promedica.org\promedica\phc_common\Automate File Transfer\HealthEdge Coexistence Testing\SI" --desc="Destination for inbound files from Axiom that need to go to Amisys" --machine="phcwapqa04"
dam new var X12Sort SiteSourceDir "\\phsi.promedica.org\promedica\phc_common\Automate File Transfer\HealthEdge Coexistence Testing\SI\FromAxiom" --desc="Staging directory for files that need sorted to trading partners or Amisys" --machine="phcwapqa04"
dam new var X12Sort ISAValues "582167964      ,1045;133068979      ,1046;232743471      ,1047;201736437      ,1080;PHC            ,4010;SOFTHEONMC2    ,4057;MMISODJFS      ,5002;SOHODJFSMMIS   ,5002;COBA           ,5004;CMS   ,5004;COB            ,5004;1079H          ,1079" --desc="Dictionary for ISA values and their corresponding internal site number" --machine="phcwapqa04"
dam new var X12Sort TestEnv "PHCWAPQA04" --desc="Test machine name used to see if ISA15 needs converted to T" --machine="phcwapqa04"
dam new var X12Sort EmailBody "An encrypted file has been placed in \\phsi.promedica.org\promedica\PHC_Common\Automate File Transfer\HealthEdge Coexistence Testing\SI\ToAxiom\NeedsDecrypted. Figure out which site it is from and decrypt it, then place it back in \\phsi.promedica.org\promedica\PHC_Common\Automate File Transfer\HealthEdge Coexistence Testing\Cerberus\SI\In\#### for the approprate submitter" --desc="Email body for encrypted file" --machine="phcwapqa04"
dam new var X12Sort EmailSubject "Encrypted File Moved" --desc="Email Subject" --machine="phcwapqa04"
dam new var X12Sort EmailFrom "FileSorter@Promedica.org" --desc="Email From" --machine="phcwapqa04"
dam new var X12Sort EmailTo "PHC-EDI@promedica.org" --desc="Email To" --machine="phcwapqa04"
dam new var X12Sort TypeDestDir "\\phsi.promedica.org\promedica\PHC_Common\Automate File Transfer\HealthEdge Coexistence Testing\SI\ToAxiom" --desc="Stage directories root for Type Sorting" --machine="phcwapqa04"
dam new var X12Sort TypeSourceDir "\\phsi.promedica.org\promedica\PHC_Common\Automate File Transfer\HealthEdge Coexistence Testing\SI\ToAxiom\Stage\*" --desc="Source directory for files that need sorted to go to TransSend" --machine="phcwapqa04"
dam new var X12Sort TransactionTypes "005010X231A1,999;005010X222A1,837;005010X223A2,837;005010X214,277CA;005010X224A2,837;005010X220A1,834;004010X095A1,834;005010X212,276;005010X218,820;005010X279A1,270" --desc="Dictionary for determining the transaction type based off of GS08 segment" --machine="phcwapqa04"
dam new var X12Sort ArchiveDir "\\tislnsmb.phsi.promedica.org\phc_syslog_archive$\X12TUnzip" --desc="Archive directory for all raw files that go through TypeSort.cs" --machine="phcwapqa04"
dam new var X12Sort LogFolder "E:\Logs" --desc="Directory that log file will be stored" --machine="phcwapqa04"
dam new var X12Sort CRSites "2089" --desc="List of sites that use Mac line endings" --machine="phcwapqa04"

```

## PROD
```powershell
dam new app X12Sort stdtemplate --type batch --desc="Coexistence X12 Sorting" --machine="phcwap04"
dam new var X12Sort SiteDestRootOut "\\phsi.promedica.org\paramount\edi\SI" --desc="Destination for inbound files from Axiom that need to go to trading partners" --machine="phcwap04"
dam new var X12Sort SiteDestRootIn "\\tislnsmb.phsi.promedica.org\ifs\phstisilon\Archive\SI" --desc="Destination for inbound files from Axiom that need to go to Amisys" --machine="phcwap04"
dam new var X12Sort SiteSourceDir "\\tislnsmb.phsi.promedica.org\ifs\phstisilon\Archive\SI\FromAxiom" --desc="Staging directory for files that need sorted to trading partners or Amisys" --machine="phcwap04"
dam new var X12Sort ISAValues "582167964      ,1045;133068979      ,1046;232743471      ,1047;201736437      ,1080;PHC            ,4010;SOFTHEONMC2    ,4057;MMISODJFS      ,5002;SOHODJFSMMIS   ,5002;COBA           ,5004;CMS   ,5004;COB            ,5004;1079H          ,1079" --desc="Dictionary for ISA values and their corresponding internal site number" --machine="phcwap04"
dam new var X12Sort TestEnv "PHCWAPQA04" --desc="Test machine name used to see if ISA15 needs converted to T" --machine="phcwap04"
dam new var X12Sort EmailBody "An encrypted file has been placed in \\tislnsmb.phsi.promedica.org\ifs\phstisilon\Archive\SI\ToAxiom\NeedsDecrypted. Figure out which site it is from and decrypt it, then place it back in \\tislnsmb.phsi.promedica.org\ifs\phstisilon\Archive\SI\In\#### for the approprate submitter" --desc="Email body for encrypted file" --machine="phcwap04"
dam new var X12Sort EmailSubject "Encrypted File Moved" --desc="Email Subject" --machine="phcwap04"
dam new var X12Sort EmailFrom "FileSorter@Promedica.org" --desc="Email From" --machine="phcwap04"
dam new var X12Sort EmailTo "PHC-EDI@promedica.org" --desc="Email To" --machine="phcwap04"
dam new var X12Sort TypeDestDir "\\tislnsmb.phsi.promedica.org\ifs\phstisilon\Archive\SI\ToAxiom" --desc="Stage directories root for Type Sorting" --machine="phcwap04"
dam new var X12Sort TypeSourceDir "\\tislnsmb.phsi.promedica.org\ifs\phstisilon\Archive\SI\ToAxiom\Stage\*" --desc="Source directory for files that need sorted to go to TransSend" --machine="phcwap04"
dam new var X12Sort TransactionTypes "005010X231A1,999;005010X222A1,837;005010X223A2,837;005010X214,277CA;005010X224A2,837;005010X220A1,834;004010X095A1,834;005010X212,276;005010X218,820;005010X279A1,270" --desc="Dictionary for determining the transaction type based off of GS08 segment" --machine="phcwap04"
dam new var X12Sort ArchiveDir "\\tislnsmb.phsi.promedica.org\phc_syslog_archive$\X12TUnzip" --desc="Archive directory for all raw files that go through TypeSort.cs" --machine="phcwap04"
dam new var X12Sort LogFolder "E:\Logs" --desc="Directory that log file will be stored" --machine="phcwap04"
dam new var X12Sort CRSites "2089" --desc="List of sites that use Mac line endings" --machine="phcwap04"
```
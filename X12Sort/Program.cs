﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using DataLayer;
using System.Text;
using System.Threading.Tasks;

namespace X12Sort
{
    class Program
    {
        
        static void Main(string[] args)
        {

            Settings.InitializeApplication(args);
            try
            {
                if (args[0].ToString() == "Type")
                {
                    _ = new TypeSort();
                }
                else if (args[0].ToString() == "Site")
                {
                    _ = new SiteSort();
                }
            } catch (Exception e)
            {
                Log.Write("Ran into an issue running the script. Find where this log stopped, that's the culprit.");
                Log.Write("If there's no actual output and just variables, you need to pass a Type or Site parameter to run this script");
                Log.Write($"{e}");
                Log.CloseLogFile();
                throw new Exception("X12Sort Completed Abnormally");
            }
        }
    }
}

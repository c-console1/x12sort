using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace CcrFileLoader
{
    public class LogSettings
    {
        public bool WriteToConsole { get; set; }
        public bool WriteToFile { get; set; }
        public bool WriteToDB { get; set; }
        public string FilePath { get; set; }
        public LogSettings()
        {
            WriteToConsole = true;
            WriteToFile = true;
            WriteToDB = true;
            FilePath = null;
        }
    }
    /// <summary>
    /// static class to handle logging operations
    /// </summary>
    public static class Log
    {
        // Log file writer
        private static StreamWriter _writer = null;
        public static LogSettings Configuration { get; set; }

        /// <summary>
        /// Initialize a log and its settings
        /// </summary>
        static Log()
        {
            Configuration = new LogSettings();
        }
        public static void WriteBreakLine()
        {
            Write("------------------------------------------------------------");
        }

        public static void WriteBreakLine(string line)
        {
            Write("------------------------------------------------------------");
            Write(line);
        }
        /// <summary>
        /// Writes a line to the database, console, and log file. DateTime prefix added for console and file.
        /// </summary>
        /// <param name="line">line to be logged</param>
        public static void Write(string line)
        {
            WriteToDB(line);
            line = $"[{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss").ToString()}]: " + line;
            WriteToConsole(line);
            WriteToFile(line);

        }
        /// <summary>
        /// Writes a line to only the console and log file. DateTime prefix is added to the line.
        /// </summary>
        /// <param name="line">line to be logged</param>
        public static void WriteNoDB(string line)
        {
            line = $"[{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss").ToString()}]: " + line;
            WriteToConsole(line);
            WriteToFile(line);
        }
        /// <summary>
        /// Writes a line to the console.
        /// </summary>
        /// <param name="line">line to be logged</param>
        private static void WriteToConsole(string line)
        {
            Console.WriteLine(line);
        }
        /// <summary>
        /// Writes a line to the logging database.
        /// </summary>
        /// <param name="line">line to be logged</param>
        private static void WriteToDB(string line)
        {
            try
            {
                Settings.Mount.Logging.Add("CCR", line);
            }
            catch (Exception ex)
            {
                Log.WriteNoDB(ex.Message);
                Log.WriteNoDB(ex.StackTrace);
                throw ex;
            }
        }
        /// <summary>
        /// Writes a line to the log file. Opens writer if needed.
        /// </summary>
        /// <param name="line">lien to be logged</param>
        private static void WriteToFile(string line)
        {
            try
            {
                OpenLogFile();
                _writer.WriteLine(line);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                throw ex;
            }

        }
        public static void OpenLogFile()
        {
            if (Configuration.WriteToFile && _writer == null)
                _writer = new StreamWriter(Configuration.FilePath, true);
        }
        public static void CloseLogFile()
        {
            if (Configuration.WriteToFile && _writer != null)
                _writer.Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataLayer;
using System.Data;
using System.IO;
using System.IO.Compression;
using PHCMessaging;
using System.Diagnostics;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;

namespace X12Sort
{

    public class TypeSort
    {

        public TypeSort()
        {
            Log.Write("--------------------- BEGIN TYPE SORT PROCESS ---------------------");
            Unzip();
            IsEncrypted();
            ODM277();
            X12File();
            Log.Write("--------------------- END TYPE SORT PROCESS ----------------------");
            Log.CloseLogFile();
        }
        private static void IsEncrypted()
        {
            //Checks to see if files have .pgp or .gpg extension, emails EDI if it catches them and moves to a hold directory.
            string SourceDir = Settings.GetDamSetting("TypeSourceDir");
            string[] inputFiles =
            Directory.GetFiles(Path.GetDirectoryName(SourceDir), Path.GetFileName(SourceDir));
            if (inputFiles.Length > 0)
            {
                string EmailFrom = Settings.GetDamSetting("EmailFrom");
                string EmailTo = Settings.GetDamSetting("EmailTo");
                string EmailSubject = Settings.GetDamSetting("EmailSubject");
                string EmailBody = Settings.GetDamSetting("EmailBody");
                string DestDir = Path.Combine(Settings.GetDamSetting("TypeDestDir"), "NeedsDecrypted");
                foreach (string filename in inputFiles)
                {
                    string name = Path.GetFileName(filename);
                    string extension = Path.GetExtension(filename);
                    if (extension.ToLower() == ".pgp" || extension.ToLower() == ".gpg")
                    {
                        Log.Write($"{name}\" is encrypted. Moving to hold and emailing EDI");
                        File.Move(filename, Path.Combine(DestDir, name));
                        Email.Send(EmailBody, EmailTo, EmailFrom, null, null, EmailSubject);
                    }
                }
            }

        }
        private static void ODM277()
        {
            //Filters out ODM 277 files to a hold directory as they'd end up going to the wrong place for Axiom.
            string SourceDir = Settings.GetDamSetting("TypeSourceDir");
            string[] inputFiles =
            Directory.GetFiles(Path.GetDirectoryName(SourceDir), Path.GetFileName(SourceDir));
            if (inputFiles.Length > 0)
            {
                string DestDir = Path.Combine(Settings.GetDamSetting("TypeDestDir"), "ODM277");
                foreach (string filename in inputFiles)
                {
                    string name = Path.GetFileName(filename);
                    string extension = Path.GetExtension(filename);
                    if (extension == ".277U")
                    {
                        Log.Write($"{name}\" is an ODM 277 file. Moving away so it doesn't go to 276 directory. Once the generic task is rerouted this won't be an issue");
                        File.Move(filename, Path.Combine(DestDir, name));
                    }
                }
            }
        }
        private static void X12File()
        {
            //Main sorting method of the TypeSort program.
            Log.Write("Starting main sorting method for X12 files");
            string SourceDir = Settings.GetDamSetting("TypeSourceDir");
            string CRSites = Settings.GetDamSetting("CRSites");
            string[] segmentTerms = { "\r", "\n" };
            string TransactionTypesRaw = Settings.GetDamSetting("TransactionTypes");
            string[] TransArray = TransactionTypesRaw.Split(';');
            Dictionary<string, string> TransactionTypes = new Dictionary<string, string>();
            foreach (string s in TransArray)
            {
                string[] temparray = s.Split(',');
                TransactionTypes.Add(temparray[0], temparray[1]);
            }
            string[] inputFiles =
            Directory.GetFiles(Path.GetDirectoryName(SourceDir), Path.GetFileName(SourceDir));
            if (inputFiles.Length > 0)
            {
                foreach (string filename in inputFiles)
                {
                    using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite))
                    {
                        using (StreamReader reader = new StreamReader(fs))
                        {
                            using (StreamWriter writer = new StreamWriter(fs))
                            {
                                writer.AutoFlush = true;
                                string ISA = GetISAData(filename, reader);
                                string ISA06 = ISA.Substring(35, 15);
                                //Site 2089 sends us CR line endings, it screws things up. Replace them with CRLF.
                                if (ISA06.Contains(CRSites))
                                {
                                    string file = reader.ReadToEnd();
                                    file = file.Replace("\r", "\r\n");
                                    fs.SetLength(0);
                                    writer.Write(file);
                                }
                                if (ISA06.Contains("\0"))
                                {
                                    ISA06 = "";
                                }
                                if (ISA06 == "" || ISA06 == " " || ISA06 == null)
                                {
                                    writer.Close();
                                    NotX12(filename);
                                    continue;
                                }
                                ConvertToTest(filename);
                                //Get the GS08 segment to figure out what the transaction type is.
                                string GSSeg = GetGSData(filename, reader);
                                string[] GSArr = GSSeg.Split('*');
                                string GS08 = GSArr[24];
                                if (GS08.Contains("~"))
                                {
                                    GS08 = GS08.Substring(0, GS08.LastIndexOf("~"));
                                }
                                else
                                {
                                    for (int i = 0; i < segmentTerms.Length; i++)
                                    {
                                        if (GS08.Contains(segmentTerms[i]))
                                        {
                                            GS08 = GS08.Substring(0, GS08.LastIndexOf(segmentTerms[i]));
                                            break;
                                        }
                                    }
                                }
                                //Now for the actual sorting logic!
                                if (TransactionTypes.TryGetValue(GS08, out string value))
                                {
                                    Log.Write($"{GS08} translates to {value}");
                                    string name = Path.GetFileName(filename);
                                    string DestDir = Path.Combine(Settings.GetDamSetting("TypeDestDir"), value);
                                    Log.Write($"{name} moved to <{DestDir}>");
                                    writer.Close();
                                    File.Move(filename, Path.Combine(DestDir, name));
                                }
                            }
                        }
                    }
                }
            }
        }
        private static void ConvertToTest(string file)
        {
            string TestEnv = Settings.GetDamSetting("TestEnv");
            string Machine = System.Environment.MachineName;
            using (FileStream fs = new FileStream(file, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite))
            {
                using (StreamReader reader = new StreamReader(fs))
                {
                    using (StreamWriter writer = new StreamWriter(fs))
                    {
                        writer.AutoFlush = true;
                        if (Machine == TestEnv)
                        {
                            string ISA = GetISAData(file, reader);
                            string ISA15 = ISA.Substring(102, 1);
                            if (ISA15 == "P")
                            {
                                Log.Write($"Running on <{Machine}>, changing ISA15 to T for Axiom");
                                string ending = ISA.Substring(104, 1);
                                switch (ending)
                                {
                                    case ":":
                                        Log.Write("ISA terminated with :");
                                        string ColEndFile = reader.ReadToEnd();
                                        ColEndFile = ColEndFile.Replace("*P*:", "*T*:");
                                        ClearReader(reader);
                                        fs.SetLength(0);
                                        writer.Write(ColEndFile);
                                        break;
                                    case "!":
                                        ClearReader(reader);
                                        Log.Write("ISA terminated with !");
                                        string ExcEndFile = reader.ReadToEnd();
                                        ExcEndFile = ExcEndFile.Replace("*P*!", "*T*!");
                                        ClearReader(reader);
                                        fs.SetLength(0);
                                        writer.Write(ExcEndFile);
                                        break;
                                    case ">":
                                        ClearReader(reader);
                                        Log.Write("ISA terminated with >");
                                        string GtEndFile = reader.ReadToEnd();
                                        GtEndFile = GtEndFile.Replace("*P*>", "*T*>");
                                        ClearReader(reader);
                                        fs.SetLength(0);
                                        writer.Write(GtEndFile);
                                        break;
                                }
                            }
                            else
                            {
                                Log.Write($"ISA15 was already <{ISA15}>, don't need to change it");
                            }
                        }
                        else
                        {
                            Log.Write($"Running on <{Machine}>, no need to correct ISA15.");
                        }
                    }
                }
            }
        }

        private static void NotX12(string filename)
        {
            string DestDir = Path.Combine(Settings.GetDamSetting("TypeDestDir"), "NotX12");
            string name = Path.GetFileName(filename);
            Log.Write($"{name} didn't pass any of the X12 tests so it's being moved to <{DestDir}>");
            File.Move(filename, Path.Combine(DestDir, name));
        }
        private static void Unzip()
        {
            Log.Write("Starting Unzip Method");
            string SourceDir = Settings.GetDamSetting("TypeSourceDir");
            string[] inputFiles =
            Directory.GetFiles(Path.GetDirectoryName(SourceDir), Path.GetFileName(SourceDir));
            if (inputFiles.Length > 0)
            {
                int counter = 0;
                string ArchiveDir = Path.Combine(Settings.GetDamSetting("ArchiveDir"), $"{DateTime.Now.ToString("yyyy-MM-dd HHmmss")}");
                Directory.CreateDirectory(ArchiveDir);
                string EmailFrom = Settings.GetDamSetting("EmailFrom");
                string EmailTo = Settings.GetDamSetting("EmailTo");
                foreach (string filename in inputFiles)
                {
                    string name = Path.GetFileName(filename);
                    File.Copy(filename, Path.Combine(ArchiveDir, name));
                    string extension = Path.GetExtension(filename);
                    if (extension.ToLower() == ".zip")
                    {
                        Log.Write($"{filename}\" is a zip file!");
                        try
                        {
                            ZipFile.ExtractToDirectory(filename, Path.GetDirectoryName(SourceDir));
                            File.Delete(filename);
                            Log.Write($"{filename}\" unzipped and original deleted");
                        }
                        catch
                        {
                            Log.Write($"Error occurred unzipping <{filename}>");
                            counter++;
                        }
                    }
                }
                Log.Write("Ending Unzip Method");
                if (counter > 0)
                {
                    Email.Send("A zip file had an issue unzipping, please check X12Sort 'Type' output for more details.", EmailTo, EmailFrom, null, null, "Zip file issue - X12Sort");
                }
            } else
            {
                Log.Write("No files to unzip, Ending Unzip Method");
            }
        }
        private static void ClearReader(StreamReader reader)
        {
            reader.DiscardBufferedData();
            reader.BaseStream.Seek(0, System.IO.SeekOrigin.Begin);
        }
        static string GetISAData(string file, StreamReader reader)
        {
            ClearReader(reader);
            char[] c = null;
            c = new char[106];
            reader.Read(c, 0, c.Length);
            StringBuilder builder = new StringBuilder();
            foreach (char ch in c)
            {
                builder.Append(ch);
            }
            string ISA = builder.ToString();
            ClearReader(reader);
            return ISA;
        }
        static string GetGSData(string file, StreamReader reader)
        {
            ClearReader(reader);
            char[] GS = null;
            GS = new char[250];
            reader.Read(GS, 0, GS.Length);
            StringBuilder builder = new StringBuilder();
            foreach (char cha in GS)
            {
                builder.Append(cha);
            }
            string GSSeg = builder.ToString();
            ClearReader(reader);
            return GSSeg;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FileIO
{
    public class Text
    {
        /// <summary>
        /// Creates a new text file
        /// </summary>
        /// <param name="path">Path to file, including file name and extension</param>
        /// <param name="text">Text to save into file</param>
        public static void CreateNewFile(string path, string text)
        {
            WriteFile(path, text, false);
        }

        /// <summary>
        /// Appends to an existing text file
        /// </summary>
        /// <param name="path">Path to file, including file name and extension</param>
        /// <param name="text">Text to save into file</param>
        public static void AppendExistingFile(string path, string text)
        {
            WriteFile(path, text, true);
        }

        /// <summary>
        /// Writes a new delimited file
        /// </summary>
        /// <param name="path">Path to file, including file name and extension</param>
        /// <param name="table">DataTable to write out</param>
        /// <param name="delimiter">Field delimiter</param>
        public static void CreateDelimitedFile(string path, DataTable table, string delimiter)
        {
            CreateDelimitedFile(path, table, delimiter, false);
        }

        /// <summary>
        /// Writes a new delimited file
        /// </summary>
        /// <param name="path">Path to file, including file name and extension</param>
        /// <param name="table">DataTable to write out</param>
        /// <param name="delimiter">Field delimiter</param>
        /// <param name="headerFromDataTable">Create row 1 of file as name of columns from DataTable</param>
        public static void CreateDelimitedFile(string path, DataTable table, string delimiter, bool headerFromDataTable)
        {
            StringBuilder builder = new StringBuilder();

            if (headerFromDataTable)
            {
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    if (i > 0)
                        builder.Append(delimiter);

                    builder.Append(table.Columns[i].ColumnName);
                }

                builder.AppendLine();
            }

            foreach (DataRow row in table.Rows)
            {
                IEnumerable<string> fields = row.ItemArray.Select(field => field.ToString());
                builder.AppendLine(string.Join(delimiter, fields));
            }
            WriteFile(path, builder.ToString(), append: false);
        }

        /// <summary>
        /// Writes a file to disk.
        /// </summary>
        /// <param name="path">Path to file, including file name and extension</param>
        /// <param name="text">Text to save into file</param>
        /// <param name="append">true = Append to existing file. false = Create new/overwrite existing file.</param>
        private static void WriteFile(string path, string text, bool append)
        {
            using (StreamWriter writer = new StreamWriter(path, append))
            {
                writer.Write(text);
            }
        }

        /// <summary>
        /// Converts delimited file to a Data Table. The first line may be a column header.
        /// </summary>
        /// <param name="file">Path to file, including file name and extension</param>
        /// <param name="delimiter">Delimiter that separates values</param>
        /// <param name="hasHeader">Flag to indicate if file has a header row</param>
        /// <returns> DataTable created from file</returns>
        public static DataTable ImportDelimitedFileToDataDable(string file, char delimiter, bool hasHeader = true)
        {
            if (!File.Exists(file))
            {
                throw new Exception($"File ({file}) does not exist. Please use a valid file path.");
            }

            using (StreamReader sr = new StreamReader(file))
            {
                string line;
                DataTable dt = new DataTable();
                bool isHeader = hasHeader;
                while ((line = sr.ReadLine()) != null)
                {
                    if (!hasHeader && dt.Columns.Count == 0)
                    {
                        int fieldCount = line.Split(delimiter).Length;
                        for (int i = 1; i <= fieldCount; i++)
                            dt.Columns.Add($"Column{i.ToString().PadLeft(3, '0')}");
                    }

                    if (isHeader)
                    {
                        string[] headers = line.Split(delimiter);
                        foreach (string header in headers)
                            dt.Columns.Add(header);
                        isHeader = false;
                    }
                    else
                        dt.Rows.Add(line.Split(delimiter));
                }
                return dt;
            }
        }

        /// <summary>
        /// Converts a csv file to a data table. File may be quoted (,,,,",",,). The first line may be a column header
        /// </summary>
        /// <param name="file">Path to file, including file name and extension</param>
        /// <param name="hasHeader">Flag to indicate if file has a header row</param>
        /// <returns></returns>
        public static DataTable ImportCSVFileToDataTable(string file, bool hasHeader = true)
        {
            if (!File.Exists(file))
            {
                throw new Exception($"File ({file}) does not exist. Please use a valid file path.");
            }

            using (StreamReader sr = new StreamReader(file))
            {
                string line;
                Regex quotedDelimiter = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                Regex digitMatch = new Regex("(\\d+)(?!.*\\d)");
                DataTable dt = new DataTable();
                bool isHeader = hasHeader;

                while ((line = sr.ReadLine()) != null)
                {
                    if (!hasHeader && dt.Columns.Count == 0)
                    {
                        string[] result = quotedDelimiter.Split(line);
                        int fieldCount = result.Length;
                        for (int i = 1; i <= fieldCount; i++)
                            dt.Columns.Add($"Column{i.ToString().PadLeft(3, '0')}");
                    }

                    if (isHeader)
                    {
                        string[] result = quotedDelimiter.Split(line);
                        int fieldCount = result.Length;
                        for (int i = 0; i < fieldCount; i++)
                        {
                            if (dt.Columns.Contains(result[i]))
                            {
                                int x = 0;

                                if (int.TryParse(digitMatch.Match(result[i]).Value, out x))
                                    x++;
                                else
                                    x = 2;

                                result[i] = result[i] + " " + x;
                            }

                            dt.Columns.Add(result[i]);
                        }
                        isHeader = false;
                    }
                    else
                        dt.Rows.Add(quotedDelimiter.Split(line));
                }
                return dt;
            }
        }

    }
}

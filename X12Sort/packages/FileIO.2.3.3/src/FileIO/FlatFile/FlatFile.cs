﻿using System.Collections.Generic;
using System.IO;
using System.Text;

/// <summary>
/// Namespace used to define classes for different file types
/// Known issues: Inheritance could be better.
/// </summary>
namespace FileIO.FlatFile
{
    #region File types
    /// <summary>
    /// Structure for a delimited file
    /// </summary>
    public abstract class DelimitedFile
    {
        #region Members
        /// <summary>
        /// Private member to store file records
        /// </summary>
        private List<DelimitedFileRecord> _records = new List<DelimitedFileRecord>();

        /// <summary>
        /// A list of file records
        /// </summary>
        public List<DelimitedFileRecord> Records
        {
            get
            {
                return _records;
            }
            set
            {
                _records = value;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Write a file to the specified location
        /// </summary>
        /// <param name="outDir">File path, including ending slash</param>
        /// <param name="outName">File name, including extension</param>
        public void WriteFile(string outDir, string outName)
        {
            WriteFile(Path.Combine(outDir, outName));
        }

        /// <summary>
        /// Write a file to the specified location
        /// </summary>
        /// <param name="path">Entire path including file name</param>
        public void WriteFile(string path)
        {
            using (StreamWriter w = new StreamWriter(path))
            {
                w.Write(ToString());
                w.Flush();
                w.Close();
            }
        }
        #endregion
    }

    /// <summary>
    /// Structure for a fixed width file
    /// </summary>
    public abstract class FixedFile
    {
        #region Members
        /// <summary>
        /// Private member to store file records
        /// </summary>
        private List<FixedFileRecord> _records = new List<FixedFileRecord>();

        /// <summary>
        /// A list of file records
        /// </summary>
        public List<FixedFileRecord> Records
        {
            get
            {
                return _records;
            }

            set
            {
                _records = value;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// String override to append the records together
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder str = new StringBuilder();

            foreach (FixedFileRecord rec in Records)
            {
                str.Append(rec);
                str.Append("\n");
            }

            return str.ToString();
        }

        /// <summary>
        /// Write a file to the specified location
        /// </summary>
        /// <param name="outDir">File path, including ending slash</param>
        /// <param name="outName">File name, including extension</param>
        public void WriteFile(string outDir, string outName)
        {
            WriteFile(Path.Combine(outDir, outName));
        }

        /// <summary>
        /// Write a file to the specified location
        /// </summary>
        /// <param name="path">Entire path including file name</param>
        public void WriteFile(string path)
        {
            using (StreamWriter w = new StreamWriter(path))
            {
                w.Write(ToString());
                w.Flush();
                w.Close();
            }
        }
        #endregion
    }

    /// <summary>
    /// Structure for creating a file that is fixed width but also delimited
    /// </summary>
    public abstract class FixedDelimitedFile
    {
        #region Members
        /// <summary>
        /// Private member to store file records
        /// </summary>
        private List<FixedDelimitedFileRecord> _records = new List<FixedDelimitedFileRecord>();

        /// <summary>
        /// A list of file records
        /// </summary>
        public List<FixedDelimitedFileRecord> Records
        {
            get
            {
                return _records;
            }

            set
            {
                _records = value;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// String override to append the records together
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder str = new StringBuilder();

            foreach (FixedDelimitedFileRecord rec in Records)
            {
                str.Append(rec);
                str.Append("\n");
            }

            return str.ToString();
        }
        /// <summary>
        /// Write a file to the specified location
        /// </summary>
        /// <param name="outDir">File path, including ending slash</param>
        /// <param name="outName">File name, including extension</param>
        public void WriteFile(string outDir, string outName)
        {
            WriteFile(Path.Combine(outDir, outName));
        }

        /// <summary>
        /// Write a file to the specified location
        /// </summary>
        /// <param name="path">Entire path including file name</param>
        public void WriteFile(string path)
        {
            using (StreamWriter w = new StreamWriter(path))
            {
                w.Write(ToString());
                w.Flush();
                w.Close();
            }
        }
        #endregion
    }
    #endregion

    #region Field Types
    /// <summary>
    /// Class represents a delimited field in a delimited file
    /// </summary>
    public class DelimitedFileField
    {
        #region Members
        /// <summary>
        /// Field value
        /// </summary>
        public string Value { get; set; }
        /// <summary>
        /// Field index. Used for sorting if necessary.
        /// </summary>
        public int Index { get; set; }
        #endregion

        #region Constructors
        public DelimitedFileField(int index, string value)
        {
            Index = index;
            Value = value;
        }
        #endregion
    }

    /// <summary>
    /// Class represents a fixed field in a fixed length file
    /// </summary>
    public class FixedFileField
    {
        #region Members
        /// <summary>
        /// Length of the field. Read-only.
        /// </summary>
        private readonly int _fieldLength;
        /// <summary>
        /// Value for the field
        /// </summary>
        public string Value { get; set; }
        /// <summary>
        /// Index represents the order in which the fields must appear//
        /// </summary>
        public int Index { get; set; }
        #endregion

        #region Properties
        /// <summary>
        /// Length of the field
        /// </summary>
        public int Length
        {
            get { return _fieldLength; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor. Specify an index, length, and value for the field.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="value"></param>
        /// <param name="fieldLength"></param>
        public FixedFileField(int index, string value, int fieldLength)
        {
            this.Index = index;
            this.Value = value;
            this._fieldLength = fieldLength;
        }
        #endregion
    }

    public class FixedDelimtedField
    {
        #region Members
        /// <summary>
        /// Length of the field. Read-only.
        /// </summary>
        private readonly int _fieldLength;

        /// <summary>
        /// Value for the field
        /// </summary>
        public string Value { get; set; }
        /// <summary>
        /// Index represents the order in which the fields must appear//
        /// </summary>
        public int Index { get; set; }
        #endregion

        #region Properties
        /// <summary>
        /// Length of the field
        /// </summary>
        public int Length
        {
            get { return _fieldLength; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor. Specify an index, length, and value for the field.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="value"></param>
        /// <param name="fieldLength"></param>
        /// <param name="delim">Specify a delimter to separate fixed fields by</param>
        public FixedDelimtedField(int index, string value, int fieldLength)
        {
            this.Index = index;
            this.Value = value;
            this._fieldLength = fieldLength;
            //this._delim = delim;
        }
        #endregion
    }
    #endregion

    #region Record types
    /// <summary>
    /// Class represents a single record in a delimited file
    /// </summary>
    public abstract class DelimitedFileRecord
    {
        #region Members
        /// <summary>
        /// Private member for holding field data
        /// </summary>
        protected Dictionary<string, DelimitedFileField> _fields;
        #endregion

        #region Properties
        /// <summary>
        /// Representation of a field: Fields["fieldName"].Value
        /// </summary>
        public Dictionary<string, DelimitedFileField> Fields
        {
            get { return _fields; }
        }
        public string Delim { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor. Initializes properties
        /// </summary>
        protected DelimitedFileRecord(string delim)
        {
            _fields = new Dictionary<string, DelimitedFileField>();
            Delim = delim;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Adds a new field element to the file record
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="field"></param>
        public void AddField(string fieldName, DelimitedFileField field)
        {
            _fields.Add(fieldName, field);
        }

        /// <summary>
        /// Sets the value of an existing field in the record
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="value"></param>
        public void SetField(string fieldName, string value)
        {
            _fields[fieldName].Value = value;
        }

        /// <summary>
        /// String override to append the proper length to the fields
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            DelimitedFileField[] orderedFields = new DelimitedFileField[Fields.Count];
            StringBuilder str = new StringBuilder();

            foreach (DelimitedFileField field in Fields.Values)
            {
                //re-order fields in record//
                //index -1 because dictionaries are 1 based, & array is 0 based//
                orderedFields[field.Index - 1] = field;
            }

            foreach (DelimitedFileField field in orderedFields)
            {
                str.Append($"{field.Value}{Delim}");
            }

            return str.ToString();
        }
        #endregion
    }

    /// <summary>
    /// Class represents a single record in a fixed length file
    /// </summary>
    public abstract class FixedFileRecord
    {
        /// <summary>
        /// Private member for holding field data
        /// </summary>
        protected Dictionary<string, FixedFileField> _fields;

        /// <summary>
        /// Represents field data in the fixed length file
        /// </summary>
        public Dictionary<string, FixedFileField> Fields
        {
            get { return _fields; }
        }

        /// <summary>
        /// Default constructor. Initializes properties.
        /// </summary>
        protected FixedFileRecord()
        {
            _fields = new Dictionary<string, FixedFileField>();
        }

        /// <summary>
        /// Adds a new field element to the file record
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="field"></param>
        public void AddField(string fieldName, FixedFileField field)
        {
            _fields.Add(fieldName, field);
        }

        /// <summary>
        /// Sets the value of an existing field in the record
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="value"></param>
        public void SetField(string fieldName, string value)
        {
            _fields[fieldName].Value = value;
        }

        /// <summary>
        /// String override to append the proper length to the fields
        /// </summary>
        public override string ToString()
        {
            FixedFileField[] orderedFields = new FixedFileField[Fields.Count];
            StringBuilder str = new StringBuilder();

            //foreach loop through dictionary//
            foreach (FixedFileField field in Fields.Values)
            {
                //re-order fields in record//
                //index -1 because dictionaries are 1 based, & array is 0 based//
                orderedFields[field.Index - 1] = field;
            }

            foreach (FixedFileField field in orderedFields)
            {
                //do padding to get correct length for field//
                str.Append(field.Value.PadRight(field.Length).Substring(0, field.Length));
            }

            return str.ToString();
        }
    }

    /// <summary>
    /// Structure for a record type which has fixed fields that are also delimited
    /// </summary>
    public abstract class FixedDelimitedFileRecord
    {
        protected Dictionary<string, FixedDelimtedField> _fields;

        /// <summary>
        /// The delimter to which fields are seperated by
        /// </summary>
        public string Delim { get; set; }

        /// <summary>
        /// Dictionary of fields
        /// </summary>
        public Dictionary<string, FixedDelimtedField> Fields
        {
            get { return _fields; }
        }

        /// <summary>
        /// Constructor, specify the delimter
        /// </summary>
        /// <param name="delim"></param>
        protected FixedDelimitedFileRecord(string delim)
        {
            _fields = new Dictionary<string, FixedDelimtedField>();
            Delim = delim;
        }

        /// <summary>
        /// Add a new field that wasn't previously initialized in the child class
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="field"></param>
        public void AddField(string fieldName, FixedDelimtedField field)
        {
            _fields.Add(fieldName, field);
        }

        /// <summary>
        /// Set the value of an existing field in the layout
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="value"></param>
        public void SetField(string fieldName, string value)
        {
            _fields[fieldName].Value = value;
        }

        /// <summary>
        /// String override to append the proper length to the fields
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            FixedDelimtedField[] orderedFields = new FixedDelimtedField[Fields.Count];
            StringBuilder str = new StringBuilder();

            foreach (FixedDelimtedField field in Fields.Values)
            {
                //re-order fields in record//
                //index -1 because dictionaries are 1 based, & array is 0 based//
                orderedFields[field.Index - 1] = field;
            }

            //Find the last field
            var last = orderedFields[Fields.Count - 1];

            foreach (FixedDelimtedField field in orderedFields)
            {
                //This check for the last record was added because the redcard vendor was reporting an extra space at the end of each record.
                //Since the last field is rarely used for the redcard format, I decided to just remove a space from it. 
                //If this class is to be added to the FileIO library, i recommend this be removed in favor of a more well rounded solution.
                if (field.Index == last.Index)
                {
                    //do padding and add delimiter to get correct length for field
                    //The length-1 logic is specific to the redcard format, i don't recommend it being used if this class is added to the FileIO library.
                    str.Append($"{field.Value.PadRight(field.Length).Substring(0, field.Length - 1)}{Delim}");
                }
                else
                {
                    //do padding and add delimiter to get correct length for field//
                    str.Append($"{field.Value.PadRight(field.Length).Substring(0, field.Length)}{Delim}");
                }
            }

            return str.ToString();
        }
    }
    #endregion
}


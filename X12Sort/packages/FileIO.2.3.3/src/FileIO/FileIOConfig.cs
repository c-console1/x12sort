﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileIO
{
    /// <summary>
    /// Users configuration for the FileIOConfig class. All user configurable settings should be placed in this class so they can be load/saved.
    /// </summary>
    /// <seealso cref="FileIOConfigBuilder"/>
    /// <seealso cref="DbExec.Create"/>
    public class FileIOConfig
    {
        #region File Operations Properties
        /// <summary>
        /// Root folder for archiving files
        /// </summary>
        public string ArchiveRoot { get; set; }
        #endregion

        #region Reporting Properties
        /// <summary>
        /// Default character used to make lines on report
        /// </summary>
        public char ReportDefaultLineCharacter { get; set; }

        /// <summary>
        /// Maximum width of individual report line
        /// </summary>
        public int ReportMaxWidth { get; set; }
        
        /// <summary>
        /// Base folder for Cypress input folders
        /// </summary>
        public string CypressBaseFolder { get; set; }

        /// <summary>
        /// Sub folder for Cypress DEV6 input
        /// </summary>
        public string CypressDEV6SubFolder { get; set; }

        /// <summary>
        /// Sub folder for Cypress PFIG input
        /// </summary>
        public string CypressPFIGSubFolder { get; set; }

        /// <summary>
        /// Sub folder for Cypress PROD input
        /// </summary>
        public string CypressPRODSubFolder { get; set; }

        /// <summary>
        /// Sub folder for Cypress TESTA input
        /// </summary>
        public string CypressTESTASubFolder { get; set; }

        /// <summary>
        /// Sub folder for Cypress TESTB input
        /// </summary>
        public string CypressTESTBSubFolder { get; set; }

        /// <summary>
        /// Sub folder for Cypress TESTC input
        /// </summary>
        public string CypressTESTCSubFolder { get; set; }

        /// <summary>
        /// Sub folder for Cypress TESTD input
        /// </summary>
        public string CypressTESTDSubFolder { get; set; }

        /// <summary>
        /// Sub folder for Cypress TESTE input
        /// </summary>
        public string CypressTESTESubFolder { get; set; }

        /// <summary>
        /// Sub folder for Cypress TFIG input
        /// </summary>
        public string CypressTFIGSubFolder { get; set; }

        /// <summary>
        /// Default Cypress report file extension 
        /// </summary>
        public string CypressFileExtension { get; set; }

        /// <summary>
        /// Billing database name
        /// </summary>
        public string BillingDatabaseName { get; set; }

        /// <summary>
        /// Billing database connection name for PFIG
        /// </summary>
        public string BillingPFIGConnectionName { get; set; }

        /// <summary>
        /// Billing database connection name for PROD
        /// </summary>
        public string BillingPRODConnectionName { get; set; }

        /// <summary>
        /// Billing database connection name for TESTA
        /// </summary>
        public string BillingTESTAConnectionName { get; set; }

        /// <summary>
        /// Billing database connection name for TESTB
        /// </summary>
        public string BillingTESTBConnectionName { get; set; }

        /// <summary>
        /// Billing database connection name for TESTC
        /// </summary>
        public string BillingTESTCConnectionName { get; set; }

        /// <summary>
        /// Billing database connection name for TESTD
        /// </summary>
        public string BillingTESTDConnectionName { get; set; }

        /// <summary>
        /// Billing database connection name for TESTE
        /// </summary>
        public string BillingTESTEConnectionName { get; set; }

        /// <summary>
        /// Billing database connection name for TFIG
        /// </summary>
        public string BillingTFIGConnectionName { get; set; }
        #endregion

        #region X12 Properties
        /// <summary>
        /// Fixed length position of segment terminator in interchange header
        /// </summary>
        public int X12SegmentTerminatorPosition { get; set; }

        /// <summary>
        /// Fixed length position of element separator in interchange header
        /// </summary>
        public int X12ElementSeparatorPosition { get; set; }

        /// <summary>
        /// Fixed length position of composite separator in interchange header
        /// </summary>
        public int X12CompositeSeparatorPosition { get; set; }

        /// <summary>
        /// Interchange header segment name
        /// </summary>
        public string X12InterchangeHeaderName { get; set; }

        /// <summary>
        /// Interchange trailer segment name
        /// </summary>
        public string X12InterchangeTrailerName { get; set; }

        /// <summary>
        /// Length of interchange header segment
        /// </summary>
        public int X12InterchangeHeaderLength { get; set; }

        /// <summary>
        /// Location of identifier element a segment
        /// </summary>
        public int X12InterchangeIdentifierElementNumber { get; set; }

        /// <summary>
        /// Element number in ISA segment where ISA version is located
        /// </summary>
        public int X12InterchangeVersionElementNumber { get; set; }

        /// <summary>
        /// Element number in ISA segment where ISA environment is located
        /// </summary>
        public int X12InterchangeEnvironmentElementNumber { get; set; }

        /// <summary>
        /// Functional group header segment name
        /// </summary>
        public string X12FunctionalGroupHeaderName { get; set; }

        /// <summary>
        /// Functional group trailer segment name
        /// </summary>
        public string X12FunctionalGroupTrailerName { get; set; }

        /// <summary>
        /// Location of identifier element in functional group header segment
        /// </summary>
        public int X12FunctionalGroupIdentifierElementNumber { get; set; }

        /// <summary>
        /// Transaction set header segment name
        /// </summary>
        public string X12TransactionSetHeaderName { get; set; }

        /// <summary>
        /// Transaction set trailer segment name
        /// </summary>
        public string X12TransactionSetTrailerName { get; set; }

        /// <summary>
        /// Location of identifier element in transaction set header segment
        /// </summary>
        public int X12TransactionSetIdentifierElementNumber { get; set; }
        #endregion

        public FileIOConfig(string environment)
        {
            // Set universal configuration
            ReportDefaultLineCharacter = '-';
            ReportMaxWidth = 130;
            CypressFileExtension = "rpt";
            CypressDEV6SubFolder = "RDM_PHC_DEV6_CypressFormat";
            CypressPFIGSubFolder = "RDM_PHC_PFIG6_CypressFormat";
            CypressPRODSubFolder = "RDM_PHC_PROD_CypressFormat";
            CypressTESTASubFolder = "RDM_PHCAA_TSTA_CypressFormat";
            CypressTESTBSubFolder = "RDM_PHCAA_TSTB_CypressFormat";
            CypressTESTCSubFolder = "RDM_PHCAA_TSTC_CypressFormat";
            CypressTESTDSubFolder = "RDM_PHCAA_TSTD_CypressFormat";
            CypressTESTESubFolder = "RDM_PHCAA_TSTE_CypressFormat";
            CypressTFIGSubFolder = "RDM_PHC_TFIG_CypressFormat";
            BillingDatabaseName = "BILLING";
            BillingPFIGConnectionName = "AMISYS_PFIG";
            BillingPRODConnectionName = "AMISYS_PROD";
            BillingTESTAConnectionName = "AMISYS_TESTA";
            BillingTESTBConnectionName = "AMISYS_TESTB";
            BillingTESTCConnectionName = "AMISYS_TESTC";
            BillingTESTDConnectionName = "AMISYS_TESTD";
            BillingTESTEConnectionName = "AMISYS_TESTE";
            BillingTFIGConnectionName = "AMISYS_TFIG";
            X12SegmentTerminatorPosition = 105;
            X12ElementSeparatorPosition = 103;
            X12CompositeSeparatorPosition = 104;
            X12InterchangeHeaderName = "ISA";
            X12InterchangeTrailerName = "IEA";
            X12InterchangeHeaderLength = 106;
            X12InterchangeIdentifierElementNumber = 1;
            X12InterchangeVersionElementNumber = 12;
            X12InterchangeEnvironmentElementNumber = 15;
            X12FunctionalGroupHeaderName = "GS";
            X12FunctionalGroupTrailerName= "GE";
            X12FunctionalGroupIdentifierElementNumber = 1;
            X12TransactionSetHeaderName = "ST";
            X12TransactionSetTrailerName = "SE";
            X12TransactionSetIdentifierElementNumber = 2;

            // Set environment specific configuration
            switch (environment)
            {
                case "dev":
                    ArchiveRoot = @"c:\jobs\temp\archive";
                    CypressBaseFolder = @"\\cypress01\paramount\";
                    break;
                case "qa":
                    ArchiveRoot = @"e:\jobs\temp\qa\archive";
                    CypressBaseFolder = @"\\cypress01\paramount\";
                    break;
                case "prod":
                    ArchiveRoot = @"e:\jobs\temp\archive";
                    CypressBaseFolder = @"\\cypress01\paramount\";
                    BillingPRODConnectionName = "AmisysPRODOracle";
                    break;
                default:
                    throw new Exception($"Invalid environment defined: {environment}");
            }
        }
    }
}

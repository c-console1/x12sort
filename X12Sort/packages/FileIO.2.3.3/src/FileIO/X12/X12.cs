﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FileIO.X12
{
    /// <summary>
    /// Contains details necessary to interpret an X12 file
    /// </summary>
    public class X12FileLayout
    {
        #region Constants/Variables
        /// <summary>
        /// Segment terminator internal variable
        /// </summary>
        private string _segmentTerminator;

        /// <summary>
        /// Element separator internal variable
        /// </summary>
        private string _elementSeparator;

        /// <summary>
        /// Element composite separator internal variable
        /// </summary>
        private string _compositeSeparator;
        #endregion

        #region Properties
        /// <summary>
        /// String used to terminate segments
        /// </summary>
        public string SegmentTerminator { get { return _segmentTerminator; } }

        /// <summary>
        /// String used to separate elements
        /// </summary>
        public string ElementSeparator { get { return _elementSeparator; } }

        /// <summary>
        /// String used to separate sub-elements within composite elements
        /// </summary>
        public string CompositeSeparator { get { return _compositeSeparator; } }
        #endregion

        #region Constructors
        /// <summary>
        /// Build X12 file layout
        /// </summary>
        /// <param name="segmentTerminator">String used to terminate segments</param>
        /// <param name="elementSeparator">String used to separate elements</param>
        /// <param name="compositeSeparator">String used to separate sub-elements within composite elements</param>
        public X12FileLayout(string segmentTerminator, string elementSeparator, string compositeSeparator)
        {
            _segmentTerminator = segmentTerminator;
            _elementSeparator = elementSeparator;
            _compositeSeparator = compositeSeparator;
        }
        #endregion
    }

    /// <summary>
    /// Contains all contents of an X12 file
    /// </summary>
    public class X12File
    {
        #region Constants/Variables
        /// <summary>
        /// Contains details necessary to interpret an X12 file
        /// </summary>
        private X12FileLayout _layout;

        /// <summary>
        /// Dictionary of interchange envelopes
        /// </summary>
        private Dictionary<string, X12InterchangeEnvelope> _interchangeEnvelopes;
        #endregion

        #region Properties
        /// <summary>
        /// Looks up an interchange envelope by identifier
        /// </summary>
        /// <param name="interchangeKey">Interchange envelope identifier</param>
        /// <returns>Specified interchange envelope</returns>
        public X12InterchangeEnvelope this[string interchangeKey] { get { return _interchangeEnvelopes[interchangeKey]; } }

        /// <summary>
        /// Contains details necessary to interpret an X12 file
        /// </summary>
        public X12FileLayout Layout { get; }

        /// <summary>
        /// Returns first interchange envelope (typically, there is only one interchange per file)
        /// </summary>
        public X12InterchangeEnvelope Interchange { get { return _interchangeEnvelopes.Values.First(); } }

        /// <summary>
        /// Whether or not the file contains multiple interchanges
        /// </summary>
        public bool HasMultipleInterchanges { get { return _interchangeEnvelopes.Values.Count > 1; } }
        #endregion

        #region Constructors
        /// <summary>
        /// Creates an X12 file object based on string contents of a file
        /// </summary>
        /// <param name="fileContents">String contents of a file</param>
        public X12File(string fileContents)
        {
            _layout = GetLayoutFromString(fileContents);
            List<X12Segment> segmentList = X12Envelope.GetSegmentsFromString(_layout, fileContents);
            _interchangeEnvelopes = X12Envelope
                .SplitSegmentList(segmentList, FileIOConfigBuilder.Instance.Config.X12InterchangeHeaderName, FileIOConfigBuilder.Instance.Config.X12InterchangeTrailerName)
                .Select(sl => new X12InterchangeEnvelope(sl))
                .ToDictionary(isa => isa.Header.Elements[FileIOConfigBuilder.Instance.Config.X12InterchangeIdentifierElementNumber].Value);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Extract file layout from te ctring contents of a file
        /// </summary>
        /// <param name="fileContents">String contents of a file</param>
        /// <returns></returns>
        public static X12FileLayout GetLayoutFromString(string fileContents)
        {
            // Validate file contents
            ValidateFile(fileContents);

            // Build file layout
            return new X12FileLayout(
                fileContents.Substring(FileIOConfigBuilder.Instance.Config.X12SegmentTerminatorPosition, 2) == "\r\n"
                    ? "\r\n"
                    : fileContents[FileIOConfigBuilder.Instance.Config.X12SegmentTerminatorPosition].ToString(),
                fileContents[FileIOConfigBuilder.Instance.Config.X12ElementSeparatorPosition].ToString(),
                fileContents[FileIOConfigBuilder.Instance.Config.X12CompositeSeparatorPosition].ToString());
        }

        /// <summary>
        /// Validate the string contents of a file
        /// </summary>
        /// <param name="fileContents">String contents of a file</param>
        /// <returns>Whether or not a file is valid</returns>
        public static bool ValidateFile(string fileContents)
        {
            if (string.IsNullOrEmpty(fileContents)) throw new Exception($"File is empty");
            if (fileContents.Length < FileIOConfigBuilder.Instance.Config.X12InterchangeHeaderLength + 1) throw new Exception($"{FileIOConfigBuilder.Instance.Config.X12InterchangeHeaderName} segment is too short (min length {FileIOConfigBuilder.Instance.Config.X12InterchangeHeaderLength})");
            // TODO: finish validation
            return true;
        }
        #endregion
    }

    /// <summary>
    /// Represents an abstract version of an envelope
    /// </summary>
    public abstract class X12Envelope
    {
        #region Constants/Variables
        /// <summary>
        /// Header segment for envelope
        /// </summary>
        protected X12Segment _header;

        /// <summary>
        /// Trailer segment for envelope
        /// </summary>
        protected X12Segment _trailer;
        #endregion

        #region Properties
        /// <summary>
        /// Header segment for envelope
        /// </summary>
        public X12Segment Header { get { return _header; } }

        /// <summary>
        /// Trailer segment for envelope
        /// </summary>
        public X12Segment Trailer { get { return _trailer; } }
        #endregion

        #region Methods
        /// <summary>
        /// Get a list of segments from an envelope
        /// </summary>
        /// <param name="layout">Contains details necessary to interpret an X12 file</param>
        /// <param name="envelopeContents">String contents of an envelope</param>
        /// <returns>Full list of segments in envelope</returns>
        public static List<X12Segment> GetSegmentsFromString(X12FileLayout layout, string envelopeContents)
        {
            // Validate element contents
            ValidateString(envelopeContents);

            // Get all elements for the segment excluding segment name
            return envelopeContents
                .Split(new string[] { layout.SegmentTerminator }, StringSplitOptions.None)
                .Select(s => new X12Segment(layout, s))
                .ToList();
        }

        /// <summary>
        /// Validate the string contents of an envelope
        /// </summary>
        /// <param name="envelopeContents">String contents of an envelope</param>
        /// <returns>Whether or not an envelope is valid</returns>
        public static bool ValidateString(string envelopeContents)
        {
            // TODO: validate segment
            return true;
        }

        /// <summary>
        /// Split a list of segments into multiple segment lists to be converted to envelopes
        /// </summary>
        /// <param name="segments">List of all segments from a file or envelope</param>
        /// <param name="headerSegmentName">Name of header segment for envelope</param>
        /// <param name="trailerSegmentName">Name of trailer segment for envelope</param>
        /// <returns>List of segment lists, each starting with a header and ending with a trailer</returns>
        public static List<List<X12Segment>> SplitSegmentList(List<X12Segment> segments, string headerSegmentName, string trailerSegmentName)
        {
            List<List<X12Segment>> splitSegments = new List<List<X12Segment>>();
            bool buildingSegmentList = false;
            foreach(X12Segment segment in segments)
            {
                if (buildingSegmentList)
                {
                    splitSegments.Last().Add(segment);
                    buildingSegmentList = (segment.Name != trailerSegmentName);
                }
                else if (segment.Name == headerSegmentName)
                {
                    buildingSegmentList = true;
                    splitSegments.Add(new List<X12Segment>());
                    splitSegments.Last().Add(segment);
                }
            }
            return splitSegments;
        }
        #endregion
    }

    /// <summary>
    /// Contains contents of an ISA/IEA envelope
    /// </summary>
    public class X12InterchangeEnvelope : X12Envelope
    {
        #region Constants/Variables
        /// <summary>
        /// Dictionary of functional group envelopes
        /// </summary>
        private Dictionary<string, X12FunctionalGroupEnvelope> _fgEnvelopes;
        #endregion

        #region Properties
        /// <summary>
        /// Dictionary of functional group envelopes
        /// </summary>
        /// <param name="fgKey">Functional group envelope identifier</param>
        /// <returns>Functional group envelope at specified identifier</returns>
        public X12FunctionalGroupEnvelope this[string fgKey] { get { return _fgEnvelopes[fgKey]; } }

        /// <summary>
        /// Returns first functional group envelope (typically, there is only one functional group per interchange)
        /// </summary>
        public X12FunctionalGroupEnvelope FunctionalGroup { get { return _fgEnvelopes.Values.First(); } }

        /// <summary>
        /// Whether or not the interchange contains multiple functional groups
        /// </summary>
        public bool HasMultipleFunctionalGroups { get { return _fgEnvelopes.Values.Count > 1; } }

        /// <summary>
        /// ISA12 - Interchange X12 format version
        /// </summary>
        public ISAVersion Version { get { return X12ElementMap.MapValue<ISAVersion>(FileIOConfigBuilder.Instance.Config.X12InterchangeHeaderName, FileIOConfigBuilder.Instance.Config.X12InterchangeVersionElementNumber, _header[FileIOConfigBuilder.Instance.Config.X12InterchangeVersionElementNumber].Value); } }

        /// <summary>
        /// ISA15 - Environment for which the interchange is intended
        /// </summary>
        public ISAEnvironment Environment { get { return X12ElementMap.MapValue<ISAEnvironment>(FileIOConfigBuilder.Instance.Config.X12InterchangeHeaderName, FileIOConfigBuilder.Instance.Config.X12InterchangeEnvironmentElementNumber, _header[FileIOConfigBuilder.Instance.Config.X12InterchangeEnvironmentElementNumber].Value); } }
        #endregion

        #region Constructors
        /// <summary>
        /// Builds an interchange envelope object
        /// </summary>
        /// <param name="segmentList">List of envelope segments in interchange</param>
        public X12InterchangeEnvelope(List<X12Segment> segmentList)
        {
            // Get header
            _header = segmentList[0];
            segmentList.RemoveAt(0);

            // Get trailer
            _trailer = segmentList[segmentList.Count - 1];
            segmentList.RemoveAt(segmentList.Count - 1);

            // Get remaining functional group envelopes
            _fgEnvelopes = 
                SplitSegmentList(segmentList, FileIOConfigBuilder.Instance.Config.X12FunctionalGroupHeaderName, FileIOConfigBuilder.Instance.Config.X12FunctionalGroupTrailerName)
                .Select(sl => new X12FunctionalGroupEnvelope(sl))
                .ToDictionary(gs => gs.Header.Elements[FileIOConfigBuilder.Instance.Config.X12FunctionalGroupIdentifierElementNumber].Value);
        }
        #endregion
    }

    /// <summary>
    /// Contains contents of a GS/GE envelope
    /// </summary>
    public class X12FunctionalGroupEnvelope : X12Envelope
    {
        #region Constants/Variables
        /// <summary>
        /// Dictionary of transaction set envelopes
        /// </summary>
        private Dictionary<string, X12TransactionSetEnvelope> _tsEnvelopes;
        #endregion

        #region Properties
        /// <summary>
        /// Dictionary of transaction set envelopes
        /// </summary>
        /// <param name="tsKey">Transaction set envelope identifier</param>
        /// <returns>Transaction set envelope at specified identifier</returns>
        public X12TransactionSetEnvelope this[string tsKey] { get { return _tsEnvelopes[tsKey]; } }

        /// <summary>
        /// Dictionary of transaction set envelopes
        /// </summary>
        public Dictionary<string, X12TransactionSetEnvelope> TransactionSets { get { return _tsEnvelopes; } }
        #endregion

        #region Constructors
        /// <summary>
        /// Builds a functional group envelope object
        /// </summary>
        /// <param name="segmentList">List of envelope segments in functional group</param>
        public X12FunctionalGroupEnvelope(List<X12Segment> segmentList)
        {
            // Get header
            _header = segmentList[0];
            segmentList.RemoveAt(0);

            // Get trailer
            _trailer = segmentList[segmentList.Count - 1];
            segmentList.RemoveAt(segmentList.Count - 1);

            // Ger remaining transaction set envelopes
            _tsEnvelopes = 
                SplitSegmentList(segmentList, FileIOConfigBuilder.Instance.Config.X12TransactionSetHeaderName, FileIOConfigBuilder.Instance.Config.X12TransactionSetTrailerName)
                .Select(sl => new X12TransactionSetEnvelope(sl))
                .ToDictionary(st => st.Header.Elements[FileIOConfigBuilder.Instance.Config.X12TransactionSetIdentifierElementNumber].Value);
        }
        #endregion
    }

    /// <summary>
    /// Contains contents of an ST/SE envelope
    /// </summary>
    public class X12TransactionSetEnvelope : X12Envelope
    {
        #region Constants/Variables
        /// <summary>
        /// List of envelope segments. First segment is the header, while the last segment is the trailer.
        /// </summary>
        private X12Segment[] _segments;
        #endregion

        #region Properties
        /// <summary>
        /// List of envelope segments. First segment is the header, while the last segment is the trailer.
        /// </summary>
        /// <param name="index">Segment index</param>
        /// <returns>Segment at the specified index</returns>
        public X12Segment this[int index] { get { return _segments[index]; } }

        /// <summary>
        /// List of envelope segments. First segment is the header, while the last segment is the trailer.
        /// </summary>
        public X12Segment[] Segments { get { return _segments; } }
        #endregion

        #region Constructors
        /// <summary>
        /// Builds a transaction set envelope object
        /// </summary>
        /// <param name="segmentList">List of envelope segments</param>
        public X12TransactionSetEnvelope(List<X12Segment> segmentList)
        {
            // Get header
            _header = segmentList[0];
            segmentList.RemoveAt(0);

            // Get trailer
            _trailer = segmentList[segmentList.Count - 1];
            segmentList.RemoveAt(segmentList.Count - 1);

            // Get remaining segments
            _segments = segmentList.ToArray();
        }
        #endregion
    }

    /// <summary>
    /// Contains elements and name of a specific X12 segment
    /// </summary>
    public class X12Segment
    {
        #region Constants/Variables
        /// <summary>
        /// Array of segment elements
        /// </summary>
        private X12Element[] _elements;
        #endregion

        #region Properties
        /// <summary>
        /// Array of segment elements. Zeroth element is the segment name.
        /// </summary>
        /// <param name="index">Element index</param>
        /// <returns>Element at the specified index</returns>
        public X12Element this[int index] { get { return _elements[index]; } }

        /// <summary>
        /// Array of segment elements
        /// </summary>
        public X12Element[] Elements { get { return _elements; } }

        /// <summary>
        /// Segment name (also zeroth element)
        /// </summary>
        public string Name { get { return _elements[0].Value; } }
        #endregion

        #region Constructors
        /// <summary>
        /// Build segment from a string
        /// </summary>
        /// <param name="layout">Contains details necessary to interpret an X12 file</param>
        /// <param name="segmentContents">String contents of a segment</param>
        public X12Segment(X12FileLayout layout, string segmentContents)
        {
            _elements = GetElementsFromString(layout, segmentContents);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Get a list of elements from a segment string
        /// </summary>
        /// <param name="layout">Contains details necessary to interpret an X12 file</param>
        /// <param name="segmentContents">String contents of a segment</param>
        /// <returns>List of elements in segment</returns>
        public static X12Element[] GetElementsFromString(X12FileLayout layout, string segmentContents)
        {
            // Validate segment contents
            ValidateString(segmentContents);

            // Get all elements for the segment excluding segment name
            return segmentContents
                .Split(new string[] { layout.ElementSeparator }, StringSplitOptions.None)
                .Select(e => new X12Element(layout, e))
                .ToArray();
        }

        /// <summary>
        /// Validate the string contents of a segment
        /// </summary>
        /// <param name="segmentContents">String contents of a segment</param>
        /// <returns>Whether or not a segment is valid</returns>
        public static bool ValidateString(string segmentContents)
        {
            // TODO: validate segment
            return true;
        }
        #endregion
    }

    /// <summary>
    /// Contains a specific element with one or multiple (composite) values
    /// </summary>
    public class X12Element
    {
        #region Constants/Variables
        /// <summary>
        /// Array of element values
        /// </summary>
        private string[] _values;
        #endregion

        #region Properties
        /// <summary>
        /// Whether or not element is a composite element (aka, it has more than one value)
        /// </summary>
        public bool IsComposite { get { return _values.Length > 1; } }

        /// <summary>
        /// First element value (typically, elements only have one value)
        /// </summary>
        public string Value { get { return _values[0]; } }

        /// <summary>
        /// Array of element values
        /// </summary>
        public string[] Values { get { return _values; } }
        #endregion

        #region Constructors
        /// <summary>
        /// Build element from a string
        /// </summary>
        /// <param name="layout">Contains details necessary to interpret an X12 file</param>
        /// <param name="elementContents">String contents of an element</param>
        public X12Element(X12FileLayout layout, string elementContents)
        {
            _values = GetValuesFromString(layout, elementContents);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Get list of values in an element string
        /// </summary>
        /// <param name="layout">Contains details necessary to interpret an X12 file</param>
        /// <param name="elementContents">String contents of an element</param>
        /// <returns>List of values in an element</returns>
        public static string[] GetValuesFromString(X12FileLayout layout, string elementContents)
        {
            // Validate element contents
            ValidateString(elementContents);

            // Parse element contents
            return elementContents
                .Split(new string[] { layout.CompositeSeparator }, StringSplitOptions.None);
        }

        /// <summary>
        /// Validate the string contents of an element
        /// </summary>
        /// <param name="elementContents">String contents of an element</param>
        /// <returns>Whether or not an element is valid</returns>
        public static bool ValidateString(string elementContents)
        {
            // TODO: validate element
            return true;
        }
        #endregion
    }
}

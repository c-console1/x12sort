﻿using System;
using System.Collections.Generic;

namespace FileIO.X12
{
    /// <summary>
    /// Static class used to convert elements between snumerations and string values
    /// </summary>
    public static class X12ElementMap
    {
        #region Constants/Variables
        /// <summary>
        /// Number of digits in an element number when sent as a segment-element combination string
        /// </summary>
        private static readonly int ELEMENT_NUMBER_LENGTH = 2;
        #endregion

        #region Enumeration mapping
        /// <summary>
        /// Enumeration map that is used to convert an element between an enumeration and a string value
        /// </summary>
        private static Dictionary<string, List<X12ElementValueMap<dynamic>>> _elementCrosswalk = new Dictionary<string, List<X12ElementValueMap<dynamic>>>
        {
            #region Enumeration map for ISA12 (ISA Version)
            {
                "ISA12", new List<X12ElementValueMap<dynamic>>
                {
                    new X12ElementValueMap<dynamic> ( "ISA", 12, "0401", ISAVersion.v4010 ),
                    new X12ElementValueMap<dynamic> ( "ISA", 12, "0501", ISAVersion.v5010 )
                }
            },
            #endregion
            #region Enumeration map for ISA15 (ISA Environment)
            {
                "ISA15", new List<X12ElementValueMap<dynamic>>
                {
                    new X12ElementValueMap<dynamic> ( "ISA", 15, "P", ISAEnvironment.Production ),
                    new X12ElementValueMap<dynamic> ( "ISA", 15, "T", ISAEnvironment.Test )
                }
            }
            #endregion
        };
        #endregion

        #region Methods
        /// <summary>
        /// Get a list of element schemas that map between string values and enumerations
        /// </summary>
        /// <param name="segment">Segment name to look up</param>
        /// <param name="element">Element number to look up</param>
        /// <returns>Element map</returns>
        public static List<X12ElementValueMap<dynamic>> GetMap(string segment, int element)
        {
            return GetMap($"{segment}{element.ToString(new string('0', ELEMENT_NUMBER_LENGTH))}");
        }

        /// <summary>
        /// Get a list of element schemas that map between string values and enumerations
        /// </summary>
        /// <param name="location">Location string to look up (segment name + element number)</param>
        /// <returns>Element map</returns>
        public static List<X12ElementValueMap<dynamic>> GetMap(string location)
        {
            if (!_elementCrosswalk.ContainsKey(location)) throw new Exception($"Location '{location}' is not defined in the _elementCrosswalk dictionary.");
            else return _elementCrosswalk[location];
        }

        /// <summary>
        /// Convert an element from a string value to an enumeration
        /// </summary>
        /// <typeparam name="T">Enumeration return type</typeparam>
        /// <param name="segment">Segment name to map</param>
        /// <param name="element">Element number to map</param>
        /// <param name="value">Value to convert to an enumeration</param>
        /// <returns>Enumeration representation of string value</returns>
        public static T MapValue<T>(string segment, int element, string value)
        {
            return MapValue<T>($"{segment}{element.ToString(new string('0', ELEMENT_NUMBER_LENGTH))}", value);
        }

        /// <summary>
        /// Convert an element from a string value to an enumeration
        /// </summary>
        /// <typeparam name="T">Enumeration return type</typeparam>
        /// <param name="location">Location string to map (segment name + element number)</param>
        /// <param name="value">Value to convert to an enumeration</param>
        /// <returns>Enumeration representation of string value</returns>
        public static T MapValue<T>(string location, string value)
        {
            if (!_elementCrosswalk.ContainsKey(location)) throw new Exception($"Location '{location}' is not defined in the _elementCrosswalk dictionary.");
            foreach (X12ElementValueMap<dynamic> schema in _elementCrosswalk[location])
                if (schema.OriginalValue == value)
                    return schema.MappedValue;
            throw new Exception($"Map data for location '{location}' and value '{value}' is not defined in _elementCrosswalk dictionary.");
        }

        /// <summary>
        /// Convert an element from an enumeration to a string value
        /// </summary>
        /// <typeparam name="T">Enumeration value type</typeparam>
        /// <param name="segment">Segment name to unmap</param>
        /// <param name="element">Element number to unmap</param>
        /// <param name="value">Value to convert to a string</param>
        /// <returns>String representation of value</returns>
        public static string UnmapValue<T>(string segment, int element, T value)
        {
            return UnmapValue<T>($"{segment}{element.ToString(new string('0', ELEMENT_NUMBER_LENGTH))}", value);
        }

        /// <summary>
        /// Convert an element from an enumeration to a string value
        /// </summary>
        /// <typeparam name="T">Enumeration value type</typeparam>
        /// <param name="location">Location string to unmap (segment name + element number)</param>
        /// <param name="value">Value to convert to a string</param>
        /// <returns>String representation of value</returns>
        public static string UnmapValue<T>(string location, T value)
        {
            if (!_elementCrosswalk.ContainsKey(location)) throw new Exception($"Location '{location}' is not defined in the _elementCrosswalk dictionary.");
            foreach (X12ElementValueMap<dynamic> schema in _elementCrosswalk[location])
                if (schema.MappedValue == value)
                    return schema.OriginalValue;
            throw new Exception($"Map data for location '{location}' and value '{value}' is not defined in _elementCrosswalk dictionary.");
        }
        #endregion
    }

    /// <summary>
    /// Element value map
    /// </summary>
    /// <typeparam name="T">Enumeration type for element conversion</typeparam>
    public class X12ElementValueMap<T>
    {
        #region Constants/Variables
        /// <summary>
        /// Segment name
        /// </summary>
        private string _segment;

        /// <summary>
        /// Element number
        /// </summary>
        private int _element;

        /// <summary>
        /// Unmapped string value
        /// </summary>
        private string _originalValue;

        /// <summary>
        /// Mapped enumeration value
        /// </summary>
        private T _mappedValue;
        #endregion

        #region Properties
        /// <summary>
        /// Segment name
        /// </summary>
        public string Segment { get { return _segment; } }

        /// <summary>
        /// Element number
        /// </summary>
        public int Element { get { return _element; } }

        /// <summary>
        /// Unmapped string value
        /// </summary>
        public string OriginalValue { get { return _originalValue; } }

        /// <summary>
        /// Mapped enumeration value
        /// </summary>
        public T MappedValue { get { return _mappedValue; } }
        #endregion

        #region Constructors
        /// <summary>
        /// Build element value map
        /// </summary>
        /// <param name="segment">Segment name</param>
        /// <param name="element">Element number</param>
        /// <param name="originalValue">Unmapped string value</param>
        /// <param name="mappedValue">Mapped enumeration value</param>
        public X12ElementValueMap(string segment, int element, string originalValue, T mappedValue)
        {
            _segment = segment;
            _element = element;
            _originalValue = originalValue;
            _mappedValue = mappedValue;
        }
        #endregion
    }

    /// <summary>
    /// ISA12 - Interchange X12 format version
    /// </summary>
    public enum ISAVersion
    {
        /// <summary>
        /// ISA version 4010
        /// </summary>
        v4010 = 4010,

        /// <summary>
        /// ISA version 5010
        /// </summary>
        v5010 = 5010
    }

    /// <summary>
    /// ISA15 - Environment for which the interchange is intended
    /// </summary>
    public enum ISAEnvironment
    {
        /// <summary>
        /// Interchange is intended for a production environment
        /// </summary>
        Production,

        /// <summary>
        /// Interchange is intended for a test environment
        /// </summary>
        Test
    }
}

﻿using System;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace FileIO
{
    /// <summary>
    /// Builds the <c> FileIOConfigBuilder </c> object from various configuration sources.
    /// </summary>
    /// <remarks>
    /// <para> This is singleton class </para>
    /// <para> Internally it uses <c> IConfiguration </c> and builds the configuration from the following sources </para>
    ///
    /// * `appSettings.json` (in folder with dam.exe)
    /// * User's `dam.json` Config File (path: `%USER%\DataAccessManager\dam.json`)
    /// * Environment Variables
    /// * Command Line Arguments
    ///
    /// *Note: This is sort by priority, lowest first. Configuration values can be over ridden in higher priority sources*
    /// <para> Reference: https://medium.com/@dmitryzaets/legacy-net-applications-configuration-management-net-framework-4-5-1-68220335d9d8 </para>
    /// <para> Command line arguments are not used by default, see example of how to use them </para>
    /// </remarks>
    /// <example>
    /// <c> DataAccessConfigBuilder </c> is created on demand when accessed for the first time
    /// <code>
    /// <![CDATA[
    /// string cxnStr = FileIOConfigBuilder.Instance.Config.ArchiveRoot;
    /// ]]>
    /// </code>
    /// </example>
    /// ///
    /// <example>
    /// If you want to use the command line arguments, make sure to call <c> UseCliArgs </c> before using the <c> DataAccessConfigBuilder </c>
    /// <code>
    /// <![CDATA[
    /// private static void Main(string[] args)
    /// {
    /// FileIOConfigBuilder.UseCliArgs(args);
    /// string cxnStr = FileIOConfigBuilder.Instance.Config.ArchiveRoot;
    /// // stuff....
    /// }
    /// ]]>
    /// </code>
    /// </example>
    /// <seealso cref="FileIOConfig"/>
    /// <seealso cref="DbExec.Create"/>
    /// <seealso cref="IConfiguration"/>
    /// <seealso cref="IConfigurationBuilder"/>
    public class FileIOConfigBuilder
    {
        private static FileIOConfigBuilder _instance;
        private IConfiguration _config;
        private FileIOConfig _dac;

        /// <summary>
        /// Absolute path to the fileioconfig.json configuration file
        /// </summary>
        /// <value> Gets the path to `%USER%\DataAccessManager\dam.json` </value>
        public static string FileIOConfigPath { get { return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "FileIO", "fileioconfig.json"); } }

        /// <summary>
        /// The singleton instance
        /// </summary>
        /// <value> Gets the private singleton instance </value>
        public static FileIOConfigBuilder Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new FileIOConfigBuilder();
                }
                return _instance;
            }
        }

        /// <summary>
        /// The "built" configuration object
        /// </summary>
        /// <value> Gets the internal `DataAccessConfig` object </value>
        public FileIOConfig Config { get { return _dac; } }

        /// <summary>
        /// 
        /// </summary>
        public static string ConfigEnvironment { get; set; } = "dev";

        /// <summary>
        /// Creates a new instance of this class
        /// </summary>
        /// <param name="environment"></param>
        private FileIOConfigBuilder()
        {
            Console.WriteLine($"File IO Config Path: {FileIOConfigPath}");

            var configBuilder = new ConfigurationBuilder()
               .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
               .AddJsonFile(FileIOConfigPath, optional: true, reloadOnChange: true);
            configBuilder.AddEnvironmentVariables();

            _config = configBuilder.Build();

            _dac = new FileIOConfig(ConfigEnvironment);
            _config.Bind(_dac);
        }
    }
}

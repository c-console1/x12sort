﻿using System.Diagnostics;

namespace FileIO.StaticUtilities
{
    /// <summary>
    /// 
    /// </summary>
    public static class CommandLineTools
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        public static void Run(string command)
        {
            Process process = new Process();
            process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            process.StartInfo.FileName = "cmd.exe";
            process.StartInfo.Arguments = command;
            process.Start();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace FileIO.StaticUtilities
{
    /// <summary>
    /// Attribute to indicate the code analyses that this is checking not null
    /// </summary>
    /// <remarks>
    /// Visual Studio does some static analysis checking for checking a parameter is not null before it is used.
    /// This can get tedious to type out all the time, so helper methods can make this much easier, but they don't get rid of the warnings.
    /// As a work around, a <c>ValidatedNotNullAttribute</c> can be used to tell the analysis that we are really checking the value to not be null.
    /// <para>
    /// Reference: https://esmithy.net/2011/03/15/suppressing-ca1062/
    /// </para>
    /// </remarks>
    /// <seealso cref="ArgumentValidationExtensions"/>
    public sealed class ValidatedNotNullAttribute : Attribute
    {
    }

    /// <summary> Extension Methods to check parameter arguments</summary>
    /// <remarks>
    /// Reference: https://softwareengineering.stackexchange.com/questions/336179/is-there-an-easier-way-to-test-argument-validation-and-field-initialization-in-a
    /// </remarks>
    /// <seealso cref="ValidatedNotNullAttribute"/>
    public static class ArgumentValidationExtensions
    {

        /// <summary> Helper function to throw an error if this argument is null </summary>
        /// <typeparam name="T"> The type of argument you are checking </typeparam>
        /// <param name="item">      The argument variable </param>
        /// <param name="parameter"> The name of the parameter </param>
        /// <param name="caller">    The function that the null value happened in </param>
        /// <returns> returns the item is it is not null, but will throw an exception if the argument is null</returns>
        /// <remarks>
        /// Use the <c>nameof</c> language feature to pass the argument name to <paramref name="parameter"/> without hard coding it. Also uses <c>[CallerMemberName]</c> to infer the what called this function for better debugging.
        /// </remarks>
        /// <exception cref="ArgumentNullException" >Thrown when <paramref name="item"/> is null</exception>
        /// <example>
        /// <code>
        /// <![CDATA[
        /// public static void ExampleFunction(DataTable table) {
        ///     // do the null check in one simple line of code
        ///     table.ThrowIfNull(nameof(table));
        ///
        ///     // do stuff
        /// }
        /// ]]>
        /// </code>
        /// </example>
        /// <seealso cref="ThrowIfNullOrEmpty(string, string, string)"/>
        /// <seealso cref="ValidatedNotNullAttribute"/>
        [SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")]
        public static T ThrowIfNull<T>([ValidatedNotNull] this T item, string parameter, [CallerMemberName] string caller = "") where T : class
        {
            if (item == null)
            {
                throw new ArgumentNullException(parameter, $"Null Argument in '{caller}'.");
            }
            return item;
        }

        /// <summary> Helper function to throw an error if this argument is null or empty </summary>
        /// <param name="item">      The argument variable </param>
        /// <param name="parameter"> The name of the parameter </param>
        /// <param name="caller">    The function that the null value happened in </param>
        /// <returns> returns the item is it is not null, but will throw an exception if the argument is null</returns>
        /// <remarks>
        /// Use the <c>nameof</c> language feature to pass the argument name to <paramref name="parameter"/> without hard coding it. Also uses <c>[CallerMemberName]</c> to infer the what called this function for better debugging.
        /// </remarks>
        /// <exception cref="ArgumentNullException" >Thrown when <paramref name="item"/> is null</exception>
        /// <example>
        /// <code>
        /// <![CDATA[
        /// public static void ExampleFunction(string value) {
        ///     // do the null check in one simple line of code
        ///     table.ThrowIfNullOrEmpty(nameof(value));
        ///
        ///     // do stuff
        /// }
        /// ]]>
        /// </code>
        /// </example>
        /// <seealso cref="ThrowIfNull{T}(T, string, string)"/>
        /// <seealso cref="ValidatedNotNullAttribute"/>
        [SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")]
        public static string ThrowIfNullOrEmpty([ValidatedNotNull] this string item, string parameter, [CallerMemberName] string caller = "")
        {
            if (string.IsNullOrEmpty(item))
            {
                throw new ArgumentNullException(parameter, $"Null/Empty Argument in '{caller}'.");
            }
            return item;
        }
    }

}

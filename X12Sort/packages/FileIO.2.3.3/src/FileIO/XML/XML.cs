﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace FileIO
{
    public class XML
    {
        /// <summary>
        /// Creates a new XML file
        /// </summary>
        /// <param name="objectToWrite">Object to create xml file for.</param>
        /// <param name="filePath">XML file path with name.</param>
        /// <param name="omitXmlDeclaration">Omit XML declaration line.</param>
        /// <param name="encoding">File text encoding. Null will default to UTF8.</param>
        public static void CreateNewFile(object objectToWrite, string filePath, bool omitXmlDeclaration = false, Encoding encoding = null)
        {
            XmlSerializerNamespaces xmlNamespace = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
            XmlWriterSettings xmlSettings = new XmlWriterSettings();
            XmlSerializer xmlSerial = new XmlSerializer(objectToWrite.GetType());

            xmlSettings.Indent = true;
            xmlSettings.OmitXmlDeclaration = omitXmlDeclaration;

            if (encoding == null)
                encoding = Encoding.UTF8;

            StreamWriter streamWriter = new StreamWriter(new FileStream(filePath, FileMode.Create), encoding);
            XmlWriter xmlWriter = XmlWriter.Create(streamWriter, xmlSettings);
            xmlSerial.Serialize(xmlWriter, objectToWrite, xmlNamespace);
            streamWriter.Close();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FileIO.Reporting
{
    /// <summary>
    /// Report section layout used for a table section
    /// </summary>
    public class ReportSectionTableLayout
    {
        #region Constants/Variables
        /// <summary>
        /// List of column widths for table row
        /// </summary>
        private List<int> _columnWidths;

        /// <summary>
        /// List of column string formats for table row (null means no special formatting)
        /// </summary>
        private List<string> _columnFormats;
        #endregion

        #region Properties
        /// <summary>
        /// List of column widths for table row
        /// </summary>
        public List<int> ColumnWidths { get { return _columnWidths; } }

        /// <summary>
        /// List of column string formats for table row (null means no special formatting)
        /// </summary>
        public List<string> ColumnFormats { get { return _columnFormats; } }
        #endregion

        #region Constructors
        /// <summary>
        /// Create ReportSectionTableLayout
        /// </summary>
        /// <param name="columnWidths">List of column widths for table row</param>
        public ReportSectionTableLayout(List<int> columnWidths) : this(columnWidths, columnWidths.Select<int, string>(w => null).ToList()) { }

        /// <summary>
        /// Create ReportSectionTableLayout
        /// </summary>
        /// <param name="columnWidths">List of column widths for table row</param>
        /// <param name="columnFormats">List of column string formats for table row (null means no special formatting)</param>
        public ReportSectionTableLayout(List<int> columnWidths, List<string> columnFormats)
        {
            if (columnWidths.Count != columnFormats.Count) throw new Exception($"Column width count ({columnWidths.Count}) and column format count ({columnFormats.Count}) do not match");
            /* if (sum of column widths + 3 character spacers between each column > report width) then throw exception */
            if (columnWidths.Sum() + (columnWidths.Count - 1) * 3 > FileIOConfigBuilder.Instance.Config.ReportMaxWidth) throw new Exception($"Combined column widths cannot exceed maximum width of {FileIOConfigBuilder.Instance.Config.ReportMaxWidth} characters");
            _columnWidths = columnWidths;
            _columnFormats = columnFormats;
        }
        #endregion
    }
}

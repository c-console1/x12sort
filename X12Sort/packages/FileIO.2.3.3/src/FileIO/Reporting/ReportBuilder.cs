﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
//using System.Runtime.Remoting;
using System.Text;
using Microsoft.CSharp;

namespace FileIO.Reporting
{
    /// <summary>
    /// Report to be output to a fixed width text file
    /// </summary>
    public class ReportBuilder
    {
        #region Constants/Variables
        /// <summary>
        /// Job name (used to name file on Cypress)
        /// </summary>
        public string _jobName;

        /// <summary>
        /// Report title for header
        /// </summary>
        public string _title;
        #endregion

        #region Properties
        /// <summary>
        /// Job name (used to name file on Cypress)
        /// </summary>
        public string JobName { get { return _jobName; } }

        /// <summary>
        /// Report title for header
        /// </summary>
        public string Title { get { return _title; } }

        /// <summary>
        /// List of report sections
        /// </summary>
        public List<ReportSection> Sections { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Create ReportBuilder
        /// </summary>
        /// <param name="title">Report title for header</param>
        public ReportBuilder(string title) : this(title, title) { }

        /// <summary>
        /// Create ReportBuilder
        /// </summary>
        /// <param name="jobName">Job name (used to name file on Cypress)</param>
        /// <param name="title">Report title for header</param>
        public ReportBuilder(string jobName, string title)
        {
            _jobName = jobName;
            _title = title;
            Sections = new List<ReportSection>();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Add a new section to the end of report
        /// </summary>
        /// <param name="section">Predefined report section</param>
        public void AddSection(ReportSection section)
        {
            Sections.Add(section);
        }

        /// <summary>
        /// Add a new footer section to the end of report
        /// </summary>
        public void AddFooterSection()
        {
            AddSection(new ReportSection(ReportSectionType.Footer));
        }

        /// <summary>
        /// Add a new header section to the end of report
        /// </summary>
        /// <param name="title">Report title for header</param>
        public void AddHeaderSection(string title)
        {
            AddSection(new ReportSection(title, true));
        }

        /// <summary>
        /// Add a new data table section to the end of report
        /// </summary>
        /// <param name="table">Data to be displayed in report section</param>
        /// <param name="columnWidths">List of column widths for table row</param>
        public void AddTableSection(DataTable table, List<int> columnWidths)
        {
            AddSection(new ReportSection(table, new ReportSectionTableLayout(columnWidths)));
        }

        /// <summary>
        /// Add a new data table section to the end of report
        /// </summary>
        /// <param name="table">Data to be displayed in report section</param>
        /// <param name="columnWidths">List of column widths for table row</param>
        /// <param name="columnFormats">List of column string formats for table row (null means no special formatting)</param>
        public void AddTableSection(DataTable table, List<int> columnWidths, List<string> columnFormats)
        {
            AddSection(new ReportSection(table, new ReportSectionTableLayout(columnWidths, columnFormats)));
        }

        /// <summary>
        /// Add a new data table section to the end of report
        /// </summary>
        /// <param name="table">Data to be displayed in report section</param>
        /// <param name="layout">Layout for data table</param>
        public void AddTableSection(DataTable table, ReportSectionTableLayout layout)
        {
            AddSection(new ReportSection(table, layout));
        }

        /// <summary>
        /// Add a new list section to the end of report
        /// </summary>
        /// <param name="fields">Field names</param>
        /// <param name="values">Field values</param>
        public void AddListSection(List<string> fields, List<object> values)
        {
            AddSection(new ReportSection(fields, values));
        }

        /// <summary>
        /// Add a new list section to the end of report
        /// </summary>
        /// <param name="list">List to display in new report section</param>
        public void AddListSection(IEnumerable<KeyValuePair<string, object>> list)
        {
            AddSection(new ReportSection(list));
        }

        /// <summary>
        /// Add a new text section to the end of the report
        /// </summary>
        /// <param name="text">Text of new report section</param>
        /// <param name="center">Determine whether text section should be centered</param>
        public void AddTextSection(string text, bool center = false)
        {
            AddSection(new ReportSection(text, isHeader: false, center: center));
        }

        /// <summary>
        /// Add a new line section to the end of the report
        /// </summary>
        public void AddLineSection()
        {
            AddLineSection(FileIOConfigBuilder.Instance.Config.ReportDefaultLineCharacter);
        }

        /// <summary>
        /// Add a new line section to the end of the report
        /// </summary>
        /// <param name="lineCharacter">Character to use for line</param>
        public void AddLineSection(char lineCharacter)
        {
            AddSection(new ReportSection(lineCharacter));
        }

        /// <summary>
        /// Writes the report out to the specified path
        /// </summary>
        /// <param name="path">Report output path</param>
        /// <param name="ignoreErrors">Determine whether program should continue when an error occurs</param>
        public void WriteToFile(string path, bool ignoreErrors = false)
        {
            try
            {
                StreamWriter writer = new StreamWriter(path);
                writer.Write(ToString());
                writer.Close();
            }
            catch (Exception ex) { if (!ignoreErrors) throw ex; }
        }

        /// <summary>
        /// Output report as a string
        /// </summary>
        /// <returns>String of report</returns>
        public override string ToString()
        {
            // Get current process information
            Process proc = Process.GetCurrentProcess();

            // Add header section if it does not exist
            if (Sections.Count == 0) Sections.Insert(0, new ReportSection(_title, true));
            else if (Sections[0].Type != ReportSectionType.Header) Sections.Insert(0, new ReportSection(_title, true));

            // Add footer section if it does not exist
            if (Sections[Sections.Count - 1].Type != ReportSectionType.Footer) Sections.Add(new ReportSection(ReportSectionType.Footer));

            // Print out each section
            StringBuilder str = new StringBuilder();
            foreach (ReportSection section in Sections)
            {
                str.AppendLine(
                    section.Type == ReportSectionType.Footer
                    ? string.Format(section.ToString(), DateTime.Now, proc.TotalProcessorTime.TotalMinutes)
                    : section.ToString());
                str.AppendLine();
            }
            return str.ToString();
        }
        #endregion

        #region Cypress Methods
        /// <summary>
        /// Get folder path for Cypress reporting environment
        /// </summary>
        /// <param name="env">Cypress reporting environment</param>
        /// <returns>Folder path for Cypress reporting environment</returns>
        public string GetCypressFolder(CypressEnvironment env)
        {
            switch (env)
            {
                case CypressEnvironment.DEV6: return FileIOConfigBuilder.Instance.Config.CypressDEV6SubFolder;
                case CypressEnvironment.PFIG: return FileIOConfigBuilder.Instance.Config.CypressPFIGSubFolder;
                case CypressEnvironment.PROD: return FileIOConfigBuilder.Instance.Config.CypressPRODSubFolder;
                case CypressEnvironment.TESTA: return FileIOConfigBuilder.Instance.Config.CypressTESTASubFolder;
                case CypressEnvironment.TESTB: return FileIOConfigBuilder.Instance.Config.CypressTESTBSubFolder;
                case CypressEnvironment.TESTC: return FileIOConfigBuilder.Instance.Config.CypressTESTCSubFolder;
                case CypressEnvironment.TESTD: return FileIOConfigBuilder.Instance.Config.CypressTESTDSubFolder;
                case CypressEnvironment.TESTE: return FileIOConfigBuilder.Instance.Config.CypressTESTESubFolder;
                case CypressEnvironment.TFIG: return FileIOConfigBuilder.Instance.Config.CypressTFIGSubFolder;
                default: throw new Exception($"Cypress folder for environment {env} was not defined");
            }
        }

        /// <summary>
        /// Send report to a Cypress environment based on the billing connection from the data access object passed from the calling program
        /// </summary>
        /// <param name="dataMount">Data mount object from calling program containing billng connection</param>
        public void SendToCypress(dynamic dataMount)
        {
            // Verify data mount exists
            if (dataMount == null)
                throw new Exception("Data mount object must be defined");

            // Verify data mount type is correct
            if (((object)dataMount).GetType().FullName != "DataLayer.DataAccess")
                throw new Exception("Data mount passed is of the wrong type");

            // Find the billing database connection and determine the environment
            foreach (var db in dataMount.Configuration.Current.Databases)
                if (db.Name == FileIOConfigBuilder.Instance.Config.BillingDatabaseName)
                {
                    string cn = (string)db.Connection.Name.ToUpper();
                    if (cn == FileIOConfigBuilder.Instance.Config.BillingPFIGConnectionName) SendToCypress(CypressEnvironment.PFIG);
                    else if(cn == FileIOConfigBuilder.Instance.Config.BillingPRODConnectionName) SendToCypress(CypressEnvironment.PROD);
                    else if (cn == FileIOConfigBuilder.Instance.Config.BillingTFIGConnectionName) SendToCypress(CypressEnvironment.TFIG); 
                    else if (cn == FileIOConfigBuilder.Instance.Config.BillingTESTAConnectionName) SendToCypress(CypressEnvironment.TESTA);
                    else if (cn == FileIOConfigBuilder.Instance.Config.BillingTESTBConnectionName) SendToCypress(CypressEnvironment.TESTB);
                    else if (cn == FileIOConfigBuilder.Instance.Config.BillingTESTCConnectionName) SendToCypress(CypressEnvironment.TESTC);
                    else if (cn == FileIOConfigBuilder.Instance.Config.BillingTESTDConnectionName) SendToCypress(CypressEnvironment.TESTD);
                    else if (cn == FileIOConfigBuilder.Instance.Config.BillingTESTEConnectionName) SendToCypress(CypressEnvironment.TESTE);
                    else SendToCypress(CypressEnvironment.DEV6);
                    return;
                }

            // Send to DEV6 if no billing connection is found
            SendToCypress(CypressEnvironment.DEV6);
        }

        /// <summary>
        /// Send report to a specified Cypress environment
        /// </summary>
        /// <param name="env">Cypress reporting environment</param>
        public void SendToCypress(CypressEnvironment env)
        {
            SendToCypress(env, $"{_jobName}_{DateTime.Now.ToString("yyyyMMdd_HHmmss_fff")}");
        }

        /// <summary>
        /// Send report to a specified Cypress environment and define the file name
        /// </summary>
        /// <param name="env">Cypress reporting environment</param>
        /// <param name="fileName">File name for Cypress report (if no extension, one will be provided)</param>
        public void SendToCypress(CypressEnvironment env, string fileName)
        {
            fileName = fileName.Contains(".") ? fileName : $"{fileName}.{FileIOConfigBuilder.Instance.Config.CypressFileExtension}";
            WriteToFile(Path.Combine(FileIOConfigBuilder.Instance.Config.CypressBaseFolder, GetCypressFolder(env), fileName));
        }
        #endregion
    }
}

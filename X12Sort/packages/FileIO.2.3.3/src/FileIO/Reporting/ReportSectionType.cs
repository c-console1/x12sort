﻿namespace FileIO.Reporting
{
    /// <summary>
    /// Type of report section
    /// </summary>
    public enum ReportSectionType
    {
        /// <summary>
        /// To be displayed at the bottom of the report, shows information about when/how the report was run
        /// </summary>
        Footer,

        /// <summary>
        /// To be displayed at the top of the report, shows title
        /// </summary>
        Header,

        /// <summary>
        /// Shows the contents of a data table
        /// </summary>
        Table,

        /// <summary>
        /// Shows the contents of a list
        /// </summary>
        List,

        /// <summary>
        /// Shows a simple text value
        /// </summary>
        Text,

        /// <summary>
        /// Shows a line
        /// </summary>
        Line
    }
}

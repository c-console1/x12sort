﻿namespace FileIO.Reporting
{
    /// <summary>
    /// Cypress environments (match up to Amisys environments)
    /// </summary>
    public enum CypressEnvironment
    {
        /// <summary>
        /// Amisys DEV6 environment
        /// </summary>
        DEV6,

        /// <summary>
        /// Amisys PFIG environment
        /// </summary>
        PFIG,

        /// <summary>
        /// Amisys PROD environment
        /// </summary>
        PROD,

        /// <summary>
        /// Amisys TESTA environment
        /// </summary>
        TESTA,

        /// <summary>
        /// Amisys TESTB environment
        /// </summary>
        TESTB,

        /// <summary>
        /// Amisys TESTC environment
        /// </summary>
        TESTC,

        /// <summary>
        /// Amisys TESTD environment
        /// </summary>
        TESTD,

        /// <summary>
        /// Amisys TESTE environment
        /// </summary>
        TESTE,

        /// <summary>
        /// Amisys TFIG environment
        /// </summary>
        TFIG
    }
}

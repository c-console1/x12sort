﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Text;

namespace FileIO.Reporting
{
    /// <summary>
    /// Section of the report containing some type of data
    /// </summary>
    public class ReportSection
    {
        #region Constants/Variables
        /// <summary>
        /// Type of report section
        /// </summary>
        private ReportSectionType _type;

        /// <summary>
        /// Report data object containing a list of lines
        /// </summary>
        private List<string> _data;
        #endregion

        #region Properties
        /// <summary>
        /// Type of report section
        /// </summary>
        public ReportSectionType Type { get { return _type; } }
        #endregion

        #region Constructors
        /// <summary>
        /// Create ReportSection (line only)
        /// </summary>
        /// <param name="lineCharacter">Character to use for line (ignored if isFooter is true)</param>
        public ReportSection() : this (FileIOConfigBuilder.Instance.Config.ReportDefaultLineCharacter) { }

        /// <summary>
        /// Create ReportSection (line only)
        /// </summary>
        /// <param name="lineCharacter">Character to use for line (ignored if isFooter is true)</param>
        public ReportSection(char lineCharacter)
        {
            _type = ReportSectionType.Line;
            _data = BuildLineOutput(lineCharacter);
        }

        /// <summary>
        /// Create ReportSection (footer only)
        /// </summary>
        /// <param name="isFooter">Determines whether section is footer</param>
        public ReportSection(ReportSectionType type)
        {
            if (type != ReportSectionType.Footer)
                throw new Exception("Only use this constructor for footers");
            _type = ReportSectionType.Footer;
            _data = BuildFooterOutput();
        }

        /// <summary>
        /// Create ReportSection
        /// </summary>
        /// <param name="text">Text ro apply to report section</param>
        /// <param name="isHeader">Determine whether text section is header</param>
        /// <param name="center">Determine whether text section should be centered (header is always centered)</param>
        public ReportSection(string text, bool isHeader = false, bool center = false)
        {
            _type = isHeader ? ReportSectionType.Header : ReportSectionType.Text;
            _data = isHeader ? BuildHeaderOutput(text) : BuildTextOutput(text, center);
        }

        /// <summary>
        /// Create ReportSection
        /// </summary>
        /// <param name="table">Data to be displayed in report section</param>
        /// <param name="layout">Layout for data table</param>
        public ReportSection(DataTable table, ReportSectionTableLayout layout)
        {
            _type = ReportSectionType.Table;
            _data = BuildTableOutput(table, layout);
        }

        /// <summary>
        /// Create ReportSection
        /// </summary>
        /// <param name="fields">Field names</param>
        /// <param name="values">Field values</param>
        public ReportSection(List<string> fields, List<object> values)
        {
            if (fields.Count != values.Count) throw new Exception($"Field count ({fields.Count}) and value count ({values.Count}) do not match");
            _type = ReportSectionType.List;
            _data = BuildListOutput(fields.Zip(values, (f, v) => new KeyValuePair<string, object>(f, v)));
        }

        /// <summary>
        /// Create ReportSection
        /// </summary>
        /// <param name="list">List to be displayed in report section</param>
        public ReportSection(IEnumerable<KeyValuePair<string, object>> list)
        {
            _type = ReportSectionType.List;
            _data = BuildListOutput(list);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Convert report section into single string
        /// </summary>
        /// <returns>String representing _data list</returns>
        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            foreach (string line in _data)
                str.AppendLine(line);
            return str.ToString();
        }

        /// <summary>
        /// Build report section lines for footer
        /// </summary>
        /// <returns>Report section output lines</returns>
        private List<string> BuildFooterOutput()
        {
            Process proc = Process.GetCurrentProcess();
            string procName = proc.ProcessName.Contains(".") ? proc.ProcessName.Substring(0, proc.ProcessName.LastIndexOf(".")) : proc.ProcessName;
            List<string> output = new List<string>();
            output.Add(CreateLine('*'));
            output.Add("");
            output.Add($"Start Time:   {proc.StartTime.ToString("yyyy-MM-dd HH:mm:ss.fff")}");
            output.Add($"End Time:     {{0:yyyy-MM-dd HH:mm:ss.fff}}");
            output.Add($"Runtime:      {{1:0.00}} min");
            output.Add("");
            output.Add($"Server Info:  {Environment.MachineName} [ {Environment.OSVersion} {(Environment.Is64BitOperatingSystem ? "64" : "32")}-bit ]");
            output.Add($"Process Info: {procName} {(Environment.Is64BitProcess ? "64" : "32")}-bit [ {proc.Id} ]");
            output.Add($"User Info:    {UserPrincipal.Current.DisplayName} [ {Environment.UserDomainName}\\{Environment.UserName} ]");
            output.Add("");
            output.Add(CreateLine('*'));
            return output;
        }

        /// <summary>
        /// Build report section lines for header
        /// </summary>
        /// <param name="title">Report title for header</param>
        /// <returns>Report section output lines</returns>
        private List<string> BuildHeaderOutput(string title)
        {
            List<string> output = new List<string>();
            output.Add(CreateLine('*'));
            output.Add("");
            output.Add(CenterText(title).ToUpper());
            output.Add("");
            output.Add(CreateLine('*'));
            return output;
        }

        /// <summary>
        /// Build report section lines from list
        /// </summary>
        /// <param name="list">List to be displayed in report section</param>
        /// <returns>Report section output lines</returns>
        private List<string> BuildListOutput(IEnumerable<KeyValuePair<string, object>> list)
        {
            int fieldNameLength = list.Max(i => i.Key.Length) + 1;
            return list.Select(i => $"{(i.Key + ":").PadRight(fieldNameLength)} {i.Value}").ToList();
        }

        /// <summary>
        /// Build report section lines from table
        /// </summary>
        /// <param name="table">Data to be displayed in report section</param>
        /// <param name="layout">Layout for data table</param>
        /// <returns>Report section output lines</returns>
        private List<string> BuildTableOutput(DataTable table, ReportSectionTableLayout layout)
        {
            List<string> output = new List<string>();
            output.Add(BuildTableLine(table.Columns.Cast<DataColumn>().Select(c => (object)c.ColumnName).ToList(), layout, true));
            output.Add(CreateLine());
            foreach (DataRow row in table.Rows)
                output.Add(BuildTableLine(row.ItemArray.ToList(), layout));
            return output;
        }

        /// <summary>
        /// Pad and format a row from a data table
        /// </summary>
        /// <param name="items">Fields to format</param>
        /// <param name="layout">Layout for data table</param>
        /// <param name="isHeader">Determines whether table line is a header</param>
        /// <returns>Padded and formatted table line</returns>
        private string BuildTableLine(List<object> items, ReportSectionTableLayout layout, bool isHeader = false)
        {
            StringBuilder str = new StringBuilder();
            for (int i = 0; i < items.Count; i++)
            {
                if (i > 0) str.Append(" | ");
                int width = layout.ColumnWidths[i];
                if (layout.ColumnFormats[i] == null || isHeader)
                {
                    string item = items[i].ToString().PadRight(width).Substring(0, width);
                    str.Append(isHeader ? item.ToUpper() : item);
                }
                else
                {
                    string format = $"{{0:{layout.ColumnFormats[i]}}}";
                    str.Append(string.Format(format.PadLeft(width), items[i]).Substring(0, width));
                }
            }
            return str.ToString();
        }

        /// <summary>
        /// Build report section lines from text
        /// </summary>
        /// <param name="text">Text ro apply to report section</param>
        /// <param name="center">Determine whether text should be centered</param>
        /// <returns>Word wrapped text in list form</returns>
        private List<string> BuildTextOutput(string text, bool center)
        {
            if (center) return WordWrap(text).Split(new string[] { Environment.NewLine }, StringSplitOptions.None).Select(i => CenterText(i)).ToList();
            else return WordWrap(text).Split(new string[] { Environment.NewLine }, StringSplitOptions.None).ToList();
        }

        /// <summary>
        /// Build report section lines from text
        /// </summary>
        /// <returns>Word wrapped text in list form</returns>
        private List<string> BuildLineOutput()
        {
            return BuildLineOutput(FileIOConfigBuilder.Instance.Config.ReportDefaultLineCharacter);
        }

        /// <summary>
        /// Build report section lines from text
        /// </summary>
        /// <param name="lineCharacter">Character to use for line</param>
        /// <returns>Word wrapped text in list form</returns>
        private List<string> BuildLineOutput(char lineCharacter)
        {
            return new List<string>() { CreateLine(lineCharacter) };
        }

        /// <summary>
        /// Create a line to display in the report
        /// </summary>
        /// <returns>Horizontal line</returns>
        private string CreateLine()
        {
            return CreateLine(FileIOConfigBuilder.Instance.Config.ReportDefaultLineCharacter);
        }

        /// <summary>
        /// Create a line to display in the report
        /// </summary>
        /// <param name="lineCharacter">Character to use for line</param>
        /// <returns>Horizontal line</returns>
        private string CreateLine(char lineCharacter)
        {
            return new string(lineCharacter, FileIOConfigBuilder.Instance.Config.ReportMaxWidth);
        }

        /// <summary>
        /// Center text in a fixed width string
        /// </summary>
        /// <param name="text">Text to center</param>
        /// <param name="paddingCharacter">Character to use when padding the centered text</param>
        /// <returns>Centered and padded string</returns>
        private string CenterText(string text, char paddingCharacter = ' ')
        {
            return CenterText(text, FileIOConfigBuilder.Instance.Config.ReportMaxWidth, paddingCharacter);
        }

        /// <summary>
        /// Center text in a fixed width string
        /// </summary>
        /// <param name="text">Text to center</param>
        /// <param name="width">Total width of string</param>
        /// <param name="paddingCharacter">Character to use when padding the centered text</param>
        /// <returns>Centered and padded string</returns>
        private string CenterText(string text, int width, char paddingCharacter = ' ')
        {
            int padding = (int)Math.Floor((width - text.Length) / 2.0);
            return (new string(paddingCharacter, padding) + text).PadRight(width, paddingCharacter);
        }

        /// <summary>
        /// Wrap a text string without breaking within a word
        /// </summary>
        /// <param name="text">Text to wrap</param>
        /// <returns>Wrapped text</returns>
        private static string WordWrap(string text)
        {
            return WordWrap(text, FileIOConfigBuilder.Instance.Config.ReportMaxWidth);
        }

        /// <summary>
        /// Wrap a text string without breaking within a word
        /// </summary>
        /// <param name="text">Text to wrap</param>
        /// <param name="width">Width of single line</param>
        /// <returns>Wrapped text</returns>
        /// <see cref="https://stackoverflow.com/a/3961365"/>
        private static string WordWrap(string text, int width)
        {
            int pos, next;
            StringBuilder sb = new StringBuilder();

            // Lucidity check
            if (width < 1) return text;

            // Parse each line of text
            for (pos = 0; pos < text.Length; pos = next)
            {
                // Find end of line
                int eol = text.IndexOf(Environment.NewLine, pos);

                if (eol == -1)
                    next = eol = text.Length;
                else
                    next = eol + Environment.NewLine.Length;

                // Copy this line of text, breaking into smaller lines as needed
                if (eol > pos)
                {
                    do
                    {
                        int len = eol - pos;

                        if (len > width)
                            len = BreakLine(text, pos, width);

                        sb.Append(text, pos, len);
                        sb.Append(Environment.NewLine);

                        // Trim whitespace following break
                        pos += len;

                        while (pos < eol && char.IsWhiteSpace(text[pos]))
                            pos++;

                    } while (eol > pos);
                }
                else sb.Append(Environment.NewLine); // Empty line
            }

            return sb.ToString();
        }

        /// <summary>
        /// Locates position to break the given line so as to avoid
        /// breaking words.
        /// </summary>
        /// <param name="text">String that contains line of text</param>
        /// <param name="pos">Index where line of text starts</param>
        /// <param name="max">Maximum line length</param>
        /// <returns>The modified line length</returns>
        /// <see cref="https://stackoverflow.com/a/3961365"/>
        public static int BreakLine(string text, int pos, int max)
        {
            // Find last whitespace in line
            int i = max - 1;
            while (i >= 0 && !Char.IsWhiteSpace(text[pos + i]))
                i--;
            if (i < 0)
                return max; // No whitespace found; break at maximum length
                            // Find start of whitespace
            while (i >= 0 && Char.IsWhiteSpace(text[pos + i]))
                i--;
            // Return length of text before whitespace
            return i + 1;
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileIO
{
    /// <summary>
    /// Exception when duplicated address is specified in an Excel style collection
    /// </summary>
    [Serializable]
    class DuplicateExcelAddressException : ArgumentException
    {
        /// <summary>
        /// Exception when duplicated address is specified in an Excel style collection
        /// </summary>
        /// <param name="address">Duplicated address</param>
        public DuplicateExcelAddressException(string address)
            :base ($"Cannot add duplicate Excel address in strict mode: {address}") { }
    }
}

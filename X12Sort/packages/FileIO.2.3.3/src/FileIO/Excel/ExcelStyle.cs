﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace FileIO
{
    /// <summary>
    /// Excel style object
    /// </summary>
    public class ExcelStyle
    {
        #region Properties
        /// <summary>
        /// Type of Excel style
        /// </summary>
        public ExcelStyleType Type { get; private set; }

        /// <summary>
        /// Style parameters
        /// </summary>
        public List<object> Parameters { get; private set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Create new Excel style object
        /// </summary>
        /// <param name="type">Type of Excel style</param>
        /// <param name="parameters">Style parameters</param>
        public ExcelStyle(ExcelStyleType type, params object[] parameters) : this(type, parameters.ToList()) { }

        /// <summary>
        /// Create new Excel style object
        /// </summary>
        /// <param name="type">Type of Excel style</param>
        /// <param name="parameters">Style parameters</param>
        public ExcelStyle(ExcelStyleType type, List<object> parameters)
        {
            Type = type;
            Parameters = new List<object>();
            AddParameters(parameters);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Add parameters to Excel style
        /// </summary>
        /// <param name="parameter">Parameters to add</param>
        public void AddParameters(params object[] parameters)
        {
            AddParameters(parameters.ToList());
        }

        /// <summary>
        /// Add parameters to Excel style
        /// </summary>
        /// <param name="parameters">Parameters to add</param>
        public void AddParameters(List<object> parameters)
        {
            Parameters.AddRange(parameters);
            if (!AreParametersValid(Parameters))
                throw new ExcelStyleParametersInvalidException(Type);
        }

        /// <summary>
        /// Validate parameters based on Excel style
        /// </summary>
        /// <param name="parameters">Parameters to evaluate</param>
        /// <returns>Whether parameters are valid or not</returns>
        public bool AreParametersValid(List<object> parameters)
        {
            Color dummyColor;
            float dummyFloat;

            switch (Type)
            {
                #region Validate alignment
                case ExcelStyleType.Alignment:
                    if (parameters == null) return false;
                    if (parameters.Count != 1) return false;
                    if (parameters[0].ToString().ToLower() != "center"
                        && parameters[0].ToString().ToLower() != "justify"
                        && parameters[0].ToString().ToLower() != "left"
                        && parameters[0].ToString().ToLower() != "right")
                        return false;
                    break;
                #endregion
                #region Validate background color
                case ExcelStyleType.BackgroundColor:
                    if (parameters == null) return false;
                    if (parameters.Count != 1) return false;
                    if (!TryGetColorParameter(out dummyColor)) return false;
                    break;
                #endregion
                #region Validate bold
                case ExcelStyleType.Bold:
                    if (parameters != null)
                        if (parameters.Count > 0)
                            return false;
                    break;
                #endregion
                #region Validate column width
                case ExcelStyleType.ColumnWidth:
                    if (parameters == null) return false;
                    if (parameters.Count != 1) return false;
                    if (!float.TryParse(parameters[0].ToString(), out dummyFloat)) return false;
                    break;
                #endregion
                #region Validate double border
                case ExcelStyleType.DoubleBorder:
                    if (parameters == null) return false;
                    if (parameters.Count < 1) return false;
                    if (parameters.Count > 4) return false;
                    foreach (string parameter in parameters)
                        if (parameter.ToLower() != "bottom"
                            && parameter.ToLower() != "left"
                            && parameter.ToLower() != "right"
                            && parameter.ToLower() != "top")
                            return false;
                    break;
                #endregion
                #region Validate font color
                case ExcelStyleType.FontColor:
                    if (parameters == null) return false;
                    if (parameters.Count != 1) return false;
                    if (!TryGetColorParameter(out dummyColor)) return false;
                    break;
                #endregion
                #region Validate font family
                case ExcelStyleType.FontFamily:
                    if (parameters == null) return false;
                    if (parameters.Count != 1) return false;
                    break;
                #endregion
                #region Validate font size
                case ExcelStyleType.FontSize:
                    if (parameters == null) return false;
                    if (parameters.Count != 1) return false;
                    if (!float.TryParse(parameters[0].ToString(), out dummyFloat)) return false;
                    break;
                #endregion
                #region Validate format date
                case ExcelStyleType.FormatDate:
                    if (parameters == null) return false;
                    if (parameters.Count != 1) return false;
                    break;
                #endregion
                #region Validate format number
                case ExcelStyleType.FormatNumber:
                    if (parameters != null)
                        if (parameters.Count != 1)
                            return false;
                    break;
                #endregion
                #region Validate format text
                case ExcelStyleType.FormatText:
                    if (parameters != null) return false;
                    break;
                #endregion
                #region Validate italic
                case ExcelStyleType.Italic:
                    if (parameters != null)
                        if (parameters.Count > 0)
                            return false;
                    break;
                #endregion
                #region Validate merge
                case ExcelStyleType.Merge:
                    if (parameters != null)
                        if (parameters.Count > 0)
                            return false;
                    break;
                #endregion
                #region Validate strikethrough
                case ExcelStyleType.StrikeThrough:
                    if (parameters != null)
                        if (parameters.Count > 0)
                            return false;
                    break;
                #endregion
                #region Validate thick border
                case ExcelStyleType.ThickBorder:
                    if (parameters == null) return false;
                    if (parameters.Count < 1) return false;
                    if (parameters.Count > 4) return false;
                    foreach (string parameter in parameters)
                        if (parameter.ToLower() != "bottom"
                            && parameter.ToLower() != "left"
                            && parameter.ToLower() != "right"
                            && parameter.ToLower() != "top")
                            return false;
                    break;
                #endregion
                #region Validate thin border
                case ExcelStyleType.ThinBorder:
                    if (parameters == null) return false;
                    if (parameters.Count < 1 ) return false;
                    if (parameters.Count > 4) return false;
                    foreach (string parameter in parameters)
                        if (parameter.ToLower() != "bottom"
                            && parameter.ToLower() != "left"
                            && parameter.ToLower() != "right"
                            && parameter.ToLower() != "top")
                            return false;
                    break;
                #endregion
                #region Validate underline
                case ExcelStyleType.Underline:
                    if (parameters != null)
                        if (parameters.Count > 0)
                            return false;
                    break;
                #endregion
                #region Validate vertical alignment
                case ExcelStyleType.VerticalAlignment:
                    if (parameters == null) return false;
                    if (parameters.Count != 1) return false;
                    if (parameters[0].ToString().ToLower() != "bottom"
                        && parameters[0].ToString().ToLower() != "center"
                        && parameters[0].ToString().ToLower() != "justify"
                        && parameters[0].ToString().ToLower() != "top")
                        return false;
                    break;
                #endregion
                default:
                    throw new Exception($"Style type not supported: {Type}");
            }
            return true;
        }

        /// <summary>
        /// Extract Color from internal parameters
        /// </summary>
        /// <returns>Converted Color object</returns>
        public Color GetColorParameter()
        {
            return GetColorParameter(Parameters);
        }

        /// <summary>
        /// Try to extract Color from internal parameters
        /// </summary>
        /// <param name="color">Output color object</param>
        /// <returns>Whether conversion worked</returns>
        public bool TryGetColorParameter(out Color color)
        {
            return TryGetColorParameter(Parameters, out color);
        }

        /// <summary>
        /// Extract Color from parameters
        /// </summary>
        /// <param name="parameters">Parameters to convert to Color</param>
        /// <returns>Converted Color object</returns>
        public static Color GetColorParameter(List<object> parameters)
        {
            // 3 parameters means that red, green, and blue values were passed individually
            if (parameters.Count == 3)
                return Color.FromArgb((int)parameters[0], (int)parameters[1], (int)parameters[2]);

            // If the first parameter starts with #, then it is a HEX color value
            else if (parameters[0].ToString().StartsWith("#"))
                return ColorTranslator.FromHtml(parameters[0].ToString());

            // Everything else is referenced by actual color name (not case sensitive)
            else
                return Color.FromName(parameters[0].ToString());
        }

        /// <summary>
        /// Try to extract Color from parameters
        /// </summary>
        /// <param name="parameters">Parameters to convert to Color</param>
        /// <param name="color">Output color object</param>
        /// <returns>Whether conversion worked</returns>
        public static bool TryGetColorParameter(List<object> parameters, out Color color)
        {
            try
            {
                color = GetColorParameter(parameters);
                return true;
            }
            catch(Exception)
            {
                return false;
            }
        }
        #endregion
    }
}

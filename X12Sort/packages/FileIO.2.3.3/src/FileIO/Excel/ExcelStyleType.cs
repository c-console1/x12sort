﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileIO
{
    /// <summary>
    /// Type of Excel style
    /// </summary>
    public enum ExcelStyleType
    {
        /// <summary>
        /// Horizontal alignment of a cell range.  Valid values are center, justify, left, and right.  Only one can be used.
        /// </summary>
        Alignment,

        /// <summary>
        /// Background color of a cell range.  Valid parameter formats are RGB values (as 3 parameters), color name (as single string), and hex color value (as sinle string, must begin with #).
        /// </summary>
        BackgroundColor,

        /// <summary>
        /// Add bold property to a cell range.  No parameters are needed.
        /// </summary>
        Bold,

        /// <summary>
        /// Column width of a cell range.  One parameter must be passed as a float value.
        /// </summary>
        ColumnWidth,

        /// <summary>
        /// Assign a double border to a cell range.  Valid values are bottom, left, right, and top.  Up to 4 values can be used.
        /// </summary>
        DoubleBorder,

        /// <summary>
        /// Font color of a cell range.  Valid parameter formats are RGB values (as 3 parameters), color name (as single string), and hex color value (as sinle string, must begin with #).
        /// </summary>
        FontColor,

        /// <summary>
        /// Font family of a cell range.  One parameter must be passed as the name of the font family to be used.
        /// </summary>
        FontFamily,

        /// <summary>
        /// Font size of a cell range.  One parameter must be passed as a float value.
        /// </summary>
        FontSize,

        /// <summary>
        /// Format a cell range as a date/time.  One parameter is needed to define the date format.
        /// </summary>
        FormatDate,

        /// <summary>
        /// Format a cell range as a number.  No parameters are needed, but a number format can be defined.  Default is 0.00
        /// </summary>
        FormatNumber,

        /// <summary>
        /// Format a cell range as text.  No parameters are needed.
        /// </summary>
        FormatText,

        /// <summary>
        /// Add italic property to a cell range.  No parameters are needed.
        /// </summary>
        Italic,

        /// <summary>
        /// Merges a range of cells.  No parameters are needed.
        /// </summary>
        Merge,

        /// <summary>
        /// Row height of a cell range.  One parameter must be passed as a float value.
        /// </summary>
        RowHeight,

        /// <summary>
        /// Add strikethrough property to a cell range.  No parameters are needed.
        /// </summary>
        StrikeThrough,

        /// <summary>
        /// Assign a thick border to a cell range.  Valid values are bottom, left, right, and top.  Up to 4 values can be used.
        /// </summary>
        ThickBorder,

        /// <summary>
        /// Assign a thin border to a cell range.  Valid values are bottom, left, right, and top.  Up to 4 values can be used.
        /// </summary>
        ThinBorder,

        /// <summary>
        /// Add underline property to a cell range.  No parameters are needed.
        /// </summary>
        Underline,

        /// <summary>
        /// Vertical alignment of a cell range. Valid values are bottom, center, justify, and top.  Only one value can be used.
        /// </summary>
        VerticalAlignment
    }
}

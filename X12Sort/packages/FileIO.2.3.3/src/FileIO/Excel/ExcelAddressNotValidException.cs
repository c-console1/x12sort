﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileIO
{
    /// <summary>
    /// Exception for when an Excel address is not valid
    /// </summary>
    [Serializable]
    class ExcelAddressNotValidException : ArgumentException
    {
        /// <summary>
        /// Exception for when an Excel address is not valid
        /// </summary>
        /// <param name="address">Invalid Excel address</param>
        public ExcelAddressNotValidException(string address)
            :base ($"Specified Excel address is invalid: {address}") { }
    }
}

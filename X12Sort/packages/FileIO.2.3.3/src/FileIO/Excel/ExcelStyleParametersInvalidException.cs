﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileIO
{
    /// <summary>
    /// Exception when a parameter for an Excel style is invalid
    /// </summary>
    [Serializable]
    class ExcelStyleParametersInvalidException : ArgumentException
    {
        /// <summary>
        /// Exception when a parameter for an Excel style is invalid
        /// </summary>
        /// <param name="type">Excel style type</param>
        /// <param name="parameter">Invalid parameter</param>
        public ExcelStyleParametersInvalidException(ExcelStyleType type, object parameter = null)
            :base ($"The Excel style parameter is not valid for type {type}: {parameter ?? ""}") { }
    }
}

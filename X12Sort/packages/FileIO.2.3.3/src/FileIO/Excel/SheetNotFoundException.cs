﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileIO
{
    /// <summary>
    /// Exception when the sheet name is not found in a given Excel workbook
    /// </summary>
    [Serializable]
    class SheetNotFoundException : KeyNotFoundException
    {
        /// <summary>
        /// Exception when the sheet name is not found in a given Excel workbook
        /// </summary>
        /// <param name="sheetName">Invalid sheet name</param>
        public SheetNotFoundException(string sheetName)
            :base ($"The specified shee does not exist: {sheetName}") { }
    }
}

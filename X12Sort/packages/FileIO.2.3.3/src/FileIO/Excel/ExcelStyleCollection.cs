﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using OfficeOpenXml;

namespace FileIO
{
    /// <summary>
    /// Collection of styles to apply to an Excel workbook
    /// </summary>
    public class ExcelStyleCollection
    {
        #region Constants/Variables
        /// <summary>
        /// Default value for whether duplicate addresses in the style collection should generate an error
        /// </summary>
        private const bool DEFAULT_STRICT_MODE = false;

        /// <summary>
        /// Collection of lists of Excel styles keyed by address string
        /// </summary>
        private Dictionary<string, List<ExcelStyle>> _stylesPerRange;
        #endregion

        #region Properties
        /// <summary>
        /// Get list of Excel styles being assigned to a given address string
        /// </summary>
        /// <param name="address">Excel address or range using colon format</param>
        /// <returns>Lists of Excel styles</returns>
        public List<ExcelStyle> this[string address]
        {
            get { return _stylesPerRange[address]; }
            set { _stylesPerRange[address] = value; }
        }

        /// <summary>
        /// Collection of Excel addresses
        /// </summary>
        public ICollection<string> Keys
        {
            get { return _stylesPerRange.Keys; }
        }

        /// <summary>
        /// Collection of lists of Excel styles
        /// </summary>
        public ICollection<List<ExcelStyle>> Values
        {
            get { return _stylesPerRange.Values; }
        }

        /// <summary>
        /// Get count of Excel addresses having their styles altered
        /// </summary>
        public int Count
        {
            get { return _stylesPerRange.Count; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Create empty Excel style collection
        /// </summary>
        public ExcelStyleCollection()
        {
            _stylesPerRange = new Dictionary<string, List<ExcelStyle>>();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Add an Excel style to the collection
        /// </summary>
        /// <param name="row">Row number of Excel address</param>
        /// <param name="col">Column number of Excel address</param>
        /// <param name="style">Style to add to the style collection</param>
        /// <param name="strict">Whether duplicate addresses in the style collection should generate an error</param>
        public void Add(int row, int col, ExcelStyle style, bool strict = DEFAULT_STRICT_MODE)
        {
            Add(ExcelCellBase.GetAddress(row, col), style, strict);
        }

        /// <summary>
        /// Add an Excel style to the collection
        /// </summary>
        /// <param name="fromRow">From row number of Excel address</param>
        /// <param name="fromCol">From column number of Excel address</param>
        /// <param name="toRow">To row number of Excel address</param>
        /// <param name="toCol">To column number of Excel address</param>
        /// <param name="style">Style to add to the style collection</param>
        /// <param name="strict">Whether duplicate addresses in the style collection should generate an error</param>
        public void Add(int fromRow, int fromCol, int toRow, int toCol, ExcelStyle style, bool strict = DEFAULT_STRICT_MODE)
        {
            Add(ExcelCellBase.GetAddress(fromRow, fromCol, toRow, toCol), style, strict);
        }

        /// <summary>
        /// Add an Excel style to the collection
        /// </summary>
        /// <param name="address">Excel address or range using colon format</param>
        /// <param name="style">Style to add to the style collection</param>
        /// <param name="strict">Whether duplicate addresses in the style collection should generate an error</param>
        public void Add(string address, ExcelStyle style, bool strict = DEFAULT_STRICT_MODE)
        {
            Add(address, new List<ExcelStyle> { style }, strict);
        }

        /// <summary>
        /// Add a list of Excel styles to the collection
        /// </summary>
        /// <param name="row">Row number of Excel address</param>
        /// <param name="col">Column number of Excel address</param>
        /// <param name="styles">List of Excel styles to add to the style collection</param>
        /// <param name="strict">Whether duplicate addresses in the style collection should generate an error</param>
        public void Add(int row, int col, List<ExcelStyle> styles, bool strict = DEFAULT_STRICT_MODE)
        {
            Add(ExcelCellBase.GetAddress(row, col), styles, strict);
        }

        /// <summary>
        /// Add a list of Excel styles to the collection
        /// </summary>
        /// <param name="fromRow">From row number of Excel address</param>
        /// <param name="fromCol">From column number of Excel address</param>
        /// <param name="toRow">To row number of Excel address</param>
        /// <param name="toCol">To column number of Excel address</param>
        /// <param name="styles">List of Excel styles to add to the style collection</param>
        /// <param name="strict">Whether duplicate addresses in the style collection should generate an error</param>
        public void Add(int fromRow, int fromCol, int toRow, int toCol, List<ExcelStyle> styles, bool strict = DEFAULT_STRICT_MODE)
        {
            Add(ExcelCellBase.GetAddress(fromRow, fromCol, toRow, toCol), styles, strict);
        }

        /// <summary>
        /// Add a list of Excel styles to the collection
        /// </summary>
        /// <param name="address">Excel address or range using colon format</param>
        /// <param name="styles">List of Excel styles to add to the style collection</param>
        /// <param name="strict">Whether duplicate addresses in the style collection should generate an error</param>
        public void Add(string address, List<ExcelStyle> styles, bool strict = DEFAULT_STRICT_MODE)
        {
            ValidateAddress(address);
            if (_stylesPerRange.ContainsKey(address))
            {
                if (strict)
                    throw new DuplicateExcelAddressException(address);
                else
                    _stylesPerRange[address].AddRange(styles);
            }
            else
                _stylesPerRange.Add(address, styles);
        }

        /// <summary>
        /// Clear out entire style collection
        /// </summary>
        public void Clear()
        {
            _stylesPerRange.Clear();
        }

        /// <summary>
        /// Determine whether the address key exists in the Excel style collection
        /// </summary>
        /// <param name="row">Row number of Excel address</param>
        /// <param name="col">Column number of Excel address</param>
        /// <returns>Whether the address key exists in the Excel style collection</returns>
        public bool ContainsAddress(int row, int col)
        {
            return ContainsAddress(ExcelCellBase.GetAddress(row, col));
        }

        /// <summary>
        /// Determine whether the address key exists in the Excel style collection
        /// </summary>
        /// <param name="fromRow">From row number of Excel address</param>
        /// <param name="fromCol">From column number of Excel address</param>
        /// <param name="toRow">To row number of Excel address</param>
        /// <param name="toCol">To column number of Excel address</param>
        /// <returns>Whether the address key exists in the Excel style collection</returns>
        public bool ContainsAddress(int fromRow, int fromCol, int toRow, int toCol)
        {
            return ContainsAddress(ExcelCellBase.GetAddress(fromRow, fromCol, toRow, toCol));
        }

        /// <summary>
        /// Determine whether the address key exists in the Excel style collection
        /// </summary>
        /// <param name="address">Excel address or range using colon format</param>
        /// <returns>Whether the address key exists in the Excel style collection</returns>
        public bool ContainsAddress(string address)
        {
            return _stylesPerRange.ContainsKey(address);
        }

        /// <summary>
        /// Remove list of Excel styles based on address
        /// </summary>
        /// <param name="row">Row number of Excel address</param>
        /// <param name="col">Column number of Excel address</param>
        /// <returns>Whether anything was removed</returns>
        public bool RemoveAddress(int row, int col)
        {
            return RemoveAddress(ExcelCellBase.GetAddress(row, col));
        }

        /// <summary>
        /// Remove list of Excel styles based on address
        /// </summary>
        /// <param name="fromRow">From row number of Excel address</param>
        /// <param name="fromCol">From column number of Excel address</param>
        /// <param name="toRow">To row number of Excel address</param>
        /// <param name="toCol">To column number of Excel address</param>
        /// <returns>Whether anything was removed</returns>
        public bool RemoveAddress(int fromRow, int fromCol, int toRow, int toCol)
        {
            return RemoveAddress(ExcelCellBase.GetAddress(fromRow, fromCol, toRow, toCol));
        }

        /// <summary>
        /// Remove list of Excel styles based on address
        /// </summary>
        /// <param name="address">Excel address or range using colon format</param>
        /// <returns>Whether anything was removed</returns>
        public bool RemoveAddress(string address)
        {
            return _stylesPerRange.Remove(address);
        }

        /// <summary>
        /// Generate error if address is not valid
        /// </summary>
        /// <param name="row">Row number of Excel address</param>
        /// <param name="col">Column number of Excel address</param>
        public void ValidateAddress(int row, int col)
        {
            ValidateAddress(ExcelCellBase.GetAddress(row, col));
        }

        /// <summary>
        /// Generate error if address is not valid
        /// </summary>
        /// <param name="fromRow">From row number of Excel address</param>
        /// <param name="fromCol">From column number of Excel address</param>
        /// <param name="toRow">To row number of Excel address</param>
        /// <param name="toCol">To column number of Excel address</param>
        public void ValidateAddress(int fromRow, int fromCol, int toRow, int toCol)
        {
            ValidateAddress(ExcelCellBase.GetAddress(fromRow, fromCol, toRow, toCol));
        }

        /// <summary>
        /// Generate error if address is not valid
        /// </summary>
        /// <param name="address">Excel address or range using colon format</param>
        public void ValidateAddress(string address)
        {
            if (!ExcelCellBase.IsValidAddress(address))
                throw new ExcelAddressNotValidException(address);
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using FileIO.StaticUtilities;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace FileIO
{
    /// <summary>
    /// This is a set of tools for reading and writing Excel files using the EPPlus library
    /// </summary>
    public class Excel
    {
        /// <summary>
        /// Base name for new sheets when a name is not provided
        /// </summary>
        private const string BASE_SHEET_NAME = "Sheet";

        #region Input Methods
        /// <summary>
        /// Creates a dictionary of data tables from an Excel file
        /// <i>NOTE: ExcelRange values are one-based, not zero-based</i>
        /// </summary>
        /// <remarks>https://stackoverflow.com/a/13396787</remarks>
        /// <param name="path">Excel file path</param>
        /// <param name="hasHeaders">Whether worksheets have headers or not</param>
        /// <param name="excludeEmptyLines">Whether the reader should skip over empty lines</param>
        /// <returns>Dictionary of data tables keyed on table names (worksheet names)</returns>
        public static Dictionary<string, DataTable> ReadFileAsDataTables(string path, bool hasHeaders = true, bool excludeEmptyLines = false)
        {
            List<DataTable> tables = new List<DataTable>();

            using (var package = new ExcelPackage())
            {
                // Read in Excel file from a stream
                using (var stream = File.OpenRead(path)) { package.Load(stream); }

                // Loop through worksheets in file
                foreach (ExcelWorksheet worksheet in package.Workbook.Worksheets)
                    tables.Add(GetDataTableFromExcelWorksheet(worksheet, hasHeaders, excludeEmptyLines));
            }

            // Convert list of tables into dictionary
            return tables.ToDictionary(table => table.TableName);
        }

        /// <summary>
        /// Find specified worksheet in workbook and return as a data table
        /// <i>NOTE: ExcelRange values are one-based, not zero-based</i>
        /// </summary>
        /// <remarks>https://stackoverflow.com/a/13396787</remarks>
        /// <param name="path">Excel file path</param>
        /// <param name="sheetName">Name of sheet (case insensitive)</param>
        /// <param name="hasHeader">Whether worksheets have headers or not</param>
        /// <param name="excludeEmptyLines">Whether the reader should skip over empty lines</param>
        /// <param name="columnsToUse">List of columns to use</param>
        /// <returns>Dictionary of data tables keyed on table names (worksheet names)</returns>
        public static DataTable ReadWorksheetAsDataTable(string path, string sheetName, bool hasHeader = true, bool excludeEmptyLines = false, List<string> columnsToUse = null)
        {
            using (var package = new ExcelPackage())
            {
                // Read in Excel file from a stream
                using (var stream = File.OpenRead(path)) { package.Load(stream); }

                // Loop through worksheets in file
                foreach (ExcelWorksheet worksheet in package.Workbook.Worksheets)
                {
                    // Return table for specified worksheet
                    if (worksheet.Name.ToLower() == sheetName.ToLower())
                        return GetDataTableFromExcelWorksheet(worksheet, hasHeader, excludeEmptyLines, columnsToUse);
                }
            }

            // Thorw exception if sheet is not found in the workbook
            throw new SheetNotFoundException(sheetName);
        }

        /// <summary>
        /// Create a data table from an Excel worksheet.
        /// <i>NOTE: ExcelRange values are one-based, not zero-based</i>
        /// </summary>
        /// <remarks>https://stackoverflow.com/a/13396787</remarks>
        /// <param name="worksheet">Predefined Excel worksheet</param>
        /// <param name="hasHeader">Whether worksheet has headers or not</param>
        /// <param name="excludeEmptyLines">Whether the reader should skip over empty lines</param>
        /// <param name="columnsToUse">List of columns to use</param>
        /// <returns>Data table representation of an Excel worksheet</returns>
        public static DataTable GetDataTableFromExcelWorksheet(ExcelWorksheet worksheet, bool hasHeader = true, bool excludeEmptyLines = false, List<string> columnsToUse = null)
        {
            DataTable table = new DataTable(worksheet.Name);

            // Create table columns
            ExcelRange range = worksheet.Cells[1, 1, 1, worksheet.Dimension.End.Column];
            for (int colNum = 1; colNum <= worksheet.Dimension.End.Column; colNum++)
            {
                // Skip column if it is not necessary
                if (!ShouldUseCell(range[1, colNum], columnsToUse)) continue;

                string columnName = (hasHeader ? range[1, colNum].Text : $"Column {range[1, colNum].Start.Column}") + "{0}";
                int attemptNum = 0;

                // Make sure that column name is unique
                while (table.Columns.Contains(string.Format(columnName, ++attemptNum > 1 ? attemptNum.ToString() : ""))) { }
                table.Columns.Add(string.Format(columnName, attemptNum > 1 ? attemptNum.ToString() : ""));
            }

            // Skip first row if headers are to be included
            var startRow = hasHeader ? 2 : 1;

            // Read in worksheet as a data table
            for (int rowNum = startRow; rowNum <= worksheet.Dimension.End.Row; rowNum++)
            {
                // Create new row
                ExcelRange worksheetRow = worksheet.Cells[rowNum, 1, rowNum, worksheet.Dimension.End.Column];

                // Loop through cells
                bool hasData = false;
                List<string> cells = new List<string>();
                for (int colNum = 1; colNum <= worksheet.Dimension.End.Column; colNum++)
                {
                    // Skip column if it is not necessary
                    if (!ShouldUseCell(worksheetRow[rowNum, colNum], columnsToUse)) continue;
                    cells.Add(worksheetRow[rowNum, colNum].Text);
                    if (cells.Last().Trim().Length > 0) hasData = true;
                }
                if (hasData) table.Rows.Add(cells.ToArray());
            }

            // Return the new data table
            return table;
        }

        /// <summary>
        /// Determin whether cell should be used
        /// </summary>
        /// <param name="cell">Cell being examined</param>
        /// <param name="columnsToUse">List of columns to use</param>
        /// <returns>True, use this cell; False, do not use this cell</returns>
        private static bool ShouldUseCell(ExcelRange cell, List<string> columnsToUse)
        {
            if (columnsToUse == null) return true;
            char columnLetter = (char)(cell.Start.Column + 64); // 1 + 64 = 65, which is ASCII 'A'
            return columnsToUse.Select(col => col.ToUpper()).ToList().Contains(columnLetter.ToString());
        }
        #endregion

        #region Output Methods
        /// <summary>
        /// Create an Excel file with a single worksheet from a data table
        /// </summary>
        /// <param name="data">Data table to create Excel file from</param>
        /// <param name="destinationPath">Full destination path for Excel file</param>
        /// <param name="includeHeaderRow">Whether to include a header row</param>
        /// <param name="sheetName">Worksheet name</param>
        /// <param name="styles">Collection of styles to apply to the workbook</param>
        public static void CreateExcelFile(DataTable data, string destinationPath, bool includeHeaderRow = true, string sheetName = BASE_SHEET_NAME + "1", ExcelStyleCollection styles = null)
        {
            DataSet set = new DataSet();
            set.Tables.Add(data ?? new DataTable());

            CreateExcelFile(
                set,
                destinationPath,
                includeHeaderRow,
                new string[] { sheetName },
                styles == null ? null : new List<ExcelStyleCollection> { styles }
            );
        }

        /// <summary>
        /// Create an Excel file with multiple worksheets from a data set
        /// </summary>
        /// <param name="data">Data set to create Excel file from</param>
        /// <param name="destinationPath">Full destination path for Excel file</param>
        /// <param name="includeHeaderRow">Whether to include a header row</param>
        /// <param name="sheetNames">Array of worksheet names</param>
        /// <param name="styles">Collection of styles to apply to the workbook</param>
        public static void CreateExcelFile(DataSet data, string destinationPath, bool includeHeaderRow = true, string[] sheetNames = null, List<ExcelStyleCollection> styles = null)
        {
            // Validate parameters
            if (data == null) data = new DataSet();
            if (data.Tables.Count == 0) data.Tables.Add(new DataTable());
            if (string.IsNullOrWhiteSpace(destinationPath)) throw new Exception("destinationPath must be populated");
            if (!Operations.IsFilePathValid(destinationPath, false)) throw new Exception($"Destination path is not valid: {destinationPath}");

            // Create new Excel file
            using (ExcelPackage excelPack = new ExcelPackage(new FileInfo(destinationPath)))
            {
                // Loop through tables in data set
                for (int i = 0; i < data.Tables.Count; i++)
                {
                    // Determine sheet name and create blank worksheet
                    string sheetName = sheetNames == null ? $"{BASE_SHEET_NAME}{i + 1}" : i >= sheetNames.Length ? $"{BASE_SHEET_NAME}{i + 1}" : sheetNames[i];
                    sheetName = sheetName.Trim('\'');
                    ExcelWorksheet worksheet = excelPack.Workbook.Worksheets.Add(sheetName);

                    // Format header, if necessary
                    if (includeHeaderRow)
                    {
                        ExcelRange headerRange = worksheet.Cells[$"A1:{GetExcelColumnName(data.Tables[i].Columns.Count)}1"];
                        headerRange.Style.Font.Bold = true;
                        headerRange.Style.Border.Bottom.Style = ExcelBorderStyle.Thick;
                    }

                    // Write data table to current worksheet
                    if (data.Tables[i].Rows.Count > 0)
                        worksheet.Cells["A1"].LoadFromDataTable(data.Tables[i], includeHeaderRow);

                    // Auto-fit columns so that fields are not truncated
                    worksheet.Cells.AutoFitColumns();
                    worksheet.Cells.Style.WrapText = true;

                    // Apply formatting
                    if (styles != null)
                        if (styles.Count > i)
                            foreach(string address in styles[i].Keys)
                            {
                                ExcelRange range = worksheet.Cells[address];
                                foreach(ExcelStyle style in styles[i][address])
                                    ApplyStyle(range, style);
                            }
                }

                // Save Excel file
                excelPack.SaveAs(new FileInfo(destinationPath));
            }
        }

        /// <summary>
        /// Determine Excel column name based on the column number, starting at 1 = A
        /// </summary>
        /// <remarks>https://stackoverflow.com/a/182924</remarks>
        /// <param name="columnNumber">Column number to translate</param>
        /// <returns>Column name</returns>
        private static string GetExcelColumnName(int columnNumber)
        {
            int dividend = columnNumber;
            string columnName = string.Empty;
            int modulo;

            while (dividend > 0)
            {
                modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = (int)((dividend - modulo) / 26);
            }

            return columnName;
        }

        /// <summary>
        /// Applys an Excel style to a defined range
        /// </summary>
        /// <param name="range">Range to assig nstyle to</param>
        /// <param name="style">Style to assign to range</param>
        private static void ApplyStyle(ExcelRange range, ExcelStyle style)
        {
            switch (style.Type)
            {
                case ExcelStyleType.Alignment:
                    switch (style.Parameters[0].ToString().ToLower())
                    {
                        case "center":
                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            break;
                        case "justify":
                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                            break;
                        case "left":
                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            break;
                        case "right":
                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            break;
                    }
                    break;
                case ExcelStyleType.BackgroundColor:
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(style.GetColorParameter());
                    break;
                case ExcelStyleType.Bold:
                    range.Style.Font.Bold = true;
                    break;
                case ExcelStyleType.ColumnWidth:
                    for (int col = range.Start.Column; col <= range.End.Column; col++)
                        range.Worksheet.Column(col).Width = float.Parse(style.Parameters[0].ToString());
                    break;
                case ExcelStyleType.DoubleBorder:
                    foreach (string side in style.Parameters)
                        switch (side.ToLower())
                        {
                            case "bottom": range.Style.Border.Bottom.Style = ExcelBorderStyle.Double; break;
                            case "left": range.Style.Border.Left.Style = ExcelBorderStyle.Double; break;
                            case "right": range.Style.Border.Right.Style = ExcelBorderStyle.Double; break;
                            case "top": range.Style.Border.Top.Style = ExcelBorderStyle.Double; break;
                        }
                    break;
                case ExcelStyleType.FontColor:
                    range.Style.Font.Color.SetColor(style.GetColorParameter());
                    break;
                case ExcelStyleType.FontFamily:
                    while (range.MoveNext())
                        range.Current.Style.Font.SetFromFont(
                            new Font(
                                style.Parameters[0].ToString(),
                                range.Current.Style.Font.Size));
                    break;
                case ExcelStyleType.FontSize:
                    range.Style.Font.Size = float.Parse(style.Parameters[0].ToString());
                    break;
                case ExcelStyleType.FormatDate:
                    range.Style.Numberformat.Format = style.Parameters[0].ToString();
                    break;
                case ExcelStyleType.FormatNumber:
                    range.Style.Numberformat.Format = style.Parameters == null ? "0.00" : style.Parameters[0].ToString();
                    break;
                case ExcelStyleType.FormatText:
                    range.Style.Numberformat.Format = "@";
                    break;
                case ExcelStyleType.Italic:
                    range.Style.Font.Italic = true;
                    break;
                case ExcelStyleType.Merge:
                    range.Merge = true;
                    break;
                case ExcelStyleType.StrikeThrough:
                    range.Style.Font.Strike = true;
                    break;
                case ExcelStyleType.ThickBorder:
                    foreach (string side in style.Parameters)
                        switch(side.ToLower())
                        {
                            case "bottom": range.Style.Border.Bottom.Style = ExcelBorderStyle.Thick; break;
                            case "left": range.Style.Border.Left.Style = ExcelBorderStyle.Thick; break;
                            case "right": range.Style.Border.Right.Style = ExcelBorderStyle.Thick; break;
                            case "top": range.Style.Border.Top.Style = ExcelBorderStyle.Thick; break;
                        }
                    break;
                case ExcelStyleType.ThinBorder:
                    foreach (string side in style.Parameters)
                        switch (side.ToLower())
                        {
                            case "bottom": range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin; break;
                            case "left": range.Style.Border.Left.Style = ExcelBorderStyle.Thin; break;
                            case "right": range.Style.Border.Right.Style = ExcelBorderStyle.Thin; break;
                            case "top": range.Style.Border.Top.Style = ExcelBorderStyle.Thin; break;
                        }
                    break;
                case ExcelStyleType.Underline:
                    range.Style.Font.UnderLine = true;
                    break;
                case ExcelStyleType.VerticalAlignment:
                    switch (style.Parameters[0].ToString().ToLower())
                    {
                        case "bottom":
                            range.Style.VerticalAlignment = ExcelVerticalAlignment.Bottom;
                            break;
                        case "center":
                            range.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            break;
                        case "justify":
                            range.Style.VerticalAlignment = ExcelVerticalAlignment.Justify;
                            break;
                        case "top":
                            range.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            break;
                    }
                    break;
                default:
                    throw new Exception($"Style type not supported: {style.Type}");
            }
        }
        #endregion
    }
}

﻿using FileIO.StaticUtilities;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace FileIO
{
    /// <summary>
    /// This class contains file operation tasks. Such as copying files, archiving files, and deleting files.
    /// </summary>
    public static class Operations
    {
        /// <summary>
        /// 
        /// </summary>
        private const bool DEFAULT_FORCE = false;

        /// <summary>
        /// The machine name running the program
        /// </summary>
        public static string MachineName
        {
            get { return Environment.MachineName; }
        }

        /// <summary>
        /// Archives the given file to the default archive folder. File will be moved to {rootArchivePath}\{MachineName}\{appName}\{currentDateTime}
        /// </summary>
        /// <param name="path">Specifies source path</param>
        /// <param name="appName">Specifies the application name</param>
        public static void ArchiveFile(string path, string appName)
        {
            ArchiveFile(path, Path.GetFileName(path), appName, DateTime.Now);
        }

        /// <summary>
        /// Archives the given file to the default archive folder. Overload to specify a date
        /// </summary>
        /// <param name="path">Specifies source path</param>
        /// <param name="appName">Specifies the application name</param>
        /// <param name="dt">Date used to create archive folder</param>
        public static void ArchiveFile(string path, string appName, DateTime dt)
        {
            ArchiveFile(path, Path.GetFileName(path), appName, dt);
        }

        /// <summary>
        /// Archives given file to a specificed archive folder
        /// </summary>
        /// <param name="srcPath">Specifies full source path</param>
        /// <param name="destPath">Specifies full destination path</param>
        /// <param name="appName">Specifies the application name</param>
        [Obsolete("This method does not work as expected; this should not be used", true)]
        public static void ArchiveFile(string srcPath, string destPath, string appName)
        {
            string destDirectory = Path.GetDirectoryName(destPath);

            try
            {
                Console.WriteLine("Archiving file");
                Console.WriteLine(" - Source:      " + srcPath);
                Console.WriteLine(" - Destination: " + destPath);

                // Create archive sub-folder if necessary and archive file
                if (!Directory.Exists($@"{destDirectory}"))
                    Directory.CreateDirectory($@"{destDirectory}");

                if (srcPath != destPath)
                    File.Copy(srcPath, destPath);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// Archives the given file to the default archive folder. Overload to specify a date and destination file name
        /// </summary>
        /// <param name="path">Specifies source path</param>
        /// <param name="destFileName"></param>
        /// <param name="appName">Specifies the application name</param>
        /// <param name="dt">Date used to create archive folder</param>
        public static void ArchiveFile(string path, string destFileName, string appName, DateTime dt)
        {
            try
            {
                string source = path;
                string destination = Path.Combine(GetArchiveFolderName(appName, dt), destFileName);

                Console.WriteLine("Archiving file");
                Console.WriteLine(" - Source:      " + source);
                Console.WriteLine(" - Destination: " + destination);

                // Create archive sub-folder if necessary and archive file
                if (!Directory.Exists(Path.GetDirectoryName(destination)))
                    Directory.CreateDirectory(Path.GetDirectoryName(destination));

                File.Copy(source, destination);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// Copy a file to a new location
        /// </summary>
        /// <param name="sourcePath">Specifies source path</param>
        /// <param name="destPath">Specifies destination path</param>
        /// <param name="force">Whether to force the file copy</param>
        public static void CopyFile(string sourcePath, string destPath, bool force = DEFAULT_FORCE)
        {
            try
            {
                Console.WriteLine("Copying file");
                Console.WriteLine($" - Source:      {sourcePath}");
                Console.WriteLine($" - Destination: {destPath}");

                if (sourcePath != destPath)
                    try
                    {
                        File.Copy(sourcePath, destPath);
                    }
                    catch (Exception ex)
                    {
                        if (!force) throw ex;
                        Console.WriteLine("...attempting forced copy...");
                        CommandLineTools.Run($"copy /Y \"{sourcePath}\" \"{destPath}\"");
                    }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// Copy a file to a new location
        /// </summary>
        /// <param name="sourceFolder">Specifies source path</param>
        /// <param name="destFolder">Specifies destination path</param>
        /// <param name="fileName">Specifies the file name</param>
        /// <param name="force">Whether to force the file copy</param>
        public static void CopyFile(string sourceFolder, string destFolder, string fileName, bool force = DEFAULT_FORCE)
        {
            CopyFile(Path.Combine(sourceFolder, fileName), Path.Combine(destFolder, fileName), force);
        }

        /// <summary>
        /// Move a file
        /// </summary>
        /// <param name="sourcePath">Specifies the source path</param>
        /// <param name="destPath">Specifies the destination path</param>
        /// <param name="force">Whether to force the file move</param>
        public static void MoveFile(string sourcePath, string destPath, bool force = DEFAULT_FORCE)
        {
            try
            {
                Console.WriteLine("Moving file");
                Console.WriteLine($" - Source:      {sourcePath}");
                Console.WriteLine($" - Destination: {destPath}");

                if (sourcePath != destPath)
                {
                    // Copy file
                    try
                    {
                        File.Copy(sourcePath, destPath, true);
                    }
                    catch (Exception ex)
                    {
                        if (!force) throw ex;
                        Console.WriteLine("...attempting forced copy...");
                        CommandLineTools.Run($"copy /Y \"{sourcePath}\" \"{destPath}\"");
                    }

                    // Delete file
                    try
                    {
                        File.Delete(sourcePath);
                    }
                    catch (Exception ex)
                    {
                        if (!force) throw ex;
                        Console.WriteLine("...attempting forced delete...");
                        CommandLineTools.Run($"del /f \"{sourcePath}\"");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// Move the specified file to the given destination folder
        /// </summary>
        /// <param name="sourceFolder">Specifies the source path</param>
        /// <param name="destFolder">Specifies the destination path</param>
        /// <param name="fileName">Specifies the file name</param>
        /// <param name="force">Whether to force the file move</param>
        public static void MoveFile(string sourceFolder, string destFolder, string fileName, bool force = DEFAULT_FORCE)
        {
            MoveFile(Path.Combine(sourceFolder, fileName), Path.Combine(destFolder, fileName), force);
        }

        /// <summary>
        /// Delete a file in the given directory
        /// </summary>
        /// <param name="path">Specifies the source path</param>
        /// <param name="force">Whether to force the delete</param>
        public static void DeleteFile(string path, bool force = DEFAULT_FORCE)
        {
            try
            {
                Console.WriteLine("Deleting file");
                Console.WriteLine($" - File:        {path}");

                try
                {
                    File.Delete(path);
                }
                catch (Exception ex)
                {
                    if (!force) throw ex;
                    Console.WriteLine("...attempting forced delete...");
                    CommandLineTools.Run($"del /f \"{path}\"");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// Delete a file
        /// </summary>
        /// <param name="sourceFolder">Specifies the source path</param>
        /// <param name="fileName">Specifies the file path</param>
        /// <param name="force">Whether to force the delete</param>
        public static void DeleteFile(string sourceFolder, string fileName, bool force = DEFAULT_FORCE)
        {
            DeleteFile(Path.Combine(sourceFolder, fileName), force);
        }

        /// <summary>
        /// Determines whether a path is valid or not
        /// </summary>
        /// <param name="filePath">File path to validate</param>
        /// <param name="canFileExist">Whether the path should be considered valid if the file already exists</param>
        /// <returns>True if file path is valid</returns>
        public static bool IsFilePathValid(string filePath, bool canFileExist = true)
        {
            if (Path.GetInvalidPathChars().Where(c => filePath.Contains(c)).Count() > 0)
            {
                Console.WriteLine($"File path contains invalid characters: {filePath}");
                return false;
            }
            if (!Path.IsPathRooted(filePath))
            {
                Console.WriteLine($"File path is not rooted: {filePath}");
                return false;
            }
            FileInfo fi = null;
            try
            {
                fi = new FileInfo(filePath);
            }
            catch (ArgumentException) { }
            catch (PathTooLongException) { Console.WriteLine($"File path is too long: {filePath}"); }
            catch (NotSupportedException) { Console.WriteLine($"File path is not supported: {filePath}"); }
            if (ReferenceEquals(fi, null)) { return false; }
            else if (!canFileExist)
            {
                if (File.Exists(filePath))
                {
                    Console.WriteLine($"File already exists: {filePath}");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Determines what the archive folder path would be given specific criteria
        /// </summary>
        /// <param name="appName">Specifies the application name</param>
        /// <param name="dt">Date used to create archive folder</param>
        /// <returns>Potential archive folder path</returns>
        public static string GetArchiveFolderName(string appName, DateTime dt)
        {
            return $@"{FileIOConfigBuilder.Instance.Config.ArchiveRoot}\{MachineName}\{appName}\{dt.ToString("yyyyMMddHHmmss")}\";
        }
    }
}

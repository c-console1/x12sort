using DataLayer.Base;
using DataLayer.HealthRulesConnectors.Models;
using DataLayer.HealthRulesConnectors.Requests;
using System.Net.Http;
using System.Threading.Tasks;

namespace DataLayer.HealthRulesConnectors
{
    public class AuthorizationAPIRepository : BaseRepository<HealthRulesConnectorsDatabase>
    {
        private ApiExec Api { get; set; }

        internal AuthorizationAPIRepository(HealthRulesConnectorsDatabase database) : base(database)
        {

        }

        //GETs are not implemented yet
        #region GET
        //public InpatientAuthorization GetInpatientAuthorization(string authNbr)
        //{
        //    //TODO: Replace
        //    return (InpatientAuthorization)GetAuthorization(authNbr).Result;
        //}

        //public OutpatientAuthorization GetOutpatientAuthorization(string authNbr)
        //{
        //    //TODO: Replace
        //    return (OutpatientAuthorization)GetAuthorization(authNbr).Result;
        //}

        //public InpatientAuthorization GetInpatientAuthorization(string memberNbr, string authNbr)
        //{
        //    //TODO: Replace
        //    //return (HRPInpatientAuthorization)GetAuthorization(memberNbr, authNbr).Result;
        //    return new InpatientAuthorization();
        //}

        //public OutpatientAuthorization GetOutpatientAuthorization(string memberNbr, string authNbr)
        //{
        //    //TODO: Replace
        //    //return (HRPOutpatientAuthorization)GetAuthorization(memberNbr, authNbr).Result;
        //    return new OutpatientAuthorization();
        //}

        //internal async Task<IAuthorization> GetAuthorization(string authNbr)
        //{
        //    //TODO: Replace
        //    HttpResponseMessage response = await Database.AuthorizationAPIRepository.PostAuthorization<ISoapAuthRequest, ISoapAuthResponse>(new object());

        //    //TODO: receive XML, not JSON
        //    return JsonConvert.DeserializeObject<IAuthorization>(await response.Content.ReadAsStringAsync());
        //}

        //internal object GetAuthorization(string memberNbr, string authNbr)
        //{
        //    return new object();
        //}
        #endregion

        #region POST
        /// <summary>
        /// Public method for submitting authorizations to HRP
        /// </summary>
        /// <param name="authorization"></param>
        /// <returns></returns>
        public async Task<HttpResponseMessage> SubmitAuthorization(SubmitAuthRequest<serviceAuthorization, HttpResponseMessage> authorization)
        {
            return await SubmitAuthorization<serviceAuthorization, HttpResponseMessage>(authorization);
        }

        /// <summary>
        /// Internal data layer method that will call ExecuteSoapAsync, attempt an xml serialization and send to HRP
        /// </summary>
        /// <typeparam name="TRequest"></typeparam>
        /// <typeparam name="TResponse"></typeparam>
        /// <param name="content"></param>
        /// <returns></returns>
        internal async Task<HttpResponseMessage> SubmitAuthorization<TRequest, TResponse>(ISoapRequest<serviceAuthorization, HttpResponseMessage> content)
            where TRequest : class where TResponse : class
        {
            Task<HttpResponseMessage> response = Database.AuthorizationAPIRepository.Api.ExecuteSoapAsync(content);

            return await response;
        }
        #endregion
    }
}

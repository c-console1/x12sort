using System.Collections.Generic;
using System.Xml;
using DataLayer.Base;
using DataLayer.HealthRulesConnectors.Enumerations;
using DataLayer.HealthRulesConnectors.Models;
using DataLayer.HealthRulesConnectors.Models.Requests;
using DataLayer.HealthRulesConnectors.Models.Responses;

namespace DataLayer.HealthRulesConnectors
{
    public class AuthorizationRepository : BaseRepository<HealthRulesConnectorsDatabase>
    {
        internal AuthorizationRepository(HealthRulesConnectorsDatabase database) : base(database)
        {

        }

        #region Authorization Call

        /// <summary>
        /// Authorization Connector Call that returns an XML string 
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public string AuthorizationText(AuthorizationRequest source)
        {
            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V5);

            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "Authorization"
            };

            string xml = objSoap.callObject.Get(conn, source.Get());
            return xml;
        }

        /// <summary>
        /// Authorization Connector Call that returns an XMLDocument object
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public XmlDocument AuthorizationXML(AuthorizationRequest source)
        {
            string xml = AuthorizationText(source);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            return doc;

        }

        /// <summary>
        /// Authorization Connector Call that returns an .NET object
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public AuthorizationResponse.Response Authorization(AuthorizationRequest source)
        {

            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V5);

            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "Authorization"
            };

            // If a single XML object comes in and gets desialized to an object that is a list
            // it will create an error unless our deserilizing knows which field to treat as
            // lists.
            List<string> forceJSONArray = new List<string>() { "services", "exceptions", "error" };

            AuthorizationResponse.Response result = objSoap.callObject.GetXML
                <AuthorizationResponse.Response>(conn, source.Get(), forceJSONArray);

            return result;

        }

        /* // Customimize your own call with simplier parameters
        /// <summary>
        /// Authorization Connector Call that returns an .NET object
        /// </summary>
        /// <param name="memberHccId">HealthRules ID</param>
        /// <param name="asOfDate">Display COB ID's equal to or later than this date</param>
        /// <returns></returns>
        public AuthorizationResponse.Response Authorization(string memberHccId = "", string asOfDate = "") {

            Requests.AuthorizationRequest source = new AuthorizationRequest()
            {
                 asOfDate = asOfDate, memberHccId = memberHccId
            };

            return Authorization(source);
        }
		*/

        #endregion Authorization Call

        #region AuthorizationLookup Call

        /// <summary>
        /// AuthorizationLookup Connector Call that returns an XML string 
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public string AuthorizationLookupText(AuthorizationLookupRequest source)
        {
            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V5);

            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "AuthorizationLookup"
            };

            string xml = objSoap.callObject.Get(conn, source.Get());
            return xml;
        }

        /// <summary>
        /// AuthorizationLookup Connector Call that returns an XMLDocument object
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public XmlDocument AuthorizationLookupXML(AuthorizationLookupRequest source)
        {
            string xml = AuthorizationLookupText(source);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            return doc;
        }

        /// <summary>
        /// AuthorizationLookup Connector Call that returns an .NET object
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public AuthorizationLookupResponse.Response AuthorizationLookup(AuthorizationLookupRequest source)
        {

            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V5);

            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "AuthorizationLookup"
            };

            // If a single XML object comes in and gets desialized to an object that is a list
            // it will create an error unless our deserilizing knows which field to treat as
            // lists.
            List<string> forceJSONArray = new List<string>() { "services", "exceptions", "error" };

            AuthorizationLookupResponse.Response result = objSoap.callObject.GetXML
                <AuthorizationLookupResponse.Response>(conn, source.Get(), forceJSONArray);

            return result;
        }

        /* // Customimize your own call with simplier parameters
        /// <summary>
        /// AuthorizationLookup Connector Call that returns an .NET object
        /// </summary>
        /// <param name="memberHccId">HealthRules ID</param>
        /// <param name="asOfDate">Display COB ID's equal to or later than this date</param>
        /// <returns></returns>
        public AuthorizationLookupResponse.Response AuthorizationLookup(string memberHccId = "", string asOfDate = "") {

            Requests.AuthorizationLookupRequest source = new AuthorizationLookupRequest()
            {
                 asOfDate = asOfDate, memberHccId = memberHccId
            };

            return AuthorizationLookup(source);
        }
		*/

        #endregion AuthorizationLookup Call

    }
}

using DataLayer.Base.Models;

namespace DataLayer.HealthRulesConnectors.Models
{
    /// <summary>
    /// Base object for all HealthRulses Connector Objects
    /// </summary>
    public abstract class BaseHealthRulesConnectorObject : BaseDataObject
    {
        /// <summary>
        /// Standard Constructor, Should be called by all inheriting objects
        /// </summary>
        protected BaseHealthRulesConnectorObject() : base()
        {
        }
    }
}

using DataLayer.Base.Models;

namespace DataLayer.HealthRulesConnectors.Models
{
    public class RequestingProvider : IAuthorizationProvider
    {
        /// <summary>
        /// Provider ID
        /// </summary>
        public string ProviderID { get; set; }

        /// <summary>
        /// Provider identifier
        /// </summary>
        public string ProviderNumber { get; set; }

        /// <summary>
        /// External provider identifier
        /// </summary>
        public string ProviderExternalNumber { get; set; }

        /// <summary>
        /// Provider name
        /// </summary>
        public string ProviderName { get; set; }

        /// <summary>
        /// Provider NPI
        /// </summary>
        public string ProviderNPI { get; set; }

        /// <summary>
        /// Provider TIN
        /// </summary>
        public string ProviderTaxonomy { get; set; }

        /// <summary>
        /// Name of contact at provider location
        /// </summary>
        public string ProviderContactName { get; set; }

        /// <summary>
        /// Provider phone
        /// </summary>
        public PhoneNumber ProviderPhone { get; set; }

        /// <summary>
        /// Provider contact phone
        /// </summary>
        public PhoneNumber ContactPhone { get; set; }
    }
}

using DataLayer.Base.Models;
using System;

namespace DataLayer.HealthRulesConnectors.Models
{
    public class Admission : IAdmission
    {
        public string DatabaseKey { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? AdmissionDate { get; set; }
        public string AdmissionSource { get; set; }
        public string AdmissionType { get; set; }
    }
}

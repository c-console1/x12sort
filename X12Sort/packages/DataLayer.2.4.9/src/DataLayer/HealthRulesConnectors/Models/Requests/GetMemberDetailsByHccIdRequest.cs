using System;
using DataLayer.StaticUtilities;

namespace DataLayer.HealthRulesConnectors.Models.Requests
{
    public class GetMemberDetailsByHccIdRequest
    {

        public string Get()
        {
            // Validate at least 1 value must be entered
            if (
                string.IsNullOrWhiteSpace(memberHccId)
                )
                throw new Exception("Enter Member HCC ID");

            // Build request
            // * Wrap the raw serialized XML with a temproary or '<junk}' tag so a multiple root XML object errot is not the
            //   thrown on XML clean-up
            // * Run the XML cleanup routine
            // * Remove the temproary or 'junk' tag
            string output = "<junk>" + XMLSerialization.Object2XML(this) + "</junk>"; // Serlize object, add temp wrapper tags
            output = XMLSerialization.RemoveEmptyTags(output); // Remove empty tag
            output = output.Replace("<junk>", "").Replace("</junk>", ""); // remove temp tags

            // * Put the XML output from above into a standard template loaded from the 'resRequests.resx' resource file
            string xmlTemplate = resRequests.GetMemberDetailsByHccId; // Load template
            output = string.Format(xmlTemplate, output); // Merge generated XML into template

            // * Return request objects
            return output;
        }

        public string memberHccId { get; set; }

        public MemberDetailFlags memberDetailFlags = new MemberDetailFlags();

        public class MemberDetailFlags
        {
            public string includeAccount { get; set; }
            public string includeCob { get; set; }
            public string includeHeadOfHousehold { get; set; }
            public string includeParentAccount { get; set; }
            public string includePlan { get; set; }
            public string includeProvider { get; set; }
            public string includeProviderPhone { get; set; }
            public string includeRider { get; set; }
            public string includeSubAccount { get; set; }
        }
    }
}

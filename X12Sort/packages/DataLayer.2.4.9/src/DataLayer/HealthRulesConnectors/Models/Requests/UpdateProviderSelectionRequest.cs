using System;
using DataLayer.StaticUtilities;


namespace DataLayer.HealthRulesConnectors.Models.Requests
{
    public class UpdateProviderSelectionRequest
    {
        public string Get()
        {
            // Validate at least 1 value must be entered
            if (string.IsNullOrWhiteSpace(memberHccId))
                throw new Exception("'memberHccId' is not set");

            if (string.IsNullOrWhiteSpace(selectionType))
                throw new Exception("'selectionType' is not set");

            if (string.IsNullOrWhiteSpace(providerSelectionReasonCode))
                throw new Exception("'providerSelectionReasonCode' is not set");

            if (string.IsNullOrWhiteSpace(forceToWorkbasket))
                throw new Exception("'forceToWorkbasket' is not set");

            if (string.IsNullOrWhiteSpace(auditLogInfo.messageCode))
                throw new Exception("'auditLogInfo.messageCode' is not set");

            if (string.IsNullOrWhiteSpace(auditLogInfo.messageDescription))
                throw new Exception("'auditLogInfo.messageDescription' is not set");


            // Build request
            // * Wrap the raw serialized XML with a temproary or '<junk}' tag so a multiple root XML object errot is not the
            //   thrown on XML clean-up
            // * Run the XML cleanup routine
            // * Remove the temproary or 'junk' tag 
            string output = "<junk>" + XMLSerialization.Object2XML(this) + "</junk>"; // Serlize object, add temp wrapper tags
            output = XMLSerialization.RemoveEmptyTags(output); // Remove empty tag
            output = output.Replace("<junk>", "").Replace("</junk>", ""); // remove temp tags

            // * Put the XML output from above into a standard template loaded from the 'resRequests.resx' resource file
            string xmlTemplate = resRequests.UpdateProviderSelection; // Load template
            output = string.Format(xmlTemplate, output); // Merge generated XML into template

            // * Return request objects
            return output;
        }

        public string memberHccId { get; set; }
        public string w3cAsOfDate { get; set; }
        public string selectionType { get; set; }
        public string practitionerHccId { get; set; }
        public string practitionerRoleHccId { get; set; }
        public string supplierHccId { get; set; }
        public string supplierNetworkHccId { get; set; }
        public string providerSelectionReasonCode { get; set; }
        public AuditLogInfo auditLogInfo = new AuditLogInfo();
        public string forceToWorkbasket { get; set; }

        public class AuditLogInfo
        {
            public string comment { get; set; }
            public string messageCode { get; set; }
            public string messageDescription { get; set; }
            public string user { get; set; }
        }

    }
}

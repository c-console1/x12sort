using System;
using System.Collections.Generic;
using System.Text;
using DataLayer.StaticUtilities;

namespace DataLayer.HealthRulesConnectors.Models.Requests
{
    public class AuthorizationRequest
    {

        /// <summary>
        /// Generate the serialized response object
        /// </summary>
        /// <returns></returns>
        public string Get()
        {
            // Validation goes here


            // Serialize response and send
            return XMLSerialization.BuildRequestObject(this, resRequests.Authorization);
        }

        public string authorizationInternalId { get; set; }
        public string recordType { get; set; }
        public string authorizationId { get; set; }
        public string externalAuthorizationId { get; set; }
        public string authorizationStatus { get; set; }
        public List<TraceInfo> traceInfo { get; set; }
        public MaintenanceInfo maintenanceInfo { get; set; }
        public string inputSource { get; set; }
        public string requestDate { get; set; }
        public string receivedDate { get; set; }
        public string requestedAdmitStatusCode { get; set; }
        public string patientTrackingNumber { get; set; }
        public string externalAuthBatchNumber { get; set; }
        public PerformingProvider performingProvider { get; set; }
        public RequestingProvider requestingProvider { get; set; }
        public AdmissionInformation admissionInformation { get; set; }
        public string servicesAction { get; set; }
        public List<Services> services { get; set; }
        public List<Diagnosis> diagnosis { get; set; }
        public string acuity { get; set; }
        public string authSeverity { get; set; }
        public TypeOfBill typeOfBill { get; set; }
        public string placeOfService { get; set; }
        public string releaseOfInformation { get; set; }
        public string memberIdentifier { get; set; }
        public string memberSystemId { get; set; }
        public MemberMatchInput memberMatchInput { get; set; }
        public AuthorizationMatchInput authorizationMatchInput { get; set; }
        public string mdcCode { get; set; }
        public List<ClinicalNotes> clinicalNotes { get; set; }
        public string udtAction { get; set; }
        public UserDefinedTerm userDefinedTerm { get; set; }
        public List<Udt> udt { get; set; }
        public List<AuthorizationAttachments> authorizationAttachments { get; set; }
        public string allowWorkbasketAmend { get; set; }


        public class TraceInfo
        {
            public string traceEntityId { get; set; }
            public string traceAdditionalId { get; set; }
        }

        public class MaintenanceInfo
        {
            public string transactionType { get; set; }
            public string overrideAction { get; set; }
            public string overrideResult { get; set; }
        }

        public class PerformingProvider
        {
            public string providerType { get; set; }
            public string providerSystemId { get; set; }
            public string providerPlanId { get; set; }
            public string providerNPIId { get; set; }
            public string providerTINId { get; set; }
            public string providerIdentifier { get; set; }
            public string providerTaxonomyCode { get; set; }
            public string organizationName { get; set; }
            public IndividualName individualName { get; set; }
            public Address address { get; set; }
            public List<PhoneNumbers> phoneNumbers { get; set; }
        }

        public class IndividualName
        {
            public string lastName { get; set; }
            public string firstName { get; set; }
            public string middleName { get; set; }
            public string namePrefix { get; set; }
            public string nameSuffix { get; set; }
            public NameSuffixList nameSuffixList { get; set; }
            public CredentialSuffixList credentialSuffixList { get; set; }
        }

        public class NameSuffixList
        {
            public string listMode { get; set; }
            public List<string> suffix { get; set; }
        }

        public class CredentialSuffixList
        {
            public string listMode { get; set; }
            public List<CredentialSuffix> CredentialSuffix { get; set; }
        }

        public class CredentialSuffix
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class Address
        {
            public Address address { get; set; }
            public string cityName { get; set; }
            public string stateCode { get; set; }
            public string zipCode { get; set; }
            public string zipExtensionCode { get; set; }
            public CountryCode countryCode { get; set; }
        }

        public class CountryCode
        {
            public CountryCode countryCode { get; set; }
        }

        public class PhoneNumbers
        {
            public string phoneCountryCode { get; set; }
            public string phoneAreaCode { get; set; }
            public string phoneNumber { get; set; }
            public string phoneExtensionNumber { get; set; }
            public IndividualPhoneTypeCode individualPhoneTypeCode { get; set; }
            public OrganizationPhoneTypeCode organizationPhoneTypeCode { get; set; }
        }

        public class IndividualPhoneTypeCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class OrganizationPhoneTypeCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class RequestingProvider
        {
            public string providerType { get; set; }
            public string providerSystemId { get; set; }
            public string providerPlanId { get; set; }
            public string providerNPIId { get; set; }
            public string providerTINId { get; set; }
            public string providerIdentifier { get; set; }
            public string providerTaxonomyCode { get; set; }
            public string organizationName { get; set; }
            public IndividualName individualName { get; set; }
            public Address address { get; set; }
            public List<PhoneNumbers> phoneNumbers { get; set; }
        }

        public class AdmissionInformation
        {
            public string expectedAdmissionDateRangeStart { get; set; }
            public string expectedAdmissionDateRangeEnd { get; set; }
            public string actualAdmissionDate { get; set; }
            public string expectedDischargeDateRangeStart { get; set; }
            public string expectedDischargeDateRangeEnd { get; set; }
            public string actualDischargeDate { get; set; }
            public string admissionSource { get; set; }
            public string admissionType { get; set; }
            public DischargeStatus dischargeStatus { get; set; }
        }

        public class DischargeStatus
        {
            public string ID { get; set; }
        }

        public class Services
        {
            public string serviceSetIdentifier { get; set; }
            public string externalServiceSetId { get; set; }
            public string startServiceDate { get; set; }
            public string endServiceDate { get; set; }
            public string requestedStartServiceDate { get; set; }
            public string requestedEndServiceDate { get; set; }
            public string receivedDate { get; set; }
            public string clinicalReceiptDate { get; set; }
            public string serviceSetStatus { get; set; }
            public string note { get; set; }
            public string approvedQuantity { get; set; }
            public string approvedUnits { get; set; }
            public string approvedAmount { get; set; }
            public string requestedQuantity { get; set; }
            public string requestedUnits { get; set; }
            public RenderingProvider renderingProvider { get; set; }
            public ServiceType serviceType { get; set; }
            public List<Diagnosis> diagnosis { get; set; }
            public ServiceAgreementDetail serviceAgreementDetail { get; set; }
            public HicApproval hicApproval { get; set; }
            public string authorizedStatusReasonCode { get; set; }
            public string authorizationTypeCode { get; set; }
            public string authorizationPriority { get; set; }
            public ReviewType reviewType { get; set; }
            public ReviewLevel reviewLevel { get; set; }
            public AuthorizationServiceSetMatchInput authorizationServiceSetMatchInput { get; set; }
        }

        public class RenderingProvider
        {
            public string providerType { get; set; }
            public string providerSystemId { get; set; }
            public string providerPlanId { get; set; }
            public string providerNPIId { get; set; }
            public string providerTINId { get; set; }
            public string providerIdentifier { get; set; }
            public string providerTaxonomyCode { get; set; }
            public string organizationName { get; set; }
            public IndividualName individualName { get; set; }
            public Address address { get; set; }
            public List<PhoneNumbers> phoneNumbers { get; set; }
        }

        public class ServiceType
        {
            public string serviceSystemId { get; set; }
            public string serviceCode { get; set; }
            public string serviceCodeType { get; set; }
            public List<string> modifierCode { get; set; }
            public string placeOfService { get; set; }
            public string referralCategoryType { get; set; }
            public ServiceType serviceType { get; set; }
        }

        public class Diagnosis
        {
            public string diagnosisSystemId { get; set; }
            public string diagnosisType { get; set; }
            public string diagnosisCode { get; set; }
            public string diagnosisCodeType { get; set; }
        }

        public class ServiceAgreementDetail
        {
            public string clearAgreementDetail { get; set; }
            public string overrideExclusions { get; set; }
            public string promoteToInNetwork { get; set; }
            public string payAtMaximum { get; set; }
            public string extendLimit { get; set; }
            public string coverTerminatedMember { get; set; }
            public string reimbursementMethod { get; set; }
            public string amount { get; set; }
            public string typeOfFlatFee { get; set; }
            public string percentageBilled { get; set; }
        }

        public class HicApproval
        {
            public string authorizedBySystemUser { get; set; }
            public string authorizedDate { get; set; }
            public AuthorizedByExternalUser authorizedByExternalUser { get; set; }
        }

        public class AuthorizedByExternalUser
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class ReviewType
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class ReviewLevel
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class AuthorizationServiceSetMatchInput
        {
            public string authorizationId { get; set; }
            public string externalServiceSetId { get; set; }
            public string serviceStartDate { get; set; }
            public string serviceEndDate { get; set; }
            public string serviceCode { get; set; }
            public string serviceCodeType { get; set; }
            public string referralCategoryType { get; set; }
            public string serviceSetStatus { get; set; }
        }

        public class TypeOfBill
        {
            public string ID { get; set; }
        }

        public class MemberMatchInput
        {
            public string definitionName { get; set; }
            public string id { get; set; }
            public string topAccount { get; set; }
            public string customString1 { get; set; }
            public string customString2 { get; set; }
            public string customString3 { get; set; }
            public string customString4 { get; set; }
            public string customInteger1 { get; set; }
            public string customInteger2 { get; set; }
            public string customInteger3 { get; set; }
            public string customInteger4 { get; set; }
            public string customDate1 { get; set; }
            public string customDate2 { get; set; }
            public string customDate3 { get; set; }
            public string customDate4 { get; set; }
            public string customDecimal1 { get; set; }
            public string customDecimal2 { get; set; }
            public string customDecimal3 { get; set; }
            public string customDecimal4 { get; set; }
            public Member member { get; set; }
            public Subscriber subscriber { get; set; }
            public string asOfDate { get; set; }
        }

        public class Member
        {
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string fullName { get; set; }
            public string gender { get; set; }
            public string dateOfBirth { get; set; }
            public string taxId { get; set; }
            public Address address { get; set; }
        }

        public class Subscriber
        {
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string fullName { get; set; }
            public string gender { get; set; }
            public string dateOfBirth { get; set; }
            public string taxId { get; set; }
            public Address address { get; set; }
        }

        public class AuthorizationMatchInput
        {
            public string authorizationId { get; set; }
            public string externalAuthorizationId { get; set; }
        }

        public class ClinicalNotes
        {
            public string noteID { get; set; }
            public string noteContent { get; set; }
            public string reviewerFirstName { get; set; }
            public string reviewerLastName { get; set; }
            public List<NoteAttachments> noteAttachments { get; set; }
        }

        public class NoteAttachments
        {
            public string externalReferenceId { get; set; }
            public string attachmentContent { get; set; }
            public string attachmentName { get; set; }
            public string attachmentDescription { get; set; }
            public string attachmentSize { get; set; }
            public string attachmentType { get; set; }
        }

        public class UserDefinedTerm
        {
            public string ID { get; set; }
        }

        public class Udt
        {
            public string attributeRoleName { get; set; }
            public string attrValueAsString { get; set; }
        }

        public class AuthorizationAttachments
        {
            public string externalReferenceId { get; set; }
            public string attachmentContent { get; set; }
            public string attachmentName { get; set; }
            public string attachmentDescription { get; set; }
            public string attachmentSize { get; set; }
            public string attachmentType { get; set; }
        }

    }
}

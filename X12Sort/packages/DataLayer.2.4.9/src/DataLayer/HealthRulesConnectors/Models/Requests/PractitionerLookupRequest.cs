using System;
using System.Collections.Generic;
using System.Xml;
using DataLayer.StaticUtilities;

namespace DataLayer.HealthRulesConnectors.Models.Requests
{
    public class PractitionerLookupRequest
    {

        public string Get()
        {
            
            string xml = resRequests.PractitionerLookup;
            xml = xml.Replace("<!--Optional:-->", "");
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            doc.SelectSingleNode("//onlyPCP").InnerText = onlyPCP ? "true" : "";
            if (asOfDate != new DateTime(1800, 1, 1))
                doc.SelectSingleNode("//asOfDate").InnerText = asOfDate.ToString("yyyy-MM-dd");
            else
                doc.SelectSingleNode("//asOfDate").InnerText = "";
            doc.SelectSingleNode("//includeTerminated").InnerText = includeTerminated ? "true" : "";
            if (endorsementEffectiveDate != new DateTime(1800, 1, 1))
                doc.SelectSingleNode("//endorsementEffectiveDate").InnerText = endorsementEffectiveDate.ToString("yyyy-MM-dd");
            else
                doc.SelectSingleNode("//endorsementEffectiveDate").InnerText = "";
            foreach (string indent in practitionerReferenceId)
            {
                XmlElement elem = doc.CreateElement("identifier");
                elem.InnerText = indent;
                doc.SelectSingleNode("//practitionerReferenceId/identifier").AppendChild(elem);
            }
            doc.SelectSingleNode("//practitionerId").InnerText = practitionerId;
            doc.SelectSingleNode("//practitionerFirstName").InnerText = practitionerFirstName;
            doc.SelectSingleNode("//practitionerLastName").InnerText = practitionerLastName;


            string output = XMLSerialization.RemoveEmptyTags(doc.InnerXml);
            // Header tag is empty so it is removed by the above operation.  This puts it back in.
            output = output.Replace("<soapenv:Body>", "<soapenv:Header/>\r\n<soapenv:Body>");

            return output;


        }

        /// <summary>
        /// Only PCP
        /// </summary>
        public bool onlyPCP = false;

        /// <summary>
        /// As of date
        /// </summary>
        public DateTime asOfDate = new DateTime(1800, 1, 1);

        /// <summary>
        /// Include terminated
        /// </summary>
        public bool includeTerminated = false;

        /// <summary>
        /// Endorsement Effective Date
        /// </summary>
        public DateTime endorsementEffectiveDate = new DateTime(1800, 1, 1);

        /// <summary>
        /// Practitioner Reference Id
        /// </summary>
        public List<string> practitionerReferenceId = new List<string>();

        /// <summary>
        /// Practitioner ID
        /// </summary>
        public string practitionerId { get; set; }

        /// <summary>
        /// Practitioner First Name
        /// </summary>
        public string practitionerFirstName { get; set; }

        /// <summary>
        /// Practitioner Last Name
        /// </summary>
        public string practitionerLastName { get; set; }

    }


}

using System;
using System.Collections.Generic;
using System.Text;
using DataLayer.StaticUtilities;

namespace DataLayer.HealthRulesConnectors.Models.Requests
{
    public class AuthorizationLookupRequest
    {
        /// <summary>
        /// Generate the serialized response object
        /// </summary>
        /// <returns></returns>
        public string Get()
        {
            // Validation goes here


            // Serialize response and send
            return XMLSerialization.BuildRequestObject(this, resRequests.AuthorizationLookup);
        }

        public string authorizationId { get; set; }
        public string externalAuthorizationId { get; set; }
        public string asOfDate { get; set; }
    }
}

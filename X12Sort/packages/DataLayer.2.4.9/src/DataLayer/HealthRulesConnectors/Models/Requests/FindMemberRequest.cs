using System;
using DataLayer.StaticUtilities;

namespace DataLayer.HealthRulesConnectors.Models.Requests
{
    public class FindMemberRequest
    {
        /// <summary>
        /// Generate the serialized response object
        /// </summary>
        /// <returns></returns>
        public string Get()
        {
            // Validate at least 1 value must be entered
            if (
                string.IsNullOrWhiteSpace(criteria.benefitPlanName)
                && string.IsNullOrWhiteSpace(criteria.hccBenefitPlanID)
                && string.IsNullOrWhiteSpace(criteria.benefitPlanType)
                && string.IsNullOrWhiteSpace(criteria.asOfDate)
                )
                throw new Exception("At least 1 value must be entered");

            // Serialize response and send
            return XMLSerialization.BuildRequestObject(this, resRequests.FindMembers);
        }

        public Criteria criteria = new Criteria();

        public class Criteria
        {
            public string benefitPlanName { get; set; }
            public string hccBenefitPlanID { get; set; }
            public string benefitPlanType { get; set; }
            public string asOfDate { get; set; }
        }

    }
}

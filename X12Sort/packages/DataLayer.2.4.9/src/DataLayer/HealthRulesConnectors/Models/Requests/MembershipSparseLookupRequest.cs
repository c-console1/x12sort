using System;
using DataLayer.StaticUtilities;

namespace DataLayer.HealthRulesConnectors.Models.Requests
{
    public class MembershipSparseLookupRequest
    {

        public string Get()
        {
            // Validate request
            if (string.IsNullOrWhiteSpace(this.definitionName))
                throw new Exception("Field 'definitionName' must be defined");

            // Build request
            // * Wrap the raw serialized XML with a temproary or '<junk}' tag so a multiple root XML object errot is not the
            //   thrown on XML clean-up
            // * Run the XML cleanup routine
            // * Remove the temproary or 'junk' tag      
            string output = "<junk>" + XMLSerialization.Object2XML(this) + "</junk>"; // Serlize object, add temp wrapper tags
            output = XMLSerialization.RemoveEmptyTags(output); // Remove empty tag
            output = output.Replace("<junk>", "").Replace("</junk>", ""); // remove temp tags

            // * Put the XML output from above into a standard template loaded from the 'resRequests.resx' resource file
            string xmlTemplate = resRequests.MembershipLookupSparse; // Load template
            output = string.Format(xmlTemplate, output); // Merge generated XML into template

            // * Return request objects
            return output;
        }

        // TBD
        public string definitionName { get; set; }
        // TBD
        public string id { get; set; }
        // TBD
        public string topAccount { get; set; }
        // TBD
        public string customString1 { get; set; }
        // TBD
        public string customString2 { get; set; }
        // TBD
        public string customString3 { get; set; }
        // TBD
        public string customString4 { get; set; }
        // TBD
        public string customInteger1 { get; set; }
        // TBD
        public string customInteger2 { get; set; }
        // TBD
        public string customInteger3 { get; set; }
        // TBD
        public string customInteger4 { get; set; }
        // TBD
        public string customDate1 { get; set; }
        // TBD
        public string customDate2 { get; set; }
        // TBD
        public string customDate3 { get; set; }
        // TBD
        public string customDate4 { get; set; }
        // TBD
        public string customDecimal1 { get; set; }
        // TBD
        public string customDecimal2 { get; set; }
        // TBD
        public string customDecimal3 { get; set; }
        // TBD
        public string customDecimal4 { get; set; }
        // TBD
        public Member member { get; set; }
        // TBD
        public Subscriber subscriber { get; set; }
        // TBD
        public string asOfDate { get; set; }


        // TBD
        public class Member
        {
            // TBD
            public string firstName { get; set; }
            // TBD
            public string lastName { get; set; }
            // TBD
            public string fullName { get; set; }
            // TBD
            public string gender { get; set; }
            // TBD
            public string dateOfBirth { get; set; }
            // TBD
            public string taxId { get; set; }
            // TBD
            public Address address { get; set; }
        }

        // TBD
        public class Address
        {
            // TBD
            public Address address { get; set; }
            // TBD
            public string cityName { get; set; }
            // TBD
            public string stateCode { get; set; }
            // TBD
            public string zipCode { get; set; }
            // TBD
            public string zipExtensionCode { get; set; }
            // TBD
            public string countryCode { get; set; }
        }

        // TBD
        public class Subscriber
        {
            // TBD
            public string firstName { get; set; }
            // TBD
            public string lastName { get; set; }
            // TBD
            public string fullName { get; set; }
            // TBD
            public string gender { get; set; }
            // TBD
            public string dateOfBirth { get; set; }
            // TBD
            public string taxId { get; set; }
            // TBD
            public Address address { get; set; }
        }

    }
}

using System;
using System.Collections.Generic;
using DataLayer.StaticUtilities;

namespace DataLayer.HealthRulesConnectors.Models.Requests
{
    public class MemberCoverageEventLookupRequest
    {

        public string Get()
        {
            // Validate Start Date
            if (string.IsNullOrWhiteSpace(startDate))
                throw new Exception("Field 'Start Date' must be defined");

            // Validate Event Type
            if (eventsTypeList.Count == 0)
                throw new Exception("You must enter at least 1 Event Type");

            // Validate Reason codes
            for (int i = 0; i < reasonCodeList.Count; i++)
            {
                if (string.IsNullOrWhiteSpace(reasonCodeList[i].messageCode))
                    throw new Exception( string.Format("Reason Code #{0} is missing the Message Code", i) );

                if (string.IsNullOrWhiteSpace(reasonCodeList[i].codeDomainName))
                    throw new Exception(string.Format("Reason Code #{0} is missing the Code Domain Name", i));

                if (string.IsNullOrWhiteSpace(reasonCodeList[i].messageDescription))
                    throw new Exception(string.Format("Reason Code #{0} is missing the Message Description", i));
            }

            // Build request
            // * Wrap the raw serialized XML with a temproary or '<junk}' tag so a multiple root XML object errot is not the
            //   thrown on XML clean-up
            // * Run the XML cleanup routine
            // * Remove the temproary or 'junk' tag
            string output = "<junk>" + XMLSerialization.Object2XML(this) + "</junk>"; // Serlize object, add temp wrapper tags
            output = XMLSerialization.RemoveEmptyTags(output); // Remove empty tag
            output = output.Replace("<junk>", "").Replace("</junk>", ""); // remove temp tags

            // * Put the XML output from above into a standard template loaded from the 'resRequests.resx' resource file
            string xmlTemplate = resRequests.MemberCoverageEventLookup; // Load template
            output = string.Format(xmlTemplate, output); // Merge generated XML into template

            // * Return request objects
            return output;
        }


        public string startDate { get; set; }
        public string startTime { get; set; }
        public string endDate { get; set; }
        public string endTime { get; set; }
        public string exchangeState { get; set; }
        public List<string> eventsTypeList = new List<string>();
        public List<ReasonCodeList> reasonCodeList = new List<ReasonCodeList>();

        public class ReasonCodeList
        {
            public string messageCode { get; set; }
            public string codeDomainName { get; set; }
            public string messageDescription { get; set; }
            public string policyName { get; set; }
            public string messageAction { get; set; }
            public string messageResponsibility { get; set; }
        }

    }
}

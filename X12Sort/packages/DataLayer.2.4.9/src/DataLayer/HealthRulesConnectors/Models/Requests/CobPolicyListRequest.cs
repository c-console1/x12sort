using System;
using System.Collections.Generic;
using DataLayer.StaticUtilities;

namespace DataLayer.HealthRulesConnectors.Models.Requests
{
    public class CobPolicyListRequest
    {
        public string Get()
        {
            // Validate at least 1 value must be entered
            if (string.IsNullOrWhiteSpace(cobPolicyList.memberHccID))
                throw new Exception("'cobPolicyList' is not set");

            if (cobPolicyList.cobPolicy.Count < 0)
                throw new Exception("At least 1 'cobPolicy' object must be set");

            foreach(CobPolicy item in cobPolicyList.cobPolicy)
            {
                if(item.dateRanges.Count == 0)
                    throw new Exception("At least 1 'dateRanges' object must be set for the 'cobPolicy'");

                foreach(DateRanges dateRange in item.dateRanges)
                {
                    if (string.IsNullOrWhiteSpace(dateRange.startDate))
                        throw new Exception("The 'startDate' value in 'dateRanges' cannot be null");

                    if (string.IsNullOrWhiteSpace(dateRange.endDate))
                        throw new Exception("The 'endDate' value in 'dateRanges' cannot be null");
                }

                //if(string.IsNullOrWhiteSpace(item.subscriberName.lastName))
                //    throw new Exception("The 'lastName' value in 'cobPolicyList' cannot be null");

            }

            // Build request
            // * Wrap the raw serialized XML with a temproary or '<junk}' tag so a multiple root XML object errot is not the
            //   thrown on XML clean-up
            // * Run the XML cleanup routine
            // * Remove the temproary or 'junk' tag
            string output = "<junk>" + XMLSerialization.Object2XML(this) + "</junk>"; // Serlize object, add temp wrapper tags
            output = XMLSerialization.RemoveEmptyTags(output); // Remove empty tag
            output = output.Replace("<junk>", "").Replace("</junk>", ""); // remove temp tags

            // * Put the XML output from above into a standard template loaded from the 'resRequests.resx' resource file
            string xmlTemplate = resRequests.CobPolicy;
            output = string.Format(xmlTemplate, output);

            // * Return request objects
            return output;
        }

        public CobPolicyList cobPolicyList { get; set; }


        public class CobPolicyList
        {
            public string memberHccID { get; set; }
            public List<CobPolicy> cobPolicy { get; set; }
        }

        public class CobPolicy
        {
            public string cobPolicyIdentifier { get; set; }
            public string cobPolicyId { get; set; }
            public LeadSourceForOtherInsuranceCode leadSourceForOtherInsuranceCode { get; set; }
            public RelationshipToSubscriberDefinitionReference relationshipToSubscriberDefinitionReference { get; set; }
            public string otherInsuranceCompanyName { get; set; }
            public string policyType { get; set; }
            public string benefitPlanName { get; set; }
            public string groupNumber { get; set; }
            public string memberId { get; set; }
            public BenefitPlanTypeCode benefitPlanTypeCode { get; set; }
            public List<DateRanges> dateRanges { get; set; }
            public List<SubscriberIdList> subscriberIdList { get; set; }
            public SubscriberName subscriberName { get; set; }
            public RxInfo rxInfo { get; set; }
            public CorrespondenceInfo correspondenceInfo { get; set; }
            public List<CarrierIds> carrierIds { get; set; }
        }

        public class LeadSourceForOtherInsuranceCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class RelationshipToSubscriberDefinitionReference
        {
            public string ID { get; set; }
        }

        public class BenefitPlanTypeCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class DateRanges
        {
            public string startDate { get; set; }
            public string endDate { get; set; }
            public string responsibilitySequenceCode { get; set; }
            public OBDReasonCode OBDReasonCode { get; set; }
        }

        public class OBDReasonCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class SubscriberIdList
        {
            public string identificationNumber { get; set; }
            public string issuingState { get; set; }
            public IssuingCountry issuingCountry { get; set; }
            public string effectiveStartDate { get; set; }
            public string effectiveEndDate { get; set; }
            public IdentificationTypeCode identificationTypeCode { get; set; }
        }

        public class IssuingCountry
        {
            public CountryCode countryCode { get; set; }
        }

        public class IdentificationTypeCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class SubscriberName
        {
            public string lastName { get; set; }
            public string firstName { get; set; }
            public string middleName { get; set; }
            public string namePrefix { get; set; }
            public string nameSuffix { get; set; }
            public NameSuffixList nameSuffixList { get; set; }
            public CredentialSuffixList credentialSuffixList { get; set; }
        }

        public class NameSuffixList
        {
            public string listMode { get; set; }
            public List<string> suffix { get; set; }
        }

        public class CredentialSuffixList
        {
            public string listMode { get; set; }
            public List<CredentialSuffix> CredentialSuffix { get; set; }
        }

        public class CredentialSuffix
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class RxInfo
        {
            public string rxBin { get; set; }
            public string pcn { get; set; }
            public string rxGroup { get; set; }
            public string rxId { get; set; }
            public string rxPhoneNumber { get; set; }
        }

        public class CorrespondenceInfo
        {
            public string emailAddress { get; set; }
            public AddressInfo addressInfo { get; set; }
            public SupplierCorrespondenceUsageCode supplierCorrespondenceUsageCode { get; set; }
            public SupplierLocationCorrespondenceUsageCode supplierLocationCorrespondenceUsageCode { get; set; }
            public AccountCorrespondenceUsageCode accountCorrespondenceUsageCode { get; set; }
        }

        public class AddressInfo
        {
            public PostalAddress postalAddress { get; set; }
            public List<AddressPhoneList> addressPhoneList { get; set; }
        }

        public class PostalAddress
        {
            public string address { get; set; }
            public string address2 { get; set; }
            public string address3 { get; set; }
            public string stateCode { get; set; }
            public string zipCode { get; set; }
            public string zipExtensionCode { get; set; }
            public string cityName { get; set; }
            public string countyCode { get; set; }
            public CountryCode countryCode { get; set; }
            public string longitude { get; set; }
            public string latitude { get; set; }
            public string ignoreAddressCheck { get; set; }
            public string undeliverableAddress { get; set; }
        }

        public class CountryCode
        {
            public CountryCode countryCode { get; set; }
        }

        public class AddressPhoneList
        {
            public string phoneCountryCode { get; set; }
            public string phoneAreaCode { get; set; }
            public string phoneNumber { get; set; }
            public string phoneExtensionNumber { get; set; }
            public IndividualPhoneTypeCode individualPhoneTypeCode { get; set; }
            public OrganizationPhoneTypeCode organizationPhoneTypeCode { get; set; }
        }

        public class IndividualPhoneTypeCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class OrganizationPhoneTypeCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class SupplierCorrespondenceUsageCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class SupplierLocationCorrespondenceUsageCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class AccountCorrespondenceUsageCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class CarrierIds
        {
            public string identificationNumber { get; set; }
            public string issuingState { get; set; }
            public IssuingCountry issuingCountry { get; set; }
            public string effectiveStartDate { get; set; }
            public string effectiveEndDate { get; set; }
            public IdentificationTypeCode identificationTypeCode { get; set; }
        }

    }



}

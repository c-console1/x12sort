using System;
using System.Collections.Generic;
using System.Text;
using DataLayer.StaticUtilities;

namespace DataLayer.HealthRulesConnectors.Models.Requests
{
    public class AccountLookupRequest
    {

        /// <summary>
        /// Generate the serialized response object
        /// </summary>
        /// <returns></returns>
        public string Get()
        {
            // Validation goes here


            // Serialize response and send
            return XMLSerialization.BuildRequestObject(this, resRequests.AccountLookup);
        }

        public AccountIdentifier accountIdentifier { get; set; }
        public ParentAccountIdentifier parentAccountIdentifier { get; set; }
        public TopAccountIdentifier topAccountIdentifier { get; set; }
        public string taxId { get; set; }
        public string asOfDate { get; set; }
        public string otherIdentificationNumber { get; set; }
        public IdentificationType identificationType { get; set; }

        public class AccountIdentifier
        {
            public string hccIdentificationNumber { get; set; }
            public string referenceIdentificationNumber { get; set; }
            public string name { get; set; }
        }

        public class ParentAccountIdentifier
        {
            public string hccIdentificationNumber { get; set; }
            public string name { get; set; }
        }

        public class TopAccountIdentifier
        {
            public string hccIdentificationNumber { get; set; }
            public string referenceIdentificationNumber { get; set; }
            public string name { get; set; }
        }

        public class IdentificationType
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

    }
}

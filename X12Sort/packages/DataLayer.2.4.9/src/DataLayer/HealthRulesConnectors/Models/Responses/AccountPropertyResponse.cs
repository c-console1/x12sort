using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.HealthRulesConnectors.Models.Responses
{
    public class AccountPropertyResponse
    {

        public class Response
        {
            public InnerEnvelope Envelope { get; set; }
        }

        public class InnerEnvelope
        {
            public string Header { get; set; }
            public Body Body { get; set; }
        }

        public class Body
        {
            public AccountProperty accountProperty { get; set; }
        }

        public class AccountProperty
        {
            public MaintenanceInfo maintenanceInfo { get; set; }
            public string maintenanceComment { get; set; }
            public MaintenanceReasonCode maintenanceReasonCode { get; set; }
            public AccountPropertyChild accountProperty { get; set; }
        }

        public class MaintenanceInfo
        {
            public string transactionType { get; set; }
        }

        public class MaintenanceReasonCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class AccountPropertyChild
        {
            public Account account { get; set; }
            public BillingHoldReasonCode billingHoldReasonCode { get; set; }
        }

        public class Account
        {
            public string accountHccIdentificationNumber { get; set; }
        }

        public class BillingHoldReasonCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

    }
}

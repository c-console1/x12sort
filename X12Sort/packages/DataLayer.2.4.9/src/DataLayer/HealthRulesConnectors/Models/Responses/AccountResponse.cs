using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.HealthRulesConnectors.Models.Responses
{
    public class AccountResponse
    {


        public class Response
        {
            public InnerEnvelope Envelope { get; set; }
        }


        public class InnerEnvelope
        {
            public string Header { get; set; }
            public Body Body { get; set; }
        }

        public class Body
        {
            public AccountsResponse accountsResponse { get; set; }
        }

        public class AccountsResponse
        {
            public string status { get; set; }
            public List<Account> account { get; set; }
            public Errors errors { get; set; }
            public TransactionInformation transactionInformation { get; set; }
        }

        public class Account
        {
            public string accountIdentifier { get; set; }
            public string accountOldHccIdentifier { get; set; }
            public string accountHccIdentifier { get; set; }
            public Organization organization { get; set; }
            public TypeOfAccount typeOfAccount { get; set; }
            public VipReason vipReason { get; set; }
            public string canHaveSubscriptions { get; set; }
            public IndustryClassification industryClassification { get; set; }
            public string employeeCount { get; set; }
            public string eligibleEmployeeCount { get; set; }
            public string description { get; set; }
            public string effectiveStartDate { get; set; }
            public AccountIdList accountIdList { get; set; }
            public Regions regions { get; set; }
            public List<AccountUDTList> accountUDTList { get; set; }
            public string billingCycleIdentifier { get; set; }
            public AccountAnniversaryDate accountAnniversaryDate { get; set; }
            public GeneralCorrespondenceAddress generalCorrespondenceAddress { get; set; }
            public TopAccount topAccount { get; set; }
            public ParentAccount parentAccount { get; set; }
            public string billParent { get; set; }
            public string accountRepresentativeUserName { get; set; }
            public string doNotAutoAllocateBills { get; set; }
            public List<PremiumPaymentAutoAllocationOP> premiumPaymentAutoAllocationOP { get; set; }
        }

        public class Organization
        {
            public string name { get; set; }
            public string taxID { get; set; }
            public List<OtherOrganizationName> otherOrganizationName { get; set; }
        }

        public class OtherOrganizationName
        {
            public string organizationName { get; set; }
            public OtherNameType otherNameType { get; set; }
        }

        public class OtherNameType
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class TypeOfAccount
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class VipReason
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class IndustryClassification
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class AccountIdList
        {
            public List<AccountOtherID> accountOtherID { get; set; }
        }

        public class AccountOtherID
        {
            public string identificationNumber { get; set; }
            public IdentificationType identificationType { get; set; }
            public string issuingState { get; set; }
            public IssuingCountry issuingCountry { get; set; }
            public string effectiveStartDate { get; set; }
            public string effectiveEndDate { get; set; }
        }

        public class IdentificationType
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class IssuingCountry
        {
            public CountryCode countryCode { get; set; }
        }

        public class Regions
        {
            public List<Region> region { get; set; }
        }

        public class Region
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class AccountUDTList
        {
            public List<UdtListValueSet> udtListValueSet { get; set; }
            public UserDefinedTermReference userDefinedTermReference { get; set; }
        }

        public class UdtListValueSet
        {
            public string attributeRoleName { get; set; }
            public string attrValueAsString { get; set; }
        }

        public class UserDefinedTermReference
        {
            public string ID { get; set; }
        }

        public class AccountAnniversaryDate
        {
            public string firstRenewalDate { get; set; }
            public string renewalInterval { get; set; }
        }

        public class GeneralCorrespondenceAddress
        {
            public string emailAddress { get; set; }
            public AddressInformation addressInformation { get; set; }
            public CorrespondenceUsageType correspondenceUsageType { get; set; }
            public AccountContactList accountContactList { get; set; }
        }

        public class AddressInformation
        {
            public PostalAddress postalAddress { get; set; }
            public AddressPhoneList addressPhoneList { get; set; }
        }

        public class PostalAddress
        {
            public string address { get; set; }
            public string address2 { get; set; }
            public string address3 { get; set; }
            public string stateCode { get; set; }
            public string zipCode { get; set; }
            public string zipExtensionCode { get; set; }
            public string cityName { get; set; }
            public string countyCode { get; set; }
            public CountryCode countryCode { get; set; }
            public string longitude { get; set; }
            public string latitude { get; set; }
            public string ignoreAddressCheck { get; set; }
            public string undeliverableAddress { get; set; }
        }

        public class CountryCode
        {
            public CountryCode countryCode { get; set; }
        }

        public class AddressPhoneList
        {
            public string listMode { get; set; }
            public List<TelephoneNumber> telephoneNumber { get; set; }
        }

        public class TelephoneNumber
        {
            public string phoneCountryCode { get; set; }
            public string phoneAreaCode { get; set; }
            public string phoneNumber { get; set; }
            public string phoneExtensionNumber { get; set; }
            public IndividualPhoneTypeCode individualPhoneTypeCode { get; set; }
            public OrganizationPhoneTypeCode organizationPhoneTypeCode { get; set; }
        }

        public class IndividualPhoneTypeCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class OrganizationPhoneTypeCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class CorrespondenceUsageType
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class AccountContactList
        {
            public List<ContactList> contactList { get; set; }
        }

        public class ContactList
        {
            public string title { get; set; }
            public string emailAddress { get; set; }
            public PersonName personName { get; set; }
            public TelephoneList telephoneList { get; set; }
            public UsageType usageType { get; set; }
        }

        public class PersonName
        {
            public string lastName { get; set; }
            public string firstName { get; set; }
            public string middleName { get; set; }
            public string namePrefix { get; set; }
            public string nameSuffix { get; set; }
            public NameSuffixList nameSuffixList { get; set; }
            public CredentialSuffixList credentialSuffixList { get; set; }
        }

        public class NameSuffixList
        {
            public string listMode { get; set; }
            public List<string> suffix { get; set; }
        }

        public class CredentialSuffixList
        {
            public string listMode { get; set; }
            public List<CredentialSuffix> CredentialSuffix { get; set; }
        }

        public class CredentialSuffix
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class TelephoneList
        {
            public string listMode { get; set; }
            public List<TelephoneNumber> telephoneNumber { get; set; }
        }

        public class UsageType
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class TopAccount
        {
            public string accountHccIdentifier { get; set; }
        }

        public class ParentAccount
        {
            public string accountHccIdentifier { get; set; }
        }

        public class PremiumPaymentAutoAllocationOP
        {
            public string billingParty { get; set; }
            public string allocationOrder { get; set; }
            public string allocateEvenly { get; set; }
            public List<ByPlanTypeAndBillingCategory> byPlanTypeAndBillingCategory { get; set; }
        }

        public class ByPlanTypeAndBillingCategory
        {
            public BenefitPlanType benefitPlanType { get; set; }
            public List<BillingCategories> billingCategories { get; set; }
        }

        public class BenefitPlanType
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class BillingCategories
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class Errors
        {
            public List<Error> error { get; set; }
        }

        public class Error
        {
            public string errorType { get; set; }
            public string message { get; set; }
            public string dateTime { get; set; }
            public string authenticatedUser { get; set; }
            public string traceInfo { get; set; }
        }

        public class TransactionInformation
        {
            public string alreadyInWorkbasket { get; set; }
            public string blockedByLock { get; set; }
            public string cvcId { get; set; }
            public List<Exceptions> exceptions { get; set; }
            public string lockingUser { get; set; }
            public string needsRepair { get; set; }
            public string needsReview { get; set; }
            public string rejected { get; set; }
            public string transactionSubmitted { get; set; }
            public string versionEffectiveDate { get; set; }
        }

        public class Exceptions
        {
            public string exceptionType { get; set; }
            public string messageCode { get; set; }
            public string messageDescription { get; set; }
            public string policyName { get; set; }
        }

    }
}

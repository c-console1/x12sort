using System.Collections.Generic;

namespace DataLayer.HealthRulesConnectors.Models.Responses
{
    public class CobPolicyLookupResponse
    {

        public class Response
        {
            public EnvelopeInner Envelope { get; set; }

            public string memberHccId
            {
                get
                {

                    if (Envelope != null)
                    {
                        if (Envelope.Body != null)
                        {
                            if (Envelope.Body.cobPolicyLookupListResponse != null)
                            {
                                return Envelope.Body.cobPolicyLookupListResponse.memberHccId;
                            }
                        }
                    }
                    return null;
                }
            }

            public List<string> cobPolicyId
            {
                get
                {
                    if (Envelope != null)
                    {
                        if (Envelope.Body != null)
                        {
                            if (Envelope.Body.cobPolicyLookupListResponse != null)
                            {
                                return Envelope.Body.cobPolicyLookupListResponse.cobPolicyId;
                            }
                        }
                    }

                    return null;
                }
            }



        }

        public class EnvelopeInner
        {
            public string Header { get; set; }
            public Body Body { get; set; }
        }

        public class Body
        {
            public CobPolicyLookupListResponse cobPolicyLookupListResponse { get; set; }
        }

        public class CobPolicyLookupListResponse
        {
            public string memberHccId { get; set; }
            public List<string> cobPolicyId { get; set; }
            public string asOfDate { get; set; }
            public string status { get; set; }
            public Errors errors { get; set; }
        }

        public class Errors
        {
            public List<Error> error { get; set; }
        }

        public class Error
        {
            public string errorType { get; set; }
            public string message { get; set; }
            public string dateTime { get; set; }
            public string authenticatedUser { get; set; }
            public string traceInfo { get; set; }
        }

    }
}

using System.Collections.Generic;

namespace DataLayer.HealthRulesConnectors.Models.Responses
{

    public class PractitionerLookup
    {

        public class Response
        {
            public EnvelopeInner Envelope { get; set; }
        }

        public class PractitionerCredential
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
        }

        public class PractitionerIdDetail
        {
            public string identificationNumber { get; set; }
            public string issuingState { get; set; }
        }

        public class CountryCode
        {
            public string countryCode { get; set; }
        }

        public class CorrespondenceAddress2
        {
            public string address { get; set; }
            public string zipCode { get; set; }
            public string zipExtensionCode { get; set; }
            public string cityName { get; set; }
            public string countyCode { get; set; }
            public CountryCode countryCode { get; set; }
        }

        public class PhoneNumber
        {
            public string phoneAreaCode { get; set; }
            public string phoneNumber { get; set; }
        }

        public class CorrespondenceAddress
        {
            public CorrespondenceAddress2 correspondenceAddress { get; set; }
            public PhoneNumber phoneNumber { get; set; }
        }

        public class PrimarySpecialty
        {
            public string specialtyCode { get; set; }
            public string specialtyName { get; set; }
        }

        public class PractitionerDetail
        {
            public string hccID { get; set; }
            public string nationalProviderId { get; set; }
            public string pcp { get; set; }
            public string isAutoAssignedAsPCP { get; set; }
            public string excludeFromProviderDir { get; set; }
            public string endorsementEffectiveDate { get; set; }
            public string lastName { get; set; }
            public string firstName { get; set; }
            public string middleName { get; set; }
            public string namePrefix { get; set; }
            public string nameSuffix { get; set; }
            public string genderCode { get; set; }
            public string ethnicityCode { get; set; }
            public string primaryLanguage { get; set; }
            public string dateOfBirth { get; set; }
            public string activeCode { get; set; }
            public string firstActiveDate { get; set; }
            public PractitionerCredential practitionerCredential { get; set; }
            public List<PractitionerIdDetail> practitionerIdDetail { get; set; }
            public CorrespondenceAddress correspondenceAddress { get; set; }
            public PrimarySpecialty primarySpecialty { get; set; }
            public string practitionerCredentialing { get; set; }
        }

        public class PractitionerLookupListResponse
        {
            public PractitionerDetail practitionerDetail { get; set; }
            public string status { get; set; }
        }

        public class Body
        {
            public PractitionerLookupListResponse practitionerLookupListResponse { get; set; }
            public Fault Fault { get; set; }

        }

        public class EnvelopeInner
        {
            public Body Body { get; set; }

        }

    }

}

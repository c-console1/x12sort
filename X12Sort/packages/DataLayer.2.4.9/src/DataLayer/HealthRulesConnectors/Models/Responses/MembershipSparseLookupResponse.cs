using System.Collections.Generic;

namespace DataLayer.HealthRulesConnectors.Models.Responses
{
    public class MembershipSparseLookupResponse
    {

        public class Response
        {
            public EnvelopeInner Envelope { get; set; }
        }

        public class EnvelopeInner
        {
            public Body Body { get; set; }
        }

        public class Body
        {
            public MembershipSparseLookupResponse2 membershipSparseLookupResponse { get; set; }
        }

        public class MembershipSparseLookupResponse2
        {
            public Membership membership { get; set; }
            public string status { get; set; }
            public Errors errors { get; set; }
        }

        public class Errors
        {
            public List<Error> error { get; set; }
        }

        public class Error
        {
            public string errorType { get; set; }
            public string message { get; set; }
            public string dateTime { get; set; }
            public string authenticatedUser { get; set; }
            public string traceInfo { get; set; }
        }


        public class PrimaryName
        {
            public string lastName { get; set; }
            public string firstName { get; set; }
            public string middleName { get; set; }
            public string namePrefix { get; set; }
            public string nameSuffix { get; set; }
            public NameSuffixList nameSuffixList { get; set; }
            public CredentialSuffixList credentialSuffixList { get; set; }
        }

        public class OtherNameInfo
        {
            public string lastName { get; set; }
            public string firstName { get; set; }
            public string middleName { get; set; }
            public string namePrefix { get; set; }
            public string nameSuffix { get; set; }
            public NameSuffixList nameSuffixList { get; set; }
            public CredentialSuffixList credentialSuffixList { get; set; }
        }

        public class OtherNameTypeCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
        }

        public class OtherName
        {
            public OtherNameInfo otherNameInfo { get; set; }
            public OtherNameTypeCode otherNameTypeCode { get; set; }
        }

        public class OtherNames
        {
            public List<OtherName> otherName { get; set; }
        }

        public class Individual
        {
            // TBD
            public string taxIdentificationNumber { get; set; }
            // TBD
            public string genderCode { get; set; }
            // TBD
            public string maritalStatusCode { get; set; }
            // TBD
            public string birthDate { get; set; }
            // TBD
            public string birthPlaceStateCode { get; set; }
            // TBD
            public string birthPlaceCountyCode { get; set; }
            // TBD
            public BirthPlaceCountryCode birthPlaceCountryCode { get; set; }
            // TBD
            public string dateOfDeath { get; set; }
            // TBD
            public PrimaryName primaryName { get; set; }
            // TBD
            public OtherNames otherNames { get; set; }
            // TBD
            public Languages languages { get; set; }
            // TBD
            public ImmigrationStatusCode immigrationStatusCode { get; set; }
        }

        // TBD
        public class Languages
        {
            // TBD
            // This may need to be converted to a <List> object
            public string listMode { get; set; }
            // The object below will most likely need to put into a <List>
            // TBD
            public List<Language> language { get; set; }
        }

        // TBD
        public class Language
        {
            // TBD
            public string primaryLanguage { get; set; }
            // TBD
            public string nativeLanguage { get; set; }
            // TBD
            public LanguageDomainCode languageDomainCode { get; set; }
        }
        public class LanguageDomainCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class BirthPlaceCountryCode
        {
            // TBD
            public CountryCode countryCode { get; set; }
        }

        public class CountryCode
        {
            public string countryCode { get; set; }
        }

        public class PostalAddress
        {
            public string address { get; set; }
            public string stateCode { get; set; }
            public string zipCode { get; set; }
            public string zipExtensionCode { get; set; }
            public string cityName { get; set; }
            public string countyCode { get; set; }
            public CountryCode countryCode { get; set; }
            public string longitude { get; set; }
            public string latitude { get; set; }
        }

        public class AddressInfo
        {
            public PostalAddress postalAddress { get; set; }
            public List<string> addressPhoneList { get; set; }
        }

        public class AddressTypeCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
        }

        public class MemberPhysicalAddress
        {
            public AddressInfo addressInfo { get; set; }
            public AddressTypeCode addressTypeCode { get; set; }
        }

        public class PhysicalAddress
        {
            public string listMode { get; set; }
            public List<MemberPhysicalAddress> memberPhysicalAddress { get; set; }
        }

        public class CountryCode2
        {
            public string countryCode { get; set; }
        }

        public class PostalAddress2
        {
            public string address { get; set; }
            public string stateCode { get; set; }
            public string zipCode { get; set; }
            public string zipExtensionCode { get; set; }
            public string cityName { get; set; }
            public string countyCode { get; set; }
            public CountryCode2 countryCode { get; set; }
            public string longitude { get; set; }
            public string latitude { get; set; }
        }

        public class CorrespondenceAddress
        {
            public PostalAddress2 postalAddress { get; set; }
        }

        public class IdentificationTypeCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
        }

        public class IdentificationNumber
        {
            // TBD
            public List<string> identificationNumber { get; set; }
            // TBD
            public string issuingState { get; set; }
            // TBD
            public IssuingCountry issuingCountry { get; set; }
            // TBD
            public string effectiveStartDate { get; set; }
            // TBD
            public string effectiveEndDate { get; set; }
            // TBD
            public IdentificationTypeCode identificationTypeCode { get; set; }
        }

        public class OtherIdNumberList
        {
            public string listMode { get; set; }
            public List<IdentificationNumber> identificationNumber { get; set; }
        }

        public class UdtListValueSet
        {
            public string attributeRoleName { get; set; }
            public string attrValueAsString { get; set; }
        }

        public class UserDefinedTermReference
        {
            public string ID { get; set; }
        }

        public class MembershipUDT
        {
            public List<UdtListValueSet> udtListValueSet { get; set; }
            public UserDefinedTermReference userDefinedTermReference { get; set; }
        }

        public class MembershipUDTList
        {
            public string listMode { get; set; }
            public List<MembershipUDT> membershipUDT { get; set; }
        }

        public class RelationshipToSubscriberDefinitionReference
        {
            public string relationshipName { get; set; }
        }

        public class AttachmentSetReference
        {
            public string ID { get; set; }
        }

        public class BenefitPlanMatchData
        {
            public string benefitPlanName { get; set; }
            public string benefitPlanHccId { get; set; }
        }

        public class BenefitPlanReference
        {
            public string identifier { get; set; }
        }

        public class PlanSelection
        {
            // TBD
            public string startDate { get; set; }
            // TBD
            public string endDate { get; set; }
            // TBD
            public string claimHistoryFromOtherPlans { get; set; }
            // TBD
            public EnrollmentInfo enrollmentInfo { get; set; }
            // TBD
            public MemberTerminateReasonCode memberTerminateReasonCode { get; set; }
            // TBD
            public SpecialEnrollmentPeriodReasonCode specialEnrollmentPeriodReasonCode { get; set; }
            // TBD
            public string waiveBenefitWaitingPeriod { get; set; }
            // TBD
            public string planDescription { get; set; }
            // TBD
            public string planCode { get; set; }
            // TBD
            public string classOfContractNumber { get; set; }
            // TBD
            public string programCode { get; set; }
            // TBD
            public BenefitPlanMatchData benefitPlanMatchData { get; set; }
            // TBD
            public BenefitPlanReference benefitPlanReference { get; set; }
            // TBD
            public string healthCoverageMaintenanceCode { get; set; }
            // TBD
            public string insuranceLineCode { get; set; }
            // TBD
            public BenefitNetworks benefitNetworks { get; set; }
        }

        public class Practitioner
        {
            public string hccIdentificationNumber { get; set; }
        }

        public class PractitionerRole
        {
            public string hccIdentificationNumber { get; set; }
            public string roleName { get; set; }
        }

        public class ProviderMatch
        {
            public Practitioner practitioner { get; set; }
            public PractitionerRole practitionerRole { get; set; }
        }

        public class PractitionerReference
        {
            public string identifier { get; set; }
        }

        public class PractitionerRoleReference
        {
            public string identifier { get; set; }
        }

        public class ProviderDateRanges
        {
            public string startDate { get; set; }
            public string endDate { get; set; }
            public ProviderMatch providerMatch { get; set; }
            public PractitionerReference practitionerReference { get; set; }
            public PractitionerRoleReference practitionerRoleReference { get; set; }
            public string pcpAutoAssigned { get; set; }
        }

        public class ProviderSelection
        {
            public string providerRoleType { get; set; }
            public List<ProviderDateRanges> providerDateRanges { get; set; }
        }

        public class ProviderSelections
        {
            public List<ProviderSelection> providerSelection { get; set; }
        }

        public class Membership
        {
            public string maintenanceTypeCode { get; set; }
            public MaintenanceReasonCode maintenanceReasonCode { get; set; }
            public string maintenanceComment { get; set; }
            public string maintenanceOverride { get; set; }
            public string maintenanceOverrideResult { get; set; }
            public string benefitStatusCode { get; set; }
            public string membershipIdentifier { get; set; }
            public string memberIsSubscriber { get; set; }
            public string hccIdentifier { get; set; }
            public string memberInputID { get; set; }
            public string receiptDate { get; set; }
            public string outOfServiceArea { get; set; }
            public string smokingStatus { get; set; }
            public string isHandicapped { get; set; }
            public string waivePHIClaimViewRestriction { get; set; }
            public string payeeHccId { get; set; }
            public string isMemberInHospice { get; set; }
            public MemberMatchData memberMatchData { get; set; }
            public OldMemberData oldMemberData { get; set; }
            public Individual individual { get; set; }
            public PhysicalAddress physicalAddress { get; set; }
            public CorrespondenceAddress correspondenceAddress { get; set; }
            public CommunicationPreferences communicationPreferences { get; set; }
            public List<SpecificCommunicationPreferences> specificCommunicationPreferences { get; set; }
            public OtherIdNumberList otherIdNumberList { get; set; }
            public DisabilityInfoForEligibility disabilityInfoForEligibility { get; set; }
            public List<DependentVerification> dependentVerification { get; set; }
            public PhysicalCharacteristics physicalCharacteristics { get; set; }
            public EmploymentInfo employmentInfo { get; set; }
            public DentalInfo dentalInfo { get; set; }
            public PriorCoverageInfo priorCoverageInfo { get; set; }
            public List<PreExistingConditionInfo> preExistingConditionInfo { get; set; }
            public PreExReduction preExReduction { get; set; }
            public List<OtherResponsiblePersonInfo> otherResponsiblePersonInfo { get; set; }
            public MemberRepresentative memberRepresentative { get; set; }
            public MembershipUDTList membershipUDTList { get; set; }
            public InformationAccessPermissionList informationAccessPermissionList { get; set; }
            public InformationAccessRestrictionList informationAccessRestrictionList { get; set; }
            public PhiAuthorizationList phiAuthorizationList { get; set; }
            public CcDirectiveList ccDirectiveList { get; set; }
            public SubscriptionReference subscriptionReference { get; set; }
            public IndividualReference individualReference { get; set; }
            public RelationshipToSubscriberDefinitionReference relationshipToSubscriberDefinitionReference { get; set; }
            public RaceOrEthnicity raceOrEthnicity { get; set; }
            public VipReasonCode vipReasonCode { get; set; }
            public HealthStatusReasonCode healthStatusReasonCode { get; set; }
            public InformationSourceCode informationSourceCode { get; set; }
            public AttachmentSetReference attachmentSetReference { get; set; }
            public string planSelectionListMode { get; set; }
            public List<PlanSelection> planSelection { get; set; }
            public ProviderSelections providerSelections { get; set; }
            public List<ReportingCategory> reportingCategory { get; set; }
            public List<MedicareHICN> medicareHICN { get; set; }
            public List<CobPolicy> cobPolicy { get; set; }
            public HeadOfHousehold headOfHousehold { get; set; }
            public MemberComplianceProgram memberComplianceProgram { get; set; }
            public EnrollmentMemberFactors enrollmentMemberFactors { get; set; }
            public AlternatePaymentContact alternatePaymentContact { get; set; }

        }

        public class MaintenanceReasonCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class MemberMatchData
        {
            public string definitionName { get; set; }
            public string id { get; set; }
            public string topAccount { get; set; }
            public string customString1 { get; set; }
            public string customString2 { get; set; }
            public string customString3 { get; set; }
            public string customString4 { get; set; }
            public string customInteger1 { get; set; }
            public string customInteger2 { get; set; }
            public string customInteger3 { get; set; }
            public string customInteger4 { get; set; }
            public string customDate1 { get; set; }
            public string customDate2 { get; set; }
            public string customDate3 { get; set; }
            public string customDate4 { get; set; }
            public string customDecimal1 { get; set; }
            public string customDecimal2 { get; set; }
            public string customDecimal3 { get; set; }
            public string customDecimal4 { get; set; }
            public Member member { get; set; }
            public Subscriber subscriber { get; set; }
            public string asOfDate { get; set; }
        }

        public class Member
        {
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string fullName { get; set; }
            public string gender { get; set; }
            public string dateOfBirth { get; set; }
            public string taxId { get; set; }
            public Address address { get; set; }
        }

        public class Address
        {
            public Address address { get; set; }
            public string cityName { get; set; }
            public string stateCode { get; set; }
            public string zipCode { get; set; }
            public string zipExtensionCode { get; set; }
            public CountryCode countryCode { get; set; }
        }

        public class Subscriber
        {
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string fullName { get; set; }
            public string gender { get; set; }
            public string dateOfBirth { get; set; }
            public string taxId { get; set; }
            public Address address { get; set; }
        }

        public class OldMemberData
        {
            public PrimaryName primaryName { get; set; }
            public string hccIdentifier { get; set; }
            public string taxIdentificationNumber { get; set; }
            public string genderCode { get; set; }
            public string maritalStatusCode { get; set; }
            public string birthDate { get; set; }
            public ImmigrationStatusCode immigrationStatusCode { get; set; }
            public Address address { get; set; }
        }

        public class ImmigrationStatusCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class CommunicationPreferences
        {
            public string contactMethod { get; set; }
            public DocumentDeliveryMethodCode documentDeliveryMethodCode { get; set; }
            public EmailFormatCode emailFormatCode { get; set; }
        }

        public class DocumentDeliveryMethodCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class EmailFormatCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class SpecificCommunicationPreferences
        {
            public string contactMethod { get; set; }
            public DocumentDeliveryMethodCode documentDeliveryMethodCode { get; set; }
            public string optOut { get; set; }
            public string correspondenceName { get; set; }
            public string recipient { get; set; }
        }

        public class DisabilityInfoForEligibility
        {
            public string disabilityTypeCode { get; set; }
            public string disabilityReportedDate { get; set; }
            public string verificationReceiptDate { get; set; }
            public DisabilityDenialReasonCode disabilityDenialReasonCode { get; set; }
            public DiagnosisReference diagnosisReference { get; set; }
        }

        public class DiagnosisReference
        {
            public string ID { get; set; }
        }

        public class DisabilityDenialReasonCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class DependentVerification
        {
            public string verificationType { get; set; }
            public string certLastInfoRequestDate { get; set; }
            public string certLastInfoReceiptDate { get; set; }
            public string certInformationDueDate { get; set; }
            public string lastVerificationDate { get; set; }
            public StudentSchoolInfo studentSchoolInfo { get; set; }
        }

        public class StudentSchoolInfo
        {
            public string studentStatusCode { get; set; }
            public string graduationDate { get; set; }
            public SchoolInformation schoolInformation { get; set; }
            public SchoolTypeCode schoolTypeCode { get; set; }
        }

        public class SchoolTypeCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class SchoolInformation
        {
            public string institutionName { get; set; }
            public ContactInfo contactInfo { get; set; }
        }

        public class ContactInfo
        {
            public string contactDescription { get; set; }
            public string contactEmailAddress { get; set; }
            public ContactName contactName { get; set; }
            public LocationAddress locationAddress { get; set; }
            public ContactTypeCode contactTypeCode { get; set; }
        }

        public class ContactName
        {
            public string lastName { get; set; }
            public string firstName { get; set; }
            public string middleName { get; set; }
            public string namePrefix { get; set; }
            public string nameSuffix { get; set; }
            public NameSuffixList nameSuffixList { get; set; }
            public CredentialSuffixList credentialSuffixList { get; set; }
        }

        public class LocationAddress
        {
            public PostalAddress postalAddress { get; set; }
            public List<AddressPhoneList> addressPhoneList { get; set; }
        }

        public class ContactTypeCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class PhysicalCharacteristics
        {
            public string heightInchesCount { get; set; }
            public string weightPoundsCount { get; set; }
        }

        public class EmploymentInfo
        {
            public string hireDate { get; set; }
            public string salaryInterval { get; set; }
            public string salaryPerInterval { get; set; }
            public string unionStatus { get; set; }
            public string positionOrTitle { get; set; }
            public string numberOfHoursWorked { get; set; }
            public string retirementDate { get; set; }
            public EmploymentStatusCodeCode employmentStatusCodeCode { get; set; }
            public SalaryGradeCodeCode salaryGradeCodeCode { get; set; }
            public UnitsDomainCode unitsDomainCode { get; set; }
            public EmployeeTypeDomainCode employeeTypeDomainCode { get; set; }
            public string department { get; set; }
        }

        public class EmploymentStatusCodeCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class SalaryGradeCodeCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class UnitsDomainCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class EmployeeTypeDomainCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class DentalInfo
        {
            public string remarks { get; set; }
            public MissingTeeth missingTeeth { get; set; }
        }

        public class MissingTeeth
        {
            public string listMode { get; set; }
            public List<string> toothName { get; set; }
        }

        public class PriorCoverageInfo
        {
            public string priorCoverageMonthCount { get; set; }
        }

        public class PreExistingConditionInfo
        {
            public string onsetOfConditionDate { get; set; }
            public string preexistingPeriodEndDate { get; set; }
            public string preexDescription { get; set; }
            public PreExDiagnosisCodeRangeConditions preExDiagnosisCodeRangeConditions { get; set; }
            public PreExProcedureCodeRangeConditions preExProcedureCodeRangeConditions { get; set; }
            public PreexistingConditions preexistingConditions { get; set; }
            public PreexistingConditionProcedures preexistingConditionProcedures { get; set; }
        }

        public class PreExDiagnosisCodeRangeConditions
        {
            public List<PreExDiagnosisCodeRangeCondition> preExDiagnosisCodeRangeCondition { get; set; }
        }

        public class PreExDiagnosisCodeRangeCondition
        {
            public RangeStartDiagnosisCode rangeStartDiagnosisCode { get; set; }
            public RangeEndDiagnosisCode rangeEndDiagnosisCode { get; set; }
        }

        public class RangeStartDiagnosisCode
        {
            public DiagnosisReference diagnosisReference { get; set; }
        }

        public class RangeEndDiagnosisCode
        {
            public DiagnosisReference diagnosisReference { get; set; }
        }

        public class PreExProcedureCodeRangeConditions
        {
            public List<PreExProcedureCodeRangeCondition> preExProcedureCodeRangeCondition { get; set; }
        }

        public class PreExProcedureCodeRangeCondition
        {
            public RangeStartProcedureCode rangeStartProcedureCode { get; set; }
            public RangeEndProcedureCode rangeEndProcedureCode { get; set; }
        }

        public class RangeStartProcedureCode
        {
            public List<ServiceReference> serviceReference { get; set; }
        }

        public class ServiceReference
        {
            public string ID { get; set; }
        }

        public class RangeEndProcedureCode
        {
            public ServiceReference serviceReference { get; set; }
        }

        public class PreexistingConditions
        {
            public DiagnosisReference diagnosisReference { get; set; }
        }

        public class PreexistingConditionProcedures
        {
            public ServiceReference serviceReference { get; set; }
        }

        public class PreExReduction
        {
            public string preexStartDate { get; set; }
            public string reductionEndDate { get; set; }
            public string reductionReasonReceiptDate { get; set; }
            public ReductionReasonCode reductionReasonCode { get; set; }
        }

        public class ReductionReasonCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class OtherResponsiblePersonInfo
        {
            public string sourceOfAuthorization { get; set; }
            public string responsiblePersonEmailAddress { get; set; }
            public ResponsiblePersonName responsiblePersonName { get; set; }
            public ResponsibleAddressInfo responsibleAddressInfo { get; set; }
            public ResponsibilityTypeCodeCode responsibilityTypeCodeCode { get; set; }
            public RelationshipToMemberCode relationshipToMemberCode { get; set; }
        }

        public class ResponsiblePersonName
        {
            public string lastName { get; set; }
            public string firstName { get; set; }
            public string middleName { get; set; }
            public string namePrefix { get; set; }
            public string nameSuffix { get; set; }
            public NameSuffixList nameSuffixList { get; set; }
            public CredentialSuffixList credentialSuffixList { get; set; }
        }

        public class ResponsibleAddressInfo
        {
            public PostalAddress postalAddress { get; set; }
            public AddressPhoneList addressPhoneList { get; set; }
        }

        public class ResponsibilityTypeCodeCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class RelationshipToMemberCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class MemberRepresentative
        {
            public string listMode { get; set; }
            public List<AuthorizedIndividual> authorizedIndividual { get; set; }
        }

        public class AuthorizedIndividual
        {
            public string effectiveStartDate { get; set; }
            public string effectiveEndDate { get; set; }
            public string scope { get; set; }
            public string hccReviewer { get; set; }
            public string documentsReceivedDate { get; set; }
            public AuthorizedPerson authorizedPerson { get; set; }
            public AuthorizedIndividualGroupPhoneList authorizedIndividualGroupPhoneList { get; set; }
            public RepresentativeTypeCode representativeTypeCode { get; set; }
            public RelationshipToMemberCode relationshipToMemberCode { get; set; }
        }

        public class AuthorizedPerson
        {
            public string lastName { get; set; }
            public string firstName { get; set; }
            public string middleName { get; set; }
            public string namePrefix { get; set; }
            public string nameSuffix { get; set; }
            public NameSuffixList nameSuffixList { get; set; }
            public CredentialSuffixList credentialSuffixList { get; set; }
        }

        public class AuthorizedIndividualGroupPhoneList
        {
            public string listMode { get; set; }
            public List<TelephoneNumber> telephoneNumber { get; set; }
        }

        public class RepresentativeTypeCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class InformationAccessPermissionList
        {
            public string listMode { get; set; }
            public List<HIPAAPermission> HIPAAPermission { get; set; }
        }

        public class HIPAAPermission
        {
            public string permissionScope { get; set; }
            public string effectiveStartDate { get; set; }
            public string effectiveEndDate { get; set; }
            public PermittedPerson permittedPerson { get; set; }
            public HIPAAPermissionGroupPhoneList hIPAAPermissionGroupPhoneList { get; set; }
            public RelationshipToMemberCode relationshipToMemberCode { get; set; }
        }

        public class PermittedPerson
        {
            public string lastName { get; set; }
            public string firstName { get; set; }
            public string middleName { get; set; }
            public string namePrefix { get; set; }
            public string nameSuffix { get; set; }
            public NameSuffixList nameSuffixList { get; set; }
            public CredentialSuffixList credentialSuffixList { get; set; }
        }

        public class HIPAAPermissionGroupPhoneList
        {
            public string listMode { get; set; }
            public List<TelephoneNumber> telephoneNumber { get; set; }
        }

        public class InformationAccessRestrictionList
        {
            public string listMode { get; set; }
            public List<HIPAARestriction> HIPAARestriction { get; set; }
        }

        public class HIPAARestriction
        {
            public string restrictionScope { get; set; }
            public string effectiveStartDate { get; set; }
            public string effectiveEndDate { get; set; }
            public RestrictedPerson restrictedPerson { get; set; }
            public RelationshipToMemberCode relationshipToMemberCode { get; set; }
        }

        public class RestrictedPerson
        {
            public string lastName { get; set; }
            public string firstName { get; set; }
            public string middleName { get; set; }
            public string namePrefix { get; set; }
            public string nameSuffix { get; set; }
            public NameSuffixList nameSuffixList { get; set; }
            public CredentialSuffixList credentialSuffixList { get; set; }
        }

        public class PhiAuthorizationList
        {
            public string listMode { get; set; }
            public List<HIPAAAuthorization> HIPAAAuthorization { get; set; }
        }

        public class HIPAAAuthorization
        {
            public string authorizationType { get; set; }
            public string authorizationScope { get; set; }
            public string effectiveStartDate { get; set; }
            public string effectiveEndDate { get; set; }
            public string whoCanBeDisclosedTo { get; set; }
            public string signature { get; set; }
            public string signatureDate { get; set; }
            public AuthorizationPurposeCode authorizationPurposeCode { get; set; }
        }

        public class AuthorizationPurposeCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class CcDirectiveList
        {
            public string listMode { get; set; }
            public List<ConfidentialCommunicationDirective> confidentialCommunicationDirective { get; set; }
        }

        public class ConfidentialCommunicationDirective
        {
            public string confidentialCommunicationScope { get; set; }
            public string effectiveStartDate { get; set; }
            public string effectiveEndDate { get; set; }
            public ConfidentialCommunicationTypeCode confidentialCommunicationTypeCode { get; set; }
        }

        public class ConfidentialCommunicationTypeCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class SubscriptionReference
        {
            public string ID { get; set; }
        }

        public class IndividualReference
        {
            public string ID { get; set; }
        }

        // TBD
        public class Reference
        {
            public string ID { get; set; }
        }

        public class RaceOrEthnicity
        {
            public string listMode { get; set; }
            public List<RaceOrEthnicityCodes> raceOrEthnicityCodes { get; set; }
        }

        public class RaceOrEthnicityCodes
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class VipReasonCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class HealthStatusReasonCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class InformationSourceCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class EnrollmentInfo
        {
            public string applicationSignatureDomain { get; set; }
            public string applicationSignatureDate { get; set; }
            public string applicationReceiptDate { get; set; }
            public string completedDate { get; set; }
            public string submitDate { get; set; }
            public string disenrollDate { get; set; }
            public string disenrollReason { get; set; }
            public MedicareEnrollmentPeriodCode medicareEnrollmentPeriodCode { get; set; }
        }

        public class MedicareEnrollmentPeriodCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class MemberTerminateReasonCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class SpecialEnrollmentPeriodReasonCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }


        public class BenefitPlanOtherIdentifier
        {
            public string otherID { get; set; }
            public IdentificationType identificationType { get; set; }
        }

        public class IdentificationType
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class BenefitNetworks
        {
            public string benefitNetworkName { get; set; }
        }


        // TBD
        public class PractitionerRoleOtherIdTypeCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class Supplier
        {
            public string supplierOtherIdentificationNumber { get; set; }
            public PrimaryName primaryName { get; set; }
            public string hccIdentificationNumber { get; set; }
        }

        public class AffiliatedSupplierNetwork
        {
            public string hccIdentificationNumber { get; set; }
        }

        public class SupplierLocation
        {
            public string hccIdentificationNumber { get; set; }
        }

        public class SupplierNetwork
        {
            public string hccIdentificationNumber { get; set; }
        }

        // TBD
        public class SupplierReference
        {
            public string identifier { get; set; }
        }

        public class AffiliatedSupplierNetworkReference
        {
            public string identifier { get; set; }
        }

        public class SupplierLocationReference
        {
            public string identifier { get; set; }
        }

        public class SupplierNetworkReference
        {
            public string identifier { get; set; }
        }

        public class ReportingCategory
        {
            public string entityIdentifierCode { get; set; }
            public string reportingCategoryName { get; set; }
            public string referenceQualifierIdentifier { get; set; }
            public string referenceId { get; set; }
            public string amount { get; set; }
            public string startDate { get; set; }
            public string endDate { get; set; }
        }

        public class MedicareHICN
        {
            public string hicn { get; set; }
            public string mbi { get; set; }
            public string effectiveDate { get; set; }
            public string expirationDate { get; set; }
            public string entryDate { get; set; }
            public string waitingPeriodStartDate { get; set; }
            public string waitingPeriodEndDate { get; set; }
            public string nursingHomeCompareIndicator { get; set; }
            public List<StateMedicaidEnrollment> stateMedicaidEnrollment { get; set; }
            public StateMedicaidEnrollments stateMedicaidEnrollments { get; set; }
            public List<WorkingSpouse> workingSpouse { get; set; }
            public WorkingSpouses workingSpouses { get; set; }
            public string longTermCareResident { get; set; }
            public List<DialysisDateRanges> dialysisDateRanges { get; set; }
            public TransplantDateRanges transplantDateRanges { get; set; }
            public string esrdIndicator { get; set; }
            public List<EsrdDateRanges> esrdDateRanges { get; set; }
            public string hospiceIndicator { get; set; }
            public List<HospiceDateRanges> hospiceDateRanges { get; set; }
            public string institutionalIndicator { get; set; }
            public List<InstitutionalDateRanges> institutionalDateRanges { get; set; }
            public List<MedicaidDateRanges> medicaidDateRanges { get; set; }
            public NhcDateRanges nhcDateRanges { get; set; }
            public List<PartADates> partADates { get; set; }
            public List<PartBDates> partBDates { get; set; }
            public PremiumWithholdDateRanges premiumWithholdDateRanges { get; set; }
            public SupplementalDateRanges supplementalDateRanges { get; set; }
            public List<PartDDateRanges> partDDateRanges { get; set; }
            public List<MedicareEligibilityReasonCode> medicareEligibilityReasonCode { get; set; }
            public MedicareHICNFields medicareHICNFields { get; set; }
            public List<MedicaidDualStatusRanges> medicaidDualStatusRanges { get; set; }
        }

        public class StateMedicaidEnrollments
        {
            public string updateMode { get; set; }
            public List<StateMedicaidEnrollment> stateMedicaidEnrollment { get; set; }
        }

        public class StateMedicaidEnrollment
        {
            public string startDate { get; set; }
            public string endDate { get; set; }
        }

        public class WorkingSpouses
        {
            public string updateMode { get; set; }
            public List<WorkingSpouse> workingSpouse { get; set; }
        }

        public class WorkingSpouse
        {
            public string startDate { get; set; }
            public string endDate { get; set; }
        }

        public class DialysisDateRanges
        {
            public string updateMode { get; set; }
            public DialysisDateRange dialysisDateRange { get; set; }
        }

        public class DialysisDateRange
        {
            public string startDate { get; set; }
            public string endDate { get; set; }
        }

        public class TransplantDateRanges
        {
            public string updateMode { get; set; }
            public List<TransplantDateRange> transplantDateRange { get; set; }
        }

        public class TransplantDateRange
        {
            public string startDate { get; set; }
            public string endDate { get; set; }
        }

        public class EsrdDateRanges
        {
            public string updateMode { get; set; }
            public List<EsrdDateRange> esrdDateRange { get; set; }
        }

        public class EsrdDateRange
        {
            public string startDate { get; set; }
            public string endDate { get; set; }
        }

        public class HospiceDateRanges
        {
            public string updateMode { get; set; }
            public List<HospiceDateRange> hospiceDateRange { get; set; }
        }

        public class HospiceDateRange
        {
            public string startDate { get; set; }
            public string endDate { get; set; }
        }

        public class InstitutionalDateRanges
        {
            public string updateMode { get; set; }
            public InstitutionalDateRange institutionalDateRange { get; set; }
        }

        public class InstitutionalDateRange
        {
            public string startDate { get; set; }
            public string endDate { get; set; }
        }

        public class MedicaidDateRanges
        {
            public string updateMode { get; set; }
            public MedicaidDateRange medicaidDateRange { get; set; }
        }

        public class MedicaidDateRange
        {
            public string startDate { get; set; }
            public string endDate { get; set; }
            public string medicaidRecertificationDate { get; set; }
        }

        public class NhcDateRanges
        {
            public string updateMode { get; set; }
            public List<NhcDateRange> nhcDateRange { get; set; }
        }

        public class NhcDateRange
        {
            public string startDate { get; set; }
            public string endDate { get; set; }
        }

        public class PartADates
        {
            public string updateMode { get; set; }
            public PartADate partADate { get; set; }
        }

        public class PartADate
        {
            public string effectiveDate { get; set; }
            public string terminationDate { get; set; }
        }

        public class PartBDates
        {
            public string updateMode { get; set; }
            public PartBDate partBDate { get; set; }
        }

        public class PartBDate
        {
            public string effectiveDate { get; set; }
            public string terminationDate { get; set; }
        }

        public class PremiumWithholdDateRanges
        {
            public string updateMode { get; set; }
            public List<PremiumWithholdDateRange> premiumWithholdDateRange { get; set; }
        }

        public class PremiumWithholdDateRange
        {
            public string startDate { get; set; }
            public string endDate { get; set; }
            public string premiumWithholdFlag { get; set; }
            public string premiumWithholdPaymentMethodType { get; set; }
            public string premiumWithholdOption { get; set; }
        }

        public class SupplementalDateRanges
        {
            public string updateMode { get; set; }
            public List<SupplementalDateRange> supplementalDateRange { get; set; }
        }

        public class SupplementalDateRange
        {
            public string startDate { get; set; }
            public string endDate { get; set; }
            public string personCode { get; set; }
            public string supplementalTypeCode { get; set; }
            public string supplementalRxCoverage { get; set; }
        }

        public class PartDDateRanges
        {
            public string startDate { get; set; }
            public string endDate { get; set; }
            public string partDBestAvailableEvidenceIndicator { get; set; }
            public string partDCreditableCoverageIndicator { get; set; }
            public List<PartDLEPDateRanges> partDLEPDateRanges { get; set; }
            public List<PartDLISCopayCategoryDates> partDLISCopayCategoryDates { get; set; }
            public List<PartDLISSubsidyLevelDates> partDLISSubsidyLevelDates { get; set; }
            public List<PartDCurrLISPremSubsidyDates> partDCurrLISPremSubsidyDates { get; set; }
        }

        public class PartDLEPDateRanges
        {
            public string startDate { get; set; }
            public string endDate { get; set; }
            public string partDLEPAmount { get; set; }
            public string partDLEPWaivedAmount { get; set; }
            public string partDLEPSubsidyAmount { get; set; }
            public string partDLEPCurrentNumberofUncoveredMonths { get; set; }
            public string partDLEPTotalNumberofUncoveredMonths { get; set; }
        }

        public class PartDLISCopayCategoryDates
        {
            public string startDate { get; set; }
            public string endDate { get; set; }
            public PartDLISCopayCategoryCode partDLISCopayCategoryCode { get; set; }
        }

        public class PartDLISCopayCategoryCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class PartDLISSubsidyLevelDates
        {
            public string startDate { get; set; }
            public string endDate { get; set; }
            public PartDLISSubsidyLevel partDLISSubsidyLevel { get; set; }
        }

        public class PartDLISSubsidyLevel
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class PartDCurrLISPremSubsidyDates
        {
            public string startDate { get; set; }
            public string endDate { get; set; }
            public string partDCurrentLISPremiumSubsidyAmount { get; set; }
        }

        public class MedicareEligibilityReasonCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class MedicareHICNFields
        {
            public string updateMode { get; set; }
            public List<MedicareHICNField> medicareHICNField { get; set; }
        }

        public class MedicareHICNField
        {
            public string fieldName { get; set; }
            public string fieldValue { get; set; }
        }

        public class MedicaidDualStatusRanges
        {
            public string updateMode { get; set; }
            public List<MedicaidDualStatus> medicaidDualStatus { get; set; }
        }

        public class MedicaidDualStatus
        {
            public string effectiveDate { get; set; }
            public string terminationDate { get; set; }
            public string medicaidDual { get; set; }
            public MedicaidDualStatusSourceCode medicaidDualStatusSourceCode { get; set; }
            public MedicaidDualStatusCode medicaidDualStatusCode { get; set; }
        }

        public class MedicaidDualStatusSourceCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class MedicaidDualStatusCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class CobPolicy
        {
            public string cobPolicyIdentifier { get; set; }
            public string cobPolicyId { get; set; }
            public LeadSourceForOtherInsuranceCode leadSourceForOtherInsuranceCode { get; set; }
            public RelationshipToSubscriberDefinitionReference relationshipToSubscriberDefinitionReference { get; set; }
            public string otherInsuranceCompanyName { get; set; }
            public string policyType { get; set; }
            public string benefitPlanName { get; set; }
            public string groupNumber { get; set; }
            public string memberId { get; set; }
            public BenefitPlanTypeCode benefitPlanTypeCode { get; set; }
            public DateRanges dateRanges { get; set; }
            public List<SubscriberIdList> subscriberIdList { get; set; }
            public SubscriberName subscriberName { get; set; }
            public RxInfo rxInfo { get; set; }
            public CorrespondenceInfo correspondenceInfo { get; set; }
            public List<CarrierIds> carrierIds { get; set; }
        }

        public class LeadSourceForOtherInsuranceCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class BenefitPlanTypeCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class DateRanges
        {
            public string startDate { get; set; }
            public string endDate { get; set; }
            public string responsibilitySequenceCode { get; set; }
            public OBDReasonCode OBDReasonCode { get; set; }
        }

        public class OBDReasonCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class SubscriberIdList
        {
            public List<IdentificationNumber> identificationNumber { get; set; }
            public string issuingState { get; set; }
            public IssuingCountry issuingCountry { get; set; }
            public string effectiveStartDate { get; set; }
            public string effectiveEndDate { get; set; }
            public IdentificationTypeCode identificationTypeCode { get; set; }
        }

        public class IssuingCountry
        {
            public CountryCode countryCode { get; set; }
        }

        public class SubscriberName
        {
            public string lastName { get; set; }
            public string firstName { get; set; }
            public string middleName { get; set; }
            public string namePrefix { get; set; }
            public string nameSuffix { get; set; }
            public NameSuffixList nameSuffixList { get; set; }
            public CredentialSuffixList credentialSuffixList { get; set; }
        }

        public class RxInfo
        {
            public string rxBin { get; set; }
            public string pcn { get; set; }
            public string rxGroup { get; set; }
            public string rxId { get; set; }
            public string rxPhoneNumber { get; set; }
        }

        public class CorrespondenceInfo
        {
            public string emailAddress { get; set; }
            public AddressInfo addressInfo { get; set; }
            public SupplierCorrespondenceUsageCode supplierCorrespondenceUsageCode { get; set; }
            public SupplierLocationCorrespondenceUsageCode supplierLocationCorrespondenceUsageCode { get; set; }
            public AccountCorrespondenceUsageCode accountCorrespondenceUsageCode { get; set; }
        }

        public class SupplierCorrespondenceUsageCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class SupplierLocationCorrespondenceUsageCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class AccountCorrespondenceUsageCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class CarrierIds
        {
            public List<IdentificationNumber> identificationNumber { get; set; }
            public string issuingState { get; set; }
            public IssuingCountry issuingCountry { get; set; }
            public string effectiveStartDate { get; set; }
            public string effectiveEndDate { get; set; }
            public IdentificationTypeCode identificationTypeCode { get; set; }
        }

        public class HeadOfHousehold
        {
            public RelatedMember relatedMember { get; set; }
            public ExternalMemberInformation externalMemberInformation { get; set; }
        }

        public class RelatedMember
        {
            public string definitionName { get; set; }
            public string id { get; set; }
            public string topAccount { get; set; }
            public string customString1 { get; set; }
            public string customString2 { get; set; }
            public string customString3 { get; set; }
            public string customString4 { get; set; }
            public string customInteger1 { get; set; }
            public string customInteger2 { get; set; }
            public string customInteger3 { get; set; }
            public string customInteger4 { get; set; }
            public string customDate1 { get; set; }
            public string customDate2 { get; set; }
            public string customDate3 { get; set; }
            public string customDate4 { get; set; }
            public string customDecimal1 { get; set; }
            public string customDecimal2 { get; set; }
            public string customDecimal3 { get; set; }
            public string customDecimal4 { get; set; }
            public Member member { get; set; }
            public Subscriber subscriber { get; set; }
            public string asOfDate { get; set; }
        }

        public class ExternalMemberInformation
        {
            public string matchDefinitionName { get; set; }
            public string taxIdentificationNumber { get; set; }
            public PrimaryName primaryName { get; set; }
            public AddressInfo addressInfo { get; set; }
        }

        public class MemberComplianceProgram
        {
            public List<ComplianceProgramSelections> complianceProgramSelections { get; set; }
        }

        public class ComplianceProgramSelections
        {
            public string updateMode { get; set; }
            public ComplianceProgramSelection complianceProgramSelection { get; set; }
        }

        public class ComplianceProgramSelection
        {
            public ComplianceProgramDateRanges complianceProgramDateRanges { get; set; }
            public ComplianceProgramReference complianceProgramReference { get; set; }
        }

        public class ComplianceProgramDateRanges
        {
            public string startDate { get; set; }
            public string endDate { get; set; }
            public string complianceCode { get; set; }
        }

        public class ComplianceProgramReference
        {
            public string ID { get; set; }
        }

        public class EnrollmentMemberFactors
        {
            public string updateMode { get; set; }
            public MemberFactors memberFactors { get; set; }
        }

        public class MemberFactors
        {
            public List<MemberFactor> memberFactor { get; set; }
        }

        public class MemberFactor
        {
            public FactorType factorType { get; set; }
            public List<FactorDateRanges> factorDateRanges { get; set; }
        }

        public class FactorType
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class FactorDateRanges
        {
            public string factorStartDate { get; set; }
            public string factorEndDate { get; set; }
            public FactorQualifier factorQualifier { get; set; }
            public string factorValue { get; set; }
        }

        public class FactorQualifier
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class AlternatePaymentContact
        {
            public RecipientName recipientName { get; set; }
            public RecipientAddress recipientAddress { get; set; }
        }

        public class RecipientName
        {
            public string lastName { get; set; }
            public string firstName { get; set; }
            public string middleName { get; set; }
            public string namePrefix { get; set; }
            public string nameSuffix { get; set; }
            public NameSuffixList nameSuffixList { get; set; }
            public CredentialSuffixList credentialSuffixList { get; set; }
        }

        public class RecipientAddress
        {
            public Address address { get; set; }
            public string address2 { get; set; }
            public string address3 { get; set; }
            public string stateCode { get; set; }
            public string zipCode { get; set; }
            public string zipExtensionCode { get; set; }
            public string cityName { get; set; }
            public string countyCode { get; set; }
            public CountryCode countryCode { get; set; }
            public string longitude { get; set; }
            public string latitude { get; set; }
            public string ignoreAddressCheck { get; set; }
            public string undeliverableAddress { get; set; }
        }


    }
}

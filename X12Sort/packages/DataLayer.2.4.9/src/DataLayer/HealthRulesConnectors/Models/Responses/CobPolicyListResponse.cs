using System.Collections.Generic;

namespace DataLayer.HealthRulesConnectors.Models.Responses
{
    /// <summary>
    /// COB Policy List response object
    /// </summary>
    public class CobPolicyListResponse
    {
        /// <summary>
        /// Overall response object        
        /// </summary>
        public class Response
        {
            /// <summary>
            /// Envelope that holds all the responses
            /// </summary>
            public EnvelopeInner Envelope { get; set; }
        }

        /// <summary>
        /// Envelope that holds all the responses
        /// </summary>
        public class EnvelopeInner
        {
            /// <summary>
            /// Header object - should always be null
            /// </summary>
            public string Header { get; set; }

            /// <summary>
            /// Body object
            /// </summary>
            public Body Body { get; set; }
        }

        /// <summary>
        /// Body class
        /// </summary>
        public class Body
        {
            /// <summary>
            /// COB Policy Response
            /// </summary>
            public CobPolicyResponse cobPolicyResponse { get; set; }
        }

        /// <summary>
        /// COB Policy Response
        /// </summary>
        public class CobPolicyResponse
        {
            /// <summary>
            /// HealthRules ID
            /// </summary>
            public string memberHccID { get; set; }

            /// <summary>
            /// Policy status
            /// </summary>
            public string status { get; set; }

            /// <summary>
            /// List of policies
            /// </summary>
            public List<CobPolicy> cobPolicy { get; set; }

            /// <summary>
            /// Errors encoutered
            /// </summary>
            public Errors errors { get; set; }
        }

        /// <summary>
        /// Individual COB policy
        /// </summary>
        public class CobPolicy
        {
            /// <summary>
            /// COB policy ID
            /// </summary>
            public string cobPolicyId { get; set; }

            /// <summary>
            /// Group #
            /// </summary>
            public string groupNumber { get; set; }

            /// <summary>
            /// Transaction type
            /// </summary>
            public string transactionType { get; set; }
        }

        /// <summary>
        /// Holder for errors
        /// </summary>
        public class Errors
        {
            /// <summary>
            /// Indidividual error
            /// </summary>
            public List<Error> error { get; set; }
        }

        /// <summary>
        /// Error object
        /// </summary>
        public class Error : Fault
        {
            /// <summary>
            /// Error type
            /// </summary>
            public string errorType { get; set; }

            /// <summary>
            /// Error message
            /// </summary>
            public string message { get; set; }

            /// <summary>
            /// Date / time the error was experiences
            /// </summary>
            public string dateTime { get; set; }

            /// <summary>
            ///  User who created the error
            /// </summary>
            public string authenticatedUser { get; set; }

            /// <summary>
            /// Trace information
            /// </summary>
            public string traceInfo { get; set; }
        }

    }
}

using System.Collections.Generic;

namespace DataLayer.HealthRulesConnectors.Models.Responses
{
    public class MembershipReferenceLookupResponse
    {

        public class Response
        {
            public EnvelopeInner Envelope { get; set; }
        }

        public class EnvelopeInner
        {
            public string Header { get; set; }
            public Body Body { get; set; }
        }

        public class Body
        {
            public MembershipReferenceLookupResponse2 membershipReferenceLookupResponse { get; set; }
        }

        public class MembershipReferenceLookupResponse2
        {
            public LookupReference lookupReference { get; set; }
            public string status { get; set; }
            public Errors errors { get; set; }
        }

        public class LookupReference
        {
            public string ID { get; set; }
        }

        public class Errors
        {
            public List<Error> error { get; set; }
        }

        public class Error
        {
            public string errorType { get; set; }
            public string message { get; set; }
            public string dateTime { get; set; }
            public string authenticatedUser { get; set; }
            public string traceInfo { get; set; }
        }


    }
}

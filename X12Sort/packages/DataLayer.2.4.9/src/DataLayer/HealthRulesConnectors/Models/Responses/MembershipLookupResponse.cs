namespace DataLayer.HealthRulesConnectors.Models.Responses
{
    public class MembershipLookupResponse
    {
        /// <summary>
        /// Primary response object
        /// </summary>
        public class Response
        {
            public EnvelopeInner Envelope { get; set; }
        }

        public class PrimaryName
        {
            public string lastName { get; set; }
            public string firstName { get; set; }
            public string middleName { get; set; }
            public string namePrefix { get; set; }
            public string nameSuffix { get; set; }
        }

        public class OtherNameInfo
        {
            public string lastName { get; set; }
            public string firstName { get; set; }
            public string middleName { get; set; }
            public string namePrefix { get; set; }
            public string nameSuffix { get; set; }
        }

        public class OtherNameTypeCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
        }

        public class OtherName
        {
            public OtherNameInfo otherNameInfo { get; set; }
            public OtherNameTypeCode otherNameTypeCode { get; set; }
        }

        public class OtherNames
        {
            public OtherName otherName { get; set; }
        }

        public class CountryCode
        {
            public string countryCode { get; set; }
        }

        public class PostalAddress
        {
            public string address { get; set; }
            public string stateCode { get; set; }
            public string zipCode { get; set; }
            public string zipExtensionCode { get; set; }
            public string cityName { get; set; }
            public string countyCode { get; set; }
            public CountryCode countryCode { get; set; }
            public string countryCode2
            {
                get
                {
                    if (countryCode == null)
                        return null;
                    return countryCode.countryCode;
                }
            }
            public string longitude { get; set; }
            public string latitude { get; set; }
        }

        public class AddressInfo
        {
            public PostalAddress postalAddress { get; set; }
        }

        public class AddressTypeCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
        }

        public class MemberPhysicalAddress
        {
            public AddressInfo addressInfo { get; set; }
            public AddressTypeCode addressTypeCode { get; set; }
        }

        public class PhysicalAddress
        {
            public MemberPhysicalAddress memberPhysicalAddress { get; set; }
        }

        public class PostalAddress2
        {
            public string address { get; set; }
            public string stateCode { get; set; }
            public string zipCode { get; set; }
            public string zipExtensionCode { get; set; }
            public string cityName { get; set; }
            public string countyCode { get; set; }
            public CountryCode countryCode { get; set; }
            public string countryCode2
            {
                get
                {
                    if (countryCode == null)
                        return null;
                    return countryCode.countryCode;
                }
            }
            public string longitude { get; set; }
            public string latitude { get; set; }
        }

        public class CorrespondenceAddress
        {
            public PostalAddress2 postalAddress { get; set; }
        }

        public class IdentificationTypeCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
        }

        public class IdentificationNumber
        {
            public string identificationNumber { get; set; }
            public IdentificationTypeCode identificationTypeCode { get; set; }
        }

        public class OtherIdNumberList
        {
            public IdentificationNumber identificationNumber { get; set; }
        }

        public class UdtListValueSet
        {
            public string attributeRoleName { get; set; }
            public string attrValueAsString { get; set; }
        }

        public class UserDefinedTermReference
        {
            public string ID { get; set; }
        }

        public class MembershipUDT
        {
            public UdtListValueSet udtListValueSet { get; set; }
            public UserDefinedTermReference userDefinedTermReference { get; set; }
        }

        public class MembershipUDTList
        {
            public MembershipUDT membershipUDT { get; set; }
        }

        public class RelationshipToSubscriberDefinitionReference
        {
            public string ID { get; set; }
        }

        public class AttachmentSetReference
        {
            public string ID { get; set; }
        }

        public class BenefitPlanReference
        {
            public string benefitPlanHccId { get; set; }
            public string benefitPlanName { get; set; }
        }

        public class PlanSelection
        {
            public string startDate { get; set; }
            public string endDate { get; set; }
            public BenefitPlanReference benefitPlanReference { get; set; }
        }

        public class Membership
        {
            public string membershipIdentifier { get; set; }
            public string memberIsSubscriber { get; set; }
            public string outOfServiceArea { get; set; }
            public string hccIdentifier { get; set; }
            public string receiptDate { get; set; }
            public string smokingStatus { get; set; }
            public string waivePHIClaimViewRestriction { get; set; }
            public string payeeHccId { get; set; }
            public string isMemberInHospice { get; set; }
            public Individual individual { get; set; }
            public PhysicalAddress physicalAddress { get; set; }
            public CorrespondenceAddress correspondenceAddress { get; set; }
            public OtherIdNumberList otherIdNumberList { get; set; }
            public DisabilityInfoForEligibility disabilityInfoForEligibility  { get; set; }
            public DependentVerification dependentVerification { get; set; }
            public PhysicalCharacteristics physicalCharacteristics { get; set; }
            public EmploymentInfo employmentInfo { get; set; }
            public DentalInfo dentalInfo { get; set; }
            public PriorCoverageInfo priorCoverageInfo { get; set; }
            public PreExistingConditionInfo preExistingConditionInfo { get; set; }
            public PreExReduction preExReduction { get; set; }
            public OtherResponsiblePersonInfo otherResponsiblePersonInfo { get; set; }
            public MemberRepresentative memberRepresentative { get; set; }
            public MembershipUDTList membershipUDTList { get; set; }
            public InformationAccessPermissionList informationAccessPermissionList { get; set; }
            // TBD
            // This may need to be converted to a <List> object
            public InformationAccessRestrictionList informationAccessRestrictionList { get; set; }
            // TBD
            // This may need to be converted to a <List> object
            public PhiAuthorizationList phiAuthorizationList { get; set; }
            // TBD
            // This may need to be converted to a <List> object
            public CcDirectiveList ccDirectiveList { get; set; }
            // TBD
            public SubscriptionReference subscriptionReference { get; set; }
            // TBD
            public IndividualReference individualReference { get; set; }
            public RaceOrEthnicityCodes raceOrEthnicityCodes { get; set; }
            // TBD
            public VipReasonCode vipReasonCode { get; set; }
            // TBD
            public HealthStatusReasonCode healthStatusReasonCode { get; set; }
            // TBD
            public InformationSourceCode informationSourceCode { get; set; }
            public RelationshipToSubscriberDefinitionReference relationshipToSubscriberDefinitionReference { get; set; }
            public AttachmentSetReference attachmentSetReference { get; set; }
            public PlanSelection planSelection { get; set; }
            public ProviderSelection providerSelection { get; set; }
        }

        // TBD
        public class ProviderSelection
        {
            // TBD
            public string providerRoleType { get; set; }
            // TBD
            public string startDate { get; set; }
            // TBD
            public string endDate { get; set; }
            // TBD
            public PractitionerReference practitionerReference { get; set; }
            // TBD
            public PractitionerRoleReference practitionerRoleReference { get; set; }
            // TBD
            public SupplierReference supplierReference { get; set; }
            // TBD
            public AffiliatedSupplierNetworkReference affiliatedSupplierNetworkReference { get; set; }
        }


        // TBD
        public class SupplierReference
        {
            // TBD
            public string hccIdentificationNumber { get; set; }
            // TBD
            public string supplierOtherIdentificationNumber { get; set; }
            // TBD
            public PrimaryName primaryName { get; set; }
            // TBD
            public string asOfDate { get; set; }
        }

        // TBD
        public class AffiliatedSupplierNetworkReference
        {
            // TBD
            public string hccIdentificationNumber { get; set; }
        }


        // TBD
        public class PractitionerReference
        {
            // TBD
            public string hccIdentificationNumber { get; set; }
            // TBD
            public string practitionerOtherIdentificationNumber { get; set; }
            // TBD
            public string firstName { get; set; }
            // TBD
            public string lastName { get; set; }
            // TBD
            public string asOfDate { get; set; }
        }

        // TBD
        public class PractitionerRoleReference
        {
            // TBD
            public string hccIdentificationNumber { get; set; }
            // TBD
            public string practitionerRoleOtherIdentificationNumber { get; set; }
            // TBD
            public PractitionerRoleOtherIdTypeCode practitionerRoleOtherIdTypeCode { get; set; }
            // TBD
            public string asOfDate { get; set; }
            // TBD
            public string roleName { get; set; }
        }

        // TBD
        public class PractitionerRoleOtherIdTypeCode
        {
            // TBD
            public string codeSetName { get; set; }
            // TBD
            public string codeEntry { get; set; }
            // TBD
            public string shortName { get; set; }
        }


        // TBD
        public class DentalInfo
        {
            // TBD
            public string remarks { get; set; }
            // TBD
            public MissingTeeth missingTeeth { get; set; }
        }

        // TBD
        public class MissingTeeth
        {
            // TBD
            // This may need to be converted to a <List> object
            public string listMode { get; set; }
            // The object below will most likely need to put into a <List>
            // TBD
            public string toothName { get; set; }
        }

        // TBD
        public class PriorCoverageInfo
        {
            // TBD
            public string priorCoverageMonthCount { get; set; }
        }

        // TBD
        public class PreExistingConditionInfo
        {
            // TBD
            public string onsetOfConditionDate { get; set; }
            // TBD
            public string preexistingPeriodEndDate { get; set; }
            // TBD
            public string preexDescription { get; set; }
            // TBD
            public PreExDiagnosisCodeRangeConditions preExDiagnosisCodeRangeConditions { get; set; }
            // TBD
            public PreExProcedureCodeRangeConditions preExProcedureCodeRangeConditions { get; set; }
            // TBD
            public PreexistingConditions preexistingConditions { get; set; }
            // TBD
            public PreexistingConditionProcedures preexistingConditionProcedures { get; set; }
        }


        // TBD
        public class MemberRepresentative
        {
            // TBD
            public AuthorizedIndividual authorizedIndividual { get; set; }
        }

        // TBD
        public class AuthorizedIndividual
        {
            // TBD
            public string effectiveStartDate { get; set; }
            // TBD
            public string effectiveEndDate { get; set; }
            // TBD
            public string scope { get; set; }
            // TBD
            public string hccReviewer { get; set; }
            // TBD
            public string documentsReceivedDate { get; set; }
            // TBD
            public AuthorizedPerson authorizedPerson { get; set; }
            // TBD
            // This may need to be converted to a <List> object
            public AuthorizedIndividualGroupPhoneList authorizedIndividualGroupPhoneList { get; set; }
            // TBD
            public RepresentativeTypeCode representativeTypeCode { get; set; }
            // TBD
            public RelationshipToMemberCode relationshipToMemberCode { get; set; }
        }

        // TBD
        public class AuthorizedPerson
        {
            // TBD
            public string lastName { get; set; }
            // TBD
            public string firstName { get; set; }
            // TBD
            public string middleName { get; set; }
            // TBD
            public string namePrefix { get; set; }
            // TBD
            public string nameSuffix { get; set; }
            // TBD
            // This may need to be converted to a <List> object
            public NameSuffixList nameSuffixList { get; set; }
            // TBD
            // This may need to be converted to a <List> object
            public CredentialSuffixList credentialSuffixList { get; set; }
        }

        // TBD
        public class AuthorizedIndividualGroupPhoneList
        {
            // TBD
            // This may need to be converted to a <List> object
            public string listMode { get; set; }
            // The object below will most likely need to put into a <List>
            // TBD
            public TelephoneNumber telephoneNumber { get; set; }
        }

        // TBD
        public class RepresentativeTypeCode
        {
            // TBD
            public string codeSetName { get; set; }
            // TBD
            public string codeEntry { get; set; }
            // TBD
            public string shortName { get; set; }
        }

        // TBD
        public class OtherResponsiblePersonInfo
        {
            // TBD
            public string sourceOfAuthorization { get; set; }
            // TBD
            public string responsiblePersonEmailAddress { get; set; }
            // TBD
            public ResponsiblePersonName responsiblePersonName { get; set; }
            // TBD
            public ResponsibleAddressInfo responsibleAddressInfo { get; set; }
            // TBD
            public ResponsibilityTypeCodeCode responsibilityTypeCodeCode { get; set; }
            // TBD
            public RelationshipToMemberCode relationshipToMemberCode { get; set; }
        }

        // TBD
        public class ResponsiblePersonName
        {
            // TBD
            public string lastName { get; set; }
            // TBD
            public string firstName { get; set; }
            // TBD
            public string middleName { get; set; }
            // TBD
            public string namePrefix { get; set; }
            // TBD
            public string nameSuffix { get; set; }
            // TBD
            // This may need to be converted to a <List> object
            public NameSuffixList nameSuffixList { get; set; }
            // TBD
            // This may need to be converted to a <List> object
            public CredentialSuffixList credentialSuffixList { get; set; }
        }

        // TBD
        public class ResponsibleAddressInfo
        {
            // TBD
            public PostalAddress postalAddress { get; set; }
            // TBD
            // This may need to be converted to a <List> object
            public AddressPhoneList addressPhoneList { get; set; }
        }

        // TBD
        public class ResponsibilityTypeCodeCode
        {
            // TBD
            public string codeSetName { get; set; }
            // TBD
            public string codeEntry { get; set; }
            // TBD
            public string shortName { get; set; }
        }

        // TBD
        public class RelationshipToMemberCode
        {
            // TBD
            public string codeSetName { get; set; }
            // TBD
            public string codeEntry { get; set; }
            // TBD
            public string shortName { get; set; }
        }


        // TBD
        public class PreExProcedureCodeRangeConditions
        {
            // TBD
            public PreExProcedureCodeRangeCondition preExProcedureCodeRangeCondition { get; set; }
        }

        // TBD
        public class PreExProcedureCodeRangeCondition
        {
            // TBD
            public RangeStartProcedureCode rangeStartProcedureCode { get; set; }
            // TBD
            public RangeEndProcedureCode rangeEndProcedureCode { get; set; }
        }


        // TBD
        public class RangeStartProcedureCode
        {
            // TBD
            public ServiceReference serviceReference { get; set; }
        }

        // TBD
        public class ServiceReference
        {
            // TBD
            public string ID { get; set; }
        }

        // TBD
        public class RangeEndProcedureCode
        {
            // TBD
            public ServiceReference serviceReference { get; set; }
        }

        // TBD
        public class PreexistingConditions
        {
            // TBD
            public DiagnosisReference diagnosisReference { get; set; }
        }

        // TBD
        public class PreexistingConditionProcedures
        {
            // TBD
            public ServiceReference serviceReference { get; set; }
        }

        // TBD
        public class PreExReduction
        {
            // TBD
            public string preexStartDate { get; set; }
            // TBD
            public string reductionEndDate { get; set; }
            // TBD
            public string reductionReasonReceiptDate { get; set; }
            // TBD
            public ReductionReasonCode reductionReasonCode { get; set; }
        }

        // TBD
        public class ReductionReasonCode
        {
            // TBD
            public string codeSetName { get; set; }
            // TBD
            public string codeEntry { get; set; }
            // TBD
            public string shortName { get; set; }
        }


        // TBD
        public class PreExDiagnosisCodeRangeConditions
        {
            // TBD
            public PreExDiagnosisCodeRangeCondition preExDiagnosisCodeRangeCondition { get; set; }
        }

        // TBD
        public class PreExDiagnosisCodeRangeCondition
        {
            // TBD
            public RangeStartDiagnosisCode rangeStartDiagnosisCode { get; set; }
            // TBD
            public RangeEndDiagnosisCode rangeEndDiagnosisCode { get; set; }
        }

        // TBD
        public class RangeStartDiagnosisCode
        {
            // TBD
            public DiagnosisReference diagnosisReference { get; set; }
        }

        // TBD
        public class RangeEndDiagnosisCode
        {
            // TBD
            public DiagnosisReference diagnosisReference { get; set; }
        }

        public class DiagnosisReference
        {
            // TBD
            public string ID { get; set; }
        }

        // TBD
        public class EmploymentInfo
        {
            // TBD
            public string hireDate { get; set; }
            // TBD
            public string salaryInterval { get; set; }
            // TBD
            public string salaryPerInterval { get; set; }
            // TBD
            public string unionStatus { get; set; }
            // TBD
            public string positionOrTitle { get; set; }
            // TBD
            public string numberOfHoursWorked { get; set; }
            // TBD
            public string retirementDate { get; set; }
            // TBD
            public EmploymentStatusCodeCode employmentStatusCodeCode { get; set; }
            // TBD
            public SalaryGradeCodeCode salaryGradeCodeCode { get; set; }
            // TBD
            public UnitsDomainCode unitsDomainCode { get; set; }
            // TBD
            public EmployeeTypeDomainCode employeeTypeDomainCode { get; set; }
            // TBD
            public string department { get; set; }
        }

        // TBD
        public class EmploymentStatusCodeCode
        {
            // TBD
            public string codeSetName { get; set; }
            // TBD
            public string codeEntry { get; set; }
            // TBD
            public string shortName { get; set; }
        }

        // TBD
        public class SalaryGradeCodeCode
        {
            // TBD
            public string codeSetName { get; set; }
            // TBD
            public string codeEntry { get; set; }
            // TBD
            public string shortName { get; set; }
        }

        // TBD
        public class UnitsDomainCode
        {
            // TBD
            public string codeSetName { get; set; }
            // TBD
            public string codeEntry { get; set; }
            // TBD
            public string shortName { get; set; }
        }

        // TBD
        public class EmployeeTypeDomainCode
        {
            // TBD
            public string codeSetName { get; set; }
            // TBD
            public string codeEntry { get; set; }
            // TBD
            public string shortName { get; set; }
        }


        // TBD
        public class InformationAccessPermissionList
        {
            // TBD
            public HIPAAPermission HIPAAPermission { get; set; }
        }

        // TBD
        public class HIPAAPermission
        {
            // TBD
            public string permissionScope { get; set; }
            // TBD
            public string effectiveStartDate { get; set; }
            // TBD
            public string effectiveEndDate { get; set; }
            // TBD
            public PermittedPerson permittedPerson { get; set; }
            // TBD
            // This may need to be converted to a <List> object
            public HIPAAPermissionGroupPhoneList hIPAAPermissionGroupPhoneList { get; set; }
            // TBD
            public RelationshipToMemberCode relationshipToMemberCode { get; set; }
        }

        // TBD
        public class PermittedPerson
        {
            // TBD
            public string lastName { get; set; }
            // TBD
            public string firstName { get; set; }
            // TBD
            public string middleName { get; set; }
            // TBD
            public string namePrefix { get; set; }
            // TBD
            public string nameSuffix { get; set; }
            // TBD
            // This may need to be converted to a <List> object
            public NameSuffixList nameSuffixList { get; set; }
            // TBD
            // This may need to be converted to a <List> object
            public CredentialSuffixList credentialSuffixList { get; set; }
        }

        // TBD
        public class HIPAAPermissionGroupPhoneList
        {
            // TBD
            // This may need to be converted to a <List> object
            public string listMode { get; set; }
            // The object below will most likely need to put into a <List>
            // TBD
            public TelephoneNumber telephoneNumber { get; set; }
        }

        // TBD
        public class InformationAccessRestrictionList
        {
            // TBD
            public HIPAARestriction HIPAARestriction { get; set; }
        }

        // TBD
        public class HIPAARestriction
        {
            // TBD
            public string restrictionScope { get; set; }
            // TBD
            public string effectiveStartDate { get; set; }
            // TBD
            public string effectiveEndDate { get; set; }
            // TBD
            public RestrictedPerson restrictedPerson { get; set; }
            // TBD
            public RelationshipToMemberCode relationshipToMemberCode { get; set; }
        }

        // TBD
        public class RestrictedPerson
        {
            // TBD
            public string lastName { get; set; }
            // TBD
            public string firstName { get; set; }
            // TBD
            public string middleName { get; set; }
            // TBD
            public string namePrefix { get; set; }
            // TBD
            public string nameSuffix { get; set; }
            // TBD
            // This may need to be converted to a <List> object
            public NameSuffixList nameSuffixList { get; set; }
            // TBD
            // This may need to be converted to a <List> object
            public CredentialSuffixList credentialSuffixList { get; set; }
        }

        // TBD
        public class PhiAuthorizationList
        {
            // TBD
            public HIPAAAuthorization HIPAAAuthorization { get; set; }
        }

        // TBD
        public class HIPAAAuthorization
        {
            // TBD
            public string authorizationType { get; set; }
            // TBD
            public string authorizationScope { get; set; }
            // TBD
            public string effectiveStartDate { get; set; }
            // TBD
            public string effectiveEndDate { get; set; }
            // TBD
            public string whoCanBeDisclosedTo { get; set; }
            // TBD
            public string signature { get; set; }
            // TBD
            public string signatureDate { get; set; }
            // TBD
            public AuthorizationPurposeCode authorizationPurposeCode { get; set; }
        }

        // TBD
        public class AuthorizationPurposeCode
        {
            // TBD
            public string codeSetName { get; set; }
            // TBD
            public string codeEntry { get; set; }
            // TBD
            public string shortName { get; set; }
        }

        // TBD
        public class CcDirectiveList
        {
            // TBD
            public ConfidentialCommunicationDirective confidentialCommunicationDirective { get; set; }
        }


        // TBD
        public class PhysicalCharacteristics
        {
            // TBD
            public string heightInchesCount { get; set; }
            // TBD
            public string weightPoundsCount { get; set; }
        }

        // TBD
        public class DependentVerification
        {
            // TBD
            public string verificationType { get; set; }
            // TBD
            public string certLastInfoRequestDate { get; set; }
            // TBD
            public string certLastInfoReceiptDate { get; set; }
            // TBD
            public string certInformationDueDate { get; set; }
            // TBD
            public string lastVerificationDate { get; set; }
            // TBD
            public StudentSchoolInfo studentSchoolInfo { get; set; }
        }

        // TBD
        public class StudentSchoolInfo
        {
            // TBD
            public string studentStatusCode { get; set; }
            // TBD
            public string graduationDate { get; set; }
            // TBD
            public SchoolInformation schoolInformation { get; set; }
            // TBD
            public SchoolTypeCode schoolTypeCode { get; set; }
        }

        // TBD
        public class SchoolInformation
        {
            // TBD
            public string institutionName { get; set; }
            // TBD
            public ContactInfo contactInfo { get; set; }
        }

        // TBD
        public class SchoolTypeCode
        {
            // TBD
            public string codeSetName { get; set; }
            // TBD
            public string codeEntry { get; set; }
            // TBD
            public string shortName { get; set; }
        }

        // TBD
        public class ContactInfo
        {
            // TBD
            public string contactDescription { get; set; }
            // TBD
            public string contactEmailAddress { get; set; }
            // TBD
            public ContactName contactName { get; set; }
            // TBD
            public LocationAddress locationAddress { get; set; }
            // TBD
            public ContactTypeCode contactTypeCode { get; set; }
        }

        // TBD
        public class ContactName
        {
            // TBD
            public string lastName { get; set; }
            // TBD
            public string firstName { get; set; }
            // TBD
            public string middleName { get; set; }
            // TBD
            public string namePrefix { get; set; }
            // TBD
            public string nameSuffix { get; set; }
            // TBD
            // This may need to be converted to a <List> object
            public NameSuffixList nameSuffixList { get; set; }
            // TBD
            // This may need to be converted to a <List> object
            public CredentialSuffixList credentialSuffixList { get; set; }
        }

        // TBD
        public class LocationAddress
        {
            // TBD
            public PostalAddress postalAddress { get; set; }
            // This may need to be converted to a <List> object
            public AddressPhoneList addressPhoneList { get; set; }
        }

        // TBD
        public class ContactTypeCode
        {
            // TBD
            public string codeSetName { get; set; }
            // TBD
            public string codeEntry { get; set; }
            // TBD
            public string shortName { get; set; }
        }

        // TBD
        public class ConfidentialCommunicationDirective
        {
            // TBD
            public string confidentialCommunicationScope { get; set; }
            // TBD
            public string effectiveStartDate { get; set; }
            // TBD
            public string effectiveEndDate { get; set; }
            // TBD
            public ConfidentialCommunicationTypeCode confidentialCommunicationTypeCode { get; set; }
        }

        // TBD
        public class ConfidentialCommunicationTypeCode
        {
            // TBD
            public string codeSetName { get; set; }
            // TBD
            public string codeEntry { get; set; }
            // TBD
            public string shortName { get; set; }
        }

        // TBD
        public class SubscriptionReference
        {
            // TBD
            public string ID { get; set; }
        }

        // TBD
        public class IndividualReference
        {
            // TBD
            public string ID { get; set; }
        }


        // TBD
        public class RaceOrEthnicityCodes
        {
            // TBD
            public string codeSetName { get; set; }
            // TBD
            public string codeEntry { get; set; }
            // TBD
            public string shortName { get; set; }
        }

        // TBD
        public class VipReasonCode
        {
            // TBD
            public string codeSetName { get; set; }
            // TBD
            public string codeEntry { get; set; }
            // TBD
            public string shortName { get; set; }
        }

        // TBD
        public class HealthStatusReasonCode
        {
            // TBD
            public string codeSetName { get; set; }
            // TBD
            public string codeEntry { get; set; }
            // TBD
            public string shortName { get; set; }
        }

        // TBD
        public class InformationSourceCode
        {
            // TBD
            public string codeSetName { get; set; }
            // TBD
            public string codeEntry { get; set; }
            // TBD
            public string shortName { get; set; }
        }


        public class DisabilityInfoForEligibility
        {
            // TBD
            public string disabilityTypeCode { get; set; }
            // TBD
            public string disabilityReportedDate { get; set; }
            // TBD
            public string verificationReceiptDate { get; set; }
            // TBD
            public DisabilityDenialReasonCode disabilityDenialReasonCode { get; set; }
            // TBD
            public DiagnosisReference diagnosisReference { get; set; }
        }

        // TBD
        public class DisabilityDenialReasonCode
        {
            // TBD
            public string codeSetName { get; set; }
            // TBD
            public string codeEntry { get; set; }
            // TBD
            public string shortName { get; set; }
        }

        // TBD
        public class Individual
        {
            // TBD
            public string taxIdentificationNumber { get; set; }
            // TBD
            public string genderCode { get; set; }
            // TBD
            public string maritalStatusCode { get; set; }
            // TBD
            public string birthDate { get; set; }
            // TBD
            public string birthPlaceStateCode { get; set; }
            // TBD
            public string birthPlaceCountyCode { get; set; }
            // TBD
            public BirthPlaceCountryCode birthPlaceCountryCode { get; set; }

            public string countryCode2
            {
                get {
                    if (birthPlaceCountryCode == null)
                        return null;
                    return birthPlaceCountryCode.countryCode.countryCode;
                }
            }

            // TBD
            public string dateOfDeath { get; set; }
            // TBD
            public PrimaryName primaryName = new PrimaryName();
            // TBD
            public OtherNames otherNames = new OtherNames();
            // TBD
            public Languages languages = new Languages();
            // TBD
            public ImmigrationStatusCode immigrationStatusCode { get; set; }
        }

        // TBD
        public class BirthPlaceCountryCode
        {
            // TBD
            public CountryCode countryCode { get; set; }
        }

        // TBD
        public class Languages
        {
            // TBD
            // This may need to be converted to a <List> object
            public string listMode { get; set; }
            // The object below will most likely need to put into a <List>
            // TBD
            public Language language { get; set; }
        }

        // TBD
        public class LanguageDomainCode
        {
            // TBD
            public string codeSetName { get; set; }
            // TBD
            public string codeEntry { get; set; }
            // TBD
            public string shortName { get; set; }
        }

        // TBD
        public class ImmigrationStatusCode
        {
            // TBD
            public string codeSetName { get; set; }
            // TBD
            public string codeEntry { get; set; }
            // TBD
            public string shortName { get; set; }
        }

        public class MembershipLookupResponse2
        {
            public Membership membership { get; set; }
            public string status { get; set; }
            // TBD
            public Errors errors { get; set; }

        }

        // TBD
        public class Errors
        {
            // TBD
            public Error error { get; set; }
        }

        // TBD
        public class Error
        {
            // TBD
            public string errorType { get; set; }
            // TBD
            public string message { get; set; }
            // TBD
            public string dateTime { get; set; }
            // TBD
            public string authenticatedUser { get; set; }
            // TBD
            public string traceInfo { get; set; }
        }

        public class Body
        {
            public MembershipLookupResponse2 membershipLookupResponse { get; set; }
        }

        public class Header
        {
            public string holder { get; set; }
        }

        /// <summary>
        /// Inner  Envelope
        /// </summary>
        public class EnvelopeInner
        {
            /// <summary>
            /// Body object
            /// </summary>
            public Body Body { get; set; }

            /// <summary>
            /// Header object
            /// </summary>
            public Header Header { get; set; }
        }




    }
}

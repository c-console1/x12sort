using System.Collections.Generic;

namespace DataLayer.HealthRulesConnectors.Models.Responses
{
    public class SubscriptionLookupResponse
    {

        /// <summary>
        /// Subscription Lookup Response Object
        /// </summary>
        public class Response
        {
            /// <summary>
            /// Inner Envelope
            /// </summary>
            public EnvelopeInner Envelope { get; set; }
        }


        public class EnvelopeInner
        {
            public string Header { get; set; }
            public Body Body { get; set; }
        }

        public class Body
        {
            public SubscriptionLookupResponse2 subscriptionLookupResponse { get; set; }
        }

        public class SubscriptionLookupResponse2
        {
            public Subscription subscription { get; set; }
            public string status { get; set; }
            public Errors errors { get; set; }
        }

        public class Subscription
        {
            public string subscriptionIdentifier { get; set; }
            public string payeeHccId { get; set; }
            public List<HccIdentifier> hccIdentifier { get; set; }
            public string receiptDate { get; set; }
            public string originalEffectiveDate { get; set; }
            public string applicationSignature { get; set; }
            public string applicationDate { get; set; }
            public string lastPremiumPaidDate { get; set; }
            public PaymentHold paymentHold { get; set; }
            public SubscriptionUDTList subscriptionUDTList { get; set; }
            public QualityControlReviewConclusions qualityControlReviewConclusions { get; set; }
            public AccountReference accountReference { get; set; }
            public VipReasonCode vipReasonCode { get; set; }
            public InformationSourceCode informationSourceCode { get; set; }
            public CobraSelection cobraSelection { get; set; }
            public PayeeBankAccount payeeBankAccount { get; set; }
        }

        public class PaymentHold
        {
            public string enablePaymentHold { get; set; }
            public string requestingUser { get; set; }
            public string paymentHoldReasonDescription { get; set; }
            public PaymentHoldReasonCode paymentHoldReasonCode { get; set; }
        }

        public class PaymentHoldReasonCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class SubscriptionUDTList
        {
            public List<SubscriptionUDT> subscriptionUDT { get; set; }
        }

        public class SubscriptionUDT
        {
            public List<UdtListValueSet> udtListValueSet { get; set; }
            public UserDefinedTermReference userDefinedTermReference { get; set; }
        }

        public class UdtListValueSet
        {
            public string attributeRoleName { get; set; }
            public string attrValueAsString { get; set; }
        }

        public class UserDefinedTermReference
        {
            public string ID { get; set; }
        }

        public class QualityControlReviewConclusions
        {
            public List<QualityControlReviewConclusion> qualityControlReviewConclusion { get; set; }
        }

        public class QualityControlReviewConclusion
        {
            public string transactionId { get; set; }
            public string sendToReview { get; set; }
            public string userName { get; set; }
        }

        public class AccountReference
        {
            public List<HccIdentifier> hccIdentifier { get; set; }
            public string otherIdentificationNumber { get; set; }
            public IdentificationType identificationType { get; set; }
            public string taxId { get; set; }
            public string asOfDate { get; set; }
        }

        public class HccIdentifier
        {
            public string accountHccIdentificationNumber { get; set; }
            public string parentAccountHccIdentificationNumber { get; set; }
            public string topAccountHccIdentificationNumber { get; set; }
        }

        public class IdentificationType
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class VipReasonCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class InformationSourceCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class CobraSelection
        {
            public string cobraStart { get; set; }
            public string cobraEnd { get; set; }
            public string cobraReceiptDate { get; set; }
            public string cobraPaidThroughDate { get; set; }
            public string comment { get; set; }
            public CobraQualifyingEventCode cobraQualifyingEventCode { get; set; }
        }

        public class CobraQualifyingEventCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class PayeeBankAccount
        {
            public string instanceActiveCode { get; set; }
            public string bankAccountId { get; set; }
            public string oldBankAccountId { get; set; }
            public string endorsementEffectiveDate { get; set; }
            public string endorsementExpirationDate { get; set; }
            public Payee payee { get; set; }
            public List<PayeeBankAccountDateRange> payeeBankAccountDateRange { get; set; }
            public PayeeBankAccountDateRanges payeeBankAccountDateRanges { get; set; }
            public string maintenanceComment { get; set; }
            public MaintenanceReasonCode maintenanceReasonCode { get; set; }
        }

        public class Payee
        {
            public string payeeSupplierHccId { get; set; }
            public string payeeSupplierLocationHccId { get; set; }
            public string payeeBrokerHccId { get; set; }
            public string payeeSubscriptionHccId { get; set; }
        }

        public class PayeeBankAccountDateRange
        {
            public string startDate { get; set; }
            public string endDate { get; set; }
            public string routingNumber { get; set; }
            public string accountNumber { get; set; }
            public string bankName { get; set; }
            public CountryCode countryCode { get; set; }
            public string accountOwner { get; set; }
            public BankAccountType bankAccountType { get; set; }
            public ExternalUsageType externalUsageType { get; set; }
        }

        public class CountryCode
        {
            public CountryCode countryCode { get; set; }
        }

        public class BankAccountType
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class ExternalUsageType
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class PayeeBankAccountDateRanges
        {
            public List<PayeeBankAccountDateRange> payeeBankAccountDateRange { get; set; }
        }

        public class MaintenanceReasonCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class Errors
        {
            public List<Error> error { get; set; }
        }

        public class Error
        {
            public string errorType { get; set; }
            public string message { get; set; }
            public string dateTime { get; set; }
            public string authenticatedUser { get; set; }
            public string traceInfo { get; set; }
        }

    }
}

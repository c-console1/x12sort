namespace DataLayer.HealthRulesConnectors.Models.Responses
{
    public class Fault
    {
        public string faultcode { get; set; }
        public string faultstring { get; set; }

        public bool IsError
        {
            get
            {
                return !string.IsNullOrWhiteSpace(faultcode);
            }
        }

    }
}

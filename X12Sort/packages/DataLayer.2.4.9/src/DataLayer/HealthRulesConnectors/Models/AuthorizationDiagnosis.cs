using DataLayer.Base.Models;
using System;

namespace DataLayer.HealthRulesConnectors.Models
{
    public class AuthorizationDiagnosis : IAuthorizationDiagnosis
    {
        public string Code { get; set; }
        public string DiagnosisType { get; set; }
        public string Description { get; set; }
        public string DiagnosisName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}

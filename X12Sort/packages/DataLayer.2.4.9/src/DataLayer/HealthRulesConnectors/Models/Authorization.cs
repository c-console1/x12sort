using System.Collections.Generic;

namespace DataLayer.HealthRulesConnectors.Models
{

    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://schemas.xmlsoap.org/soap/envelope/", IsNullable = false)]
    public partial class Envelope
    {

        private object headerField;

        private EnvelopeBody bodyField;

        /// <remarks/>
        public object Header
        {
            get
            {
                return this.headerField;
            }
            set
            {
                this.headerField = value;
            }
        }

        /// <remarks/>
        public EnvelopeBody Body
        {
            get
            {
                return this.bodyField;
            }
            set
            {
                this.bodyField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public partial class EnvelopeBody
    {

        private serviceAuthorization serviceAuthorizationField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.healthedge.com/connector/schema/authorization")]
        public serviceAuthorization serviceAuthorization
        {
            get
            {
                return this.serviceAuthorizationField;
            }
            set
            {
                this.serviceAuthorizationField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.healthedge.com/connector/schema/authorization")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.healthedge.com/connector/schema/authorization", IsNullable = false)]
    public partial class serviceAuthorization
    {

        private string authorizationInternalIdField;

        private string recordTypeField;

        private string authorizationIdField;

        private string externalAuthorizationIdField;

        private string authorizationStatusField;

        private List<traceInfo> traceInfoField;

        private maintenanceInfo maintenanceInfoField;

        private string inputSourceField;

        private string requestDateField;

        private string receivedDateField;

        private string requestedAdmitStatusCodeField;

        private string patientTrackingNumberField;

        private string externalAuthBatchNumberField;

        private performingProvider performingProviderField;

        private requestingProvider requestingProviderField;

        private admissionInformation admissionInformationField;

        private string servicesActionField;

        private List<services> servicesField;

        private List<diagnosis> diagnosisField;

        private string acuityField;

        private string authSeverityField;

        private typeOfBill typeOfBillField;

        private string placeOfServiceField;

        private string releaseOfInformationField;

        private string memberIdentifierField;

        private string memberSystemIdField;

        private memberMatchInput memberMatchInputField;

        private authorizationMatchInput authorizationMatchInputField;

        private string mdcCodeField;

        private List<clinicalNotes> clinicalNotesField;

        private string udtActionField;

        private userDefinedTerm userDefinedTermField;

        private List<udt> udtField;

        private List<authorizationAttachments> authorizationAttachmentsField;

        private string allowWorkbasketAmendField;

        private string resourceURIField;

        private string versionField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public string authorizationInternalId
        {
            get
            {
                return this.authorizationInternalIdField;
            }
            set
            {
                this.authorizationInternalIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public string recordType
        {
            get
            {
                return this.recordTypeField;
            }
            set
            {
                this.recordTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public string authorizationId
        {
            get
            {
                return this.authorizationIdField;
            }
            set
            {
                this.authorizationIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public string externalAuthorizationId
        {
            get
            {
                return this.externalAuthorizationIdField;
            }
            set
            {
                this.externalAuthorizationIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public string authorizationStatus
        {
            get
            {
                return this.authorizationStatusField;
            }
            set
            {
                this.authorizationStatusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public List<traceInfo> traceInfo
        {
            get
            {
                return this.traceInfoField;
            }
            set
            {
                this.traceInfoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public maintenanceInfo maintenanceInfo
        {
            get
            {
                return this.maintenanceInfoField;
            }
            set
            {
                this.maintenanceInfoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public string inputSource
        {
            get
            {
                return this.inputSourceField;
            }
            set
            {
                this.inputSourceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public string requestDate
        {
            get
            {
                return this.requestDateField;
            }
            set
            {
                this.requestDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public string receivedDate
        {
            get
            {
                return this.receivedDateField;
            }
            set
            {
                this.receivedDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public string requestedAdmitStatusCode
        {
            get
            {
                return this.requestedAdmitStatusCodeField;
            }
            set
            {
                this.requestedAdmitStatusCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public string patientTrackingNumber
        {
            get
            {
                return this.patientTrackingNumberField;
            }
            set
            {
                this.patientTrackingNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public string externalAuthBatchNumber
        {
            get
            {
                return this.externalAuthBatchNumberField;
            }
            set
            {
                this.externalAuthBatchNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public performingProvider performingProvider
        {
            get
            {
                return this.performingProviderField;
            }
            set
            {
                this.performingProviderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public requestingProvider requestingProvider
        {
            get
            {
                return this.requestingProviderField;
            }
            set
            {
                this.requestingProviderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public admissionInformation admissionInformation
        {
            get
            {
                return this.admissionInformationField;
            }
            set
            {
                this.admissionInformationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public string servicesAction
        {
            get
            {
                return this.servicesActionField;
            }
            set
            {
                this.servicesActionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public List<services> services
        {
            get
            {
                return this.servicesField;
            }
            set
            {
                this.servicesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public List<diagnosis> diagnosis
        {
            get
            {
                return this.diagnosisField;
            }
            set
            {
                this.diagnosisField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public string acuity
        {
            get
            {
                return this.acuityField;
            }
            set
            {
                this.acuityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public string authSeverity
        {
            get
            {
                return this.authSeverityField;
            }
            set
            {
                this.authSeverityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public typeOfBill typeOfBill
        {
            get
            {
                return this.typeOfBillField;
            }
            set
            {
                this.typeOfBillField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public string placeOfService
        {
            get
            {
                return this.placeOfServiceField;
            }
            set
            {
                this.placeOfServiceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public string releaseOfInformation
        {
            get
            {
                return this.releaseOfInformationField;
            }
            set
            {
                this.releaseOfInformationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public string memberIdentifier
        {
            get
            {
                return this.memberIdentifierField;
            }
            set
            {
                this.memberIdentifierField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public string memberSystemId
        {
            get
            {
                return this.memberSystemIdField;
            }
            set
            {
                this.memberSystemIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public memberMatchInput memberMatchInput
        {
            get
            {
                return this.memberMatchInputField;
            }
            set
            {
                this.memberMatchInputField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public authorizationMatchInput authorizationMatchInput
        {
            get
            {
                return this.authorizationMatchInputField;
            }
            set
            {
                this.authorizationMatchInputField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public string mdcCode
        {
            get
            {
                return this.mdcCodeField;
            }
            set
            {
                this.mdcCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public List<clinicalNotes> clinicalNotes
        {
            get
            {
                return this.clinicalNotesField;
            }
            set
            {
                this.clinicalNotesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public string udtAction
        {
            get
            {
                return this.udtActionField;
            }
            set
            {
                this.udtActionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public userDefinedTerm userDefinedTerm
        {
            get
            {
                return this.userDefinedTermField;
            }
            set
            {
                this.userDefinedTermField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public List<udt> udt
        {
            get
            {
                return this.udtField;
            }
            set
            {
                this.udtField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public List<authorizationAttachments> authorizationAttachments
        {
            get
            {
                return this.authorizationAttachmentsField;
            }
            set
            {
                this.authorizationAttachmentsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public string allowWorkbasketAmend
        {
            get
            {
                return this.allowWorkbasketAmendField;
            }
            set
            {
                this.allowWorkbasketAmendField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string resourceURI
        {
            get
            {
                return this.resourceURIField;
            }
            set
            {
                this.resourceURIField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string version
        {
            get
            {
                return this.versionField;
            }
            set
            {
                this.versionField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class traceInfo
    {

        private string traceEntityIdField;

        private string traceAdditionalIdField;

        /// <remarks/>
        public string traceEntityId
        {
            get
            {
                return this.traceEntityIdField;
            }
            set
            {
                this.traceEntityIdField = value;
            }
        }

        /// <remarks/>
        public string traceAdditionalId
        {
            get
            {
                return this.traceAdditionalIdField;
            }
            set
            {
                this.traceAdditionalIdField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class maintenanceInfo
    {

        private string transactionTypeField;

        private string overrideActionField;

        private string overrideResultField;

        /// <remarks/>
        public string transactionType
        {
            get
            {
                return this.transactionTypeField;
            }
            set
            {
                this.transactionTypeField = value;
            }
        }

        /// <remarks/>
        public string overrideAction
        {
            get
            {
                return this.overrideActionField;
            }
            set
            {
                this.overrideActionField = value;
            }
        }

        /// <remarks/>
        public string overrideResult
        {
            get
            {
                return this.overrideResultField;
            }
            set
            {
                this.overrideResultField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class performingProvider
    {

        private string providerTypeField;

        private string providerSystemIdField;

        private string providerPlanIdField;

        private string providerNPIIdField;

        private string providerTINIdField;

        private string providerIdentifierField;

        private string providerTaxonomyCodeField;

        private string organizationNameField;

        private performingProviderIndividualName individualNameField;

        private performingProviderAddress addressField;

        private List<performingProviderPhoneNumbers> phoneNumbersField;

        /// <remarks/>
        public string providerType
        {
            get
            {
                return this.providerTypeField;
            }
            set
            {
                this.providerTypeField = value;
            }
        }

        /// <remarks/>
        public string providerSystemId
        {
            get
            {
                return this.providerSystemIdField;
            }
            set
            {
                this.providerSystemIdField = value;
            }
        }

        /// <remarks/>
        public string providerPlanId
        {
            get
            {
                return this.providerPlanIdField;
            }
            set
            {
                this.providerPlanIdField = value;
            }
        }

        /// <remarks/>
        public string providerNPIId
        {
            get
            {
                return this.providerNPIIdField;
            }
            set
            {
                this.providerNPIIdField = value;
            }
        }

        /// <remarks/>
        public string providerTINId
        {
            get
            {
                return this.providerTINIdField;
            }
            set
            {
                this.providerTINIdField = value;
            }
        }

        /// <remarks/>
        public string providerIdentifier
        {
            get
            {
                return this.providerIdentifierField;
            }
            set
            {
                this.providerIdentifierField = value;
            }
        }

        /// <remarks/>
        public string providerTaxonomyCode
        {
            get
            {
                return this.providerTaxonomyCodeField;
            }
            set
            {
                this.providerTaxonomyCodeField = value;
            }
        }

        /// <remarks/>
        public string organizationName
        {
            get
            {
                return this.organizationNameField;
            }
            set
            {
                this.organizationNameField = value;
            }
        }

        /// <remarks/>
        public performingProviderIndividualName individualName
        {
            get
            {
                return this.individualNameField;
            }
            set
            {
                this.individualNameField = value;
            }
        }

        /// <remarks/>
        public performingProviderAddress address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>
        public List<performingProviderPhoneNumbers> phoneNumbers
        {
            get
            {
                return this.phoneNumbersField;
            }
            set
            {
                this.phoneNumbersField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class performingProviderIndividualName
    {

        private string lastNameField;

        private string firstNameField;

        private string middleNameField;

        private string namePrefixField;

        private string nameSuffixField;

        private performingProviderIndividualNameNameSuffixList nameSuffixListField;

        private List<performingProviderIndividualNameCredentialSuffixList> credentialSuffixListField;

        /// <remarks/>
        public string lastName
        {
            get
            {
                return this.lastNameField;
            }
            set
            {
                this.lastNameField = value;
            }
        }

        /// <remarks/>
        public string firstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public string middleName
        {
            get
            {
                return this.middleNameField;
            }
            set
            {
                this.middleNameField = value;
            }
        }

        /// <remarks/>
        public string namePrefix
        {
            get
            {
                return this.namePrefixField;
            }
            set
            {
                this.namePrefixField = value;
            }
        }

        /// <remarks/>
        public string nameSuffix
        {
            get
            {
                return this.nameSuffixField;
            }
            set
            {
                this.nameSuffixField = value;
            }
        }

        /// <remarks/>
        public performingProviderIndividualNameNameSuffixList nameSuffixList
        {
            get
            {
                return this.nameSuffixListField;
            }
            set
            {
                this.nameSuffixListField = value;
            }
        }

        /// <remarks/>
        public List<performingProviderIndividualNameCredentialSuffixList> credentialSuffixList
        {
            get
            {
                return this.credentialSuffixListField;
            }
            set
            {
                this.credentialSuffixListField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class performingProviderIndividualNameNameSuffixList
    {

        private string listModeField;

        private List<string> suffixField;

        /// <remarks/>
        public string listMode
        {
            get
            {
                return this.listModeField;
            }
            set
            {
                this.listModeField = value;
            }
        }

        /// <remarks/>
        public List<string> suffix
        {
            get
            {
                return this.suffixField;
            }
            set
            {
                this.suffixField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class performingProviderIndividualNameCredentialSuffixList
    {

        private string listModeField;

        private List<performingProviderIndividualNameCredentialSuffixListCredentialSuffix> credentialSuffixField;

        /// <remarks/>
        public string listMode
        {
            get
            {
                return this.listModeField;
            }
            set
            {
                this.listModeField = value;
            }
        }

        /// <remarks/>
        public List<performingProviderIndividualNameCredentialSuffixListCredentialSuffix> CredentialSuffix
        {
            get
            {
                return this.credentialSuffixField;
            }
            set
            {
                this.credentialSuffixField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class performingProviderIndividualNameCredentialSuffixListCredentialSuffix
    {

        private string codeSetNameField;

        private string codeEntryField;

        private string shortNameField;

        /// <remarks/>
        public string codeSetName
        {
            get
            {
                return this.codeSetNameField;
            }
            set
            {
                this.codeSetNameField = value;
            }
        }

        /// <remarks/>
        public string codeEntry
        {
            get
            {
                return this.codeEntryField;
            }
            set
            {
                this.codeEntryField = value;
            }
        }

        /// <remarks/>
        public string shortName
        {
            get
            {
                return this.shortNameField;
            }
            set
            {
                this.shortNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class performingProviderAddress
    {

        private string addressField;

        private string address2Field;

        private string address3Field;

        private string stateCodeField;

        private string zipCodeField;

        private string zipExtensionCodeField;

        private string cityNameField;

        private string countyCodeField;

        private performingProviderAddressCountryCode countryCodeField;

        private string longitudeField;

        private string latitudeField;

        private string ignoreAddressCheckField;

        private string undeliverableAddressField;

        /// <remarks/>
        public string address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>
        public string address2
        {
            get
            {
                return this.address2Field;
            }
            set
            {
                this.address2Field = value;
            }
        }

        /// <remarks/>
        public string address3
        {
            get
            {
                return this.address3Field;
            }
            set
            {
                this.address3Field = value;
            }
        }

        /// <remarks/>
        public string stateCode
        {
            get
            {
                return this.stateCodeField;
            }
            set
            {
                this.stateCodeField = value;
            }
        }

        /// <remarks/>
        public string zipCode
        {
            get
            {
                return this.zipCodeField;
            }
            set
            {
                this.zipCodeField = value;
            }
        }

        /// <remarks/>
        public string zipExtensionCode
        {
            get
            {
                return this.zipExtensionCodeField;
            }
            set
            {
                this.zipExtensionCodeField = value;
            }
        }

        /// <remarks/>
        public string cityName
        {
            get
            {
                return this.cityNameField;
            }
            set
            {
                this.cityNameField = value;
            }
        }

        /// <remarks/>
        public string countyCode
        {
            get
            {
                return this.countyCodeField;
            }
            set
            {
                this.countyCodeField = value;
            }
        }

        /// <remarks/>
        public performingProviderAddressCountryCode countryCode
        {
            get
            {
                return this.countryCodeField;
            }
            set
            {
                this.countryCodeField = value;
            }
        }

        /// <remarks/>
        public string longitude
        {
            get
            {
                return this.longitudeField;
            }
            set
            {
                this.longitudeField = value;
            }
        }

        /// <remarks/>
        public string latitude
        {
            get
            {
                return this.latitudeField;
            }
            set
            {
                this.latitudeField = value;
            }
        }

        /// <remarks/>
        public string ignoreAddressCheck
        {
            get
            {
                return this.ignoreAddressCheckField;
            }
            set
            {
                this.ignoreAddressCheckField = value;
            }
        }

        /// <remarks/>
        public string undeliverableAddress
        {
            get
            {
                return this.undeliverableAddressField;
            }
            set
            {
                this.undeliverableAddressField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class performingProviderAddressCountryCode
    {

        private string countryCodeField;

        /// <remarks/>
        public string countryCode
        {
            get
            {
                return this.countryCodeField;
            }
            set
            {
                this.countryCodeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class performingProviderPhoneNumbers
    {

        private string phoneCountryCodeField;

        private string phoneAreaCodeField;

        private string phoneNumberField;

        private string phoneExtensionNumberField;

        private performingProviderPhoneNumbersIndividualPhoneTypeCode individualPhoneTypeCodeField;

        private performingProviderPhoneNumbersOrganizationPhoneTypeCode organizationPhoneTypeCodeField;

        /// <remarks/>
        public string phoneCountryCode
        {
            get
            {
                return this.phoneCountryCodeField;
            }
            set
            {
                this.phoneCountryCodeField = value;
            }
        }

        /// <remarks/>
        public string phoneAreaCode
        {
            get
            {
                return this.phoneAreaCodeField;
            }
            set
            {
                this.phoneAreaCodeField = value;
            }
        }

        /// <remarks/>
        public string phoneNumber
        {
            get
            {
                return this.phoneNumberField;
            }
            set
            {
                this.phoneNumberField = value;
            }
        }

        /// <remarks/>
        public string phoneExtensionNumber
        {
            get
            {
                return this.phoneExtensionNumberField;
            }
            set
            {
                this.phoneExtensionNumberField = value;
            }
        }

        /// <remarks/>
        public performingProviderPhoneNumbersIndividualPhoneTypeCode individualPhoneTypeCode
        {
            get
            {
                return this.individualPhoneTypeCodeField;
            }
            set
            {
                this.individualPhoneTypeCodeField = value;
            }
        }

        /// <remarks/>
        public performingProviderPhoneNumbersOrganizationPhoneTypeCode organizationPhoneTypeCode
        {
            get
            {
                return this.organizationPhoneTypeCodeField;
            }
            set
            {
                this.organizationPhoneTypeCodeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class performingProviderPhoneNumbersIndividualPhoneTypeCode
    {

        private string codeSetNameField;

        private string codeEntryField;

        private string shortNameField;

        /// <remarks/>
        public string codeSetName
        {
            get
            {
                return this.codeSetNameField;
            }
            set
            {
                this.codeSetNameField = value;
            }
        }

        /// <remarks/>
        public string codeEntry
        {
            get
            {
                return this.codeEntryField;
            }
            set
            {
                this.codeEntryField = value;
            }
        }

        /// <remarks/>
        public string shortName
        {
            get
            {
                return this.shortNameField;
            }
            set
            {
                this.shortNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class performingProviderPhoneNumbersOrganizationPhoneTypeCode
    {

        private string codeSetNameField;

        private string codeEntryField;

        private string shortNameField;

        /// <remarks/>
        public string codeSetName
        {
            get
            {
                return this.codeSetNameField;
            }
            set
            {
                this.codeSetNameField = value;
            }
        }

        /// <remarks/>
        public string codeEntry
        {
            get
            {
                return this.codeEntryField;
            }
            set
            {
                this.codeEntryField = value;
            }
        }

        /// <remarks/>
        public string shortName
        {
            get
            {
                return this.shortNameField;
            }
            set
            {
                this.shortNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class requestingProvider
    {

        private string providerTypeField;

        private string providerSystemIdField;

        private string providerPlanIdField;

        private string providerNPIIdField;

        private string providerTINIdField;

        private string providerIdentifierField;

        private string providerTaxonomyCodeField;

        private string organizationNameField;

        private requestingProviderIndividualName individualNameField;

        private requestingProviderAddress addressField;

        private List<requestingProviderPhoneNumbers> phoneNumbersField;

        /// <remarks/>
        public string providerType
        {
            get
            {
                return this.providerTypeField;
            }
            set
            {
                this.providerTypeField = value;
            }
        }

        /// <remarks/>
        public string providerSystemId
        {
            get
            {
                return this.providerSystemIdField;
            }
            set
            {
                this.providerSystemIdField = value;
            }
        }

        /// <remarks/>
        public string providerPlanId
        {
            get
            {
                return this.providerPlanIdField;
            }
            set
            {
                this.providerPlanIdField = value;
            }
        }

        /// <remarks/>
        public string providerNPIId
        {
            get
            {
                return this.providerNPIIdField;
            }
            set
            {
                this.providerNPIIdField = value;
            }
        }

        /// <remarks/>
        public string providerTINId
        {
            get
            {
                return this.providerTINIdField;
            }
            set
            {
                this.providerTINIdField = value;
            }
        }

        /// <remarks/>
        public string providerIdentifier
        {
            get
            {
                return this.providerIdentifierField;
            }
            set
            {
                this.providerIdentifierField = value;
            }
        }

        /// <remarks/>
        public string providerTaxonomyCode
        {
            get
            {
                return this.providerTaxonomyCodeField;
            }
            set
            {
                this.providerTaxonomyCodeField = value;
            }
        }

        /// <remarks/>
        public string organizationName
        {
            get
            {
                return this.organizationNameField;
            }
            set
            {
                this.organizationNameField = value;
            }
        }

        /// <remarks/>
        public requestingProviderIndividualName individualName
        {
            get
            {
                return this.individualNameField;
            }
            set
            {
                this.individualNameField = value;
            }
        }

        /// <remarks/>
        public requestingProviderAddress address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>
        public List<requestingProviderPhoneNumbers> phoneNumbers
        {
            get
            {
                return this.phoneNumbersField;
            }
            set
            {
                this.phoneNumbersField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class requestingProviderIndividualName
    {

        private string lastNameField;

        private string firstNameField;

        private string middleNameField;

        private string namePrefixField;

        private string nameSuffixField;

        private requestingProviderIndividualNameNameSuffixList nameSuffixListField;

        private List<requestingProviderIndividualNameCredentialSuffixList> credentialSuffixListField;

        /// <remarks/>
        public string lastName
        {
            get
            {
                return this.lastNameField;
            }
            set
            {
                this.lastNameField = value;
            }
        }

        /// <remarks/>
        public string firstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public string middleName
        {
            get
            {
                return this.middleNameField;
            }
            set
            {
                this.middleNameField = value;
            }
        }

        /// <remarks/>
        public string namePrefix
        {
            get
            {
                return this.namePrefixField;
            }
            set
            {
                this.namePrefixField = value;
            }
        }

        /// <remarks/>
        public string nameSuffix
        {
            get
            {
                return this.nameSuffixField;
            }
            set
            {
                this.nameSuffixField = value;
            }
        }

        /// <remarks/>
        public requestingProviderIndividualNameNameSuffixList nameSuffixList
        {
            get
            {
                return this.nameSuffixListField;
            }
            set
            {
                this.nameSuffixListField = value;
            }
        }

        /// <remarks/>
        public List<requestingProviderIndividualNameCredentialSuffixList> credentialSuffixList
        {
            get
            {
                return this.credentialSuffixListField;
            }
            set
            {
                this.credentialSuffixListField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class requestingProviderIndividualNameNameSuffixList
    {

        private string listModeField;

        private List<string> suffixField;

        /// <remarks/>
        public string listMode
        {
            get
            {
                return this.listModeField;
            }
            set
            {
                this.listModeField = value;
            }
        }

        /// <remarks/>
        public List<string> suffix
        {
            get
            {
                return this.suffixField;
            }
            set
            {
                this.suffixField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class requestingProviderIndividualNameCredentialSuffixList
    {

        private string listModeField;

        private List<requestingProviderIndividualNameCredentialSuffixListCredentialSuffix> credentialSuffixField;

        /// <remarks/>
        public string listMode
        {
            get
            {
                return this.listModeField;
            }
            set
            {
                this.listModeField = value;
            }
        }

        /// <remarks/>
        public List<requestingProviderIndividualNameCredentialSuffixListCredentialSuffix> CredentialSuffix
        {
            get
            {
                return this.credentialSuffixField;
            }
            set
            {
                this.credentialSuffixField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class requestingProviderIndividualNameCredentialSuffixListCredentialSuffix
    {

        private string codeSetNameField;

        private string codeEntryField;

        private string shortNameField;

        /// <remarks/>
        public string codeSetName
        {
            get
            {
                return this.codeSetNameField;
            }
            set
            {
                this.codeSetNameField = value;
            }
        }

        /// <remarks/>
        public string codeEntry
        {
            get
            {
                return this.codeEntryField;
            }
            set
            {
                this.codeEntryField = value;
            }
        }

        /// <remarks/>
        public string shortName
        {
            get
            {
                return this.shortNameField;
            }
            set
            {
                this.shortNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class requestingProviderAddress
    {

        private string addressField;

        private string address2Field;

        private string address3Field;

        private string stateCodeField;

        private string zipCodeField;

        private string zipExtensionCodeField;

        private string cityNameField;

        private string countyCodeField;

        private requestingProviderAddressCountryCode countryCodeField;

        private string longitudeField;

        private string latitudeField;

        private string ignoreAddressCheckField;

        private string undeliverableAddressField;

        /// <remarks/>
        public string address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>
        public string address2
        {
            get
            {
                return this.address2Field;
            }
            set
            {
                this.address2Field = value;
            }
        }

        /// <remarks/>
        public string address3
        {
            get
            {
                return this.address3Field;
            }
            set
            {
                this.address3Field = value;
            }
        }

        /// <remarks/>
        public string stateCode
        {
            get
            {
                return this.stateCodeField;
            }
            set
            {
                this.stateCodeField = value;
            }
        }

        /// <remarks/>
        public string zipCode
        {
            get
            {
                return this.zipCodeField;
            }
            set
            {
                this.zipCodeField = value;
            }
        }

        /// <remarks/>
        public string zipExtensionCode
        {
            get
            {
                return this.zipExtensionCodeField;
            }
            set
            {
                this.zipExtensionCodeField = value;
            }
        }

        /// <remarks/>
        public string cityName
        {
            get
            {
                return this.cityNameField;
            }
            set
            {
                this.cityNameField = value;
            }
        }

        /// <remarks/>
        public string countyCode
        {
            get
            {
                return this.countyCodeField;
            }
            set
            {
                this.countyCodeField = value;
            }
        }

        /// <remarks/>
        public requestingProviderAddressCountryCode countryCode
        {
            get
            {
                return this.countryCodeField;
            }
            set
            {
                this.countryCodeField = value;
            }
        }

        /// <remarks/>
        public string longitude
        {
            get
            {
                return this.longitudeField;
            }
            set
            {
                this.longitudeField = value;
            }
        }

        /// <remarks/>
        public string latitude
        {
            get
            {
                return this.latitudeField;
            }
            set
            {
                this.latitudeField = value;
            }
        }

        /// <remarks/>
        public string ignoreAddressCheck
        {
            get
            {
                return this.ignoreAddressCheckField;
            }
            set
            {
                this.ignoreAddressCheckField = value;
            }
        }

        /// <remarks/>
        public string undeliverableAddress
        {
            get
            {
                return this.undeliverableAddressField;
            }
            set
            {
                this.undeliverableAddressField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class requestingProviderAddressCountryCode
    {

        private string countryCodeField;

        /// <remarks/>
        public string countryCode
        {
            get
            {
                return this.countryCodeField;
            }
            set
            {
                this.countryCodeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class requestingProviderPhoneNumbers
    {

        private string phoneCountryCodeField;

        private string phoneAreaCodeField;

        private string phoneNumberField;

        private string phoneExtensionNumberField;

        private requestingProviderPhoneNumbersIndividualPhoneTypeCode individualPhoneTypeCodeField;

        private requestingProviderPhoneNumbersOrganizationPhoneTypeCode organizationPhoneTypeCodeField;

        /// <remarks/>
        public string phoneCountryCode
        {
            get
            {
                return this.phoneCountryCodeField;
            }
            set
            {
                this.phoneCountryCodeField = value;
            }
        }

        /// <remarks/>
        public string phoneAreaCode
        {
            get
            {
                return this.phoneAreaCodeField;
            }
            set
            {
                this.phoneAreaCodeField = value;
            }
        }

        /// <remarks/>
        public string phoneNumber
        {
            get
            {
                return this.phoneNumberField;
            }
            set
            {
                this.phoneNumberField = value;
            }
        }

        /// <remarks/>
        public string phoneExtensionNumber
        {
            get
            {
                return this.phoneExtensionNumberField;
            }
            set
            {
                this.phoneExtensionNumberField = value;
            }
        }

        /// <remarks/>
        public requestingProviderPhoneNumbersIndividualPhoneTypeCode individualPhoneTypeCode
        {
            get
            {
                return this.individualPhoneTypeCodeField;
            }
            set
            {
                this.individualPhoneTypeCodeField = value;
            }
        }

        /// <remarks/>
        public requestingProviderPhoneNumbersOrganizationPhoneTypeCode organizationPhoneTypeCode
        {
            get
            {
                return this.organizationPhoneTypeCodeField;
            }
            set
            {
                this.organizationPhoneTypeCodeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class requestingProviderPhoneNumbersIndividualPhoneTypeCode
    {

        private string codeSetNameField;

        private string codeEntryField;

        private string shortNameField;

        /// <remarks/>
        public string codeSetName
        {
            get
            {
                return this.codeSetNameField;
            }
            set
            {
                this.codeSetNameField = value;
            }
        }

        /// <remarks/>
        public string codeEntry
        {
            get
            {
                return this.codeEntryField;
            }
            set
            {
                this.codeEntryField = value;
            }
        }

        /// <remarks/>
        public string shortName
        {
            get
            {
                return this.shortNameField;
            }
            set
            {
                this.shortNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class requestingProviderPhoneNumbersOrganizationPhoneTypeCode
    {

        private string codeSetNameField;

        private string codeEntryField;

        private string shortNameField;

        /// <remarks/>
        public string codeSetName
        {
            get
            {
                return this.codeSetNameField;
            }
            set
            {
                this.codeSetNameField = value;
            }
        }

        /// <remarks/>
        public string codeEntry
        {
            get
            {
                return this.codeEntryField;
            }
            set
            {
                this.codeEntryField = value;
            }
        }

        /// <remarks/>
        public string shortName
        {
            get
            {
                return this.shortNameField;
            }
            set
            {
                this.shortNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class admissionInformation
    {

        private string expectedAdmissionDateRangeStartField;

        private string expectedAdmissionDateRangeEndField;

        private string actualAdmissionDateField;

        private string expectedDischargeDateRangeStartField;

        private string expectedDischargeDateRangeEndField;

        private string actualDischargeDateField;

        private string admissionSourceField;

        private string admissionTypeField;

        private admissionInformationDischargeStatus dischargeStatusField;

        /// <remarks/>
        public string expectedAdmissionDateRangeStart
        {
            get
            {
                return this.expectedAdmissionDateRangeStartField;
            }
            set
            {
                this.expectedAdmissionDateRangeStartField = value;
            }
        }

        /// <remarks/>
        public string expectedAdmissionDateRangeEnd
        {
            get
            {
                return this.expectedAdmissionDateRangeEndField;
            }
            set
            {
                this.expectedAdmissionDateRangeEndField = value;
            }
        }

        /// <remarks/>
        public string actualAdmissionDate
        {
            get
            {
                return this.actualAdmissionDateField;
            }
            set
            {
                this.actualAdmissionDateField = value;
            }
        }

        /// <remarks/>
        public string expectedDischargeDateRangeStart
        {
            get
            {
                return this.expectedDischargeDateRangeStartField;
            }
            set
            {
                this.expectedDischargeDateRangeStartField = value;
            }
        }

        /// <remarks/>
        public string expectedDischargeDateRangeEnd
        {
            get
            {
                return this.expectedDischargeDateRangeEndField;
            }
            set
            {
                this.expectedDischargeDateRangeEndField = value;
            }
        }

        /// <remarks/>
        public string actualDischargeDate
        {
            get
            {
                return this.actualDischargeDateField;
            }
            set
            {
                this.actualDischargeDateField = value;
            }
        }

        /// <remarks/>
        public string admissionSource
        {
            get
            {
                return this.admissionSourceField;
            }
            set
            {
                this.admissionSourceField = value;
            }
        }

        /// <remarks/>
        public string admissionType
        {
            get
            {
                return this.admissionTypeField;
            }
            set
            {
                this.admissionTypeField = value;
            }
        }

        /// <remarks/>
        public admissionInformationDischargeStatus dischargeStatus
        {
            get
            {
                return this.dischargeStatusField;
            }
            set
            {
                this.dischargeStatusField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class admissionInformationDischargeStatus
    {

        private string idField;

        /// <remarks/>
        public string ID
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class services
    {

        private string serviceSetIdentifierField;

        private string externalServiceSetIdField;

        private string startServiceDateField;

        private string endServiceDateField;

        private string requestedStartServiceDateField;

        private string requestedEndServiceDateField;

        private string receivedDateField;

        private string clinicalReceiptDateField;

        private string serviceSetStatusField;

        private string noteField;

        private string approvedQuantityField;

        private string approvedUnitsField;

        private string approvedAmountField;

        private string requestedQuantityField;

        private string requestedUnitsField;

        private servicesRenderingProvider renderingProviderField;

        private servicesServiceType serviceTypeField;

        private List<servicesDiagnosis> diagnosisField;

        private servicesServiceAgreementDetail serviceAgreementDetailField;

        private servicesHicApproval hicApprovalField;

        private string authorizedStatusReasonCodeField;

        private string authorizationTypeCodeField;

        private string authorizationPriorityField;

        private servicesReviewType reviewTypeField;

        private servicesReviewLevel reviewLevelField;

        private servicesAuthorizationServiceSetMatchInput authorizationServiceSetMatchInputField;

        /// <remarks/>
        public string serviceSetIdentifier
        {
            get
            {
                return this.serviceSetIdentifierField;
            }
            set
            {
                this.serviceSetIdentifierField = value;
            }
        }

        /// <remarks/>
        public string externalServiceSetId
        {
            get
            {
                return this.externalServiceSetIdField;
            }
            set
            {
                this.externalServiceSetIdField = value;
            }
        }

        /// <remarks/>
        public string startServiceDate
        {
            get
            {
                return this.startServiceDateField;
            }
            set
            {
                this.startServiceDateField = value;
            }
        }

        /// <remarks/>
        public string endServiceDate
        {
            get
            {
                return this.endServiceDateField;
            }
            set
            {
                this.endServiceDateField = value;
            }
        }

        /// <remarks/>
        public string requestedStartServiceDate
        {
            get
            {
                return this.requestedStartServiceDateField;
            }
            set
            {
                this.requestedStartServiceDateField = value;
            }
        }

        /// <remarks/>
        public string requestedEndServiceDate
        {
            get
            {
                return this.requestedEndServiceDateField;
            }
            set
            {
                this.requestedEndServiceDateField = value;
            }
        }

        /// <remarks/>
        public string receivedDate
        {
            get
            {
                return this.receivedDateField;
            }
            set
            {
                this.receivedDateField = value;
            }
        }

        /// <remarks/>
        public string clinicalReceiptDate
        {
            get
            {
                return this.clinicalReceiptDateField;
            }
            set
            {
                this.clinicalReceiptDateField = value;
            }
        }

        /// <remarks/>
        public string serviceSetStatus
        {
            get
            {
                return this.serviceSetStatusField;
            }
            set
            {
                this.serviceSetStatusField = value;
            }
        }

        /// <remarks/>
        public string note
        {
            get
            {
                return this.noteField;
            }
            set
            {
                this.noteField = value;
            }
        }

        /// <remarks/>
        public string approvedQuantity
        {
            get
            {
                return this.approvedQuantityField;
            }
            set
            {
                this.approvedQuantityField = value;
            }
        }

        /// <remarks/>
        public string approvedUnits
        {
            get
            {
                return this.approvedUnitsField;
            }
            set
            {
                this.approvedUnitsField = value;
            }
        }

        /// <remarks/>
        public string approvedAmount
        {
            get
            {
                return this.approvedAmountField;
            }
            set
            {
                this.approvedAmountField = value;
            }
        }

        /// <remarks/>
        public string requestedQuantity
        {
            get
            {
                return this.requestedQuantityField;
            }
            set
            {
                this.requestedQuantityField = value;
            }
        }

        /// <remarks/>
        public string requestedUnits
        {
            get
            {
                return this.requestedUnitsField;
            }
            set
            {
                this.requestedUnitsField = value;
            }
        }

        /// <remarks/>
        public servicesRenderingProvider renderingProvider
        {
            get
            {
                return this.renderingProviderField;
            }
            set
            {
                this.renderingProviderField = value;
            }
        }

        /// <remarks/>
        public servicesServiceType serviceType
        {
            get
            {
                return this.serviceTypeField;
            }
            set
            {
                this.serviceTypeField = value;
            }
        }

        /// <remarks/>
        public List<servicesDiagnosis> diagnosis
        {
            get
            {
                return this.diagnosisField;
            }
            set
            {
                this.diagnosisField = value;
            }
        }

        /// <remarks/>
        public servicesServiceAgreementDetail serviceAgreementDetail
        {
            get
            {
                return this.serviceAgreementDetailField;
            }
            set
            {
                this.serviceAgreementDetailField = value;
            }
        }

        /// <remarks/>
        public servicesHicApproval hicApproval
        {
            get
            {
                return this.hicApprovalField;
            }
            set
            {
                this.hicApprovalField = value;
            }
        }

        /// <remarks/>
        public string authorizedStatusReasonCode
        {
            get
            {
                return this.authorizedStatusReasonCodeField;
            }
            set
            {
                this.authorizedStatusReasonCodeField = value;
            }
        }

        /// <remarks/>
        public string authorizationTypeCode
        {
            get
            {
                return this.authorizationTypeCodeField;
            }
            set
            {
                this.authorizationTypeCodeField = value;
            }
        }

        /// <remarks/>
        public string authorizationPriority
        {
            get
            {
                return this.authorizationPriorityField;
            }
            set
            {
                this.authorizationPriorityField = value;
            }
        }

        /// <remarks/>
        public servicesReviewType reviewType
        {
            get
            {
                return this.reviewTypeField;
            }
            set
            {
                this.reviewTypeField = value;
            }
        }

        /// <remarks/>
        public servicesReviewLevel reviewLevel
        {
            get
            {
                return this.reviewLevelField;
            }
            set
            {
                this.reviewLevelField = value;
            }
        }

        /// <remarks/>
        public servicesAuthorizationServiceSetMatchInput authorizationServiceSetMatchInput
        {
            get
            {
                return this.authorizationServiceSetMatchInputField;
            }
            set
            {
                this.authorizationServiceSetMatchInputField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class servicesRenderingProvider
    {

        private string providerTypeField;

        private string providerSystemIdField;

        private string providerPlanIdField;

        private string providerNPIIdField;

        private string providerTINIdField;

        private string providerIdentifierField;

        private string providerTaxonomyCodeField;

        private string organizationNameField;

        private servicesRenderingProviderIndividualName individualNameField;

        private servicesRenderingProviderAddress addressField;

        private List<servicesRenderingProviderPhoneNumbers> phoneNumbersField;

        /// <remarks/>
        public string providerType
        {
            get
            {
                return this.providerTypeField;
            }
            set
            {
                this.providerTypeField = value;
            }
        }

        /// <remarks/>
        public string providerSystemId
        {
            get
            {
                return this.providerSystemIdField;
            }
            set
            {
                this.providerSystemIdField = value;
            }
        }

        /// <remarks/>
        public string providerPlanId
        {
            get
            {
                return this.providerPlanIdField;
            }
            set
            {
                this.providerPlanIdField = value;
            }
        }

        /// <remarks/>
        public string providerNPIId
        {
            get
            {
                return this.providerNPIIdField;
            }
            set
            {
                this.providerNPIIdField = value;
            }
        }

        /// <remarks/>
        public string providerTINId
        {
            get
            {
                return this.providerTINIdField;
            }
            set
            {
                this.providerTINIdField = value;
            }
        }

        /// <remarks/>
        public string providerIdentifier
        {
            get
            {
                return this.providerIdentifierField;
            }
            set
            {
                this.providerIdentifierField = value;
            }
        }

        /// <remarks/>
        public string providerTaxonomyCode
        {
            get
            {
                return this.providerTaxonomyCodeField;
            }
            set
            {
                this.providerTaxonomyCodeField = value;
            }
        }

        /// <remarks/>
        public string organizationName
        {
            get
            {
                return this.organizationNameField;
            }
            set
            {
                this.organizationNameField = value;
            }
        }

        /// <remarks/>
        public servicesRenderingProviderIndividualName individualName
        {
            get
            {
                return this.individualNameField;
            }
            set
            {
                this.individualNameField = value;
            }
        }

        /// <remarks/>
        public servicesRenderingProviderAddress address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>
        public List<servicesRenderingProviderPhoneNumbers> phoneNumbers
        {
            get
            {
                return this.phoneNumbersField;
            }
            set
            {
                this.phoneNumbersField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class servicesRenderingProviderIndividualName
    {

        private string lastNameField;

        private string firstNameField;

        private string middleNameField;

        private string namePrefixField;

        private string nameSuffixField;

        private servicesRenderingProviderIndividualNameNameSuffixList nameSuffixListField;

        private List<servicesRenderingProviderIndividualNameCredentialSuffixList> credentialSuffixListField;

        /// <remarks/>
        public string lastName
        {
            get
            {
                return this.lastNameField;
            }
            set
            {
                this.lastNameField = value;
            }
        }

        /// <remarks/>
        public string firstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public string middleName
        {
            get
            {
                return this.middleNameField;
            }
            set
            {
                this.middleNameField = value;
            }
        }

        /// <remarks/>
        public string namePrefix
        {
            get
            {
                return this.namePrefixField;
            }
            set
            {
                this.namePrefixField = value;
            }
        }

        /// <remarks/>
        public string nameSuffix
        {
            get
            {
                return this.nameSuffixField;
            }
            set
            {
                this.nameSuffixField = value;
            }
        }

        /// <remarks/>
        public servicesRenderingProviderIndividualNameNameSuffixList nameSuffixList
        {
            get
            {
                return this.nameSuffixListField;
            }
            set
            {
                this.nameSuffixListField = value;
            }
        }

        /// <remarks/>
        public List<servicesRenderingProviderIndividualNameCredentialSuffixList> credentialSuffixList
        {
            get
            {
                return this.credentialSuffixListField;
            }
            set
            {
                this.credentialSuffixListField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class servicesRenderingProviderIndividualNameNameSuffixList
    {

        private string listModeField;

        private List<string> suffixField;

        /// <remarks/>
        public string listMode
        {
            get
            {
                return this.listModeField;
            }
            set
            {
                this.listModeField = value;
            }
        }

        /// <remarks/>
        public List<string> suffix
        {
            get
            {
                return this.suffixField;
            }
            set
            {
                this.suffixField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class servicesRenderingProviderIndividualNameCredentialSuffixList
    {

        private string listModeField;

        private List<servicesRenderingProviderIndividualNameCredentialSuffixListCredentialSuffix> credentialSuffixField;

        /// <remarks/>
        public string listMode
        {
            get
            {
                return this.listModeField;
            }
            set
            {
                this.listModeField = value;
            }
        }

        /// <remarks/>
        public List<servicesRenderingProviderIndividualNameCredentialSuffixListCredentialSuffix> CredentialSuffix
        {
            get
            {
                return this.credentialSuffixField;
            }
            set
            {
                this.credentialSuffixField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class servicesRenderingProviderIndividualNameCredentialSuffixListCredentialSuffix
    {

        private string codeSetNameField;

        private string codeEntryField;

        private string shortNameField;

        /// <remarks/>
        public string codeSetName
        {
            get
            {
                return this.codeSetNameField;
            }
            set
            {
                this.codeSetNameField = value;
            }
        }

        /// <remarks/>
        public string codeEntry
        {
            get
            {
                return this.codeEntryField;
            }
            set
            {
                this.codeEntryField = value;
            }
        }

        /// <remarks/>
        public string shortName
        {
            get
            {
                return this.shortNameField;
            }
            set
            {
                this.shortNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class servicesRenderingProviderAddress
    {

        private string addressField;

        private string address2Field;

        private string address3Field;

        private string stateCodeField;

        private string zipCodeField;

        private string zipExtensionCodeField;

        private string cityNameField;

        private string countyCodeField;

        private servicesRenderingProviderAddressCountryCode countryCodeField;

        private string longitudeField;

        private string latitudeField;

        private string ignoreAddressCheckField;

        private string undeliverableAddressField;

        /// <remarks/>
        public string address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>
        public string address2
        {
            get
            {
                return this.address2Field;
            }
            set
            {
                this.address2Field = value;
            }
        }

        /// <remarks/>
        public string address3
        {
            get
            {
                return this.address3Field;
            }
            set
            {
                this.address3Field = value;
            }
        }

        /// <remarks/>
        public string stateCode
        {
            get
            {
                return this.stateCodeField;
            }
            set
            {
                this.stateCodeField = value;
            }
        }

        /// <remarks/>
        public string zipCode
        {
            get
            {
                return this.zipCodeField;
            }
            set
            {
                this.zipCodeField = value;
            }
        }

        /// <remarks/>
        public string zipExtensionCode
        {
            get
            {
                return this.zipExtensionCodeField;
            }
            set
            {
                this.zipExtensionCodeField = value;
            }
        }

        /// <remarks/>
        public string cityName
        {
            get
            {
                return this.cityNameField;
            }
            set
            {
                this.cityNameField = value;
            }
        }

        /// <remarks/>
        public string countyCode
        {
            get
            {
                return this.countyCodeField;
            }
            set
            {
                this.countyCodeField = value;
            }
        }

        /// <remarks/>
        public servicesRenderingProviderAddressCountryCode countryCode
        {
            get
            {
                return this.countryCodeField;
            }
            set
            {
                this.countryCodeField = value;
            }
        }

        /// <remarks/>
        public string longitude
        {
            get
            {
                return this.longitudeField;
            }
            set
            {
                this.longitudeField = value;
            }
        }

        /// <remarks/>
        public string latitude
        {
            get
            {
                return this.latitudeField;
            }
            set
            {
                this.latitudeField = value;
            }
        }

        /// <remarks/>
        public string ignoreAddressCheck
        {
            get
            {
                return this.ignoreAddressCheckField;
            }
            set
            {
                this.ignoreAddressCheckField = value;
            }
        }

        /// <remarks/>
        public string undeliverableAddress
        {
            get
            {
                return this.undeliverableAddressField;
            }
            set
            {
                this.undeliverableAddressField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class servicesRenderingProviderAddressCountryCode
    {

        private string countryCodeField;

        /// <remarks/>
        public string countryCode
        {
            get
            {
                return this.countryCodeField;
            }
            set
            {
                this.countryCodeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class servicesRenderingProviderPhoneNumbers
    {

        private string phoneCountryCodeField;

        private string phoneAreaCodeField;

        private string phoneNumberField;

        private string phoneExtensionNumberField;

        private servicesRenderingProviderPhoneNumbersIndividualPhoneTypeCode individualPhoneTypeCodeField;

        private servicesRenderingProviderPhoneNumbersOrganizationPhoneTypeCode organizationPhoneTypeCodeField;

        /// <remarks/>
        public string phoneCountryCode
        {
            get
            {
                return this.phoneCountryCodeField;
            }
            set
            {
                this.phoneCountryCodeField = value;
            }
        }

        /// <remarks/>
        public string phoneAreaCode
        {
            get
            {
                return this.phoneAreaCodeField;
            }
            set
            {
                this.phoneAreaCodeField = value;
            }
        }

        /// <remarks/>
        public string phoneNumber
        {
            get
            {
                return this.phoneNumberField;
            }
            set
            {
                this.phoneNumberField = value;
            }
        }

        /// <remarks/>
        public string phoneExtensionNumber
        {
            get
            {
                return this.phoneExtensionNumberField;
            }
            set
            {
                this.phoneExtensionNumberField = value;
            }
        }

        /// <remarks/>
        public servicesRenderingProviderPhoneNumbersIndividualPhoneTypeCode individualPhoneTypeCode
        {
            get
            {
                return this.individualPhoneTypeCodeField;
            }
            set
            {
                this.individualPhoneTypeCodeField = value;
            }
        }

        /// <remarks/>
        public servicesRenderingProviderPhoneNumbersOrganizationPhoneTypeCode organizationPhoneTypeCode
        {
            get
            {
                return this.organizationPhoneTypeCodeField;
            }
            set
            {
                this.organizationPhoneTypeCodeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class servicesRenderingProviderPhoneNumbersIndividualPhoneTypeCode
    {

        private string codeSetNameField;

        private string codeEntryField;

        private string shortNameField;

        /// <remarks/>
        public string codeSetName
        {
            get
            {
                return this.codeSetNameField;
            }
            set
            {
                this.codeSetNameField = value;
            }
        }

        /// <remarks/>
        public string codeEntry
        {
            get
            {
                return this.codeEntryField;
            }
            set
            {
                this.codeEntryField = value;
            }
        }

        /// <remarks/>
        public string shortName
        {
            get
            {
                return this.shortNameField;
            }
            set
            {
                this.shortNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class servicesRenderingProviderPhoneNumbersOrganizationPhoneTypeCode
    {

        private string codeSetNameField;

        private string codeEntryField;

        private string shortNameField;

        /// <remarks/>
        public string codeSetName
        {
            get
            {
                return this.codeSetNameField;
            }
            set
            {
                this.codeSetNameField = value;
            }
        }

        /// <remarks/>
        public string codeEntry
        {
            get
            {
                return this.codeEntryField;
            }
            set
            {
                this.codeEntryField = value;
            }
        }

        /// <remarks/>
        public string shortName
        {
            get
            {
                return this.shortNameField;
            }
            set
            {
                this.shortNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class servicesServiceType
    {

        private string serviceSystemIdField;

        private string serviceCodeField;

        private string serviceCodeTypeField;

        private List<string> modifierCodeField;

        private string placeOfServiceField;

        private string referralCategoryTypeField;

        private string serviceTypeField;

        /// <remarks/>
        public string serviceSystemId
        {
            get
            {
                return this.serviceSystemIdField;
            }
            set
            {
                this.serviceSystemIdField = value;
            }
        }

        /// <remarks/>
        public string serviceCode
        {
            get
            {
                return this.serviceCodeField;
            }
            set
            {
                this.serviceCodeField = value;
            }
        }

        /// <remarks/>
        public string serviceCodeType
        {
            get
            {
                return this.serviceCodeTypeField;
            }
            set
            {
                this.serviceCodeTypeField = value;
            }
        }

        /// <remarks/>
        public List<string> modifierCode
        {
            get
            {
                return this.modifierCodeField;
            }
            set
            {
                this.modifierCodeField = value;
            }
        }

        /// <remarks/>
        public string placeOfService
        {
            get
            {
                return this.placeOfServiceField;
            }
            set
            {
                this.placeOfServiceField = value;
            }
        }

        /// <remarks/>
        public string referralCategoryType
        {
            get
            {
                return this.referralCategoryTypeField;
            }
            set
            {
                this.referralCategoryTypeField = value;
            }
        }

        /// <remarks/>
        public string serviceType
        {
            get
            {
                return this.serviceTypeField;
            }
            set
            {
                this.serviceTypeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class servicesDiagnosis
    {

        private string diagnosisSystemIdField;

        private string diagnosisTypeField;

        private string diagnosisCodeField;

        private string diagnosisCodeTypeField;

        /// <remarks/>
        public string diagnosisSystemId
        {
            get
            {
                return this.diagnosisSystemIdField;
            }
            set
            {
                this.diagnosisSystemIdField = value;
            }
        }

        /// <remarks/>
        public string diagnosisType
        {
            get
            {
                return this.diagnosisTypeField;
            }
            set
            {
                this.diagnosisTypeField = value;
            }
        }

        /// <remarks/>
        public string diagnosisCode
        {
            get
            {
                return this.diagnosisCodeField;
            }
            set
            {
                this.diagnosisCodeField = value;
            }
        }

        /// <remarks/>
        public string diagnosisCodeType
        {
            get
            {
                return this.diagnosisCodeTypeField;
            }
            set
            {
                this.diagnosisCodeTypeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class servicesServiceAgreementDetail
    {

        private string clearAgreementDetailField;

        private string overrideExclusionsField;

        private string promoteToInNetworkField;

        private string payAtMaximumField;

        private string extendLimitField;

        private string coverTerminatedMemberField;

        private string reimbursementMethodField;

        private string amountField;

        private string typeOfFlatFeeField;

        private string percentageBilledField;

        /// <remarks/>
        public string clearAgreementDetail
        {
            get
            {
                return this.clearAgreementDetailField;
            }
            set
            {
                this.clearAgreementDetailField = value;
            }
        }

        /// <remarks/>
        public string overrideExclusions
        {
            get
            {
                return this.overrideExclusionsField;
            }
            set
            {
                this.overrideExclusionsField = value;
            }
        }

        /// <remarks/>
        public string promoteToInNetwork
        {
            get
            {
                return this.promoteToInNetworkField;
            }
            set
            {
                this.promoteToInNetworkField = value;
            }
        }

        /// <remarks/>
        public string payAtMaximum
        {
            get
            {
                return this.payAtMaximumField;
            }
            set
            {
                this.payAtMaximumField = value;
            }
        }

        /// <remarks/>
        public string extendLimit
        {
            get
            {
                return this.extendLimitField;
            }
            set
            {
                this.extendLimitField = value;
            }
        }

        /// <remarks/>
        public string coverTerminatedMember
        {
            get
            {
                return this.coverTerminatedMemberField;
            }
            set
            {
                this.coverTerminatedMemberField = value;
            }
        }

        /// <remarks/>
        public string reimbursementMethod
        {
            get
            {
                return this.reimbursementMethodField;
            }
            set
            {
                this.reimbursementMethodField = value;
            }
        }

        /// <remarks/>
        public string amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>
        public string typeOfFlatFee
        {
            get
            {
                return this.typeOfFlatFeeField;
            }
            set
            {
                this.typeOfFlatFeeField = value;
            }
        }

        /// <remarks/>
        public string percentageBilled
        {
            get
            {
                return this.percentageBilledField;
            }
            set
            {
                this.percentageBilledField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class servicesHicApproval
    {

        private string authorizedBySystemUserField;

        private string authorizedDateField;

        private servicesHicApprovalAuthorizedByExternalUser authorizedByExternalUserField;

        /// <remarks/>
        public string authorizedBySystemUser
        {
            get
            {
                return this.authorizedBySystemUserField;
            }
            set
            {
                this.authorizedBySystemUserField = value;
            }
        }

        /// <remarks/>
        public string authorizedDate
        {
            get
            {
                return this.authorizedDateField;
            }
            set
            {
                this.authorizedDateField = value;
            }
        }

        /// <remarks/>
        public servicesHicApprovalAuthorizedByExternalUser authorizedByExternalUser
        {
            get
            {
                return this.authorizedByExternalUserField;
            }
            set
            {
                this.authorizedByExternalUserField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class servicesHicApprovalAuthorizedByExternalUser
    {

        private string codeSetNameField;

        private string codeEntryField;

        private string shortNameField;

        /// <remarks/>
        public string codeSetName
        {
            get
            {
                return this.codeSetNameField;
            }
            set
            {
                this.codeSetNameField = value;
            }
        }

        /// <remarks/>
        public string codeEntry
        {
            get
            {
                return this.codeEntryField;
            }
            set
            {
                this.codeEntryField = value;
            }
        }

        /// <remarks/>
        public string shortName
        {
            get
            {
                return this.shortNameField;
            }
            set
            {
                this.shortNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class servicesReviewType
    {

        private string codeSetNameField;

        private string codeEntryField;

        private string shortNameField;

        /// <remarks/>
        public string codeSetName
        {
            get
            {
                return this.codeSetNameField;
            }
            set
            {
                this.codeSetNameField = value;
            }
        }

        /// <remarks/>
        public string codeEntry
        {
            get
            {
                return this.codeEntryField;
            }
            set
            {
                this.codeEntryField = value;
            }
        }

        /// <remarks/>
        public string shortName
        {
            get
            {
                return this.shortNameField;
            }
            set
            {
                this.shortNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class servicesReviewLevel
    {

        private string codeSetNameField;

        private string codeEntryField;

        private string shortNameField;

        /// <remarks/>
        public string codeSetName
        {
            get
            {
                return this.codeSetNameField;
            }
            set
            {
                this.codeSetNameField = value;
            }
        }

        /// <remarks/>
        public string codeEntry
        {
            get
            {
                return this.codeEntryField;
            }
            set
            {
                this.codeEntryField = value;
            }
        }

        /// <remarks/>
        public string shortName
        {
            get
            {
                return this.shortNameField;
            }
            set
            {
                this.shortNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class servicesAuthorizationServiceSetMatchInput
    {

        private string authorizationIdField;

        private string externalServiceSetIdField;

        private string serviceStartDateField;

        private string serviceEndDateField;

        private string serviceCodeField;

        private string serviceCodeTypeField;

        private string referralCategoryTypeField;

        private string serviceSetStatusField;

        /// <remarks/>
        public string authorizationId
        {
            get
            {
                return this.authorizationIdField;
            }
            set
            {
                this.authorizationIdField = value;
            }
        }

        /// <remarks/>
        public string externalServiceSetId
        {
            get
            {
                return this.externalServiceSetIdField;
            }
            set
            {
                this.externalServiceSetIdField = value;
            }
        }

        /// <remarks/>
        public string serviceStartDate
        {
            get
            {
                return this.serviceStartDateField;
            }
            set
            {
                this.serviceStartDateField = value;
            }
        }

        /// <remarks/>
        public string serviceEndDate
        {
            get
            {
                return this.serviceEndDateField;
            }
            set
            {
                this.serviceEndDateField = value;
            }
        }

        /// <remarks/>
        public string serviceCode
        {
            get
            {
                return this.serviceCodeField;
            }
            set
            {
                this.serviceCodeField = value;
            }
        }

        /// <remarks/>
        public string serviceCodeType
        {
            get
            {
                return this.serviceCodeTypeField;
            }
            set
            {
                this.serviceCodeTypeField = value;
            }
        }

        /// <remarks/>
        public string referralCategoryType
        {
            get
            {
                return this.referralCategoryTypeField;
            }
            set
            {
                this.referralCategoryTypeField = value;
            }
        }

        /// <remarks/>
        public string serviceSetStatus
        {
            get
            {
                return this.serviceSetStatusField;
            }
            set
            {
                this.serviceSetStatusField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class diagnosis
    {

        private string diagnosisSystemIdField;

        private string diagnosisTypeField;

        private string diagnosisCodeField;

        private string diagnosisCodeTypeField;

        /// <remarks/>
        public string diagnosisSystemId
        {
            get
            {
                return this.diagnosisSystemIdField;
            }
            set
            {
                this.diagnosisSystemIdField = value;
            }
        }

        /// <remarks/>
        public string diagnosisType
        {
            get
            {
                return this.diagnosisTypeField;
            }
            set
            {
                this.diagnosisTypeField = value;
            }
        }

        /// <remarks/>
        public string diagnosisCode
        {
            get
            {
                return this.diagnosisCodeField;
            }
            set
            {
                this.diagnosisCodeField = value;
            }
        }

        /// <remarks/>
        public string diagnosisCodeType
        {
            get
            {
                return this.diagnosisCodeTypeField;
            }
            set
            {
                this.diagnosisCodeTypeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class typeOfBill
    {

        private string idField;

        /// <remarks/>
        public string ID
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class memberMatchInput
    {

        private string insurancePlanIdField;

        private string c4cIdField;

        private memberMatchInputMemberMatchInput memberMatchInput1Field;

        /// <remarks/>
        public string insurancePlanId
        {
            get
            {
                return this.insurancePlanIdField;
            }
            set
            {
                this.insurancePlanIdField = value;
            }
        }

        /// <remarks/>
        public string c4cId
        {
            get
            {
                return this.c4cIdField;
            }
            set
            {
                this.c4cIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("memberMatchInput")]
        public memberMatchInputMemberMatchInput memberMatchInput1
        {
            get
            {
                return this.memberMatchInput1Field;
            }
            set
            {
                this.memberMatchInput1Field = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class memberMatchInputMemberMatchInput
    {

        private string definitionNameField;

        private string idField;

        private string topAccountField;

        private string customString1Field;

        private string customString2Field;

        private string customString3Field;

        private string customString4Field;

        private string customInteger1Field;

        private string customInteger2Field;

        private string customInteger3Field;

        private string customInteger4Field;

        private string customDate1Field;

        private string customDate2Field;

        private string customDate3Field;

        private string customDate4Field;

        private string customDecimal1Field;

        private string customDecimal2Field;

        private string customDecimal3Field;

        private string customDecimal4Field;

        private memberMatchInputMemberMatchInputMember memberField;

        private memberMatchInputMemberMatchInputSubscriber subscriberField;

        private string asOfDateField;

        /// <remarks/>
        public string definitionName
        {
            get
            {
                return this.definitionNameField;
            }
            set
            {
                this.definitionNameField = value;
            }
        }

        /// <remarks/>
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public string topAccount
        {
            get
            {
                return this.topAccountField;
            }
            set
            {
                this.topAccountField = value;
            }
        }

        /// <remarks/>
        public string customString1
        {
            get
            {
                return this.customString1Field;
            }
            set
            {
                this.customString1Field = value;
            }
        }

        /// <remarks/>
        public string customString2
        {
            get
            {
                return this.customString2Field;
            }
            set
            {
                this.customString2Field = value;
            }
        }

        /// <remarks/>
        public string customString3
        {
            get
            {
                return this.customString3Field;
            }
            set
            {
                this.customString3Field = value;
            }
        }

        /// <remarks/>
        public string customString4
        {
            get
            {
                return this.customString4Field;
            }
            set
            {
                this.customString4Field = value;
            }
        }

        /// <remarks/>
        public string customInteger1
        {
            get
            {
                return this.customInteger1Field;
            }
            set
            {
                this.customInteger1Field = value;
            }
        }

        /// <remarks/>
        public string customInteger2
        {
            get
            {
                return this.customInteger2Field;
            }
            set
            {
                this.customInteger2Field = value;
            }
        }

        /// <remarks/>
        public string customInteger3
        {
            get
            {
                return this.customInteger3Field;
            }
            set
            {
                this.customInteger3Field = value;
            }
        }

        /// <remarks/>
        public string customInteger4
        {
            get
            {
                return this.customInteger4Field;
            }
            set
            {
                this.customInteger4Field = value;
            }
        }

        /// <remarks/>
        public string customDate1
        {
            get
            {
                return this.customDate1Field;
            }
            set
            {
                this.customDate1Field = value;
            }
        }

        /// <remarks/>
        public string customDate2
        {
            get
            {
                return this.customDate2Field;
            }
            set
            {
                this.customDate2Field = value;
            }
        }

        /// <remarks/>
        public string customDate3
        {
            get
            {
                return this.customDate3Field;
            }
            set
            {
                this.customDate3Field = value;
            }
        }

        /// <remarks/>
        public string customDate4
        {
            get
            {
                return this.customDate4Field;
            }
            set
            {
                this.customDate4Field = value;
            }
        }

        /// <remarks/>
        public string customDecimal1
        {
            get
            {
                return this.customDecimal1Field;
            }
            set
            {
                this.customDecimal1Field = value;
            }
        }

        /// <remarks/>
        public string customDecimal2
        {
            get
            {
                return this.customDecimal2Field;
            }
            set
            {
                this.customDecimal2Field = value;
            }
        }

        /// <remarks/>
        public string customDecimal3
        {
            get
            {
                return this.customDecimal3Field;
            }
            set
            {
                this.customDecimal3Field = value;
            }
        }

        /// <remarks/>
        public string customDecimal4
        {
            get
            {
                return this.customDecimal4Field;
            }
            set
            {
                this.customDecimal4Field = value;
            }
        }

        /// <remarks/>
        public memberMatchInputMemberMatchInputMember member
        {
            get
            {
                return this.memberField;
            }
            set
            {
                this.memberField = value;
            }
        }

        /// <remarks/>
        public memberMatchInputMemberMatchInputSubscriber subscriber
        {
            get
            {
                return this.subscriberField;
            }
            set
            {
                this.subscriberField = value;
            }
        }

        /// <remarks/>
        public string asOfDate
        {
            get
            {
                return this.asOfDateField;
            }
            set
            {
                this.asOfDateField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class memberMatchInputMemberMatchInputMember
    {

        private string firstNameField;

        private string lastNameField;

        private string fullNameField;

        private string genderField;

        private string dateOfBirthField;

        private string taxIdField;

        private memberMatchInputMemberMatchInputMemberAddress addressField;

        /// <remarks/>
        public string firstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public string lastName
        {
            get
            {
                return this.lastNameField;
            }
            set
            {
                this.lastNameField = value;
            }
        }

        /// <remarks/>
        public string fullName
        {
            get
            {
                return this.fullNameField;
            }
            set
            {
                this.fullNameField = value;
            }
        }

        /// <remarks/>
        public string gender
        {
            get
            {
                return this.genderField;
            }
            set
            {
                this.genderField = value;
            }
        }

        /// <remarks/>
        public string dateOfBirth
        {
            get
            {
                return this.dateOfBirthField;
            }
            set
            {
                this.dateOfBirthField = value;
            }
        }

        /// <remarks/>
        public string taxId
        {
            get
            {
                return this.taxIdField;
            }
            set
            {
                this.taxIdField = value;
            }
        }

        /// <remarks/>
        public memberMatchInputMemberMatchInputMemberAddress address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class memberMatchInputMemberMatchInputMemberAddress
    {

        private string addressField;

        private string cityNameField;

        private string stateCodeField;

        private string zipCodeField;

        private string zipExtensionCodeField;

        private string countryCodeField;

        /// <remarks/>
        public string address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>
        public string cityName
        {
            get
            {
                return this.cityNameField;
            }
            set
            {
                this.cityNameField = value;
            }
        }

        /// <remarks/>
        public string stateCode
        {
            get
            {
                return this.stateCodeField;
            }
            set
            {
                this.stateCodeField = value;
            }
        }

        /// <remarks/>
        public string zipCode
        {
            get
            {
                return this.zipCodeField;
            }
            set
            {
                this.zipCodeField = value;
            }
        }

        /// <remarks/>
        public string zipExtensionCode
        {
            get
            {
                return this.zipExtensionCodeField;
            }
            set
            {
                this.zipExtensionCodeField = value;
            }
        }

        /// <remarks/>
        public string countryCode
        {
            get
            {
                return this.countryCodeField;
            }
            set
            {
                this.countryCodeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class memberMatchInputMemberMatchInputSubscriber
    {

        private string firstNameField;

        private string lastNameField;

        private string fullNameField;

        private string genderField;

        private string dateOfBirthField;

        private string taxIdField;

        private memberMatchInputMemberMatchInputSubscriberAddress addressField;

        /// <remarks/>
        public string firstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public string lastName
        {
            get
            {
                return this.lastNameField;
            }
            set
            {
                this.lastNameField = value;
            }
        }

        /// <remarks/>
        public string fullName
        {
            get
            {
                return this.fullNameField;
            }
            set
            {
                this.fullNameField = value;
            }
        }

        /// <remarks/>
        public string gender
        {
            get
            {
                return this.genderField;
            }
            set
            {
                this.genderField = value;
            }
        }

        /// <remarks/>
        public string dateOfBirth
        {
            get
            {
                return this.dateOfBirthField;
            }
            set
            {
                this.dateOfBirthField = value;
            }
        }

        /// <remarks/>
        public string taxId
        {
            get
            {
                return this.taxIdField;
            }
            set
            {
                this.taxIdField = value;
            }
        }

        /// <remarks/>
        public memberMatchInputMemberMatchInputSubscriberAddress address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class memberMatchInputMemberMatchInputSubscriberAddress
    {

        private string addressField;

        private string cityNameField;

        private string stateCodeField;

        private string zipCodeField;

        private string zipExtensionCodeField;

        private string countryCodeField;

        /// <remarks/>
        public string address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>
        public string cityName
        {
            get
            {
                return this.cityNameField;
            }
            set
            {
                this.cityNameField = value;
            }
        }

        /// <remarks/>
        public string stateCode
        {
            get
            {
                return this.stateCodeField;
            }
            set
            {
                this.stateCodeField = value;
            }
        }

        /// <remarks/>
        public string zipCode
        {
            get
            {
                return this.zipCodeField;
            }
            set
            {
                this.zipCodeField = value;
            }
        }

        /// <remarks/>
        public string zipExtensionCode
        {
            get
            {
                return this.zipExtensionCodeField;
            }
            set
            {
                this.zipExtensionCodeField = value;
            }
        }

        /// <remarks/>
        public string countryCode
        {
            get
            {
                return this.countryCodeField;
            }
            set
            {
                this.countryCodeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class authorizationMatchInput
    {

        private string authorizationIdField;

        private string externalAuthorizationIdField;

        /// <remarks/>
        public string authorizationId
        {
            get
            {
                return this.authorizationIdField;
            }
            set
            {
                this.authorizationIdField = value;
            }
        }

        /// <remarks/>
        public string externalAuthorizationId
        {
            get
            {
                return this.externalAuthorizationIdField;
            }
            set
            {
                this.externalAuthorizationIdField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class clinicalNotes
    {

        private string noteIDField;

        private string noteContentField;

        private string reviewerFirstNameField;

        private string reviewerLastNameField;

        private List<clinicalNotesNoteAttachments> noteAttachmentsField;

        /// <remarks/>
        public string noteID
        {
            get
            {
                return this.noteIDField;
            }
            set
            {
                this.noteIDField = value;
            }
        }

        /// <remarks/>
        public string noteContent
        {
            get
            {
                return this.noteContentField;
            }
            set
            {
                this.noteContentField = value;
            }
        }

        /// <remarks/>
        public string reviewerFirstName
        {
            get
            {
                return this.reviewerFirstNameField;
            }
            set
            {
                this.reviewerFirstNameField = value;
            }
        }

        /// <remarks/>
        public string reviewerLastName
        {
            get
            {
                return this.reviewerLastNameField;
            }
            set
            {
                this.reviewerLastNameField = value;
            }
        }

        /// <remarks/>
        public List<clinicalNotesNoteAttachments> noteAttachments
        {
            get
            {
                return this.noteAttachmentsField;
            }
            set
            {
                this.noteAttachmentsField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class clinicalNotesNoteAttachments
    {

        private string externalReferenceIdField;

        private string attachmentContentField;

        private string attachmentNameField;

        private string attachmentDescriptionField;

        private string attachmentSizeField;

        private string attachmentTypeField;

        /// <remarks/>
        public string externalReferenceId
        {
            get
            {
                return this.externalReferenceIdField;
            }
            set
            {
                this.externalReferenceIdField = value;
            }
        }

        /// <remarks/>
        public string attachmentContent
        {
            get
            {
                return this.attachmentContentField;
            }
            set
            {
                this.attachmentContentField = value;
            }
        }

        /// <remarks/>
        public string attachmentName
        {
            get
            {
                return this.attachmentNameField;
            }
            set
            {
                this.attachmentNameField = value;
            }
        }

        /// <remarks/>
        public string attachmentDescription
        {
            get
            {
                return this.attachmentDescriptionField;
            }
            set
            {
                this.attachmentDescriptionField = value;
            }
        }

        /// <remarks/>
        public string attachmentSize
        {
            get
            {
                return this.attachmentSizeField;
            }
            set
            {
                this.attachmentSizeField = value;
            }
        }

        /// <remarks/>
        public string attachmentType
        {
            get
            {
                return this.attachmentTypeField;
            }
            set
            {
                this.attachmentTypeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class userDefinedTerm
    {

        private string idField;

        /// <remarks/>
        public string ID
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class udt
    {

        private string attributeRoleNameField;

        private string attrValueAsStringField;

        /// <remarks/>
        public string attributeRoleName
        {
            get
            {
                return this.attributeRoleNameField;
            }
            set
            {
                this.attributeRoleNameField = value;
            }
        }

        /// <remarks/>
        public string attrValueAsString
        {
            get
            {
                return this.attrValueAsStringField;
            }
            set
            {
                this.attrValueAsStringField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class authorizationAttachments
    {

        private string externalReferenceIdField;

        private string attachmentContentField;

        private string attachmentNameField;

        private string attachmentDescriptionField;

        private string attachmentSizeField;

        private string attachmentTypeField;

        /// <remarks/>
        public string externalReferenceId
        {
            get
            {
                return this.externalReferenceIdField;
            }
            set
            {
                this.externalReferenceIdField = value;
            }
        }

        /// <remarks/>
        public string attachmentContent
        {
            get
            {
                return this.attachmentContentField;
            }
            set
            {
                this.attachmentContentField = value;
            }
        }

        /// <remarks/>
        public string attachmentName
        {
            get
            {
                return this.attachmentNameField;
            }
            set
            {
                this.attachmentNameField = value;
            }
        }

        /// <remarks/>
        public string attachmentDescription
        {
            get
            {
                return this.attachmentDescriptionField;
            }
            set
            {
                this.attachmentDescriptionField = value;
            }
        }

        /// <remarks/>
        public string attachmentSize
        {
            get
            {
                return this.attachmentSizeField;
            }
            set
            {
                this.attachmentSizeField = value;
            }
        }

        /// <remarks/>
        public string attachmentType
        {
            get
            {
                return this.attachmentTypeField;
            }
            set
            {
                this.attachmentTypeField = value;
            }
        }
    }


}

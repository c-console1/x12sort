using System.Collections.Generic;
using System.Xml;
using DataLayer.Base;
using DataLayer.HealthRulesConnectors.Enumerations;
using DataLayer.HealthRulesConnectors.Models;
using DataLayer.HealthRulesConnectors.Models.Requests;
using DataLayer.HealthRulesConnectors.Models.Responses;

namespace DataLayer.HealthRulesConnectors
{
    public class AccountRepository : BaseRepository<HealthRulesConnectorsDatabase>
    {
        internal AccountRepository(HealthRulesConnectorsDatabase database) : base(database)
        {

        }

        #region Account Call

        /// <summary>
        /// Account Connector Call that returns an XML string 
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public string AccountText(AccountRequest source)
        {
            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "Account"
            };

            string xml = objSoap.callObject.Get(conn, source.Get());
            return xml;
        }

        /// <summary>
        /// Account Connector Call that returns an XMLDocument object
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public XmlDocument AccountXML(AccountRequest source)
        {
            string xml = AccountText(source);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            return doc;

        }

        /// <summary>
        /// Account Connector Call that returns an .NET object
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public AccountResponse.Response Account(AccountRequest source)
        {

            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "Account"
            };

            // If a single XML object comes in and gets desialized to an object that is a list
            // it will create an error unless our deserilizing knows which field to treat as
            // lists.
            List<string> forceJSONArray = new List<string>() { "exceptions", "error" };

            AccountResponse.Response result = objSoap.callObject.GetXML
                <AccountResponse.Response>(conn, source.Get(), forceJSONArray);

            return result;

        }

        /* // Customimize your own call with simplier parameters
        /// <summary>
        /// Account Connector Call that returns an .NET object
        /// </summary>
        /// <param name="memberHccId">HealthRules ID</param>
        /// <param name="asOfDate">Display COB ID's equal to or later than this date</param>
        /// <returns></returns>
        public AccountResponse.Response Account(string memberHccId = "", string asOfDate = "") {

            Requests.PropertyRequest source = new AccountRequest()
            {
                 asOfDate = asOfDate, memberHccId = memberHccId
            };

            return Account(source);
        }
		*/

        #endregion Account Call

        #region AccountProperty Call

        /// <summary>
        /// AccountProperty Connector Call that returns an XML string 
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public string AccountPropertyText(AccountPropertyRequest source)
        {
            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "AccountProperty"
            };

            string xml = objSoap.callObject.Get(conn, source.Get());
            return xml;
        }

        /// <summary>
        /// AccountProperty Connector Call that returns an XMLDocument object
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public XmlDocument AccountPropertyXML(AccountPropertyRequest source)
        {
            string xml = AccountPropertyText(source);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            return doc;

        }

        /// <summary>
        /// AccountProperty Connector Call that returns an .NET object
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public AccountPropertyResponse.Response AccountProperty(AccountPropertyRequest source)
        {

            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "AccountProperty"
            };

            // If a single XML object comes in and gets desialized to an object that is a list
            // it will create an error unless our deserilizing knows which field to treat as
            // lists.
            List<string> forceJSONArray = new List<string>() { "exceptions", "error" };

            AccountPropertyResponse.Response result = objSoap.callObject.GetXML
                <AccountPropertyResponse.Response>(conn, source.Get(), forceJSONArray);

            return result;

        }

        /* // Customimize your own call with simplier parameters
        /// <summary>
        /// AccountProperty Connector Call that returns an .NET object
        /// </summary>
        /// <param name="memberHccId">HealthRules ID</param>
        /// <param name="asOfDate">Display COB ID's equal to or later than this date</param>
        /// <returns></returns>
        public AccountPropertyResponse.Response AccountProperty(string memberHccId = "", string asOfDate = "") {

            Requests.PropertyRequest source = new AccountPropertyRequest()
            {
                 asOfDate = asOfDate, memberHccId = memberHccId
            };

            return AccountProperty(source);
        }
		*/

        #endregion AccountProperty Call

        #region AccountLookup Call

        /// <summary>
        /// AccountLookupCriteria Connector Call that returns an XML string 
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public string AccountLookupCriteriaText(AccountLookupRequest source)
        {
            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "AccountLookupCriteria"
            };

            string xml = objSoap.callObject.Get(conn, source.Get());
            return xml;
        }

        /// <summary>
        /// AccountLookupCriteria Connector Call that returns an XMLDocument object
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public XmlDocument AccountLookupCriteriaXML(AccountLookupRequest source)
        {
            string xml = AccountLookupCriteriaText(source);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            return doc;

        }

        /// <summary>
        /// AccountLookupCriteria Connector Call that returns an .NET object
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public AccountLookupResponse.Response AccountLookupCriteria(AccountLookupRequest source)
        {

            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "AccountLookupCriteria"
            };

            // If a single XML object comes in and gets desialized to an object that is a list
            // it will create an error unless our deserilizing knows which field to treat as
            // lists.
            List<string> forceJSONArray = new List<string>() { "account", "otherNames", "otherIDs", "error" };

            AccountLookupResponse.Response result = objSoap.callObject.GetXML
                <AccountLookupResponse.Response>(conn, source.Get(), forceJSONArray);

            return result;

        }

        /* // Customimize your own call with simplier parameters
        /// <summary>
        /// AccountLookupCriteria Connector Call that returns an .NET object
        /// </summary>
        /// <param name="memberHccId">HealthRules ID</param>
        /// <param name="asOfDate">Display COB ID's equal to or later than this date</param>
        /// <returns></returns>
        public AccountLookupCriteriaResponse.Response AccountLookupCriteria(string memberHccId = "", string asOfDate = "") {

            Requests.AccountLookupCriteriaRequest source = new AccountLookupCriteriaRequest()
            {
                 asOfDate = asOfDate, memberHccId = memberHccId
            };

            return AccountLookupCriteria(source);
        }
		*/

        #endregion AccountLookupCriteria Call
    }
}

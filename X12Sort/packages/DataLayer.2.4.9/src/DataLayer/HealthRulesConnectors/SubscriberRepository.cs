using System.Collections.Generic;
using System.Xml;
using DataLayer.Base;
using DataLayer.HealthRulesConnectors.Enumerations;
using DataLayer.HealthRulesConnectors.Models;
using DataLayer.HealthRulesConnectors.Models.Requests;
using DataLayer.HealthRulesConnectors.Models.Responses;

namespace DataLayer.HealthRulesConnectors
{
    public class SubscriberRepository : BaseRepository<HealthRulesConnectorsDatabase>
    {
        internal SubscriberRepository(HealthRulesConnectorsDatabase database) : base(database)
        {

        }

        #region SubscriptionLookup

        /// <summary>
        /// Subscriber Lookup - Raw rewuest object
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public SubscriptionLookupResponse.Response SubscriptionLookup(SubscriptionLookupRequest source)
        {
            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "SubscriptionLookup"
            };

            List<string> forceJSONList = new List<string>() {
                        "subscriptionUDT", "udtListValueSet", "qualityControlReviewConclusion", "hccIdentifier",
                        "payeeBankAccountDateRange", "error"
                    };

            SubscriptionLookupResponse.Response result = objSoap.callObject.GetXML
                <SubscriptionLookupResponse.Response>(conn, source.Get(), forceJSONList);

            return result;
        }

        /// <summary>
        /// Subscriber Lookup - Raw rewuest object, returns XML document
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public XmlDocument SubscriptionLookupXML(SubscriptionLookupRequest source)
        {
            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "SubscriptionLookup"
            };

            //List<string> forceJSONList = new List<string>() { "memberId" };

            string result = objSoap.callObject.Get(conn, source.Get());

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(result);
            return doc;
        }

        #endregion SubscriptionLookup
    }
}

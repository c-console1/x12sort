using System;
using DataLayer.Base;
using DataLayer.HealthRulesConnectors.Enumerations;
using DataLayer.HealthRulesConnectors.Models;
using DataLayer.HealthRulesConnectors.Models.Requests;
using DataLayer.HealthRulesConnectors.Models.Responses;

namespace DataLayer.HealthRulesConnectors
{
    public class PractitionerRepository : BaseRepository<HealthRulesConnectorsDatabase>
    {
        internal PractitionerRepository(HealthRulesConnectorsDatabase database) : base(database)
        {

        }

        #region PractitionerLookup

        /// <summary>
        /// Practitioner Lookup - raw request object
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public PractitionerLookup.Response PractitionerLookup(PractitionerLookupRequest source)
        {
            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "PractitionerLookup"
            };

            //string test = objSoap.callObject.Get(conn, source.Get());

            PractitionerLookup.Response result = objSoap.callObject.GetXML
                <PractitionerLookup.Response>(conn, source.Get());

            return result;
        }

        /// <summary>
        /// Practitioner Lookup - common fields
        /// </summary>
        /// <param name="AsOfDate"></param>
        /// <param name="PractitionerId"></param>
        /// <param name="FirstName"></param>
        /// <param name="LastName"></param>
        /// <returns></returns>
        public PractitionerLookup.Response PractitionerLookup(DateTime AsOfDate, string PractitionerId = "", string FirstName = "", string LastName = "")
        {
            PractitionerLookupRequest source = new PractitionerLookupRequest();
            if (!string.IsNullOrWhiteSpace(PractitionerId))
                source.practitionerId = PractitionerId;
            if (!string.IsNullOrWhiteSpace(FirstName))
                source.practitionerFirstName = FirstName;
            if (!string.IsNullOrWhiteSpace(LastName))
                source.practitionerLastName = LastName;
            if (AsOfDate == null)
                source.asOfDate = DateTime.Now;
            else
                source.asOfDate = AsOfDate;
            return PractitionerLookup(source);
        }

        /// <summary>
        /// Practitioner Lookup 
        /// </summary>
        /// <param name="PractitionerId"></param>
        /// <param name="FirstName"></param>
        /// <param name="LastName"></param>
        /// <returns></returns>
        public PractitionerLookup.Response PractitionerLookup(string PractitionerId = "", string FirstName = "", string LastName = "")
        {
            PractitionerLookupRequest source = new PractitionerLookupRequest();
            if (!string.IsNullOrWhiteSpace(PractitionerId))
                source.practitionerId = PractitionerId;
            if (!string.IsNullOrWhiteSpace(FirstName))
                source.practitionerFirstName = FirstName;
            if (!string.IsNullOrWhiteSpace(LastName))
                source.practitionerLastName = LastName;
            return PractitionerLookup(source);
        }

        #endregion PractitionerLookup
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.HealthRulesConnectors
{
    /// <summary>
    /// Type Code - common generic key/value/value set that HealthRule API uses
    /// </summary>
    public class TypeCode
    {
        /// <summary>
        /// Code set name- primary key
        /// </summary>
        public string codeSetName { get; set; }

        /// <summary>
        /// Code set entry = code values for this set
        /// Either codeEntry or shortName must be set
        /// </summary>
        public string codeEntry { get; set; }

        /// <summary>
        /// Short name for this set
        /// Either codeEntry or shortName must be set
        /// </summary>
        public string shortName { get; set; }
    }

    /// <summary>
    /// Common list of name suffixes, ex. Jr.
    /// </summary>
    public class NameSuffixList
    {
        /// <summary>
        /// List mode for how HealthRules should treat this list 
        /// of values
        /// </summary>
        public string listMode { get; set; }

        /// <summary>
        /// Suffix text list
        /// </summary>
        public List<string> suffix = new List<string>();
    }

    /// <summary>
    /// Common list of credential suffixes, ex. RN, LPN
    /// </summary>
    public class CredentialSuffixList
    {
        /// <summary>
        /// List mode for how HealthRules should treat this list 
        /// of values
        /// </summary>
        public string listMode { get; set; }

        /// <summary>
        /// Suffix text list
        /// </summary>
        public List<TypeCode> CredentialSuffix = new List<TypeCode>();
    }

    /// <summary>
    /// Multiple other names
    /// </summary>
    public class OtherNames
    {
        // TBD
        // This may need to be converted to a <List> object
        public string listMode { get; set; }
        // The object below will most likely need to put into a <List>
        // TBD
        public List<OtherName> otherName = new List<OtherName>();
    }

    /// <summary>
    /// Other name information
    /// </summary>
    public class OtherName
    {
        /// <summary>
        /// Extended other name information
        /// </summary>
        public OtherNameInfo otherNameInfo = new OtherNameInfo();

        /// <summary>
        /// Other name types
        /// </summary>
        public TypeCode otherNameTypeCode = new TypeCode();
    }

    /// <summary>
    /// Other name information
    /// </summary>
    public class OtherNameInfo
    {
        /// <summary>
        /// Last name
        /// </summary>
        public string lastName { get; set; }

        /// <summary>
        /// First name
        /// </summary>
        public string firstName { get; set; }

        /// <summary>
        /// Middle name
        /// </summary>
        public string middleName { get; set; }

        // Name prefix - singlar instance
        public string namePrefix { get; set; }

        // Name suffix - singlar instance
        public string nameSuffix { get; set; }

        // Name suffix - List
        public List<NameSuffixList> nameSuffixList = new List<NameSuffixList>();


        // Credential Suffix - list        
        public List<CredentialSuffixList> credentialSuffixList = new List<CredentialSuffixList>();
    }

    /// <summary>
    /// Language information
    /// </summary>
    public class Language
    {
        /// <summary>
        /// Primary language
        /// </summary>
        public string primaryLanguage { get; set; }

        /// <summary>
        /// Native language
        /// </summary>
        public string nativeLanguage { get; set; }

        // Language Domain Code
        public TypeCode languageDomainCode = new TypeCode();
    }

    /// <summary>
    /// Address information
    /// </summary>
    public class AddressInfo
    {
        /// <summary>
        /// Postal address information
        /// </summary>
        public PostalAddress postalAddress = new PostalAddress();

        /// <summary>
        /// Address phone list
        /// </summary>
        public AddressPhoneList addressPhoneList = new AddressPhoneList();
    }

    /// <summary>
    /// Address values
    /// </summary>
    public class Address
    {
        /// <summary>
        /// Address line 1
        /// </summary>
        public string address { get; set; }

        /// <summary>
        /// Address line 2
        /// </summary>
        public string address2 { get; set; }

        /// <summary>
        /// Address line 3
        /// </summary>
        public string address3 { get; set; }

        /// <summary>
        /// City name
        /// </summary>
        public string cityName { get; set; }

        /// <summary>
        /// State code
        /// </summary>
        public string stateCode { get; set; }

        /// <summary>
        /// 5-6 character zip code
        /// </summary>
        public string zipCode { get; set; }

        /// <summary>
        /// 4 character zip code extension
        /// </summary>
        public string zipExtensionCode { get; set; }

        /// <summary>
        /// Country code
        /// </summary>
        public CountryCode countryCode = new CountryCode();

        /// <summary>
        /// County code
        /// </summary>
        public string countyCode { get; set; }

        /// <summary>
        /// Longitude cordinate
        /// </summary>
        public string longitude { get; set; }

        /// <summary>
        /// Longitude cordinate
        /// </summary>
        public string latitude { get; set; }

        /// <summary>
        /// Ignore address check
        /// </summary>
        public string ignoreAddressCheck { get; set; }

        /// <summary>
        /// Is this an undeliverable address?
        /// </summary>
        public string undeliverableAddress { get; set; }

    }

    /// <summary>
    /// Contry code
    /// </summary>
    public class CountryCode
    {
        /// <summary>
        /// 2 letter country code
        /// </summary>
        public string countryCode { get; set; }
    }


    /// <summary>
    /// Postal address
    /// </summary>
    public class PostalAddress
    {
        /// <summary>
        /// Address line 1
        /// </summary>
        public string address { get; set; }

        /// <summary>
        /// Address line 2
        /// </summary>
        public string address2 { get; set; }

        /// <summary>
        /// Address line 3
        /// </summary>
        public string address3 { get; set; }

        /// <summary>
        /// City name
        /// </summary>
        public string cityName { get; set; }

        /// <summary>
        /// State code
        /// </summary>
        public string stateCode { get; set; }

        /// <summary>
        /// 5-6 character zip code
        /// </summary>
        public string zipCode { get; set; }

        /// <summary>
        /// 4 character zip code extension
        /// </summary>
        public string zipExtensionCode { get; set; }

        /// <summary>
        /// County code
        /// </summary>
        public string countyCode { get; set; }

        /// <summary>
        /// Country code
        /// </summary>
        public CountryCode countryCode = new CountryCode();

        /// <summary>
        /// Longitude cordinate
        /// </summary>
        public string longitude { get; set; }

        /// <summary>
        /// Longitude cordinate
        /// </summary>
        public string latitude { get; set; }

        /// <summary>
        /// Ignore address check
        /// </summary>
        public string ignoreAddressCheck { get; set; }

        /// <summary>
        /// Is this an undeliverable address?
        /// </summary>
        public string undeliverableAddress { get; set; }
    }

    /// <summary>
    /// Phone number list
    /// </summary>
    public class AddressPhoneList
    {
        /// <summary>
        /// List mode
        /// </summary>
        public string listMode { get; set; }

        /// <summary>
        /// List of telephone numbers and thier meta information
        /// </summary>
        public List<TelephoneNumber> telephoneNumber = new List<TelephoneNumber>();
    }

    /// <summary>
    /// Individual phone number
    /// </summary>
    public class TelephoneNumber
    {
        /// <summary>
        /// Phone country code - default is '01'
        /// </summary>
        public string phoneCountryCode { get; set; }

        /// <summary>
        /// 3 number area code
        /// </summary>
        public string phoneAreaCode { get; set; }

        /// <summary>
        /// Phone #
        /// </summary>
        public string phoneNumber { get; set; }

        /// <summary>
        /// Phone # extension
        /// </summary>
        public string phoneExtensionNumber { get; set; }

        /// <summary>
        /// Individual phone type code set
        /// </summary>
        public TypeCode individualPhoneTypeCode = new TypeCode();

        /// <summary>
        /// Individual organization type code set
        /// </summary>
        public TypeCode organizationPhoneTypeCode = new TypeCode();
    }
}

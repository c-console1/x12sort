using System;
using System.Collections.Generic;
using System.Xml;
using DataLayer.Base;
using DataLayer.HealthRulesConnectors.Enumerations;
using DataLayer.HealthRulesConnectors.Models;
using DataLayer.HealthRulesConnectors.Models.Requests;
using DataLayer.HealthRulesConnectors.Models.Responses;

namespace DataLayer.HealthRulesConnectors
{
    public class MemberRepository : BaseRepository<HealthRulesConnectorsDatabase>
    {
        internal MemberRepository(HealthRulesConnectorsDatabase database) : base(database)
        {

        }

        #region CobPolicyList - Update COB Information
        // See HealthRules Connector Guides - Vol 5: Extracts and Services - 

        /// <summary>
        /// CobPolicyListText Connector Call that returns an XML string 
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public string CobPolicyListText(CobPolicyListRequest source)
        {
            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "COBPolicy"
            };

            string xml = objSoap.callObject.Get(conn, source.Get());
            return xml;
        }

        /// <summary>
        /// CobPolicyListText Connector Call that returns an XML Document 
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public XmlDocument CobPolicyListXML(CobPolicyListRequest source)
        {
            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "COBPolicy"
            };

            string xml = objSoap.callObject.Get(conn, source.Get());
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            return doc;
        }

        /// <summary>
        /// CobPolicyListText Connector Call that returns a .NET object
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public CobPolicyListResponse.Response CobPolicyList(CobPolicyListRequest source)
        {
            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "COBPolicy"
            };

            // Singlular instances of an XML object into a List or IEnuermable will deserialize as
            // singlular object. IE what should be seen as a List<string> will deserialize as string
            // and break the model.
            List<string> forceJSONList = new List<string>()
            {
                "cobPolicy", "error"
            };

            CobPolicyListResponse.Response result = objSoap.callObject.GetXML
                <CobPolicyListResponse.Response>(conn, source.Get(), forceJSONList);

            return result;
        }

        /// <summary>
        /// CobPolicyListText Connector Call that returns a .NET object
        /// </summary>
        /// <param name="memberHccID">Member HCC ID</param>
        /// <param name="startDate">Start Date</param>
        /// <param name="endDate">End Date</param>
        /// <param name="cobPolicyId">Policy ID</param>
        /// <param name="cobPolicyIdentifier">Policy Identifer</param>
        /// <returns></returns>
        public CobPolicyListResponse.Response CobPolicyList(string memberHccID, string startDate, string endDate, string cobPolicyId = "", string cobPolicyIdentifier = "")
        {
            CobPolicyListRequest source = new CobPolicyListRequest();

            // Attach call to a member
            source.cobPolicyList.memberHccID = memberHccID;

            // Initialize the list of policies
            source.cobPolicyList.cobPolicy = new List<CobPolicyListRequest.CobPolicy>();

            // Create a single to add, add basic information
            CobPolicyListRequest.CobPolicy indPolicy = new CobPolicyListRequest.CobPolicy();
            indPolicy.cobPolicyId = cobPolicyId;
            indPolicy.cobPolicyIdentifier = cobPolicyIdentifier;

            // Initialize the list of data ranges this policy will cover
            indPolicy.dateRanges = new List<CobPolicyListRequest.DateRanges>();

            // Set an individual date range for the policy
            CobPolicyListRequest.DateRanges indDateRange = new CobPolicyListRequest.DateRanges();
            indDateRange.startDate = startDate;
            indDateRange.endDate = endDate;

            // Add the date range to the policy
            indPolicy.dateRanges.Add(indDateRange);

            // Add the individual policy to the list of policies
            source.cobPolicyList.cobPolicy.Add(indPolicy);

            // Get the response
            return CobPolicyList(source);
        }

        #endregion CobPolicyList

        #region CobPolicyLookup

        /// <summary>
        /// CobPolicyLookup Connector Call that returns an XML string 
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public string CobPolicyLookupText(CobPolicyLookupRequest source)
        {
            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "COBPolicyLookup"
            };

            string xml = objSoap.callObject.Get(conn, source.Get());
            return xml;
        }

        /// <summary>
        /// CobPolicyLookup Connector Call that returns an XMLDocument object
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public XmlDocument CobPolicyLookupXML(CobPolicyLookupRequest source)
        {
            string xml = CobPolicyLookupText(source);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            return doc;

        }

        /// <summary>
        /// CobPolicyLookup Connector Call that returns an .NET object
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public CobPolicyLookupResponse.Response CobPolicyLookup(CobPolicyLookupRequest source)
        {
            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "COBPolicyLookup"
            };

            // If a single XML object comes in and gets desialized to an object that is a list
            // it will create an error unless our deserilizing knows which field to treat as
            // lists.
            List<string> forceJSONArray = new List<string>() { "cobPolicyId", "error" };

            CobPolicyLookupResponse.Response result = objSoap.callObject.GetXML
                <CobPolicyLookupResponse.Response>(conn, source.Get(), forceJSONArray);

            return result;
        }

        /// <summary>
        /// CobPolicyLookup Connector Call that returns an .NET object
        /// </summary>
        /// <param name="memberHccId">HealthRules ID</param>
        /// <param name="asOfDate">Display COB ID's equal to or later than this date</param>
        /// <returns></returns>
        public CobPolicyLookupResponse.Response cobPolicyLookup(string memberHccId = "", string asOfDate = "")
        {
            CobPolicyLookupRequest source = new CobPolicyLookupRequest()
            {
                asOfDate = asOfDate,
                memberHccId = memberHccId
            };

            return CobPolicyLookup(source);
        }

        #endregion CobPolicyLookup

        #region MemberCoverageEventLookup

        /// <summary>
        /// Member Coverage Event Lookup
        /// </summary>
        /// <param name="source"></param>
        /// <returns>MemberCoverageEventLookupResponse object</returns>
        public string MemberCoverageEventLookupText(MemberCoverageEventLookupRequest source)
        {
            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "MemberCoverageEventLookup"
            };

            return objSoap.callObject.Get(conn, source.Get());
        }

        public XmlDocument MemberCoverageEventLookupXml(MemberCoverageEventLookupRequest source)
        {
            string xml = MemberCoverageEventLookupText(source);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            return doc;
        }

        /// <summary>
        /// Member Coverage Event Lookup
        /// </summary>
        /// <param name="source"></param>
        /// <returns>MemberCoverageEventLookupResponse object</returns>
        public MemberCoverageEventLookupResponse.Response MemberCoverageEventLookup(MemberCoverageEventLookupRequest source)
        {
            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "MemberCoverageEventLookup"
            };

            MemberCoverageEventLookupResponse.Response result = objSoap.callObject.GetXML
                <MemberCoverageEventLookupResponse.Response>(conn, source.Get());

            return result;
        }

        /// <summary>
        /// Member Coverage Event Lookup
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="eventsTypeList"></param>
        /// <returns>MemberCoverageEventLookupResponse object</returns>
        public MemberCoverageEventLookupResponse.Response MemberCoverageEventLookup(DateTime startDate, List<string> eventsTypeList)
        {
            if (eventsTypeList.Count == 0)
                throw new Exception("At least 1 event type must be set");

            MemberCoverageEventLookupRequest source = new MemberCoverageEventLookupRequest();
            source.startDate = startDate.ToString("yyyy-MM-dd");
            source.eventsTypeList = eventsTypeList;

            return MemberCoverageEventLookup(source);

        }

        #endregion MemberCoverageEventLookup

        #region MembershipLookup

        /// <summary>
        /// Member lookup, full source object
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public MembershipLookupResponse.Response MembershipLookup(MembershipLookupRequest source)
        {
            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "MembershipLookup"
            };

            // Fields that that our deserilizers HAS to see as arrays
            List<string> forceJSONList = new List<string>() {
                "raceOrEthnicityCodes", "suffix", "CredentialSuffix", "otherName", "language", "memberPhysicalAddress",
                "telephoneNumber", "identificationNumber", "dependentVerification", "toothName",
                "preExistingConditionInfo", "preExDiagnosisCodeRangeCondition", "preExProcedureCodeRangeCondition",
                "diagnosisReference", "serviceReference", "otherResponsiblePersonInfo", "authorizedIndividual",
                "membershipUDT", "udtListValueSet", "HIPAAPermission", "HIPAARestriction", "HIPAAAuthorization",
                "confidentialCommunicationDirective", "error"
            };

            MembershipLookupResponse.Response result = objSoap.callObject.GetXML
                <MembershipLookupResponse.Response>(conn, source.Get(), forceJSONList);

            return result;
        }

        /// <summary>
        /// Member lookup, common user objects
        /// </summary>
        /// <param name="asOfDate">As of date</param>
        /// <param name="id">Source ID</param>
        /// <param name="memberFirstName">Member first name</param>
        /// <param name="memberLastName">Member last name</param>
        /// <param name="definitionName">Search type definition</param>
        /// <returns></returns>
        public MembershipLookupResponse.Response MembershipLookup(string asOfDate = "", string id = "", string memberFirstName = "", string memberLastName = "", string definitionName = "MemberMatchDefinition")
        {
            // Generate soap envolope
            MembershipLookupRequest source = new MembershipLookupRequest();
            source.definitionName = definitionName;
            if (!string.IsNullOrWhiteSpace(asOfDate))
                source.asOfDate = DateTime.Parse(asOfDate).ToString("yyyy-MM-dd");
            source.id = id;
            source.member.firstName = memberFirstName;
            source.member.lastName = memberLastName;

            // Make the call
            return MembershipLookup(source);
        }

        /// <summary>
        /// Member lookup, raw XML string
        /// </summary>
        /// <param name="asOfDate">As of date</param>
        /// <param name="id">Source ID</param>
        /// <param name="memberFirstName">Member first name</param>
        /// <param name="memberLastName">Member last name</param>
        /// <param name="definitionName">Search type definition</param>
        /// <returns></returns>
        public string MembershipLookupText(string asOfDate = "", string id = "", string memberFirstName = "", string memberLastName = "", string definitionName = "MemberMatchDefinition")
        {
            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

            // Generate soap envolope
            MembershipLookupRequest source = new MembershipLookupRequest();
            source.definitionName = definitionName;
            if (!string.IsNullOrWhiteSpace(asOfDate))
                source.asOfDate = DateTime.Parse(asOfDate).ToString("yyyy-MM-dd");
            source.id = id;
            source.member.firstName = memberFirstName;
            source.member.lastName = memberLastName;

            // Create a connection object that describes how to connect to the SOAP call
            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "MembershipLookup"
            };

            // Get the output
            string output = objSoap.callObject.Get(conn, source.Get());
            return output;
        }

        /// <summary>
        /// Member lookup, returns XML Document so you can run XPath queries
        /// </summary>
        /// <param name="asOfDate">As of date</param>
        /// <param name="id">Source ID</param>
        /// <param name="memberFirstName">Member first name</param>
        /// <param name="memberLastName">Member last name</param>
        /// <param name="definitionName">Search type definition</param>
        /// <returns></returns>
        public XmlDocument MembershipLookupXML(string asOfDate = "", string id = "", string memberFirstName = "", string memberLastName = "", string definitionName = "MemberMatchDefinition")
        {
            string xml = MembershipLookupText(asOfDate, id, memberFirstName, memberLastName, definitionName);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            return doc;
        }

        #endregion MembershipLookup

        #region MembershipReferenceLookup

        /// <summary>
        /// Membership reference lookup
        /// </summary>
        /// <param name="source">Raw source object</param>
        /// <returns></returns>
        public string MembershipReferenceLookupText(string asOfDate = "", string id = "", string memberFirstName = "", string memberLastName = "", string definitionName = "MemberMatchDefinition")
        {
            MembershipReferenceLookupRequest source = new MembershipReferenceLookupRequest();
            source.definitionName = definitionName;
            if (!string.IsNullOrWhiteSpace(asOfDate))
                source.asOfDate = DateTime.Parse(asOfDate).ToString("yyyy-MM-dd");
            source.id = id;
            source.member = new MembershipReferenceLookupRequest.Member();
            source.member.firstName = memberFirstName;
            source.member.lastName = memberLastName;

            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

            // Build connection object
            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "MembershipReferenceLookup"
            };

            // Fields that that our deserilizers HAS to see as arrays
            string result = objSoap.callObject.Get(conn, source.Get());

            return result;
        }

        /// <summary>
        /// Membership reference lookup - XML Document for easy XPath queries
        /// </summary>
        /// <param name="source">Raw source object</param>
        /// <returns></returns>
        public XmlDocument MembershipReferenceLookupXML(string asOfDate = "", string id = "", string memberFirstName = "", string memberLastName = "", string definitionName = "MemberMatchDefinition")
        {
            string xml = MembershipReferenceLookupText(asOfDate, id, memberFirstName, memberLastName, definitionName);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            return doc;
        }

        /// <summary>
        /// Membership reference lookup
        /// </summary>
        /// <param name="source">Raw source object</param>
        /// <returns></returns>
        public MembershipReferenceLookupResponse.Response MembershipReferenceLookup(MembershipReferenceLookupRequest source)
        {
            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

            // Build connection object
            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "MembershipReferenceLookup"
            };

            // Fields that that our deserilizers HAS to see as arrays
            List<string> forceJSONList = new List<string>()
            {
                "error"
            };

            // Get a C# object
            MembershipReferenceLookupResponse.Response result = objSoap.callObject.GetXML
                <MembershipReferenceLookupResponse.Response>(conn, source.Get(), forceJSONList);

            return result;
        }

        /// <summary>
        /// Membership reference lookup
        /// </summary>
        /// <param name="source">Raw source object</param>
        /// <returns></returns>
        public MembershipReferenceLookupResponse.Response MembershipReferenceLookup(string asOfDate = "", string id = "", string memberFirstName = "", string memberLastName = "", string definitionName = "MemberMatchDefinition")
        {
            MembershipReferenceLookupRequest source = new MembershipReferenceLookupRequest();
            source.definitionName = definitionName;
            if (!string.IsNullOrWhiteSpace(asOfDate))
                source.asOfDate = DateTime.Parse(asOfDate).ToString("yyyy-MM-dd");
            source.id = id;
            source.member = new MembershipReferenceLookupRequest.Member();
            source.member.firstName = memberFirstName;
            source.member.lastName = memberLastName;

            return MembershipReferenceLookup(source);

        }

        #endregion MembershipReferenceLookup

        #region MembershipSparseLookup

        /// <summary>
        /// Membership lookup
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public string MembershipSparseLookupText(string asOfDate = "", string id = "", string memberFirstName = "", string memberLastName = "", string definitionName = "MemberMatchDefinition")
        {
            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "MembershipSparseLookup"
            };

            MembershipSparseLookupRequest source = new MembershipSparseLookupRequest();
            source.definitionName = definitionName;
            if (!string.IsNullOrWhiteSpace(asOfDate))
                source.asOfDate = DateTime.Parse(asOfDate).ToString("yyyy-MM-dd");
            source.id = id;
            source.member = new MembershipSparseLookupRequest.Member();
            source.member.firstName = memberFirstName;
            source.member.lastName = memberLastName;

            string result = objSoap.callObject.Get(conn, source.Get());

            return result;
        }

        public XmlDocument MembershipSparseLookupXML(string asOfDate = "", string id = "", string memberFirstName = "", string memberLastName = "", string definitionName = "MemberMatchDefinition")
        {
            string xml = MembershipSparseLookupText(asOfDate, id, memberFirstName, memberLastName, definitionName);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            return doc;
        }

        /// <summary>
        /// Membership lookup
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public MembershipSparseLookupResponse.Response MembershipSparseLookup(MembershipSparseLookupRequest source)
        {
            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "MembershipSparseLookup"
            };

            List<string> forceJSONList = new List<string>() {
                "specificCommunicationPreferences", "planSelection", "reportingCategory", "medicareHICN", "cobPolicy", "suffix",
                "CredentialSuffix", "otherName", "language", "memberPhysicalAddress", "telephoneNumber", "identificationNumber",
                "dependentVerification", "toothName", "preExistingConditionInfo", "preExDiagnosisCodeRangeCondition",
                "preExProcedureCodeRangeCondition", "diagnosisReference", "serviceReference", "otherResponsiblePersonInfo",
                "authorizedIndividual", "membershipUDT", "udtListValueSet", "HIPAAPermission", "HIPAARestriction", "HIPAAAuthorization",
                "confidentialCommunicationDirective", "raceOrEthnicityCodes", "benefitNetworkName", "providerSelection",
                "providerDateRanges", "stateMedicaidEnrollment", "workingSpouse", "dialysisDateRange", "transplantDateRange",
                "esrdDateRange", "hospiceDateRange", "institutionalDateRange", "medicaidDateRange", "nhcDateRange", "partADate",
                "partBDate", "premiumWithholdDateRange", "supplementalDateRange", "partDDateRanges", "partDLEPDateRanges",
                "partDLISCopayCategoryDates", "partDLISSubsidyLevelDates", "partDCurrLISPremSubsidyDates", "medicareHICNField",
                "medicaidDualStatus", "dateRanges", "subscriberIdList", "carrierIds", "addressPhoneList", "complianceProgramSelection",
                "memberFactor", "factorDateRanges", "error"
            };

            MembershipSparseLookupResponse.Response result = objSoap.callObject.GetXML
                <MembershipSparseLookupResponse.Response>(conn, source.Get(), forceJSONList);

            return result;
        }

        /// <summary>
        /// Membership lookup
        /// </summary>
        /// <param name="asOfDate">As of date</param>
        /// <param name="id">Meber ID</param>
        /// <param name="memberFirstName">Member first name</param>
        /// <param name="memberLastName">Member last name</param>
        /// <param name="definitionName"></param>
        /// <returns></returns>
        public MembershipSparseLookupResponse.Response MembershipSparseLookup(string asOfDate = "", string id = "", string memberFirstName = "", string memberLastName = "", string definitionName = "MemberMatchDefinition")
        {
            MembershipSparseLookupRequest source = new MembershipSparseLookupRequest();
            source.definitionName = definitionName;
            if (!string.IsNullOrWhiteSpace(asOfDate))
                source.asOfDate = DateTime.Parse(asOfDate).ToString("yyyy-MM-dd");
            source.id = id;
            source.member = new MembershipSparseLookupRequest.Member();
            source.member.firstName = memberFirstName;
            source.member.lastName = memberLastName;

            return MembershipSparseLookup(source);
        }

        #endregion MembershipSparseLookup

        #region MemberServiceStronglyTyped - FindMember

        /// <summary>
        /// Find member - raw request object
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public FindMemberResponse.Response FindMember(FindMemberRequest source)
        {
            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.Classic);

            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "MemberServiceStronglyTyped"
            };

            List<string> forceJSONList = new List<string>() { "memberId" };

            FindMemberResponse.Response result = objSoap.callObject.GetXML
                <FindMemberResponse.Response>(conn, source.Get(), forceJSONList);

            return result;
        }

        /// <summary>
        /// Find member
        /// </summary>
        /// <param name="hccBenefitPlanID"></param>
        /// <param name="benefitPlanType"></param>
        /// <param name="benefitPlanName"></param>
        /// <param name="asOfDate"></param>
        /// <returns></returns>
        public FindMemberResponse.Response FindMember(string hccBenefitPlanID = "", string benefitPlanType = "", string benefitPlanName = "", string asOfDate = "")
        {
            FindMemberRequest source = new FindMemberRequest();
            source.criteria = new FindMemberRequest.Criteria();
            source.criteria.hccBenefitPlanID = hccBenefitPlanID;
            source.criteria.benefitPlanType = benefitPlanType;
            source.criteria.benefitPlanName = benefitPlanName;
            source.criteria.asOfDate = asOfDate;

            return FindMember(source);
        }

        #endregion MemberServiceStronglyTyped - FindMember

        #region MemberServiceStronglyTyped - GetMemberDetailsByHccId

        /// <summary>
        /// Get raw XML document, results are freeform
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public string GetMemberDetailsByHccIdText(GetMemberDetailsByHccIdRequest source)
        {
            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.Classic);

            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "MemberServiceStronglyTyped"
            };

            List<string> forceJSONList = new List<string>() { "memberId" };

            string result = objSoap.callObject.Get(conn, source.Get());
            return result;

        }

        /// <summary>
        /// Get raw XML document, results are freeform
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public XmlDocument GetMemberDetailsByHccIdXML(GetMemberDetailsByHccIdRequest source)
        {
            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.Classic);

            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "MemberServiceStronglyTyped"
            };

            List<string> forceJSONList = new List<string>() { "memberId" };

            string result = objSoap.callObject.Get(conn, source.Get());

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(result);
            return doc;
        }

        #endregion MemberServiceStronglyTyped - GetMemberDetailsByHccId

        #region MemberServiceStronglyTyped - UpdateProviderSelection

        /// <summary>
        /// Get raw string document, results are freeform
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public string UpdateProviderSelectionText(UpdateProviderSelectionRequest source)
        {
            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.Classic);

            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "MemberServiceStronglyTyped"
            };

            //List<string> forceJSONList = new List<string>() { "memberId" };

            string result = objSoap.callObject.Get(conn, source.Get());

            return result;
        }

        /// <summary>
        /// Get raw XML document, results are freeform
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public XmlDocument UpdateProviderSelectionXML(UpdateProviderSelectionRequest source)
        {
            string result = UpdateProviderSelectionText(source);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(result);
            return doc;
        }

        /// <summary>
        /// Required fields only
        /// </summary>
        /// <param name="memberHccId"></param>
        /// <param name="selectionType"></param>
        /// <param name="providerSelectionReasonCode"></param>
        /// <param name="forceToWorkbasket"></param>
        /// <param name="messageCode"></param>
        /// <param name="messageDescription"></param>
        /// <returns></returns>
        public UpdateProviderSelectionResponse.Response UpdateProviderSelection(string memberHccId, string selectionType, string providerSelectionReasonCode, string forceToWorkbasket, string messageCode, string messageDescription)
        {
            UpdateProviderSelectionRequest source = new UpdateProviderSelectionRequest();
            source.memberHccId = memberHccId;
            source.selectionType = selectionType;
            source.providerSelectionReasonCode = providerSelectionReasonCode;
            source.forceToWorkbasket = forceToWorkbasket;
            source.auditLogInfo.messageCode = messageCode;
            source.auditLogInfo.messageDescription = messageDescription;
            return UpdateProviderSelection(source);
        }

        /// <summary>
        /// Full response code
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public UpdateProviderSelectionResponse.Response UpdateProviderSelection(UpdateProviderSelectionRequest source)
        {
            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.Classic);

            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "MemberServiceStronglyTyped"
            };

            List<string> forceJSONList = new List<string>() { "exceptions" };

            UpdateProviderSelectionResponse.Response result = objSoap.callObject.GetXML
                <UpdateProviderSelectionResponse.Response>(conn, source.Get(), forceJSONList);

            return result;
        }

        #endregion MemberServiceStronglyTyped - UpdateProviderSelection
    }
}

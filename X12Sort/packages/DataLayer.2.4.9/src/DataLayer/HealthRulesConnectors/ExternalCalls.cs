using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Newtonsoft.Json;
using DataLayer.StaticUtilities;

namespace DataLayer.HealthRulesConnectors
{
   public class ExternalCalls
    {

        /// <summary>
        /// base URL for all calls
        /// </summary>
        public string BaseURL = "";


        public T GetJSON<T>(ExternalCallsProperties properties, string envelope)
        {
            string source = Get(properties, envelope);
            return JsonConvert.DeserializeObject<T>(source);
        }

        /// <summary>
        /// Get XML from and convert it to a C# object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="properties"></param>
        /// <param name="envelope"></param>
        /// <param name="xmlTagsAsArray">Force Newton JSON to see XML as Arrays when there is only 1 instance of an object</param>
        /// <returns></returns>
        public T GetXML<T>(ExternalCallsProperties properties, string envelope, List<string> xmlTagsAsArray)
        {
            string source = GetSOAP(properties, envelope);
            source = XMLSerialization.RemoveEmptyTags(source);
            string json = XMLSerialization.XML2JSON(source, xmlTagsAsArray);

            return JsonConvert.DeserializeObject<T>(json);
        }

        /// <summary>
        /// Get XML from and convert it to a C# object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="properties"></param>
        /// <param name="envelope"></param>        
        /// <returns></returns>
        public T GetXML<T>(ExternalCallsProperties properties, string envelope)
        {
            return GetXML<T>(properties, envelope, new List<string> { });
        }

        /// <summary>
        /// Get the raw XML from a call and return it as an XmlDocument
        /// </summary>
        /// <param name="properties"></param>
        /// <param name="envelope"></param>
        /// <returns></returns>
        public XmlDocument GetXML(ExternalCallsProperties properties, string envelope)
        {
            string source = "";

            try
            {
                source = GetSOAP(properties, envelope);
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(source);
                return xml;
            }
            catch (Exception ex)
            {
                string error = string.Format("Exception: {0}\r\nValue returned: {1}",
                    ex.Message, source);
                throw new Exception(error, ex.InnerException);
            }
        }

        /// <summary>
        /// Get raw text from a call
        /// </summary>
        /// <param name="properties">Set request properties like POST verb and headers</param>
        /// <param name="envelope">Text envolope you want to send</param>
        /// <returns></returns>
        public string Get(ExternalCallsProperties properties, string envelope)
        {

            HttpWebRequest request = CreateWebRequest(properties);

            // Write text array

            byte[] byteArray = Encoding.UTF8.GetBytes(envelope);

            if (properties.verb.ToUpper() != "GET")
            {
                Stream stream = request.GetRequestStream();
                stream.Write(byteArray, 0, byteArray.Length);
            }

            StringBuilder output = new StringBuilder();

            try
            {

                using (WebResponse response = request.GetResponse())
                {
                    using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                    {
                        string soapResult = rd.ReadToEnd();
                        output.Append(soapResult);
                    }
                }

            }
            catch (WebException e)
            {
                using (WebResponse response = e.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)response;
                    using (Stream data = response.GetResponseStream())
                    using (var reader = new StreamReader(data))
                    {
                        output.Append(reader.ReadToEnd());
                    }
                }
            }

            if(output == null)
            {
                string error = string.Format(
                    "ExternalCalls.Get() - No results returned - {0} - {1}", properties.username, properties.url
                    );
                throw new Exception(error);
            }

            return output.ToString();

        }

        /// <summary>
        /// Get the raw text from a SOAP call
        /// </summary>
        /// <param name="url">URL appended to base URL to pull from</param>
        /// <param name="requestEnv">Request envolope</param>
        /// <param name="verb">Verb (ex. POST, GET, PUT)</param>
        /// <returns>Result of the response</returns>
        public string GetSOAP(ExternalCallsProperties properties, string envelope)
        {

            HttpWebRequest request = CreateWebRequest(properties);
            XmlDocument soapEnvelopeXml = new XmlDocument();
            soapEnvelopeXml.LoadXml(envelope);

            StringBuilder output = new StringBuilder();

            try
            {

                using (Stream stream = request.GetRequestStream())
                {
                    soapEnvelopeXml.Save(stream);
                }

                using (WebResponse response = request.GetResponse())
                {
                    using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                    {
                        string soapResult = rd.ReadToEnd();
                        output.Append(soapResult);
                    }
                }

                return output.ToString();
            }
            catch (WebException e)
            {
                using (WebResponse response = e.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)response;
                    using (Stream data = response.GetResponseStream())
                    using (var reader = new StreamReader(data))
                    {
                        output.Append(reader.ReadToEnd());
                    }
                }

                return output.ToString();
            }
            catch (Exception ex)
            {
                string error = string.Format("Message: {0}, Output: {1}", ex.Message, output.ToString());
                throw new Exception(error, ex.InnerException);
            }
        }


        /// <summary>
        /// Create a webrequest to URL, this will ONLY do SOAP request
        /// </summary>
        /// <param name="url">URL to call</param>
        /// <param name="username">Basic authtication username, if left blank authication will happen</param>
        /// <param name="password">Basic authtication password</param>
        /// <param name="verb">Method to access the URL, default verb is POST</param>
        /// <returns>.NET Web Request</returns>
        private HttpWebRequest CreateWebRequest(ExternalCallsProperties properties)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(BaseURL + properties.url);

            // Content Type
            webRequest.ContentType = properties.contentType;

            // PreAuthenticate
            if (properties.preAuthenticate)
            {
                webRequest.PreAuthenticate = true;
            }

            // Basic auth
            if (!string.IsNullOrWhiteSpace(properties.username))
            {
                String encoded = System.Convert.ToBase64String(
                    System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(properties.username + ":" + properties.password));
                webRequest.Headers.Add("Authorization", "Basic " + encoded);
            }

            // Token auth
            if (!string.IsNullOrWhiteSpace(properties.accessToken))
            {
                webRequest.PreAuthenticate = true;
                webRequest.Headers.Add("Authorization", "Bearer " + properties.accessToken);
            }

            // Headers
            foreach (KeyValuePair<string, string> headerItem in properties.headers)
            {
                if (string.IsNullOrEmpty(headerItem.Value))
                {
                    webRequest.Headers.Add(headerItem.Key);
                }
                else
                {
                    webRequest.Headers.Add(headerItem.Key, headerItem.Value);
                }
            }

            // Accept and Method
            webRequest.Accept = properties.acceptType;
            webRequest.Method = properties.verb;
            return webRequest;
        }


    }

    /// <summary>
    /// External call settings
    /// </summary>
    public class ExternalCallsProperties
    {

        /// <summary>
        /// Content type
        /// </summary>
        public string contentType = "text/xml;charset=\"utf-8\""; // JSON should be "application/json"

        /// <summary>
        /// Base URL
        /// </summary>
        public string url { get; set; }

        /// <summary>
        /// Username
        /// </summary>
        public string username { get; set; }

        /// <summary>
        /// Password
        /// </summary>
        public string password { get; set; }

        /// <summary>
        /// Access token - ussually an encoded username and password
        /// </summary>
        public string accessToken { get; set; }

        /// <summary>
        /// Specialized headers
        /// </summary>
        public Dictionary<string, string> headers = new Dictionary<string, string>();

        /// <summary>
        /// Is preauthication required?
        /// </summary>
        public bool preAuthenticate = false;

        /// <summary>
        /// Accept type
        /// </summary>
        public string acceptType = "text/xml"; // JSON should be "application/json"

        /// <summary>
        /// 
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// Verb (GET, POST, PUT, DELETE, or custom request that REST may have)
        /// </summary>
        public string verb = "POST";

    }
}

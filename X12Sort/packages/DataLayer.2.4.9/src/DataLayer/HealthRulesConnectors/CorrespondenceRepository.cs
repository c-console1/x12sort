using System.Xml;
using DataLayer.Base;
using DataLayer.HealthRulesConnectors.Enumerations;
using DataLayer.HealthRulesConnectors.Models;
using DataLayer.HealthRulesConnectors.Models.Requests;

namespace DataLayer.HealthRulesConnectors
{
    public class CorrespondenceRepository : BaseRepository<HealthRulesConnectorsDatabase>
    {
        internal CorrespondenceRepository(HealthRulesConnectorsDatabase database) : base(database)
        {

        }

        #region CorrespondenceServiceStronglyTyped - FindCorrespondence

        /// <summary>
        /// FindCorrespondence Connector Call that returns an XmlDocument object
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public string FindCorrespondenceText(FindCorrespondenceRequest source)
        {
            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.Classic);

            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "CorrespondenceServiceStronglyTyped"
            };

            string xml = objSoap.callObject.Get(conn, source.Get());
            return xml;
        }

        /// <summary>
        /// FindCorrespondence Connector Call that returns an XmlDocument object
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public XmlDocument FindCorrespondence(FindCorrespondenceRequest source)
        {
            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.Classic);

            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "CorrespondenceServiceStronglyTyped"
            };

            string xml = objSoap.callObject.Get(conn, source.Get());
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            return doc;
        }

        #endregion CorrespondenceServiceStronglyTyped - FindCorrespondence
    }
}

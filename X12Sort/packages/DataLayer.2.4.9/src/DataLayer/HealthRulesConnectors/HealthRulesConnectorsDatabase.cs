using DataLayer.Base;
using DataLayer.HealthRulesConnectors.Enumerations;
using DataLayer.HealthRulesConnectors.Models;
using System;
using System.Linq;

namespace DataLayer.HealthRulesConnectors
{
    /// <summary>
    /// HealthRules Connectors Database Object
    /// </summary>
    public class HealthRulesConnectorsDatabase : BaseDatabase
    {
        public ApiExec ClassicAPI { get; private set; }
        public ApiExec StandardAPI { get; private set; }

        public AccountRepository Account { get; private set; }
        public AuthorizationRepository Authorization { get; private set; }
        public CorrespondenceRepository Correspondence { get; private set; }
        public MemberRepository Member { get; private set; }
        public PractitionerRepository Practitioner { get; private set; }
        public SubscriberRepository Subscriber { get; private set; }

        /// <summary>
        /// API executor to make API calls
        /// </summary>
        public AuthorizationAPIRepository AuthorizationAPIRepository { get; set; }

        /// <summary>
        /// Instantiate HealthRules Connectors Database Object
        /// </summary>
        /// <param name="executors">Database executers</param>
        internal HealthRulesConnectorsDatabase(params IExecutor[] executors) : base(executors)
        {
            ClassicAPI = Executors.First(q => q.DatabaseGroup.Contains("HEALTHRULESCONNECTORS_CLASSIC")) as ApiExec;
            StandardAPI = Executors.First(q => q.DatabaseGroup.Contains("HEALTHRULESCONNECTORS_STANDARD")) as ApiExec;

            Account = new AccountRepository(this);
            Authorization = new AuthorizationRepository(this);
            Correspondence = new CorrespondenceRepository(this);
            Member = new MemberRepository(this);
            Practitioner = new PractitionerRepository(this);
            Subscriber = new SubscriberRepository(this);
        }

        /// <summary>
        /// Generate settings to send to use to make a call
        /// </summary>
        /// <param name="versionID">API version</param>
        /// <returns></returns>
        internal GenerateCallData GenerateCall(ApiVersionNumber versionID)
        {
            GenerateCallData output = new GenerateCallData();
            output.callObject = new ExternalCalls();

            switch (versionID)
            {
                // Classic connector, requires special settings
                case ApiVersionNumber.Classic:
                    output.callObject.BaseURL = ClassicAPI.Client.BaseUrl + "classic/";
                    output.username = ClassicAPI.UserID;
                    output.password = ClassicAPI.Password;
                    break;

                // Versions 1-9 of the standard connector call
                case ApiVersionNumber.V1:
                case ApiVersionNumber.V2:
                case ApiVersionNumber.V3:
                case ApiVersionNumber.V4:
                case ApiVersionNumber.V5:
                case ApiVersionNumber.V6:
                case ApiVersionNumber.V7:
                case ApiVersionNumber.V8:
                case ApiVersionNumber.V9:
                    output.callObject.BaseURL = StandardAPI.Client.BaseUrl + $"v{(int)versionID}/";
                    output.username = StandardAPI.UserID;
                    output.password = StandardAPI.Password;
                    break;

                // Fallback to Version 4 which most calls use
                default:
                    output.callObject.BaseURL = StandardAPI.Client.BaseUrl + "v4/";
                    output.username = StandardAPI.UserID;
                    output.password = StandardAPI.Password;
                    break;
            }

            return output;
        }
    }
}

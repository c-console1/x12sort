using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;

namespace DataLayer.StaticUtilities
{
    /// <summary>
    /// A collection of static helper with Oracle database operations
    /// </summary>
    public class TypeConverter
    {
        /// <summary>
        /// Translates a .NET type to an OracleDbType
        /// </summary>
        /// <param name="type">.NET type to translate</param>
        /// <returns></returns>
        public static OracleDbType ToOracleDbType(Type type)
        {
            var typeMap = new Dictionary<Type, OracleDbType>
            {
                [typeof(byte)] = OracleDbType.Byte,
                [typeof(sbyte)] = OracleDbType.Byte,
                [typeof(short)] = OracleDbType.Int16,
                [typeof(ushort)] = OracleDbType.Int16,
                [typeof(int)] = OracleDbType.Int32,
                [typeof(uint)] = OracleDbType.Int32,
                [typeof(long)] = OracleDbType.Int64,
                [typeof(ulong)] = OracleDbType.Int64,
                [typeof(float)] = OracleDbType.Single,
                [typeof(double)] = OracleDbType.Double,
                [typeof(decimal)] = OracleDbType.Decimal,
                [typeof(bool)] = OracleDbType.Boolean,
                [typeof(string)] = OracleDbType.Varchar2,
                [typeof(char)] = OracleDbType.Char,
                [typeof(Guid)] = OracleDbType.Varchar2,
                [typeof(DateTime)] = OracleDbType.Date,
                [typeof(DateTimeOffset)] = OracleDbType.Date,
                [typeof(TimeSpan)] = OracleDbType.TimeStamp,
                [typeof(byte[])] = OracleDbType.Blob,
                [typeof(byte?)] = OracleDbType.Byte,
                [typeof(sbyte?)] = OracleDbType.Byte,
                [typeof(short?)] = OracleDbType.Int16,
                [typeof(ushort?)] = OracleDbType.Int16,
                [typeof(int?)] = OracleDbType.Int32,
                [typeof(uint?)] = OracleDbType.Int32,
                [typeof(long?)] = OracleDbType.Int64,
                [typeof(ulong?)] = OracleDbType.Int64,
                [typeof(float?)] = OracleDbType.Single,
                [typeof(double?)] = OracleDbType.Double,
                [typeof(decimal?)] = OracleDbType.Decimal,
                [typeof(bool?)] = OracleDbType.Boolean,
                [typeof(char?)] = OracleDbType.Varchar2,
                [typeof(Guid?)] = OracleDbType.Varchar2,
                [typeof(DateTime?)] = OracleDbType.Date,
                [typeof(DateTimeOffset?)] = OracleDbType.Date,
                [typeof(TimeSpan?)] = OracleDbType.TimeStamp,
                [typeof(object)] = OracleDbType.Blob
            };
            return typeMap[type];
        }

        /// <summary>
        /// Translates a .NET type to an DbType
        /// </summary>
        /// <param name="type">.NET type to translate</param>
        /// <remarks>
        /// This list of conversion is from the dapper library
        /// </remarks>
        public static DbType ToDbType(Type type)
        {
            var typeMap = new Dictionary<Type, DbType>
            {
                [typeof(byte)] = DbType.Byte,
                [typeof(sbyte)] = DbType.SByte,
                [typeof(short)] = DbType.Int16,
                [typeof(ushort)] = DbType.UInt16,
                [typeof(int)] = DbType.Int32,
                [typeof(uint)] = DbType.UInt32,
                [typeof(long)] = DbType.Int64,
                [typeof(ulong)] = DbType.UInt64,
                [typeof(float)] = DbType.Single,
                [typeof(double)] = DbType.Double,
                [typeof(decimal)] = DbType.Decimal,
                [typeof(bool)] = DbType.Boolean,
                [typeof(string)] = DbType.String,
                [typeof(char)] = DbType.StringFixedLength,
                [typeof(Guid)] = DbType.Guid,
                [typeof(DateTime)] = DbType.DateTime,
                [typeof(DateTimeOffset)] = DbType.DateTimeOffset,
                [typeof(TimeSpan)] = DbType.Time,
                [typeof(byte[])] = DbType.Binary,
                [typeof(byte?)] = DbType.Byte,
                [typeof(sbyte?)] = DbType.SByte,
                [typeof(short?)] = DbType.Int16,
                [typeof(ushort?)] = DbType.UInt16,
                [typeof(int?)] = DbType.Int32,
                [typeof(uint?)] = DbType.UInt32,
                [typeof(long?)] = DbType.Int64,
                [typeof(ulong?)] = DbType.UInt64,
                [typeof(float?)] = DbType.Single,
                [typeof(double?)] = DbType.Double,
                [typeof(decimal?)] = DbType.Decimal,
                [typeof(bool?)] = DbType.Boolean,
                [typeof(char?)] = DbType.StringFixedLength,
                [typeof(Guid?)] = DbType.Guid,
                [typeof(DateTime?)] = DbType.DateTime,
                [typeof(DateTimeOffset?)] = DbType.DateTimeOffset,
                [typeof(TimeSpan?)] = DbType.Time,
                [typeof(object)] = DbType.Object
            };
            return typeMap[type];
        }
    }
}

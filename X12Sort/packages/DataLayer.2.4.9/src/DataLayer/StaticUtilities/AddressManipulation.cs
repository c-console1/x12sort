using DataLayer.Base.Models;
using System;
using USAddress;

namespace DataLayer.StaticUtilities
{
    public static class AddressManipulation
    {
        private static AddressParser _parser;

        internal static AddressParser Parser
        {
            get
            {
                if (_parser == null)
                {
                    _parser = new AddressParser();
                }

                return _parser;
            }
        }

        /// <summary>
        /// Parses an address and attempts to format to a standard format.
        /// "1901 indian wood Circle Maumee oh 43537" -> "1901 Indian Wood Cir Maumee, OH 43537"
        /// </summary>
        /// <param name="input">Address to format</param>
        /// <remarks>
        /// Code consuming the results of this funciton will need AddressParser by Jim Counts from NuGet
        /// https://github.com/jamesrcounts/usaddress
        /// </remarks>
        /// <returns></returns>
        public static AddressParseResult Parse(string input)
        {
            AddressParseResult parserResult = _parser.ParseAddress(input);
            return parserResult;
        }


        /// <summary>
        /// Converts a two character abbreviated state to full length. Case insensitive.
        /// OH -> Ohio
        /// </summary>
        /// <param name="abbreviation">State to expand</param>
        /// <returns></returns>
        public static string StateAbbreviationToVerbose(string abbreviation)
        {
            if (abbreviation != null)
            {
                switch (abbreviation.ToUpper())
                {
                    case "AL": return "Alabama";
                    case "AK": return "Alaska";
                    case "AZ": return "Arizona";
                    case "AR": return "Arkansas";
                    case "CA": return "California";
                    case "CO": return "Colorado";
                    case "CT": return "Connecticut";
                    case "DE": return "Delaware";
                    case "FL": return "Florida";
                    case "GA": return "Georgia";
                    case "HI": return "Hawaii";
                    case "ID": return "Idaho";
                    case "IL": return "Illinois";
                    case "IN": return "Indiana";
                    case "IA": return "Iowa";
                    case "KS": return "Kansas";
                    case "KY": return "Kentucky";
                    case "LA": return "Louisiana";
                    case "ME": return "Maine";
                    case "MD": return "Maryland";
                    case "MA": return "Massachusetts";
                    case "MI": return "Michigan";
                    case "MN": return "Minnesota";
                    case "MS": return "Mississippi";
                    case "MO": return "Missouri";
                    case "MT": return "Montana";
                    case "NE": return "Nebraska";
                    case "NV": return "Nevada";
                    case "NH": return "New Hampshire";
                    case "NJ": return "New Jersey";
                    case "NM": return "New Mexico";
                    case "NY": return "New York";
                    case "NC": return "North Carolina";
                    case "ND": return "North Dakota";
                    case "OH": return "Ohio";
                    case "OK": return "Oklahoma";
                    case "OR": return "Oregon";
                    case "PA": return "Pennsylvania";
                    case "RI": return "Rhode Island";
                    case "SC": return "South Carolina";
                    case "SD": return "South Dakota";
                    case "TN": return "Tennessee";
                    case "TX": return "Texas";
                    case "UT": return "Utah";
                    case "VT": return "Vermont";
                    case "VA": return "Virginia";
                    case "WA": return "Washington";
                    case "WV": return "West Virginia";
                    case "WI": return "Wisconsin";
                    case "WY": return "Wyoming";

                    // hail Canada
                    case "AB": return "ALBERTA";
                    case "BC": return "BRITISH COLUMBIA";
                    case "MB": return "MANITOBA";
                    case "NB": return "NEW BRUNSWICK";
                    case "NL": return "NEWFOUNDLAND AND LABRADOR";
                    case "NT": return "NORTHWEST TERRITORIES";
                    case "NS": return "NOVA SCOTIA";
                    case "NU": return "NUNAVUT";
                    case "ON": return "ONTARIO";
                    case "PE": return "PRINCE EDWARD ISLAND";
                    case "QC": return "QUÉBEC";
                    case "SK": return "SASKATCHEWAN";
                    case "YT": return "YUKON";
                }
            }

            return "";
        }

        /// <summary>
        /// Converts a full length state state to its two character abbreviation. Case insensitive.
        /// Ohio -> OH
        /// </summary>
        /// <param name="verbose">State to abbreviate</param>
        /// <returns></returns>
        public static string StateVerboseToAbbreviation(string verbose)
        {
            if (verbose != null)
            {
                switch (verbose.ToUpper())
                {
                    case "ALABAMA": return "AL";
                    case "ALASKA": return "AK";
                    case "ARIZONA": return "AZ";
                    case "ARKANSAS": return "AR";
                    case "CALIFORNIA": return "CA";
                    case "COLORADO": return "CO";
                    case "CONNECTICUT": return "CT";
                    case "DELAWARE": return "DE";
                    case "FLORIDA": return "FL";
                    case "GEORGIA": return "GA";
                    case "HAWAII": return "HI";
                    case "IDAHO": return "ID";
                    case "ILLINOIS": return "IL";
                    case "INDIANA": return "IN";
                    case "IOWA": return "IA";
                    case "KANSAS": return "KS";
                    case "KENTUCKY": return "KY";
                    case "LOUISIANA": return "LA";
                    case "MAINE": return "ME";
                    case "MARYLAND": return "MD";
                    case "MASSACHUSETTS": return "MA";
                    case "MICHIGAN": return "MI";
                    case "MINNESOTA": return "MN";
                    case "MISSISSIPPI": return "MS";
                    case "MISSOURI": return "MO";
                    case "MONTANA": return "MT";
                    case "NEBRASKA": return "NE";
                    case "NEVADA": return "NV";
                    case "NEW HAMPSHIRE": return "NH";
                    case "NEW JERSEY": return "NJ";
                    case "NEW MEXICO": return "NM";
                    case "NEW YORK": return "NY";
                    case "NORTH CAROLINA": return "NC";
                    case "NORTH DAKOTA": return "ND";
                    case "OHIO": return "OH";
                    case "OKLAHOMA": return "OK";
                    case "OREGON": return "OR";
                    case "PENNSYLVANIA": return "PA";
                    case "RHODE ISLAND": return "RI";
                    case "SOUTH CAROLINA": return "SC";
                    case "SOUTH DAKOTA": return "SD";
                    case "TENNESSEE": return "TN";
                    case "TEXAS": return "TX";
                    case "UTAH": return "UT";
                    case "VERMONT": return "VT";
                    case "VIRGINIA": return "VA";
                    case "WASHINGTON": return "WA";
                    case "WEST VIRGINIA": return "WV";
                    case "WISCONSIN": return "WI";
                    case "WYOMING": return "WY";

                    // hail canada
                    case "ALBERTA": return "AB";
                    case "BRITISH COLUMBIA": return "BC";
                    case "MANITOBA": return "MB";
                    case "NEW BRUNSWICK": return "NB";
                    case "NEWFOUNDLAND AND LABRADOR": return "NL";
                    case "NORTHWEST TERRITORIES": return "NT";
                    case "NOVA SCOTIA": return "NS";
                    case "NUNAVUT": return "NU";
                    case "ONTARIO": return "ON";
                    case "PRINCE EDWARD ISLAND": return "PE";
                    case "QUÉBEC": return "QC";
                    case "SASKATCHEWAN": return "SK";
                    case "YUKON": return "YT";
                }
            }

            return "";
        }

        //public static Country ToCountry(string name)
        //{
        //  
        //}
    }
}

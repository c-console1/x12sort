using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace DataLayer.StaticUtilities
{
    /// <summary>
    /// Collection of Static Functions for String and Character Operations
    /// </summary>
    public static class StringManipulation
    {
        #region Extension Methods

        /// <summary>
        /// Count occurrence of a string in a string
        /// </summary>
        /// <param name="instr">   </param>
        /// <param name="search">  </param>
        /// <returns>  </returns>
        public static int CountOccurrences(this string instr, string search)
        {
            return Regex.Matches(instr, search).Count;
        }

        /// <summary>
        /// Count Occurrences of String inside a string
        /// </summary>
        /// <param name="sourceLine">    Source line </param>
        /// <param name="word">          Word to search by </param>
        /// <param name="caseSensitive"> Is this sensitive? </param>
        /// <returns> Number of occurrences found </returns>
        public static int CountOccurrencesOf(this string sourceLine, string word, bool caseSensitive = false)
        {
            int strt = 0;
            int cnt = -1;
            int idx = -1;
            while (strt != -1)
            {
                strt = sourceLine.IndexOf(word, idx + 1,
                    caseSensitive ? StringComparison.InvariantCultureIgnoreCase : StringComparison.InvariantCulture);
                cnt += 1;
                idx = strt;
            }

            return cnt;
        }

        /// <summary>
        /// Count words in a string
        /// </summary>
        /// <param name="sourceLine"> Source line </param>
        /// <returns> Number of word </returns>
        public static int CountWords(this string sourceLine)
        {
            if (string.IsNullOrWhiteSpace(sourceLine))
            {
                return 0;
            }

            MatchCollection wordColl = Regex.Matches(sourceLine, @"[\S]+");
            return wordColl.Count;
        }

        /// <summary>
        /// GZIP Compress String
        /// </summary>
        /// <param name="value"> String value </param>
        /// <returns> Compressed string </returns>
        public static string Compress(this string value)
        {
            ////Transform string into byte[]
            byte[] byteArray = new byte[value.Length];
            int indexBA = 0;
            foreach (char item in value.ToCharArray())
            {
                byteArray[indexBA++] = (byte)item;
            }

            ////Prepare for compress
            MemoryStream ms = new MemoryStream();
            GZipStream sw = new GZipStream(ms, CompressionMode.Compress);

            ////Compress
            sw.Write(byteArray, 0, byteArray.Length);
            ////Close, DO NOT FLUSH cause bytes will go missing...
            sw.Close();

            ////Transform byte[] zip data to string
            byteArray = ms.ToArray();
            StringBuilder sB = new StringBuilder(byteArray.Length);
            foreach (byte item in byteArray)
            {
                sB.Append((char)item);
            }

            ms.Close();
            sw.Dispose();
            ms.Dispose();
            return sB.ToString();
        }

        /// <summary>
        /// Decompress GZIP'ed string to uncompressed string
        /// </summary>
        /// <param name="value"> Source string </param>
        /// <returns> Decompressed string </returns>
        public static string Decompress(this string value)
        {
            ////Transform string into byte[]
            byte[] byteArray = new byte[value.Length];
            int indexBA = 0;
            foreach (char item in value.ToCharArray())
            {
                byteArray[indexBA++] = (byte)item;
            }

            ////Prepare for decompress
            MemoryStream ms = new MemoryStream(byteArray);
            GZipStream sr = new GZipStream(ms, CompressionMode.Decompress);

            ////Reset variable to collect uncompressed result
            byteArray = new byte[byteArray.Length];

            ////Decompress
            int indByte = sr.Read(byteArray, 0, byteArray.Length);

            ////Transform byte[] unzip data to string
            StringBuilder sB = new StringBuilder(indByte);
            ////Read the number of bytes GZipStream red and do not a for each bytes in
            ////resultByteArray;
            for (int i = 0; i < indByte; i++)
            {
                sB.Append((char)byteArray[i]);
            }

            sr.Close();
            ms.Close();
            sr.Dispose();
            ms.Dispose();
            return sB.ToString();
        }

        /// <summary>
        /// Replaces . , - ( ) \ " with a space. Handy when getting addresses into a standard format.
        /// </summary>
        /// <param name="input"> String to remove punctuation from </param>
        /// <param name="value"> The value to replace with, defaults to a space</param>
        /// <returns>  </returns>
        public static string ReplacePunctuation(this string input, string value = " ")
        {
            if (input != null)
                input = Regex.Replace(input, @"[\.,\-\)\(\\\""]", value);
            return input;
        }

        /// <summary>
        /// Reverse the string
        /// </summary>
        /// <param name="sourceLine"> Source string </param>
        /// <returns> Reversed string </returns>
        public static string Reverse(this string sourceLine)
        {
            char[] inputarray = sourceLine.ToCharArray();
            Array.Reverse(inputarray);
            return new string(inputarray);
        }

        /// <summary>
        /// Cleans the string of anything that is not a alphanumeric or underscore character
        /// </summary>
        /// <param name="str"> The string to sanitize </param>
        /// <returns>  </returns>
        public static string Sanitize(this string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return str;
            }
            else
            {
                return new string(str.Where(q => char.IsLetterOrDigit(q) || q == '_').ToArray());
            }
        }

        /// <summary>
        /// Cleans the string of anything that is not a alphanumeric or other char in <paramref name="charsToKeep"/>
        /// </summary>
        /// <param name="str"> The string to sanitize </param>
        /// <param name="charsToKeep">A list of characters that are valid and should be kept</param>
        /// <returns>  </returns>
        public static string Sanitize(this string str, params char[] charsToKeep)
        {
            HashSet<char> set = new HashSet<char>(charsToKeep);
            if (string.IsNullOrEmpty(str))
            {
                return str;
            }
            else
            {
                return new string(str.Where(q => char.IsLetterOrDigit(q) || set.Contains(q) ).ToArray());
            }
        }

        /// <summary>
        /// Split a CSV string into a list, parse double quotes
        /// </summary>
        /// <param name="sourceLine"> Source text to split </param>
        /// <returns> List of string </returns>
        public static List<string> SplitCsv(this string sourceLine)
        {
            List<string> output = new List<string>();

            Regex regex = new Regex("(?<=^|,)(\"(?:[^\"]|\"\")*\"|[^,]*)");

            foreach (Match m in regex.Matches(sourceLine))
            {
                output.Add(m.Value);
            }

            return output;
        }

        /// <summary>
        /// Splits string into array with chunks of given size. Split is bases on characters, not words
        /// </summary>
        /// <param name="toSplit"> This string to split into chunks </param>
        /// <param name="chunkSize"> the number of chunks to make </param>
        /// <returns>  </returns>
        public static string[] SplitIntoChunks(this string toSplit, int chunkSize)
        {
            if (string.IsNullOrEmpty(toSplit))
                return new string[] { "" };

            decimal stringLength = toSplit.Length;

            int chunksRequired = (int)Math.Ceiling(stringLength / chunkSize);
            var stringArray = new string[chunksRequired];

            int lengthRemaining = (int)stringLength;

            for (int i = 0; i < chunksRequired; i++)
            {
                int lengthToUse = Math.Min(lengthRemaining, chunkSize);
                int startIndex = chunkSize * i;
                stringArray[i] = toSplit.Substring(startIndex, lengthToUse);

                lengthRemaining = lengthRemaining - lengthToUse;
            }

            return stringArray;
        }

        /// <summary>
        /// Convert multiple sequential spaces into a single space. "a  space" -&gt; "a space"
        /// </summary>
        /// <param name="input"> String to remove extra spaces from </param>
        /// <returns>  </returns>
        public static string StripMultipleWhitespace(this string input)
        {
            if (input != null)
                input = Regex.Replace(input, @"\s+", " ").Trim();

            return input;
        }

        /// <summary>
        /// Strip all non numeric characters from a string
        /// </summary>
        /// <param name="input"> input value </param>
        /// <returns> numbers found </returns>
        public static string StripNonDigits(this string input)
        {
            if (string.IsNullOrEmpty(input))
                return input;
            else
                return new string(input.Where(c => char.IsDigit(c)).ToArray());
        }

        /// <summary>
        /// Strip HTML or XML tags from a string
        /// </summary>
        /// <param name="html">  </param>
        /// <returns>  </returns>
        public static string StripXmlTags(this string html)
        {
            if (string.IsNullOrEmpty(html))
                return string.Empty;

            return Regex.Replace(html, @"<[^>]*>", string.Empty);
        }

        /// <summary>
        /// String to Base64 string
        /// </summary>
        /// <param name="origString">  </param>
        /// <returns>  </returns>
        public static string ToBase64(this string origString)
        {
            byte[] byt = Encoding.UTF8.GetBytes(origString);
            return Convert.ToBase64String(byt);
        }

        /// <summary>
        /// Base64 string to string
        /// </summary>
        /// <param name="origString">  </param>
        /// <returns>  </returns>
        public static string FromBase64(this string origString)
        {
            byte[] base64EncodedBytes = Convert.FromBase64String(origString);
            return Encoding.UTF8.GetString(base64EncodedBytes);
        }

        /// <summary>
        /// Convert a string to a byte array
        /// </summary>
        /// <param name="sourceLine">  </param>
        /// <returns>  </returns>
        public static byte[] ToByteArray(this string sourceLine)
        {
            return Encoding.Unicode.GetBytes(sourceLine);
        }

        /// <summary>
        /// String to Stream
        /// </summary>
        /// <param name="s"> String value </param>
        /// <returns> Stream </returns>
        public static Stream ToStream(this string s)
        {
            byte[] byteArray = Encoding.ASCII.GetBytes(s);
            MemoryStream stream = new MemoryStream(byteArray);
            return stream;
        }

        /// <summary>
        /// Converts string to title case. "a random String" -&gt; "A Random String"
        /// </summary>
        /// <param name="input"> String to convert to title case </param>
        /// <returns>  </returns>
        public static string ToTitleCase(this string input)
        {
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;

            if (input != null)
                input = textInfo.ToTitleCase(input.ToLower());

            return input;
        }

        /// <summary>
        /// Repeat a character multiple times
        /// </summary>
        /// <param name="ch">Character to repeat</param>
        /// <param name="num">Number of repetitions</param>
        /// <returns>A string representing the repeated charaters</returns>
        public static string Repeat(this char ch, int num)
        {
            if(num < 0 )
            {
                throw new ArgumentException("Number of repetitions must be a whole number", nameof(num));
            }
            return string.Empty.PadLeft(num, ch);
        }

        /// <summary>
        /// Determine whether string is a vlid email address
        /// </summary>
        /// //Valid: david.jones@proseware.com
        /// <remarks>https://docs.microsoft.com/en-us/dotnet/standard/base-types/how-to-verify-that-strings-are-in-valid-email-format</remarks>
        /// Valid:   jones@ms1.proseware.com
        /// Valid:   j@proseware.com9
        /// Valid:   js#internal@proseware.com
        /// Valid:   j_9@[129.126.118.1]
        /// Valid:   "j\"s\""@proseware.com
        /// Valid:   js@contoso.中国
        /// Invalid: j.@server1.proseware.com
        /// Invalid: js*@proseware.com
        /// Invalid: js@proseware..com
        /// <param name="emailAddress"></param>
        /// <returns>Whether string is an email address</returns>
        public static bool IsValidEmailAddress(this string emailAddress)
        {
            if (string.IsNullOrWhiteSpace(emailAddress))
                return false;

            try
            {
                // Normalize the domain
                emailAddress = Regex.Replace(emailAddress, @"(@)(.+)$", DomainMapper, RegexOptions.None, TimeSpan.FromMilliseconds(200));

                // Examines the domain part of the email and normalizes it.
                string DomainMapper(Match match)
                {
                    // Use IdnMapping class to convert Unicode domain names.
                    var idn = new IdnMapping();

                    // Pull out and process domain name (throws ArgumentException on invalid)
                    var domainName = idn.GetAscii(match.Groups[2].Value);

                    return match.Groups[1].Value + domainName;
                }
            }
            catch (RegexMatchTimeoutException e)
            {
                return false;
            }
            catch (ArgumentException e)
            {
                return false;
            }

            try
            {
                #region Detailed Explanation
                /*
                    The regular expression pattern is interpreted as shown in the following legend. The regular expression is compiled using the RegexOptions.IgnoreCase flag.

                    Pattern ^: Begin the match at the start of the string.

                    Pattern (?("): Determine whether the first character is a quotation mark. (?(") is the beginning of an alternation construct.

                    Pattern (?(")(".+?(?<!\\)"@): If the first character is a quotation mark, match a beginning quotation mark followed by at least one occurrence of any character,
                       followed by an ending quotation mark. The ending quotation mark must not be preceded by a backslash character (\). (?<! is the beginning of a zero-width negative
                       lookbehind assertion. The string should conclude with an at sign (@).

                    Pattern |(([0-9a-z]: If the first character is not a quotation mark, match any alphabetic character from a to z or A to Z (the comparison is case insensitive), or
                       any numeric character from 0 to 9.

                    Pattern (\.(?!\.)): If the next character is a period, match it. If it is not a period, look ahead to the next character and continue the match. (?!\.) is a
                       zero-width negative lookahead assertion that prevents two consecutive periods from appearing in the local part of an email address.

                    Pattern |[-!#\$%&'\*\+/=\?\^`\{\}\|~\w]: If the next character is not a period, match any word character or one of the following characters: -!#$%&'*+/=?^`{}|~

                    Pattern ((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*: Match the alternation pattern (a period followed by a non-period, or one of a number of characters) zero or
                       more times.

                    Pattern @: Match the @ character.

                    Pattern (?<=[0-9a-z]): Continue the match if the character that precedes the @ character is A through Z, a through z, or 0 through 9. This pattern defines a
                       zero-width positive lookbehind assertion.

                    Pattern (?(\[): Check whether the character that follows @ is an opening bracket.

                    Pattern (\[(\d{1,3}\.){3}\d{1,3}\]): If it is an opening bracket, match the opening bracket followed by an IP address (four sets of one to three digits, with each
                       set separated by a period) and a closing bracket.

                    Pattern |(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+: If the character that follows @ is not an opening bracket, match one alphanumeric character with a value of A-Z, a-z, or
                       0-9, followed by zero or more occurrences of a hyphen, followed by zero or one alphanumeric character with a value of A-Z, a-z, or 0-9, followed by a period. This
                       pattern can be repeated one or more times, and must be followed by the top-level domain name.

                    Pattern [a-z0-9][\-a-z0-9]{0,22}[a-z0-9])): The top-level domain name must begin and end with an alphanumeric character (a-z, A-Z, and 0-9). It can also include from
                       zero to 22 ASCII characters that are either alphanumeric or hyphens.

                    Pattern $: End the match at the end of the string.
                 */
                #endregion
                // TLDR...
                // This regular expression merely verifies that the top-level domain name consists of between two and twenty-four ASCII characters,
                // with alphanumeric first and last characters and the remaining characters being either alphanumeric or a hyphen (-)
                return Regex.IsMatch(emailAddress,
                    @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                    @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                    RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        #endregion Extension Methods

        /// <summary>
        /// Break the words in a paragraph and wrap words to a maximum width. This was made for logs.
        /// </summary>
        /// <param name="MaxLength">  </param>
        /// <param name="Content">    </param>
        /// <returns> Wrapped content </returns>
        public static string WrapParagraphByMaxLength(int MaxLength, string Content)
        {
            string[] words = Content.Split(' ');

            StringBuilder newSentence = new StringBuilder();

            string line = "";
            foreach (string word in words)
            {
                if ((line + word).Length > MaxLength)
                {
                    newSentence.AppendLine(line);
                    line = "";
                }

                line += string.Format("{0} ", word);
            }

            if (line.Length > 0)
                newSentence.AppendLine(line);

            return newSentence.ToString();
        }

        /// <summary>
        /// Break the words in a series of paragraphs and wrap words to a maximum width. This was made for logs.
        /// </summary>
        /// <param name="MaxLength">  </param>
        /// <param name="Paragraphs">    </param>
        /// <returns>  </returns>
        public static string WrapBookByMaxLength(int MaxLength, string Paragraphs)
        {
            string[] arrParagraphs = Paragraphs.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
            StringBuilder sb = new StringBuilder();
            foreach (string paragraph in arrParagraphs)
            {
                sb.Append(WrapParagraphByMaxLength(MaxLength, paragraph));
            }

            return sb.ToString();
        }

        /// <summary>
        /// Stream to String
        /// </summary>
        /// <param name="s"> .NET Stream </param>
        /// <returns> String value </returns>
        public static string StringFromStream(Stream s)
        {
            StreamReader reader = new StreamReader(s);
            string text = reader.ReadToEnd();
            return text;
        }

        /// <summary>
        /// Base64 string to regular string
        /// </summary>
        /// <param name="origString"> Encoded string </param>
        /// <returns> Base64 string to regular string </returns>
        public static string Base64ToString(string origString)
        {
            byte[] b = Convert.FromBase64String(origString);
            return Encoding.UTF8.GetString(b);
        }

        /// <summary>
        /// Byte Array to String
        /// </summary>
        /// <param name="sourceArray">  </param>
        /// <returns>  </returns>
        public static string ByteArrayToString(byte[] sourceArray)
        {
            return Encoding.Unicode.GetString(sourceArray);
        }

        /// <summary>
        /// Convert a date time value to a readable string
        /// </summary>
        /// <param name="value"> Date time value </param>
        /// <returns> Readable string </returns>
        public static string ToReadableTime(this DateTime value)
        {
            var ts = new TimeSpan(DateTime.UtcNow.Ticks - value.Ticks);
            double delta = ts.TotalSeconds;
            if (delta < 60)
            {
                return ts.Seconds == 1 ? "one second ago" : ts.Seconds + " seconds ago";
            }
            if (delta < 120)
            {
                return "a minute ago";
            }
            if (delta < 2700) // 45 * 60
            {
                return ts.Minutes + " minutes ago";
            }
            if (delta < 5400) // 90 * 60
            {
                return "an hour ago";
            }
            if (delta < 86400) // 24 * 60 * 60
            {
                return ts.Hours + " hours ago";
            }
            if (delta < 172800) // 48 * 60 * 60
            {
                return "yesterday";
            }
            if (delta < 2592000) // 30 * 24 * 60 * 60
            {
                return ts.Days + " days ago";
            }
            if (delta < 31104000) // 12 * 30 * 24 * 60 * 60
            {
                int months = Convert.ToInt32(Math.Floor((double)ts.Days / 30));
                return months <= 1 ? "one month ago" : months + " months ago";
            }
            var years = Convert.ToInt32(Math.Floor((double)ts.Days / 365));
            return years <= 1 ? "one year ago" : years + " years ago";
        }

        /// <summary>
        /// Convert COBOL / Amisys Date Format to
        /// </summary>
        /// <param name="dateString"> Date string </param>
        /// <returns> Formatted COBOL / Amisys date </returns>
        [SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Legacy code")]
        public static string DateFormatCOBOL(string dateString)
        {
            if (dateString.Length == 8)
            {
                return dateString.Substring(4, 2) + "/" + dateString.Substring(6, 2) + "/" + dateString.Substring(0, 4);
            }
            else
            {
                return dateString;
            }
        }

        /// <summary>
        /// Convert a number to textual representation of the file size given
        /// </summary>
        public static string ToFileSize(this long size)
        {
            if (size < 1024) { return (size).ToString("F0") + " bytes"; }
            if (size < Math.Pow(1024, 2)) { return (size / 1024).ToString("F0") + "KB"; }
            if (size < Math.Pow(1024, 3)) { return (size / Math.Pow(1024, 2)).ToString("F0") + "MB"; }
            if (size < Math.Pow(1024, 4)) { return (size / Math.Pow(1024, 3)).ToString("F0") + "GB"; }
            if (size < Math.Pow(1024, 5)) { return (size / Math.Pow(1024, 4)).ToString("F0") + "TB"; }
            if (size < Math.Pow(1024, 6)) { return (size / Math.Pow(1024, 5)).ToString("F0") + "PB"; }
            return (size / Math.Pow(1024, 6)).ToString("F0") + "EB";
        }

        /// <summary>
        /// Triple DES Encryption
        /// </summary>
        /// <param name="Key">  Text Key - Should be stored in web.config </param>
        /// <param name="Data"> Textual Data </param>
        /// <returns>  </returns>
        public static string TripleDESEncrypt(string Key, string Data)
        {
            TripleDESCryptoServiceProvider DES = new TripleDESCryptoServiceProvider();

            DES.Mode = CipherMode.ECB;
            DES.Key = Encoding.Unicode.GetBytes(Key);

            DES.Padding = PaddingMode.PKCS7;
            ICryptoTransform DESEncrypt = DES.CreateEncryptor();
            Byte[] Buffer = ASCIIEncoding.ASCII.GetBytes(Data);

            return Convert.ToBase64String(DESEncrypt.TransformFinalBlock(Buffer, 0, Buffer.Length));
        }

        /// <summary>
        /// Triple DES Decryption
        /// </summary>
        /// <param name="Key">   </param>
        /// <param name="Data">  </param>
        /// <returns>  </returns>
        public static string TripleDESDecrypt(string Key, string Data)
        {
            TripleDESCryptoServiceProvider DES = new TripleDESCryptoServiceProvider();

            DES.Mode = CipherMode.ECB;
            DES.Key = Encoding.Unicode.GetBytes(Key);

            DES.Padding = PaddingMode.PKCS7;
            ICryptoTransform DESEncrypt = DES.CreateDecryptor();
            byte[] Buffer = Convert.FromBase64String(Data.Replace(" ", "+"));

            return Encoding.UTF8.GetString(DESEncrypt.TransformFinalBlock(Buffer, 0, Buffer.Length));
        }

        /// <summary>
        /// Encrypting a string with AES
        /// </summary>
        /// <param name="inText">  </param>
        /// <param name="key">     </param>
        /// <returns>  </returns>
        public static string AesEncrypt(string inText, string key)
        {
            byte[] bytesBuff = Encoding.Unicode.GetBytes(inText);
            using (Aes aes = Aes.Create())
            {
                Rfc2898DeriveBytes crypto = new Rfc2898DeriveBytes(key, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                aes.Key = crypto.GetBytes(32);
                aes.IV = crypto.GetBytes(16);
                using (MemoryStream mStream = new MemoryStream())
                {
                    using (CryptoStream cStream = new CryptoStream(mStream, aes.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cStream.Write(bytesBuff, 0, bytesBuff.Length);
                        cStream.Close();
                    }
                    inText = Convert.ToBase64String(mStream.ToArray());
                }
            }
            return inText;
        }

        /// <summary>
        /// Decrypting a string
        /// </summary>
        /// <param name="cryptTxt">  </param>
        /// <param name="key">       </param>
        /// <returns>  </returns>
        public static string AesDecrypt(string cryptTxt, string key)
        {
            cryptTxt = cryptTxt.Replace(" ", "+");
            byte[] bytesBuff = Convert.FromBase64String(cryptTxt);
            using (Aes aes = Aes.Create())
            {
                Rfc2898DeriveBytes crypto = new Rfc2898DeriveBytes(key, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                aes.Key = crypto.GetBytes(32);
                aes.IV = crypto.GetBytes(16);
                using (MemoryStream mStream = new MemoryStream())
                {
                    using (CryptoStream cStream = new CryptoStream(mStream, aes.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cStream.Write(bytesBuff, 0, bytesBuff.Length);
                        cStream.Close();
                    }
                    cryptTxt = Encoding.Unicode.GetString(mStream.ToArray());
                }
            }
            return cryptTxt;
        }

        /// <summary>
        /// Add a suffix to a day
        /// </summary>
        /// <param name="datetime">  </param>
        /// <returns>  </returns>
        public static string OrdinalSuffix(DateTime datetime)
        {
            int day = datetime.Day;
            if (day % 100 >= 11 && day % 100 <= 13)
                return string.Concat(day, "th");
            switch (day % 10)
            {
                case 1:
                    return string.Concat(day, "st");

                case 2:
                    return string.Concat(day, "nd");

                case 3:
                    return string.Concat(day, "rd");

                default:
                    return string.Concat(day, "th");
            }
        }

        /// <summary>
        /// Compute the distance between two strings.
        /// </summary>
        /// <remark>Stolen from https://www.dotnetperls.com/levenshtein</remark>
        public static int LevenshteinDistance(this string self, string other)
        {
            int n = self.Length;
            int m = other.Length;
            int[,] d = new int[n + 1, m + 1];

            // Step 1
            if (n == 0)
            {
                return m;
            }

            if (m == 0)
            {
                return n;
            }

            // Step 2
            for (int i = 0; i <= n; d[i, 0] = i++)
            {
            }

            for (int j = 0; j <= m; d[0, j] = j++)
            {
            }

            // Step 3
            for (int i = 1; i <= n; i++)
            {
                //Step 4
                for (int j = 1; j <= m; j++)
                {
                    // Step 5
                    int cost = (other[j - 1] == self[i - 1]) ? 0 : 1;

                    // Step 6
                    d[i, j] = Math.Min(
                        Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1),
                        d[i - 1, j - 1] + cost);
                }
            }
            // Step 7
            return d[n, m];
        }
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.StaticUtilities.Attributes
{
    /// <summary>
    /// Convert an Enum to/from a string representation 
    /// </summary>
    /// <seealso cref="EnumConverter"/>
    [AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    sealed class EnumConverterAttribute : Attribute
    {
        public EnumConverterAttribute() : this(null) { }
        public EnumConverterAttribute(string value) : this(value, false) { }
        public EnumConverterAttribute(string value, bool isDefault)
        {
            Value = value;
            IsDefault = isDefault;
        }

        /// <summary>
        /// A string representation for the enum 
        /// </summary>
        public string Value { get; }

        /// <summary>
        /// Fall back value when converting from string
        /// </summary>
        /// <remarks>
        /// If a string value can't be found, an enum for this value
        /// </remarks>
        public bool IsDefault { get; }
    }
}

using DataLayer.StaticUtilities.Attributes;
using DataLayer.StaticUtilities.Extensions;
using System;
using System.Reflection;

namespace DataLayer.StaticUtilities
{
    /// <summary>
    /// Enumeration converter changes enumerations to/from string representations based on predefined mapping from `EnumConverterAttribute`.
    /// </summary>
    /// <seealso cref="EnumConverterAttribute"/>
    public static class EnumConverter
    {
        /// <summary>
        /// Convert enumeration value into the string representation
        /// </summary>
        /// <param name="value">Enumeration value to convert</param>
        /// <returns></returns>
        public static string ToValue(this Enum value)
        {
            if (value == null)
            {
                throw new ArgumentNullException(nameof(value), "Enum is null");
            }

            FieldInfo field = value.GetType().GetField(value.ToString());
            EnumConverterAttribute attr = (EnumConverterAttribute)Attribute.GetCustomAttribute(field, typeof(EnumConverterAttribute));
            if (attr == null)
            {
                return value.ToString();
            }
            else
            {
                return attr.Value;
            }
        }

        /// <summary>
        /// Convert string representation to enumeration value
        /// </summary>
        /// <remarks>
        /// Search for the string in the Enum Name or the attribute value. Also tracks the `IsDefault` property.
        /// If no string can be found, it will either:
        /// * Return the enum marked `IsDefault`
        /// * throw an `InvalidOperationException`
        /// </remarks>
        /// <param name="value">String value to convert</param>
        /// <returns></returns>
        public static T ToEnum<T>(string value) where T : Enum
        {
            value.ThrowIfNullOrEmpty(nameof(value));

            T defaultValue = default;
            bool hasDefault = false;
            value = value.Trim();
            foreach (var field in typeof(T).GetFields())
            {
                EnumConverterAttribute attr = (EnumConverterAttribute)Attribute.GetCustomAttribute(field, typeof(EnumConverterAttribute));
                if (attr == null)
                {
                    if (value.ToLower() == field.Name.ToLower())
                    {
                        // equal to enum field name exactly
                        return (T)Enum.Parse(typeof(T), field.Name, true);
                    }
                    //continue loop
                }
                else
                {
                    if (value.ToLower() == attr.Value.ToLower())
                    {
                        // equal to attr name
                        return (T)Enum.Parse(typeof(T), field.Name, true);
                    }
                    else if (attr.IsDefault)
                    {
                        hasDefault = true;
                        defaultValue = (T)Enum.Parse(typeof(T), field.Name, true);
                    }
                    //continue loop
                }
            }
            if (hasDefault)
            {
                return defaultValue;
            }
            else
            {
                throw new InvalidOperationException($"Can't convert '{value}' to {typeof(T).Name} and there is no default attribute set");
            }
        }
    }
}

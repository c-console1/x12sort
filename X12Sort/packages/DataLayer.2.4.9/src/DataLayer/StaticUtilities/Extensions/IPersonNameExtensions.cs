using System.Linq;
using DataLayer.Base.Models;

namespace DataLayer.StaticUtilities.Extensions
{
    /// <summary>
    /// Name formating style
    /// </summary>
    public enum NameFormat
    {
        /// <summary>
        /// Doe, John Thomas
        /// </summary>
        LastFirstMiddle,

        /// <summary>
        /// John Thomas Doe
        /// </summary>
        FirstMiddleLast,

        /// <summary>
        /// Mr. John Thomas Doe III.
        /// </summary>
        Full,
    }

    /// <summary>
    /// Collection of static extensions for `IPersonName`
    /// </summary>
    /// <seealso cref="IPersonName"/>
    public static class IPersonNameExtensions
    {
        /// <summary>
        /// Determine whether name has any prefixes
        /// </summary>
        /// <param name="name">The `IPersonName` being reviewed</param>
        /// <returns>True = name has at least one prefix</returns>
        public static bool HasPrefix(this IPersonName name)
        {
            return name.Prefixes == default ? false : name.Prefixes.Count() > 0;
        }

        /// <summary>
        /// Determine whether name has any suffixes
        /// </summary>
        /// <param name="name">The `IPersonName` being reviewed</param>
        /// <returns>True = name has at least one prefix</returns>
        public static bool HasSuffix(this IPersonName name)
        {
            return name.Suffixes == default ? false : name.Suffixes.Count() > 0;
        }

        /// <summary>
        /// Format list of prefixes on name
        /// </summary>
        /// <param name="name">The `IPersonName` being formated</param>
        /// <returns>Formatted list of prefixes</returns>
        public static string FormatPrefixes(this IPersonName name)
        {
            if (!name.HasPrefix()) return "";
            return string.Join(", ", name.Prefixes);
        }

        /// <summary>
        /// Format list of suffixes on name
        /// </summary>
        /// <param name="name">The `IPersonName` being formated</param>
        /// <returns>Formatted list of suffixes</returns>
        public static string FormatSuffixes(this IPersonName name)
        {
            if (!name.HasSuffix()) return "";
            return string.Join(", ", name.Suffixes);
        }

        /// <summary>
        /// Get a person's middle initial
        /// </summary>
        /// <param name="name">The person name object</param>
        /// <returns>First character of name uppercased</returns>
        public static string MiddleInital(this IPersonName name)
        {
            return string.IsNullOrEmpty(name.MiddleName) ? "" : (name.MiddleName[0]).ToString().ToUpper();
        }

        /// <summary>
        /// Format `IPersonName`'s name
        /// </summary>
        /// <param name="name">The `IPersonName` being formated</param>
        /// <param name="format">The desired name format</param>
        /// <returns>String with formatted name</returns>
        public static string FormatName(this IPersonName name, NameFormat format)
        {
            name.ThrowIfNull(nameof(name));

            string formattedName = "";
            switch (format)
            {
                case NameFormat.FirstMiddleLast:
                    if (!string.IsNullOrEmpty(name.MiddleName))
                        formattedName = $"{name.FirstName} {name.MiddleName} {name.LastName}";
                    else
                        formattedName = $"{name.FirstName} {name.LastName}";
                    break;

                case NameFormat.LastFirstMiddle:
                    formattedName = $"{name.LastName}, {name.FirstName}";
                    if (!string.IsNullOrEmpty(name.MiddleName))
                        formattedName += $" {name.MiddleName}";
                    break;

                case NameFormat.Full:
                    if (!string.IsNullOrEmpty(name.MiddleName))
                        formattedName = $"{name.FirstName} {name.MiddleName} {name.LastName}";
                    else
                        formattedName = $"{name.FirstName} {name.LastName}";

                    if (name.HasSuffix())
                        formattedName += $" {name.FormatSuffixes()}";

                    if (name.HasPrefix())
                        formattedName = $"{name.FormatPrefixes()} " + formattedName;
                    break;

                default:
                    break;
            }
            return formattedName.Trim();
        }
    }
}

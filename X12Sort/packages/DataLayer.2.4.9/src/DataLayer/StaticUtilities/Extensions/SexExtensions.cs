using System;
using System.Collections.Generic;
using System.Text;
using DataLayer.Base.Enumerations;

namespace DataLayer.StaticUtilities.Extensions
{
    public static class SexExtensions
    {
        public static string GetTitle(this Sex sex)
        {
            return sex.ToString("F");
        }

        public static string GetLabel(this Sex sex)
        {
            return EnumConverter.ToValue(sex);
        }

        public static int GetInt(this Sex sex)
        {
            return (int)sex;
        }
    }
}

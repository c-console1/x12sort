using DataLayer.Base.Models;
using Newtonsoft.Json;
using NLog;
using System;

namespace DataLayer.StaticUtilities.Extensions
{
    /// <summary>
    /// Extension methods for the IBaseDataObject interface
    /// </summary>
    public static class IBaseDataObjectExtensions
    {
        private static ILogger Logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Converts this object to JSON string.
        /// </summary>
        /// <param name="self">The object to convert to JSON</param>
        /// <param name="formatting">Indicates how the output should be formatted.</param>
        /// <param name="settings">The Newtonsoft.Json.JsonSerializerSettings used to serialize the object. If this is null, default serialization settings will be used.</param>
        public static string ToJsonString(this IBaseDataObject self, Formatting formatting = Formatting.None, JsonSerializerSettings settings = null)
        {
            self.ThrowIfNull(nameof(self));
            try
            {
                string json = JsonConvert.SerializeObject(self, formatting, settings);
                return json;
            }
            catch (NullReferenceException e)
            {
                Logger.Warn("Failed to convert object to JSON, Error: " + e.Message);
                return null;
            }
        }
    }
}

using DataLayer.Base.Models;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace DataLayer.StaticUtilities.Extensions
{
    /// <summary>
    /// Extensions related to BaseDataObject
    /// </summary>
    public static class BaseDataObjectExtensions
    {
        /// <summary>
        /// Serialize current object into an XML string
        /// </summary>
        /// <param name="self">Current object being serialized</param>
        /// <returns>XML string representation of object</returns>
        public static string ToXML(this BaseDataObject self)
        {
            // Create serializer
            XmlSerializer xmlSerializer = new XmlSerializer(self.GetType());
            string xml = string.Empty;

            // Serialize object
            using (StringWriter sWriter = new StringWriter())
            {
                using (XmlWriter xWriter = XmlWriter.Create(sWriter))
                {
                    xmlSerializer.Serialize(xWriter, self);
                    xml = sWriter.ToString();
                }
            }

            return xml;
        }
    }
}

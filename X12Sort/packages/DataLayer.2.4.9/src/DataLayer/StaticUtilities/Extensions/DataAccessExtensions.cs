using DataLayer.Configuration.Models;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DataLayer.StaticUtilities.Extensions
{
    public static class DataAccessExtensions
    {
        private static ILogger _logger = LogManager.GetCurrentClassLogger();

        #region GetVar

        /// <summary>
        /// Helper function to get a configuration variable and convert from JSON
        /// </summary>
        /// <typeparam name="T">The type of object you variable represents</typeparam>
        /// <param name="dataAccess">The dataAccess object has a configuration loaded</param>
        /// <param name="name">Variable name</param>
        /// <returns></returns>
        public static T GetVar<T>(this DataAccess dataAccess, string name) where T : class
        {
            string value = dataAccess.GetVar(name);
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }
            else
            {
                T objValue = JsonConvert.DeserializeObject<T>(value);
                return objValue;
            }
        }


        /// <summary>
        /// Helper function to get a configuration variable and converts it to a integer
        /// </summary>
        /// <param name="dataAccess">The dataAccess object has a configuration loaded</param>
        /// <param name="name">Variable name</param>
        /// <returns></returns>
        public static int? GetVarInt(this DataAccess dataAccess, string name)
        {
            string value = dataAccess.GetVar(name);
            if (int.TryParse(value, out int intValue))
            {
                return intValue;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Helper function to get a configuration variable and converts it to a boolean
        /// </summary>
        /// <param name="dataAccess">The dataAccess object has a configuration loaded</param>
        /// <param name="name">Variable name</param>
        /// <returns></returns>
        public static bool? GetVarBool(this DataAccess dataAccess, string name)
        {
            string value = dataAccess.GetVar(name);
            if (bool.TryParse(value, out bool boolValue))
            {
                return boolValue;
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// Helper function to get a configuration variable
        /// </summary>
        /// <param name="dataAccess">The dataAccess object has a configuration loaded</param>
        /// <param name="name">Variable name</param>
        /// <returns></returns>
        public static string GetVar(this DataAccess dataAccess, string name)
        {
            dataAccess.ThrowIfNull(nameof(dataAccess));
            name.ThrowIfNullOrEmpty(nameof(name));
            if (dataAccess.Configuration.Current == null)
            {
                throw new InvalidOperationException("DataAccess configuration is not loaded! Please call LoadConfiguration()");
            }

            ConfigAppVar variable = dataAccess.Configuration.Current.Variables.FirstOrDefault(q => q.Name == name);
            return variable?.Value;
        }

        #endregion

        #region LoadApplication
        /// <summary>
        /// Helper function to get a configuration variable
        /// </summary>
        /// <param name="dataAccess">The dataAccess object has a configuration loaded</param>
        /// <param name="name">Variable name</param>
        /// <returns></returns>
        public static void LoadApplication(this DataAccess dataAccess, bool logInstance = true)
        {
            string machine = Environment.MachineName;
            string applicationName = Assembly.GetEntryAssembly().GetName().Name;
            _logger.Info($"Loading machine:'{machine}' and application:'{applicationName}' from environment and assembly name");
            dataAccess.LoadApplication(machine, applicationName, logInstance);
        }


        #endregion
    }
}

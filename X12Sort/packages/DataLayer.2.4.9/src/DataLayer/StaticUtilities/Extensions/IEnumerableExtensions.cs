using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.StaticUtilities.Extensions
{
    public static class IEnumerableExtensions
    {
        /// <summary>
        /// Converts a tree of items into a flat of all nodes
        /// </summary>
        /// <param name="root">The starting object to flatten</param>
        /// <remarks>Flatten objects starting with the given object and then the children, operating recursively </remarks>
        /// <returns></returns>
        public static IEnumerable<T> Flatten<T>(this T root, Func<T, IEnumerable<T>> childrenSelector) where T : class
        {
            root.ThrowIfNull(nameof(root));
            childrenSelector.ThrowIfNull(nameof(childrenSelector));

            yield return root;
            foreach (T flatItem in childrenSelector(root).Flatten(childrenSelector))
            {
                yield return flatItem;

            }
        }


        /// <summary>
        /// Converts a enumeration of trees into a flat list of all nodes
        /// </summary>
        /// <param name="root">The starting object to flatten</param>
        /// <remarks>Flatten objects starting with the given object and then the children, operating recursively </remarks>
        /// <returns></returns>
        public static IEnumerable<T> Flatten<T>(this IEnumerable<T> root, Func<T, IEnumerable<T>> childrenSelector) where T : class
        {
            root.ThrowIfNull(nameof(root));
            childrenSelector.ThrowIfNull(nameof(childrenSelector));

            foreach (T item in root)
            {
                foreach (T flatItem in item.Flatten(childrenSelector))
                {
                    yield return flatItem;
                }
            }
        }
    }
}

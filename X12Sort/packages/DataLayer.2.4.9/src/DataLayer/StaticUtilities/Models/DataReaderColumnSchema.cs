using System;

namespace DataLayer.StaticUtilities.Models
{
    /// <summary>
    /// A class that repersents a data reader column's schemas
    /// </summary>
    /// <remarks>
    /// Values takesn from here: https://docs.microsoft.com/en-us/dotnet/api/system.data.datatablereader.getschematable?view=netframework-4.8
    /// </remarks>
    public class DataReaderColumnSchema : IEquatable<DataReaderColumnSchema>
    {
        /// <summary>
        /// The name of the column as it appears in the DataTable.
        /// </summary>
        public string ColumnName { get; set; }

        /// <summary>
        ///  The ordinal of the column
        /// </summary>
        public int ColumnOrdinal { get; set; }

        /// <summary>
        /// -1 if the ColumnSize (or MaxLength) property of the DataColumn cannot be determined or is not relevant;
        /// otherwise, 0 or a positive integer that contains the MaxLength value.
        /// </summary>
        public int ColumnSize { get; set; }

        /// <summary>
        /// The underlying type of the column.
        /// </summary>
        public Type DataType { get; set; }

        /// <summary>
        /// true if the AllowDbNull constraint is set to true for the column; otherwise, false.
        /// </summary>
        public bool AllowDBNull { get; set; }

        /// <summary>
        /// Pretty printing of the object
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string nullStr = AllowDBNull ? "'NULL'" : "";
            return $"{ColumnName} [{DataType}({ColumnSize})] {nullStr }";
        }

        /// <summary>
        /// Compares two Schema obeject to see if they are the same
        /// </summary>
        /// <param name="other">The other schema object</param>
        /// <returns></returns>
        public bool Equals(DataReaderColumnSchema other)
        {
            // Make some nice output to show the column mappings, Using 30 chars because column names can be that long

            bool valid = true;
            // validate the columns schemas
            if (DataType != other.DataType)
            {
                valid = false;
            }
            if ((ColumnSize > other.ColumnSize) && other.DataType.Name.ToLower().Contains("date"))
            {
                valid = false;
            }
            return valid;
        }
    }
}

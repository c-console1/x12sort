using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Dapper;

namespace DataLayer.HealthEdgePIK
{
    public class HealthEdgePIKDataBaseSettings
    {
        private readonly IDbConnection _appConfigConnection;

        public HealthEdgePIKDataBaseSettings()
        {
            _appConfigConnection = new SqlConnection(DataAccessConfigBuilder.Instance.Config.AppConfigConnectionString);
        }

        public string GetKey(int PIKSecurityKeyID)
        {
            string SQLQuery = @"
            EXECUTE AS USER = 'intrabit'
            SELECT [das].fn_GetPIKSecurityKey(@PIKSecurityKeyID);
            REVERT;
            ";

            _appConfigConnection.Open();
            string key = _appConfigConnection.QueryFirstOrDefault<string>(SQLQuery, new { PIKSecurityKeyID });
            _appConfigConnection.Close();

            return key;
        }

        public void SetKey(int PIKSecurityKeyID, string key, string name)
        {
            _appConfigConnection.Open();
            _appConfigConnection.QueryFirstOrDefault<string>("[das].sp_SetPIKSecurityKey", new { KeyID = PIKSecurityKeyID, Key = key, Name = name }, commandType: CommandType.StoredProcedure);
            _appConfigConnection.Close();
        }

        public DateTime GetKeyLastUpdatedDate(int PIKSecurityKeyID)
        {
            string SQLQuery = @"
            SELECT CASE 
                    WHEN UpdatedOn IS NULL
                        THEN CreatedOn
                    ELSE UpdatedON
                    END AS LastUpdatedDate
            FROM [das].[PIKSecurityKeys]
            WHERE PIKSecurityKeyID = @PIKSecurityKeyID;
            ";

            _appConfigConnection.Open();
            DateTime lastUpdatedDate = _appConfigConnection.QueryFirstOrDefault<DateTime>(SQLQuery, new { PIKSecurityKeyID });
            _appConfigConnection.Close();

            return lastUpdatedDate;
        }
    }
}

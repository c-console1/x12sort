using DataLayer.Base;
using DataLayer.HealthEdgePIK.Models;
using Flurl;
using Flurl.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;

namespace DataLayer.HealthEdgePIK
{
    public class HealthEdgePIKRepository : BaseRepository<HealthEdgePIKDatabase>
    {
        private readonly string _apiKey;
        private readonly string _baseURL;
        private HealthEdgePIKDataBaseSettings _pikDataBaseSettings;
        private readonly HealthEdgePIKCrypto _cryptoFactory;

    internal HealthEdgePIKRepository(HealthEdgePIKDatabase database) : base(database)
    {
        _pikDataBaseSettings = new HealthEdgePIKDataBaseSettings();
        _cryptoFactory = new HealthEdgePIKCrypto(_pikDataBaseSettings);
        _apiKey = _pikDataBaseSettings.GetKey((int)HealthEdgePIKSecurityKeyEnum.APIKey);
        _baseURL = DataAccessConfigBuilder.Instance.Config.HealthEdgePIKBaseRESTURL;
    }

    public string TokenCreation()
        {
            HealthEdgePIKTokenBody tokenBody = new HealthEdgePIKTokenBody
            {
                lifeTime = DataAccessConfigBuilder.Instance.Config.HealthEdgePIKTokenLifeDays,
                apiKey = _apiKey
            };

            string[] segments = new string[]
            {
                "auth",
                "token",
                "create"
            };

            return _baseURL.AppendPathSegments(segments).PostJsonAsync(tokenBody).ReceiveString().Result;
        }

        public string TokenRefresh()
        {

            HealthEdgePIKTokenBody tokenBody = new HealthEdgePIKTokenBody
            {
                lifeTime = DataAccessConfigBuilder.Instance.Config.HealthEdgePIKTokenLifeDays,
                apiKey = _apiKey
            };

            string[] segments = new string[]
            {
                "auth",
                "token",
                "refresh"
            };

            string token = _baseURL.AppendPathSegments(segments).PostJsonAsync(tokenBody).ReceiveString().Result;

            _pikDataBaseSettings.SetKey((int)HealthEdgePIKSecurityKeyEnum.Token, token, "Token");

            return token;
        }

        public HealthEdgePIKEligibilityEnquiry EligibilityEnquiry(string memberID)
        {
            string token = GetToken();
            string[] segments = new string[]
            {
                "rti",
                "fetch",
                "eligibility"
            };

            HttpResponseMessage httpResponseMessage = _baseURL.AppendPathSegments(segments).SetQueryParam("memberId", memberID).WithHeader("Authorization", token).GetAsync().Result;

            if (httpResponseMessage.Content.ReadAsStringAsync().Result == "Authentication Failed - Invalid Token.")
            {
                TokenRefresh();
                httpResponseMessage = _baseURL.AppendPathSegments(segments).SetQueryParam("memberId", memberID).WithHeader("Authorization", token).GetAsync().Result;
            }

            string signature = httpResponseMessage.Headers.GetValues("X-Healthedge-Signature").FirstOrDefault();

            string DecryptedResponse = _cryptoFactory.DecryptResponse(httpResponseMessage.Content.ReadAsStringAsync().Result, signature);

            HealthEdgePIKEligibilityEnquiry eligibilityEnquiry = JsonConvert.DeserializeObject<HealthEdgePIKEligibilityEnquiry>(DecryptedResponse);

            return eligibilityEnquiry;
        }

        private string GetToken()
        {
            string token;

            if (_pikDataBaseSettings.GetKey((int)HealthEdgePIKSecurityKeyEnum.Token) == null)
            {
                token = TokenCreation();

                if (token == "Token already created for the customer PARAMOUNT with app Paramount Data Layer")
                {
                    token = TokenRefresh();
                }
                else
                {
                    _pikDataBaseSettings.SetKey((int)HealthEdgePIKSecurityKeyEnum.Token, token, "Token");
                }
            }
            else if (_pikDataBaseSettings.GetKeyLastUpdatedDate((int)HealthEdgePIKSecurityKeyEnum.Token) < DateTime.Now.AddDays(-DataAccessConfigBuilder.Instance.Config.HealthEdgePIKTokenLifeDays))
            {
                token = TokenRefresh();
            }
            else
            {
                token = _pikDataBaseSettings.GetKey((int)HealthEdgePIKSecurityKeyEnum.Token);
            }

            if (token == null)
            {
                throw new Exception("PIK Token Invalid");
            }

            return token;
        }
    }
}

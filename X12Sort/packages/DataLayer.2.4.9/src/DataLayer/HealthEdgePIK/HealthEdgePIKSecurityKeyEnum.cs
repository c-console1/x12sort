namespace DataLayer.HealthEdgePIK
{
    public enum HealthEdgePIKSecurityKeyEnum
    {
        APIKey = 1,
        PublicKey = 2,
        SignatureKey = 3,
        Token = 4
    }
}

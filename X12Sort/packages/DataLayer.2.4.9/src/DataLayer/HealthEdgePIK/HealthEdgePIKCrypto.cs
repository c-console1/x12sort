using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace DataLayer.HealthEdgePIK
{
    public class HealthEdgePIKCrypto
    {
        private byte[] Key;
        private HealthEdgePIKDataBaseSettings _pikDataBaseSettings;

        public HealthEdgePIKCrypto(HealthEdgePIKDataBaseSettings pikDataBaseSettings)
        {
            _pikDataBaseSettings = pikDataBaseSettings;
            Key = HexStringToByteArray(_pikDataBaseSettings.GetKey((int)HealthEdgePIKSecurityKeyEnum.PublicKey));
        }

        public string DecryptResponse(string EncryptedResponse, string signature)
        {
            byte[] responsedecoded = Convert.FromBase64String(EncryptedResponse);

            byte[] IV = new byte[16];
            Array.Copy(responsedecoded, 0, IV, 0, IV.Length);
            byte[] cipherText = new byte[responsedecoded.Length - 16];
            Array.Copy(responsedecoded, 16, cipherText, 0, cipherText.Length);

            using AesManaged aesAlg = new AesManaged
            {
                KeySize = 256,
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7,
                Key = Key,
                IV = IV
            };
            
            ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
            
            using MemoryStream msDecrypt = new MemoryStream(cipherText);
            using CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read);
            using StreamReader srDecrypt = new StreamReader(csDecrypt);
            
            string plaintext = srDecrypt.ReadToEnd();


            if (!VerifySignature(plaintext, signature))
            {
                Exception exception = new Exception("RESPONSE SIGNATURE IS NOT VALID!!!");
                throw exception;
            }

            return plaintext;
        }

        public string EncryptPayload(string payload)
        {
            byte[] payloadBase64 = Encoding.UTF8.GetBytes(payload);
            byte[] payloadEncrypted;

            byte[] IV = new byte[16];
            Array.Copy(payloadBase64, 0, IV, 0, IV.Length);

            AesManaged aesManaged = new AesManaged
            {
                KeySize = 256,
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7,
                Key = Key,
                IV = IV
            };

            using (ICryptoTransform cryptoTransform = aesManaged.CreateEncryptor())
            {
                payloadEncrypted = cryptoTransform.TransformFinalBlock(payloadBase64, 0, payloadBase64.Length);
            }

            return Convert.ToBase64String(payloadEncrypted);
        }

        private byte[] HexStringToByteArray(string stringToConvert)
        {
            int len = stringToConvert.Length;
            byte[] data = new byte[len / 2];

            for (int i = 0; i < len; i += 2)
            {
                data[i / 2] = Convert.ToByte(stringToConvert.Substring(i, 2), 16);
            }

            return data;
        }

        private bool VerifySignature(string plainTextResult, string signature)
        {
            HMACSHA256 HMACSHA256 = new HMACSHA256();

            byte[] Key = HexStringToByteArray(_pikDataBaseSettings.GetKey((int)HealthEdgePIKSecurityKeyEnum.SignatureKey));
            byte[] bytesResult = Encoding.UTF8.GetBytes(plainTextResult);

            HMACSHA256.Key = Key;
            byte[] hash = HMACSHA256.ComputeHash(bytesResult);

            string signatureFromResult = Convert.ToBase64String(hash);

            if (signatureFromResult == signature)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

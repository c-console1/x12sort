using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.HealthEdgePIK.Models
{
    public class HealthEdgePIKTokenBody
    {
        public int lifeTime { get; set; }
        public string apiKey { get; set; }
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    /// <summary>
    /// Base interface for all executors
    /// </summary>
    /// <remarks>
    /// This interface is created to make it easier to pass a list of executors to the various logical databases. This is done inside the `DataAccess` object automatically with reflection
    /// </remarks>
    /// <seealso cref="IDbExec"/>
    /// <seealso cref="IApiExec"/>
    /// <seealso cref="ApiExec"/>
    /// <seealso cref="DbExec"/>
    public interface IExecutor: IDisposable
    {
        /// <summary>
        /// The name of the executor
        /// </summary>
        string Name { get; }

        /// <summary>
        /// The logical database that this connection string belongs
        /// </summary>
        string DatabaseGroup { get; }
    }
}

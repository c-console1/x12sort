using System;
using System.Collections.Generic;
using System.Text;

// Need to mark the DataLayer.Tests assembly as friendly so we can test internal methods and properties
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("Datalayer.Tests")]

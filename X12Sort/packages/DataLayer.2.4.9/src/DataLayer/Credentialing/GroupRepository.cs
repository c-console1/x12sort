using DataLayer.Base;
using DataLayer.Credentialing.Models;
using System.Collections.Generic;

namespace DataLayer.Credentialing
{
    public class GroupRepository : BaseRepository<CredentialingDatabase>
    {
        internal GroupRepository(CredentialingDatabase database) : base(database)
        {

        }

        /// <summary>
        /// Gets a provider group by name
        /// </summary>
        /// <param name="GroupName">Group name to search for</param>
        /// <returns></returns>
        public Group GetGroupByName(string GroupName)
        {
            string sql = @"select g.group_k as DatabaseKey, rtrim(g.groupname) as Name, g.npi, g.taxid
                from visualcactus.groups g
                where 1 = 1
                and g.entity_k in('      para', '       all')
                and g.active = 1
                and g.groupname = @name";

            Group results = Database.QueryFirstOrDefault<Group>(sql, new { name = GroupName });
            return results;
        }

        /// <summary>
        /// Gets all groups for a given provider key
        /// </summary>
        /// <param name="providerKey">Primary provider key to get groups for</param>
        /// <returns></returns>
        public IEnumerable<Group> GetProviderGroups(string providerKey)
        {
            string sql = @"select distinct g.group_k as DatabaseKey, rtrim(g.groupname) as Name, g.npi, g.taxid
                from visualcactus.provideraddresses pa
                join visualcactus.groupsprovidersaddresses gpa on pa.provideraddress_k = gpa.provideraddress_k
                join visualcactus.groupaddress ga on gpa.groupaddress_k = ga.groupaddress_k
                join visualcactus.groups g on ga.group_k = g.group_k
                where 1=1
                and pa.entity_k in('      para','       all')
                and ga.entity_k in('      para','       all')
                and g.entity_k in('      para','       all')
                and pa.active = 1
                and ga.active = 1
                and g.active = 1
                and pa.provider_k = @providerKey";

            IEnumerable<Group> results = Database.Query<Group>(sql, new { providerKey = providerKey });
            return results;
        }

        /// <summary>
        /// Gets all addresses for a given group key
        /// </summary>
        /// <param name="groupKey">Primary group key to get addresses for</param>
        /// <returns></returns>
        public IEnumerable<Address> GetGroupAddresses(string groupKey)
        {
            string sql = @"select ga.groupaddress_k as DatabaseKey, rtrim(rt.description) as Type,
				rtrim(a.addressline1) as address1, rtrim(a.addressline2) as address2, rtrim(a.city) as city, rtrim(a.state) as state, a.zipcode, a.phone, a.fax, a.taxid,
                a.latitude, a.longitude
				from visualcactus.groupaddress ga
				join visualcactus.reftable rt on ga.addresstype_rtk = rt.reftable_k
				join visualcactus.addresses a on ga.address_k=a.address_k
				where 1 = 1
				and ga.entity_k in ('      para', '       all')
				and ga.active = 1
				and ga.group_k = @groupKey";

            IEnumerable<Address> results = Database.Query<Address>(sql, new { groupKey = groupKey });
            return results;
        }

    }
}

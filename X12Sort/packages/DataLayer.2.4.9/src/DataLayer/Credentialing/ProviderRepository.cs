using DataLayer.Base;
using DataLayer.Credentialing.Models;
using System.Collections.Generic;

namespace DataLayer.Credentialing
{
    public class ProviderRepository : BaseRepository<CredentialingDatabase>
    {
        internal ProviderRepository(CredentialingDatabase database) : base(database)
        {

        }

        #region provider

        /// <summary>
        /// Gets a provider from the provider database
        /// </summary>
        /// <param name="id">Provider id number</param>
        /// <returns></returns>
        public Provider GetProvider(string id)
        {
            return GetProvider(id, null, null, null);
        }

        /// <summary>
        /// Gets a provider from the provider database
        /// </summary>
        /// <param name="firstName">Provider first name</param>
        /// <param name="lastName">Provider last name</param>
        /// <param name="npi">Provider NPI number</param>
        /// <returns></returns>
        public Provider GetProvider(string firstName, string lastName, string npi)
        {
            return GetProvider(null, firstName, lastName, npi);
        }

        /// <summary>
        /// Gets a provider from the provider database
        /// </summary>
        /// <param name="id">Provider id number</param>
		/// <param name="firstName">Provider first name</param>
		/// <param name="lastName">Provider last name</param>
		/// <param name="npi">Provider NPI number</param>
        /// <returns></returns>
		private Provider GetProvider(string id, string firstName, string lastName, string npi)
        {
            Provider results = new Provider();
            string sql = @"select p.provider_k as DatabaseKey, rtrim(p.firstname) as firstname, rtrim(p.middlename) as middlename, rtrim(p.lastname) as lastnamne,
                rtrim(p.suffix) as suffix, p.dateofbirth as BirthDate, p.ssn, p.upin, p.npi,
                case when p.individualinstitutionflag = 1 then 'true'
                    else 'false'
                    end as isindividual
                from visualcactus.providers p
                where 1=1";

            if (id != null)
            {
                sql += " and p.id = @id";

                results = Database.QueryFirstOrDefault<Provider>(sql, new { id = id });
            }
            else if (firstName != null && lastName != null && npi != null)
            {
                sql += " rtrim(p.firstname) = @firstName and rtrim(p.lastname) = @lastName and rtrim(p.npi) = @npi";
                results = Database.QueryFirstOrDefault<Provider>(sql, new { firstname = firstName, lastname = lastName, npi = npi });
            }


            return results;
        }

        /// <summary>
        /// Gets all providers for a group
        /// </summary>
        /// <param name="groupKey">Group primary key</param>
        /// <returns></returns>
        public IEnumerable<Provider> GetGroupProviders(string groupKey)
        {
            string sql = @"select distinct p.provider_k as DatabaseKey, rtrim(p.firstname) as firstname, rtrim(p.middlename) as middlename, rtrim(p.lastname) as lastnamne,
                rtrim(p.suffix) as suffix, p.dateofbirth as BirthDate, p.ssn, p.upin, p.npi,
                case when p.individualinstitutionflag = 1 then 'true'
	                else 'false'
	                end as isindividual
                from visualcactus.groupaddress ga
                join visualcactus.groupsprovidersaddresses gpa on ga.groupaddress_k = gpa.groupaddress_k
                join visualcactus.provideraddresses pa on gpa.provideraddress_k = pa.provideraddress_k
                join visualcactus.providers p on pa.provider_k = p.provider_k
                where 1 = 1
                and ga.entity_k in('      para','       all')
                and pa.entity_k in('      para','       all')
                and ga.active = 1
                and pa.active = 1
                and ga.group_k = @groupKey";

            IEnumerable<Provider> results = Database.Query<Provider>(sql, new { groupKey = groupKey });
            return results;
        }

        #endregion

        #region provider addresses

        /// <summary>
        /// Gets a list of all addresses currently used by a provider
        /// </summary>
        /// <param name="providerKey">Provider primary key to get addresses for</param>
        /// <returns></returns>
        public IEnumerable<Address> GetProviderAddresses(string providerKey)
        {
            string sql = @"select pa.provideraddress_k as DatabaseKey, rtrim(rt.description) as type,
				rtrim(a.addressline1) as address1, rtrim(a.addressline2) as address2, rtrim(a.city) as city, rtrim(a.state) as state, a.zipcode, a.phone, a.fax, a.taxid,
                a.latitude, a.longitude
				from visualcactus.provideraddresses pa
				join visualcactus.reftable rt on pa.addresstype_rtk = rt.reftable_k
				join visualcactus.addresses a on pa.address_k=a.address_k
				where 1 = 1
				and pa.entity_k in ('      para', '       all')
				and pa.active = 1
				and pa.provider_k = @providerKey
				order by rt.description desc";

            IEnumerable<Address> results = Database.Query<Address>(sql, new { providerKey = providerKey });
            return results;
        }

        #endregion

        #region provider boards

        /// <summary> Gets a list of all boards currently used by a provider </summary>
        /// <param name="providerKey"> Provider primary key to get boards for </param>
        /// <returns></returns>
        public IEnumerable<ProviderBoard> GetProviderBoards(string providerKey)
        {
            string sql = @"select pb.providerboard_k as DatabaseKey, pb.provider_k as ProviderKey, rtrim(rt.description) as Description
				from visualcactus.providerboards pb
				join visualcactus.reftable rt on pb.board_rtk = rt.reftable_k
				where 1 = 1
				and pb.entity_k in ('      para', '       all')
				and pb.provider_k = @providerKey";

            IEnumerable<ProviderBoard> results = Database.Query<ProviderBoard>(sql, new { providerKey = providerKey });
            return results;
        }

        #endregion

        #region provider education

        /// <summary>
        /// Gets a list of all board statuses currently used in the provider database
        /// </summary>
        /// <returns></returns>
        // disabled until needed
        //public Dictionary<string, string> GetProviderBoardStatuses()
        //{
        //    string sql = @"select distinct pb.boardstatus_rtk as [Key], rtrim(rt.description) as [Value]
        //		from visualcactus.providerboards pb
        //		join visualcactus.reftable rt on pb.boardstatus_rtk = rt.reftable_k
        //		where 1 = 1
        //		and pb.entity_k in ('      para', '       all')
        //		and pb.active = 1
        //		order by [value], [key]";

        //    return Database.GetDictionary<string, string>(sql);
        //}

        #endregion provider boards

        #region provider education
        /// <summary>
        /// Gets a list of all education currently used by a provider
        /// </summary>
        /// <param name="providerKey">Provider primary key to get education for</param>
        /// <returns></returns>
        public IEnumerable<ProviderEducation> GetProviderEducation(string providerKey)
        {
            string sql = @"select pe.providereducation_k as DatabaseKey, pe.provider_k as ProviderKey, pe.degree_rtk as DegreeKey, rtrim(rt.description) as Description,
        		rtrim(rt2.description) as Specialty, rtrim(rt3.description) as Program, rtrim(i.name) as Institution,
                pe.startdate, pe.finishdate as enddate
        		from visualcactus.providereducation pe
        		join visualcactus.reftable rt on pe.degree_rtk = rt.reftable_k
        		join visualcactus.reftable rt2 on pe.specialty_rtk = rt2.reftable_k
                join visualcactus.reftable rt3 on pe.program_rtk = rt3.reftable_k
        		join visualcactus.institutions i on pe.institution_k = i.institution_k
        		where 1 = 1
        		and pe.entity_k in ('      para', '       all')
        		and pe.provider_k = @providerKey";

            IEnumerable<ProviderEducation> results = Database.Query<ProviderEducation>(sql, new { providerKey = providerKey });
            return results;
        }

        ///// <summary>
        ///// Gets a list of all education degrees currently used in the provider database
        ///// </summary>
        ///// <returns></returns>
        //disabled until needed
        //public Dictionary<string, string> GetProviderEducationDegrees()
        //{
        //	string sql = @"select distinct rtrim(rt.description) as [Key], pe.degree_rtk as [Value]
        //		from visualcactus.providereducation pe
        //		join visualcactus.reftable rt on pe.degree_rtk = rt.reftable_k
        //		where 1 = 1
        //		and pe.entity_k in ('      para', '       all')
        //		order by [value], [key]";

        //	return Database.GetDictionary<string, string>(sql);
        //}

        ///// <summary>
        ///// Gets a list of all education degree types currently used in the provider database
        ///// </summary>
        ///// <returns></returns>
        //disabled until needed
        //public Dictionary<string, string> GetProviderEducationDegreeTypes()
        //{
        //	string sql = @"select distinct pe.degreetype_rtk as [Value], rtrim(rt.description) as [Key]
        //		from visualcactus.providereducation pe
        //		join visualcactus.reftable rt on pe.degreetype_rtk = rt.reftable_k
        //		where 1 = 1
        //		and pe.entity_k in ('      para', '       all')
        //		order by [value], [key]";

        //	return Database.GetDictionary<string, string>(sql);
        //}

        ///// <summary>
        ///// Gets a list of all education institutions currently used in the provider database
        ///// </summary>
        ///// <returns></returns>
        //disabled until needed
        //public Dictionary<string, string> GetProviderEducationInstitutions()
        //{
        //	string sql = @"select distinct rtrim(i.name) as [Key], pe.institution_k as [Value]
        //		from visualcactus.providereducation pe
        //		join visualcactus.institutions i on pe.institution_k=i.institution_k
        //		where 1 = 1
        //		and pe.entity_k in ('      para', '       all')
        //		order by [value], [key]";

        //	return Database.GetDictionary<string, string>(sql);
        //}

        ///// <summary>
        ///// Gets a list of all education specialties currently used in the provider database
        ///// </summary>
        ///// <returns></returns>
        //disabled until needed
        //public Dictionary<string, string> GetProviderEducationSpecialties()
        //{
        //	string sql = @"select distinct rtrim(rt.description) as [Key], pe.specialty_rtk as [Value]
        //		from visualcactus.providereducation pe
        //		join visualcactus.reftable rt on specialty_rtk = rt.reftable_k
        //		where 1 = 1
        //		and pe.entity_k in ('      para', '       all')
        //		order by [value], [key]";

        //	return Database.GetDictionary<string, string>(sql);
        //}

        #endregion

        #region provider entity assignments

        /// <summary> Gets a list of all entity assignments used by a provider </summary>
        /// <param name="providerKey"> provider primary key to get entity assignments for </param>
        /// <returns></returns>
        public IEnumerable<ProviderEntityAssignment> GetProviderEntityAssignments(string providerKey)
        {
            string sql = @"select ea.ea_k as DatabaseKey, ea.provider_k as ProviderKey, rtrim(ea.recordtype) as RecordType, 
                rtrim(rt.description) as assignment, rtrim(rt2.description) as status, rtrim(rt3.description) as category,
                ea.originalappointmentdate as startdate, ea.termination_date as enddate
                from visualcactus.entityassignments ea
                left join visualcactus.reftable rt on ea.assignment_rtk = rt.reftable_k
                left join visualcactus.reftable rt2 on ea.status_rtk = rt2.reftable_k
                left join visualcactus.reftable rt3 on ea.category_rtk = rt3.reftable_k
                where 1 = 1
                and ea.entity_k = '      para'
        		and ea.provider_k = @providerKey";
            sql += " order by ea.recordtype desc";

            IEnumerable<ProviderEntityAssignment> results = Database.Query<ProviderEntityAssignment>(sql, new { providerKey = providerKey });
            return results;
        }

        #endregion

        #region provider languages

        /// <summary> Gets a list of all languages currently used by a provider </summary>
        /// <param name="providerKey"> Provider primary key to get languages for </param>
        /// <returns></returns>
        public IEnumerable<ProviderLanguage> GetProviderLanguages(string providerKey)
        {
            string sql = @"select pl.providerlanguage_k as DatabaseKey, pl.provider_k as ProviderKey, pl.language_rtk as LanguageKey, rtrim(rt.description) as Description
        		from visualcactus.providerlanguages pl
        		join visualcactus.reftable rt on pl.language_rtk = rt.reftable_k
        		where 1 = 1
        		and pl.provider_k = @providerKey";

            IEnumerable<ProviderLanguage> results = Database.Query<ProviderLanguage>(sql, new { providerKey = providerKey });
            return results;
        }

        ///// <summary> Gets a list of all languages currently used in the provider database </summary>
        ///// <returns></returns>
        //disabled until needed
        //public Dictionary<string, string> GetProviderLanguages()
        //{
        //	string sql = @"select distinct rtrim(rt.description)  as [Key], pl.language_rtk as [Value]
        //		from visualcactus.providerlanguages pl
        //		join visualcactus.reftable rt on pl.language_rtk = rt.reftable_k
        //		where pl.entity_k in ('      para', '       all')
        //		order by [value], [key]";

        //	return Database.GetDictionary<string, string>(sql);
        //}

        #endregion provider languages

        #region provider licenses

        /// <summary>
        /// Gets a list of all licenses currently used by a provider
        /// </summary>
        /// <param name="providerKey">Provider primary key to get specialties for</param>
        /// <returns></returns>
        public IEnumerable<ProviderLicense> GetProviderLicenses(string providerKey)
        {
            string sql = @"select pl.providerlicense_k as DatabaseKey, pl.provider_k as ProviderKey, rtrim(rt.description as Description), rtrim(i.name) as InstitutionName,
                rtrim(rt2.description) as status, rtrim(pl.licensenumber) as licensenumber, rtrim(pl.state) as licensestate,
                pl.awardeddate as startdate, pl.expirationdate as enddate
        		from visualcactus.providerlicenses pl
        		join visualcactus.reftable rt on pl.license_rtk = rt.reftable_k
				join visualcactus.reftable rt2 on pl.status_rtk = rt2.reftable_k
        		join visualcactus.institutions i on pl.institution_k = i.institution_k
        		where 1 = 1
        		and pl.entity_k in ('      para', '       all')
        		and pl.provider_k = @providerKey";

            IEnumerable<ProviderLicense> results = Database.Query<ProviderLicense>(sql, new { providerKey = providerKey });
            return results;
        }

        #endregion

        #region provider specialties

        /// <summary>Gets a list of all specialties currently used by a provider </summary>
        /// <param name="providerKey">Provider primary key to get specialties for </param>
        /// <returns></returns>
        public IEnumerable<ProviderSpecialty> GetProviderSpecialties(string providerKey)
        {
            string sql = @"select ps.providerspecialty_k as DatabaseKey, ps.provider_k as ProviderKey, ps.specialty_rtk as SpecialtyKey, rtrim(rt2.shortdescription) as Code, rtrim(rt.description) as Description
        		from visualcactus.providerspecialties ps
        		join visualcactus.reftable rt on ps.specialty_rtk = rt.reftable_k
                left join visualcactus.reftable rt2 on rt.foriegn_k = rt2.reftable_k
        		where 1 = 1
        		and ps.entity_k in ('      para', '       all')
        		and ps.active = 1
        		and ps.provider_k = @providerKey";

            IEnumerable<ProviderSpecialty> results = Database.Query<ProviderSpecialty>(sql, new { providerKey = providerKey });
            return results;
        }

        ///// <summary>Gets a list of all specialties currently used in the provider database </summary>
        ///// <returns></returns>
        //disabled until needed
        //public Dictionary<string, string> GetProviderSpecialties()
        //{
        //	string sql = @"select distinct rtrim(rt.description) as [Key], ps.specialty_rtk as [Value]
        //		from visualcactus.providerspecialties ps
        //		join visualcactus.reftable rt on specialty_rtk = rt.reftable_k
        //		where 1 = 1
        //		and ps.entity_k in ('      para', '       all')
        //		and ps.active = 1
        //		order by [value], [key]";

        //	return Database.GetDictionary<string, string>(sql);
        //}

        ///// <summary>Gets a list of all specialty types currently used in the provider database </summary>
        ///// <returns></returns>
        //disabled until needed
        //public Dictionary<string, string> GetProviderSpecialtyType()
        //{
        //	string sql = @"select distinct rtrim(rt.description) as [Key], ps.specialtytype_rtk as [Value]
        //		from visualcactus.providerspecialties ps
        //		join visualcactus.reftable rt on specialtystatus_rtk = rt.reftable_k
        //		where 1 = 1
        //		and ps.entity_k in ('      para', '       all')
        //		and specialtytype_rtk <> '      NONE'
        //		and ps.active = 1
        //		order by [value], [key]";

        //	return Database.GetDictionary<string, string>(sql);
        //}

        #endregion provider specialties

    }
}

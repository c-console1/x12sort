﻿using DataLayer.Base;

namespace DataLayer.Credentialing
{
    public class UtilityRepository : BaseRepository<CredentialingDatabase>
    {
        internal UtilityRepository(CredentialingDatabase database) : base(database)
        {

        }

        /// <summary>
        /// Gets next primary key for a given table.
        /// </summary>
        /// <param name="tablePrefix">Prefix for table, see source code for list of prefixes</param>
        /// <param name="table">Table to get key for</param>
        /// <param name="keyColumn">Primary key column name</param>
        /// <remarks>
        /// Abbreviations
        /// * ad - address
        /// * as - assignment specialties
        /// * al - audit log
        /// * af - audit field
        /// * ar - audit record
        /// * ea - entity assignment
        /// * gr - group
        /// * ga - group address
        /// * gp - group provider address
        /// * pv - provider
        /// * pf - provider affiliation
        /// * pb - provider board
        /// * pl - provider language
        /// * ps - provider specialty
        /// * pa - provider address
        /// </remarks>
        /// <returns></returns>
        public string GetNextPrimaryKey(string tablePrefix, string table, string keyColumn)
        {

            table = "visualcactus." + table;
            string sql = "select max(" + keyColumn + ") from " + table + " where " + keyColumn + " like '" + tablePrefix + "#%'";

            string result= Database.QueryFirstOrDefault<string>(sql);

            result = tablePrefix.ToUpper() +"#"+ StaticUtilities.Base36Converter.Encode(StaticUtilities.Base36Converter.Decode(result.Substring(3)) + 1).PadLeft(7, '0');

            return result;
        }

    }
}

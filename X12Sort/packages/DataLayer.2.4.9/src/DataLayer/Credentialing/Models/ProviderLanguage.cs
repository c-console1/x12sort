﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Credentialing.Models
{
    /// <summary>
    /// This is a class for cactus provider languages
    /// </summary>
    public class ProviderLanguage : BaseCredentialingObject
    {
        /// <summary>
        /// Provider primary key
        /// </summary>
        public string ProviderKey { get; set; }
        /// <summary>
        /// Language primary key
        /// </summary>
        public string LanguageKey { get; set; }
        /// <summary>
        /// Language code
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// Language description
        /// </summary>
        public string Description { get; set; }
    }

}

using DataLayer.Base.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Credentialing.Models
{
    /// <summary>
    /// This is a class for cactus provider entity assignments / contracts
    /// </summary>
    public class ProviderEntityAssignment : BaseCredentialingObject, IStartEnd
    {
        /// <summary>
        /// Provider primary key
        /// </summary>
        public string ProviderKey { get; set; }
        /// <summary>
        /// Record type. E = the provider, A = a contract
        /// </summary>
        public string RecordType { get; set; }
        /// <summary>
        /// Contract assignment (advantage, elite, etc)
        /// </summary>
        public string Assignment { get; set; }
        /// <summary>
        /// Contract status
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// Contract category
        /// </summary>
        public string Category { get; set; }
        /// <summary>
        /// Contract start date
        /// </summary>
        public DateTime? StartDate { get; set; }
        /// <summary>
        /// Contract end date
        /// </summary>
        public DateTime? EndDate { get; set; }
    }

}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DataLayer.CaseManagement.Models
{
    /// <summary>
    /// The Actual Answered Assessment, think form filled by user
    /// Table: Assessment
    /// </summary>
    public class Assessment
    {
        /// <summary>
        /// Assessment.assessment_id
        /// </summary>
        public string ID { get; set; }
        /// <summary>
        /// The date the assessment was completed
        /// </summary>
        public DateTime CompletedDate { get; set; }
        /// <summary>
        /// The TruCare Member ID, for the person who this assessment is for 
        /// </summary>
        public string MemberID { get; set; }

        /// <summary>
        /// The Member number for the person who this assessment is for 
        /// </summary>
        public string MemberNumber { get; set; }

        /// <summary>
        /// Random unique id for member not tied to member's data
        /// </summary>
        /// <remarks>This a string formated based of the `AppConfig` value, `[Key] = 'SubjectIdFomat'`</remarks>
        public string SubjectIdentifier { get; set; }
        /// <summary>
        /// Not really sure.....
        /// </summary>
        public int Score { get; set; }
        /// <summary>
        /// Link to the "Blank Form"
        /// </summary>
        public AssessmentDefintion Definition { get; set; }
        /// <summary>
        /// List of all the answer objects associated to this assessment
        /// </summary>
        public List<AssessmentAnswer> Answers { get; set; }

        public void AddAnswer(AssessmentAnswer ans)
        {
            if (ans == null) { return; }

            if (Answers == null)
            {
                Answers = new List<AssessmentAnswer>();
            }
            Answers.Add(ans);
        }

        public Assessment LinkDefintion(AssessmentDefintion defintion)
        {
            Definition = defintion;
            return this;
        }
    }

    /// <summary>
    /// The actual question answer
    /// Every questions doesn't have an associated answer object
    /// Table: Answer
    /// </summary>
    public class AssessmentAnswer
    {
        /// <summary>
        /// Answer.answer_id
        /// </summary>
        public string ID { get; set; }
        /// <summary>
        /// The question ID that is answer is linked to
        /// </summary>
        public string QuestionID { get; set; }
        /// <summary>
        /// The Assessment Id this answer is linked to
        /// </summary>
        public string AssessmentID { get; set; }
        /// <summary>
        /// The actual answer
        /// </summary>
        public string Value { get; set; }
        /// <summary>
        /// If the question had a details, this is what is put in that box
        /// </summary>
        public string AnswerDetail { get; set; }
        /// <summary>
        /// If the questions was a Meta Option Group, this is the ID of the value
        /// </summary>
        public string MetaOptionID { get; set; }
        /// <summary>
        /// If the question was a bool date, this is the date part
        /// </summary>
        public DateTime BoolDate { get; set; }
        /// <summary>
        /// Link to the question
        /// </summary>
        public AssessmentQuestion Question { get; set; }
    }

    /// <summary>
    /// The top level assessment object that defines an assessment. Think unanswered form
    /// Table: assessmentMeta
    /// </summary>
    public class AssessmentDefintion
    {
        /// <summary>
        /// assessmentMeta.assessment_meta_id
        /// </summary>
        public string ID { get; set; }
        /// <summary>
        /// AssessmentMeta.assessment_name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// AssessmentMeta.assessment_type
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// Collection of Groups/Pages in this assessment
        /// </summary>
        public List<AssessmentGroup> Groups { get; set; } = new List<AssessmentGroup>();

        public override string ToString()
        {
            return $"{Name}: ({ID})";
        }

        /// <summary>
        /// Enumeration of all questions in each group
        /// </summary>
        public IEnumerable<AssessmentQuestion> Questions => Groups.SelectMany(q => q.Questions);

        public void AddUpdateGroupQuestion(AssessmentGroup group, AssessmentQuestion question)
        {
            if (group == null) { return; }
            if (question == null) { return; }
            if (Groups == null) { Groups = new List<AssessmentGroup>(); }

            // Add new group to assessment
            if (!Groups.Any(x => x.ID == group.ID))
            {
                group.AddQuestion(question);
                Groups.Add(group);
            }
            else
            {
                Groups.First(x => x.ID == group.ID).AddQuestion(question);

            }
        }
    }

    /// <summary>
    /// A group/page of questions in an assessment definition
    /// Table: AssessmentQuestionGroupDefinition
    /// </summary>
    public class AssessmentGroup
    {
        /// <summary>
        /// AssessmentQuestionGroupDefinition.assessment_question_group_definition_id
        /// </summary>
        public string ID { get; set; }
        /// <summary>
        /// QuestionGroupDefinition.qg_name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Order in which this group appears
        /// AssessmentQuestionGroupDefinition.ordinal
        /// </summary>
        public int Order { get; set; }
        /// <summary>
        /// Collection of questions in this group
        /// </summary>
        public List<AssessmentQuestion> Questions { get; set; } = new List<AssessmentQuestion>();

        public override string ToString()
        {
            return $"{Name}: ({ID})";
        }

        public void AddQuestion(AssessmentQuestion question)
        {
            if (question == null) { return; }
            if (Questions == null)
            {
                Questions = new List<AssessmentQuestion>() { question };
            }
            else
            {
                if (!Questions.Any(q => q.ID == question.ID))
                {
                    Questions.Add(question);
                }
            }
        }
    }

    /// <summary>
    /// This is an questions in the assessment
    /// Table: QuestionDefinition (primary, there is a bunch...)
    /// </summary>
    public class AssessmentQuestion
    {
        /// <summary>
        /// QuestionDefinition.question_id
        /// </summary>
        public string ID { get; set; }
        /// <summary>
        /// Should be the question ID of the parent, if this is a conditional question
        /// </summary>
        public string ParentID { get; set; }
        /// <summary>
        /// The order this question is in its "Group", it can be the order in the Assessment Group or a order a conditional set of questions
        /// ConditionalResponseDefinition_QuestionDefinition.ordinal
        /// </summary>
        public int Order { get; set; }
        /// <summary>
        /// The actual question ...
        /// QuestionDefinition.question_text
        /// </summary>
        public string QuestionText { get; set; }
        public string ToolTip { get; set; }
        public bool IsRequired { get; set; }
        /// <summary>
        /// I have no idea what the value is pre-populated with... Good luck
        /// </summary>
        public bool IsPrePopulated { get; set; }
        /// <summary>
        /// How is the value displayed on the TruCare Assessment
        /// WATCH OUT FOR LABELS, they are technically unanswerable questions
        /// 
        /// Values: SELECT * FROM DisplayTypeConfiguration
        /// </summary>
        public string DisplayedAs { get; set; }
        /// <summary>
        /// The "type" of the response, you will want to check these values before a converting
        /// Values: BOOLEAN, BOOLEAN_DATE, DATE, INTERGER, META_OPTION, STRING, DIAGNOSIS
        /// </summary>
        public string DataType { get; set; }
        /// <summary>
        /// If the question is a meta option Group, this is the name of that group
        /// </summary>
        public string OptionGroupName { get; set; }
        /// <summary>
        /// User Defined Field, not used by the system
        /// </summary>
        public string Notes { get; set; }
        /// <summary>
        /// A incomplete list of possible answers that have special properties, mostly secondary set of questions dependent on the answer
        /// </summary>
        public List<AssessmentActionableAnswer> ActionableAnswers { get; set; } = new List<AssessmentActionableAnswer>();
        /// <summary>
        /// A list of answer objects for this questions, loaded with assessments
        /// </summary>
        public List<AssessmentAnswer> Answers { get; set; } = new List<AssessmentAnswer>();

        /// <summary>
        /// An enumeration of all the conditional questions in the `ActionableAnswers`
        /// </summary>
        /// <seealso cref="ActionableAnswers"/>
        public IEnumerable<AssessmentQuestion> ChildConditionalQuestions => ActionableAnswers.Where(q => q.ConditionalQuestions != null).SelectMany(q => q.ConditionalQuestions);
        

        public override string ToString()
        {
            return $"{QuestionText} [{DisplayedAs}] ({ID})";
        }

        public void AddAnswer(AssessmentAnswer ans, bool linkAnswer)
        {
            if (ans == null) { return; }

            if (Answers == null)
            {
                Answers = new List<AssessmentAnswer>();
            }
            Answers.Add(ans);
            if (linkAnswer)
            {
                ans.Question = this;
            }
        }

        /// <summary>
        /// Add/Update an actionable action and associated conditional Question
        /// </summary>
        /// <param name="actAns"></param>
        /// <param name="conditionalQuestion"></param>
        /// <remarks>There can be answer details without having conditional responses</remarks>
        public void AddUpdateActionableAnswer(AssessmentActionableAnswer actAns, AssessmentQuestion conditionalQuestion)
        {
            if (actAns == null) { return; }
            if (ActionableAnswers == null)
            {
                ActionableAnswers = new List<AssessmentActionableAnswer>();
            }

            if (!ActionableAnswers.Any(q => q.ID == actAns.ID))
            {
                actAns.AddConditionalQuestion(conditionalQuestion);
                ActionableAnswers.Add(actAns);
            }
            else
            {
                ActionableAnswers.First(q => q.ID == actAns.ID).AddConditionalQuestion(conditionalQuestion);
            }
        }

        public void UpdateData(AssessmentQuestion questionData)
        {
            IsPrePopulated = questionData.IsPrePopulated;
            IsRequired = questionData.IsRequired;
            Order = questionData.Order;
            QuestionText = questionData.QuestionText;
            ToolTip = questionData.ToolTip;
            DisplayedAs = questionData.DisplayedAs;
            DataType = questionData.DataType;
            OptionGroupName = questionData.OptionGroupName;
            ActionableAnswers = questionData.ActionableAnswers.ToList();
        }
    }

    /// <summary>
    /// This is a weird class...
    /// Basically it is a "if this answer, this happens" class
    /// Table: AssessmentResponseDefinition
    /// </summary>
    public class AssessmentActionableAnswer
    {
        /// <summary>
        /// AssessmentResponseDefinition.assessment_response_definition_id
        /// </summary>
        public string ID { get; set; }
        /// <summary>
        /// No idea what this is used for, but it is occasionally filled in 
        /// </summary>
        public int Score { get; set; }
        /// <summary>
        /// The value that must be present in order for this "action" to be applied
        /// </summary>
        public string Value { get; set; }
        /// <summary>
        /// A sub-list of questions that are now dependent on the given answer
        /// </summary>
        public List<AssessmentQuestion> ConditionalQuestions { get; set; }
        public override string ToString()
        {
            return $"{Value} ({Score})";
        }

        public void AddConditionalQuestion(AssessmentQuestion question)
        {
            if (question == null) { return; }
            if (ConditionalQuestions == null)
            {
                ConditionalQuestions = new List<AssessmentQuestion>() { question };
            }
            else
            {
                if (!ConditionalQuestions.Any(q => q.ID == question.ID))
                {
                    ConditionalQuestions.Add(question);
                }
            }
        }
    }

}

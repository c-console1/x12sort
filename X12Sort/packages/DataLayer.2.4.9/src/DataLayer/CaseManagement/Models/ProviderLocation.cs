using System.Collections.Generic;
using System.Xml.Serialization;

namespace DataLayer.CaseManagement.Models
{
    [XmlRoot(ElementName = "provider_location")]
    public class ProviderLocation : BaseCaseManagementObject
    {
        [XmlElement(ElementName = "external_provider_location_id")]
        public string ExternalID { get; set; }

        [XmlElement(ElementName = "provider_type_id")]
        public string TypeID { get; set; }

        [XmlElement(ElementName = "name")]
        public string Name { get; set; }

        [XmlElement(ElementName = "tin")]
        public string TaxID { get; set; }

        [XmlElement(ElementName = "provider_npi")]
        public string NPI { get; set; }

        [XmlElement(ElementName = "medicare_number")]
        public string MedicareNumber { get; set; }

        [XmlElement(ElementName = "medicaid_number")]
        public string MedicaidNumber { get; set; }

        [XmlElement(ElementName = "preferred")]
        public bool IsPreferred { get; set; }

        [XmlElement(ElementName = "number")]
        public string ProviderNumber { get; set; }

        [XmlElement(ElementName = "external_provider_group_id")]
        public string ExternalGroupID { get; set; }

        [XmlArray(ElementName = "addresses")]
        public IEnumerable<ProviderAddress> Addresses { get; set; }

        [XmlArray(ElementName = "phones")]
        public IEnumerable<ProviderPhone> Phones { get; set; }

        [XmlArray(ElementName = "emails")]
        public IEnumerable<ProviderEmail> Emails { get; set; }

        [XmlElement]
        public IEnumerable<ProviderLocationNetworkReference> Networks { get; set; }
    }
}

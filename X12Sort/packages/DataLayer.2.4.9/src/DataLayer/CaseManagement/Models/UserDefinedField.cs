using System.Xml.Serialization;

namespace DataLayer.CaseManagement.Models
{
    public class UserDefinedField
    {
        [XmlElement(ElementName = "extension_field_name")]
        public string Name { get; set; }

        [XmlElement(ElementName = "extension_field_value", IsNullable = false)]
        public string Value { get; set; }

        [XmlElement(ElementName = "extension_field_option_value_id", IsNullable = false)]
        public string OptionValue { get; set; }

        [XmlIgnore]
        public bool IsOptionValue { get { return OptionValue != null; } }
    }
}

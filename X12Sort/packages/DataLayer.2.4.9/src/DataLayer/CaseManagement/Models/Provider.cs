using System.Collections.Generic;
using System.Xml.Serialization;

namespace DataLayer.CaseManagement.Models
{
    [XmlRoot(ElementName = "provider")]
    public class Provider : BaseCaseManagementObject
    {
        [XmlElement(ElementName = "external_provider_id")]
        public string ExternalID { get; set; }

        [XmlElement(ElementName = "demographic")]
        public ProviderDemographic Demographics { get; set; }

        [XmlArray(ElementName = "provider_specialties")]
        public IEnumerable<ProviderSpecialty> Specialties { get; set; }

        [XmlArray(ElementName = "provider_boardcertifications")]
        public IEnumerable<ProviderBoardCertification> Certifications { get; set; }

        [XmlArray(ElementName = "provider_locations")]
        public IEnumerable<ProviderLocation> Locations { get; set; }
    }
}

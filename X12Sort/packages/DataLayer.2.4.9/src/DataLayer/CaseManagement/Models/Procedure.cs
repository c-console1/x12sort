using DataLayer.Base.Models;
using System;

namespace DataLayer.CaseManagement.Models
{
    public class Procedure : BaseCaseManagementObject, IAuthorizationProcedure
    {
        public string ProcedureType { get; set; }
        public string ProcedureName { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}

using DataLayer.Base.Models;

namespace DataLayer.CaseManagement.Models
{
    //TODO: replace temp types with clases
    public class ServicingProvider : BaseCaseManagementObject, IAuthorizationProvider
    {
        public string ProviderID { get; set; }
        public string ProviderExternalNumber { get; set; }
        public string ProviderName { get; set; }
        public string ProviderNPI { get; set; }
        public string ProviderTaxonomy { get; set; }
        /// <summary>
        /// Name of contact at provider location
        /// </summary>
        public string ProviderContactName { get; set; }
        public dynamic ProviderPhone { get; set; } //TODO: utilize datalayer phone object
        public dynamic ContactPhone { get; set; } //TODO: utilize datalayer phone object
    }
}

using DataLayer.Base.Models;
using System;


namespace DataLayer.CaseManagement.Models
{
    public class Admission : BaseCaseManagementObject, IAdmission
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? AdmissionDate { get; set; }
        public string AdmissionSource { get; set; }
        public string AdmissionType { get; set; }
    }
}

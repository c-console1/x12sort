using System.Collections.Generic;
using System.Xml.Serialization;

namespace DataLayer.CaseManagement.Models
{
    [XmlRoot(ElementName = "network_set")]
    public class NetworkSet : BaseCaseManagementStartEndXmlObject
    {
        [XmlElement(ElementName = "external_network_set_id")]
        public string ExternalID { get; set; }

        [XmlElement(ElementName = "name")]
        public string Name { get; set; }

        [XmlArray(ElementName = "network_set_to_networks")]
        public IEnumerable<NetworkNetworkSetReference> NetworkSets { get; set; }
}
}

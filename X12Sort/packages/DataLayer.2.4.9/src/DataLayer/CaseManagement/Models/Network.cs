using System.Collections.Generic;
using System.Xml.Serialization;

namespace DataLayer.CaseManagement.Models
{
    [XmlRoot(ElementName = "network")]
    public class Network : BaseCaseManagementStartEndXmlObject
    {
        [XmlElement(ElementName = "external_network_id")]
        public string ExternalID { get; set; }

        [XmlElement(ElementName = "name")]
        public string Name { get; set; }
    }
}

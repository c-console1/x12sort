using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using DataLayer.Base.Models;

namespace DataLayer.CaseManagement.Models
{
    [XmlRoot(ElementName = "provider_specialty")]
    public class ProviderSpecialty : BaseCaseManagementStartEndXmlObject
    {
        [XmlElement(ElementName = "external_provider_specialty_id")]
        public string ExternalID { get; set; }

        [XmlElement(ElementName = "specialty_code")]
        public string Code { get; set; }

        [XmlElement(ElementName = "effective_date")]
        public override string XmlStartDate { get { return base.XmlStartDate; } set { base.XmlStartDate = value; } }

        [XmlElement(ElementName = "termination_date")]
        public override string XmlEndDate { get { return base.XmlEndDate; } set { base.XmlEndDate = value; } }
    }
}

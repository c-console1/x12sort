using DataLayer.Base.Models;

namespace DataLayer.CaseManagement.Models
{
    public class RequestingProvider : BaseCaseManagementObject, IAuthorizationProvider
    {
        public string ProviderID { get; set; }
        public string ProviderExternalNumber { get; set; }
        public string ProviderName { get; set; }
        public string ProviderNPI { get; set; }
        public string ProviderTaxonomy { get; set; }
        /// <summary>
        /// Name of contact at provider location
        /// </summary>
        public string ProviderContactName { get; set; }
        /// <summary>
        /// Provider phone
        /// </summary>
        public dynamic ProviderPhone { get; set; } //TODO: utilize datalayer phone object
        /// <summary>
        /// Provider contact phone
        /// </summary>
        public dynamic ContactPhone { get; set; } //TODO: utilize datalayer phone object
    }
}


using System.Xml.Serialization;

namespace DataLayer.CaseManagement.Models
{
    [XmlRoot(ElementName = "network_set_to_network")]
    public class NetworkNetworkSetReference : BaseCaseManagementStartEndXmlObject
    {
        [XmlElement(ElementName = "external_network_set_to_network_id")]
        public string ExternalID { get; set; }

        [XmlElement(ElementName = "external_network_set_id")]
        public string ExternalNetworkSetID { get; set; }
    }
}

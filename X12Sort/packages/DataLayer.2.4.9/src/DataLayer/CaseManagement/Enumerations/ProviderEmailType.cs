using System.Xml.Serialization;

namespace DataLayer.CaseManagement.Enumerations
{
    public enum ProviderEmailType : int
    {
        [XmlEnum(Name = "")]
        Unknown = 0,

        [XmlEnum(Name = "Office")]
        Office = 1,

        [XmlEnum(Name = "Direct")]
        Direct = 2
    }
}

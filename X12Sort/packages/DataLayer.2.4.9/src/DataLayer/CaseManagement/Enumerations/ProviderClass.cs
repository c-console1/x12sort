using System.Xml.Serialization;

namespace DataLayer.CaseManagement.Enumerations
{
    public enum ProviderClass : int
    {
        [XmlEnum("")]
        Unknown = 0,

        [XmlEnum("1")]
        Individual = 1,

        [XmlEnum("2")]
        Facility = 2
    }
}

using DataLayer.Base;
using DataLayer.CaseManagement.Models;
using System.Collections.Generic;

namespace DataLayer.CaseManagement
{
	public class ProviderRepository : BaseRepository<CaseManagementDatabase>
	{
		internal ProviderRepository(CaseManagementDatabase database) : base(database)
		{

		}

		public Provider GetProvider(string externalID)
		{
			string sql = @"select provider_id as DatabaseKey, external_id as ExternalID, provider_name as Name, medicaid_number as MedicaidNumber, medicare_number as MedicareNumber,
				npi as NPI, tin as TaxID, start_date as StartDate, end_date as EndDate
				from provider
				where 1=1
				and external_id = @externalID";

            Provider result = Database.QueryFirstOrDefault<Provider>(sql, new
			{
				externalID = externalID
			});

			return result;
		}

		public IEnumerable<ProviderLocation> GetLocationsByProvider(string providerID)
		{
			string sql = @"select location_id as DatabaseKey, external_id as ExternalID, name as Name, location_type as LocationType, medicaid_number as MedicaidNumber,
				medicare_number as MedicareNumber, tin as TaxID, start_date as StartDate, end_date as EndDate
				from location
				where 1=1
				and provider_id = @providerID";

			IEnumerable<ProviderLocation> result = Database.Query<ProviderLocation>(sql, new
			{
				providerID = providerID
			});

			return result;
		}

		public IEnumerable<ProviderLocationNetworkReference> GetNetworksByLocation(string locationID)
		{
			string sql = @"select nl.network_location_id as DatabaseKey, n.name as Name, nl.medicaid_par as MedicaidNumber, nl.medicare_par as MedicareNumber, nl.start_date as StartDate, nl.end_date as EndDate
				from networklocation nl
				join network n on nl.network_id = n.network_id
				where 1=1
				and nl.location_id = @locationID";

			IEnumerable<ProviderLocationNetworkReference> result = Database.Query<ProviderLocationNetworkReference>(sql, new
			{
				locationID = locationID
			});

			return result;
		}

	}
}

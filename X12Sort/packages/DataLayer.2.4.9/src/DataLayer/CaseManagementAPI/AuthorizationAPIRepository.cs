using DataLayer.Base;
using DataLayer.Base.Models;
using DataLayer.CaseManagementAPI.Models;
using Flurl.Http;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;


namespace DataLayer.CaseManagementAPI
{
    public class AuthorizationAPIRepository : BaseRepository<CaseManagementAPIDatabase>
    {
        internal AuthorizationAPIRepository(CaseManagementAPIDatabase database) : base(database)
        {

        }
        public RestInpatientAuthorization GetInpatientAuthorization(string authNbr)
        {
            return (RestInpatientAuthorization)GetAuthorization(authNbr).Result;
        }

        public RestOutpatientAuthorization GetOutpatientAuthorization(string authNbr)
        {
            return (RestOutpatientAuthorization)GetAuthorization(authNbr).Result;
        }

        public RestInpatientAuthorization GetInpatientAuthorization(string memberNbr, string authNbr)
        {
            return (RestInpatientAuthorization)GetAuthorization(memberNbr, authNbr).Result;
        }

        public RestOutpatientAuthorization GetOutpatientAuthorization(string memberNbr, string authNbr)
        {
            return (RestOutpatientAuthorization)GetAuthorization(memberNbr, authNbr).Result;
        }

        internal async Task<IAuthorization> GetAuthorization(string authNbr)
        {
            HttpResponseMessage response = await Database.Api
                .Request($"/members/DEFAULT/authorizations/{authNbr}?memberIdType=EXTERNAL&authorizationIdType=AUTHORIZATION_NUMBER")
                .GetJsonAsync();

            return JsonConvert.DeserializeObject<IAuthorization>(await response.Content.ReadAsStringAsync());
        }

        internal async Task<IAuthorization> GetAuthorization(string memberNbr, string authNbr)
        {
            HttpResponseMessage response = await Database.Api
                .Request($"/members/{memberNbr}/authorizations/{authNbr}?memberIdType=EXTERNAL&authorizationIdType=AUTHORIZATION_NUMBER")
                .GetJsonAsync();

            return JsonConvert.DeserializeObject<IAuthorization>(await response.Content.ReadAsStringAsync());
        }
        //TODO: Add more methods for posting and requesting data
        //TODO: Set up DAM api access for TC test
    }
}

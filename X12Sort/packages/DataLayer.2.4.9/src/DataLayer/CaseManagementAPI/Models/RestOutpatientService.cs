using DataLayer.Base.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DataLayer.CaseManagementAPI.Models
{
    public class RestOutpatientService : BaseCaseManagementAPIObject, IAuthorizedService
    {
        #region Members
        private List<RestProcedure> _procs;
        #endregion
        #region Metadata
        /// <summary>
        /// Auth version
        /// </summary>
        [JsonProperty(PropertyName = "version")]
        public string AuthorizationVersion { get; set; }
        /// <summary>
        /// Date the service was authorized
        /// </summary>
        [JsonProperty(PropertyName = "requestDate")]
        public DateTime? AuthorizedDate { get; set; }
        /// <summary>
        /// User who authorized the service
        /// </summary>
        [JsonIgnore]
        public string AuthorizedBy { get; set; }
        /// <summary>
        /// Type of authorization
        /// </summary>
        [JsonProperty(PropertyName = "serviceType")]
        public string AuthorizationType { get; set; }
        /// <summary>
        /// Start date of the service
        /// </summary>
        [JsonProperty(PropertyName = "fromDate")]
        public DateTime? StartDate { get; set; }
        /// <summary>
        /// End date of the service
        /// </summary>
        [JsonProperty(PropertyName = "toDate")]
        public DateTime? EndDate { get; set; }
        /// <summary>
        /// Urgency level of the service
        /// </summary>
        [JsonIgnore]
        public string Priority { get; set; }
        /// <summary>
        /// Status of the service. Approved, denied, etc.
        /// </summary>
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }
        /// <summary>
        /// Type of the service authorized
        /// </summary>
        [JsonProperty(PropertyName = "lineItemType")]
        public string Type { get; set; }
        /// <summary>
        /// Count of approved items (units, visits, etc)
        /// </summary>
        [JsonProperty(PropertyName = "unitsToDetermine")]
        public int ApprovedCount { get; set; }
        /// <summary>
        /// Amount approved
        /// </summary>
        [JsonIgnore]
        public int ApprovedAmount { get; set; }
        /// <summary>
        /// Type of unit approved (units, visits, days, etc)
        /// </summary>
        [JsonIgnore]
        public string UnitType { get; set; }
        /// <summary>
        /// Service notes
        /// </summary>
        [JsonIgnore]
        public List<dynamic> Notes { get; set; } //TODO: Use note object in existing datalayer?
        /// <summary>
        /// List of UDFs, can be anything
        /// </summary>
        [JsonIgnore]
        public List<dynamic> UserDefined { get; set; }
        #endregion
        #region Proc codes
        /// <summary>
        /// List of procedure codes
        /// </summary>
        [JsonProperty(PropertyName = "procedures")]
        public List<RestProcedure> ProcedureCodes
        {
            get => _procs;
            set => _procs = value;
        }
        [JsonIgnore]
        List<IAuthorizationProcedure> IAuthorizedService.ProcedureCodes
        {
            get => _procs.Select(proc => (IAuthorizationProcedure)proc).ToList();
            set => _procs = value.Cast<RestProcedure>().ToList();
        }
        #endregion
        /// <summary>
        /// Provider performing the service
        /// </summary>
        [JsonProperty(PropertyName = "servicingProvider")]
        public RestServicingProvider ServicingProvider { get; set; }
    }
}

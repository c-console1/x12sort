using DataLayer.Base.Models;
using Newtonsoft.Json;
using System;

namespace DataLayer.CaseManagementAPI.Models
{
    public class RestAuthorizationDiagnosis : BaseCaseManagementAPIObject, IAuthorizationDiagnosis
    {
        [JsonProperty(PropertyName = "diagnosisCode")]
        public string Code { get; set; }

        [JsonIgnore]
        public string DiagnosisType { get; set; }

        [JsonIgnore]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "diagnosisName")]
        public string DiagnosisName { get; set; }

        [JsonProperty(PropertyName = "effectiveDate")]
        public DateTime? StartDate { get; set; }

        [JsonProperty(PropertyName = "terminationDate")]
        public DateTime? EndDate { get; set; }
    }
}

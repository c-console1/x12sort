using DataLayer.Base.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace DataLayer.CaseManagementAPI.Models
{
    /// <summary>
    /// Inpatient utilization management authorization object
    /// </summary>
    public class RestInpatientAuthorization : BaseCaseManagementAPIObject, IAuthorization
    {
        #region Members
        private List<RestInpatientService> _authorizedServices;
        private RestRequestingProvider _requestingProvider;
        private RestAuthorizationDiagnosis _diagnosis;
        #endregion

        #region Metadata
        /// <summary>
        /// Version of the authorization
        /// </summary>
        [JsonProperty(PropertyName = "version")]
        public int AuthVersion { get; set; }

        /// <summary>
        /// Authorization number
        /// </summary>
        [JsonProperty(PropertyName = "authorizationNum")]
        public string AuthorizationNumber { get; set; }

        /// <summary>
        /// Header information of the authorization
        /// </summary>
        [JsonProperty(PropertyName = "authorizationHeader")]
        public RestAuthorizationHeader Header { get; set; }
        #endregion

        #region Admission
        /// <summary>
        /// Admission for inpatient stays. Casenet API does not return this in getAuthorization.
        /// </summary>
        [JsonIgnore]
        public RestAdmission Admission { get; set; } //TODO: may have to either remove, or map by hand, there is no matching json attribute
        #endregion

        #region Diagnosis
        IAuthorizationDiagnosis IAuthorization.AuthorizationDiagnosis
        {
            get => _diagnosis;
            set => _diagnosis = (RestAuthorizationDiagnosis)value;
        }

        /// <summary>
        /// Diagnosis information for the authorization
        /// </summary>
        [JsonProperty(PropertyName = "authorizationDiagnosis")]
        public RestAuthorizationDiagnosis AuthorizationDiagnosis
        {
            get => _diagnosis;
            set => _diagnosis = value;
        }
        #endregion

        #region Services
        /// <summary>
        /// Collection of the service or services for the authorization
        /// </summary>
        [JsonProperty(PropertyName = "lineItems")]
        public List<RestInpatientService> AuthorizedServices
        {
            get => _authorizedServices;
            set => _authorizedServices = value;
        }

        List<IAuthorizedService> IAuthorization.AuthorizedServices
        {
            get => _authorizedServices.Select(serv => (IAuthorizedService)serv).ToList();
            set => _authorizedServices = value.Cast<RestInpatientService>().ToList();
        }
        #endregion

        #region Providers
        /// <summary>
        /// Provider information for the provider whom requested the services
        /// </summary>
        [JsonProperty(PropertyName = "requestingProvider")]
        public RestRequestingProvider RequestingProvider
        {
            get => _requestingProvider;
            set => _requestingProvider = value;
        }

        IAuthorizationProvider IAuthorization.RequestingProvider
        {
            get => (IAuthorizationProvider)_requestingProvider;
            set => _requestingProvider = (RestRequestingProvider)value;
        }

        /// <summary>
        /// Provider information for the provider whome performed the services
        /// </summary>
        [JsonProperty(PropertyName = "servicingFacility")]
        public RestServicingProvider ServicingProvider { get; set; }
        #endregion
    }
}

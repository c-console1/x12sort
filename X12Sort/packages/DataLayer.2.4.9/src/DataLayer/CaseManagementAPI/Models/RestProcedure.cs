using DataLayer.Base.Models;
using Newtonsoft.Json;
using System;

namespace DataLayer.CaseManagementAPI.Models
{
    public class RestProcedure : BaseCaseManagementAPIObject
    {
        [JsonProperty(PropertyName = "id")]
        public string ProcedureID { get; set; }

        [JsonProperty(PropertyName = "procedureCode")]
        public TCRestProcedureCode ProcedureCode { get; set; }
    }

    public class TCRestProcedureCode : BaseCaseManagementAPIObject, IAuthorizationProcedure
    {
        [JsonIgnore]
        public string ProcedureType { get; set; }

        [JsonIgnore]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "procedureName")]
        public string ProcedureName { get; set; }

        [JsonProperty(PropertyName = "procedureCode")]
        public string Code { get; set; }

        [JsonProperty(PropertyName = "effectiveDate")]
        public DateTime? StartDate { get; set; }

        [JsonProperty(PropertyName = "terminationDate")]
        public DateTime? EndDate { get; set; }
    }
}

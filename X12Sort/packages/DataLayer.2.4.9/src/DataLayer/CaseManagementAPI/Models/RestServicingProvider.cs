using Newtonsoft.Json;

namespace DataLayer.CaseManagementAPI.Models
{
    public class RestServicingProvider : BaseCaseManagementAPIObject
    {
        [JsonProperty(PropertyName = "servicingProvider")]
        public TCRestInnerProvider Provider { get; set; }

        [JsonProperty(PropertyName = "servicingFacility")]
        public TCRestInnerProvider Facility { get; set; }
    }
}

using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace DataLayer.CaseManagementAPI.Models
{
    public class RestAuthorizationHeader : BaseCaseManagementAPIObject
    {
        /// <summary>
        /// Authorization number
        /// </summary>
        [JsonProperty(PropertyName = "authorizationNum")]
        public string AuthorizationNumber { get; set; }

        /// <summary>
        /// Member number. Casenet API does not return this in getAuthorization.
        /// </summary>
        [JsonIgnore]
        public string MemberNumber { get; set; }

        /// <summary>
        /// Denotes whether the authorization is open
        /// </summary>
        [JsonProperty(PropertyName = "isOpen")]
        public bool Open { get; set; }

        /// <summary>
        /// Overall status of the authorization (approved, partial approved, denied, etc)
        /// </summary>
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }

        /// <summary>
        /// Party from which the authorization was referred (PCP, self, specialist, etc)
        /// </summary>
        [JsonIgnore]
        public string ReferralSource { get; set; }

        /// <summary>
        /// Date of the authorization
        /// </summary>
        [JsonProperty(PropertyName = "stateDate")]
        public DateTime? StateDate { get; set; }

        /// <summary>
        /// Date the authorization was requested
        /// </summary>
        [JsonProperty(PropertyName = "requestDate")]
        public DateTime? RequestedDate { get; set; }

        /// <summary>
        /// Date the authorization was received
        /// </summary>
        [JsonProperty(PropertyName = "authorizationNum")]
        public DateTime? ReceivedDate { get; set; }

        /// <summary>
        /// Input source of the authorization
        /// </summary>
        [JsonIgnore]
        public string InputSource { get; set; }

        /// <summary>
        /// Date the authorization was moved to the claims system
        /// </summary>
        [JsonProperty(PropertyName = "dateSentToClaims")]
        public DateTime? SentToClaims { get; set; }

        /// <summary>
        /// User defined elements to an authorization. Can be anything.
        /// </summary>
        [JsonProperty(PropertyName = "extendedAttributes")]
        public List<dynamic> UserDefined { get; set; }
    }
}

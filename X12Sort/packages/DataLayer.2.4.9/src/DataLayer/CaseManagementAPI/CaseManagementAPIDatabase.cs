using DataLayer.Base;
using System.Linq;

namespace DataLayer.CaseManagementAPI
{
    public class CaseManagementAPIDatabase : BaseDatabase
    {
        /// <summary>
        /// API executor to make actual API calls
        /// </summary>
        public IApiExec Api { get; private set; }
        public AuthorizationAPIRepository AuthorizationAPI { get; private set; }

        internal CaseManagementAPIDatabase(params IExecutor[] executors) : base(executors)
        {
            Api = Executors.First(q => q.Name.Contains("CaseManagementAPI")) as IApiExec;
            ((ApiExec)Api).Client.Headers["Content-Type"] = "application/json";
            AuthorizationAPI = new AuthorizationAPIRepository(this);
        }
    }
}

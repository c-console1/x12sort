using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace DataLayer
{
    /// <summary>
    /// Builds the <c> DataAccessConfig </c> object from various configuration sources.
    /// </summary>
    /// <remarks>
    /// <para> This is singleton class </para>
    /// <para> Internally it uses <c> IConfiguration </c> and builds the configuration from the following sources </para>
    ///
    /// * `appSettings.json` (in folder with dam.exe)
    /// * User's `dam.json` Config File (path: `%USER%\DataAccessManager\dam.json`)
    /// * Environment Variables
    /// * Command Line Arguments
    ///
    /// *Note: This is sort by priority, lowest first. Configuration values can be over ridden in higher priority sources*
    /// <para> Reference: https://medium.com/@dmitryzaets/legacy-net-applications-configuration-management-net-framework-4-5-1-68220335d9d8 </para>
    /// <para> Command line arguments are not used by default, see example of how to use them </para>
    /// </remarks>
    /// <example>
    /// <c> DataAccessConfigBuilder </c> is created on demand when accessed for the first time
    /// <code>
    /// <![CDATA[
    /// string cxnStr = DataAccessConfigBuilder.Instance.Config.AppConfigConnectionString;
    /// ]]>
    /// </code>
    /// </example>
    /// ///
    /// <example>
    /// If you want to use the command line arguments, make sure to call <c> UseCliArgs </c> before using the <c> DataAccessConfigBuilder </c>
    /// <code>
    /// <![CDATA[
    /// private static void Main(string[] args)
    /// {
    /// DataAccessConfigBuilder.UseCliArgs(args);
    /// string cxnStr = DataAccessConfigBuilder.Instance.Config.AppConfigConnectionString;
    /// // stuff....
    /// }
    /// ]]>
    /// </code>
    /// </example>
    /// <seealso cref="DataAccessConfig"/>
    /// <seealso cref="DbExec.Create"/>
    /// <seealso cref="IConfiguration"/>
    /// <seealso cref="IConfigurationBuilder"/>
    public class DataAccessConfigBuilder
    {
        private static DataAccessConfigBuilder _instance;
        private IConfiguration _config;
        private readonly DataAccessConfig _dac;

        /// <summary>
        /// Absolute path to the dam.json configuration file
        /// </summary>
        /// <value> Gets the path to `%USER%\DataAccessManager\dam.json` </value>
        public string DamConfigPath { get { return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "DataAccessManager", "dam.json"); } }

        /// <summary>
        /// The "built" configuration object
        /// </summary>
        /// <value> Gets the internal `DataAccessConfig` object </value>
        public DataAccessConfig Config { get { return _dac; } }

        /// <summary>
        /// The singleton instance
        /// </summary>
        /// <value> Gets the private singleton instance </value>
        public static DataAccessConfigBuilder Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new DataAccessConfigBuilder(null);
                }
                return _instance;
            }
        }

        /// <summary>
        /// Creates a new instance of this class
        /// </summary>
        /// <param name="args"> The command line arguments array </param>
        private DataAccessConfigBuilder(string[] args)
        {
            var configBuilder = new ConfigurationBuilder()
               .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
               .AddJsonFile(DamConfigPath, optional: true, reloadOnChange: true);

            configBuilder.AddEnvironmentVariables();

            if (args != null)
            {
                configBuilder.AddCommandLine(args);
            }
            _config = configBuilder.Build();

            _dac = new DataAccessConfig();
            _config.Bind(_dac);
        }

        /// <summary>
        /// Create <c> DataAccessConfigBuilder </c> instance and use command line arguments
        /// </summary>
        /// <param name="args"> The command line arguments array </param>
        public static void UseCliArgs(string[] args)
        {
            _instance = new DataAccessConfigBuilder(args);
        }
    }
}

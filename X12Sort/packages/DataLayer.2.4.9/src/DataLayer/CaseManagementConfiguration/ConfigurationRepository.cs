using DataLayer.Base;
using DataLayer.CaseManagement.Models;
using System.Collections.Generic;
using System.Data;

namespace DataLayer.CaseManagementConfiguration
{
    public class ConfigurationRepository : BaseRepository<CaseManagementConfigurationDatabase>
    {
        internal ConfigurationRepository(CaseManagementConfigurationDatabase database) : base(database)
        {

        }

        /// <summary>
        /// Gets an integer from the database
        /// </summary>
        /// <param name="FeedName"></param>
        /// <param name="Key"></param>
        /// <returns></returns>
        public int GetInt(string FeedName, string Key)
        {
            string sql = @"
                select value from appconfig
                where 1=1
                and feedname = @FeedName
                and [key] = @Key";

            int result = Database.QueryFirstOrDefault<int>(sql, new
            {
                FeedName = FeedName,
                Key = Key
            });

            return result;
        }

        /// <summary>
        /// Gets a string from the database
        /// </summary>
        /// <param name="FeedName"></param>
        /// <param name="Key"></param>
        /// <returns></returns>
        public string GetString(string FeedName, string Key)
        {
            string sql = @"
                select value from appconfig
                where 1=1
                and feedname = @FeedName
                and [key] = @Key";

            string result = Database.QueryFirstOrDefault<string>(sql, new
            {
                FeedName = FeedName,
                Key = Key
            });

            return result;
        }

        /// <summary>
        /// Gets a lookup table from the database
        /// </summary>
        /// <param name="FeedName"></param>
        /// <param name="LookupName"></param>
        /// <returns></returns>
        public Dictionary<string, string> GetLookupTable(string FeedName, string LookupName)
        {
            string sql = @"
                select [key], value
                  from lookup
                 where feedname = @FeedName
                   and lookupname = @LookupName
                   and void = 0";

            Dictionary<string, string> result = Database.GetDictionary<string, string>(sql, new
            {
                FeedName = FeedName,
                LookupName = LookupName
            });

            return result;
        }

        /// <summary>
        /// Saves a lookup value to the database
        /// </summary>
        /// <param name="FeedName"></param>
        /// <param name="LookupName"></param>
        /// <param name="Key"></param>
        /// <param name="Value"></param>
        /// <param name="Description"></param>
        public void SetLookupValue(string FeedName, string LookupName, string Key, string Value, string Description)
        {
            string sql = @"
                UPDATE lookup
                   SET value    = @Value, OpNumber = 'DATAFEED'
                WHERE feedname = @FeedName
                AND lookupname = @LookupName
                AND [key]      = @Key
                AND Void       = 0
                if @@rowcount = 0
                begin
                    insert into lookup (feedname, lookupname, [Key], value, description, void, opnumber, timestamp)
                	VALUES (@FeedName, @LookupName, @Key, @Value, @Description, 0, 'DATAFEED', getdate())
                end";

            Database.Execute(sql, new
            {
                FeedName = FeedName,
                LookupName = LookupName,
                Key = Key,
                Value = Value,
                Description = Description
            });
        }

    }
}

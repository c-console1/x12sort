using DataLayer.Base;
using System;

namespace DataLayer.CaseManagementConfiguration
{
    /// <summary>
    /// Logical Database for the Credentialing System
    /// </summary>
    public class CaseManagementConfigurationDatabase : BaseDatabase
    {
        public ConfigurationRepository Configuration { get; private set; }

        internal CaseManagementConfigurationDatabase(IDbExec dbExec) : base(dbExec)
        {
            Configuration = new ConfigurationRepository(this);
        }

        internal CaseManagementConfigurationDatabase(params IExecutor[] executors) : base(executors)
        {
            Configuration = new ConfigurationRepository(this);
        }
    }

}

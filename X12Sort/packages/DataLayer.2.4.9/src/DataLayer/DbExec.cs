using Dapper;
using DataLayer.Configuration.Models;
using DataLayer.StaticUtilities;
using DataLayer.StaticUtilities.Extensions;
using NLog;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Threading.Tasks;
using static Dapper.SqlMapper;

namespace DataLayer
{
    /// <summary>
    /// Database Executor Implementation. Wrapper around <c> IDbConnection </c> and <c> dapper </c> to provider an easily mock-able database connection
    /// </summary>
    /// <seealso cref="SqlMapper"/>
    /// <seealso cref="IDbConnection"/>
    public class DbExec : IDisposable, IDbExec
    {
        private IDbConnection _connection;
        private Logger _logger;

        /// <summary>
        /// Name of the executor
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The logical database that this executor belongs to
        /// </summary>
        public string DatabaseGroup { get; private set; }

        /// <summary>
        /// The live connection to the database
        /// </summary>
        internal IDbConnection Connection
        {
            get
            {
                if (ConnectionAutoOpen && _connection.State == ConnectionState.Closed)
                {
                    _connection.Open();
                }

                return _connection;
            }
        }

        /// <summary>
        /// Internal toggle that can be used to prevent the connection from being automatically opened. The main idea of this is for testing so we can access the Connection object without trying to connect to an actual database.
        /// </summary>
        internal bool ConnectionAutoOpen { get; set; } = true;

        /// <summary>
        /// The type of database that is being connected too
        /// </summary>
        /// <value> The provider type that was set when the object instantiated </value>
        public ProviderType Provider { get; private set; }

        /// <summary>
        /// Creates a new DbExec from a Connection String Configuration Object
        /// </summary>
        /// <param name="configDbExec">         The DbExec for the configuration database, needed to get the password </param>
        /// <param name="connectionStringItem"> The connection string object from the data layer config </param>
        internal DbExec(DbExec configDbExec, ConfigConnectionString connectionStringItem)
        {
            configDbExec.ThrowIfNull(nameof(configDbExec));
            connectionStringItem.ThrowIfNull(nameof(connectionStringItem));

            Name = connectionStringItem.Name.ToUpper();
            DatabaseGroup = connectionStringItem.DatabaseGroup;
            Provider = connectionStringItem.Provider;
            _logger = LogManager.GetCurrentClassLogger();
            _connection = _createConnection(_cloneWithPassword(configDbExec, connectionStringItem));
        }

        /// <summary>
        /// Create a new DbExec using a connection string
        /// </summary>
        /// <param name="provider">         The type of provider, i.e. Oracle or sql </param>
        /// <param name="connectionString"> The full connection string </param>
        public DbExec(ProviderType provider, string connectionString)
        {
            Provider = provider;
            _logger = LogManager.GetCurrentClassLogger();
            _connection = _createConnection(_toBuilder(connectionString));
        }

        #region Query Functions

        /// <summary>
        /// Query the database, returns list of objects
        /// </summary>
        /// <typeparam name="TReturn"> The type of object you want a list of </typeparam>
        /// <param name="sql">        The query to run </param>
        /// <param name="parameters"> Optional object of parameters for the query </param>
        /// <returns> Uses Dapper to returns an enumerable list of object from the database </returns>
        public IEnumerable<TReturn> Query<TReturn>(string sql, object parameters)
        {
            try
            {
                parameters = parameters ?? new { };
                IEnumerable<TReturn> raw = Connection.Query<TReturn>(sql, parameters);
                return raw;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Gets the first value from the database, throw if none are found
        /// </summary>
        /// <typeparam name="TReturn"> The type of value you a getting </typeparam>
        /// <param name="sql">        The query to run </param>
        /// <param name="parameters"> Optional object of parameters for the query </param>
        /// <returns> Uses Dapper to return a new instance of the given type that is populated from the database </returns>
        public TReturn QueryFirst<TReturn>(string sql, object parameters)
        {
            try
            {
                parameters = parameters ?? new { };
                TReturn raw = Connection.QueryFirst<TReturn>(sql, parameters);
                return raw;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Gets the first value from the database, return null if not found
        /// </summary>
        /// <typeparam name="TReturn"> The type of value you a getting </typeparam>
        /// <param name="sql">        The query to run </param>
        /// <param name="parameters"> Optional object of parameters for the query </param>
        /// <returns> Uses Dapper to return a new instance of the given type that is populated from the database </returns>
        public TReturn QueryFirstOrDefault<TReturn>(string sql, object parameters)
        {
            try
            {
                parameters = parameters ?? new { };
                TReturn raw = Connection.QueryFirstOrDefault<TReturn>(sql, parameters);
                return raw;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Execute any arbitrary query on database
        /// </summary>
        /// <param name="sql">        The query to run </param>
        /// <param name="parameters"> Optional object of parameters for the query </param>
        /// <returns> The number of row effected </returns>
        public int Execute(string sql, object parameters)
        {
            try
            {
                parameters = parameters ?? new { };
                int raw = Connection.Execute(sql, parameters);
                return raw;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Gets a single value from the database
        /// </summary>
        /// <typeparam name="TReturn"> The type of value you a getting </typeparam>
        /// <param name="sql">        The query to run </param>
        /// <param name="parameters"> Optional object of parameters for the query </param>
        /// <returns> Uses Dapper to return a single value, usually string or int </returns>
        public TReturn ExecuteScalar<TReturn>(string sql, object parameters)
        {
            try
            {
                parameters = parameters ?? new { };
                TReturn raw = Connection.ExecuteScalar<TReturn>(sql, parameters);
                return raw;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Opens a data read for the query to stream data
        /// </summary>
        /// <param name="sql">        The query to run </param>
        /// <param name="parameters"> Optional object of parameters for the query </param>
        /// <returns> Uses Dapper to return a reader object for the query </returns>
        public IDataReader GetReader(string sql, object parameters)
        {
            IDataReader reader = Connection.ExecuteReader(sql, parameters);
            return reader;
        }

        #endregion Query Functions

        /// <summary>
        /// Creates a new `IDbCommand` object that can be used for more advanced quering
        /// </summary>
        /// <returns>IDbCommand that corresponed to the connection type</returns>
        public IDbCommand CreateCommand()
        {
            return Connection.CreateCommand();
        }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns> A string that represents the current object. </returns>
        public override string ToString()
        {
            return $"DbExec: {Connection.Database} [{Connection.State}] - {DatabaseGroup}:{Name}";
        }

        /// <summary>
        /// Creates the IDbConnection and opens the connection to the database
        /// </summary>
        /// <param name="builder"> The connection string builder to create a connection for </param>
        /// <returns>  </returns>
        private IDbConnection _createConnection(DbConnectionStringBuilder builder)
        {
            builder.ThrowIfNull(nameof(builder));

            // oracle doesn't has a catalog :(
            string catalog = builder.ContainsKey("Initial Catalog") ? builder["Initial Catalog"].ToString() : "";
            _logger.Info($"Creating {Provider} Connection to {builder["Data Source"]}:{ catalog}");

            string connectionString = builder.ConnectionString;
            IDbConnection connection;
            switch (Provider)
            {
                case ProviderType.Oracle:
                    connection = new OracleConnection(connectionString);
                    break;

                case ProviderType.Sql:
                    connection = new SqlConnection(connectionString);
                    break;

                default:
                    throw new NotSupportedException($"Provider Type: {Provider.ToString()} not defined");
            }
            return connection;
        }

        /// <summary>
        /// Creates the proper DbConnectionStringBuilder from the given connection string
        /// </summary>
        /// <param name="connectionString"> The connection string to create a connection for </param>
        /// <returns>  </returns>
        private DbConnectionStringBuilder _toBuilder(string connectionString)
        {
            connectionString.ThrowIfNullOrEmpty(nameof(connectionString));

            DbConnectionStringBuilder builder;
            switch (Provider)
            {
                case ProviderType.Oracle:
                    builder = new OracleConnectionStringBuilder(connectionString);
                    break;

                case ProviderType.Sql:
                    builder = new SqlConnectionStringBuilder(connectionString);
                    break;

                default:
                    throw new NotSupportedException($"Provider Type: {Provider.ToString()} not defined");
            }
            return builder;
        }

        private DbConnectionStringBuilder _cloneWithPassword(DbExec configDbExec, ConfigConnectionString configCxnStr)
        {
            DbConnectionStringBuilder clone = configCxnStr.Clone();
            clone["Password"] = configDbExec.Connection.QueryFirstOrDefault<string>(@"
EXECUTE AS USER = 'intrabit'
SELECT [das].fn_GetSecurityKey(@ConnectionID);
REVERT;
", new
            {
                configCxnStr.ConnectionID
            });
            return clone;
        }

        #region Dapper Functions

        /// <summary>
        /// Execute parameterized SQL.
        /// </summary>
        /// <param name="sql">            The SQL to execute for this query. </param>
        /// <param name="param">          The parameters to use for this query. </param>
        /// <param name="transaction">    The transaction to use for this query. </param>
        /// <param name="commandTimeout"> Number of seconds before command execution timeout. </param>
        /// <param name="commandType">    Is it a stored proc or a batch? </param>
        /// <returns> The number of rows affected. </returns>
        public int Execute(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.Execute(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute parameterized SQL that selects a single value.
        /// </summary>
        /// <param name="sql">            The SQL to execute. </param>
        /// <param name="param">          The parameters to use for this command. </param>
        /// <param name="transaction">    The transaction to use for this command. </param>
        /// <param name="commandTimeout"> Number of seconds before command execution timeout. </param>
        /// <param name="commandType">    Is it a stored proc or a batch? </param>
        /// <returns> The first cell selected as <see cref="object"/>. </returns>
        public object ExecuteScalar(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.ExecuteScalar(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute parameterized SQL that selects a single value.
        /// </summary>
        /// <typeparam name="T"> The type to return. </typeparam>
        /// <param name="sql">            The SQL to execute. </param>
        /// <param name="param">          The parameters to use for this command. </param>
        /// <param name="transaction">    The transaction to use for this command. </param>
        /// <param name="commandTimeout"> Number of seconds before command execution timeout. </param>
        /// <param name="commandType">    Is it a stored proc or a batch? </param>
        /// <returns> The first cell returned, as <typeparamref name="T"/>. </returns>
        public T ExecuteScalar<T>(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.ExecuteScalar<T>(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute parameterized SQL and return an <see cref="IDataReader"/>.
        /// </summary>
        /// <param name="sql">            The SQL to execute. </param>
        /// <param name="param">          The parameters to use for this command. </param>
        /// <param name="transaction">    The transaction to use for this command. </param>
        /// <param name="commandTimeout"> Number of seconds before command execution timeout. </param>
        /// <param name="commandType">    Is it a stored proc or a batch? </param>
        /// <returns> An <see cref="IDataReader"/> that can be used to iterate over the results of the SQL query. </returns>
        /// <remarks> This is typically used when the results of a query are not processed by Dapper, for example, used to fill a <see cref="DataTable"/> or <see cref="T:DataSet"/>. </remarks>
        /// <example>
        /// <code>
        /// <![CDATA[
        /// DataTable table = new DataTable("MyTable");
        /// using (var reader = ExecuteReader(cnn, sql, param))
        /// {
        /// table.Load(reader);
        /// }
        /// ]]>
        /// </code>
        /// </example>
        public IDataReader ExecuteReader(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.ExecuteReader(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Return a sequence of dynamic objects with properties matching the columns.
        /// </summary>
        /// <param name="sql">            The SQL to execute for the query. </param>
        /// <param name="param">          The parameters to pass, if any. </param>
        /// <param name="transaction">    The transaction to use, if any. </param>
        /// <param name="buffered">       Whether to buffer the results in memory. </param>
        /// <param name="commandTimeout"> The command timeout (in seconds). </param>
        /// <param name="commandType">    The type of command to execute. </param>
        /// <remarks> Note: each row can be accessed via "dynamic", or by casting to an IDictionary&lt;string,object&gt; </remarks>
        public IEnumerable<dynamic> Query(string sql, object param = null, IDbTransaction transaction = null, bool buffered = true, int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.Query<dynamic>(sql, param, transaction, buffered, commandTimeout, commandType);

        /// <summary>
        /// Return a dynamic object with properties matching the columns.
        /// </summary>
        /// <param name="sql">            The SQL to execute for the query. </param>
        /// <param name="param">          The parameters to pass, if any. </param>
        /// <param name="transaction">    The transaction to use, if any. </param>
        /// <param name="commandTimeout"> The command timeout (in seconds). </param>
        /// <param name="commandType">    The type of command to execute. </param>
        /// <remarks> Note: the row can be accessed via "dynamic", or by casting to an IDictionary&lt;string,object&gt; </remarks>
        public dynamic QueryFirst(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.QueryFirst<dynamic>(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Return a dynamic object with properties matching the columns.
        /// </summary>
        /// <param name="sql">            The SQL to execute for the query. </param>
        /// <param name="param">          The parameters to pass, if any. </param>
        /// <param name="transaction">    The transaction to use, if any. </param>
        /// <param name="commandTimeout"> The command timeout (in seconds). </param>
        /// <param name="commandType">    The type of command to execute. </param>
        /// <remarks> Note: the row can be accessed via "dynamic", or by casting to an IDictionary&lt;string,object&gt; </remarks>
        public dynamic QueryFirstOrDefault(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.QueryFirstOrDefault<dynamic>(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Return a dynamic object with properties matching the columns.
        /// </summary>
        /// <param name="sql">            The SQL to execute for the query. </param>
        /// <param name="param">          The parameters to pass, if any. </param>
        /// <param name="transaction">    The transaction to use, if any. </param>
        /// <param name="commandTimeout"> The command timeout (in seconds). </param>
        /// <param name="commandType">    The type of command to execute. </param>
        /// <remarks> Note: the row can be accessed via "dynamic", or by casting to an IDictionary&lt;string,object&gt; </remarks>
        public dynamic QuerySingle(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.QuerySingle<dynamic>(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Return a dynamic object with properties matching the columns.
        /// </summary>
        /// <param name="sql">            The SQL to execute for the query. </param>
        /// <param name="param">          The parameters to pass, if any. </param>
        /// <param name="transaction">    The transaction to use, if any. </param>
        /// <param name="commandTimeout"> The command timeout (in seconds). </param>
        /// <param name="commandType">    The type of command to execute. </param>
        /// <remarks> Note: the row can be accessed via "dynamic", or by casting to an IDictionary&lt;string,object&gt; </remarks>
        public dynamic QuerySingleOrDefault(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.QuerySingleOrDefault<dynamic>(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Executes a query, returning the data typed as <typeparamref name="T"/>.
        /// </summary>
        /// <typeparam name="T"> The type of results to return. </typeparam>
        /// <param name="sql">            The SQL to execute for the query. </param>
        /// <param name="param">          The parameters to pass, if any. </param>
        /// <param name="transaction">    The transaction to use, if any. </param>
        /// <param name="buffered">       Whether to buffer results in memory. </param>
        /// <param name="commandTimeout"> The command timeout (in seconds). </param>
        /// <param name="commandType">    The type of command to execute. </param>
        /// <returns>
        /// A sequence of data of the supplied type; if a basic type (int, string, etc) is queried then the data from the first column in assumed, otherwise an instance is created per row, and a direct column-name===member-name mapping is assumed (case insensitive).
        /// </returns>
        public IEnumerable<T> Query<T>(string sql, object param = null, IDbTransaction transaction = null, bool buffered = true, int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.Query<T>(sql, param, transaction, buffered, commandTimeout, commandType);

        /// <summary>
        /// Executes a single-row query, returning the data typed as <typeparamref name="T"/>.
        /// </summary>
        /// <typeparam name="T"> The type of result to return. </typeparam>
        /// <param name="sql">            The SQL to execute for the query. </param>
        /// <param name="param">          The parameters to pass, if any. </param>
        /// <param name="transaction">    The transaction to use, if any. </param>
        /// <param name="commandTimeout"> The command timeout (in seconds). </param>
        /// <param name="commandType">    The type of command to execute. </param>
        /// <returns>
        /// A sequence of data of the supplied type; if a basic type (int, string, etc) is queried then the data from the first column in assumed, otherwise an instance is created per row, and a direct column-name===member-name mapping is assumed (case insensitive).
        /// </returns>
        public T QueryFirst<T>(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.QueryFirst<T>(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Executes a single-row query, returning the data typed as <typeparamref name="T"/>.
        /// </summary>
        /// <typeparam name="T"> The type of result to return. </typeparam>
        /// <param name="sql">            The SQL to execute for the query. </param>
        /// <param name="param">          The parameters to pass, if any. </param>
        /// <param name="transaction">    The transaction to use, if any. </param>
        /// <param name="commandTimeout"> The command timeout (in seconds). </param>
        /// <param name="commandType">    The type of command to execute. </param>
        /// <returns>
        /// A sequence of data of the supplied type; if a basic type (int, string, etc) is queried then the data from the first column in assumed, otherwise an instance is created per row, and a direct column-name===member-name mapping is assumed (case insensitive).
        /// </returns>
        public T QueryFirstOrDefault<T>(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.QueryFirstOrDefault<T>(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Executes a single-row query, returning the data typed as <typeparamref name="T"/>.
        /// </summary>
        /// <typeparam name="T"> The type of result to return. </typeparam>
        /// <param name="sql">            The SQL to execute for the query. </param>
        /// <param name="param">          The parameters to pass, if any. </param>
        /// <param name="transaction">    The transaction to use, if any. </param>
        /// <param name="commandTimeout"> The command timeout (in seconds). </param>
        /// <param name="commandType">    The type of command to execute. </param>
        /// <returns>
        /// A sequence of data of the supplied type; if a basic type (int, string, etc) is queried then the data from the first column in assumed, otherwise an instance is created per row, and a direct column-name===member-name mapping is assumed (case insensitive).
        /// </returns>
        public T QuerySingle<T>(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.QuerySingle<T>(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Executes a single-row query, returning the data typed as <typeparamref name="T"/>.
        /// </summary>
        /// <typeparam name="T"> The type of result to return. </typeparam>
        /// <param name="sql">            The SQL to execute for the query. </param>
        /// <param name="param">          The parameters to pass, if any. </param>
        /// <param name="transaction">    The transaction to use, if any. </param>
        /// <param name="commandTimeout"> The command timeout (in seconds). </param>
        /// <param name="commandType">    The type of command to execute. </param>
        /// <returns>
        /// A sequence of data of the supplied type; if a basic type (int, string, etc) is queried then the data from the first column in assumed, otherwise an instance is created per row, and a direct column-name===member-name mapping is assumed (case insensitive).
        /// </returns>
        public T QuerySingleOrDefault<T>(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.QuerySingleOrDefault<T>(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Executes a single-row query, returning the data typed as <paramref name="type"/>.
        /// </summary>
        /// <param name="type">           The type to return. </param>
        /// <param name="sql">            The SQL to execute for the query. </param>
        /// <param name="param">          The parameters to pass, if any. </param>
        /// <param name="transaction">    The transaction to use, if any. </param>
        /// <param name="buffered">       Whether to buffer results in memory. </param>
        /// <param name="commandTimeout"> The command timeout (in seconds). </param>
        /// <param name="commandType">    The type of command to execute. </param>
        /// <exception cref="ArgumentNullException"> <paramref name="type"/> is <c> null </c>. </exception>
        /// <returns>
        /// A sequence of data of the supplied type; if a basic type (int, string, etc) is queried then the data from the first column in assumed, otherwise an instance is created per row, and a direct column-name===member-name mapping is assumed (case insensitive).
        /// </returns>
        public IEnumerable<object> Query(Type type, string sql, object param = null, IDbTransaction transaction = null, bool buffered = true, int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.Query(type, sql, param, transaction, buffered, commandTimeout, commandType);

        /// <summary>
        /// Executes a single-row query, returning the data typed as <paramref name="type"/>.
        /// </summary>
        /// <param name="type">           The type to return. </param>
        /// <param name="sql">            The SQL to execute for the query. </param>
        /// <param name="param">          The parameters to pass, if any. </param>
        /// <param name="transaction">    The transaction to use, if any. </param>
        /// <param name="commandTimeout"> The command timeout (in seconds). </param>
        /// <param name="commandType">    The type of command to execute. </param>
        /// <exception cref="ArgumentNullException"> <paramref name="type"/> is <c> null </c>. </exception>
        /// <returns>
        /// A sequence of data of the supplied type; if a basic type (int, string, etc) is queried then the data from the first column in assumed, otherwise an instance is created per row, and a direct column-name===member-name mapping is assumed (case insensitive).
        /// </returns>
        public object QueryFirst(Type type, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.QueryFirst(type, sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Executes a single-row query, returning the data typed as <paramref name="type"/>.
        /// </summary>
        /// <param name="type">           The type to return. </param>
        /// <param name="sql">            The SQL to execute for the query. </param>
        /// <param name="param">          The parameters to pass, if any. </param>
        /// <param name="transaction">    The transaction to use, if any. </param>
        /// <param name="commandTimeout"> The command timeout (in seconds). </param>
        /// <param name="commandType">    The type of command to execute. </param>
        /// <exception cref="ArgumentNullException"> <paramref name="type"/> is <c> null </c>. </exception>
        /// <returns>
        /// A sequence of data of the supplied type; if a basic type (int, string, etc) is queried then the data from the first column in assumed, otherwise an instance is created per row, and a direct column-name===member-name mapping is assumed (case insensitive).
        /// </returns>
        public object QueryFirstOrDefault(Type type, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.QueryFirstOrDefault(type, sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Executes a single-row query, returning the data typed as <paramref name="type"/>.
        /// </summary>
        /// <param name="type">           The type to return. </param>
        /// <param name="sql">            The SQL to execute for the query. </param>
        /// <param name="param">          The parameters to pass, if any. </param>
        /// <param name="transaction">    The transaction to use, if any. </param>
        /// <param name="commandTimeout"> The command timeout (in seconds). </param>
        /// <param name="commandType">    The type of command to execute. </param>
        /// <exception cref="ArgumentNullException"> <paramref name="type"/> is <c> null </c>. </exception>
        /// <returns>
        /// A sequence of data of the supplied type; if a basic type (int, string, etc) is queried then the data from the first column in assumed, otherwise an instance is created per row, and a direct column-name===member-name mapping is assumed (case insensitive).
        /// </returns>
        public object QuerySingle(Type type, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.QuerySingle(type, sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Executes a single-row query, returning the data typed as <paramref name="type"/>.
        /// </summary>
        /// <param name="type">           The type to return. </param>
        /// <param name="sql">            The SQL to execute for the query. </param>
        /// <param name="param">          The parameters to pass, if any. </param>
        /// <param name="transaction">    The transaction to use, if any. </param>
        /// <param name="commandTimeout"> The command timeout (in seconds). </param>
        /// <param name="commandType">    The type of command to execute. </param>
        /// <exception cref="ArgumentNullException"> <paramref name="type"/> is <c> null </c>. </exception>
        /// <returns>
        /// A sequence of data of the supplied type; if a basic type (int, string, etc) is queried then the data from the first column in assumed, otherwise an instance is created per row, and a direct column-name===member-name mapping is assumed (case insensitive).
        /// </returns>
        public object QuerySingleOrDefault(Type type, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.QuerySingleOrDefault(type, sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute a command that returns multiple result sets, and access each in turn.
        /// </summary>
        /// <param name="sql">            The SQL to execute for this query. </param>
        /// <param name="param">          The parameters to use for this query. </param>
        /// <param name="transaction">    The transaction to use for this query. </param>
        /// <param name="commandTimeout"> Number of seconds before command execution timeout. </param>
        /// <param name="commandType">    Is it a stored proc or a batch? </param>
        public IDbGridReader QueryMultiple(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            new DbGridReader(Connection.QueryMultiple(sql, param, transaction, commandTimeout, commandType));

        /// <summary>
        /// Perform a multi-mapping query with 2 input types. This returns a single type, combined from the raw types via <paramref name="map"/>.
        /// </summary>
        /// <typeparam name="TFirst"> The first type in the recordset. </typeparam>
        /// <typeparam name="TSecond"> The second type in the recordset. </typeparam>
        /// <typeparam name="TReturn"> The combined type to return. </typeparam>
        /// <param name="sql">            The SQL to execute for this query. </param>
        /// <param name="map">            The function to map row types to the return type. </param>
        /// <param name="param">          The parameters to use for this query. </param>
        /// <param name="transaction">    The transaction to use for this query. </param>
        /// <param name="buffered">       Whether to buffer the results in memory. </param>
        /// <param name="splitOn">        The field we should split and read the second object from (default: "Id"). </param>
        /// <param name="commandTimeout"> Number of seconds before command execution timeout. </param>
        /// <param name="commandType">    Is it a stored proc or a batch? </param>
        /// <returns> An enumerable of <typeparamref name="TReturn"/>. </returns>
        public IEnumerable<TReturn> Query<TFirst, TSecond, TReturn>(string sql, Func<TFirst, TSecond, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.Query(sql, map, param, transaction, buffered, splitOn, commandTimeout, commandType);

        /// <summary>
        /// Perform a multi-mapping query with 3 input types. This returns a single type, combined from the raw types via <paramref name="map"/>.
        /// </summary>
        /// <typeparam name="TFirst"> The first type in the recordset. </typeparam>
        /// <typeparam name="TSecond"> The second type in the recordset. </typeparam>
        /// <typeparam name="TThird"> The third type in the recordset. </typeparam>
        /// <typeparam name="TReturn"> The combined type to return. </typeparam>
        /// <param name="sql">            The SQL to execute for this query. </param>
        /// <param name="map">            The function to map row types to the return type. </param>
        /// <param name="param">          The parameters to use for this query. </param>
        /// <param name="transaction">    The transaction to use for this query. </param>
        /// <param name="buffered">       Whether to buffer the results in memory. </param>
        /// <param name="splitOn">        The field we should split and read the second object from (default: "Id"). </param>
        /// <param name="commandTimeout"> Number of seconds before command execution timeout. </param>
        /// <param name="commandType">    Is it a stored proc or a batch? </param>
        /// <returns> An enumerable of <typeparamref name="TReturn"/>. </returns>
        public IEnumerable<TReturn> Query<TFirst, TSecond, TThird, TReturn>(string sql, Func<TFirst, TSecond, TThird, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.Query(sql, map, param, transaction, buffered, splitOn, commandTimeout, commandType);

        /// <summary>
        /// Perform a multi-mapping query with 4 input types. This returns a single type, combined from the raw types via <paramref name="map"/>.
        /// </summary>
        /// <typeparam name="TFirst"> The first type in the recordset. </typeparam>
        /// <typeparam name="TSecond"> The second type in the recordset. </typeparam>
        /// <typeparam name="TThird"> The third type in the recordset. </typeparam>
        /// <typeparam name="TFourth"> The fourth type in the recordset. </typeparam>
        /// <typeparam name="TReturn"> The combined type to return. </typeparam>
        /// <param name="sql">            The SQL to execute for this query. </param>
        /// <param name="map">            The function to map row types to the return type. </param>
        /// <param name="param">          The parameters to use for this query. </param>
        /// <param name="transaction">    The transaction to use for this query. </param>
        /// <param name="buffered">       Whether to buffer the results in memory. </param>
        /// <param name="splitOn">        The field we should split and read the second object from (default: "Id"). </param>
        /// <param name="commandTimeout"> Number of seconds before command execution timeout. </param>
        /// <param name="commandType">    Is it a stored proc or a batch? </param>
        /// <returns> An enumerable of <typeparamref name="TReturn"/>. </returns>
        public IEnumerable<TReturn> Query<TFirst, TSecond, TThird, TFourth, TReturn>(string sql, Func<TFirst, TSecond, TThird, TFourth, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.Query(sql, map, param, transaction, buffered, splitOn, commandTimeout, commandType);

        /// <summary>
        /// Perform a multi-mapping query with 5 input types. This returns a single type, combined from the raw types via <paramref name="map"/>.
        /// </summary>
        /// <typeparam name="TFirst"> The first type in the recordset. </typeparam>
        /// <typeparam name="TSecond"> The second type in the recordset. </typeparam>
        /// <typeparam name="TThird"> The third type in the recordset. </typeparam>
        /// <typeparam name="TFourth"> The fourth type in the recordset. </typeparam>
        /// <typeparam name="TFifth"> The fifth type in the recordset. </typeparam>
        /// <typeparam name="TReturn"> The combined type to return. </typeparam>
        /// <param name="sql">            The SQL to execute for this query. </param>
        /// <param name="map">            The function to map row types to the return type. </param>
        /// <param name="param">          The parameters to use for this query. </param>
        /// <param name="transaction">    The transaction to use for this query. </param>
        /// <param name="buffered">       Whether to buffer the results in memory. </param>
        /// <param name="splitOn">        The field we should split and read the second object from (default: "Id"). </param>
        /// <param name="commandTimeout"> Number of seconds before command execution timeout. </param>
        /// <param name="commandType">    Is it a stored proc or a batch? </param>
        /// <returns> An enumerable of <typeparamref name="TReturn"/>. </returns>
        public IEnumerable<TReturn> Query<TFirst, TSecond, TThird, TFourth, TFifth, TReturn>(string sql, Func<TFirst, TSecond, TThird, TFourth, TFifth, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.Query(sql, map, param, transaction, buffered, splitOn, commandTimeout, commandType);

        /// <summary>
        /// Perform a multi-mapping query with 6 input types. This returns a single type, combined from the raw types via <paramref name="map"/>.
        /// </summary>
        /// <typeparam name="TFirst"> The first type in the recordset. </typeparam>
        /// <typeparam name="TSecond"> The second type in the recordset. </typeparam>
        /// <typeparam name="TThird"> The third type in the recordset. </typeparam>
        /// <typeparam name="TFourth"> The fourth type in the recordset. </typeparam>
        /// <typeparam name="TFifth"> The fifth type in the recordset. </typeparam>
        /// <typeparam name="TSixth"> The sixth type in the recordset. </typeparam>
        /// <typeparam name="TReturn"> The combined type to return. </typeparam>
        /// <param name="sql">            The SQL to execute for this query. </param>
        /// <param name="map">            The function to map row types to the return type. </param>
        /// <param name="param">          The parameters to use for this query. </param>
        /// <param name="transaction">    The transaction to use for this query. </param>
        /// <param name="buffered">       Whether to buffer the results in memory. </param>
        /// <param name="splitOn">        The field we should split and read the second object from (default: "Id"). </param>
        /// <param name="commandTimeout"> Number of seconds before command execution timeout. </param>
        /// <param name="commandType">    Is it a stored proc or a batch? </param>
        /// <returns> An enumerable of <typeparamref name="TReturn"/>. </returns>
        public IEnumerable<TReturn> Query<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TReturn>(string sql, Func<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.Query(sql, map, param, transaction, buffered, splitOn, commandTimeout, commandType);

        /// <summary>
        /// Perform a multi-mapping query with 7 input types. If you need more types -&gt; use Query with Type[] parameter. This returns a single type, combined from the raw types via <paramref name="map"/>.
        /// </summary>
        /// <typeparam name="TFirst"> The first type in the recordset. </typeparam>
        /// <typeparam name="TSecond"> The second type in the recordset. </typeparam>
        /// <typeparam name="TThird"> The third type in the recordset. </typeparam>
        /// <typeparam name="TFourth"> The fourth type in the recordset. </typeparam>
        /// <typeparam name="TFifth"> The fifth type in the recordset. </typeparam>
        /// <typeparam name="TSixth"> The sixth type in the recordset. </typeparam>
        /// <typeparam name="TSeventh"> The seventh type in the recordset. </typeparam>
        /// <typeparam name="TReturn"> The combined type to return. </typeparam>
        /// <param name="sql">            The SQL to execute for this query. </param>
        /// <param name="map">            The function to map row types to the return type. </param>
        /// <param name="param">          The parameters to use for this query. </param>
        /// <param name="transaction">    The transaction to use for this query. </param>
        /// <param name="buffered">       Whether to buffer the results in memory. </param>
        /// <param name="splitOn">        The field we should split and read the second object from (default: "Id"). </param>
        /// <param name="commandTimeout"> Number of seconds before command execution timeout. </param>
        /// <param name="commandType">    Is it a stored proc or a batch? </param>
        /// <returns> An enumerable of <typeparamref name="TReturn"/>. </returns>
        public IEnumerable<TReturn> Query<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh, TReturn>(string sql, Func<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.Query(sql, map, param, transaction, buffered, splitOn, commandTimeout, commandType);

        /// <summary>
        /// Perform a multi-mapping query with an arbitrary number of input types. This returns a single type, combined from the raw types via <paramref name="map"/>.
        /// </summary>
        /// <typeparam name="TReturn"> The combined type to return. </typeparam>
        /// <param name="sql">            The SQL to execute for this query. </param>
        /// <param name="types">          Array of types in the recordset. </param>
        /// <param name="map">            The function to map row types to the return type. </param>
        /// <param name="param">          The parameters to use for this query. </param>
        /// <param name="transaction">    The transaction to use for this query. </param>
        /// <param name="buffered">       Whether to buffer the results in memory. </param>
        /// <param name="splitOn">        The field we should split and read the second object from (default: "Id"). </param>
        /// <param name="commandTimeout"> Number of seconds before command execution timeout. </param>
        /// <param name="commandType">    Is it a stored proc or a batch? </param>
        /// <returns> An enumerable of <typeparamref name="TReturn"/>. </returns>
        public IEnumerable<TReturn> Query<TReturn>(string sql, Type[] types, Func<object[], TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.Query(sql, types, map, param, transaction, buffered, splitOn, commandTimeout, commandType);

        #endregion Dapper Functions

        #region IDisposable Support

        private bool disposedValue = false; // To detect redundant calls

        private void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (_connection != null)
                    {
                        _connection.Close();
                        _connection.Dispose();
                    }
                }
                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }

        #endregion IDisposable Support

        #region Static Functions

        /// <summary>
        /// Static function that creates a new <c> DbExec </c> from the app settings
        /// </summary>
        /// <remarks> The configuration is automatically loaded by from the <c> DataAccessConfigBuilder </c> class. The connection string for the AppConfig database is used from here. </remarks>
        /// <returns> A DbExec instance that is connected to the AppConfig database </returns>
        /// <example>
        /// <code>
        /// <![CDATA[
        /// IDbExec appConfig = DbExec.Create();
        /// ]]>
        /// </code>
        /// </example>
        /// <seealso cref="DataAccessConfigBuilder"/>
        /// <seealso cref="DataAccess"/>
        public static DbExec Create()
        {
            string cxnStr = DataAccessConfigBuilder.Instance.Config.AppConfigConnectionString;
            if (string.IsNullOrEmpty(cxnStr))
            {
                throw new InvalidOperationException("No AppConfigConnectionString value found");
            }
            return new DbExec(ProviderType.Sql, cxnStr) { Name = "AppConfigConnectionString", DatabaseGroup = "AppConfigConnectionString" };
        }

        /// <summary>
        /// Static function that creates a new <c> DbExec </c> from the app settings using the connection key
        /// </summary>
        /// <remarks> The configuration is automatically loaded by from the <c> DataAccessConfigBuilder </c> class. The connection string is must be one of the keys in the `Connection` dictionary. </remarks>
        /// <returns> A DbExec instance that is connected to the AppConfig database </returns>
        /// <example>
        /// <code>
        /// <![CDATA[
        /// IDbExec appConfig = DbExec.Create("prod");
        /// ]]>
        /// </code>
        /// </example>
        /// <seealso cref="DataAccessConfigBuilder"/>
        /// <seealso cref="DataAccessConfig.Connections"/>
        /// <seealso cref="DataAccess"/>
        public static DbExec Create(string connectionKey)
        {
            connectionKey.ThrowIfNullOrEmpty(nameof(connectionKey));

            if (DataAccessConfigBuilder.Instance.Config.Connections.TryGetValue(connectionKey, out string cxnStr))
            {
                return new DbExec(ProviderType.Sql, cxnStr) { Name = connectionKey, DatabaseGroup = "AppConfigConnectionString" };
            }
            else
            {
                throw new InvalidOperationException($"No connection string found for key: '{connectionKey}'");
            }
        }

        #endregion Static Functions

        #region Async Dapper  Functions

        /// <summary>
        /// Execute a query asynchronously using Task.
        /// </summary>
        /// <param name="sql">The SQL to execute for the query.</param>
        /// <param name="param">The parameters to pass, if any.</param>
        /// <param name="transaction">The transaction to use, if any.</param>
        /// <param name="commandTimeout">The command timeout (in seconds).</param>
        /// <param name="commandType">The type of command to execute.</param>
        /// <remarks>Note: each row can be accessed via "dynamic", or by casting to an IDictionary&lt;string,object&gt;</remarks>
        public Task<IEnumerable<dynamic>> QueryAsync(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.QueryAsync(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute a query asynchronously using Task.
        /// </summary>
        /// <typeparam name="T">The type of results to return.</typeparam>
        /// <param name="sql">The SQL to execute for the query.</param>
        /// <param name="param">The parameters to pass, if any.</param>
        /// <param name="transaction">The transaction to use, if any.</param>
        /// <param name="commandTimeout">The command timeout (in seconds).</param>
        /// <param name="commandType">The type of command to execute.</param>
        /// <returns>
        /// A sequence of data of <typeparamref name="T"/>; if a basic type (int, string, etc) is queried then the data from the first column in assumed, otherwise an instance is
        /// created per row, and a direct column-name===member-name mapping is assumed (case insensitive).
        /// </returns>
        public Task<IEnumerable<T>> QueryAsync<T>(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
             Connection.QueryAsync<T>(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute a single-row query asynchronously using Task.
        /// </summary>
        /// <typeparam name="T">The type of result to return.</typeparam>
        /// <param name="sql">The SQL to execute for the query.</param>
        /// <param name="param">The parameters to pass, if any.</param>
        /// <param name="transaction">The transaction to use, if any.</param>
        /// <param name="commandTimeout">The command timeout (in seconds).</param>
        /// <param name="commandType">The type of command to execute.</param>
        public Task<T> QueryFirstAsync<T>(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
             Connection.QueryFirstAsync<T>(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute a single-row query asynchronously using Task.
        /// </summary>
        /// <typeparam name="T">The type of result to return.</typeparam>
        /// <param name="sql">The SQL to execute for the query.</param>
        /// <param name="param">The parameters to pass, if any.</param>
        /// <param name="transaction">The transaction to use, if any.</param>
        /// <param name="commandTimeout">The command timeout (in seconds).</param>
        /// <param name="commandType">The type of command to execute.</param>
        public Task<T> QueryFirstOrDefaultAsync<T>(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.QueryFirstOrDefaultAsync<T>(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute a single-row query asynchronously using Task.
        /// </summary>
        /// <typeparam name="T">The type of result to return.</typeparam>
        /// <param name="sql">The SQL to execute for the query.</param>
        /// <param name="param">The parameters to pass, if any.</param>
        /// <param name="transaction">The transaction to use, if any.</param>
        /// <param name="commandTimeout">The command timeout (in seconds).</param>
        /// <param name="commandType">The type of command to execute.</param>
        public Task<T> QuerySingleAsync<T>(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.QuerySingleAsync<T>(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute a single-row query asynchronously using Task.
        /// </summary>
        /// <typeparam name="T">The type to return.</typeparam>
        /// <param name="sql">The SQL to execute for the query.</param>
        /// <param name="param">The parameters to pass, if any.</param>
        /// <param name="transaction">The transaction to use, if any.</param>
        /// <param name="commandTimeout">The command timeout (in seconds).</param>
        /// <param name="commandType">The type of command to execute.</param>
        public Task<T> QuerySingleOrDefaultAsync<T>(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.QuerySingleOrDefaultAsync<T>(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute a single-row query asynchronously using Task.
        /// </summary>
        /// <param name="sql">The SQL to execute for the query.</param>
        /// <param name="param">The parameters to pass, if any.</param>
        /// <param name="transaction">The transaction to use, if any.</param>
        /// <param name="commandTimeout">The command timeout (in seconds).</param>
        /// <param name="commandType">The type of command to execute.</param>
        public Task<dynamic> QueryFirstAsync(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.QueryFirstAsync(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute a single-row query asynchronously using Task.
        /// </summary>
        /// <param name="sql">The SQL to execute for the query.</param>
        /// <param name="param">The parameters to pass, if any.</param>
        /// <param name="transaction">The transaction to use, if any.</param>
        /// <param name="commandTimeout">The command timeout (in seconds).</param>
        /// <param name="commandType">The type of command to execute.</param>
        public Task<dynamic> QueryFirstOrDefaultAsync(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.QueryFirstOrDefaultAsync(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute a single-row query asynchronously using Task.
        /// </summary>
        /// <param name="sql">The SQL to execute for the query.</param>
        /// <param name="param">The parameters to pass, if any.</param>
        /// <param name="transaction">The transaction to use, if any.</param>
        /// <param name="commandTimeout">The command timeout (in seconds).</param>
        /// <param name="commandType">The type of command to execute.</param>
        public Task<dynamic> QuerySingleAsync(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.QuerySingleAsync(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute a single-row query asynchronously using Task.
        /// </summary>
        /// <param name="sql">The SQL to execute for the query.</param>
        /// <param name="param">The parameters to pass, if any.</param>
        /// <param name="transaction">The transaction to use, if any.</param>
        /// <param name="commandTimeout">The command timeout (in seconds).</param>
        /// <param name="commandType">The type of command to execute.</param>
        public Task<dynamic> QuerySingleOrDefaultAsync(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.QuerySingleOrDefault(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute a query asynchronously using Task.
        /// </summary>
        /// <param name="type">The type to return.</param>
        /// <param name="sql">The SQL to execute for the query.</param>
        /// <param name="param">The parameters to pass, if any.</param>
        /// <param name="transaction">The transaction to use, if any.</param>
        /// <param name="commandTimeout">The command timeout (in seconds).</param>
        /// <param name="commandType">The type of command to execute.</param>
        /// <exception cref="ArgumentNullException"><paramref name="type"/> is <c>null</c>.</exception>
        public Task<IEnumerable<object>> QueryAsync(Type type, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
             Connection.QueryAsync(type, sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute a single-row query asynchronously using Task.
        /// </summary>
        /// <param name="type">The type to return.</param>
        /// <param name="sql">The SQL to execute for the query.</param>
        /// <param name="param">The parameters to pass, if any.</param>
        /// <param name="transaction">The transaction to use, if any.</param>
        /// <param name="commandTimeout">The command timeout (in seconds).</param>
        /// <param name="commandType">The type of command to execute.</param>
        /// <exception cref="ArgumentNullException"><paramref name="type"/> is <c>null</c>.</exception>
        public Task<object> QueryFirstAsync(Type type, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.QueryFirstAsync(type, sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute a single-row query asynchronously using Task.
        /// </summary>
        /// <param name="type">The type to return.</param>
        /// <param name="sql">The SQL to execute for the query.</param>
        /// <param name="param">The parameters to pass, if any.</param>
        /// <param name="transaction">The transaction to use, if any.</param>
        /// <param name="commandTimeout">The command timeout (in seconds).</param>
        /// <param name="commandType">The type of command to execute.</param>
        /// <exception cref="ArgumentNullException"><paramref name="type"/> is <c>null</c>.</exception>
        public Task<object> QueryFirstOrDefaultAsync(Type type, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.QueryFirstOrDefaultAsync(type, sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute a single-row query asynchronously using Task.
        /// </summary>
        /// <param name="type">The type to return.</param>
        /// <param name="sql">The SQL to execute for the query.</param>
        /// <param name="param">The parameters to pass, if any.</param>
        /// <param name="transaction">The transaction to use, if any.</param>
        /// <param name="commandTimeout">The command timeout (in seconds).</param>
        /// <param name="commandType">The type of command to execute.</param>
        /// <exception cref="ArgumentNullException"><paramref name="type"/> is <c>null</c>.</exception>
        public Task<object> QuerySingleAsync(Type type, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.QuerySingleAsync(type, sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute a single-row query asynchronously using Task.
        /// </summary>
        /// <param name="type">The type to return.</param>
        /// <param name="sql">The SQL to execute for the query.</param>
        /// <param name="param">The parameters to pass, if any.</param>
        /// <param name="transaction">The transaction to use, if any.</param>
        /// <param name="commandTimeout">The command timeout (in seconds).</param>
        /// <param name="commandType">The type of command to execute.</param>
        /// <exception cref="ArgumentNullException"><paramref name="type"/> is <c>null</c>.</exception>
        public Task<object> QuerySingleOrDefaultAsync(Type type, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.QuerySingleOrDefaultAsync(type, sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute a command asynchronously using Task.
        /// </summary>
        /// <param name="sql">The SQL to execute for this query.</param>
        /// <param name="param">The parameters to use for this query.</param>
        /// <param name="transaction">The transaction to use for this query.</param>
        /// <param name="commandTimeout">Number of seconds before command execution timeout.</param>
        /// <param name="commandType">Is it a stored proc or a batch?</param>
        /// <returns>The number of rows affected.</returns>
        public Task<int> ExecuteAsync(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
             Connection.ExecuteAsync(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Perform a asynchronous multi-mapping query with 2 input types.
        /// This returns a single type, combined from the raw types via <paramref name="map"/>.
        /// </summary>
        /// <typeparam name="TFirst">The first type in the recordset.</typeparam>
        /// <typeparam name="TSecond">The second type in the recordset.</typeparam>
        /// <typeparam name="TReturn">The combined type to return.</typeparam>
        /// <param name="sql">The SQL to execute for this query.</param>
        /// <param name="map">The function to map row types to the return type.</param>
        /// <param name="param">The parameters to use for this query.</param>
        /// <param name="transaction">The transaction to use for this query.</param>
        /// <param name="buffered">Whether to buffer the results in memory.</param>
        /// <param name="splitOn">The field we should split and read the second object from (default: "Id").</param>
        /// <param name="commandTimeout">Number of seconds before command execution timeout.</param>
        /// <param name="commandType">Is it a stored proc or a batch?</param>
        /// <returns>An enumerable of <typeparamref name="TReturn"/>.</returns>
        public Task<IEnumerable<TReturn>> QueryAsync<TFirst, TSecond, TReturn>(string sql, Func<TFirst, TSecond, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.QueryAsync(sql, map, param, transaction, buffered, splitOn, commandTimeout, commandType);

        /// <summary>
        /// Perform a asynchronous multi-mapping query with 3 input types.
        /// This returns a single type, combined from the raw types via <paramref name="map"/>.
        /// </summary>
        /// <typeparam name="TFirst">The first type in the recordset.</typeparam>
        /// <typeparam name="TSecond">The second type in the recordset.</typeparam>
        /// <typeparam name="TThird">The third type in the recordset.</typeparam>
        /// <typeparam name="TReturn">The combined type to return.</typeparam>
        /// <param name="sql">The SQL to execute for this query.</param>
        /// <param name="map">The function to map row types to the return type.</param>
        /// <param name="param">The parameters to use for this query.</param>
        /// <param name="transaction">The transaction to use for this query.</param>
        /// <param name="buffered">Whether to buffer the results in memory.</param>
        /// <param name="splitOn">The field we should split and read the second object from (default: "Id").</param>
        /// <param name="commandTimeout">Number of seconds before command execution timeout.</param>
        /// <param name="commandType">Is it a stored proc or a batch?</param>
        /// <returns>An enumerable of <typeparamref name="TReturn"/>.</returns>
        public Task<IEnumerable<TReturn>> QueryAsync<TFirst, TSecond, TThird, TReturn>(string sql, Func<TFirst, TSecond, TThird, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.QueryAsync(sql, map, param, transaction, buffered, splitOn, commandTimeout, commandType);

        /// <summary>
        /// Perform a asynchronous multi-mapping query with 4 input types.
        /// This returns a single type, combined from the raw types via <paramref name="map"/>.
        /// </summary>
        /// <typeparam name="TFirst">The first type in the recordset.</typeparam>
        /// <typeparam name="TSecond">The second type in the recordset.</typeparam>
        /// <typeparam name="TThird">The third type in the recordset.</typeparam>
        /// <typeparam name="TFourth">The fourth type in the recordset.</typeparam>
        /// <typeparam name="TReturn">The combined type to return.</typeparam>
        /// <param name="sql">The SQL to execute for this query.</param>
        /// <param name="map">The function to map row types to the return type.</param>
        /// <param name="param">The parameters to use for this query.</param>
        /// <param name="transaction">The transaction to use for this query.</param>
        /// <param name="buffered">Whether to buffer the results in memory.</param>
        /// <param name="splitOn">The field we should split and read the second object from (default: "Id").</param>
        /// <param name="commandTimeout">Number of seconds before command execution timeout.</param>
        /// <param name="commandType">Is it a stored proc or a batch?</param>
        /// <returns>An enumerable of <typeparamref name="TReturn"/>.</returns>
        public Task<IEnumerable<TReturn>> QueryAsync<TFirst, TSecond, TThird, TFourth, TReturn>(string sql, Func<TFirst, TSecond, TThird, TFourth, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.QueryAsync(sql, map, param, transaction, buffered, splitOn, commandTimeout, commandType);

        /// <summary>
        /// Perform a asynchronous multi-mapping query with 5 input types.
        /// This returns a single type, combined from the raw types via <paramref name="map"/>.
        /// </summary>
        /// <typeparam name="TFirst">The first type in the recordset.</typeparam>
        /// <typeparam name="TSecond">The second type in the recordset.</typeparam>
        /// <typeparam name="TThird">The third type in the recordset.</typeparam>
        /// <typeparam name="TFourth">The fourth type in the recordset.</typeparam>
        /// <typeparam name="TFifth">The fifth type in the recordset.</typeparam>
        /// <typeparam name="TReturn">The combined type to return.</typeparam>
        /// <param name="sql">The SQL to execute for this query.</param>
        /// <param name="map">The function to map row types to the return type.</param>
        /// <param name="param">The parameters to use for this query.</param>
        /// <param name="transaction">The transaction to use for this query.</param>
        /// <param name="buffered">Whether to buffer the results in memory.</param>
        /// <param name="splitOn">The field we should split and read the second object from (default: "Id").</param>
        /// <param name="commandTimeout">Number of seconds before command execution timeout.</param>
        /// <param name="commandType">Is it a stored proc or a batch?</param>
        /// <returns>An enumerable of <typeparamref name="TReturn"/>.</returns>
        public Task<IEnumerable<TReturn>> QueryAsync<TFirst, TSecond, TThird, TFourth, TFifth, TReturn>(string sql, Func<TFirst, TSecond, TThird, TFourth, TFifth, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.QueryAsync(sql, map, param, transaction, buffered, splitOn, commandTimeout, commandType);

        /// <summary>
        /// Perform a asynchronous multi-mapping query with 6 input types.
        /// This returns a single type, combined from the raw types via <paramref name="map"/>.
        /// </summary>
        /// <typeparam name="TFirst">The first type in the recordset.</typeparam>
        /// <typeparam name="TSecond">The second type in the recordset.</typeparam>
        /// <typeparam name="TThird">The third type in the recordset.</typeparam>
        /// <typeparam name="TFourth">The fourth type in the recordset.</typeparam>
        /// <typeparam name="TFifth">The fifth type in the recordset.</typeparam>
        /// <typeparam name="TSixth">The sixth type in the recordset.</typeparam>
        /// <typeparam name="TReturn">The combined type to return.</typeparam>
        /// <param name="sql">The SQL to execute for this query.</param>
        /// <param name="map">The function to map row types to the return type.</param>
        /// <param name="param">The parameters to use for this query.</param>
        /// <param name="transaction">The transaction to use for this query.</param>
        /// <param name="buffered">Whether to buffer the results in memory.</param>
        /// <param name="splitOn">The field we should split and read the second object from (default: "Id").</param>
        /// <param name="commandTimeout">Number of seconds before command execution timeout.</param>
        /// <param name="commandType">Is it a stored proc or a batch?</param>
        /// <returns>An enumerable of <typeparamref name="TReturn"/>.</returns>
        public Task<IEnumerable<TReturn>> QueryAsync<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TReturn>(string sql, Func<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.QueryAsync(sql, map, param, transaction, buffered, splitOn, commandTimeout, commandType);

        /// <summary>
        /// Perform a asynchronous multi-mapping query with 7 input types.
        /// This returns a single type, combined from the raw types via <paramref name="map"/>.
        /// </summary>
        /// <typeparam name="TFirst">The first type in the recordset.</typeparam>
        /// <typeparam name="TSecond">The second type in the recordset.</typeparam>
        /// <typeparam name="TThird">The third type in the recordset.</typeparam>
        /// <typeparam name="TFourth">The fourth type in the recordset.</typeparam>
        /// <typeparam name="TFifth">The fifth type in the recordset.</typeparam>
        /// <typeparam name="TSixth">The sixth type in the recordset.</typeparam>
        /// <typeparam name="TSeventh">The seventh type in the recordset.</typeparam>
        /// <typeparam name="TReturn">The combined type to return.</typeparam>
        /// <param name="sql">The SQL to execute for this query.</param>
        /// <param name="map">The function to map row types to the return type.</param>
        /// <param name="param">The parameters to use for this query.</param>
        /// <param name="transaction">The transaction to use for this query.</param>
        /// <param name="buffered">Whether to buffer the results in memory.</param>
        /// <param name="splitOn">The field we should split and read the second object from (default: "Id").</param>
        /// <param name="commandTimeout">Number of seconds before command execution timeout.</param>
        /// <param name="commandType">Is it a stored proc or a batch?</param>
        /// <returns>An enumerable of <typeparamref name="TReturn"/>.</returns>
        public Task<IEnumerable<TReturn>> QueryAsync<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh, TReturn>(string sql, Func<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.QueryAsync(sql, map, param, transaction, buffered, splitOn, commandTimeout, commandType);

        /// <summary>
        /// Perform a asynchronous multi-mapping query with an arbitrary number of input types.
        /// This returns a single type, combined from the raw types via <paramref name="map"/>.
        /// </summary>
        /// <typeparam name="TReturn">The combined type to return.</typeparam
        /// <param name="sql">The SQL to execute for this query.</param>
        /// <param name="types">Array of types in the recordset.</param>
        /// <param name="map">The function to map row types to the return type.</param>
        /// <param name="param">The parameters to use for this query.</param>
        /// <param name="transaction">The transaction to use for this query.</param>
        /// <param name="buffered">Whether to buffer the results in memory.</param>
        /// <param name="splitOn">The field we should split and read the second object from (default: "Id").</param>
        /// <param name="commandTimeout">Number of seconds before command execution timeout.</param>
        /// <param name="commandType">Is it a stored proc or a batch?</param>
        /// <returns>An enumerable of <typeparamref name="TReturn"/>.</returns>
        public Task<IEnumerable<TReturn>> QueryAsync<TReturn>(string sql, Type[] types, Func<object[], TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.QueryAsync(sql, types, map, param, transaction, buffered, splitOn, commandTimeout, commandType);

        /// <summary>
        /// Execute a command that returns multiple result sets, and access each in turn.
        /// </summary>
        /// <param name="sql">The SQL to execute for this query.</param>
        /// <param name="param">The parameters to use for this query.</param>
        /// <param name="transaction">The transaction to use for this query.</param>
        /// <param name="commandTimeout">Number of seconds before command execution timeout.</param>
        /// <param name="commandType">Is it a stored proc or a batch?</param>
        public async Task<IDbGridReader> QueryMultipleAsync(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
        new DbGridReader(await Connection.QueryMultipleAsync(sql, param, transaction, commandTimeout, commandType));

        /// <summary>
        /// Execute parameterized SQL and return an <see cref="IDataReader"/>.
        /// </summary>
        /// <param name="sql">The SQL to execute.</param>
        /// <param name="param">The parameters to use for this command.</param>
        /// <param name="transaction">The transaction to use for this command.</param>
        /// <param name="commandTimeout">Number of seconds before command execution timeout.</param>
        /// <param name="commandType">Is it a stored proc or a batch?</param>
        /// <returns>An <see cref="IDataReader"/> that can be used to iterate over the results of the SQL query.</returns>
        /// <remarks>
        /// This is typically used when the results of a query are not processed by Dapper, for example, used to fill a <see cref="DataTable"/>
        /// or <see cref="T:DataSet"/>.
        /// </remarks>
        /// <example>
        /// <code>
        /// <![CDATA[
        /// DataTable table = new DataTable("MyTable") =>
        /// using (var reader = ExecuteReader(cnn, sql, param))
        /// {
        ///     table.Load(reader) =>
        /// }
        /// ]]>
        /// </code>
        /// </example>
        public Task<IDataReader> ExecuteReaderAsync(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
             Connection.ExecuteReaderAsync(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute parameterized SQL that selects a single value.
        /// </summary>
        /// <param name="sql">The SQL to execute.</param>
        /// <param name="param">The parameters to use for this command.</param>
        /// <param name="transaction">The transaction to use for this command.</param>
        /// <param name="commandTimeout">Number of seconds before command execution timeout.</param>
        /// <param name="commandType">Is it a stored proc or a batch?</param>
        /// <returns>The first cell returned, as <see cref="object"/>.</returns>
        public Task<object> ExecuteScalarAsync(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.ExecuteScalarAsync(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute parameterized SQL that selects a single value.
        /// </summary>
        /// <typeparam name="T">The type to return.</typeparam>
        /// <param name="sql">The SQL to execute.</param>
        /// <param name="param">The parameters to use for this command.</param>
        /// <param name="transaction">The transaction to use for this command.</param>
        /// <param name="commandTimeout">Number of seconds before command execution timeout.</param>
        /// <param name="commandType">Is it a stored proc or a batch?</param>
        /// <returns>The first cell returned, as <typeparamref name="T"/>.</returns>
        public Task<T> ExecuteScalarAsync<T>(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            Connection.ExecuteScalarAsync<T>(sql, param, transaction, commandTimeout, commandType);

        #endregion Async Dapper  Functions
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace DataLayer
{
    /// <summary>
    /// Basic class that implements a SOAP request that has all the info a single request
    /// </summary>
    /// <typeparam name="TRequest">The soap request body without SOAP envelope</typeparam>
    /// <typeparam name="TResponse">The soap response body without SOAP envelope</typeparam>
    public class SoapRequest<TRequest, TResponse> : ISoapRequest<TRequest, TResponse> where TRequest : class where TResponse : class
    {
        public static readonly XmlNamespaceManager DefaultManager;

        static SoapRequest()
        {
            DefaultManager = new XmlNamespaceManager(new NameTable());
            DefaultManager.AddNamespace("S", "http://schemas.xmlsoap.org/soap/envelope/");
        }

        /// <summary>
        /// The endpoint to be added to the URL for this request
        /// </summary>
        public string EndPoint { get; }

        /// <summary>
        /// The soap request body without SOAP envelope
        /// </summary>
        public TRequest Body { get; set; }

        /// <summary>
        /// Namespace manager for serializing the XML
        /// </summary>
        IXmlNamespaceResolver ISoapRequest<TRequest, TResponse>.NamespaceManager => NamespaceManager;

        /// <summary>
        /// Namespace manager for serializing the XML
        /// </summary>
        /// <remarks>
        /// This is a shared static object which may be a bad idea. Can be set to new object
        /// </remarks>
        public XmlNamespaceManager NamespaceManager { get; set; }

        public SoapRequest(string endpoint)
        {
            EndPoint = endpoint;
            NamespaceManager = DefaultManager;
        }
    }
}

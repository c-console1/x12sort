using DataLayer.Base;
using System;

namespace DataLayer.Utility
{
    /// <summary>
    /// A generic utility database. This database is not tried to any specific database currently, but it for a place holder for general information that is convenient to store in a database.
    /// </summary>
    public class UtilityDatabase : BaseDatabase
    {
        internal UtilityDatabase(IDbExec dbExec, IApiExec apiExec) : base(dbExec)
        {
        }

        internal UtilityDatabase(params IExecutor[] executors) : base(executors)
        {
        }
    }
}

using System.Xml;

namespace DataLayer
{
    /// <summary>
    /// Wrapper around a SOAP request that has all the info a single request
    /// </summary>
    /// <typeparam name="TRequest">The soap request body without SOAP envelope</typeparam>
    /// <typeparam name="TResponse">The soap response body without SOAP envelope</typeparam>
    public interface ISoapRequest<TRequest, TResponse>
        where TRequest : class
        where TResponse : class
    {
        /// <summary>
        /// The endpoint to be added to the URL for this request
        /// </summary>
        string EndPoint { get; }

        /// <summary>
        /// The soap request body without SOAP envelope
        /// </summary>
        TRequest Body { get; set; }

        /// <summary>
        /// Namespace manager for serializing the XML
        /// </summary>
        IXmlNamespaceResolver NamespaceManager { get; }
    }
}

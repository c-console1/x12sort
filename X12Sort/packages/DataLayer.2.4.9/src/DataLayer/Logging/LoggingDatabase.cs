using DataLayer.Base;
using DataLayer.Configuration.Models;
using DataLayer.Logging.Models;
using DataLayer.StaticUtilities;
using DataLayer.StaticUtilities.Extensions;
using NLog;
using System;

namespace DataLayer.Logging
{
    /// <summary>
    /// Logical Database for the logging database
    /// </summary>
    public class LoggingDatabase : BaseDatabase
    {
        private Guid _instanceId;
        internal readonly string AppStartLabel = "ApplicationStart";
        internal readonly string AppCompleteLabel = "ApplicationEnd";
        internal readonly string AppUnexpectedEndLabel = "UnexpectedEnd";

        /// <summary>
        /// The current logging instance
        /// </summary>
        public Guid InstanceId { get { return _instanceId; } set { _instanceId = value; } }

        internal LoggingDatabase(IDbExec dbExec) : base(dbExec)
        {
            _instanceId = Guid.Empty;
        }

        internal LoggingDatabase(params IExecutor[] executors) : base(executors)
        {
            _instanceId = Guid.Empty;
        }
        

        /// <summary>
        /// Logs a data point to the database. Use async to run fire and forget task
        /// </summary>
        /// <param name="label"> The label description the value being logged </param>
        /// <param name="value"> Object being logged </param>
        public void Add(string label, dynamic value)
        {
            label.ThrowIfNullOrEmpty(nameof(label));

            if (_instanceId == Guid.Empty)
            {
                throw new InvalidOperationException("Logging Instance has not been initialized. Please create a New Instance");
            }
            LogDataPoint view = new LogDataPoint(InstanceId, label, value);

            Logger.Info($"Logging Data Point:  {view.Label} = {value?.ToString()}");
            try
            {
                DbExec.Execute(view.CreateQuery(), view);
            }
            catch (Exception e)
            {
                Logger.Error(e);
                Logger.Warn("Logging Error... Continuing Application");
                if(DataAccessConfigBuilder.Instance.Config.ThrowLoggingDatabaseExceptions)
                {
                    throw e;
                }
                // else ignoring exception
            }
        }

        /// <summary>
        /// Creates a New Logging instance for the giving application
        /// </summary>
        /// <param name="machineApp"> Machine Application Instance </param>
        public Guid NewInstance(ConfigMachineApplication machineApp)
        {
            machineApp.ThrowIfNull(nameof(machineApp));

            LogInstance instance = new LogInstance()
            {
                MachineName = machineApp.Machine.MachineName,
                ApplicationName = machineApp.ApplicationName,
                ApplicationType = machineApp.ApplicationType
            };
            _instanceId = ExecuteScalar<Guid>(instance.CreateQuery(), instance);
            Logger.Info($"New Logging Instance: {_instanceId} for {instance.ApplicationName} on {instance.MachineName}");
            Add(AppStartLabel, DateTime.Now);
            return _instanceId;
        }

        /// <summary>
        /// Adds the application end data point and removes reference to instance ID
        /// </summary>
        public void EndInstance()
        {
            Add(AppCompleteLabel, DateTime.Now);
            _instanceId = Guid.Empty;
        }

        /// <summary>
        /// Logs a message indicating the application was unexpectedly cleaned up (aka, EndInstance() wasn't called)
        /// </summary>
        internal void UnexpectedCleanup()
        {
            Logger.Warn("Application Was Not Cleaned Up!!!");
            Logger.Info("You should end your program with a `Logging.EndInstance()` so it is easy to see if the program exited unexpectedly.");
            //TODO: Handle this differently based on app type ???
            Add(AppUnexpectedEndLabel, DateTime.Now);
        }
    }
}

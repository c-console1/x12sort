using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// Key to the ALL_SUBSCRIPTION_HISTORY_FACT table.
		/// </summary>
		public class SUBSCRIBER_DIM{

			/// <summary>
			/// Key to the ALL_SUBSCRIPTION_HISTORY_FACT table.
			/// </summary>
			public Int64 SUBSCRIBER_DIM_KEY{ get; set; }

			/// <summary>
			/// Subscriber exchange application ID.
			/// </summary>
			public String APPLICATION_ID{ get; set; }

			/// <summary>
			/// Subscription HCC ID
			/// </summary>
			public String SUBSCRIPTION_HCC_ID{ get; set; }

			/// <summary>
			/// Subscription STATUS NAME
			/// </summary>
			public String SUBSCRIPTION_STATUS_NAME{ get; set; }

			/// <summary>
			/// Fhe history transaction COUNT for the exchange enrollment.
			/// </summary>
			public Int64 SUB_HISTORY_FACT_COUNT{ get; set; }

			/// <summary>
			/// The payment transaction ID for the exchange enrollment.
			/// </summary>
			public String APPLICATION_PAYMENT_TX_ID{ get; set; }

			/// <summary>
			/// The exchange rating area that is used to determine premium amounts.
			/// </summary>
			public String APPLICATION_RATING_AREA{ get; set; }

			/// <summary>
			/// Key to the AUDIT_LOG table.
			/// </summary>
			public Int64 AUDIT_LOG_KEY{ get; set; }

			/// <summary>
			/// Flag "Y' or "N"
			/// </summary>
			public String AUTO_WITHDRAWAL_ENABLED{ get; set; }

			/// <summary>
			/// 
			/// </summary>
			public String INFORMATION_SOURCE_TYPE_NAME{ get; set; }

			/// <summary>
			/// HCC ID Alloted to a PAYEE
			/// </summary>
			public String PAYEE_HCC_ID{ get; set; }

			/// <summary>
			/// Payment Type
			/// </summary>
			public String PAYMENT_TYPE_NAME{ get; set; }

			/// <summary>
			/// Withdrwl request Day
			/// </summary>
			public String REQUESTED_WITHDRAWAL_DAY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 TENANT_HCC_ID{ get; set; }

			/// <summary>
			/// Assigned workbasket item group.
			/// </summary>
			public String WB_GROUP{ get; set; }

			/// <summary>
			/// Assigned workbasket item owner.
			/// </summary>
			public String WB_OWNER{ get; set; }

			/// <summary>
			/// Original Effective Date
			/// </summary>
			public Int32 ORIGINAL_EFFECTIVE_DATE{ get; set; }

			/// <summary>
			/// Application Payment Date
			/// </summary>
			public Int32 APPLICATION_PAYMENT_DATE{ get; set; }

			/// <summary>
			/// Last Modified Date Time
			/// </summary>
			public DateTime LAST_MODIFIED_DATE_TIME{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// Key to the ALL_BILLING_ADJUSTMENT_FACT table.
		/// </summary>
		public class BILLING_ADJUSTMENT_FACT{

			/// <summary>
			/// Key to the ALL_BILLING_ADJUSTMENT_FACT table.
			/// </summary>
			public Int64 BILLING_ADJUSTMENT_FACT_KEY{ get; set; }

			/// <summary>
			/// Key to the ALL_ACCOUNT_HISTORY_FACT table.
			/// </summary>
			public Int64 ACCOUNT_DIM_KEY{ get; set; }

			/// <summary>
			/// Associated billing plan.
			/// </summary>
			public Int64 BENEFIT_PLAN_DIM_KEY{ get; set; }

			/// <summary>
			/// Member ID.
			/// </summary>
			public Int64 MEMBER_DIM_KEY{ get; set; }

			/// <summary>
			/// Funding request number. Key to the FUNDING_REQUEST_FACT table.
			/// </summary>
			public Int64 FUNDING_REQUEST_KEY{ get; set; }

			/// <summary>
			/// Reason code name (e.g., Settlement, Write-off, Discount).
			/// </summary>
			public String BILLING_ADJUSTMENT_REASON_NAME{ get; set; }

			/// <summary>
			/// Comment entered for the billing adjustment.
			/// </summary>
			public String BILLING_ADJUSTMENT_COMMENT{ get; set; }

			/// <summary>
			/// Name of billing category (e.g., Reinsurance, Premium).
			/// </summary>
			public String BILLING_CATEGORY_NAME{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Decimal BILLING_ADJUSTMENT_AMOUNT{ get; set; }

			/// <summary>
			/// Associated entry set.
			/// </summary>
			public Int64 ENTRY_SET_KEY{ get; set; }

			/// <summary>
			/// Indicates the billing adjustment is reconciled.
			/// </summary>
			public String IS_RECONCILED{ get; set; }

			/// <summary>
			/// Indicates the billing adjustment is for statistical billing only.
			/// </summary>
			public String IS_STATISTICAL{ get; set; }

			/// <summary>
			/// ID of person who last updated the billing adjustment.
			/// </summary>
			public String LAST_TX_USER{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 TENANT_HCC_ID{ get; set; }

			/// <summary>
			/// Transaction number assigned to the billing adjustment.
			/// </summary>
			public String TRANSACT_NUM{ get; set; }

			/// <summary>
			/// Indicates that line item reconciliation is turned on for this account and used for the billing adjustment.
			/// </summary>
			public String USES_LINE_ITEM_RECONCILIATION{ get; set; }

			/// <summary>
			/// Coverage date.
			/// </summary>
			public Int32 COVERAGE_DATE{ get; set; }

			/// <summary>
			/// Date the billing adjustment was processed.
			/// </summary>
			public Int32 PROCESS_DATE{ get; set; }

			/// <summary>
			/// Date of billing adjustment.
			/// </summary>
			public Int32 ADJUSTMENT_DATE{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// Primary key for PREMIUM_PAYMENT_FACT table. Auto genrated surrogate key.
		/// </summary>
		public class PREMIUM_PAYMENT_FACT{

			/// <summary>
			/// Primary key for PREMIUM_PAYMENT_FACT table. Auto genrated surrogate key.
			/// </summary>
			public Int64 PREMIUM_PAYMENT_FACT_KEY{ get; set; }

			/// <summary>
			/// 
			/// </summary>
			public Int64 SENT_FOR_BILL_FACT_KEY{ get; set; }

			/// <summary>
			/// Primary key for PREMIUM_PAYMENT_FACT table. Auto generated surrogate key.
			/// </summary>
			public Int64 ORIG_PREM_PAYMENT_FACT_KEY{ get; set; }

			/// <summary>
			/// The other bill recipient for which the premium payment is made. Null if payment is from account or subscription.
			/// </summary>
			public Int64 OTHER_BILL_RECIPIENT_FACT_KEY{ get; set; }

			/// <summary>
			/// Account Dimension key
			/// </summary>
			public Int64 ACCOUNT_DIM_KEY{ get; set; }

			/// <summary>
			/// Member Dimension key
			/// </summary>
			public Int64 MEMBER_DIM_KEY{ get; set; }

			/// <summary>
			/// Set when record originated from an EDI820 feed.
			/// </summary>
			public String TRANSACT_NUM{ get; set; }

			/// <summary>
			/// Check number.
			/// </summary>
			public String PAYMENT_NUMBER{ get; set; }

			/// <summary>
			/// Type of payment (Check, Wire, ACH). Joins to the PAYMENT_TYPE_CODE table.
			/// </summary>
			public String PAYMENT_TYPE_NAME{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 CARRIER_DIM_KEY{ get; set; }

			/// <summary>
			/// 
			/// </summary>
			public Int64 ENTRY_SET_KEY{ get; set; }

			/// <summary>
			/// 
			/// </summary>
			public Int64 FUNDING_REQUEST_FACT_KEY{ get; set; }

			/// <summary>
			/// Indicates if the payment is reconciled.
			/// </summary>
			public String IS_RECONCILED{ get; set; }

			/// <summary>
			/// Indicates a premium payment reversal of allocations, due to a void payment.
			/// </summary>
			public String IS_REVERSAL{ get; set; }

			/// <summary>
			/// Indicates a voided premium payment.
			/// </summary>
			public String IS_VOID{ get; set; }

			/// <summary>
			/// 
			/// </summary>
			public Int64 ATTACHMENT_SET_ID{ get; set; }

			/// <summary>
			/// Column for summing number of records in a query. No business application. Always has a value of 1.
			/// </summary>
			public Int64 PREMIUM_PAYMENT_FACT_COUNT{ get; set; }

			/// <summary>
			/// 
			/// </summary>
			public String RECEIVED_ALL_ROSTER_ENTRIES{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 TENANT_HCC_ID{ get; set; }

			/// <summary>
			/// Audit Log Key
			/// </summary>
			public Int64 AUDIT_LOG_KEY{ get; set; }

			/// <summary>
			/// Indicates if the premium payment is reconciled against a bill.
			/// </summary>
			public String USES_LINE_ITEM_RECONCILIATION{ get; set; }

			/// <summary>
			/// Key conecting the PREM_PAYMENT_VOID_REASON table.
			/// </summary>
			public String VOID_REASON_NAME{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 CREATE_DATE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 ALLOCATED_DATE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 PAYMENT_DATE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 RECEIPT_DATE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 PROCESS_DATE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 PAID_THROUGH_DATE{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
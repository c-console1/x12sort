using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// Primary key for BANK_ACCOUNT table. Auto-generated surrogate key.
		/// </summary>
		public class BANK_ACCOUNT_DETAILS{

			/// <summary>
			/// Primary key for BANK_ACCOUNT table. Auto-generated surrogate key.
			/// </summary>
			public Int64 BANK_ACCOUNT_KEY{ get; set; }

			/// <summary>
			/// HCC bank account name.
			/// </summary>
			public String ACCOUNT_NAME{ get; set; }

			/// <summary>
			/// Bank account number on generated check.
			/// </summary>
			public String ACCOUNT_NUMBER{ get; set; }

			/// <summary>
			/// Name of bank holding account.
			/// </summary>
			public String BANK_NAME{ get; set; }

			/// <summary>
			/// Bank routing number.
			/// </summary>
			public String ROUTING_NUMBER{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 TENANT_HCC_ID{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
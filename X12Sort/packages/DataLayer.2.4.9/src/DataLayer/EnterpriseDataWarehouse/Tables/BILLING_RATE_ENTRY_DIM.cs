using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// Billing rate Dimension key
		/// </summary>
		public class BILLING_RATE_DIM{

			/// <summary>
			/// Billing rate Dimension key
			/// </summary>
			public Int64 BILLING_RATE_DIM_KEY{ get; set; }

			/// <summary>
			/// Billing Rate Dimension key
			/// </summary>
			public Int64 BILLING_RATE_ENTRY_DIM_KEY{ get; set; }

			/// <summary>
			/// Account Dimension key
			/// </summary>
			public Int64 ACCOUNT_DIM_KEY{ get; set; }

			/// <summary>
			/// Benefit Plan Dimension key
			/// </summary>
			public Int64 BENEFIT_PLAN_DIM_KEY{ get; set; }

			/// <summary>
			/// Status (Active, Terminated, Unknown).
			/// </summary>
			public String BILLING_RATE_STATUS_NAME{ get; set; }

			/// <summary>
			/// Method that determines basis for billing rate: tier, salary, coverage, per member per month.
			/// </summary>
			public String BILLING_METHOD{ get; set; }

			/// <summary>
			/// Member gender.
			/// </summary>
			public String GENDER{ get; set; }

			/// <summary>
			/// Percentage of claim applied
			/// </summary>
			public Decimal PERC_OF_CLAIM_APPLIED{ get; set; }

			/// <summary>
			/// Rate applied
			/// </summary>
			public Decimal RATE_APPLIED{ get; set; }

			/// <summary>
			/// Tier name
			/// </summary>
			public String TIER_NAME{ get; set; }

			/// <summary>
			/// Tier Set name
			/// </summary>
			public String TIER_SET_NAME{ get; set; }

			/// <summary>
			/// Member age.
			/// </summary>
			public Int64 AGE{ get; set; }

			/// <summary>
			/// Name of billing category (e.g., Reinsurance, Premium).
			/// </summary>
			public String BILLING_CATEGORY_NAME{ get; set; }

			/// <summary>
			/// Billing rate fact count
			/// </summary>
			public Int64 BILL_RATE_HISTORY_FACT_COUNT{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 TENANT_HCC_ID{ get; set; }

			/// <summary>
			/// Audit log key
			/// </summary>
			public Int64 AUDIT_LOG_KEY{ get; set; }

			/// <summary>
			/// Billing Rate termination date
			/// </summary>
			public Int32 BILL_RATE_TERMINATION_DATE{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
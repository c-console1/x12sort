using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// Bill Fact Key
		/// </summary>
		public class BILL_FACT{

			/// <summary>
			/// Bill Fact Key
			/// </summary>
			public Int64 BILL_FACT_KEY{ get; set; }

			/// <summary>
			/// Account Dimension key
			/// </summary>
			public Int64 ACCOUNT_DIM_KEY{ get; set; }

			/// <summary>
			/// Subscription Dimension key
			/// </summary>
			public Int64 SUBSCRIBER_DIM_KEY{ get; set; }

			/// <summary>
			/// Bill status (e.g., Needs Review, Accepted, Rejected).
			/// </summary>
			public String BILL_STATUS_NAME{ get; set; }

			/// <summary>
			/// Unpaid balance carried to current billing period.
			/// </summary>
			public Decimal BILL_BALANCE_FORWARD{ get; set; }

			/// <summary>
			/// Premium charges for the current period.
			/// </summary>
			public Decimal BILL_PREMIUM_CHARGES{ get; set; }

			/// <summary>
			/// Subsidy for the current period.
			/// </summary>
			public Decimal BILL_SUBSIDY_CHARGES{ get; set; }

			/// <summary>
			/// Sum of all adjustments for bill.
			/// </summary>
			public Decimal BILL_TOTAL_ADJUSTMENTS{ get; set; }

			/// <summary>
			/// Total billed amount.
			/// </summary>
			public Decimal BILL_TOTAL_CHARGES{ get; set; }

			/// <summary>
			/// Total amount due for current bill.
			/// </summary>
			public Decimal BILL_TOTAL_DUE{ get; set; }

			/// <summary>
			/// Total amount of payments received against current bill.
			/// </summary>
			public Decimal BILL_TOTAL_PAYMENTS{ get; set; }

			/// <summary>
			/// Total Refunds on the Premium
			/// </summary>
			public Decimal BILL_TOTAL_PREMIUM_REFUNDS{ get; set; }

			/// <summary>
			/// Total Adjustments
			/// </summary>
			public Decimal BILL_TOTAL_STAT_ADJUSTMENT{ get; set; }

			/// <summary>
			/// Total Charges
			/// </summary>
			public Decimal BILL_TOTAL_STAT_CHARGE{ get; set; }

			/// <summary>
			/// Total of all voided bill payments.
			/// </summary>
			public Decimal BILL_TOTAL_VOID_PAYMENTS{ get; set; }

			/// <summary>
			/// Account for which the bill is rendered. In the case of a subscriber bill, it would be the parent account of the subscriber.
			/// </summary>
			public Int64 BILLED_PARTY_ACCOUNT_KEY{ get; set; }

			/// <summary>
			/// Type of Billed party (e.g., Account, subscription).
			/// </summary>
			public String BILLED_PARTY_TYPE{ get; set; }

			/// <summary>
			/// Bill cycle name.
			/// </summary>
			public String BILLING_CYCLE_NAME{ get; set; }

			/// <summary>
			/// Indicates if the bill is fully reconciled.
			/// </summary>
			public String IS_RECONCILED{ get; set; }

			/// <summary>
			/// Indicates if the bill is Voided
			/// </summary>
			public string IS_VOID{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 TENANT_HCC_ID{ get; set; }

			/// <summary>
			/// Indicates if the bill is reconciled against a premium payment using the Line Item Reconciliation feature enabled for the account.
			/// </summary>
			public String USES_LINE_ITEM_RECONCILIATION{ get; set; }

			/// <summary>
			/// Column for summing number of records in a query. No business application. Always has a value of 1.
			/// </summary>
			public Int64 BILL_FACT_COUNT{ get; set; }

			/// <summary>
			/// Bill Allocated Date Key
			/// </summary>
			public Int32 ALLOCATED_DATE{ get; set; }

			/// <summary>
			/// Bill Due Date
			/// </summary>
			public Int32 BILL_DUE_DATE{ get; set; }

			/// <summary>
			/// Date From which the bill is generated
			/// </summary>
			public Int32 BILL_FROM_DATE{ get; set; }

			/// <summary>
			/// Date when bill is sent
			/// </summary>
			public Int32 BILL_SENT_DATE{ get; set; }

			/// <summary>
			/// Date of the Bill Through
			/// </summary>
			public Int32 BILL_THROUGH_DATE{ get; set; }

			/// <summary>
			/// Date on which the bill is processed.
			/// </summary>
			public Int32 BILL_PROCESSING_DATE{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
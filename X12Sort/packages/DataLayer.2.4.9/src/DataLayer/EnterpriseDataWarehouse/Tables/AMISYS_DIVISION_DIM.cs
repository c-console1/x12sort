using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// IMAGE_RECNBR is used internally by Cobol routines.
		/// </summary>
		public class AMISYS_DIVISION_BENEFIT_PKG_DETAILS{

			/// <summary>
			/// IMAGE_RECNBR is used internally by Cobol routines.
			/// </summary>
			public Int32 IMAGE_RECNBR{ get; set; }

			/// <summary>
			/// Identification number of the employer division.
    /// 			Identification number assigned to employer group
    /// 			divisions. For ease of identification, the DIVISION#
    /// 			consists of GROUP#, X(6) plus a 4 character extension.
			/// </summary>
			public String DIVISION_NBR{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String GROUP_NBR{ get; set; }

			/// <summary>
			/// Alternate division id.
    /// 			An alternate division id used to create a cross
    /// 			to a higher level division/group/corp hierarchy.
			/// </summary>
			public String ALT_DIVISION{ get; set; }

			/// <summary>
			/// Identification number for usage external to AMISYS.
    /// 			The alternate ID is used to identify an entity for
    /// 			purposes external to AMISYS. For example, it could be
    /// 			the number by which Blue Cross identifies the Division.
    /// 			The label on the Division Screen can be overridden by
    /// 			a ''MELABEL'' SUPER-TABLE entry to be more descriptive of
    /// 			the usage within a health plan. The data item can also
    /// 			be used for data extraction.
			/// </summary>
			public String ALTERNATE_ID{ get; set; }

			/// <summary>
			/// [BC] Benefit Category.
    /// 			The Benefit Category is used to identify the category
    /// 			of benefits assigned to a Division. It can be used to
    /// 			identify the Benefit Package to an entity external to
    /// 			AMISYS.
			/// </summary>
			public String BENEFIT_CAT{ get; set; }

			/// <summary>
			/// The name of a general group of benefits.
    /// 			The name of the most general definition for benefits.
    /// 			A group of benefits form a component. They are
    /// 			linked together by CB-LINK (component/benefit link).
    /// 			A group of components form a rider. They are
    /// 			linked together by RC-LINK (rider/component link).
    /// 			A group of riders form a benefit package. They are
    /// 			linked together by BR-LINK (benefit/rider link).
			/// </summary>
			public String BENEFIT_PKG{ get; set; }

			/// <summary>
			/// [BK] Broker code.
    /// 			The Broker Code is used to identify the Broker assigned
    /// 			to a Division.
			/// </summary>
			public String BROKER{ get; set; }

			/// <summary>
			/// [DT] Division type.
    /// 			[DT] The division type categorizes divisions. It is
    /// 			used to vary pricing and to prohibit members who belong
    /// 			to certain divisions from receiving authorizations to
    /// 			non-participating providers.
			/// </summary>
			public String DIV_TYPE{ get; set; }

			/// <summary>
			/// Operator ID# of individual performing transaction update.
			/// </summary>
			public String OP_NBR{ get; set; }

			/// <summary>
			/// Defines composition of OTHCOVDEF for this division.
    /// 			
			/// </summary>
			public String OTHCOVKEY{ get; set; }

			/// <summary>
			/// The rate tier that a given contract type is a part of
    /// 			for Other Coverage in a Division.
			/// </summary>
			public String OTHCOVTIER{ get; set; }

			/// <summary>
			/// Assigns composition of the premium rates for this division.
			/// </summary>
			public String PREMKEY{ get; set; }

			/// <summary>
			/// [RY] Reason for data entry or modification.
			/// </summary>
			public String REASON{ get; set; }

			/// <summary>
			/// Defines which ALT-PROVIDER recs are auto created.
    /// 			Defines the capitation types (CAPTYPEs) for automatic
    /// 			creation of ALT-PROVIDER records. So, if the SPECKEY
    /// 			for a member''s division defines LAB and DRUG as
    /// 			CAPTYPEs, then alternate provider records for LAB and
    /// 			DRUG capitation will be automatically created for the
    /// 			member. SPECKEY values, with their associated
    /// 			capitation types, are defined in the SUPER-TABLE.
			/// </summary>
			public String SPECKEY{ get; set; }

			/// <summary>
			/// The rate tier that a given contract type is a part of.
			/// </summary>
			public String TIER{ get; set; }

			/// <summary>
			/// Transaction code.
    /// 			For AUTHORIZATION, ADMISSION, and AUTH-DETAIL,
    /// 			TRANSCODE position 1: A (add), C (change)
    /// 			position 2: B (batch), blank (online)
			/// </summary>
			public String TRANSCODE{ get; set; }

			/// <summary>
			/// A ''V'' in the first position indicates a voided record.
    /// 			A ''V'' in the first position indicates that the record
    /// 			has been voided (most likely due to errors). Voided
    /// 			records are generally excluded from validation scans.
    /// 			For the CLEARCK-TAPE record, the second position of
    /// 			VOID indicates the reason for the void.
			/// </summary>
			public String VOID{ get; set; }

			/// <summary>
			/// Effective or start date.
    /// 			For UB-92 this is From Date- date the occurrence starts
    /// 			The UB-92 from date is the date the occurrence is for.
    /// 			Occur date corresponds to the occur code event relating
    /// 			to this bill. This ''from date'' must not be later than
    /// 			a ''thru date'' when a thru date is entered.
    /// 			Refer to UB92 locator FIELD 32a-36a and 32b-36b
    /// 			of service. A one position, alpha numeric value. See
    /// 			UB92 FIELD locator 32 through 36 for value.
    /// 			For PROV-CON-ED, YMDEFF is the start date for the class
    /// 			or seminar.
			/// </summary>
			public Int32 YMDEFF{ get; set; }

			/// <summary>
			/// End date.
    /// 			For UB-92 this is Thru Date- date the occurrence ends.
    /// 			The UB-92 thru date is the date the occurrence is for.
    /// 			Occur date corresponds to the occur code event relating
    /// 			to this bill. This ''thru date'' must not be less than
    /// 			a ''from date'' when a thru date is entered.
    /// 			Refer to UB92 locator FIELDS 32a-36a and 32b-36b values
			/// </summary>
			public Int32 YMDEND{ get; set; }

			/// <summary>
			/// Date of most recent transaction.
			/// </summary>
			public Int32 YMDTRANS{ get; set; }

			/// <summary>
			/// Counter incremented via trigger after any updates.
    /// 			Used by the JAVA to duplicate the ''record has changed
    /// 			since you found it'' feature of AMISYS 3000 and
    /// 			AMISYS Advance Release 1 that is provided by the Cognos
    /// 			Quick screens.
			/// </summary>
			public Int64 CHECKSUM_VALUE{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
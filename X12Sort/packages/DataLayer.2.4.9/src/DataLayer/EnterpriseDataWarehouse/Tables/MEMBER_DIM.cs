using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// Primary key
		/// </summary>
		public class CROSSWALK_SUBSCRIBER{

			/// <summary>
			/// Primary key
			/// </summary>
			public Int64 CROSSWALK_SUBSCRIBER_KEY{ get; set; }

			/// <summary>
			/// This column is referring to the SUBSCRIBER_NBR column in Amisys SUBSCRIBER dimension
			/// </summary>
			public String AMISYS_SUBSCRIBER_ID{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 SUBSCRIBER_DIM_KEY{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
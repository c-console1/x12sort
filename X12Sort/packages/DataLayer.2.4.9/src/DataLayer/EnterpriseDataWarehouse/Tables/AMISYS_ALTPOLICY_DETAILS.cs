using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// IMAGE_RECNBR is used internally by Cobol routines
		/// </summary>
		public class AMISYS_ALTCARRIER_DIM{

			/// <summary>
			/// IMAGE_RECNBR is used internally by Cobol routines
			/// </summary>
			public Int64 IMAGE_RECNBR{ get; set; }

			/// <summary>
			/// Health plan carrier of alternate coverage for members.
    /// 			A unique identifier assigned by the AMISYS health plan
    /// 			to other health care carriers. These carriers provide
    /// 			alternate or additional coverage to the AMISYS members.
    /// 			For Stop Loss logic, ALTCARRIER# is the STOP LOSS
    /// 			carrier for the health plan.
			/// </summary>
			public String ALTCARRIER_NBR{ get; set; }

			/// <summary>
			/// Alternate carrier type.
    /// 			[AY] Alternate carrier type code set value. Used to
    /// 			categorize the carrier as medicare, workers comp,
    /// 			subrogation, no fault, etc.
			/// </summary>
			public String ALTCARTYPE{ get; set; }

			/// <summary>
			/// Indicator that alternate carrier coordinates benefits.
    /// 			This indicator describes whether or not an alternate
    /// 			carrier coordinates benefits.
			/// </summary>
			public String COBPROVISION{ get; set; }

			/// <summary>
			/// Customer representative to whom inquires are directed.
			/// </summary>
			public String CONTACT{ get; set; }

			/// <summary>
			/// Firstname + lastname, or company name.
    /// 			This is the complete name. If it is first + last
    /// 			names, then spaces are deleted, except to distinguish
    /// 			the separate words in a name.
    /// 			FULL-NAME of ADDRESS is the same as
    /// 			CONTACT-NAME of LETTER-STATUS.
			/// </summary>
			public String FULL_NAME{ get; set; }

			/// <summary>
			/// Operator ID# of individual performing transaction update.
			/// </summary>
			public String OP_NBR{ get; set; }

			/// <summary>
			/// Birthday or Gender rule used to determine primacy.
    /// 			The birthday or gender rule assists in determining the
    /// 			order of primacy. If the birthday rule is used, then
    /// 			the parent with birthday earliest in the year is
    /// 			considered primary (only the month and day are
    /// 			compared).
			/// </summary>
			public String RULE{ get; set; }

			/// <summary>
			/// Transaction code.
    /// 			For AUTHORIZATION, ADMISSION, and AUTH-DETAIL,
    /// 			TRANSCODE position 1: A (add), C (change)
    /// 			position 2: B (batch), blank (online)
			/// </summary>
			public String TRANSCODE{ get; set; }

			/// <summary>
			/// Date of most recent transaction.
			/// </summary>
			public Int32 YMDTRANS{ get; set; }

			/// <summary>
			/// Counter incremented via trigger after any updates.
    /// 			Used by the JAVA to duplicate the ''record has changed
    /// 			since you found it'' feature of AMISYS 3000 and
    /// 			AMISYS Advance Release 1 that is provided by the Cognos
    /// 			Quick screens.
			/// </summary>
			public Decimal CHECKSUM_VALUE{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
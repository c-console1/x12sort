using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// IMAGE_RECNBR is used internally by Cobol routines.
		/// </summary>
		public class AMISYS_BENEFIT_DIM{

			/// <summary>
			/// IMAGE_RECNBR is used internally by Cobol routines.
			/// </summary>
			public Int64 IMAGE_RECNBR{ get; set; }

			/// <summary>
			/// The name of the most specific definition for a benefit.
    /// 			Identifies the most specific level of benefits.
    /// 			A group of benefits form a more general level of
    /// 			benefits, called components.
			/// </summary>
			public String BENEFIT{ get; set; }

			/// <summary>
			/// [AR##] Authorization type.
    /// 			[AR##] This code category identifies the valid
    /// 			code, list of codes, and/or range of codes for
    /// 			the authorization type code category [AR].
    /// 			The authorization type code category [AR] defines
    /// 			the type or source of an authorization for post
    /// 			authorization review.
			/// </summary>
			public String AR_CODE{ get; set; }

			/// <summary>
			/// [AS##] Authorization extent of care.
    /// 			[AS##] This code category identifies the valid
    /// 			code, list of codes, and/or range of codes for the
    /// 			authorization extent of care [AS] code category.
    /// 			The authorization extent of care [AS] code category
    /// 			defines the extent of care that the health plan has
    /// 			approved for an authorization. For ADMISSION and AUTH-DETAIL, the AUTH-TYPE value is
    /// 			initialized from the AUTHORIZATION record, but the
    /// 			value can be changed. The detail authorization data
    /// 			may map to a different benefit qualifier or service
    /// 			qualifier based on date ranges.
			/// </summary>
			public String AS_CODE{ get; set; }

			/// <summary>
			/// Indicates if an authorization is required (Y/N).
			/// </summary>
			public String AUTH_REQ{ get; set; }

			/// <summary>
			/// A bank account identifier.
    /// 			A value used to identify a bank account. Used to
    /// 			determine to which bank account a check belongs.
    /// 			Valid values are defined in the SUPER-TABLE,
    /// 			by ''BANK-CODE''.
			/// </summary>
			public String BANK{ get; set; }

			/// <summary>
			/// [CI##] Cause of illness.
    /// 			[CI##] This code category identifies the valid CI
    /// 			code, list of CI codes, and/or range of CI codes
    /// 			defined by the health plan as valid cause of illness
    /// 			values.
			/// </summary>
			public String CI_CODE{ get; set; }

			/// <summary>
			/// [CL##] Member''s relationship to subscriber.
    /// 			[CL##] This code category identifies the valid CL
    /// 			code, list of CL codes, and/or range of CL codes
    /// 			defined by the health plan as valid relationships
    /// 			members have with the subscriber.
			/// </summary>
			public String CL_CODE{ get; set; }

			/// <summary>
			/// [CP##] Claim type.
    /// 			[CP##] This code category identifies the valid CP
    /// 			code(s), medical and/or hospital, for the given application.
			/// </summary>
			public String CP_CODE{ get; set; }

			/// <summary>
			/// Eff, end, admit, dischg, init, proc, rec, or pay date.
    /// 			Defines either the type of date found in YMDDATE, or
    /// 			which type of date to qualify against.
    /// 			01 = effective date of service
    /// 			02 = end date of service
    /// 			03 = admission date
    /// 			04 = discharge date
    /// 			05 = initial date
    /// 			06 = process date
    /// 			07 = payment date
    /// 			08 = receipt date
    /// 			Benefit values: 01, 02, 03, 04, 06, 07, 08
    /// 			Counter values: 01, 02, 03, 04, 05
    /// 			Pricing values: 01, 02, 03, 04, 05, 06, 07, 08
    /// 			AUTH-FLAG values: 01, 02, 03, 04, 05, 06, 07, 08
			/// </summary>
			public String DATE_TYPE{ get; set; }

			/// <summary>
			/// First line of a user defined description.
    /// 			The user defined description
    /// 			For HEALTH.MESSAGES-M, DESCRIPTION is the first
    /// 			line of user defined parameters (keywords).
			/// </summary>
			public String DESCRIPTION{ get; set; }

			/// <summary>
			/// [LD##] Diagnosis list (or a valid DIAG#).
    /// 			[LD##] This code category identifies the valid
    /// 			diagnosis number, list of diagnosis numbers, and/or
    /// 			range of diagnosis numbers allowed by the health plan.
    /// 			If the value for DIAG-LIST does not begin with ''LD##'',
    /// 			then the value must exist as a valid diagnosis number
    /// 			on the DIAG table.
			/// </summary>
			public String DIAG_LIST{ get; set; }

			/// <summary>
			/// DS##] Discharge status.
    /// 			[DS##] This code category identifies the valid DS code,
    /// 			list of DS codes, and/or range of DS codes defined
    /// 			by the health plan to indicate discharge status of
    /// 			patient from hospital stay (home, transferred, deceased).
			/// </summary>
			public String DS_CODE{ get; set; }

			/// <summary>
			/// [LC##] Location of service.
    /// 			[LC##] This code category identifies the valid LC
    /// 			code, list of LC codes, and/or range of LC codes
    /// 			defined by the health plan. The LC code defines the
    /// 			location where the service is provided (hospital,
    /// 			office, lab, etc.).
			/// </summary>
			public String LC_CODE{ get; set; }

			/// <summary>
			/// [M1##] Procedure modifier for pricing.
    /// 			[M1##] This code category identifies the valid M1 code,
    /// 			list of M1 codes, and/or range of M1 codes for defining
    /// 			special pricing arrangements for assistant surgeons,
    /// 			anesthesiologists, etc.
			/// </summary>
			public String M1_CODE{ get; set; }

			/// <summary>
			/// [ML##] Mouth Location (tooth number, arch, or full mouth).
			/// </summary>
			public String ML_CODE{ get; set; }

			/// <summary>
			/// Operator ID# of individual performing transaction update.
			/// </summary>
			public String OP_NBR{ get; set; }

			/// <summary>
			/// [PS##] Provider participation status for PCP.
    /// 			[PS##] This code category defines the provider
    /// 			participating status (in-plan participating, outpatient
    /// 			hospital participating, etc.).
			/// </summary>
			public String PCP_PS_CODE{ get; set; }

			/// <summary>
			/// [PL##] Procedure class.
			/// </summary>
			public String PL_CODE{ get; set; }

			/// <summary>
			/// Defines a provider type or range of provider types.
    /// 			The [PR##] structure for PR-CODE values allows the
    /// 			definition of a single provider type, or the definition
    /// 			of multiple provider types.
			/// </summary>
			public String PR_CODE{ get; set; }

			/// <summary>
			/// Order in which AMISYS evaluates the records.
    /// 			For RIDER-APP, PRIORITY defines the order in which
    /// 			the cash receipt is distributed among the riders.
    /// 			The RIDER-APP PRIORITY is only used when the cash
    /// 			receipt is less than the open invoice.
    /// 			For WORK FLOW PROCESSING: This field stores a numeric
    /// 			priority in the range 1 to 99 to allow a user to order
    /// 			his or her own tasks within the Act By Date.
			/// </summary>
			public String PRIORITY{ get; set; }

			/// <summary>
			/// [LP##] Procedures associated with a given category.
			/// </summary>
			public String PROC_LIST{ get; set; }

			/// <summary>
			/// Identifies health plan, IPA, and/or program area.
			/// </summary>
			public String PROG_NBR{ get; set; }

			/// <summary>
			/// Provider participation status for prov of service.
			/// </summary>
			public String PROV_PS_CODE{ get; set; }

			/// <summary>
			/// Provider specialty for the provider of service.
			/// </summary>
			public String PROV_SP_CODE{ get; set; }

			/// <summary>
			/// Defines a region, or a list of regions.
    /// 			The value of REGION-LIST defines one region value
    /// 			(validated by the SUPER-TABLE) or a list of region
    /// 			values (validated by the CODE set using the [LR] prefix.
			/// </summary>
			public String REGION_LIST{ get; set; }

			/// <summary>
			/// Relationship between providers on claim.
			/// </summary>
			public String RL_CODE{ get; set; }

			/// <summary>
			/// [RP##] Risk population (mix of the enrolled population).
			/// </summary>
			public String RP_CODE{ get; set; }

			/// <summary>
			/// [SA] Sex and age ranges for covered benefits.
			/// </summary>
			public String SA_CODE{ get; set; }

			/// <summary>
			/// [LS##] Defines a sub program, or a list of sub programs.
    /// 			The value of SUBPROG-LIST defines one sub-program value
    /// 			(validated by the SUPER-TABLE) or a list of sub-program
    /// 			values (validated by the CODE set using the [LS] prefix.
			/// </summary>
			public String SUBPROG_LIST{ get; set; }

			/// <summary>
			/// Transaction code.
    /// 			For AUTHORIZATION, ADMISSION, and AUTH-DETAIL,
    /// 			TRANSCODE position 1: A (add), C (change)
    /// 			position 2: B (batch), blank (online)
			/// </summary>
			public String TRANSCODE{ get; set; }

			/// <summary>
			/// [TS##] Tooth surface definition.
    /// 			The [TS] code category defines tooth surfaces.
    /// 			The TS-CODE item allows for a range of values to be
    /// 			defined via the ''##'' structure and logic.
			/// </summary>
			public String TS_CODE{ get; set; }

			/// <summary>
			/// [TT##] Treatment type.
    /// 			The [TT] treatment type code category defines treatments
    /// 			for benefits, claims expensing, and utilization reporting.
			/// </summary>
			public String TT_CODE{ get; set; }

			/// <summary>
			/// Date of most recent transaction.
			/// </summary>
			public Int32 YMDTRANS{ get; set; }

			/// <summary>
			/// Counter incremented via trigger after any updates.
    /// 			Used by the JAVA to duplicate the ''record has changed
    /// 			since you found it'' feature of AMISYS 3000 and
    /// 			AMISYS Advance Release 1 that is provided by the Cognos
    /// 			Quick screens.
			/// </summary>
			public Decimal CHECKSUM_VALUE{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
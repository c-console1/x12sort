using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// IMAGE_RECNBR is used internally by Cobol routines.
		/// </summary>
		public class AMISYS_BENEFIT_PKG_DIM{

			/// <summary>
			/// IMAGE_RECNBR is used internally by Cobol routines.
			/// </summary>
			public Int64 IMAGE_RECNBR{ get; set; }

			/// <summary>
			/// The name of a general group of benefits.
    /// 			The name of the most general definition for benefits.
    /// 			A group of benefits form a component. They are
    /// 			linked together by CB-LINK (component/benefit link).
    /// 			A group of components form a rider. They are
    /// 			linked together by RC-LINK (rider/component link).
    /// 			A group of riders form a benefit package. They are
    /// 			linked together by BR-LINK (benefit/rider link).
			/// </summary>
			public String BENEFIT_PKG{ get; set; }

			/// <summary>
			/// First line of a user defined description.
    /// 			The user defined description
    /// 			For HEALTH.MESSAGES-M, DESCRIPTION is the first
    /// 			line of user defined parameters (keywords).
			/// </summary>
			public String DESCRIPTION{ get; set; }

			/// <summary>
			/// Operator ID# of individual performing transaction update.
			/// </summary>
			public String OP_NBR{ get; set; }

			/// <summary>
			/// ransaction code.
    /// 			For AUTHORIZATION, ADMISSION, and AUTH-DETAIL,
    /// 			TRANSCODE position 1: A (add), C (change)
    /// 			position 2: B (batch), blank (online)
			/// </summary>
			public String TRANSCODE{ get; set; }

			/// <summary>
			/// Date of most recent transaction.
			/// </summary>
			public Int32 YMDTRANS{ get; set; }

			/// <summary>
			/// Counter incremented via trigger after any updates.
    /// 			Used by the JAVA to duplicate the ''record has changed
    /// 			since you found it'' feature of AMISYS 3000 and
    /// 			AMISYS Advance Release 1 that is provided by the Cognos
    /// 			Quick screens.
			/// </summary>
			public Decimal CHECKSUM_VALUE{ get; set; }

			/// <summary>
			/// This field should be set to ''Y'' to enable Anniversary Date benefit
    /// 			DETAIL, which will select the appropriate benefits based on the first
    /// 			day of the fiscal anniversary benefit year.A value of ''N'' or blank will
    /// 			disable Anniversary Date benefit DETAIL.
    /// 			Valid values are:
    /// 			''Y'' - Yes
    /// 			''N'' - No
    /// 			blank - No'
			/// </summary>
			public String ANNIV_BEN_MAP{ get; set; }

			/// <summary>
			/// The anniversary period to be used when deriving an anniversary
    /// 			benefit search date.This column will be blank, when Anniversary
    /// 			Date benefit DETAIL is disabled.
    /// 			Value values are:
    /// 			''D'' - Division
    /// 			''G'' - Group
    /// 			''I'' - Individual
			/// </summary>
			public String ANNIV_PERIOD{ get; set; }

			/// <summary>
			/// The Explain (EX) code returned to the claims processor, when
    /// 			Anniversary Date benefit DETAIL is enabled for a Benefit Package
    /// 			and the Benefit Package Anniversary Period conflicts with one or more
    /// 			Benefit Keyword Counter Tracking Periods.
			/// </summary>
			public String CONFLICT_EX{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
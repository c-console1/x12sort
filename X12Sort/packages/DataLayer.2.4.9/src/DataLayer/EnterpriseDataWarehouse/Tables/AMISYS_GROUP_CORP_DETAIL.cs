using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// IMAGE_RECNBR is used internally by Cobol routines.
		/// </summary>
		public class AMISYS_DIVISION_EXPENSE_FACT{

			/// <summary>
			/// IMAGE_RECNBR is used internally by Cobol routines.
			/// </summary>
			public Int64 IMAGE_RECNBR{ get; set; }

			/// <summary>
			/// Risk Pool Name
			/// </summary>
			public String POOL{ get; set; }

			/// <summary>
			/// Identification number of the employer division.
    /// 			Identification number assigned to employer group
    /// 			divisions. For ease of identification, the DIVISION#
    /// 			consists of GROUP#, X(6) plus a 4 character extension.
			/// </summary>
			public String DIVISION_NBR{ get; set; }

			/// <summary>
			/// Number assigned to employer groups.
    /// 			The number assigned to employer groups who have
    /// 			contracted with the health plan to provide health
    /// 			insurance coverage.
			/// </summary>
			public String GROUP_NBR{ get; set; }

			/// <summary>
			/// The independent geographic or profit center entities
    /// 			within the health plan with which members and providers
    /// 			are affiliated.
			/// </summary>
			public String BUSINESS_UNIT{ get; set; }

			/// <summary>
			/// Carrier / underwriter of risk.
    /// 			Identifies the carrier / underwriter of risk and how
    /// 			the risk will be managed. Valid values are defined
    /// 			in the SUPER-TABLE table.
			/// </summary>
			public String CARRIER{ get; set; }

			/// <summary>
			/// Fee-for-service (FFS) amount charged for the Division
			/// </summary>
			public Int64 FFS_AMOUNT{ get; set; }

			/// <summary>
			/// LAB Amount Charged for the Division
			/// </summary>
			public Int64 LAB_AMOUNT{ get; set; }

			/// <summary>
			/// LPU Amount Charge for the Division
			/// </summary>
			public Int64 LPU_AMOUNT{ get; set; }

			/// <summary>
			/// MGT Amount charged for the Division
			/// </summary>
			public Int64 MGT_AMOUNT{ get; set; }

			/// <summary>
			/// MNH_Amount charged for the Division
			/// </summary>
			public Int64 MNH_AMOUNT{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String MONTH{ get; set; }

			/// <summary>
			/// Primary Care Physician Amount billed to the Division.
			/// </summary>
			public Int64 PCP_AMOUNT{ get; set; }

			/// <summary>
			/// Identifies health plan, IPA, and/or program area
			/// </summary>
			public String PROG_NBR{ get; set; }

			/// <summary>
			/// Regional area associated with the specified entity.
			/// </summary>
			public String REGION{ get; set; }

			/// <summary>
			/// VIS Amount charged for the Division.
			/// </summary>
			public Int64 VIS_AMOUNT{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
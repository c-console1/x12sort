using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// 
		/// </summary>
		public class BENEFIT_PLAN_DIM{

			/// <summary>
			/// 
			/// </summary>
			public Int64 BENEFIT_PLAN_DIM_KEY{ get; set; }

			/// <summary>
			/// Description of the benefit plan.
			/// </summary>
			public String BENEFIT_PLAN_DESC{ get; set; }

			/// <summary>
			/// Business centric identifier for the benefit plan.
			/// </summary>
			public String BENEFIT_PLAN_HCC_ID{ get; set; }

			/// <summary>
			/// Column for summing number of records in a query. No business application. Always has a value of 1.
			/// </summary>
			public Int64 BENEFIT_PLAN_HIST_FACT_COUNT{ get; set; }

			/// <summary>
			/// Is the benefit plan wrapped. When a claim is adjudicated, the base benefit amount is applied against the outstanding amount. A wrap benefit amount is applied against a remaining member responsible amount.
			/// </summary>
			public Decimal BENEFIT_PLAN_IS_WRAPPED{ get; set; }

			/// <summary>
			/// Name of the benefit plan.
			/// </summary>
			public String BENEFIT_PLAN_NAME{ get; set; }

			/// <summary>
			/// Product on which the the benefit plan is based (e.g., PPO).
			/// </summary>
			public Int64 BENEFIT_PLAN_PRODUCT_KEY{ get; set; }

			/// <summary>
			/// HCC assigned identifier for Product provided by HCC.
			/// </summary>
			public String PRODUCT_HCC_ID{ get; set; }

			/// <summary>
			/// Name of a given Product provided by HCC.
			/// </summary>
			public String PRODUCT_NAME{ get; set; }

			/// <summary>
			/// The state in which the benefit plan is legally established. Dictates availability of benefits for a member in another geographic region. For example, a member resides in in New Jersey, but the plan is purchased by a Massachusetts company. The member may be eligible for Massachusetts benefits. The HCC needs this data to help monitor regulatory compliance and reporting.
			/// </summary>
			public String BENEFIT_PLAN_SITUS_CODE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public string BENEFIT_PLAN_STATUS_NAME{ get; set; }

			/// <summary>
			/// 
			/// </summary>
			public Int64 BENEFIT_PLAN_TEMPLATE_KEY{ get; set; }

			/// <summary>
			/// Name of benefit plan template.
			/// </summary>
			public String BENEFIT_PLAN_TEMPLATE_NAME{ get; set; }

			/// <summary>
			/// Time of year when benefit plan begins (e.g., Jan 1).
			/// </summary>
			public String BENEFIT_PLAN_YEAR{ get; set; }

			/// <summary>
			/// Benefit Level Name
			/// </summary>
			public String EXCHANGE_BENEFIT_LEVEL_NAME{ get; set; }

			/// <summary>
			/// Segment Name for the Benefit Exchange
			/// </summary>
			public String EXCHANGE_SEGMENT_NAME{ get; set; }

			/// <summary>
			/// Exchange State Code
			/// </summary>
			public String EXCHANGE_STATE_CODE{ get; set; }

			/// <summary>
			/// Exchange Type Name
			/// </summary>
			public String EXCHANGE_TYPE_NAME{ get; set; }

			/// <summary>
			/// Values are: Paid Amount Reference Fee
			/// </summary>
			public String FUNDING_AMOUNT_BASIS_NAME{ get; set; }

			/// <summary>
			/// Values are: Fully insured Self-Funded ASO Minimum Premium Other
			/// </summary>
			public String FUNDING_METHOD_NAME{ get; set; }

			/// <summary>
			/// Flag "Y' or "N"
			/// </summary>
			public Decimal IS_EXCHANGE{ get; set; }

			/// <summary>
			/// For Medicare supplement plans, the is the standard Medicare supplement plan model identifier.
			/// </summary>
			public Int64 STANDARD_PLAN_MODEL_ID{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 TENANT_HCC_ID{ get; set; }

			/// <summary>
			/// Links to the ATTACHMENT_FACT_LINK table, linking to the ATTACHMENT_FACT table. Provides a unique ID for attachments linked to benefit plan.
			/// </summary>
			public Int64 ATTACHMENT_SET_ID{ get; set; }

			/// <summary>
			/// Primary key for AUDIT_LOG table. Auto-generated surrogate key.
			/// </summary>
			public Int64 AUDIT_LOG_KEY{ get; set; }

			/// <summary>
			/// End Date of Override
			/// </summary>
			public Int32 PLAN_YR_END_DT_OVERRIDE{ get; set; }

			/// <summary>
			/// Start date of Override.
			/// </summary>
			public Int32 PLAN_YR_START_DT_OVERRIDE{ get; set; }

			/// <summary>
			/// Plan Effective Start Date
			/// </summary>
			public Int32 PLAN_EFFECTIVE_DATE{ get; set; }

			/// <summary>
			/// Plan Termination Date
			/// </summary>
			public Int32 PLAN_TERMINATION_DATE{ get; set; }

			/// <summary>
			/// Date when the Benefit Plan is locked.
			/// </summary>
			public Int32 RATE_LOCK_DATE{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
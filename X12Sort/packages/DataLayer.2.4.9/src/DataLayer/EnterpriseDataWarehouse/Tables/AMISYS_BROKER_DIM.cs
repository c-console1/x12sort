using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// IMAGE_RECNBR is used internally by Cobol routines.
		/// </summary>
		public class AMISYS_BENEFIT_PKG_RIDER_DIM{

			/// <summary>
			/// IMAGE_RECNBR is used internally by Cobol routines.
			/// </summary>
			public Int64 IMAGE_RECNBR{ get; set; }

			/// <summary>
			/// Desc
    /// 			The user defined title associated with a rider.
			/// </summary>
			public String RIDER_TITLE{ get; set; }

			/// <summary>
			/// Types of capitation (ie, specialist, drug, etc).
    /// 			User defined types of capitation for which a provider
    /// 			can be eligible (i.e., specialist, drug, etc). These
    /// 			values are defined in the SUPER-TABLE table.
			/// </summary>
			public String CAPTYPE{ get; set; }

			/// <summary>
			/// First line of a user defined description.
    /// 			The user defined description
    /// 			For HEALTH.MESSAGES-M, DESCRIPTION is the first
    /// 			line of user defined parameters (keywords).
			/// </summary>
			public String DESCRIPTION{ get; set; }

			/// <summary>
			/// Operator ID# of individual performing transaction update.
			/// </summary>
			public String OP_NBR{ get; set; }

			/// <summary>
			/// Defines rider as Med/Life/Fin/Admin/SuppLife/Dental/etc
    /// 			Defines the rider class.
    /// 			M Medical
    /// 			L Life
    /// 			F Financial
    /// 			A Administrative Fee
    /// 			SL Supplemental Life
    /// 			DL Dependent Life
    /// 			AD Accidental Death and Dismemberment
    /// 			D Dental
    /// 			SD Short Term Disability
    /// 			LD Long Term Disability
    /// 			SS Specific Stop Loss
    /// 			AS Aggregate Stop Loss
			/// </summary>
			public String RIDER_CLASS{ get; set; }

			/// <summary>
			/// RT] Rider type.
			/// </summary>
			public String RIDER_TYPE{ get; set; }

			/// <summary>
			/// Transaction code.
    /// 			For AUTHORIZATION, ADMISSION, and AUTH-DETAIL,
    /// 			TRANSCODE position 1: A (add), C (change)
    /// 			position 2: B (batch), blank (online)
			/// </summary>
			public String TRANSCODE{ get; set; }

			/// <summary>
			/// Date of most recent transaction.
			/// </summary>
			public Int32 YMDTRANS{ get; set; }

			/// <summary>
			/// Counter incremented via trigger after any updates.
    /// 			Used by the JAVA to duplicate the ''record has changed
    /// 			since you found it'' feature of AMISYS 3000 and
    /// 			AMISYS Advance Release 1 that is provided by the Cognos
    /// 			Quick screens.
			/// </summary>
			public Decimal CHECKSUM_VALUE{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
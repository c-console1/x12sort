using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// IMAGE_RECNBR is used internally by Cobol routines.
		/// </summary>
		public class AMISYS_BENEFIT_DETAILS{

			/// <summary>
			/// IMAGE_RECNBR is used internally by Cobol routines.
			/// </summary>
			public Int64 IMAGE_RECNBR{ get; set; }

			/// <summary>
			/// Desc
    /// 			The BP-EXPLODE# is composed of the following:
    /// 			BENEFIT-PKG + PL + CP + LC + TT
    /// 			(X2) (X2) (X2) (X2) (X2)
			/// </summary>
			public String BP_EXPLODE_NBR{ get; set; }

			/// <summary>
			/// The BP-BENEFIT is composed of the following:
    /// 			BENEFIT-PKG (X2) + BENEFIT (X8)
			/// </summary>
			public String BP_BENEFIT{ get; set; }

			/// <summary>
			/// A group of benefits linked together to form a component.
    /// 			
			/// </summary>
			public String COMPONENT{ get; set; }

			/// <summary>
			/// The user defined title associated with a rider.
			/// </summary>
			public String RIDER_TITLE{ get; set; }

			/// <summary>
			/// Effective or start date.
    /// 			For UB-92 this is From Date- date the occurrence starts
    /// 			The UB-92 from date is the date the occurrence is for.
    /// 			Occur date corresponds to the occur code event relating
    /// 			to this bill. This ''from date'' must not be later than
    /// 			a ''thru date'' when a thru date is entered.
    /// 			Refer to UB92 locator FIELD 32a-36a and 32b-36b
    /// 			of service. A one position, alpha numeric value. See
    /// 			UB92 FIELD locator 32 through 36 for value.
    /// 			For PROV-CON-ED, YMDEFF is the start date for the class
    /// 			or seminar.
			/// </summary>
			public Int32 YMDEFF{ get; set; }

			/// <summary>
			/// End date.
    /// 			For UB-92 this is Thru Date- date the occurrence ends.
    /// 			The UB-92 thru date is the date the occurrence is for.
    /// 			Occur date corresponds to the occur code event relating
    /// 			to this bill. This ''thru date'' must not be less than
    /// 			a ''from date'' when a thru date is entered.
    /// 			Refer to UB92 locator FIELDS 32a-36a and 32b-36b values
			/// </summary>
			public Int32 YMDEND{ get; set; }

			/// <summary>
			/// Order in which AMISYS evaluates the records.
    /// 			For RIDER-APP, PRIORITY defines the order in which
    /// 			the cash receipt is distributed among the riders.
    /// 			The RIDER-APP PRIORITY is only used when the cash
    /// 			receipt is less than the open invoice.
    /// 			For WORK FLOW PROCESSING: This field stores a numeric
    /// 			priority in the range 1 to 99 to allow a user to order
    /// 			his or her own tasks within the Act By Date.
			/// </summary>
			public String PRIORITY{ get; set; }

			/// <summary>
			/// Counter incremented via trigger after any updates.
    /// 			Used by the JAVA to duplicate the ''record has changed
    /// 			since you found it'' feature of AMISYS 3000 and
    /// 			AMISYS Advance Release 1 that is provided by the Cognos
    /// 			Quick screens.
			/// </summary>
			public Decimal CHECKSUM_VALUE{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
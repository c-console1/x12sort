using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// No Description Found
		/// </summary>
		public class RIDER_TO_BENEFIT_PLAN_COMPONENT{

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 BENEFIT_PLAN_RIDER_DIM_KEY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 BENEFIT_PLAN_COMPONENT_DIM_KEY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String RIDER_OPERATION_TYPE_NAME{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String SLOT_NAME{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 SORT_ORDER{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
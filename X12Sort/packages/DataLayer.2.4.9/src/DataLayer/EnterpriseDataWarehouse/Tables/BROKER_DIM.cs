using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// Broker agent Dimension Key
		/// </summary>
		public class BROKER_AGENT_DIM{

			/// <summary>
			/// Broker agent Dimension Key
			/// </summary>
			public Int64 BROKER_AGENT_DIM_KEY{ get; set; }

			/// <summary>
			/// Primary key for the BROKER_AGENT_HIST_FACT table.
			/// </summary>
			public Int64 BROKER_DIM_KEY{ get; set; }

			/// <summary>
			/// Broker Full Name
			/// </summary>
			public Int64 BROKER_NAME{ get; set; }

			/// <summary>
			/// System generated key for the broker agent.
			/// </summary>
			public Int64 BROKER_AGENT_KEY{ get; set; }

			/// <summary>
			/// Business centric ID for the broker agent.
			/// </summary>
			public String BROKER_AGENT_HCC_ID{ get; set; }

			/// <summary>
			/// Broker agent first name.
			/// </summary>
			public String BROKER_AGENT_FIRST_NAME{ get; set; }

			/// <summary>
			/// Broker agent full name.
			/// </summary>
			public String BROKER_AGENT_LAST_NAME{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String BROKER_AGENT_FULL_NAME{ get; set; }

			/// <summary>
			/// Status code (a - Active, t - Terminated, u - Unknown).
			/// </summary>
			public String BROKER_AGENT_STATUS_NAME{ get; set; }

			/// <summary>
			/// Type of broker agent (general, independent). Joins to BROKER_AGENT_TYPE_CODE table.
			/// </summary>
			public String BROKER_AGENT_TYPE_NAME{ get; set; }

			/// <summary>
			/// Broker Agent History fact Count
			/// </summary>
			public Int64 BROKER_AGENT_HIST_FACT_COUNT{ get; set; }

			/// <summary>
			/// Audit log key
			/// </summary>
			public Int64 AUDIT_LOG_KEY{ get; set; }

			/// <summary>
			/// Begin date for this version of the broker agent record. Joins to the DATE_DIMENSION table.
			/// </summary>
			public DateTime? BROKER_AGENT_EFF_DATE{ get; set; }

			/// <summary>
			/// End date for this version of the broker agent record. Joins to the DATE_DIMENSION table.
			/// </summary>
			public DateTime? BROKER_AGENT_TERM_DATE { get; set; }

			/// <summary>
			/// Primary key for DATE_DIMENSION table. Auto-generated surrogate key.
			/// </summary>
			public DateTime? VERSION_EFF_DATE { get; set; }

			/// <summary>
			/// Primary key for DATE_DIMENSION table. Auto-generated surrogate key.
			/// </summary>
			public DateTime? VERSION_EXP_DATE { get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime? LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}

using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// No Description Found
		/// </summary>
		public class ACCOUNT_TO_ACCOUNT_UDT{

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 ACCOUNT_DIM_KEY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 ACCOUNT_UDT_KEY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
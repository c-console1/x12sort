using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// IMAGE_RECNBR is used internally by Cobol routines.
		/// </summary>
		public class AMISYS_ALTPOLICY_MEMBER_DIM{

			/// <summary>
			/// IMAGE_RECNBR is used internally by Cobol routines.
			/// </summary>
			public Int64 IMAGE_RECNBR{ get; set; }

			/// <summary>
			/// Alternate policy number (the ALTCARRIER# + the policy#).
    /// 			The alternate policy number is a concatenation of the
    /// 			ALTCARRIER# X(4) + the policy# X(12).
			/// </summary>
			public String ALTPOLICY_NBR{ get; set; }

			/// <summary>
			/// Alt Carrier location that sends bills to alt policy.
    /// 			The address or location of the alternate carrier that
    /// 			sends the bills to the alternate policy holder''s group.
    /// 			This address is used for correspondence to better
    /// 			coordinate benefits.
			/// </summary>
			public String BILLPOINT{ get; set; }

			/// <summary>
			/// MemberFirst name
			/// </summary>
			public String FIRSTNAME{ get; set; }

			/// <summary>
			/// MemberLast name
			/// </summary>
			public String LASTNAME{ get; set; }

			/// <summary>
			/// Identification# assigned to members of the health plan.
    /// 			This element is a concatenation of the following:
    /// 			CONTRACT# X(9) + extension X(2)
			/// </summary>
			public String MEMBER_NBR{ get; set; }

			/// <summary>
			/// Operator ID# of individual performing transaction update.
			/// </summary>
			public String OP_NBR{ get; set; }

			/// <summary>
			/// Address of alternate carrier''s payment center.
			/// </summary>
			public String PAYPOINT{ get; set; }

			/// <summary>
			/// Group# of the alternate policy.
    /// 			
			/// </summary>
			public String POLICYGRP_NBR{ get; set; }

			/// <summary>
			/// Group name of the alternate policy.
			/// </summary>
			public String POLICYGRPNAME{ get; set; }

			/// <summary>
			/// Sex. (M)ale or (F)emale. (See ''??'' for more info).
    /// 			Sex. (M)ale or (F)emale.
    /// 			Some AMISYS screens do not require a value for SEX.
    /// 			These screens allow '' '' as a valid value.
    /// 			The AMISYS actuarial table processing for capitation
    /// 			rates allows a value of ''B'' (for both sexes).
			/// </summary>
			public String SEX{ get; set; }

			/// <summary>
			/// Social security number.
			/// </summary>
			public String SSN{ get; set; }

			/// <summary>
			/// Transaction code.
    /// 			For AUTHORIZATION, ADMISSION, and AUTH-DETAIL,
    /// 			TRANSCODE position 1: A (add), C (change)
    /// 			position 2: B (batch), blank (online)
			/// </summary>
			public String TRANSCODE{ get; set; }

			/// <summary>
			/// Date of birth.
			/// </summary>
			public Int32 YMDBIRTH{ get; set; }

			/// <summary>
			/// Date of most recent transaction.
			/// </summary>
			public Int32 YMDTRANS{ get; set; }

			/// <summary>
			/// Counter incremented via trigger after any updates.
    /// 			Used by the JAVA to duplicate the ''record has changed
    /// 			since you found it'' feature of AMISYS 3000 and
    /// 			AMISYS Advance Release 1 that is provided by the Cognos
    /// 			Quick screens.
			/// </summary>
			public Decimal CHECKSUM_VALUE{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
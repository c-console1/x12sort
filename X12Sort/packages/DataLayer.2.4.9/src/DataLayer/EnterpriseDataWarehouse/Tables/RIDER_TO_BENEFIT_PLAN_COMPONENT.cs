using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// No Description Found
		/// </summary>
		public class PROVIDER_LICENSES_DETAILS{

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 PROVIDER_LICENSE_KEY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 PROVIDER_DIM_KEY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 PROVIDER_ENTITY_DIM_KEY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String PROVIDER_TYPE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String LICENSE_NUMBER{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String LICENSE_NAME{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String LICENSUR_EFIELD{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String STATUS{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String IS_ACTIVE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String STATE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String INSTITUTION_NAME{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String INSTITUTION_SALUTATION{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String INSTITUTION_CONTACT{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String INSTITUTION_ADDRESS_1{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String INSTITUTION_ADDRESS_2{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String INSTITUTION_CITY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String INSTITUTION_STATE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String INSTITUTION_ZIP_CODE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String INSTITUTION_ZIP_4_CODE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String INSTITUTION_COUNTRY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String INSTITUTION_FACILITY_TYPE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Decimal INSTITUTION_LONGITUDE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Decimal INSTITUTION_LATITUDE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String INSTITUTION_PHONE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String INSTITUTION_ALTERNATE_PHONE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String INSTITUTION_FAX{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String INSTITUTION_EMAIL{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String INSTITUTION_HOMEPAGEURL{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String CONTACT_NAME{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String CONTACT_PHONE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String CONTACT_FAX{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String CONTACT_EMAIL{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 AWARDED_DATE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 EXPIRATION_DATE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String SOURCE_KEY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// Used internally by Cobol routines.
		/// </summary>
		public class AMISYS_MEMBER_PREV_INFO_DIM{

			/// <summary>
			/// Used internally by Cobol routines.
			/// </summary>
			public Int64 IMAGE_RECNBR{ get; set; }

			/// <summary>
			/// Identification# assigned to members of the health plan.
    /// 			This element is a concatenation of the following:
    /// 			CONTRACT# X(9) + extension X(2)
			/// </summary>
			public String MEMBER_NBR{ get; set; }

			/// <summary>
			/// Extended First Name
    /// 			Identifies the first name of a given entity.
			/// </summary>
			public String NAME_FIRST{ get; set; }

			/// <summary>
			/// Extended Last Name
    /// 			Identifies the last name of a given entity.
    /// 			The NAME-LAST field in MEMBER-PREV-INFO holds the
    /// 			member Last Name prior to the last database update. The
    /// 			MENBER-PREV-INFO table captures name corrections as
    /// 			well as legal name changes.
    /// 			Note that the LASTNAME-P field, which resides in the
    /// 			MEMBER-INFO table, is only intended to capture
    /// 			legal name changes.
			/// </summary>
			public String NAME_LAST{ get; set; }

			/// <summary>
			/// Extended Middle Name
    /// 			Identifies the middle name of a given entity.
			/// </summary>
			public String NAME_MIDDLE{ get; set; }

			/// <summary>
			/// Extended Name Prefix
    /// 			Identifies the name prefix of a given entity. Examples
    /// 			are 'MR', 'MS', and 'MRS'
			/// </summary>
			public String NAME_PREFIX{ get; set; }

			/// <summary>
			/// Extended Name Suffix
    /// 			Identifies the name suffix of a given entity. Examples
    /// 			are 'JR', 'SR', 'I', 'II', and 'III'
			/// </summary>
			public String NAME_SUFFIX{ get; set; }

			/// <summary>
			/// Sex. (M)ale or (F)emale. (See '??' for more info).
    /// 			Sex. (M)ale or (F)emale.
    /// 			Some AMISYS screens do not require a value for SEX.
    /// 			These screens allow ' ' as a valid value.
    /// 			The AMISYS actuarial table processing for capitation
    /// 			rates allows a value of 'B' (for both sexes).
			/// </summary>
			public String SEX{ get; set; }

			/// <summary>
			/// Social security number.
			/// </summary>
			public String SSN{ get; set; }

			/// <summary>
			/// Date of birth.
			/// </summary>
			public Int32 YMDBIRTH{ get; set; }

			/// <summary>
			/// Defines the date the member first name was changed
    /// 			Defines the date the member first name was changed
			/// </summary>
			public Int32 YMDTRANS_NM_FIRS{ get; set; }

			/// <summary>
			/// Defines the date the member last name was changed
			/// </summary>
			public Int32 YMDTRANS_NM_LAST{ get; set; }

			/// <summary>
			/// Defines the date the member middle name was changed
			/// </summary>
			public Int32 YMDTRANS_NM_MID{ get; set; }

			/// <summary>
			/// Defines the date the member name prefix was changed
			/// </summary>
			public Int32 YMDTRANS_NM_PFX{ get; set; }

			/// <summary>
			/// Defines the date the member name suffix was changed
			/// </summary>
			public Int32 YMDTRANS_NM_SFX{ get; set; }

			/// <summary>
			/// Defines the date the member sex was changed
			/// </summary>
			public Int32 YMDTRANS_SEX{ get; set; }

			/// <summary>
			/// Defines the date the member SSN was changed
			/// </summary>
			public Int32 YMDTRANS_SSN{ get; set; }

			/// <summary>
			/// Defines the date the member birth date was changed
			/// </summary>
			public Int32 YMDTRANS_BIRTH{ get; set; }

			/// <summary>
			/// Counter incremented via trigger after any updates.
    /// 			Used by the JAVA to duplicate the 'record has changed
    /// 			since you found it' feature of AMISYS 3000 and
    /// 			AMISYS Advance Release 1 that is provided by the Cognos
    /// 			Quick screens.
			/// </summary>
			public Decimal CHECKSUM_VALUE{ get; set; }

			/// <summary>
			/// Marital status of Contract Holder.
    /// 			Marital status of Contract holder stored on CONTRACT-SPAN and copied
    /// 			down to MEMBER-SPAN records for capitation. For UB-92, Marital Status
    /// 			is patients marital status at time of service. If UB92, see UB92 FIELD
    /// 			locator 16 for value.
			/// </summary>
			public String MARITALSTAT{ get; set; }

			/// <summary>
			/// This code is required when the member has specified access
    /// 			to their info.Keywords on the Confidentiality Code (C8) control
    /// 			whether an access warning is raised on the Customer Service
    /// 			Phone Log and Membership Inquiry screens when access is restricted.
    /// 			HIPAA 5010 values are:
    /// 			'R' - Restricted
    /// 			'U' - Unrestricted.
			/// </summary>
			public String CONFIDENTIAL{ get; set; }

			/// <summary>
			/// This code is used to indicate the member's race or ethnicity (RA).
    /// 			HIPAA 5010 allows the Race or Ethnicity Code to be specified
    /// 			using a limited code set similar to the Bureau of Census or the
    /// 			more expansive Classification of Race or Ethnicity code set used
    /// 			by the CDC.This field is used for HEDIS Reporting.
			/// </summary>
			public String RACE_ETHNICITY{ get; set; }

			/// <summary>
			/// This code is used to indicate the member's citizenship status (C9).
    /// 			HIPAA 5010 values are:
    /// 			'1' - U.S. Citizen
    /// 			'2' - Non-Resident Alien
    /// 			'3' - Resident Alien
    /// 			'4' - Illegal Alien
    /// 			'5' - Alien
    /// 			'6' - U.S. Citizen - Non-Resident
    /// 			'7' - U.S. Citizen - Resident.
			/// </summary>
			public String CITIZENSHIP{ get; set; }

			/// <summary>
			/// Stores the date in which the Marital Status (MR) was last changed on the
    /// 			Member Maintenance (ME1700) screen.This field is used in conjunction with
    /// 			the CITIZENSHIP column in this table.When the Citizenship (C9) code set is
    /// 			changed on the Member Maintenance (ME1700) screen a row is inserted in to
    /// 			this table and stores the original or 'old' value along with the system date in
    /// 			which the change was made / row inserted in this
			/// </summary>
			public Int32 YMDTRANS_CITIZEN{ get; set; }

			/// <summary>
			/// Stores the date in which the Confidentiality Code (C8) code was last changed
    /// 			on the Member Maintenance (ME1700) screen.This field is used in conjunction
    /// 			with the RACE_ETHNICITY column in this table.When the Race/Ethnicity Code
    /// 			(RA) code set is changed on the Member Maintenance (ME1700) screen a row is
    /// 			inserted in to this table and stores the original or 'old' value along with the system
    /// 			date in which the change was made / row inserted in this table.
			/// </summary>
			public Int32 YMDTRANS_RACEETH{ get; set; }

			/// <summary>
			/// Stores the date in which the member Race or Ethnicity (RA) code was last
    /// 			changed on the Member Maintenance (ME1700) screen.This field is used in
    /// 			conjunction with the CONFIDENTIAL column in this table.When the Confidentiality
    /// 			(C8) code set is changed on the Member Maintenance (ME1700) screen a row is
    /// 			inserted in to this table and stores the original or 'old' value along with the system
    /// 			date in which the change was made / row inserted in this table.
			/// </summary>
			public Int32 YMDTRANS_CNFDNTL{ get; set; }

			/// <summary>
			/// Stores the date in which the member Citizenship Status (C9). code was last
    /// 			changed on the Member Maintenance (ME1700) screen.This field is used in
    /// 			conjunction with the MARITALSTAT column in this table.When the Marital Status
    /// 			(MR) code set is changed on the Contract Maintenance (ME1600) screen a row is
    /// 			inserted in to this table and stores the original or 'old' value along with the system
    /// 			date in which the change was made / row inserted in this table
			/// </summary>
			public Int32 YMDTRANS_MARSTAT{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
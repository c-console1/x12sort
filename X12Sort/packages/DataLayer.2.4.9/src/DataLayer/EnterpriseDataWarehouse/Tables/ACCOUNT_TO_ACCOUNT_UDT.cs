using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// Key to the ALL_ACCOUNT_HISTORY_FACT table
		/// </summary>
		public class ACCOUNT_OTHER_ID_DETAILS{

			/// <summary>
			/// Key to the ALL_ACCOUNT_HISTORY_FACT table
			/// </summary>
			public Int64 ACCOUNT_DIM_KEY{ get; set; }

			/// <summary>
			/// Other ID for Account.
			/// </summary>
			public Int64 IDENTIFICATION_NUMBER_KEY{ get; set; }

			/// <summary>
			/// Identification number.
			/// </summary>
			public String IDENTIFICATION_NUMBER{ get; set; }

			/// <summary>
			/// The state which issued the identification number if applicable.
			/// </summary>
			public String STATE_CODE{ get; set; }

			/// <summary>
			/// State name.
			/// </summary>
			public String STATE_NAME{ get; set; }

			/// <summary>
			/// The country which issued the identification number if applicable.
			/// </summary>
			public String COUNTRY_CODE{ get; set; }

			/// <summary>
			/// Country Name
			/// </summary>
			public String COUNTRY_CODE_NAME{ get; set; }

			/// <summary>
			/// Name of identification type. Examples are: Not Applicable Not Specified NPI HCFA Plan ID Medical Record Number
			/// </summary>
			public String ID_TYPE_NAME{ get; set; }

			/// <summary>
			/// Effective end date.
			/// </summary>
			public Int32 EFFECTIVE_START_DATE{ get; set; }

			/// <summary>
			/// Effective end date.
			/// </summary>
			public Int32 EFFECTIVE_END_DATE{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
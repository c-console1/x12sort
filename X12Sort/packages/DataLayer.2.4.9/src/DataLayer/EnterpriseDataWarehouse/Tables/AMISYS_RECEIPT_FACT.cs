using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// No Description Found
		/// </summary>
		public class AMISYS_RACE_ETHNICITY_DIM{

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 RACE_ETHN_ID{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String RACE_ETHN_NAME{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String RACE_ETHN_IND{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String RACE_ETHN_CODE{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// No Description Found
		/// </summary>
		public class AMISYS_SU_FSA_SUBSCRIBER_DIM{

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 AMISYS_SU_FSA_SUBSCRIBER_DETAIL_KEY{ get; set; }

			/// <summary>
			/// Social Secuirty Number
			/// </summary>
			public String SSN{ get; set; }

			/// <summary>
			/// Employee Number
			/// </summary>
			public String SU_EMPLOYEE_NBR{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String SUBSCRIBER_NBR{ get; set; }

			/// <summary>
			/// Division Number
			/// </summary>
			public String DIVISION_NBR{ get; set; }

			/// <summary>
			/// FSA Contract plan year
			/// </summary>
			public Int16 SU_FSA_PLAN_YEAR{ get; set; }

			/// <summary>
			/// Benefit Focus trans type: N=New/Change, L=Cancel
			/// </summary>
			public String SU_TRANS_TYPE{ get; set; }

			/// <summary>
			/// Last date to extract claims for Contract and plan year
			/// </summary>
			public Int32 SU_FSA_YMDEND{ get; set; }

			/// <summary>
			/// Effectice or Start Date
			/// </summary>
			public Int32 YMDEFF{ get; set; }

			/// <summary>
			/// End Date
			/// </summary>
			public Int32 YMDEND{ get; set; }

			/// <summary>
			/// Date the contract is Cancelled.
			/// </summary>
			public Int32 YMDCANCELLED{ get; set; }

			/// <summary>
			/// Operator ID# of individual performing transaction update.
			/// </summary>
			public String OP_NBR{ get; set; }

			/// <summary>
			/// Transaction code.
    /// 			For AUTHORIZATION, ADMISSION, and AUTH-DETAIL,
    /// 			TRANSCODE position 1: A (add), C (change)
    /// 			position 2: B (batch), blank (online)
			/// </summary>
			public String TRANSCODE{ get; set; }

			/// <summary>
			/// Date of the Transaction
			/// </summary>
			public Int32 YMDTRANS{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
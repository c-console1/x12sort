using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// Dimensional Key for Account
		/// </summary>
		public class ACCOUNT_DIM{

			/// <summary>
			/// Dimensional Key for Account
			/// </summary>
			public Int64 ACCOUNT_DIM_KEY{ get; set; }

			/// <summary>
			/// Address for account correspondence.
			/// </summary>
			public Int64 ACCOUNT_ADDRESS_DIM_KEY{ get; set; }

			/// <summary>
			/// Key to the BUSINESS_SEGMENT_HISTORY_FACT table.
			/// </summary>
			public Int64 BUSINESS_SEGMENT_DIM_KEY{ get; set; }

			/// <summary>
			/// Business segment name.
			/// </summary>
			public String BUSINESS_SEGMENT_NAME{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 TAX_DIM_KEY{ get; set; }

			/// <summary>
			/// Tax ID of the supplier.
			/// </summary>
			public String TAX_ID{ get; set; }

			/// <summary>
			/// Tax entity name.
			/// </summary>
			public String TAX_NAME{ get; set; }

			/// <summary>
			/// Key to the Representative accosciated to the account.
			/// </summary>
			public Int64 ACCOUNT_REPRESENTATIVE_KEY{ get; set; }

			/// <summary>
			/// Primary key for INDUSTRY_CLASS_CODE table. Auto-generated surrogate key.
			/// </summary>
			public Int64 ACCOUNT_CLASS_CODE_KEY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 TOP_ACCOUNT_DIM_KEY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 PARENT_ACCOUNT_DIM_KEY{ get; set; }

			/// <summary>
			/// Account identifier specified by HCC.
			/// </summary>
			public String ACCOUNT_HCC_ID{ get; set; }

			/// <summary>
			/// Column for summing number of records in a query. No business application. Always has a value of 1.
			/// </summary>
			public Int64 ACCOUNT_HISTORY_FACT_COUNT{ get; set; }

			/// <summary>
			/// The Level of the Account.
			/// </summary>
			public Int64 ACCOUNT_LEVEL{ get; set; }

			/// <summary>
			/// The name of the immediate parent account.
			/// </summary>
			public String PARENT_ACCOUNT_NAMES{ get; set; }

			/// <summary>
			/// The business primary name. It is up to users to decide whether this is going to be a preferred name, legal name, or something else. This is the name that shows up in all search results and UI screens.
			/// </summary>
			public String ACCOUNT_NAME{ get; set; }

			/// <summary>
			/// Status (Active, Terminated, Unknown).
			/// </summary>
			public String ACCOUNT_STATUS_NAME{ get; set; }

			/// <summary>
			/// Name of the account type.
			/// </summary>
			public String ACCOUNT_TYPE_NAME{ get; set; }

			/// <summary>
			/// Name of the region (e.g., East, South, North, West).
			/// </summary>
			public String ACCOUNT_REGION_NAME{ get; set; }

			/// <summary>
			/// The associated authorization attachment set ID.
			/// </summary>
			public Int64 ATTACHMENT_SET_ID{ get; set; }

			/// <summary>
			/// Y or N flag indicating whether or not this account is billed. If BILLED equals N, then parent account is billed.
			/// </summary>
			public String BILLED{ get; set; }

			/// <summary>
			/// Billing cycle assigned to the account.
			/// </summary>
			public String BILLING_CYCLE_NAME{ get; set; }

			/// <summary>
			/// When there is a sub-account structure, members should only be attached to the low level sub-accounts. When CAN_HAVE_SUBSCR is set to false on a higher level accounts, subscriptions can be prevented from attaching to the higher levele accounts.
			/// </summary>
			public String CAN_HAVE_SUBSCR{ get; set; }

			/// <summary>
			/// Text description of an account or sub-account.
			/// </summary>
			public String DESCRIPTION{ get; set; }

			/// <summary>
			/// The number of employees and other personnel in the account who are eligible to be covered under the benefit plans of the account plan contract.
			/// </summary>
			public Int64 ELIGIBLE_EMPLOYEE_COUNT{ get; set; }

			/// <summary>
			/// The number of employees and other personnel that work for the account. This may include personnel such as contractors, temporary workers and interns.
			/// </summary>
			public Int64 EMPLOYEE_COUNT{ get; set; }

			/// <summary>
			/// Correspondance Address sames as Parent.
			/// </summary>
			public String GENERAL_CORR_SAME_AS_PARENT{ get; set; }

			/// <summary>
			/// Individual Broker Relationship.
			/// </summary>
			public String IS_INDIV_BROKER_RELSHIP{ get; set; }

			/// <summary>
			/// Indicates a VIP account.
			/// </summary>
			public String IS_VIP{ get; set; }

			/// <summary>
			/// Other Correspondance Address sames as Parent.
			/// </summary>
			public String OTHER_CORR_SAME_AS_PARENT{ get; set; }

			/// <summary>
			/// Renewal interval (in months). If this is left blank, then this will be interpreted as 12 months.
			/// </summary>
			public Int64 RENEWAL_INTERVAL{ get; set; }

			/// <summary>
			/// The retroactive billing period.
			/// </summary>
			public Int64 RETROACTIVE_BILLING_PRD{ get; set; }

			/// <summary>
			/// The retroactive changes subscription period.
			/// </summary>
			public Int64 RETROACTIVE_SUBS_CHNG_PRD{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 TENANT_HCC_ID{ get; set; }

			/// <summary>
			/// Line wise item for Account Billing
			/// </summary>
			public String USES_LINE_ITEM_FOR_ACCT_BLLNG{ get; set; }

			/// <summary>
			/// Line wise item for SUB - Account Billing
			/// </summary>
			public String USES_LINE_ITEM_FOR_SUB_BLLNG{ get; set; }

			/// <summary>
			/// Reason the account is a VIP account.
			/// </summary>
			public Int64 VIP_REASON_KEY{ get; set; }

			/// <summary>
			/// If account is in the workbasket, the currently assigned group.
			/// </summary>
			public String WB_GROUP{ get; set; }

			/// <summary>
			/// If account is in the workbasket, the currently assigned owner.
			/// </summary>
			public String WB_OWNER{ get; set; }

			/// <summary>
			/// Primary key for AUDIT_LOG table. Auto-generated surrogate key.
			/// </summary>
			public Int64 AUDIT_LOG_KEY{ get; set; }

			/// <summary>
			/// Start date of Account relationship with HCC.
			/// </summary>
			public Int32 ACCOUNT_EFFECTIVE_DATE{ get; set; }

			/// <summary>
			/// Last date of affilliation with HCC.
			/// </summary>
			public Int32 ACCOUNT_TERMINATION_DATE{ get; set; }

			/// <summary>
			/// Primary key for DATE_DIMENSION table. Auto-generated surrogate key.
			/// </summary>
			public Int32 FIRST_EFFECTIVE_DATE{ get; set; }

			/// <summary>
			/// First renewal date. Subsequent renewals occur in a period defined by RENEWAL_INTERVAL, on the day of the month of the first renewal date (typically the 1st of the month).
			/// </summary>
			public Int32 FIRST_RENEWAL_DATE{ get; set; }

			/// <summary>
			/// Subsequent renewal date if different from the first renewal date.
			/// </summary>
			public Int32 INTERVAL_RENEWAL_DATE{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
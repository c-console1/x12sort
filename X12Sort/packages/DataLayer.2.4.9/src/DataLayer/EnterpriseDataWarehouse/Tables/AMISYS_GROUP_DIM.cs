using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// Used internally by Cobol routines.
		/// </summary>
		public class AMISYS_GROUP_CORP_DETAIL{

			/// <summary>
			/// Used internally by Cobol routines.
			/// </summary>
			public Int64 IMAGE_RECNBR{ get; set; }

			/// <summary>
			/// Number assigned to employer groups.
    /// 			
    /// 			The number assigned to employer groups who have
    /// 			
    /// 			contracted with the health plan to provide health
    /// 			
    /// 			insurance coverage.
			/// </summary>
			public String GROUP_NBR{ get; set; }

			/// <summary>
			/// Corporation number or national account number.
    /// 			
    /// 			CORP# allows the healthplan to define a national
    /// 			
    /// 			account level that encompasses several employer groups.
			/// </summary>
			public String CORP_NBR{ get; set; }

			/// <summary>
			/// [GC] Group category.
    /// 			
    /// 			The [GC] group category codes define employer groups
    /// 			
    /// 			as the health plan requires. The health plan may
    /// 			
    /// 			decide to categorize their employer groups by size,
    /// 			
    /// 			union affiliation, commercial, etc.
			/// </summary>
			public String GROUP_CAT{ get; set; }

			/// <summary>
			/// Number of days delay before payment will be rendered.
			/// </summary>
			public Int64 LAG{ get; set; }

			/// <summary>
			/// Operator ID# of individual performing transaction update.
			/// </summary>
			public String OP_NBR{ get; set; }

			/// <summary>
			/// [RY] Reason for data entry or modification.
			/// </summary>
			public String REASON{ get; set; }

			/// <summary>
			/// Transaction code.
    /// 			
    /// 			For AUTHORIZATION, ADMISSION, and AUTH-DETAIL,
    /// 			
    /// 			TRANSCODE position 1: A (add), C (change)
    /// 			
    /// 			position 2: B (batch), blank (online)
			/// </summary>
			public String TRANSCODE{ get; set; }

			/// <summary>
			/// A 'V' in the first position indicates a voided record.
    /// 			
    /// 			A 'V' in the first position indicates that the record
    /// 			
    /// 			has been voided (most likely due to errors). Voided
    /// 			
    /// 			records are generally excluded from validation scans.
    /// 			
    /// 			For the CLEARCK-TAPE record, the second position of
    /// 			
    /// 			VOID indicates the reason for the void.
			/// </summary>
			public String VOID{ get; set; }

			/// <summary>
			/// Effective or start date.
    /// 			
    /// 			For UB-92 this is From Date- date the occurrence starts
    /// 			
    /// 			The UB-92 from date is the date the occurrence is for.
    /// 			
    /// 			Occur date corresponds to the occur code event relating
    /// 			
    /// 			to this bill. This 'from date' must not be later than
    /// 			
    /// 			a 'thru date' when a thru date is entered.
    /// 			
    /// 			Refer to UB92 locator FIELD 32a-36a and 32b-36b
    /// 			
    /// 			of service. A one position, alpha numeric value. See
    /// 			
    /// 			UB92 FIELD locator 32 through 36 for value.
    /// 			
    /// 			For PROV-CON-ED, YMDEFF is the start date for the class
    /// 			
    /// 			or seminar.
			/// </summary>
			public Int32 YMDEFF{ get; set; }

			/// <summary>
			/// For UB-92 this is Thru Date- date the occurrence ends.
    /// 			
    /// 			The UB-92 thru date is the date the occurrence is for.
    /// 			
    /// 			Occur date corresponds to the occur code event relating
    /// 			
    /// 			to this bill. This 'thru date' must not be less than
    /// 			
    /// 			a 'from date' when a thru date is entered.
    /// 			
    /// 			Refer to UB92 locator FIELDS 32a-36a and 32b-36b values
			/// </summary>
			public Int32 YMDEND{ get; set; }

			/// <summary>
			/// Date of most recent transaction.
			/// </summary>
			public Int32 YMDTRANS{ get; set; }

			/// <summary>
			/// Counter incremented via trigger after any updates.
    /// 			
    /// 			Used by the JAVA to duplicate the 'record has changed
    /// 			
    /// 			since you found it' feature of AMISYS 3000 and
    /// 			
    /// 			AMISYS Advance Release 1 that is provided by the Cognos
    /// 			
    /// 			Quick screens.
			/// </summary>
			public Decimal CHECKSUM_VALUE{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
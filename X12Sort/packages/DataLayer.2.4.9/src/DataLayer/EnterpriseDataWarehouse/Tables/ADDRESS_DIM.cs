using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// Key to the Account plan details
		/// </summary>
		public class ACCOUNT_TO_BENEFIT_PLAN_DETAILS{

			/// <summary>
			/// Key to the Account plan details
			/// </summary>
			public Int64 ACCOUNT_PLAN_DETAILS_DIM_KEY{ get; set; }

			/// <summary>
			/// Fact Key to the Account plan details
			/// </summary>
			public Int64 ACCOUNT_PLAN_DETAILS_KEY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 PARENT_ACCOUNT_DIM_KEY{ get; set; }

			/// <summary>
			/// Key to the Account
			/// </summary>
			public Int64 ACCOUNT_DIM_KEY{ get; set; }

			/// <summary>
			/// Key relation to Benefit plan history fact
			/// </summary>
			public Int64 BENEFIT_PLAN_DIM_KEY{ get; set; }

			/// <summary>
			/// Prefix key for the Account Plan
			/// </summary>
			public Int64 ID_PREFIX_KEY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 TENANT_HCC_ID{ get; set; }

			/// <summary>
			/// 
			/// </summary>
			public Int64 ACCOUNT_PLAN_DETAILS_COUNT{ get; set; }

			/// <summary>
			/// Primary key for AUDIT_LOG table. Auto-generated surrogate key.
			/// </summary>
			public Int64 AUDIT_LOG_KEY{ get; set; }

			/// <summary>
			/// The start of the date range. Joins to the DATE_DIMENSION table.
			/// </summary>
			public Int32 START_DATE{ get; set; }

			/// <summary>
			/// The end of the date range. Joins to the DATE_DIMENSION table.
			/// </summary>
			public Int32 END_DATE{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
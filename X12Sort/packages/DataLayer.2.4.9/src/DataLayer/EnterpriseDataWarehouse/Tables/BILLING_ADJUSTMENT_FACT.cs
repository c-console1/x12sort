using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// Bill Line Fact Key
		/// </summary>
		public class BILL_LINE_FACT{

			/// <summary>
			/// Bill Line Fact Key
			/// </summary>
			public Int64 BILL_LINE_FACT_KEY{ get; set; }

			/// <summary>
			/// Bill Fact Key
			/// </summary>
			public Int64 BILL_FACT_KEY{ get; set; }

			/// <summary>
			/// Account Dimension key
			/// </summary>
			public Int64 ACCOUNT_DIM_KEY{ get; set; }

			/// <summary>
			/// Subscriber Dimension key
			/// </summary>
			public Int64 SUBSCRIBER_DIM_KEY{ get; set; }

			/// <summary>
			/// member Dimension key
			/// </summary>
			public Int64 MEMBER_DIM_KEY{ get; set; }

			/// <summary>
			/// Benefit Plan Dimension key
			/// </summary>
			public Int64 BENEFIT_PLAN_DIM_KEY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 BILLING_RATE_DIM_KEY{ get; set; }

			/// <summary>
			/// Premium Pay Receive Fact Key
			/// </summary>
			public Int64 PREMIUM_PAY_RECEIVE_FACT_KEY{ get; set; }

			/// <summary>
			/// Claim ID.
			/// </summary>
			public String CLAIM_HCC_ID{ get; set; }

			/// <summary>
			/// Bill line fact count.
			/// </summary>
			public Int64 BILL_LINE_FACT_COUNT{ get; set; }

			/// <summary>
			/// Bill line type Name
			/// </summary>
			public String BILL_LINE_TYPE_NAME{ get; set; }

			/// <summary>
			/// Reason code describing reason for retro billing adjustment.
			/// </summary>
			public String BILL_RETRO_ADJ_REASON_NAME{ get; set; }

			/// <summary>
			/// Name of billing category (e.g., Reinsurance, Premium).
			/// </summary>
			public String BILLING_CATEGORY_NAME{ get; set; }

			/// <summary>
			/// Bill percentage.
			/// </summary>
			public Decimal BILL_PERCENTAGE{ get; set; }

			/// <summary>
			/// The billed amount.
			/// </summary>
			public Decimal BILLED_AMOUNT{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Decimal CLAIM_PAID_AMOUNT{ get; set; }

			/// <summary>
			/// The original bill amount.
			/// </summary>
			public Decimal ORIGINAL_BILLED_AMOUNT{ get; set; }

			/// <summary>
			/// Coverage tier name.
			/// </summary>
			public String COVERAGE_TIER_NM{ get; set; }

			/// <summary>
			/// Indicates that bill is retroactive.
			/// </summary>
			public String IS_RETROACTIVE{ get; set; }

			/// <summary>
			/// Indicates that the bill is for statistical purposes only. No financials are adjusted for statistical bills.
			/// </summary>
			public String IS_STATISTICAL{ get; set; }

			/// <summary>
			/// Billing period end date.
			/// </summary>
			public Int32 BILLED_PERIOD_END_DATE{ get; set; }

			/// <summary>
			/// Billing period start date.
			/// </summary>
			public Int32 BILLED_PERIOD_START_DATE{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
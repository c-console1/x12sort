using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// 
		/// </summary>
		public class AMISYS_RECEIVABLE_FACT{

			/// <summary>
			/// 
			/// </summary>
			public Int64 IMAGE_RECNBR{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 ENTITY_level{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String CORP_NBR{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String GROUP_NBR{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String DIVISION_NBR{ get; set; }

			/// <summary>
			/// Turns on/off auto advance paid through date processing.
    /// 			Denotes whether the billed-to entity will be excluded from using the automatic
    /// 			calculation of paid-through date enhancement.Valid values are "Y" or blank
			/// </summary>
			public String AUTOCALC_EXCLUDE{ get; set; }

			/// <summary>
			/// The 'Previous Balance' value on most current invoice.
    /// 			The BALFORWARD on the billing entity record on the
    /// 			HEALTH database represents the amount due at the time
    /// 			of the last billing post process, shown as the 'Please
    /// 			Pay' amount on the last invoice. This value is carried
    /// 			forward to the next billing post process, and displayed
    /// 			on the next invoice as the 'Previous Balance'.
    /// 			The billing entity record is updated during the billing
    /// 			post process as follows:
    /// 			BALFORWARD: old PREVBALANCE
    /// 			+ old PREMIUMS
    /// 			PREVBALANCE: old PREVBALANCE
    /// 			+ old PREMIUMS
    /// 			+ old ADJUSTMENTS (the manual adjustments)
    /// 			+ new automatic adjustments calc by billing
    /// 			- old PAYMENTS
    /// 			PAYMENTS: 0
    /// 			ADJUSTMENTS: 0
    /// 			PREMIUMS: new invoice premiums calc by billing
			/// </summary>
			public Int64 BALFORWARD{ get; set; }

			/// <summary>
			/// Individual to whom all billing inquiries are directed.
			/// </summary>
			public String BILLCONTACT{ get; set; }

			/// <summary>
			/// Flag indicating this billed entity will not be included in the
    /// 			current run of the bill cycle.When the flag is turned off, the
    /// 			entity can be billed by itself outside of the cycle
			/// </summary>
			public String BILL_EXCLUDE{ get; set; }

			/// <summary>
			/// Indicates the receivable record is locked by the billing extract process
			/// </summary>
			public String BILL_LOCK{ get; set; }

			/// <summary>
			/// Billing remark to be displayed on billing invoices.
    /// 			
			/// </summary>
			public String BILLREMARK{ get; set; }

			/// <summary>
			/// [AC] Billing cycle or [CX] Cap cycle.
    /// 			CORPORATION: [AC] Billing cycle.
    /// 			DIVISION: [AC] Billing cycle.
    /// 			GROUP-M: [AC] Billing cycle.
    /// 			INVOICE-DETAIL: [AC] Billing cycle.
    /// 			AFFILIATION: [CX] Capitation cycle.
    /// 			AFFTRAN: [CX] Capitation cycle.
			/// </summary>
			public String CYCLE{ get; set; }

			/// <summary>
			/// Day the cycle begins.
			/// </summary>
			public Int64 CYCLE_DAY{ get; set; }

			/// <summary>
			/// Number of months in the billing cycle.
			/// </summary>
			public Int64 CYCLE_MONTHS{ get; set; }

			/// <summary>
			/// Electronic fund transfer information.
    /// 			Electronic fund transfer information.
    /// 			The first position describes the EFT status for the
    /// 			billing entity. The user can set the EFT-INFO value
    /// 			manually for the purpose of suspending and
    /// 			re-activating the Electronic Fund Transfer processing
    /// 			for a billing entity. Otherwise, the value is
    /// 			automatically maintained by the billing process.
    /// 			The billing process evaluates all EFT-BANK records with
    /// 			an EFT-INFO value of Y, P, or S. Based on a comparison
    /// 			of the billing due date with EFT-BANK.YMDEND and
    /// 			EFT-BANK.YMDPRENOTE, the billing process determines
    /// 			any updates required for EFT-INFO. The billing entity
    /// 			record on the HEALTH database is then updated based on
    /// 			the EFT-BANK updates. The EFT-INFO value on the HEALTH
    /// 			database determines if EFT logic is used in the
    /// 			generation of invoices.
    /// 			The second position directs the EFT logic when a
    /// 			previous balance exists (that is the result of
    /// 			insufficient funds during EFT processing). Once a
    /// 			previous balance becomes current, the 'Y' or 'N' is
    /// 			updated to blank.
    /// 			Position 1: 'Y' indicates the Corporation, Division,
    /// 			Group, or Contract currently utilizes EFT.
    /// 			' ' indicates the Corporation, Division,
    /// 			Group, or Contract does not utilize EFT.
    /// 			'N' indicates the EFT authorization for the
    /// 			Corporation, Division, Group, or
    /// 			Contract has expired.
    /// 			'P' indicates EFT is pending. The EFT will
    /// 			become effective in the first billing
    /// 			period after the authorized effective
    /// 			date.
    /// 			'S' indicates the EFT utilization has been
    /// 			suspended.
    /// 			Position 2: 'Y' indicates the next EFT should include
    /// 			the previous balance.
    /// 			'N' indicates the next EFT should not
    /// 			include the previous balance.
			/// </summary>
			public String EFT_INFO{ get; set; }

			/// <summary>
			/// Unbilled manual adjustments
			/// </summary>
			public Int64 MISC_CHARGES{ get; set; }

			/// <summary>
			/// Operator ID# of individual performing transaction update.
			/// </summary>
			public String OP_NBR{ get; set; }

			/// <summary>
			/// Total amount paid since last billing.
    /// 			The value reflects the total amount paid since the last
    /// 			billing post process. The billing post process will
    /// 			reset this value to zero.
    /// 			The billing entity record is updated during the billing
    /// 			post process as follows:
    /// 			BALFORWARD: old PREVBALANCE
    /// 			+ old PREMIUMS
    /// 			PREVBALANCE: old PREVBALANCE
    /// 			+ old PREMIUMS
    /// 			+ old ADJUSTMENTS (the manual adjustments)
    /// 			+ new automatic adjustments calc by billing
    /// 			- old PAYMENTS
    /// 			PAYMENTS: 0
    /// 			ADJUSTMENTS: 0
    /// 			PREMIUMS: new invoice premiums calc by billing
			/// </summary>
			public Int64 PAYMENTS{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 PERCENT{ get; set; }

			/// <summary>
			/// Total of all premiums from the last billing post process.
    /// 			The premium total is calculated by the billing process.
    /// 			The premium is recalculated for each run.
    /// 			The billing entity record is updated during the billing
    /// 			post process as follows:
    /// 			BALFORWARD: old PREVBALANCE
    /// 			+ old PREMIUMS
    /// 			PREVBALANCE: old PREVBALANCE
    /// 			+ old PREMIUMS
    /// 			+ old ADJUSTMENTS (the manual adjustments)
    /// 			+ new automatic adjustments calc by billing
    /// 			- old PAYMENTS
    /// 			PAYMENTS: 0
    /// 			ADJUSTMENTS: 0
    /// 			PREMIUMS: new invoice premiums calc by billing
			/// </summary>
			public Int64 PREMIUMS{ get; set; }

			/// <summary>
			/// The 'Balance Forward' amount on the most current invoice.
    /// 			The billing entity record is updated during the billing
    /// 			post process as follows:
    /// 			BALFORWARD: old PREVBALANCE
    /// 			+ old PREMIUMS
    /// 			PREVBALANCE: old PREVBALANCE
    /// 			+ old PREMIUMS
    /// 			+ old ADJUSTMENTS (the manual adjustments)
    /// 			+ new automatic adjustments calc by billing
    /// 			- old PAYMENTS
    /// 			PAYMENTS: 0
    /// 			ADJUSTMENTS: 0
    /// 			PREMIUMS: new invoice premiums calc by billing
			/// </summary>
			public Int64 PREVBALANCE{ get; set; }

			/// <summary>
			/// Automatic or system adjustments invoiced on the last bill
			/// </summary>
			public Int64 PRIOR_ADJ{ get; set; }

			/// <summary>
			/// Manual adjustmented invoiced on the last bill
			/// </summary>
			public Int64 PRIOR_MISCHARGES{ get; set; }

			/// <summary>
			/// Retro Limit in months.
    /// 			Defines the number of months to allow adjustments for
    /// 			retroactive Additions and Terminations for a Division.
    /// 			Overrides the Business Segment Retro Limit Parameter.
			/// </summary>
			public String RETRO_LIMIT{ get; set; }

			/// <summary>
			/// Defines the billing roster sort order (C/CY/N/NY).
    /// 			Defines how to sort the billing roster.
    /// 			C - Sort billing roster by CONTRACT#
    /// 			CY - Sort billing roster by SUBSCRIBERTYPE then CONTRACT#
    /// 			N - Sort billing roster by contract LASTNAME
    /// 			NY - Sort billing roster by SUBSCRIBERTYPE then LASTNAME
			/// </summary>
			public String ROSTER_SORT{ get; set; }

			/// <summary>
			/// Y' indicates the Corp is a 3rd Party billed entity.
    /// 			A 'Y' indicates the Corporation is a Third Party
    /// 			billed entity.
			/// </summary>
			public String THIRDPARTY{ get; set; }

			/// <summary>
			/// Percentage of invoice amount due for auto advance paidthru.
    /// 			Contains the threshold percentage value for invoices.
    /// 			This value will denote the percentage of an invoice
    /// 			amount due that is required for the invoice to be considered satisfied.
    /// 			Valid values are a 3-digit integer from "001" through "100", with no decimal places, or blank.
			/// </summary>
			public String THRESHOLD_PCT{ get; set; }

			/// <summary>
			/// For AUTHORIZATION, ADMISSION, and AUTH-DETAIL,
    /// 			TRANSCODE position 1: A (add), C (change)
    /// 			position 2: B (batch), blank (online)
			/// </summary>
			public String TRANSCODE{ get; set; }

			/// <summary>
			/// Last date the division was billed
			/// </summary>
			public Int32 YMDBILLEDTHRU{ get; set; }

			/// <summary>
			/// 
			/// </summary>
			public Int32 YMDBROKER{ get; set; }

			/// <summary>
			/// Date through which the division has paid.
    /// 			
			/// </summary>
			public Int32 YMDPAIDTHRU{ get; set; }

			/// <summary>
			/// Flag indicating whether paid through date was manually set.
    /// 			Denotes whether the paid-through date for the billed-to entity was updated
    /// 			manually (by an online user) or automatically (during
    /// 			batch invoice payment or batch cash application).
    /// 			Valid values: A (automatic) or M (manual)
			/// </summary>
			public String YMDPAIDTHRU_FLAG{ get; set; }

			/// <summary>
			/// Id of last person to update through date.
    /// 			The batch program name or operator-id (OP_NBR) that last updated
    /// 			the paid-throughdate.
			/// </summary>
			public String YMDPAIDTHRU_PROG{ get; set; }

			/// <summary>
			/// Date of the Transaction
			/// </summary>
			public Int32 YMDTRANS{ get; set; }

			/// <summary>
			/// Counter incremented via trigger after any updates.
    /// 			
    /// 			Used by the JAVA to duplicate the 'record has changed
    /// 			
    /// 			since you found it' feature of AMISYS 3000 and
    /// 			
    /// 			AMISYS Advance Release 1 that is provided by the Cognos
    /// 			
    /// 			Quick screens.
			/// </summary>
			public Decimal CHECKSUM_VALUE{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// Corporation number or national account number.
    /// 			CORP# allows the healthplan to define a national
    /// 			account level that encompasses several employer groups.
		/// </summary>
		public class AMISYS_CORPORATION_DIM{

			/// <summary>
			/// Corporation number or national account number.
    /// 			CORP# allows the healthplan to define a national
    /// 			account level that encompasses several employer groups.
			/// </summary>
			public Int64 IMAGE_RECNBR{ get; set; }

			/// <summary>
			/// Defines the billing invoice format.
    /// 			BILL-FORMAT allows the selection of one of six
    /// 			invoice formats for 'rollup' billing. The formats
    /// 			are as follows:
    /// 			' ' = Defaults to format '1'
    /// 			1 = Division / Group / Corporation
    /// 			All data is merged into one invoice package
    /// 			for the billed entity.
    /// 			The invoice package includes:
    /// 			Premium Roster
    /// 			Adjustment Roster
    /// 			Summary Report
    /// 			Balance Due Page
    /// 			2 = Group / Corporation
    /// 			Only one invoice package for the billed
    /// 			entity is produced, with the addition of
    /// 			division subtotals on the premium and
    /// 			adjustment rosters.
    /// 			The invoice package includes:
    /// 			Premium Roster with Division Subtotals
    /// 			Adjustment Roster with Division Subtotals
    /// 			Summary Report
    /// 			Balance Due Page
    /// 			3 = Group / Corporation
    /// 			No merge of data. Each division receives a
    /// 			separate invoice package, but all divisions
    /// 			share a common invoice#. There is a final
    /// 			summary and balance due page that combines all
    /// 			division information rolling up to the group
    /// 			or corporation.
    /// 			The invoice package includes:
    /// 			Premium Roster with Division Totals
    /// 			Adjustment Roster with Division Totals
    /// 			Summary Report for Division
    /// 			Balance Due Page for Division
    /// 			* Final Summary Report combining all Divisions
    /// 			* Final Balance Due Page combining all Divisions
    /// 			4 = Corporation
    /// 			Only one invoice package for the billed
    /// 			entity is produced with group subtotals on
    /// 			the premium and adjustment rosters.
    /// 			The invoice package includes:
    /// 			Premium Roster with Group Subtotals
    /// 			Adjustment Roster with Group Subtotals
    /// 			Summary Report
    /// 			Balance Due Page
    /// 			5 = Corporation
    /// 			No merge of data. Each group rolling up to
    /// 			the corporation has a separate invoice
    /// 			package. All groups share a common invoice#.
    /// 			There is a final summary and balance due
    /// 			page for the corporation that combines all
    /// 			the group information.
    /// 			The invoice package includes:
    /// 			Premium Roster with Group Totals
    /// 			Adjustment Roster with Group Totals
    /// 			Summary Report for the Group
    /// 			Balance Due Page for the Group
    /// 			* Final Summary Report combining all Groups
    /// 			* Final Balance Due Page combining all Groups
    /// 			6 = Corporation
    /// 			No merge of data. Each group level premium
    /// 			and adjustment rosters also have division
    /// 			subtotals. Each group rolling up to the
    /// 			corporation gets a separate invoice package.
    /// 			All groups share a common invoice#. There
    /// 			is a final summary and balance due page for
    /// 			the corporation that combines all the group
    /// 			information.
    /// 			The invoice package includes:
    /// 			Premium Roster w/ Division Subtot and Group Tot
    /// 			Adjustment Roster w/ Division Subtot and Group Tot
    /// 			Summary Report for the Group
    /// 			Balance Due Page for the Group
    /// 			* Final Summary Report combining all Groups
    /// 			* Final Balance Due Page combining all Groups
			/// </summary>
			public String CORP_NBR{ get; set; }

			/// <summary>
			/// Name associated with the given entity.
    /// 			The name associated with the given entity for the
    /// 			specific application. This could be the name of the
    /// 			employer group, division, corporation, etc.
			/// </summary>
			public String BILL_FORMAT{ get; set; }

			/// <summary>
			/// Operator ID# of individual performing transaction update.
			/// </summary>
			public String BILL_METHOD{ get; set; }

			/// <summary>
			/// Defines the Employer Identification Number
    /// 			Defines the Employer Identification Number.
    /// 			EIN is a 9 digit number assigned by the Internal
    /// 			Revenue Service. The EIN is formatted as nn-nnnnnnn,
    /// 			however, formatting characters are not included in
    /// 			AMISYS. An EIN may also be referred to as a Taxpayer
    /// 			Identification Number (TIN). The EIN is synonymous
    /// 			with the HIPAA X12 National Employer ID (NEI).
			/// </summary>
			public String BUSINESS_UNIT{ get; set; }

			/// <summary>
			/// Positional flags to indicate if a given record is
    /// 			locked due to processing requirements.
    /// 			For the CLAIM table, LOCK positions 5 and 6 are
    /// 			reserved for user entered EXPLAIN codes for *'d status
    /// 			values.
    /// 			For the CONTRACT and MEMBER tables,
    /// 			if position 1 = Y, then record is locked by billing
    /// 			if position 2 = Y, then record is locked by capitation
    /// 			if position 3 = Y, then record is locked by membership
			/// </summary>
			public String CARRIER{ get; set; }

			/// <summary>
			/// The independent geographic or profit center entities
    /// 			within the health plan with which members and providers
    /// 			are affiliated.
			/// </summary>
			public String CONTACT{ get; set; }

			/// <summary>
			/// Corporation category id for CG processing.
			/// </summary>
			public String CORP_CAT{ get; set; }

			/// <summary>
			/// Identifies the carrier / underwriter of risk and how
    /// 			the risk will be managed. Valid values are defined
    /// 			in the SUPER-TABLE table.
			/// </summary>
			public String EIN{ get; set; }

			/// <summary>
			/// Identifies health plan, IPA, and/or program area
			/// </summary>
			public String LOCK_X{ get; set; }

			/// <summary>
			/// Help: Prorate/Wash/1st of Mo/Prorate rate chg/Full Mo/Full Per
    /// 			Billing method or invoice calculation method.
    /// 			1 = PRORATE
    /// 			2 = WASH METHOD
    /// 			3 = 1ST OF THE MONTH
    /// 			4 = PRORATE WITH RATE CHANGE - a special form of
    /// 			proration that allows one or more rate changes
    /// 			in the billing cycle.
    /// 			5 = FULL MONTH - if a Contract is effective at least
    /// 			one day in a month, bill the Division for a full
    /// 			months premium.
    /// 			6 = FULL PERIOD - if a Contract is effective at least
    /// 			one day in a non-monthly billing period, then
    /// 			bill the Division for the full period premium.
    /// 			7 = BILLING PERIOD - for billing periods defined in
    /// 			PERIOD-DETAIL and related to the Calendar Month
    /// 			of Coverage in the Cycle (AC) record keywords in
    /// 			CODE-DETAIL.
			/// </summary>
			public String NAME_X{ get; set; }

			/// <summary>
			/// Regional area associated with the specified entity.
			/// </summary>
			public String OP_NBR{ get; set; }

			/// <summary>
			/// Transaction code.
    /// 			For AUTHORIZATION, ADMISSION, and AUTH-DETAIL,
    /// 			TRANSCODE position 1: A (add), C (change)
			/// </summary>
			public String PROG_NBR{ get; set; }

			/// <summary>
			/// Customer representative to whom inquires are directed.
			/// </summary>
			public String REGION{ get; set; }

			/// <summary>
			/// Date of most recent transaction.
			/// </summary>
			public String TRANSCODE{ get; set; }

			/// <summary>
			/// Counter incremented via trigger after any updates.
    /// 			Used by the JAVA to duplicate the 'record has changed
    /// 			since you found it' feature of AMISYS 3000 and
    /// 			AMISYS Advance Release 1 that is provided by the Cognos
    /// 			Quick screens.
			/// </summary>
			public Int64 YMDTRANS{ get; set; }

			/// <summary>
			/// Used internally by Cobol routines.
			/// </summary>
			public Int64 CHECKSUM_VALUE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public DateTime RunTimeNow{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String TBLNM{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int64 BATCH_ID{ get; set; }

	}
}
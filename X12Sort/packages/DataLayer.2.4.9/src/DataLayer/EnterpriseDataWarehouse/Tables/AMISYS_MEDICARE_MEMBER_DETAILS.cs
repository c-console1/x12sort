using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// No Description Found
		/// </summary>
		public class AMISYS_INVOICE_FACT{

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 AMISYS_INVOICE_FACT_KEY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 SOURCE_KEY{ get; set; }

			/// <summary>
			/// 
    /// 			BU/PG specific, overlapping sequential invoice numbers.
    /// 			Structure is: x(4) table# + 9(8) sequential number.
			/// </summary>
			public String INVOICE_NBR{ get; set; }

			/// <summary>
			/// Billing entity for the premium billing process.
    /// 			Defines the billing entity for the premium billing
    /// 			process. The billing entity can be the corporation,
    /// 			group, division, or contract.
			/// </summary>
			public String ACCT_ID_NBR{ get; set; }

			/// <summary>
			/// Accounting month for this transaction, YYYYMM format.
    /// 			Identifies the accounting month in which the transaction
    /// 			occurred.
			/// </summary>
			public String ACCT_MONTH{ get; set; }

			/// <summary>
			/// Identifies the dollar amount for the given transaction.
    /// 			For CP0500M, the AMOUNTACT is the sum of SERVICE.AMTPAY
    /// 			for all services of the claim. The AMOUNTs of all the
    /// 			claims to be paid manually are then totaled, and the
    /// 			the value is posted to CHECK.AMOUNT.
    /// 			For CAPADJ-FUND, the AMOUNTACT is the dollar amount
    /// 			applied to a fund.
    /// 			For CAPADJ, the AMOUNTACT is the total amount of the
    /// 			of the manual capitation adjustment. This total amount
    /// 			represents the sum total of the amounts on the related
    /// 			CAPADJ-FUND records.
    /// 			For IQ3390M, AMOUNT is the total of COUNTER.AMOUNTs.
			/// </summary>
			public Int64 AMOUNTACT{ get; set; }

			/// <summary>
			/// Adjusted billing amount of invoice or stop loss claim.
    /// 			For BILLING, AMT-ADJS represents the total dollar
    /// 			amount of all system generated adjustments plus all
    /// 			non-invoice specific manual adjustments.
    /// 			For STOP LOSS, AMT-ADJS is the negative or positive
    /// 			adjustment amount for the claim.
			/// </summary>
			public Int64 AMT_ADJS{ get; set; }

			/// <summary>
			/// Amount of liability.
    /// 			For INV-AR-RIDERS, the amount that may be applied to
    /// 			RIDER-TITLE1.
    /// 			For INVOICE-DETAIL, the dollar amount applied against
    /// 			an invoice.
			/// </summary>
			public Int64 AMT_APPLY1{ get; set; }

			/// <summary>
			/// Manual adjustment amount for invoice adjustments.
    /// 			The total dollar amount from any invoice specific
    /// 			manual adjustment made to a prior invoice.
			/// </summary>
			public Int64 AMT_MAN_ADJS{ get; set; }

			/// <summary>
			/// Amount paid against the invoice
			/// </summary>
			public Int64 AMT_PAID{ get; set; }

			/// <summary>
			/// Current premium billing amount.
			/// </summary>
			public Int64 AMT_PREMS{ get; set; }

			/// <summary>
			/// Amount of retroactive payment applied to old invoices.
    /// 			The total dollar amount from any invoice specific
    /// 			adjustment that will display on the current invoice.
			/// </summary>
			public Int64 AMT_RETRO{ get; set; }

			/// <summary>
			/// The G/L accounts receivable account number derived from the
    /// 			ACCOUNT-DEF record
			/// </summary>
			public String AR_ACCOUNT_NBR{ get; set; }

			/// <summary>
			/// Accounts Receivable recognition switch derived from the
    /// 			SUPER-TABLE entry of BASWITCHES,
    /// 			C = book A/R in current month (ACCT-MONTH)
    /// 			R = book A/R in revenue month (MONTH)
			/// </summary>
			public String AR_RECOGNITION{ get; set; }

			/// <summary>
			/// The entity (Corp/Group/Division/Indiv) who is to be billed.
    /// 			C = Corporation level billing
    /// 			G = Group level billing
    /// 			D = Division level billing
    /// 			I = Individual billing
    /// 			For the TICKLER and LETTER-STATUS tables, the BILL-TO
    /// 			value shows the billing entity who has a delinquent
    /// 			amount due, and was sent a dunning letter.
			/// </summary>
			public String BILL_TO{ get; set; }

			/// <summary>
			/// Independent geographic or profit center.
    /// 			The independent geographic or profit center entities
    /// 			within the health plan with which members and providers
    /// 			are affiliated.
			/// </summary>
			public String BUSINESS_UNIT{ get; set; }

			/// <summary>
			/// Carrier / underwriter of risk.
    /// 			Identifies the carrier / underwriter of risk and how
    /// 			the risk will be managed. Valid values are defined
    /// 			in the SUPER-TABLE table.
			/// </summary>
			public String CARRIER{ get; set; }

			/// <summary>
			/// [AC] Billing cycle or [CX] Cap cycle.
    /// 			CORPORATION: [AC] Billing cycle.
    /// 			DIVISION: [AC] Billing cycle.
    /// 			GROUP-M: [AC] Billing cycle.
    /// 			INVOICE-DETAIL: [AC] Billing cycle.
    /// 			AFFILIATION: [CX] Capitation cycle.
    /// 			AFFTRAN: [CX] Capitation cycle.
			/// </summary>
			public String CYCLE{ get; set; }

			/// <summary>
			/// Identification number of the employer division.
    /// 			Identification number assigned to employer group
    /// 			divisions. For ease of identification, the DIVISION#
    /// 			consists of GROUP#, X(6) plus a 4 character extension.
			/// </summary>
			public String DIVISION_NBR{ get; set; }

			/// <summary>
			/// Reference number assigned to invoice by outside source.
    /// 			An external reference number from an outside source
    /// 			that references an AMISYS invoice number.
			/// </summary>
			public String EXTERNAL_REF_NBR{ get; set; }

			/// <summary>
			/// Month affected by this transaction, format YYYYMM.
			/// </summary>
			public String MONTH_X{ get; set; }

			/// <summary>
			/// Identifies health plan, IPA, and/or program area
			/// </summary>
			public String PROG_NBR{ get; set; }

			/// <summary>
			/// [RY] Reason for data entry or modification.
			/// </summary>
			public String REASON{ get; set; }

			/// <summary>
			/// Regional area associated with the specified entity.
			/// </summary>
			public String REGION{ get; set; }

			/// <summary>
			/// X(4) Table# + Audit trail or expensed entry number.
    /// 			For the PREMCASH-RECEIPT table, TRANS# contains
    /// 			DIVISION# when BILL-TO = ''I ''.
			/// </summary>
			public String TRANS_NBR{ get; set; }

			/// <summary>
			/// Represents the cost center, department, division, etc.
    /// 			The unit code that represents the cost center,
    /// 			department, division, and/or company.
			/// </summary>
			public String UNIT{ get; set; }

			/// <summary>
			/// Unearned revenue G/L account# derived from ACCOUNT-DEF.
			/// </summary>
			public String UR_ACCOUNT_NBR{ get; set; }

			/// <summary>
			/// Date by which service is to be paid.
			/// </summary>
			public Int32 YMDDUE{ get; set; }

			/// <summary>
			/// Date paid for the CHECK table, YMDPAID is the date that is
    /// 			printed on the check.
			/// </summary>
			public Int32 YMDPAID{ get; set; }

			/// <summary>
			/// Date processed.
    /// 			For the CHECK table, YMDPROCESS is the system date
    /// 			of the day the CHECK record was created. The value is
    /// 			never updated (it retains the original creation date).
    /// 			For the IN-PROCESS table, the YMDPROCESS is set
    /// 			equal to the system date of the day the non-liable EOB
    /// 			report is printed for the first time. It remains
    /// 			unchanged for any subsequent reruns.
			/// </summary>
			public Int32 YMDPROCESS{ get; set; }

			/// <summary>
			/// Counter incremented via trigger after any updates.
    /// 			Used by the JAVA to duplicate the ''record has changed
    /// 			since you found it'' feature of AMISYS 3000 and
    /// 			AMISYS Advance Release 1 that is provided by the Cognos
    /// 			Quick screens.
			/// </summary>
			public Decimal CHECKSUM_VALUE{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
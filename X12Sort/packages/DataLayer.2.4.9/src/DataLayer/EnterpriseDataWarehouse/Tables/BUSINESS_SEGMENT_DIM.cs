using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// Primary key for BROKER_HISTORY_FACT table. Auto-generated surrogate key.
		/// </summary>
		public class BROKER_DIM{

			/// <summary>
			/// Primary key for BROKER_HISTORY_FACT table. Auto-generated surrogate key.
			/// </summary>
			public Int64 BROKER_DIM_KEY{ get; set; }

			/// <summary>
			/// Correspondence address for the broker. Joins to the POSTAL_ADDRESS table.
			/// </summary>
			public Int64 BROKER_ADDRESS_DIM_KEY{ get; set; }

			/// <summary>
			/// Broker Agent Key
			/// </summary>
			public Int64 BROKER_KEY{ get; set; }

			/// <summary>
			/// Broker Agent ID
			/// </summary>
			public String BROKER_HCC_ID{ get; set; }

			/// <summary>
			/// Health Insurance Company relation with the broker (e.g., Active, Referred and Recommended, Application in Progress, Completed Contract, Training)
			/// </summary>
			public String BROKER_HIC_RELATION_NAME{ get; set; }

			/// <summary>
			/// Name of broker.
			/// </summary>
			public String BROKER_NAME{ get; set; }

			/// <summary>
			/// Status of the broker (a - Active, t - Terminated, p - Pending, u - Not Specified).
			/// </summary>
			public String BROKER_STATUS_NAME{ get; set; }

			/// <summary>
			/// Status (Active, Terminated, Unknown).
			/// </summary>
			public Int64 BROKER_HISTORY_FACT_COUNT{ get; set; }

			/// <summary>
			/// Contact name at broker.
			/// </summary>
			public String BROKER_CONTACT_NAME{ get; set; }

			/// <summary>
			/// Telephone number of the broker.
			/// </summary>
			public String TELEPHONE_NUMBER{ get; set; }

			/// <summary>
			/// Email address for the broker.
			/// </summary>
			public String EMAIL_ADDRESS{ get; set; }

			/// <summary>
			/// Values are: Not Applicable Not Specified ACH Wire Check EFT
			/// </summary>
			public String PAYMENT_TYPE_NAME{ get; set; }

			/// <summary>
			/// Values are: Not Specified Not Applicable Paper EDI 835 Both
			/// </summary>
			public String REMITTANCE_TYPE_NAME{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 TAX_DIM_KEY{ get; set; }

			/// <summary>
			/// Broker TAX ID
			/// </summary>
			public String TAX_ID{ get; set; }

			/// <summary>
			/// Tax entity name.
			/// </summary>
			public String TAX_NAME{ get; set; }

			/// <summary>
			/// Links to the ATTACHMENT_FACT_LINK table, linking to the ATTACHMENT_FACT table. Provides a unique ID for attachments linked to broker.
			/// </summary>
			public Int64 ATTACHMENT_SET_ID{ get; set; }

			/// <summary>
			/// Aduit log Key
			/// </summary>
			public Int64 AUDIT_LOG_KEY{ get; set; }

			/// <summary>
			/// Broker's HIPPA Agreement Date
			/// </summary>
			public Int32 HIPAA_AGREEMENT_SIG_DATE{ get; set; }

			/// <summary>
			/// Broker Contract termination Dater
			/// </summary>
			public Int32 BROKER_TERMINATION_DATE{ get; set; }

			/// <summary>
			/// Version effective date
			/// </summary>
			public Int32 VERSION_EFF_DATE{ get; set; }

			/// <summary>
			/// Version End Date
			/// </summary>
			public Int32 VERSION_EXP_DATE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
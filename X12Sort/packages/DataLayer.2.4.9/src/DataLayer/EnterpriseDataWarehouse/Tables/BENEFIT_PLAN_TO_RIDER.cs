using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// Benefit Plan Key
		/// </summary>
		public class BENEFIT_PLAN_OTHER_ID_DETAILS{

			/// <summary>
			/// Benefit Plan Key
			/// </summary>
			public Int64 BENEFIT_PLAN_DIM_KEY{ get; set; }

			/// <summary>
			/// Identification number key
			/// </summary>
			public Int64 IDENTIFICATION_NUMBER_KEY{ get; set; }

			/// <summary>
			/// Identification number.
			/// </summary>
			public String IDENTIFICATION_NUMBER{ get; set; }

			/// <summary>
			/// Name of identification type. Examples are: Not Applicable Not Specified NPI HCFA Plan ID Medical Record Number
			/// </summary>
			public String ID_TYPE_NAME{ get; set; }

			/// <summary>
			/// State name.
			/// </summary>
			public String STATE_NAME{ get; set; }

			/// <summary>
			/// The country code name, such as England or United States.
			/// </summary>
			public String COUNTRY_CODE_NAME{ get; set; }

			/// <summary>
			/// Effective date of the ID. Joins to the DATE_DIMENSION table.
			/// </summary>
			public Int32 EFFECTIVE_START_DATE{ get; set; }

			/// <summary>
			/// End date of the ID. Joins to the DATE_DIMENSION table.
			/// </summary>
			public Int32 EFFECTIVE_END_DATE{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
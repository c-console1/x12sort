using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// Used internally by Cobol routines.
		/// </summary>
		public class AMISYS_SUBSCRIBER_BENEFIT_PLAN_DETAILS{

			/// <summary>
			/// Used internally by Cobol routines.
			/// </summary>
			public Int64 IMAGE_RECNBR{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String SUBSCRIBER_NBR{ get; set; }

			/// <summary>
			/// Identification number of the employer division.
    /// 			Identification number assigned to employer group
    /// 			divisions. For ease of identification, the DIVISION#
    /// 			consists of GROUP#, X(6) plus a 4 character extension.
			/// </summary>
			public String DIVISION_NBR{ get; set; }

			/// <summary>
			/// Number assigned to employer groups.
    /// 			The number assigned to employer groups who have
    /// 			contracted with the health plan to provide health
    /// 			insurance coverage.
			/// </summary>
			public String GROUP_NBR{ get; set; }

			/// <summary>
			/// Corporation number or national account number.
    /// 			CORP# allows the healthplan to define a national
    /// 			account level that encompasses several employer groups.
			/// </summary>
			public String CORP_NBR{ get; set; }

			/// <summary>
			/// Independent geographic or profit center.
    /// 			The independent geographic or profit center entities
    /// 			within the health plan with which members and providers
    /// 			are affiliated.
			/// </summary>
			public String BUSINESS_UNIT{ get; set; }

			/// <summary>
			/// Identification# assigned to brokers of the health plan.
			/// </summary>
			public String BROKER_NBR{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String AGECAT_SUBS{ get; set; }

			/// <summary>
			/// Age category for the spouse of the contract holder.
    /// 			Age category for the spouse of the contract holder.
    /// 			Defines a related set of age ranges (0-5, 6-21, 22-55, ...)
    /// 			to be used in determining a premium rate.
			/// </summary>
			public String AGECAT_SPOUS{ get; set; }

			/// <summary>
			/// The name of a general group of benefits.
    /// 			The name of the most general definition for benefits.
    /// 			A group of benefits form a component. They are
    /// 			linked together by CB-LINK (component/benefit link).
    /// 			A group of components form a rider. They are
    /// 			linked together by RC-LINK (rider/component link).
    /// 			A group of riders form a benefit package. They are
    /// 			linked together by BR-LINK (benefit/rider link).
			/// </summary>
			public String BENEFIT_PKG{ get; set; }

			/// <summary>
			/// The entity (Corp/Group/Division/Indiv) who is to be billed.
    /// 			C = Corporation level billing
    /// 			G = Group level billing
    /// 			D = Division level billing
    /// 			I = Individual billing
    /// 			For the TICKLER and LETTER-STATUS tables, the BILL-TO
    /// 			value shows the billing entity who has a delinquent
    /// 			amount due, and was sent a dunning letter.
			/// </summary>
			public String BILL_TO{ get; set; }

			/// <summary>
			/// [BS] Premium Rate Split Code.
    /// 			Used as part of the Premium Rate Key to allow varying
    /// 			Employer/Employee portions of Premiums.
			/// </summary>
			public String BILL_TO_SPLIT{ get; set; }

			/// <summary>
			/// Defines the membership activity or change.
    /// 			This value is set by the membership processor to
    /// 			indicate the type of membership activity (benefit
    /// 			change, contract type change, termination, etc).
    /// 			The billing process uses this value to determine the
    /// 			remarks written on invoices for billing adjustments.
			/// </summary>
			public String BILLING_CODE{ get; set; }

			/// <summary>
			/// Carrier / underwriter of risk.
    /// 			Identifies the carrier / underwriter of risk and how
    /// 			the risk will be managed. Valid values are defined
    /// 			in the SUPER-TABLE table.
			/// </summary>
			public String CARRIER{ get; set; }

			/// <summary>
			/// Flags indicating user override of system calculations
    /// 			A series of flags used to indicate the user has
    /// 			overridden values calculated by the system.
			/// </summary>
			public String CON_OV_FLAGS{ get; set; }

			/// <summary>
			/// [CY] Contract type.
    /// 			The [CY] contract type code category defines the basic
    /// 			contract structure (family, double, single, etc) that
    /// 			has been purchased from the health plan.
    /// 			The element SUBSCRIBERTYPE for the STOP LOSS subsystem
    /// 			defines the following:
    /// 			'1' PAID
    /// 			'2' INCURRED
    /// 			'3' INCURRED and PAID
			/// </summary>
			public String SUBSCRIBERTYPE{ get; set; }

			/// <summary>
			/// Indicates a break in coverage from previous span.
			/// </summary>
			public String COV_BREAK{ get; set; }

			/// <summary>
			/// [ES] Employment status (such as active, retired, etc).
    /// 			[ES] Employment status codes to describe such things
    /// 			as whether or not the employee is actively working,
    /// 			retired, unemployed, etc.
			/// </summary>
			public String EMPLSTAT{ get; set; }

			/// <summary>
			/// Hours Worked/Week determines a Contracts' Annual Salary.
    /// 			Hours Worked/Week is used to determine a Contracts'
    /// 			Annual Salary. This field will be available if 'Salary by
    /// 			Pay Frequency' is turned on at the Division Level via
    /// 			Division Category code (DC). Only numeric values are
    /// 			valid.
			/// </summary>
			public Int64 HRS_WORK_WK{ get; set; }

			/// <summary>
			/// Marital status of Contract Holder.
    /// 			Marital status of Contract holder stored on
    /// 			CONTRACT-SPAN and copied down to MEMBER-SPAN records
    /// 			for capitation.
    /// 			For UB-92, Marital Status is patients marital status at
    /// 			time of service.
    /// 			If UB92, see UB92 FIELD locator 16 for value.
			/// </summary>
			public String MARITALSTAT{ get; set; }

			/// <summary>
			/// Operator ID# of individual performing transaction update.
			/// </summary>
			public String OP_NBR{ get; set; }

			/// <summary>
			/// Pay Frequency used to determine a Contracts' Annual Salary
    /// 			Pay Frequency is used to determine a Contracts' Annual
    /// 			Salary. This field will be available if 'Salary by Pay
    /// 			Frequency' is turned on at the Division level via
    /// 			Division Category code (DC).
    /// 			Valid values are:
    /// 			'A' - Annual 'B' - Bi-Weekly
    /// 			'M' - Monthly 'W' - Weekly
    /// 			'S' - Simi-Monthly 'H' - Hourly
    /// 			Pay Annual Salary
    /// 			Frequency Calculations
    /// 			--------- ----------------------------------------------
    /// 			A 'Annual Salary' is entered by user
    /// 			M 'Annual Salary' equals 'Pay Rate' times 12
    /// 			S 'Annual Salary' equals 'Pay Rate' times 24
    /// 			B 'Annual Salary' equals 'Pay Rate' times 26
    /// 			W 'Annual Salary' equals 'Pay Rate' times 52
    /// 			H 'Annual Salary' equals
    /// 			'Pay Rate' times 'Hours Worked/Week' times 52
			/// </summary>
			public String PAY_FREQ{ get; set; }

			/// <summary>
			/// Pay Rate is used to determine a Contracts' Annual Salary.
    /// 			Pay Rate is used to determine a Contracts' Annual
    /// 			Salary. This field will be available if 'Salary by Pay
    /// 			Frequency' is turned on at the Division Level via
    /// 			Division Category code (DC). Only numeric values are
    /// 			valid.
			/// </summary>
			public Int64 PAY_RATE{ get; set; }

			/// <summary>
			/// Assigns composition of the premium rates for this division.
			/// </summary>
			public String PREMKEY{ get; set; }

			/// <summary>
			/// Identifies health plan, IPA, and/or program area
			/// </summary>
			public String PROG_NBR{ get; set; }

			/// <summary>
			/// [RY] Reason for data entry or modification.
    /// 			
			/// </summary>
			public String REASON{ get; set; }

			/// <summary>
			/// Regional area associated with the specified entity.
			/// </summary>
			public String REGION{ get; set; }

			/// <summary>
			/// Risk population, ex: commercial, medicare, medicaid, union
    /// 			federal, large groups, etc.
			/// </summary>
			public String RISKPOP{ get; set; }

			/// <summary>
			/// Annual salary of the contract holder (subscriber).
			/// </summary>
			public Int64 SALARY{ get; set; }

			/// <summary>
			/// The rate tier that a given contract type is a part of.
			/// </summary>
			public String TIER{ get; set; }

			/// <summary>
			/// For AUTHORIZATION, ADMISSION, and AUTH-DETAIL,
    /// 			TRANSCODE position 1: A (add), C (change)
    /// 			position 2: B (batch), blank (online)
			/// </summary>
			public String TRANSCODE{ get; set; }

			/// <summary>
			/// Underwriting status of the person covered.
    /// 			Defines the Underwriting status of the person covered.
    /// 			In this case the dependent with reduced coverage.
    /// 			Valid Code-set keywords are:
    /// 			'PASSED ', 'WAIVED ', 'DENIED ',
    /// 			'HIGH RISK ', 'NOT REQUIRED', 'NOT COVERED '
			/// </summary>
			public String UNWRISTAT{ get; set; }

			/// <summary>
			/// A 'V' in the first position indicates a voided record.
    /// 			A 'V' in the first position indicates that the record
    /// 			has been voided (most likely due to errors). Voided
    /// 			records are generally excluded from validation scans.
    /// 			For the CLEARCK-TAPE record, the second position of
    /// 			VOID indicates the reason for the void.
			/// </summary>
			public String VOID{ get; set; }

			/// <summary>
			/// Effective or start date.
    /// 			For UB-92 this is From Date- date the occurrence starts
    /// 			The UB-92 from date is the date the occurrence is for.
    /// 			Occur date corresponds to the occur code event relating
    /// 			to this bill. This 'from date' must not be later than
    /// 			a 'thru date' when a thru date is entered.
    /// 			Refer to UB92 locator FIELD 32a-36a and 32b-36b
    /// 			of service. A one position, alpha numeric value. See
    /// 			UB92 FIELD locator 32 through 36 for value.
    /// 			For PROV-CON-ED, YMDEFF is the start date for the class
    /// 			or seminar.
			/// </summary>
			public Int32 YMDEFF{ get; set; }

			/// <summary>
			/// The date the entity is eligible for coverage/benefits.
    /// 			Effective date of the first span of continuous coverage.
    /// 			This is the date the entity was first eligible for
    /// 			coverage/benefits.
			/// </summary>
			public Int32 YMDELIG{ get; set; }

			/// <summary>
			/// End date.
    /// 			For UB-92 this is Thru Date- date the occurrence ends.
    /// 			The UB-92 thru date is the date the occurrence is for.
    /// 			Occur date corresponds to the occur code event relating
    /// 			to this bill. This 'thru date' must not be less than
    /// 			a 'from date' when a thru date is entered.
    /// 			Refer to UB92 locator FIELDS 32a-36a and 32b-36b values
			/// </summary>
			public Int32 YMDEND{ get; set; }

			/// <summary>
			/// Billing start date for billing span
    /// 			
			/// </summary>
			public Int32 YMDFIRSTBILL{ get; set; }

			/// <summary>
			/// Date the subscriber was hired (mm/dd/yyyy).
			/// </summary>
			public Int32 YMDHIRE{ get; set; }

			/// <summary>
			/// End billing date for billing span.
    /// 			
			/// </summary>
			public Int32 YMDLASTBILL{ get; set; }

			/// <summary>
			/// Date of most recent transaction.
			/// </summary>
			public Int32 YMDTRANS{ get; set; }

			/// <summary>
			/// Counter incremented via trigger after any updates.
    /// 			Used by the JAVA to duplicate the 'record has changed
    /// 			since you found it' feature of AMISYS 3000 and
    /// 			AMISYS Advance Release 1 that is provided by the Cognos
    /// 			Quick screens.
			/// </summary>
			public Decimal CHECKSUM_VALUE{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
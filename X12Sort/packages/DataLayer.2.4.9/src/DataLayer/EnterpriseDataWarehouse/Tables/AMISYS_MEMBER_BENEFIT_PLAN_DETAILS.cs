using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// No Description Found
		/// </summary>
		public class AMISYS_MEMBER_ADDRESS_DETAILS{

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 IMAGE_RECNBR{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String MEMBER_NBR{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String CONTACT_ID{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String CONTACT_ID_NBR{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String USE_FOR_CORRESP{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String OP_NBR{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String ADDR_WHO{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String ADDRTYPE_WHO{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String FULL_NAME{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String ADDRESS1{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String ADDRESS2{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String COUNTY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String CITY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String STATE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String ZIP{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String COUNTRY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String COUNTRY_SUBDIVSN{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String PHONE1{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String PHONE1INFO{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String PHONE2{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String PHONE2INFO{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String PHONE3{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String PHONE3INFO{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String EMAIL{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String REASON{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String VOID{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String TRANSCODE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 YMDEFF{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 YMDEND{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 YMDTRANS{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String URL{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Decimal CHECKSUM_VALUE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
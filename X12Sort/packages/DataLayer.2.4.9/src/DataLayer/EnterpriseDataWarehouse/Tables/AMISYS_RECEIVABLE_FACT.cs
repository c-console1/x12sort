using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// No Description Found
		/// </summary>
		public class AMISYS_RECEIPT_FACT{

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 AMISYS_RECEIPT_FACT_KEY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 SOURCE_KEY{ get; set; }

			/// <summary>
			/// X(4) Table# + 9(8) sequential receipt number.
			/// </summary>
			public String RECEIPT_NBR{ get; set; }

			/// <summary>
			/// BU/PG specific, overlapping sequential invoice numbers.
    /// 			Structure is: x(4) table# + 9(8) sequential number.
			/// </summary>
			public String INVOICE_NBR{ get; set; }

			/// <summary>
			/// Billing entity for the premium billing process.
    /// 			Defines the billing entity for the premium billing
    /// 			process. The billing entity can be the corporation,
    /// 			group, division, or contract.
			/// </summary>
			public String ACCT_ID_NBR{ get; set; }

			/// <summary>
			/// Accounting month for this transaction, YYYYMM format.
    /// 			Identifies the accounting month in which the transaction
    /// 			occurred.
			/// </summary>
			public String ACCT_MONTH{ get; set; }

			/// <summary>
			/// Identifies the dollar amount for the given transaction.
    /// 			For CP0500M, the AMOUNTACT is the sum of SERVICE.AMTPAY
    /// 			for all services of the claim. The AMOUNTs of all the
    /// 			claims to be paid manually are then totaled, and the
    /// 			the value is posted to CHECK.AMOUNT.
    /// 			For CAPADJ-FUND, the AMOUNTACT is the dollar amount
    /// 			applied to a fund.
    /// 			For CAPADJ, the AMOUNTACT is the total amount of the
    /// 			of the manual capitation adjustment. This total amount
    /// 			represents the sum total of the amounts on the related
    /// 			CAPADJ-FUND records.
    /// 			For IQ3390M, AMOUNT is the total of COUNTER.AMOUNTs.
			/// </summary>
			public Int64 AMOUNTACT{ get; set; }

			/// <summary>
			/// Contains the original prepayment amount.
			/// </summary>
			public Int64 AMOUNT1{ get; set; }

			/// <summary>
			/// Stores plan liability for the plan''s receipts.
    /// 			Stores running total of available money for prepayments
			/// </summary>
			public Int64 AMT_LIABILITY{ get; set; }

			/// <summary>
			/// Denotes status of payments applied to specific set.
			/// </summary>
			public String APPLIED_FLAG{ get; set; }

			/// <summary>
			/// The G/L accounts receivable account number derived from the
    /// 			ACCOUNT-DEF record
			/// </summary>
			public String AR_ACCOUNT_NBR{ get; set; }

			/// <summary>
			/// Accounts Receivable recognition switch derived from the
    /// 			SUPER-TABLE entry of BASWITCHES,
    /// 			C = book A/R in current month (ACCT-MONTH)
    /// 			R = book A/R in revenue month (MONTH)
			/// </summary>
			public String AR_RECOGNITION{ get; set; }

			/// <summary>
			/// The entity (Corp/Group/Division/Indiv) who is to be billed.
    /// 			C = Corporation level billing
    /// 			G = Group level billing
    /// 			D = Division level billing
    /// 			I = Individual billing
    /// 			For the TICKLER and LETTER-STATUS tables, the BILL-TO
    /// 			value shows the billing entity who has a delinquent
    /// 			amount due, and was sent a dunning letter.
			/// </summary>
			public String BILL_TO{ get; set; }

			/// <summary>
			/// Independent geographic or profit center.
    /// 			The independent geographic or profit center entities
    /// 			within the health plan with which members and providers
    /// 			are affiliated.
			/// </summary>
			public String BUSINESS_UNIT{ get; set; }

			/// <summary>
			/// Carrier / underwriter of risk.
    /// 			Identifies the carrier / underwriter of risk and how
    /// 			the risk will be managed. Valid values are defined
    /// 			in the SUPER-TABLE table.
			/// </summary>
			public String CARRIER{ get; set; }

			/// <summary>
			/// Receivable or adjustment commission payment status.
    /// 			The receivable or adjustment commission payment status.
    /// 			It defines whether or not the commission has been paid
    /// 			for the particular receivable or adjustment.
    /// 			''P '' the commission has been paid
    /// 			'' '' the commission has not been paid
			/// </summary>
			public String COMMIS_STAT{ get; set; }

			/// <summary>
			/// The G/L cash account number derived from ACCOUNT-DEF.
			/// </summary>
			public String CS_ACCOUNT_NBR{ get; set; }

			/// <summary>
			/// Identification number of the employer division.
    /// 			Identification number assigned to employer group
    /// 			divisions. For ease of identification, the DIVISION#
    /// 			consists of GROUP#, X(6) plus a 4 character extension.
			/// </summary>
			public String DIVISION_NBR{ get; set; }

			/// <summary>
			/// Electronic fund transfer information.
    /// 			Electronic fund transfer information.
    /// 			The first position describes the EFT status for the
    /// 			billing entity. The user can set the EFT-INFO value
    /// 			manually for the purpose of suspending and
    /// 			re-activating the Electronic Fund Transfer processing
    /// 			for a billing entity. Otherwise, the value is
    /// 			automatically maintained by the billing process.
    /// 			The billing process evaluates all EFT-BANK records with
    /// 			an EFT-INFO value of Y, P, or S. Based on a comparison
    /// 			of the billing due date with EFT-BANK.YMDEND and
    /// 			EFT-BANK.YMDPRENOTE, the billing process determines
    /// 			any updates required for EFT-INFO. The billing entity
    /// 			record on the HEALTH database is then updated based on
    /// 			the EFT-BANK updates. The EFT-INFO value on the HEALTH
    /// 			database determines if EFT logic is used in the
    /// 			generation of invoices.
    /// 			The second position directs the EFT logic when a
    /// 			previous balance exists (that is the result of
    /// 			insufficient funds during EFT processing). Once a
    /// 			previous balance becomes current, the ''Y'' or ''N'' is
    /// 			updated to blank.
    /// 			Position 1: ''Y'' indicates the Corporation, Division,
    /// 			Group, or Contract currently utilizes EFT.
    /// 			'' '' indicates the Corporation, Division,
    /// 			Group, or Contract does not utilize EFT.
    /// 			''N'' indicates the EFT authorization for the
    /// 			Corporation, Division, Group, or
    /// 			Contract has expired.
    /// 			''P'' indicates EFT is pending. The EFT will
    /// 			become effective in the first billing
    /// 			period after the authorized effective
    /// 			date.
    /// 			''S'' indicates the EFT utilization has been
    /// 			suspended.
    /// 			Position 2: ''Y'' indicates the next EFT should include
    /// 			the previous balance.
    /// 			''N'' indicates the next EFT should not
    /// 			include the previous balance.
    /// 			'' '' indicates there is no previous balance.
			/// </summary>
			public String EFT_INFO{ get; set; }

			/// <summary>
			/// Transaction type for jorunal entry.
			/// </summary>
			public String GL_TYPE{ get; set; }

			/// <summary>
			/// Month affected by this transaction, format YYYYMM
			/// </summary>
			public String MONTH_X{ get; set; }

			/// <summary>
			/// Indicator for online ''O '' or batch ''B '' source.
    /// 			For RECEIPT-DETAIL, the values for POSTED2 are:
    /// 			''O '' created by on-line process
    /// 			''B '' created by batch process
			/// </summary>
			public String POSTED2{ get; set; }

			/// <summary>
			/// The G/L prepaid premium account# derived from ACCOUNT-DEF.
			/// </summary>
			public String PP_ACCOUNT_NBR{ get; set; }

			/// <summary>
			/// Identifies health plan, IPA, and/or program area
			/// </summary>
			public String PROG_NBR{ get; set; }

			/// <summary>
			/// Regional area associated with the specified entity.
			/// </summary>
			public String REGION{ get; set; }

			/// <summary>
			/// A field to more uniquely identify an INV-SR-RIDER.
    /// 			For RECEIPT-RIDERS and INV-AR-RIDERS tables, to
    /// 			further identify the relationship between the two data
    /// 			sets with linking records.
			/// </summary>
			public Int64 SUB_REC_NBR{ get; set; }

			/// <summary>
			/// X(4) Table# + Audit trail or expensed entry number.
    /// 			For the PREMCASH-RECEIPT table, TRANS# contains
    /// 			DIVISION# when BILL-TO = ''I ''. '
			/// </summary>
			public String TRANS_NBR{ get; set; }

			/// <summary>
			/// A trans number for Batch Cash Process.
    /// 			This field stores a transaction number that allows the
    /// 			ability to trace what cash transactions were made with a
    /// 			particular Batch Cash run. X(4)Table# + 9(8)Transaction#
			/// </summary>
			public String TRANS1_NBR{ get; set; }

			/// <summary>
			/// Represents the cost center, department, division, etc.
    /// 			The unit code that represents the cost center,
    /// 			department, division, and/or company.
			/// </summary>
			public String UNIT{ get; set; }

			/// <summary>
			/// Date the broker was commissioned for the receipt.
    /// 			For the RECEIPT-RIDERS table, YMDCOMMIS reflects
    /// 			the date broker commissions were paid on this rider.
			/// </summary>
			public Int32 YMDCOMMIS{ get; set; }

			/// <summary>
			/// Date processed.
    /// 			For the CHECK table, YMDPROCESS is the system date
    /// 			of the day the CHECK record was created. The value is
    /// 			never updated (it retains the original creation date).
    /// 			For the IN-PROCESS table, the YMDPROCESS is set
    /// 			equal to the system date of the day the non-liable EOB
    /// 			report is printed for the first time. It remains
    /// 			unchanged for any subsequent reruns.
			/// </summary>
			public Int32 YMDPROCESS{ get; set; }

			/// <summary>
			/// Counter incremented via trigger after any updates.
    /// 			Used by the JAVA to duplicate the ''record has changed
    /// 			since you found it'' feature of AMISYS 3000 and
    /// 			AMISYS Advance Release 1 that is provided by the Cognos
    /// 			Quick screens.
			/// </summary>
			public Decimal CHECKSUM_VALUE{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
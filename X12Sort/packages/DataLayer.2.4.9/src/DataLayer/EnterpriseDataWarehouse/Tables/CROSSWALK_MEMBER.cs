using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// No Description Found
		/// </summary>
		public class CARRIER_DIM{

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 CARRIER_DIM_KEY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String CARRIER_HCC_ID{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String CARRIER_PRIMARY_NAME{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String CARRIER_STATUS{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String CARRIER_TYPE_NAME{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String BILLING_CYCLE_NAME{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 TAX_DIM_KEY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String TAX_ID{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String TAX_NAME{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 TENANT_HCC_ID{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 CARRIER_HIST_FACT_COUNT{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 AUDIT_LOG_KEY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 EFF_START_DATE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 EFF_THROUGH_DATE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// Used internally by Cobol routines.
		/// </summary>
		public class AMISYS_BROKER_DIM{

			/// <summary>
			/// Used internally by Cobol routines.
			/// </summary>
			public Int64 IMAGE_RECNBR{ get; set; }

			/// <summary>
			/// Broker Member ID
			/// </summary>
			public String BROKER_NBR_BR{ get; set; }

			/// <summary>
			/// The tax id number established for the provider, group
    /// 			
    /// 			practice, broker, or other entity.
			/// </summary>
			public String IRS_NBR{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String TYPE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String NAME{ get; set; }

			/// <summary>
			/// The Associated ADDRESS Line 1 & 2 with the Broker.
			/// </summary>
			public String ADDRESS{ get; set; }

			/// <summary>
			/// Street Name of the Address for the Broker.
			/// </summary>
			public String STREET{ get; set; }

			/// <summary>
			/// City Name of the Adress for the Broker
			/// </summary>
			public String CITY_BR{ get; set; }

			/// <summary>
			/// State Name for the Broker.
			/// </summary>
			public String STATE{ get; set; }

			/// <summary>
			/// Zip Code # Associated with the broker's address.
			/// </summary>
			public String ZIP{ get; set; }

			/// <summary>
			/// Phone Number Associated with the Broker.
			/// </summary>
			public String PHONE{ get; set; }

			/// <summary>
			/// Effective or start date.
    /// 			For UB-92 this is From Date- date the occurrence starts
    /// 			The UB-92 from date is the date the occurrence is for.
    /// 			Occur date corresponds to the occur code event relating
    /// 			to this bill. This 'from date' must not be later than
    /// 			a 'thru date' when a thru date is entered.
    /// 			Refer to UB92 locator FIELD 32a-36a and 32b-36b
    /// 			of service. A one position, alpha numeric value. See
    /// 			UB92 FIELD locator 32 through 36 for value.
    /// 			For PROV-CON-ED, YMDEFF is the start date for the class
    /// 			or seminar.
			/// </summary>
			public Int32 YMDEFF{ get; set; }

			/// <summary>
			/// End date.
    /// 			
    /// 			For UB-92 this is Thru Date- date the occurrence ends.
    /// 			
    /// 			The UB-92 thru date is the date the occurrence is for.
    /// 			
    /// 			Occur date corresponds to the occur code event relating
    /// 			
    /// 			to this bill. This 'thru date' must not be less than
    /// 			
    /// 			a 'from date' when a thru date is entered.
    /// 			
    /// 			Refer to UB92 locator FIELDS 32a-36a and 32b-36b values
			/// </summary>
			public Int32 YMDEND{ get; set; }

			/// <summary>
			/// 
			/// </summary>
			public String SPLIT{ get; set; }

			/// <summary>
			/// Billing entity for the premium billing process.
    /// 			
    /// 			Defines the billing entity for the premium billing
    /// 			
    /// 			process. The billing entity can be the corporation,
    /// 			
    /// 			group, division, or contract.
			/// </summary>
			public String ACCT_ID_NBR{ get; set; }

			/// <summary>
			/// 
			/// </summary>
			public String TBA_FLAG{ get; set; }

			/// <summary>
			/// Date of most recent transaction.
			/// </summary>
			public Int32 YMDTRANS{ get; set; }

			/// <summary>
			/// Operator ID# of individual performing transaction update.
			/// </summary>
			public String OP_NBR{ get; set; }

			/// <summary>
			/// For AUTHORIZATION, ADMISSION, and AUTH-DETAIL,
    /// 			
    /// 			TRANSCODE position 1: A (add), C (change)
    /// 			
    /// 			position 2: B (batch), blank (online)
			/// </summary>
			public String TRANSCODE{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
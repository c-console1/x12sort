using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// Member Dimension Key
		/// </summary>
		public class MEMBER_OTHER_INSURANCE_COVERAGE_DETAILS{

			/// <summary>
			/// Member Dimension Key
			/// </summary>
			public Int64 MEMBER_DIM_KEY{ get; set; }

			/// <summary>
			/// Member other insurance coverage ID
			/// </summary>
			public Int64 MEMBER_OTHER_INSURANCE_COVERAGE_KEY{ get; set; }

			/// <summary>
			/// Primary key for the LEAD_SOURCE_FOR_OTHER_INS table.
			/// </summary>
			public Int64 LEAD_SOURCE_FOR_OTHER_INS_KEY{ get; set; }

			/// <summary>
			/// Values are: 1 - Primary 2 - Secondary 3 - Tertiary 4 - Equal Split 5 - Unknown
			/// </summary>
			public String OTHER_INS_PRIORITY_NAME{ get; set; }

			/// <summary>
			/// Primary key of the OBD_REASON_CODE table.
			/// </summary>
			public Int64 OTHER_INS_REASON{ get; set; }

			/// <summary>
			/// Primary key for OTHER_INSURANCE_COMPANY table. Auto-generated surrogate key.
			/// </summary>
			public String OTHER_INSURANCE_COMPANY_NAME{ get; set; }

			/// <summary>
			/// Member ID
			/// </summary>
			public String MEMBER_ID{ get; set; }

			/// <summary>
			/// Subscriber Date of Birth
			/// </summary>
			public Int32 SUBSCRIBER_DOB{ get; set; }

			/// <summary>
			/// Subscriber gender
			/// </summary>
			public String SUBSCRIBER_GENDER_NAME{ get; set; }

			/// <summary>
			/// Subscriber name
			/// </summary>
			public String SUBSCRIBER_NAME{ get; set; }

			/// <summary>
			/// Relationship to subscriber key.
			/// </summary>
			public String RLTP_TO_SUBSCRIBER_NAME{ get; set; }

			/// <summary>
			/// Benefit Plan Type Key
			/// </summary>
			public String BENEFIT_PLAN_TYPE_KEY{ get; set; }

			/// <summary>
			/// Identifier of other insurance coverage plan.
			/// </summary>
			public String PLAN_ID{ get; set; }

			/// <summary>
			/// Name of other insurance coverage plan.
			/// </summary>
			public String PLAN_NAME{ get; set; }

			/// <summary>
			/// Values are: 1 - Medicare Part A 2 - Medicare Part B 3 - Medicare Part A and B 4 - Medicare Unknown 5 - Medicare - ESRD 6 - Medicare Part A only - ESRD 7 - Non-Medicare 8 - Tricare Military Health Insurance 9 - Medicare Part C 10 - Medicare Part D 11 - Medicare Part C and D 12 - Medicare Supplement
			/// </summary>
			public String POLICY_TYPE_CODE{ get; set; }

			/// <summary>
			/// 
			/// </summary>
			public Int64 COB_POLICY_HIST_FACT_COUNT{ get; set; }

			/// <summary>
			/// The COB Policy ID.
			/// </summary>
			public String COB_POLICY_ID_ALIAS{ get; set; }

			/// <summary>
			/// The COB Policy Key
			/// </summary>
			public Int64 COB_POLICY_KEY{ get; set; }

			/// <summary>
			/// Status code (a Active, t Terminated, u Unknown).
			/// </summary>
			public string CASE_STATUS_CODE{ get; set; }

			/// <summary>
			/// 
			/// </summary>
			public String RX_BIN{ get; set; }

			/// <summary>
			/// RX Group
			/// </summary>
			public String RX_GROUP{ get; set; }

			/// <summary>
			/// RX ID
			/// </summary>
			public String RX_ID{ get; set; }

			/// <summary>
			/// RX PCN
			/// </summary>
			public String RX_PCN{ get; set; }

			/// <summary>
			/// RX Phone Number
			/// </summary>
			public String RX_PHONE_NUMBER{ get; set; }

			/// <summary>
			/// The Court-Ordered Coverage check box in the Member Information section of the COB Other Insurance.
			/// </summary>
			public String SUPPORT_ORDER_EXISTS_IND{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 TENANT_HCC_ID{ get; set; }

			/// <summary>
			/// Audit Log Key
			/// </summary>
			public Int64 AUDIT_LOG_KEY{ get; set; }

			/// <summary>
			/// Primary key for DATE_DIMENSION table. Auto-generated surrogate key.
			/// </summary>
			public Int32 INFO_RECEIPT_DATE{ get; set; }

			/// <summary>
			/// Primary key for DATE_DIMENSION table. Auto-generated surrogate key.
			/// </summary>
			public Int32 LAST_VERIFICATION_DATE{ get; set; }

			/// <summary>
			/// Primary key for DATE_DIMENSION table. Auto-generated surrogate key.
			/// </summary>
			public Int32 EFFECTIVE_DATE{ get; set; }

			/// <summary>
			/// Primary key for DATE_DIMENSION table. Auto-generated surrogate key.
			/// </summary>
			public Int32 TERMINATION_DATE{ get; set; }

			/// <summary>
			/// Primary key for DATE_DIMENSION table. Auto-generated surrogate key.
			/// </summary>
			public Int32 VERSION_EFF_DATE{ get; set; }

			/// <summary>
			/// Primary key for DATE_DIMENSION table. Auto-generated surrogate key.
			/// </summary>
			public Int32 VERSION_EXP_DATE{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
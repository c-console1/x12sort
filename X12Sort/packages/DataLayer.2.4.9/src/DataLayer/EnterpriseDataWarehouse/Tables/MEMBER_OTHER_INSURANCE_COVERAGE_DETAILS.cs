using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// The member associated with the ID. Joins to the MEMBER_HISTORY_FACT table.
		/// </summary>
		public class MEMBER_OTHER_ID_DETAILS{

			/// <summary>
			/// The member associated with the ID. Joins to the MEMBER_HISTORY_FACT table.
			/// </summary>
			public Int64 MEMBER_DIM_KEY{ get; set; }

			/// <summary>
			/// Primary key for the MEMBER_OTHER_ID table.
			/// </summary>
			public Int64 MEMBER_OTHER_ID_KEY{ get; set; }

			/// <summary>
			/// The actual ID number.
			/// </summary>
			public String IDENTIFICATION_NUMBER{ get; set; }

			/// <summary>
			/// The type of ID (e.g., Federal Taxpayer ID). Joins to the ID_TYPE_CODE table.
			/// </summary>
			public String ID_TYPE_NAME{ get; set; }

			/// <summary>
			/// The state from where the ID originated.
			/// </summary>
			public String STATE_NAME{ get; set; }

			/// <summary>
			/// The country from where the ID originated.
			/// </summary>
			public String COUNTRY_CODE_NAME{ get; set; }

			/// <summary>
			/// The effective end date of the ID. Joins to the DATE_DIMENSION table.
			/// </summary>
			public Int32 EFFECTIVE_START_DATE{ get; set; }

			/// <summary>
			/// The effective start date of the ID. Joins to the DATE_DIMENSION table.
			/// </summary>
			public Int32 EFFECTIVE_END_DATE{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
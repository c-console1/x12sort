using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// Account Dimension key --
    /// 			Key to the HR_ACCOUNT_BANK_ACCOUNTS_DETAILS
		/// </summary>
		public class ACCOUNT_BANK_ACCOUNTS_DETAILS{

			/// <summary>
			/// Account Dimension key --
    /// 			Key to the HR_ACCOUNT_BANK_ACCOUNTS_DETAILS
			/// </summary>
			public Int64 ACCOUNT_DIM_KEY{ get; set; }

			/// <summary>
			/// Key to the Account_ID
			/// </summary>
			public Int64 ACCT_ID_KEY{ get; set; }

			/// <summary>
			/// Key to the BANK_ACCOUNT table.
			/// </summary>
			public Int64 BANK_ACCOUNT_KEY{ get; set; }

			/// <summary>
			/// Bank Account number.
			/// </summary>
			public String ACCOUNT_NUMBER{ get; set; }

			/// <summary>
			/// Name of the bank associated with the Account.
			/// </summary>
			public String BANK_NAME{ get; set; }

			/// <summary>
			/// Status (Active, Terminated, Unknown).
			/// </summary>
			public String ACCOUNT_BANK_ACCOUNT_STATUS_NAME{ get; set; }

			/// <summary>
			/// Keeps a record count of the bank acounts for the account..
			/// </summary>
			public Int64 ACC_BNK_ACC_FACT_COUNT{ get; set; }

			/// <summary>
			/// Key to the AUDIT_LOG table.
			/// </summary>
			public Int64 AUDIT_LOG_ID_KEY{ get; set; }

			/// <summary>
			/// Version effective date value for Accounts.
    /// 			Date in YYYY-MM-DD format.
			/// </summary>
			public Int32 VERSION_EFF_DATE{ get; set; }

			/// <summary>
			/// Version end date value for Accounts.
    /// 			Date in YYYY-MM-DD format.
			/// </summary>
			public Int32 VERSION_EXP_DATE{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}

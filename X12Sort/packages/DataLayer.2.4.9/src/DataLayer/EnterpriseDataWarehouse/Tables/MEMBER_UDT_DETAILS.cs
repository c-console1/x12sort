using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// Joins back to MEMBER_HISTORY_FACT table. The member associated with the benefit plan.
		/// </summary>
		public class MEMBER_TO_BENEFIT_PLAN_DETAILS{

			/// <summary>
			/// Joins back to MEMBER_HISTORY_FACT table. The member associated with the benefit plan.
			/// </summary>
			public Int64 MEMBER_DIM_KEY{ get; set; }

			/// <summary>
			/// Benefit planfactassociated with the member.
			/// </summary>
			public Int64 BENEFIT_PLAN_DIM_KEY{ get; set; }

			/// <summary>
			/// Primary key for the MEMBER_TERMINATE_REASON_CODE table.
			/// </summary>
			public String MEMBER_TERMINATE_REASON_NAME{ get; set; }

			/// <summary>
			/// Primary key for PRODUCT table. Auto-generated surrogate key.
			/// </summary>
			public Int64 PRODUCT_KEY{ get; set; }

			/// <summary>
			/// Enables sorting of records (in reports) in the same way they show up in the UI. For example, if you add multiple records, they appear in a certain order. That order is preserved in OLTP. Sort order in data warehouse is used to capture that order on data warehouse side.
			/// </summary>
			public Int64 SORT_ORDER{ get; set; }

			/// <summary>
			/// The SPECIAL_ENROLLMENT_PRD_KEY for the Special Enrollment Period table.
			/// </summary>
			public String SPECIAL_ENROLLMENT_PERIOD_NAME{ get; set; }

			/// <summary>
			/// Primary key of the MEDICARE_ENROLLMENT_PERIOD table. Medicare enrollment period is set only for medicare benefit plans.
			/// </summary>
			public String MEDICARE_ENROLLMENT_PERIOD_NAME{ get; set; }

			/// <summary>
			/// Covergae Details
			/// </summary>
			public Decimal COVERAGE{ get; set; }

			/// <summary>
			/// Claim History From other Plans
			/// </summary>
			public String CLAIM_HIST_FROM_OTHER_PLANS{ get; set; }

			/// <summary>
			/// Reason of drop from Medicare Advantage plan to original Medicare. This attribute is captured from market prominence.
			/// </summary>
			public String DISENROLL_REASON{ get; set; }

			/// <summary>
			/// Name of the Applicant signed
			/// </summary>
			public String ENROLLMENT_APPLICATION_SIGNATURE_NAME{ get; set; }

			/// <summary>
			/// Tier (e.g., Individual, Family).
			/// </summary>
			public String TIER_NAME{ get; set; }

			/// <summary>
			/// The coverage tier set (e.g., Individual, Family).
			/// </summary>
			public String TIER_SET_NAME{ get; set; }

			/// <summary>
			/// Weight is used to report aggregated values where one to many relationships are involved. It is used in bridge tables to allow creation of reports that join through one-to-many relationships without double counting. The weight is one over the number of rows in the bridge table.
			/// </summary>
			public Decimal WEIGHT{ get; set; }

			/// <summary>
			/// Application submission date. This attribute captured from market prominence. Joins to the DATE_DIMENSION table.
			/// </summary>
			public Int32 SUBMIT_DATE{ get; set; }

			/// <summary>
			/// Application completed date. This attribute captured from market prominence.
			/// </summary>
			public Int32 COMPLETED_DATE{ get; set; }

			/// <summary>
			/// Date drop from Medicare Advantage plan to original Medicare. This attribute captured from market prominence. Joins to the DATE_DIMENSION table.
			/// </summary>
			public Int32 DISENROLL_DATE{ get; set; }

			/// <summary>
			/// When was the application received. Joins to the DATE_DIMENSION table.
			/// </summary>
			public Int32 ENROLLMENT_APP_RECEIPT_DATE{ get; set; }

			/// <summary>
			/// When was the application signed. Joins to the DATE_DIMENSION table.
			/// </summary>
			public Int32 ENROLLMENT_APP_SIGN_DATE{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
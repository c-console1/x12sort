using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// Primary key to the ALL_MEMBER_VERSION_HIST_FACT table.
		/// </summary>
		public class MEMBER_DIM{

			/// <summary>
			/// Primary key to the ALL_MEMBER_VERSION_HIST_FACT table.
			/// </summary>
			public Int64 MEMBER_DIM_KEY{ get; set; }

			/// <summary>
			/// Account Dimension key
			/// </summary>
			public Int64 ACCOUNT_DIM_KEY{ get; set; }

			/// <summary>
			/// Name of the account type.
			/// </summary>
			public String ACCOUNT_NAME{ get; set; }

			/// <summary>
			/// Alternate payment Recipient Address dimension key
			/// </summary>
			public Int64 ALT_PAYMENT_RECIPIENT_ADDRESS_DIM_KEY{ get; set; }

			/// <summary>
			/// Member home address dimension key
			/// </summary>
			public Int64 MEMBER_HOME_ADDRESS_DIM_KEY{ get; set; }

			/// <summary>
			/// Member mailing adress dimension key
			/// </summary>
			public Int64 MEMBER_MAILING_ADDRESS_DIM_KEY{ get; set; }

			/// <summary>
			/// Alternate payment Recipient First Name
			/// </summary>
			public String ALT_PMT_RECIPIENT_FIRST_NAME{ get; set; }

			/// <summary>
			/// Alternate payment Recipient Full Name
			/// </summary>
			public String ALT_PMT_RECIPIENT_FULL_NAME{ get; set; }

			/// <summary>
			/// Alternate payment Recipient Last Name
			/// </summary>
			public String ALT_PMT_RECIPIENT_LAST_NAME{ get; set; }

			/// <summary>
			/// Alternate payment Recipient Middle Name
			/// </summary>
			public String ALT_PMT_RECIPIENT_MIDDLE_NAME{ get; set; }

			/// <summary>
			/// Alternate payment Recipient Name Prefix
			/// </summary>
			public String ALT_PMT_RECIPIENT_NAME_PREFIX{ get; set; }

			/// <summary>
			/// Alternate payment Recipient Name Suffix
			/// </summary>
			public String ALT_PMT_RECIPIENT_NAME_SUFFIX{ get; set; }

			/// <summary>
			/// Annual wage of the member.
			/// </summary>
			public Decimal ANNUAL_WAGE_AMOUNT{ get; set; }

			/// <summary>
			/// Attachment set ID
			/// </summary>
			public Int64 ATTACHMENT_SET_ID{ get; set; }

			/// <summary>
			/// Contact method for the member.
			/// </summary>
			public String CONTACT_METHOD_NAME{ get; set; }

			/// <summary>
			/// The associated department of the member.
			/// </summary>
			public String DEPARTMENT_NAME{ get; set; }

			/// <summary>
			/// The reason why a member is denied disability status (e.g., Was Not Covered Before Disabled). Joins to the DISABILITY_DENIAL_REASON table.
			/// </summary>
			public String DISABILITY_DENIAL_REASON_NAME{ get; set; }

			/// <summary>
			/// Disability diagnosis. This is used to describe why a member is disabled. Joins to the DIAGNOSIS table.
			/// </summary>
			public String DISABILITY_DIAG{ get; set; }

			/// <summary>
			/// Type of disability as listed in DISABILITY_TYPE table. Approval status (e.g., Temporarily Approved, Indefinetely Approved, Approved, Denied, Awaiting Verification etc).
			/// </summary>
			public String DISABILITY_TYPE_NAME{ get; set; }

			/// <summary>
			/// The method that documents should be delivered to the member. Document delivery method for the member.
			/// </summary>
			public String DOCUMENT_DELIVERY_METHOD_NAME{ get; set; }

			/// <summary>
			/// The email address of the member.
			/// </summary>
			public String EMAIL_ADDRESS{ get; set; }

			/// <summary>
			/// Email format.
			/// </summary>
			public String EMAIL_FORMAT_NAME{ get; set; }

			/// <summary>
			/// Employee Type ID.
			/// </summary>
			public String EMPLOYEE_TYPE_NAME{ get; set; }

			/// <summary>
			/// Status of employee (e.g., full time, part time, active military, retired).
			/// </summary>
			public String EMPLOYMENT_STATUS_NAME{ get; set; }

			/// <summary>
			/// Indicates if the member is handicapped.
			/// </summary>
			public String IS_HANDICAPPED{ get; set; }

			/// <summary>
			/// Indicates if member is in a union.
			/// </summary>
			public String IS_IN_UNION{ get; set; }

			/// <summary>
			/// Indicates if member is currently in hospice care.
			/// </summary>
			public String IS_MEMBER_IN_HOSPICE{ get; set; }

			/// <summary>
			/// Indictes if member is a VIP.
			/// </summary>
			public String IS_VIP{ get; set; }

			/// <summary>
			/// Marital Status (e.g., Married, Single, Divorced etc).
			/// </summary>
			public String IMMIGRATION_STATUS_TYPE_NAME{ get; set; }

			/// <summary>
			/// Indicates the primary source of member information. Primary key for the INFORMATION_SOURCE_TYPE_CODE table.
			/// </summary>
			public String INFORMATION_SOURCE_TYPE_NAME{ get; set; }

			/// <summary>
			/// Marital Status (e.g., Married, Single, Divorced etc).
			/// </summary>
			public String MARITTAL_STATUS_NAME{ get; set; }

			/// <summary>
			/// First name of Member.
			/// </summary>
			public String MEMBER_FIRST_NAME{ get; set; }

			/// <summary>
			/// Patient (Member) name in the form LAST, FIRST
			/// </summary>
			public String MEMBER_FULL_NAME{ get; set; }

			/// <summary>
			/// Patient gender. Primary key for GENDER_CODE table. Value can be M (male), F (female), or U (unknown).
			/// </summary>
			public String MEMBER_GENDER_NAME{ get; set; }

			/// <summary>
			/// Member identifier assigned by HCC.
			/// </summary>
			public String MEMBER_HCC_ID{ get; set; }

			/// <summary>
			/// Column for summing number of records in a query. No business application. Always has a value of 1.
			/// </summary>
			public Int64 MEMBER_HISTORY_FACT_COUNT{ get; set; }

			/// <summary>
			/// Internal identifier for a member. Can use MEMBER_KEY to join to other tables containing MEMBER_KEY.
			/// </summary>
			public Int64 MEMBER_KEY{ get; set; }

			/// <summary>
			/// Last name of member.
			/// </summary>
			public String MEMBER_LAST_NAME{ get; set; }

			/// <summary>
			/// Middle name of member.
			/// </summary>
			public String MEMBER_MIDDLE_NAME{ get; set; }

			/// <summary>
			/// Member Name Prefix
			/// </summary>
			public String MEMBER_NAME_PREFIX{ get; set; }

			/// <summary>
			/// Member Name Suffix
			/// </summary>
			public String MEMBER_NAME_SUFFIX{ get; set; }

			/// <summary>
			/// Current status of the member.
			/// </summary>
			public String MEMBER_STATUS_NAME{ get; set; }

			/// <summary>
			/// Primary key of the LANGUAGE_CODE table.
			/// </summary>
			public String NATIVE_LANGUAGE_DOMAIN_NAME{ get; set; }

			/// <summary>
			/// Associated payee ID for the member.
			/// </summary>
			public String PAYEE_HCC_ID{ get; set; }

			/// <summary>
			/// Pre-reduction reason code.
			/// </summary>
			public Int64 PRE_REDUCTION_REASON{ get; set; }

			/// <summary>
			/// Member primary language. Primary key of the LANGUAGE_CODE table.
			/// </summary>
			public String PRIMARY_LANGUAGE_DOMAIN_NAME{ get; set; }

			/// <summary>
			/// Credible Coverage period in days (format: x,xxx).
			/// </summary>
			public Int64 PRIOR_COVERAGE_MONTH_COUNT{ get; set; }

			/// <summary>
			/// Member relationship to subscriber. Joins to MEMBER_HISTORY_FACT.RLTP_TO_SUBSCRIBER_KEY. Primary key for RLTP_TO_SUBSCRIBER table.
			/// </summary>
			public String RLTP_TO_SUBSCRIBER_NAME{ get; set; }

			/// <summary>
			/// Code for salary grade of the member.
			/// </summary>
			public String SALARY_GRADE_CODE{ get; set; }

			/// <summary>
			/// Frequency of salary payments.
			/// </summary>
			public String SALARY_INTERVAL{ get; set; }

			/// <summary>
			/// Institution where member is enrolled.
			/// </summary>
			public String SCHOOL_NAME{ get; set; }

			/// <summary>
			/// Type of school where member is enrolled.
			/// </summary>
			public Int64 SCHOOL_TYPE{ get; set; }

			/// <summary>
			/// Member smoking status (e.g., Non Smoker, Smoker, Unknown).
			/// </summary>
			public String SMOKING_STATUS{ get; set; }

			/// <summary>
			/// The status of the student. Joins to MEMBER_HISTORY_FACT.STUDENT_STATUS_CODE. Values are: F - Full-time N - Not a Student P - Part-time
			/// </summary>
			public String STUDENT_STATUS_NAME{ get; set; }

			/// <summary>
			/// Subscriber first name.
			/// </summary>
			public String SUBSCRIBER_FIRST_NAME{ get; set; }

			/// <summary>
			/// Subscriber full name
			/// </summary>
			public String SUBSCRIBER_FULL_NAME{ get; set; }

			/// <summary>
			/// Subscriber last name
			/// </summary>
			public String SUBSCRIBER_LAST_NAME{ get; set; }

			/// <summary>
			/// Subscriber middle name.
			/// </summary>
			public String SUBSCRIBER_MIDDLE_NAME{ get; set; }

			/// <summary>
			/// Subscriber prefix (Mr., Mrs. etc.).
			/// </summary>
			public String SUBSCRIBER_NAME_PREFIX{ get; set; }

			/// <summary>
			/// Subscriber suffix (e.g., Sr., Jr.).
			/// </summary>
			public String SUBSCRIBER_NAME_SUFFIX{ get; set; }

			/// <summary>
			/// The social security number of the subscriber. This is in the format of XXX-XX-XXXX.
			/// </summary>
			public String SUBSCRIBER_TAX_ID{ get; set; }

			/// <summary>
			/// Subscription identifier assigned by HCC.
			/// </summary>
			public String SUBSCRIPTION_HCC_ID{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 SUBSCRIBER_DIM_KEY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String MEMBER_TAX_ID{ get; set; }

			/// <summary>
			/// Telephone number of the member.
			/// </summary>
			public String TELEPHONE_NUMBER{ get; set; }

			/// <summary>
			/// The member title.
			/// </summary>
			public String TITLE{ get; set; }

			/// <summary>
			/// 
			/// </summary>
			public String UNITS_HOURS_INTV_NAME{ get; set; }

			/// <summary>
			/// IP Reason Name
			/// </summary>
			public String VIP_REASON_NAME{ get; set; }

			/// <summary>
			/// Claim Restrictions
			/// </summary>
			public String WAIVE_PHI_CLAIM_RESTRICTION{ get; set; }

			/// <summary>
			/// Audit Log Key
			/// </summary>
			public Int64 AUDIT_LOG_KEY{ get; set; }

			/// <summary>
			/// Disability Verification Date
			/// </summary>
			public Int32 DISABILITY_VERIF_RCPT_DATE{ get; set; }

			/// <summary>
			/// Disability Reported Date
			/// </summary>
			public Int32 DISABILITY_REPORTED_DATE{ get; set; }

			/// <summary>
			/// Effective First Date
			/// </summary>
			public Int32 FIRST_EFFECTIVE_DATE{ get; set; }

			/// <summary>
			/// Member Graduation Date
			/// </summary>
			public Int32 GRADUATION_DATE{ get; set; }

			/// <summary>
			/// Member Employer Hire Date
			/// </summary>
			public Int32 HIRE_DATE{ get; set; }

			/// <summary>
			/// Member birth Date
			/// </summary>
			public Int32 MEMBER_BIRTH_DATE{ get; set; }

			/// <summary>
			/// Member Death Date
			/// </summary>
			public Int32 MEMBER_DEATH_DATE{ get; set; }

			/// <summary>
			/// Memmber Effective Date
			/// </summary>
			public Int32 MEMBER_EFFECTIVE_DATE{ get; set; }

			/// <summary>
			/// Member Receipt Date
			/// </summary>
			public Int32 MEMBER_RECEIPT_DATE{ get; set; }

			/// <summary>
			/// Member job termination Date
			/// </summary>
			public Int32 MEMBER_TERMINATION_DATE{ get; set; }

			/// <summary>
			/// Reducation Reason Reciept Date
			/// </summary>
			public Int32 PRE_RED_REASON_RECPT_DATE{ get; set; }

			/// <summary>
			/// Reduction Reason End Date
			/// </summary>
			public Int32 PRE_REDUCTION_END_DATE{ get; set; }

			/// <summary>
			/// Reduction start Date
			/// </summary>
			public Int32 PRE_REDUCTION_START_DATE{ get; set; }

			/// <summary>
			/// Member Retirement Date
			/// </summary>
			public Int32 RETIREMENT_DATE{ get; set; }

			/// <summary>
			/// Subscription Reciept Date
			/// </summary>
			public Int32 SUBSCRIPTION_RECEIPT_DATE{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
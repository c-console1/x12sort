using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// Key to Account Contact
		/// </summary>
		public class ACCOUNT_CONTACT_DETAILS{

			/// <summary>
			/// Key to Account Contact
			/// </summary>
			public Int64 ACCOUNT_CONTACT_KEY{ get; set; }

			/// <summary>
			/// Dimension key for Account
			/// </summary>
			public Int64 ACCOUNT_DIM_KEY{ get; set; }

			/// <summary>
			/// First name of account contact.
			/// </summary>
			public String CONTACT_FIRST_NAME{ get; set; }

			/// <summary>
			/// Full name of account contact.
			/// </summary>
			public String CONTACT_FULL_NAME{ get; set; }

			/// <summary>
			/// Last name of account contact.
			/// </summary>
			public String CONTACT_LAST_NAME{ get; set; }

			/// <summary>
			/// Long description of contact type. Exmaples are: Not Applicable, Not Specified, Business Associate.
			/// </summary>
			public String CONTACT_TYPE_NAME{ get; set; }

			/// <summary>
			/// Email address for this contact.
			/// </summary>
			public String EMAIL_ADDRESS{ get; set; }

			/// <summary>
			/// Telephone number for this contact.
			/// </summary>
			public String TELEPHONE_NUMBER{ get; set; }

			/// <summary>
			/// Weight is used to report aggregated values where one to many relationships are involved. It is used in bridge tables to allow creation of reports that join through one-to-many relationships without double counting. The weight is one over the number of rows in the bridge table.
			/// </summary>
			public Decimal WEIGHT{ get; set; }

			/// <summary>
			/// A sort order value of 1 denotes the primary relationship between the two entities. Higher values denote secondary, tertiary relationships etc.
			/// </summary>
			public Int64 SORT_ORDER{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
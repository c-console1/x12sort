using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// No Description Found
		/// </summary>
		public class BILLING_RATE_ENTRY_DIM{

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 BILLING_RATE_ENTRY_DIM_KEY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 BILLING_RATE_ENTRY_SET_FCT_KEY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Decimal COVERAGE_INCREMENT{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String GENDER_NAME{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 MAX_AGE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Decimal MAX_COVERAGE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 MIN_AGE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Decimal MIN_COVERAGE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Decimal PERCENTAGE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Decimal RATE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String SMOKING_STATUS{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 MONTHS_FROM_HIRE_DATE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
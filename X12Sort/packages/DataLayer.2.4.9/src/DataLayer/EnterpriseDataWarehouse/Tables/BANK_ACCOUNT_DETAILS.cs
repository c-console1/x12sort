using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// Used internally by Cobol routines.
		/// </summary>
		public class AMISYS_SUBSCRIBER_DIM{

			/// <summary>
			/// Used internally by Cobol routines.
			/// </summary>
			public Int64 IMAGE_RECNBR{ get; set; }

			/// <summary>
			/// Identification number assigned to the contract holder.
    /// 			Identification number assigned to the contract holder
    /// 			of the health insurance policy. The CONTRACT# is also
    /// 			incorporated in the MEMBER# to help tie members to
    /// 			the the contract holder. Also known as the subscriber.
			/// </summary>
			public String CONTRACT_NBR{ get; set; }

			/// <summary>
			/// The [AA] medical service area code category defines
    /// 			the demographic regional breakdown for pricing,
    /// 			enrollment, and capitation.
			/// </summary>
			public String AREA{ get; set; }

			/// <summary>
			/// The [CR] ID card distribution code category defines the
    /// 			generation status of membership cards.
			/// </summary>
			public String CARD{ get; set; }

			/// <summary>
			/// The [CN] county code category defines the county of
    /// 			residence for the given entity.
    /// 			The demographic or geographic 'county' defined by the
    /// 			health plan to classify membership, provider, broker,
    /// 			and address information. Since it is user defined, the
    /// 			[CN] code may not represent the official county of the
    /// 			entity's location.
			/// </summary>
			public String COUNTY{ get; set; }

			/// <summary>
			/// First name.
			/// </summary>
			public String FIRSTNAME{ get; set; }

			/// <summary>
			/// [NS] Former insurance carrier.
			/// </summary>
			public String FORMER_INS{ get; set; }

			/// <summary>
			/// Language indicator (English, Spanish, French).
    /// 			The [LG] code category is used to identify the language
    /// 			used for correspondence to the membership.
			/// </summary>
			public String LANGUAGE_X{ get; set; }

			/// <summary>
			/// Last name.
			/// </summary>
			public String LASTNAME{ get; set; }

			/// <summary>
			/// Positional flags to indicate if a given record is
    /// 			locked due to processing requirements.
    /// 			For the CLAIM table, LOCK positions 5 and 6 are
    /// 			reserved for user entered EXPLAIN codes for *'d status
    /// 			values.
    /// 			For the CONTRACT and MEMBER tables,
    /// 			if position 1 = Y, then record is locked by billing
    /// 			if position 2 = Y, then record is locked by capitation
    /// 			if position 3 = Y, then record is locked by membership
			/// </summary>
			public String LOCK_X{ get; set; }

			/// <summary>
			/// Marketing representative for the member.
    /// 			Code set entry that identifies the marketing rep that
    /// 			signed the member to the health plan.
			/// </summary>
			public String MARKETING_REP{ get; set; }

			/// <summary>
			/// Extended First Name
    /// 			Identifies the first name of a given entity.
			/// </summary>
			public String NAME_FIRST{ get; set; }

			/// <summary>
			/// Extended Last Name
    /// 			Identifies the last name of a given entity.
    /// 			The NAME-LAST field in MEMBER-PREV-INFO holds the
    /// 			member Last Name prior to the last database update. The
    /// 			MENBER-PREV-INFO table captures name corrections as
    /// 			well as legal name changes.
    /// 			Note that the LASTNAME-P field, which resides in the
    /// 			MEMBER-INFO table, is only intended to capture
    /// 			legal name changes.
			/// </summary>
			public String NAME_LAST{ get; set; }

			/// <summary>
			/// Extended Middle Name
    /// 			Identifies the middle name of a given entity
			/// </summary>
			public String NAME_MIDDLE{ get; set; }

			/// <summary>
			/// Extended Name Prefix
    /// 			Identifies the name prefix of a given entity. Examples
    /// 			are 'MR', 'MS', and 'MRS'
			/// </summary>
			public String NAME_PREFIX{ get; set; }

			/// <summary>
			/// Extended Name Suffix
    /// 			Identifies the name suffix of a given entity. Examples
    /// 			are 'JR', 'SR', 'I', 'II', and 'III'
			/// </summary>
			public String NAME_SUFFIX{ get; set; }

			/// <summary>
			/// Operator ID# of individual performing transaction update.
			/// </summary>
			public String OP_NBR{ get; set; }

			/// <summary>
			/// For AUTHORIZATION, ADMISSION, and AUTH-DETAIL,
    /// 			TRANSCODE position 1: A (add), C (change)
    /// 			position 2: B (batch), blank (online)
			/// </summary>
			public String TRANSCODE{ get; set; }

			/// <summary>
			/// Anniversary date.
			/// </summary>
			public Int64 YMDANNIV{ get; set; }

			/// <summary>
			/// Cobra Review Date.
    /// 			
			/// </summary>
			public Int64 YMDCOBRA{ get; set; }

			/// <summary>
			/// User definable date field. The label for this field can
    /// 			be configured through MELABEL of the SUPER-TABLE. The
    /// 			value is not used or edited by AMISYS processes. The
    /// 			value is for information only.
			/// </summary>
			public Int64 YMDMISC{ get; set; }

			/// <summary>
			/// Date of most recent transaction.
			/// </summary>
			public Int64 YMDTRANS{ get; set; }

			/// <summary>
			/// Counter incremented via trigger after any updates.
    /// 			Used by the JAVA to duplicate the 'record has changed
    /// 			since you found it' feature of AMISYS 3000 and
    /// 			AMISYS Advance Release 1 that is provided by the Cognos
    /// 			Quick screens.
			/// </summary>
			public Int64 CHECKSUM_VALUE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public DateTime RunTimeNow{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String TBLNM{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int64 BATCH_ID{ get; set; }

	}
}
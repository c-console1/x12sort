using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// Primary key for PRODUCT table. Auto-generated surrogate key.
		/// </summary>
		public class PRODUCT_DETAILS{

			/// <summary>
			/// Primary key for PRODUCT table. Auto-generated surrogate key.
			/// </summary>
			public Int64 PRODUCT_KEY{ get; set; }

			/// <summary>
			/// HCC assigned identifier for Product provided by HCC.
			/// </summary>
			public String PRODUCT_HCC_ID{ get; set; }

			/// <summary>
			/// Name Sub-Company which provides this product.
			/// </summary>
			public String PRODUCT_NAME{ get; set; }

			/// <summary>
			/// Description of a given Product provided by HCC.
			/// </summary>
			public String PRODUCT_DESC{ get; set; }

			/// <summary>
			/// Category of benefit plan with which this product is intended to be used (e.g., Medicare Part C, Medicare Part D, Medicare Part C and D, Medicare Supplement). Whether the product is medicare complement is determined by matching the product's benefit plan category with medicare complement plan categories defined by the health care company.
			/// </summary>
			public String BENEFIT_PLAN_CATEGORY_NAME{ get; set; }

			/// <summary>
			/// The type of benefit plan with which this product is intended to be used (medical, vision, life insurance).
			/// </summary>
			public String BENEFIT_PLAN_TYPE_NAME{ get; set; }

			/// <summary>
			/// Countries, states, counties, zip or postal codes that are part of this service area. Joins to the SERVICE_AREA table.
			/// </summary>
			public Int64 SERVICE_AREA_KEY{ get; set; }

			/// <summary>
			/// HCC assigned identifier for the Sub-Company which provides this product.
			/// </summary>
			public String SUB_COMPANY_HCC_ID{ get; set; }

			/// <summary>
			/// Name Sub-Company which provides this product.
			/// </summary>
			public String SUB_COMPANY_NAME{ get; set; }

			/// <summary>
			/// Tax ID of Sub-Company which provides this product.
			/// </summary>
			public String SUB_COMPANY_TAX_ID{ get; set; }

			/// <summary>
			/// Is the product a capitated product.
			/// </summary>
			public String IS_CAPITATED{ get; set; }

			/// <summary>
			/// Indicator to signify whether or not the product is a Medicare product.
			/// </summary>
			public String IS_MEDICARE{ get; set; }

			/// <summary>
			/// Indicator to signify whether or not a member signature is required for enrolling in benefit plan.
			/// </summary>
			public String IS_MEMBER_SIGNATURE_REQUIRED{ get; set; }

			/// <summary>
			/// Key to the PANEL_TYPE_CODE table.
			/// </summary>
			public String PANEL_TYPE_NAME{ get; set; }

			/// <summary>
			/// Medicare part D Contract Number
			/// </summary>
			public String PART_D_CONTRACT_NUMBER{ get; set; }

			/// <summary>
			/// Pharmacy part D bank identification number. This attribute is captured from Market Prominence.
			/// </summary>
			public String PART_D_RX_BIN{ get; set; }

			/// <summary>
			/// Pharmacy part D processor code number. This attribute is captured from Market Prominence.
			/// </summary>
			public String PART_D_RX_PCN{ get; set; }

			/// <summary>
			/// Indicator to signify whether or not the product requires a member to select a PCP upon enrollment.
			/// </summary>
			public String PCP_REQUIREMENT{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
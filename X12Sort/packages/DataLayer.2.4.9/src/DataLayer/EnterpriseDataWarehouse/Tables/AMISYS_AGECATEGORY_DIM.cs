using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// Primary key for POSTAL_ADDRESS table.
		/// </summary>
		public class ADDRESS_DIM{

			/// <summary>
			/// Primary key for POSTAL_ADDRESS table.
			/// </summary>
			public Int64 ADDRESS_DIM_KEY{ get; set; }

			/// <summary>
			/// Address line 1.
			/// </summary>
			public String ADDRESS_LINE{ get; set; }

			/// <summary>
			/// Address line 2.
			/// </summary>
			public String ADDRESS_LINE_2{ get; set; }

			/// <summary>
			/// Address line 3.
			/// </summary>
			public String ADDRESS_LINE_3{ get; set; }

			/// <summary>
			/// Address type (e.g., PO Box Address).
			/// </summary>
			public String ADDRESS_TYPE{ get; set; }

			/// <summary>
			/// City name.
			/// </summary>
			public String CITY_NAME{ get; set; }

			/// <summary>
			/// Country code (e.g., US for United States).
			/// </summary>
			public String COUNTY_CODE{ get; set; }

			/// <summary>
			/// Country name (e.g., UNITED STATES).
			/// </summary>
			public String COUNTY_NAME{ get; set; }

			/// <summary>
			/// Primary key for STATE_CODE table. State abreviation.
			/// </summary>
			public String STATE_CODE{ get; set; }

			/// <summary>
			/// State abreviation from STATE_CODE table.
			/// </summary>
			public String STATE_NAME{ get; set; }

			/// <summary>
			/// Country code (e.g., US for United States).
			/// </summary>
			public String COUNTRY_CODE{ get; set; }

			/// <summary>
			/// Country name (e.g., UNITED STATES).
			/// </summary>
			public String COUNTRY_NAME{ get; set; }

			/// <summary>
			/// Primary key for STATE_CODE table. State abreviation.
			/// </summary>
			public String ZIP_CODE{ get; set; }

			/// <summary>
			/// ZIP plus 4 code.
			/// </summary>
			public String ZIP_4_CODE{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
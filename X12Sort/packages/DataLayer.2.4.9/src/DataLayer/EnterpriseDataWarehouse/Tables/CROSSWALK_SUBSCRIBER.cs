using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// No Description Found
		/// </summary>
		public class CROSSWALK_MEMBER_BENEFIT_PLAN{

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 CROSSWALK_MEMBER_BENEFIT_PLAN_KEY{ get; set; }

			/// <summary>
			/// Primary key of CROSSWALK_MEMBER
			/// </summary>
			public Int64 CROSSWALK_MEMBER_KEY{ get; set; }

			/// <summary>
			/// Primary key of CROSSWALK_SUBSCRIBER
			/// </summary>
			public Int64 CROSSWALK_SUBSCRIBER_KEY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 BENEFIT_PLAN_DIM_KEY{ get; set; }

			/// <summary>
			/// This column is referring to Benefit package of Amisys Benefit PKG dimension
			/// </summary>
			public String BENEFIT_PACKAGE_CODE{ get; set; }

			/// <summary>
			/// Plan effective start date
			/// </summary>
			public Int32 REC_EFFECTIVE_START_DATE{ get; set; }

			/// <summary>
			/// Plan effective end date
			/// </summary>
			public Int32 REC_EFFECTIVE_END_DATE{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// No Description Found
		/// </summary>
		public class AMISYS_MEMBER_BENEFIT_PLAN_DETAILS{

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 IMAGE_RECNBR{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String MEMBER_NBR{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String SUBSCRIBER_NBR{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String DIVISION_NBR{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String GROUP_NBR{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String GROUP_CAT{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String BENEFIT_PKG{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String BENEFIT_STATUS{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String AFF_NBR{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String AFF_AREA{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String AFF_STATUS{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String BUSINESS_UNIT{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String CAP_CODE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String CAP_CYCLE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String CAPTYPE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String CARRIER{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String CLAIM_TYPE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String CLASS{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String SUBSCRIBERTYPE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String EMPLSTAT{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String ENROLL_SOURCE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String HAT_CODE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String HEALTHSTAT{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String INSURANCE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String MARITALSTAT{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String MEDICARE_PLAN{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String MEM_OV_FLAGS{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String OP_NBR{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String PRE_EXIST{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String PRIME_SECOND{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String PROG_NBR{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String REASON{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String REGION{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String RISKPOP{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String STATUS{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String TRANSCODE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String VOID{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 YMDEFF{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 YMDEND{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 YMDFIRSTCAP{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 YMDLASTCAP{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 YMDPREX{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 YMDPREXEND{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 YMDTRANS{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Decimal CHECKSUM_VALUE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String MEDICARE_ELIG_RY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String CITIZENSHIP{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int16 PRIOR_CVG_MONTHS{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
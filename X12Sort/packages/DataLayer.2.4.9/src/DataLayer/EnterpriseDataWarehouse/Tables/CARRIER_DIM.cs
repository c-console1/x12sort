using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// Business segment Dimension key
		/// </summary>
		public class BUSINESS_SEGMENT_DIM{

			/// <summary>
			/// Business segment Dimension key
			/// </summary>
			public Int64 BUSINESS_SEGMENT_DIM_KEY{ get; set; }

			/// <summary>
			/// Business segment name.
			/// </summary>
			public String BUSINESS_SEGMENT_NAME{ get; set; }

			/// <summary>
			/// Business segment description.
			/// </summary>
			public String BUSINESS_SEGMENT_DESC{ get; set; }

			/// <summary>
			/// Status of the business segment.
			/// </summary>
			public String BUSINESS_SEGMENT_STATUS_NAME{ get; set; }

			/// <summary>
			/// Key to the BUSINESS_SEGMENT_HISTORY_FACT table.
			/// </summary>
			public Int64 BUSINESS_SEGMENT_HIST_FACT_COUNT{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 CARRIER_DIM_KEY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 TENANT_HCC_ID{ get; set; }

			/// <summary>
			/// Key to the audit log table.
			/// </summary>
			public Int64 AUDIT_LOG_KEY{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// IMAGE_RECNBR is used internally by Cobol routines.
		/// </summary>
		public class AMISYS_PREM_RATE_RIDER_DIM{

			/// <summary>
			/// IMAGE_RECNBR is used internally by Cobol routines.
			/// </summary>
			public Int64 IMAGE_RECNBR{ get; set; }

			/// <summary>
			/// 
    /// 			It is the Concatenation of the following:
    /// 			PREMKEY [01:02] required
    /// 			BENEFIT-PKG [03:04] required
    /// 			BUSINESS-UNIT [05:06] optional
    /// 			PROG# [07:08] optional
    /// 			CARRIER [09:10] optional
    /// 			REGION [11:12] optional
    /// 			DIVISION/GROUP [13:22] optional
    /// 			DIVISION/GROUP CATEGORY [23:24] optional
    /// 			RISKPOP [25:26] optional
    /// 			TIER [27:28] optional
    /// 			SUBSCRIBERTYPE [29:30] optional
    /// 			BILL-TO-SPLIT [31:32] optional
    /// 			AGECAT [33:34] optional
    /// 			SEX [35:36] optional
    /// 			UNWRISTAT [37:38] optional
    /// 			EMPLSTAT [39:40] optional
    /// 			RIDER-TITLE [41:48] optional
    /// 			AMTCOV [49:56] optional
    /// 			FILLER [57:60] future expansion
			/// </summary>
			public String RATE_NBR{ get; set; }

			/// <summary>
			/// Assigns composition of the premium rates for this division.
			/// </summary>
			public String PREMKEY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String BENEFIT_PKG{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String BUSINESS_UNIT{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String PROG{ get; set; }

			/// <summary>
			/// Carrier Details
			/// </summary>
			public String CARRIER{ get; set; }

			/// <summary>
			/// Regional area associated with the specified entity.
			/// </summary>
			public String REGION{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String ACCOUNT_NBR{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String ACCOUNT_CAT{ get; set; }

			/// <summary>
			/// 
    /// 			Risk population, ex: commercial, medicare, medicaid, union
    /// 			federal, large groups, etc.
			/// </summary>
			public String RISKPOP{ get; set; }

			/// <summary>
			/// The rate tier that a given contract type is a part of.
			/// </summary>
			public String TIER{ get; set; }

			/// <summary>
			/// The [CY] contract type code category defines the basic
    /// 			contract structure (family, double, single, etc) that
    /// 			has been purchased from the health plan.
    /// 			The element SUBSCRIBERTYPE for the STOP LOSS subsystem
    /// 			defines the following:
    /// 			'1' PAID
    /// 			'2' INCURRED
    /// 			'3' INCURRED and PAID
			/// </summary>
			public String SUBSCRIBERTYPE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String BILL_TO_SPLIT{ get; set; }

			/// <summary>
			/// A related set of Age ranges (0-5, 6-21, 22-55, ...).
    /// 			Defines a set of age ranges and is used to derive a
    /// 			premium rate.
			/// </summary>
			public String AGECAT{ get; set; }

			/// <summary>
			/// Sex. (M)ale or (F)emale. (See '??' for more info).
    /// 			Sex. (M)ale or (F)emale.
    /// 			Some AMISYS screens do not require a value for SEX.
    /// 			These screens allow ' ' as a valid value.
    /// 			The AMISYS actuarial table processing for capitation
    /// 			rates allows a value of 'B' (for both sexes).
			/// </summary>
			public String SEX{ get; set; }

			/// <summary>
			/// Underwriting status of the person covered.
    /// 			Defines the Underwriting status of the person covered.
    /// 			In this case the dependent with reduced coverage.
    /// 			Valid Code-set keywords are:
    /// 			'PASSED ', 'WAIVED ', 'DENIED ',
    /// 			'HIGH RISK ', 'NOT REQUIRED', 'NOT COVERED '
			/// </summary>
			public String UNWRISTAT{ get; set; }

			/// <summary>
			/// Employment status (such as active, retired, etc).
    /// 			[ES] Employment status codes to describe such things
    /// 			as whether or not the employee is actively working,
    /// 			retired, unemployed, etc.
			/// </summary>
			public String EMPLSTAT{ get; set; }

			/// <summary>
			/// The user defined title associated with a rider.
			/// </summary>
			public String RIDER_TITLE{ get; set; }

			/// <summary>
			/// Covergae Amounts
			/// </summary>
			public String AMTCOV{ get; set; }

			/// <summary>
			/// Filler details
			/// </summary>
			public String FILLER{ get; set; }

			/// <summary>
			/// Defines the employer''s share of premium amount.
			/// </summary>
			public Int64 AMTAFTTAX{ get; set; }

			/// <summary>
			/// Defines the employer''s share of premium amount.
			/// </summary>
			public Int64 AMTEMPLOYER{ get; set; }

			/// <summary>
			/// Employee Pre tax premium amount.
    /// 			Defines the pre tax portion of the employee''s share of
    /// 			the premium rate.
			/// </summary>
			public Int64 AMTPRETAX{ get; set; }

			/// <summary>
			/// Premium amount type (F or R) and period (M or P).
    /// 			Defines the premium amount type in the 1st position.
    /// 			(F) flat amount
    /// 			(R) rate per thousand
    /// 			Defines the premium rate period in the 2nd position.
    /// 			(M) monthly
    /// 			(P) some other period
			/// </summary>
			public String AMTYPE_PERIOD{ get; set; }

			/// <summary>
			/// Types of capitation (ie, specialist, drug, etc).
    /// 			User defined types of capitation for which a provider
    /// 			can be eligible (i.e., specialist, drug, etc). These
    /// 			values are defined in the SUPER-TABLE table.
			/// </summary>
			public String CAPTYPE{ get; set; }

			/// <summary>
			/// Columns to print premium amounts on bill.
    /// 			This field indicates in which column to print the
    /// 			amount on the Invoice.
    /// 			The valid entries depend on the style and format of
    /// 			the invoice.
    /// 			Valid values for each field are as follows:
    /// 			Byte Description
    /// 			1 The column to print employer amount
    /// 			Valid Values: 1, 2, 3, 4, 5, 6, 7, 8, 9, A, B, C
    /// 			2 The column to print employee pre-tax amount
    /// 			Valid Values: 1, 2, 3, 4, 5, 6, 7, 8, 9, A, B, C
    /// 			3 The column to print employee after-tax amount
    /// 			Valid Values: 1, 2, 3, 4, 5, 6, 7, 8, 9, A, B, C
    /// 			4 Optional 2nd column to print employer amount
    /// 			Valid Values: 1, 2, 3, 4, 5, 6, 7, 8, 9, A, B, C
    /// 			and blank (do not print)
    /// 			5 Opt 2nd column to print employee pre-tax amount
    /// 			Valid Values: 1, 2, 3, 4, 5, 6, 7, 8, 9, A, B, C
    /// 			and blank (do not print)
    /// 			6 Opt 2nd column to print employee after-tax amount
    /// 			Valid Values: 1, 2, 3, 4, 5, 6, 7, 8, 9, A, B, C
    /// 			and blank (do not print)
    /// 			7 Dollar Classification
    /// 			Valid Values: P Premium classification
    /// 			F Fund classification
    /// 			Blank Classification is not used
    /// 			8 Indicator to optionally print volume on invoice
    /// 			Valid Values: 0 No volume exists for rate
    /// 			or do not use the volume
    /// 			Blank No volume exists for rate
    /// 			or do not use the volume
    /// 			The following values indicate
    /// 			that volume is to be printed on
    /// 			the invoice, and in which column
    /// 			1, 2, 3, 4, 5, 6, 7, 8, 9, A, B, C
    /// 			The values A, B, C as described above are:
    /// 			A - Print in column 10
    /// 			B - Print in column 11
    /// 			C - Print in column 12
			/// </summary>
			public String COLS_PRINT{ get; set; }

			/// <summary>
			/// Defines (A)utomatic, (B)asic, or (S)upplemental Coverage.
			/// </summary>
			public String COV_ABS{ get; set; }

			/// <summary>
			/// Operator ID# of individual performing transaction update.
			/// </summary>
			public String OP_NBR{ get; set; }

			/// <summary>
			/// Second level Premkey for child dependents.
    /// 			Contains the premkey for building the RATE# when the
    /// 			dependents have a different set of variables than
    /// 			the ones defining who is eligible for the rider.
			/// </summary>
			public String PREMKEY_CHILD{ get; set; }

			/// <summary>
			/// Second level Premkey for contract holder.
    /// 			Contains the premkey for building the RATE# when the
    /// 			contract holder has a different set of variables than
    /// 			the ones defining who is eligible for the rider.
			/// </summary>
			public String PREMKEY_CONTR{ get; set; }

			/// <summary>
			/// Second level Premkey for spouse.
    /// 			Contains the premkey for building the RATE# when the
    /// 			spouse has a different set of variables than the ones
    /// 			defining who is eligible for the rider.
			/// </summary>
			public String PREMKEY_SPOUS{ get; set; }

			/// <summary>
			/// [RY] Reason for data entry or modification.
			/// </summary>
			public String REASON{ get; set; }

			/// <summary>
			/// Defines rider as Med/Life/Fin/Admin/SuppLife/Dental/etc
    /// 			Defines the rider class.
    /// 			M Medical
    /// 			L Life
    /// 			F Financial
    /// 			A Administrative Fee
    /// 			SL Supplemental Life
    /// 			DL Dependent Life
    /// 			AD Accidental Death and Dismemberment
    /// 			D Dental
    /// 			SD Short Term Disability
    /// 			LD Long Term Disability
    /// 			SS Specific Stop Loss
    /// 			AS Aggregate Stop Loss
			/// </summary>
			public String RIDER_CLASS{ get; set; }

			/// <summary>
			/// Rounding Rule: (U)p, (D) own or (N)earest.
    /// 			Defines a rounding rule used when the premium amount
    /// 			is a (R)ate per thousand.
    /// 			''U'' rounds UP
    /// 			''D'' rounds DOWN
    /// 			''N'' rounds to the nearest ROUNDVALUE.
			/// </summary>
			public String ROUNDRULE{ get; set; }

			/// <summary>
			/// Value to round to (for rate per thousand prem rates).
    /// 			Defines a decimal value (used when the premium amount
    /// 			is a Rate per thousand) to ROUND to in accordance with
    /// 			the ROUNDRULE.
			/// </summary>
			public Int64 ROUNDVALUE{ get; set; }

			/// <summary>
			/// Transaction code.
    /// 			For AUTHORIZATION, ADMISSION, and AUTH-DETAIL,
    /// 			TRANSCODE position 1: A (add), C (change)
    /// 			position 2: B (batch), blank (online)
			/// </summary>
			public String TRANSCODE{ get; set; }

			/// <summary>
			/// A ''V'' in the first position indicates a voided record.
    /// 			A ''V'' in the first position indicates that the record
    /// 			has been voided (most likely due to errors). Voided
    /// 			records are generally excluded from validation scans.
    /// 			For the CLEARCK-TAPE record, the second position of
    /// 			VOID indicates the reason for the void.
			/// </summary>
			public String VOID{ get; set; }

			/// <summary>
			/// End date.
			/// </summary>
			public Int32 YMDEND{ get; set; }

			/// <summary>
			/// Date of most recent transaction.
			/// </summary>
			public Int32 YMDTRANS{ get; set; }

			/// <summary>
			/// Effective or start date.
			/// </summary>
			public Int32 YMDEFF{ get; set; }

			/// <summary>
			/// [RT] Rider type.
			/// </summary>
			public String RIDER_TYPE{ get; set; }

			/// <summary>
			/// Counter incremented via trigger after any updates.
    /// 			Used by the JAVA to duplicate the ''record has changed
    /// 			since you found it'' feature of AMISYS 3000 and
    /// 			AMISYS Advance Release 1 that is provided by the Cognos
    /// 			Quick screens.
			/// </summary>
			public Decimal CHECKSUM_VALUE{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String DERIVED_RIDER_TITLE_RATE_NBR{ get; set; }

	}
}
using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// Primary key of the TAX_ENTITY_HISTORY_FACT table.
		/// </summary>
		public class TAX_DIM{

			/// <summary>
			/// Primary key of the TAX_ENTITY_HISTORY_FACT table.
			/// </summary>
			public Int64 TAX_DIM_KEY{ get; set; }

			/// <summary>
			/// Address of the tax entity. Joins to the POSTAL_ADDRESS table.
			/// </summary>
			public Int64 TAX_ADDRESS_DIM_KEY{ get; set; }

			/// <summary>
			/// Tax ID of the supplier.
			/// </summary>
			public String TAX_ID{ get; set; }

			/// <summary>
			/// Tax entity name.
			/// </summary>
			public String TAX_NAME{ get; set; }

			/// <summary>
			/// Tax ID number.
			/// </summary>
			public String TIN{ get; set; }

			/// <summary>
			/// The Social Security Number.
			/// </summary>
			public String SSN{ get; set; }

			/// <summary>
			/// Status code (a is Active, t is Terminated, u is Unknown).
			/// </summary>
			public String TAX_ENTITY_STATUS_NAME{ get; set; }

			/// <summary>
			/// Always a value of 1. Used in aggregations.
			/// </summary>
			public Int64 TAX_ENTITY_HISTORY_FACT_COUNT{ get; set; }

			/// <summary>
			/// Business name, trade name, DBA name, or disregarded entity name if different from the name reported to the IRS.
			/// </summary>
			public String BUSINESS_NAME{ get; set; }

			/// <summary>
			/// Federal tax classification.
			/// </summary>
			public String BUSINESS_TYPE{ get; set; }

			/// <summary>
			/// Other Business types
			/// </summary>
			public String BUSINESS_TYPE_OTHER{ get; set; }

			/// <summary>
			/// Code identifies payee is exempt from reporting under FATCA. The code applies to persons submitting this form for accounts maintained outside of the United States by certain foreign financial institutions.
			/// </summary>
			public String EXEMPT_FATCA_PAYEE_CD{ get; set; }

			/// <summary>
			/// Code identifies payee is exempt from backup withholding.
			/// </summary>
			public String EXEMPT_PAYEE_CD{ get; set; }

			/// <summary>
			/// Federal tax classification for Limited Liability Corporations (LLC).
			/// </summary>
			public String LTD_LIAB_COMP_BUSINESS_TYPE{ get; set; }

			/// <summary>
			/// Primary key for AUDIT_LOG table. Auto-generated surrogate key.
			/// </summary>
			public Int64 AUDIT_LOG_KEY{ get; set; }

			/// <summary>
			/// 
			/// </summary>
			public Int32 SIGN_DATE{ get; set; }

			/// <summary>
			/// Effective date for the tax entity. Joins to the DATE_DIMENSION table.
			/// </summary>
			public Int32 TAX_ENTITY_EFF_DATE{ get; set; }

			/// <summary>
			/// Termination date of the tax entity. Joins to the DATE_DIMENSION table.
			/// </summary>
			public Int32 TAX_ENTITY_TERM_DATE{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
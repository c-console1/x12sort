using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// Primary key for MEMBER_UDT table.
		/// </summary>
		public class MEMBER_UDT_DETAILS{

			/// <summary>
			/// Primary key for MEMBER_UDT table.
			/// </summary>
			public Int64 MEMBER_UDT_KEY{ get; set; }

			/// <summary>
			/// Udt Key
			/// </summary>
			public Int64 UDT_KEY{ get; set; }

			/// <summary>
			/// UDT Name
			/// </summary>
			public String UDT_NAME{ get; set; }

			/// <summary>
			/// The name of this attribute.
			/// </summary>
			public String MEMBER_UDT_ATTR_NAME{ get; set; }

			/// <summary>
			/// Member UDT Attribute number value
			/// </summary>
			public Decimal MEMBER_UDT_ATTR_NUM_VALUE{ get; set; }

			/// <summary>
			/// Member UDT Attribute time value
			/// </summary>
			public DateTime MEMBER_UDT_ATTR_TIME_VALUE{ get; set; }

			/// <summary>
			/// Member Udt Attribute Value
			/// </summary>
			public String MEMBER_UDT_ATTR_VALUE{ get; set; }

			/// <summary>
			/// Member UDT Attribute Date
			/// </summary>
			public Int32 MEMBER_UDT_ATTR_DATE{ get; set; }

			/// <summary>
			/// The long description of this attribute.
			/// </summary>
			public String VALUE_LONG_NAME{ get; set; }

			/// <summary>
			/// The short name of this attribute.
			/// </summary>
			public String VALUE_SHORT_NAME{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
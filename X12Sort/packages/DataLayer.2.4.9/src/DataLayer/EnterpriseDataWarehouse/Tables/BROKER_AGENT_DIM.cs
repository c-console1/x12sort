using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// No Description Found
		/// </summary>
		public class BILLING_RATE_TO_BILLING_RATE_UDT{

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 BILLING_RATE_DIM_KEY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 BILLING_RATE_UDT_DIM_KEY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// IMAGE_RECNBR is used internally by Cobol routines.
		/// </summary>
		public class AMISYS_DIVISION_DIM{

			/// <summary>
			/// IMAGE_RECNBR is used internally by Cobol routines.
			/// </summary>
			public Int64 IMAGE_RECNBR{ get; set; }

			/// <summary>
			/// Identification number of the employer division.
    /// 			Identification number assigned to employer group
    /// 			divisions. For ease of identification, the DIVISION#
    /// 			consists of GROUP#, X(6) plus a 4 character extension.
			/// </summary>
			public String DIVISION_NBR{ get; set; }

			/// <summary>
			/// Number assigned to employer groups.
    /// 			The number assigned to employer groups who have
    /// 			contracted with the health plan to provide health
    /// 			insurance coverage.
			/// </summary>
			public String GROUP_NBR{ get; set; }

			/// <summary>
			/// Allows divisions to be billed in various combinations.
    /// 			If billing at corporation level (BILL-TO = C), then
    /// 			ROLLUP# will indicate to which corporation the division
    /// 			will be billed. Divisions have the ability to ''rollup''
    /// 			to any corporation.
    /// 			If billing at group level (BILL-TO = G), then ROLLUP#
    /// 			will indicate to which group the division will be billed.
    /// 			Divisions have the ability to ''rollup'' to any group.
    /// 			If billing at the division level (BILL-TO = D), then
    /// 			ROLLUP# will be DIVISION#.
    /// 			If billing at the individual level (BILL-TO = I), then
    /// 			ROLLUP# will be DIVISION#. Invoices will be sent to the
    /// 			individual subscribers within the division.
			/// </summary>
			public String ROLLUP_NBR{ get; set; }

			/// <summary>
			/// Alternate division identifier.
    /// 			Identifies division information in lieu of the actual
    /// 			DIVISION#.
			/// </summary>
			public String ALT_KEY1{ get; set; }

			/// <summary>
			/// [AP] Age policy for setting termination dates.
    /// 			AP code category. It governs the automatic
    /// 			initialization of end dates for students and
    /// 			other dependents.
			/// </summary>
			public String AGEPOLICY{ get; set; }

			/// <summary>
			/// Prorate/Wash/1st of Mo/Prorate rate chg/Full Mo/Full Per
    /// 			Billing method or invoice calculation method.
    /// 			1 = PRORATE
    /// 			2 = WASH METHOD
    /// 			3 = 1ST OF THE MONTH
    /// 			4 = PRORATE WITH RATE CHANGE - a special form of
    /// 			proration that allows one or more rate changes
    /// 			in the billing cycle.
    /// 			5 = FULL MONTH - if a Contract is effective at least
    /// 			one day in a month, bill the Division for a full
    /// 			months premium.
    /// 			6 = FULL PERIOD - if a Contract is effective at least
    /// 			one day in a non-monthly billing period, then
    /// 			bill the Division for the full period premium.
    /// 			7 = BILLING PERIOD - for billing periods defined in
    /// 			PERIOD-DETAIL and related to the Calendar Month
    /// 			of Coverage in the Cycle (AC) record keywords in
    /// 			CODE-DETAIL.
			/// </summary>
			public String BILL_METHOD{ get; set; }

			/// <summary>
			/// The entity (Corp/Group/Division/Indiv) who is to be billed.
    /// 			C = Corporation level billing
    /// 			G = Group level billing
    /// 			D = Division level billing
    /// 			I = Individual billing
    /// 			For the TICKLER and LETTER-STATUS tables, the BILL-TO
    /// 			value shows the billing entity who has a delinquent
    /// 			amount due, and was sent a dunning letter.
			/// </summary>
			public String BILL_TO{ get; set; }

			/// <summary>
			/// Independent geographic or profit center.
    /// 			The independent geographic or profit center entities
    /// 			within the health plan with which members and providers
    /// 			are affiliated.
			/// </summary>
			public String BUSINESS_UNIT{ get; set; }

			/// <summary>
			/// Carrier / underwriter of risk.
    /// 			Identifies the carrier / underwriter of risk and how
    /// 			the risk will be managed. Valid values are defined
    /// 			in the SUPER-TABLE table.
			/// </summary>
			public String CARRIER{ get; set; }

			/// <summary>
			/// Customer representative to whom inquires are directed.
			/// </summary>
			public String CONTACT{ get; set; }

			/// <summary>
			/// [CN] County.
    /// 			The [CN] county code category defines the county of
    /// 			residence for the given entity.
    /// 			The demographic or geographic ''county'' defined by the
    /// 			health plan to classify membership, provider, broker,
    /// 			and address information. Since it is user defined, the
    /// 			[CN] code may not represent the official county of the
    /// 			entity''s location.
			/// </summary>
			public String COUNTY{ get; set; }

			/// <summary>
			/// [DC] Division category.
    /// 			DIV-CAT or division category, provides a method
    /// 			for the healthplan to define similar divisions.
    /// 			The healthplan has the option of structuring
    /// 			premium rates and account types by DIV-CAT.
			/// </summary>
			public String DIV_CAT{ get; set; }

			/// <summary>
			/// [DV] Division status.
    /// 			The [DV] division status code category defines the
    /// 			grace period for the premium payment.
			/// </summary>
			public String DIV_STATUS{ get; set; }

			/// <summary>
			/// Defines the Employer Identification Number
    /// 			Defines the Employer Identification Number.
    /// 			EIN is a 9 digit number assigned by the Internal
    /// 			Revenue Service. The EIN is formatted as nn-nnnnnnn,
    /// 			however, formatting characters are not included in
    /// 			AMISYS. An EIN may also be referred to as a Taxpayer
    /// 			Identification Number (TIN). The EIN is synonymous
    /// 			with the HIPAA X12 National Employer ID (NEI).
			/// </summary>
			public String EIN{ get; set; }

			/// <summary>
			/// Number of eligible employees.
    /// 			ELIGIBLE# is the number of eligible employees in a
    /// 			division. It is informational only, and is
    /// 			generally used by marketing to indicate the size of
    /// 			the client''s company.
			/// </summary>
			public Int64 ELIGIBLE_NBR{ get; set; }

			/// <summary>
			/// Number of enrolled participants (employees/contracts).
			/// </summary>
			public Int64 ENROLLED_NBR{ get; set; }

			/// <summary>
			/// NULL
    /// 			Positional flags to indicate if a given record is
    /// 			locked due to processing requirements.
    /// 			For the CLAIM table, LOCK positions 5 and 6 are
    /// 			reserved for user entered EXPLAIN codes for *''d status
    /// 			values.
    /// 			For the CONTRACT and MEMBER tables,
    /// 			if position 1 = Y, then record is locked by billing
    /// 			if position 2 = Y, then record is locked by capitation
    /// 			if position 3 = Y, then record is locked by membership ';
			/// </summary>
			public String LOCK_X{ get; set; }

			/// <summary>
			/// Marketing rep responsible for inquires from employer group.
			/// </summary>
			public String MKTREP{ get; set; }

			/// <summary>
			/// Name associated with the given entity.
    /// 			The name associated with the given entity for the
    /// 			specific application. This could be the name of the
    /// 			employer group, division, corporation, etc.
			/// </summary>
			public String NAME_X{ get; set; }

			/// <summary>
			/// Benefit eligibility policy for new hires.
			/// </summary>
			public String NEWHIRE{ get; set; }

			/// <summary>
			/// Operator ID# of individual performing transaction update.
			/// </summary>
			public String OP_NBR{ get; set; }

			/// <summary>
			/// Identifies health plan, IPA, and/or program area.
			/// </summary>
			public String PROG_NBR{ get; set; }

			/// <summary>
			/// Additional health plan identifier
			/// </summary>
			public String REFERENCE_NBR{ get; set; }

			/// <summary>
			/// Regional area associated with the specified entity.
			/// </summary>
			public String REGION{ get; set; }

			/// <summary>
			/// Identifies health plan, IPA, and/or program area
    /// 			For the DIVISION table, SUB-PROG# works in
    /// 			conjuction with the SUPER-TABLE / Member-Init record and
    /// 			AFFILIATION.SUB-PROG# to validate member selection of
    /// 			providers within the proper sub-program or network.
			/// </summary>
			public String SUB_PROG_NBR{ get; set; }

			/// <summary>
			/// Transaction code.
    /// 			For AUTHORIZATION, ADMISSION, and AUTH-DETAIL,
    /// 			TRANSCODE position 1: A (add), C (change)
    /// 			position 2: B (batch), blank (online)
			/// </summary>
			public String TRANSCODE{ get; set; }

			/// <summary>
			/// Anniversary date.
			/// </summary>
			public Int64 YMDANNIV{ get; set; }

			/// <summary>
			/// Date of most recent transaction.
			/// </summary>
			public Int64 YMDTRANS{ get; set; }

			/// <summary>
			/// Counter incremented via trigger after any updates.
    /// 			Used by the JAVA to duplicate the ''record has changed
    /// 			since you found it'' feature of AMISYS 3000 and
    /// 			AMISYS Advance Release 1 that is provided by the Cognos
    /// 			Quick screens.
			/// </summary>
			public Int64 CHECKSUM_VALUE{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public DateTime RunTimeNow{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String TBLNM{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int64 BATCH_ID{ get; set; }

	}
}
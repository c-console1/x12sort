using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// Identification# assigned to members of the health plan.
    /// 			This element is a concatenation of the following:
    /// 			CONTRACT# X(9) + extension X(2)
		/// </summary>
		public class AMISYS_MEDICARE_MEMBER_DETAILS{

			/// <summary>
			/// Identification# assigned to members of the health plan.
    /// 			This element is a concatenation of the following:
    /// 			CONTRACT# X(9) + extension X(2)
			/// </summary>
			public String MEMBER_NBR{ get; set; }

			/// <summary>
			/// HCFA identification number for Medicare processing.
			/// </summary>
			public String HCFA_NBR{ get; set; }

			/// <summary>
			/// Alternate member identifier.
    /// 			Identifies member information in lieu of the actual MEMBER#.
			/// </summary>
			public String ALT_KEY{ get; set; }

			/// <summary>
			/// The independent geographic or profit center entities
    /// 			within the health plan with which members and providers
    /// 			are affiliated.
			/// </summary>
			public String BUSINESS_UNIT{ get; set; }

			/// <summary>
			/// Member First name
			/// </summary>
			public String FIRSTNAME{ get; set; }

			/// <summary>
			/// Member Last Name
			/// </summary>
			public String LASTNAME{ get; set; }

			/// <summary>
			/// Operator ID# of individual performing transaction update.
			/// </summary>
			public String OP_NBR{ get; set; }

			/// <summary>
			/// Identifies health plan, IPA, and/or program area
			/// </summary>
			public String PROG_NBR{ get; set; }

			/// <summary>
			/// Sex. (M)ale or (F)emale. (See '??' for more info).
    /// 			Sex. (M)ale or (F)emale.
    /// 			Some AMISYS screens do not require a value for SEX.
    /// 			These screens allow ' ' as a valid value.
    /// 			The AMISYS actuarial table processing for capitation
    /// 			rates allows a value of 'B' (for both sexes).
			/// </summary>
			public String SEX{ get; set; }

			/// <summary>
			/// Social security number.
			/// </summary>
			public String SSN{ get; set; }

			/// <summary>
			/// Date Sent.
			/// </summary>
			public Int32 SU_YMDSENT{ get; set; }

			/// <summary>
			/// Medicare coverage Start Date for the member.
			/// </summary>
			public Int32 SU_YMDSTART{ get; set; }

			/// <summary>
			/// Transaction code.
    /// 			For AUTHORIZATION, ADMISSION, and AUTH-DETAIL,
    /// 			TRANSCODE position 1: A (add), C (change)
    /// 			position 2: B (batch), blank (online)
			/// </summary>
			public String TRANSCODE{ get; set; }

			/// <summary>
			/// Birth Date
			/// </summary>
			public Int32 YMDBIRTH{ get; set; }

			/// <summary>
			/// Effective or start date.
    /// 			For UB-92 this is From Date- date the occurrence starts
    /// 			The UB-92 from date is the date the occurrence is for.
    /// 			Occur date corresponds to the occur code event relating
    /// 			to this bill. This ''from date'' must not be later than
    /// 			a ''thru date'' when a thru date is entered.
    /// 			Refer to UB92 locator FIELD 32a-36a and 32b-36b
    /// 			of service. A one position, alpha numeric value. See
    /// 			UB92 FIELD locator 32 through 36 for value.
    /// 			For PROV-CON-ED, YMDEFF is the start date for the class
    /// 			or seminar.
			/// </summary>
			public Int32 YMDEFF{ get; set; }

			/// <summary>
			/// End date.
    /// 			For UB-92 this is Thru Date- date the occurrence ends.
    /// 			The UB-92 thru date is the date the occurrence is for.
    /// 			Occur date corresponds to the occur code event relating
    /// 			to this bill. This ''thru date'' must not be less than
    /// 			a ''from date'' when a thru date is entered.
    /// 			Refer to UB92 locator FIELDS 32a-36a and 32b-36b values'
			/// </summary>
			public Int32 YMDEND{ get; set; }

			/// <summary>
			/// Date of most recent transaction.
			/// </summary>
			public Int32 YMDTRANS{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// Primary key
		/// </summary>
		public class CROSSWALK_MEMBER{

			/// <summary>
			/// Primary key
			/// </summary>
			public Int64 CROSSWALK_MEMBER_KEY{ get; set; }

			/// <summary>
			/// This column is referring to the MEMBER_NBR column in Amisys MEMBER dimension
			/// </summary>
			public String AMISYS_MEMBER_ID{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 MEMBER_DIM_KEY{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
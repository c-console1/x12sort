using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// No Description Found
		/// </summary>
		public class AMISYS_ALTPOLICY_DETAILS{

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 AMISYS_ALTPOLICY_DETAILS_KEY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int32 SOURCE_KEY{ get; set; }

			/// <summary>
			/// Alternate policy number (the ALTCARRIER# + the policy#).
    /// 			The alternate policy number is a concatenation of the
    /// 			ALTCARRIER# X(4) + the policy# X(12).
			/// </summary>
			public String ALTPOLICY_NBR{ get; set; }

			/// <summary>
			/// [CY] Contract type.
    /// 			The [CY] contract type code category defines the basic
    /// 			contract structure (family, double, single, etc) that
    /// 			has been purchased from the health plan.
    /// 			The element SUBSCRIBERTYPE for the STOP LOSS subsystem
    /// 			defines the following:
    /// 			''1'' PAID
    /// 			''2'' INCURRED
    /// 			''3'' INCURRED and PAID
			/// </summary>
			public String SUBSCRIBERTYPE{ get; set; }

			/// <summary>
			/// [CO] Ten types of coverage.
    /// 			[CO] Type of coverage codes. Values can identify
    /// 			mental health, dental, vision, etc. This element
    /// 			can contain up to ten X(2) coverage codes.
			/// </summary>
			public String COV_ARRAY{ get; set; }

			/// <summary>
			/// [ES] Employment status (such as active, retired, etc).
    /// 			[ES] Employment status codes to describe such things
    /// 			as whether or not the employee is actively working,
    /// 			retired, unemployed, etc.
			/// </summary>
			public String EMPLSTAT{ get; set; }

			/// <summary>
			/// [GC] Group category.
    /// 			The [GC] group category codes define employer groups
    /// 			as the health plan requires. The health plan may
    /// 			decide to categorize their employer groups by size,
    /// 			union affiliation, commercial, etc.
			/// </summary>
			public String GROUP_CAT{ get; set; }

			/// <summary>
			/// Operator ID# of individual performing transaction update.
			/// </summary>
			public String OP_NBR{ get; set; }

			/// <summary>
			/// [RY] Reason for data entry or modification.
			/// </summary>
			public String REASON{ get; set; }

			/// <summary>
			/// Transaction code For AUTHORIZATION, ADMISSION, and AUTH-DETAIL,
    /// 			TRANSCODE position 1: A (add), C (change)
    /// 			position 2: B (batch), blank (online)
			/// </summary>
			public String TRANSCODE{ get; set; }

			/// <summary>
			/// A ''V'' in the first position indicates that the record
    /// 			has been voided (most likely due to errors). Voided
    /// 			records are generally excluded from validation scans.
    /// 			For the CLEARCK-TAPE record, the second position of
    /// 			VOID indicates the reason for the void.
			/// </summary>
			public String VOID{ get; set; }

			/// <summary>
			/// Effective or start date.
    /// 			For UB-92 this is From Date- date the occurrence starts
    /// 			The UB-92 from date is the date the occurrence is for.
    /// 			Occur date corresponds to the occur code event relating
    /// 			to this bill. This ''from date'' must not be later than
    /// 			a ''thru date'' when a thru date is entered.
    /// 			Refer to UB92 locator FIELD 32a-36a and 32b-36b
    /// 			of service. A one position, alpha numeric value. See
    /// 			UB92 FIELD locator 32 through 36 for value.
    /// 			For PROV-CON-ED, YMDEFF is the start date for the class
    /// 			or seminar.
			/// </summary>
			public Int32 YMDEFF{ get; set; }

			/// <summary>
			/// The date the entity is eligible for coverage/benefits.
    /// 			Effective date of the first span of continuous coverage.
    /// 			This is the date the entity was first eligible for
    /// 			coverage/benefits.
			/// </summary>
			public Int32 YMDELIG{ get; set; }

			/// <summary>
			/// End date.
    /// 			For UB-92 this is Thru Date- date the occurrence ends.
    /// 			The UB-92 thru date is the date the occurrence is for.
    /// 			Occur date corresponds to the occur code event relating
    /// 			to this bill. This ''thru date'' must not be less than
    /// 			a ''from date'' when a thru date is entered.
    /// 			Refer to UB92 locator FIELDS 32a-36a and 32b-36b values
			/// </summary>
			public Int32 YMDEND{ get; set; }

			/// <summary>
			/// Date of most recent transaction.
			/// </summary>
			public Int32 YMDTRANS{ get; set; }

			/// <summary>
			/// Counter incremented via trigger after any updates.
    /// 			Used by the JAVA to duplicate the ''record has changed
    /// 			since you found it'' feature of AMISYS 3000 and
    /// 			AMISYS Advance Release 1 that is provided by the Cognos
    /// 			Quick screens.
			/// </summary>
			public Int64 CHECKSUM_VALUE{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// IMAGE_RECNBR is used internally by Cobol routines.
		/// </summary>
		public class AMISYS_MEMBER_PCP_SUM{

			/// <summary>
			/// IMAGE_RECNBR is used internally by Cobol routines.
			/// </summary>
			public Int64 IMAGE_RECNBR{ get; set; }

			/// <summary>
			/// Identification# assigned to members of the health plan.
    /// 			This element is a concatenation of the following:
    /// 			CONTRACT# X(9) + extension X(2)
			/// </summary>
			public String MEMBER_NBR{ get; set; }

			/// <summary>
			/// Contractual affiliation of a provider with a health plan.
    /// 			Extension of the provider number which defines the
    /// 			unique contractual number for the provider.
    /// 			Providers with multiple relationships with a health
    /// 			plan (as a specialist, with a group practice, with
    /// 			an HMO, with a PPO, etc) will have multiple
    /// 			affiliation numbers.
    /// 			Concatenation: PROV# X12 plus 4 character affiliation
			/// </summary>
			public String AFF_NBR{ get; set; }

			/// <summary>
			/// Operator ID# of individual performing transaction update.
			/// </summary>
			public String OP_NBR{ get; set; }

			/// <summary>
			/// [RY] Reason for data entry or modification.
			/// </summary>
			public String REASON{ get; set; }

			/// <summary>
			/// A 'V' in the first position indicates a voided record.
    /// 			A 'V' in the first position indicates that the record
    /// 			has been voided (most likely due to errors). Voided
    /// 			records are generally excluded from validation scans.
    /// 			For the CLEARCK-TAPE record, the second position of
    /// 			VOID indicates the reason for the void.
			/// </summary>
			public String VOID{ get; set; }

			/// <summary>
			/// Effective or start date.
    /// 			For UB-92 this is From Date- date the occurrence starts
    /// 			The UB-92 from date is the date the occurrence is for.
    /// 			Occur date corresponds to the occur code event relating
    /// 			to this bill. This ''from date'' must not be later than
    /// 			a ''thru date'' when a thru date is entered.
    /// 			Refer to UB92 locator FIELD 32a-36a and 32b-36b
    /// 			of service. A one position, alpha numeric value. See
    /// 			UB92 FIELD locator 32 through 36 for value.
    /// 			For PROV-CON-ED, YMDEFF is the start date for the class
    /// 			or seminar.
			/// </summary>
			public Int32 YMDEFF{ get; set; }

			/// <summary>
			/// End date.
    /// 			For UB-92 this is Thru Date- date the occurrence ends.
    /// 			The UB-92 thru date is the date the occurrence is for.
    /// 			Occur date corresponds to the occur code event relating
    /// 			to this bill. This ''thru date'' must not be less than
    /// 			a ''from date'' when a thru date is entered.
    /// 			Refer to UB92 locator FIELDS 32a-36a and 32b-36b values'
			/// </summary>
			public Int32 YMDEND{ get; set; }

			/// <summary>
			/// Date of the most recent entry
			/// </summary>
			public Int32 YMDENTERED{ get; set; }

			/// <summary>
			/// Date of most recent transaction.
			/// </summary>
			public Int32 YMDTRANS{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// Used internally by Cobol routines.
		/// </summary>
		public class AMISYS_MEMBER_DIM{

			/// <summary>
			/// Used internally by Cobol routines.
			/// </summary>
			public Int64 IMAGE_RECNBR{ get; set; }

			/// <summary>
			/// Identification# assigned to members of the health plan.
    /// 			This element is a concatenation of the following:
    /// 			CONTRACT# X(9) + extension X(2)
			/// </summary>
			public String MEMBER_NBR{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String SUBSCRIBER_NBR{ get; set; }

			/// <summary>
			/// Alternate member identifier.
    /// 			Identifies member information in lieu of the actual MEMBER#.
			/// </summary>
			public String ALT_KEY{ get; set; }

			/// <summary>
			/// Member's Second Alternate Key
			/// </summary>
			public String ALT_KEY2{ get; set; }

			/// <summary>
			/// Social security number
			/// </summary>
			public String SSN{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String BROKER_NBR{ get; set; }

			/// <summary>
			/// [AA] Medical service area.
    /// 			The [AA] medical service area code category defines
    /// 			the demographic regional breakdown for pricing,
    /// 			enrollment, and capitation.
			/// </summary>
			public String AREA{ get; set; }

			/// <summary>
			/// [CR] ID card distribution.
    /// 			The [CR] ID card distribution code category defines the
    /// 			generation status of membership cards.
			/// </summary>
			public String CARD{ get; set; }

			/// <summary>
			/// MemberFirst name
			/// </summary>
			public String FIRSTNAME{ get; set; }

			/// <summary>
			/// MemberLast name
			/// </summary>
			public String LASTNAME{ get; set; }

			/// <summary>
			/// Title.
			/// </summary>
			public String TITLE{ get; set; }

			/// <summary>
			/// Extended First Name
    /// 			Identifies the first name of a given entity.
			/// </summary>
			public String NAME_FIRST{ get; set; }

			/// <summary>
			/// Extended Last Name
    /// 			Identifies the last name of a given entity.
    /// 			The NAME-LAST field in MEMBER-PREV-INFO holds the
    /// 			member Last Name prior to the last database update. The
    /// 			MENBER-PREV-INFO table captures name corrections as
    /// 			well as legal name changes.
    /// 			Note that the LASTNAME-P field, which resides in the
    /// 			MEMBER-INFO table, is only intended to capture
    /// 			legal name changes.
			/// </summary>
			public String NAME_LAST{ get; set; }

			/// <summary>
			/// Extended Middle Name
    /// 			Identifies the middle name of a given entity.
			/// </summary>
			public String NAME_MIDDLE{ get; set; }

			/// <summary>
			/// Extended Name Prefix
    /// 			Identifies the name prefix of a given entity. Examples
    /// 			are 'MR', 'MS', and 'MRS'
			/// </summary>
			public String NAME_PREFIX{ get; set; }

			/// <summary>
			/// Help: Extended Name Suffix
    /// 			Identifies the name suffix of a given entity. Examples
    /// 			are 'JR', 'SR', 'I', 'II', and 'III'
			/// </summary>
			public String NAME_SUFFIX{ get; set; }

			/// <summary>
			/// Sex. (M)ale or (F)emale. (See '??' for more info).
    /// 			Sex. (M)ale or (F)emale.
    /// 			Some AMISYS screens do not require a value for SEX.
    /// 			These screens allow ' ' as a valid value.
    /// 			The AMISYS actuarial table processing for capitation
    /// 			rates allows a value of 'B' (for both sexes).
			/// </summary>
			public String SEX{ get; set; }

			/// <summary>
			/// HCFA identification number for Medicare processing.
    /// 			
			/// </summary>
			public String HCFA_NBR{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String LOCK{ get; set; }

			/// <summary>
			/// Member's response to alternate coverage queries.
    /// 			Member's response to alternate coverage queries.
    /// 			'Y' = member responded that s/he has other coverage
    /// 			'N' = member responded that s/he do not have other coverage
    /// 			' ' = member has not responded to queries
    /// 			The health plan may configure various processing
    /// 			variations based on the value of this item.
			/// </summary>
			public String LR_RESPONSE{ get; set; }

			/// <summary>
			/// Operator ID# of individual performing transaction update.
			/// </summary>
			public String OP_NBR{ get; set; }

			/// <summary>
			/// Enter the P3 code which stands for the PCP being
    /// 			This field represents how the PCP was assigned to
    /// 			the member. If the PCP was member selected the code
    /// 			will be systematically assigned. If the PCP was assigned
    /// 			via the User Exit Algorithm or the Default process, the
    /// 			P3 code for system assigned will be used. This code
    /// 			may be updated in the Member screen (ME1700) if the
    /// 			member calls in to select their own PCP.
			/// </summary>
			public String PCPASSIGN_CODE{ get; set; }

			/// <summary>
			/// Unique identifier for each person enrolled as a member.
    /// 			System generated number to uniquely identify each
    /// 			person even if the person is covered under multiple
    /// 			contracts.
    /// 			On member entry, if the person is 'new' (the search is
    /// 			done on name, sex, date of birth, and social security
    /// 			number), then the member is assigned the next available
    /// 			PERSON# (controlled by the NUMBER-GEN-M table).
			/// </summary>
			public String PERSON_NBR{ get; set; }

			/// <summary>
			/// Patient's medical record number.
			/// </summary>
			public String RECORD_NBR{ get; set; }

			/// <summary>
			/// Transaction code.
    /// 			For AUTHORIZATION, ADMISSION, and AUTH-DETAIL,
    /// 			TRANSCODE position 1: A (add), C (change)
    /// 			position 2: B (batch), blank (online)
			/// </summary>
			public String TRANSCODE{ get; set; }

			/// <summary>
			/// Date of birth.
			/// </summary>
			public Int32 YMDBIRTH{ get; set; }

			/// <summary>
			/// Membership card date (issue date or selection date).
    /// 			For the MEMBER and MEMBERB tables, YMDCARD
    /// 			is the date the membership card was issued.
    /// 			For LETTER-STATUS of OPTION, YMDCARD is the
    /// 			selection date used in the generation of membership
    /// 			cards through CG processing. The selection date is
    /// 			compared against the member's effective date (and the
    /// 			membership card code is evaluated) to determine if
    /// 			a membership card will be generated
			/// </summary>
			public Int32 YMDCARD{ get; set; }

			/// <summary>
			/// Date of death.
			/// </summary>
			public Int32 YMDDEATH{ get; set; }

			/// <summary>
			/// Benefit pre-existing condition start date.
			/// </summary>
			public Int32 YMDPREX{ get; set; }

			/// <summary>
			/// Ending date of the pre-existing condition period.
    /// 			The ending date of the period for member specific
    /// 			pre-existing conditions.
			/// </summary>
			public Int32 YMDPREXEND{ get; set; }

			/// <summary>
			/// Date of most recent transaction.
			/// </summary>
			public Int32 YMDTRANS{ get; set; }

			/// <summary>
			/// Date of verification.
    /// 			For the MEMBER and MEMBERB tables, YMDVERIFY is
    /// 			the date the alternate coverage date was last checked.
    /// 			For the INTAKE table, YMDVERIFY is the date the
    /// 			intake coordinator determines that the intake record
    /// 			requires verification.
    /// 			For the Credential tables, YMDVERIFY is the date
    /// 			the health plan verified the information.
			/// </summary>
			public Int32 YMDVERIFY{ get; set; }

			/// <summary>
			/// This code is required when the member has specified access
    /// 			to their info.Keywords on the Confidentiality Code (C8) control
    /// 			whether an access warning is raised on the Customer Service
    /// 			Phone Log and Membership Inquiry screens when access is restricted.
    /// 			HIPAA 5010 values are:
    /// 			'R' - Restricted
    /// 			'U' - Unrestricted.
			/// </summary>
			public Decimal CONFIDENTIAL{ get; set; }

			/// <summary>
			/// Set this field to a numeric value or leave blank. This field is
    /// 			required when reporting family members with the same birth
    /// 			date if a birth sequence number is needed for proper reporting,
    /// 			tracking or response to benefits.
			/// </summary>
			public Int64 BIRTH_SEQ{ get; set; }

			/// <summary>
			/// This code is used to indicate the member's race or ethnicity (RA).
    /// 			HIPAA 5010 allows the Race or Ethnicity Code to be specified
    /// 			using a limited code set similar to the Bureau of Census or the
    /// 			more expansive Classification of Race or Ethnicity code set used
    /// 			by the CDC.This field is used for HEDIS Reporting.
			/// </summary>
			public String RACE_ETHNICITY{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String LANGUAGE{ get; set; }

			/// <summary>
			/// This field should be set to 'Y' if the member requires an interpreter.
    /// 			Used in HEDIS reporting. Valid values are:
    /// 			'Y' - Yes
    /// 			'N' - No
    /// 			'U' - Unknown.
			/// </summary>
			public String INTERPRETER_REQ{ get; set; }

			/// <summary>
			/// Data source for race/ethnicity information.
    /// 			Values are defined in the S2 code set
			/// </summary>
			public String RACE_ETHNIC_SRC{ get; set; }

			/// <summary>
			/// Data source for spoken language information.
    /// 			Values are defined in the L2 code set.
			/// </summary>
			public String LANG_SPOKEN_SRC{ get; set; }

			/// <summary>
			/// Member's written language. Values are defined in the LG code set.
			/// </summary>
			public String LANG_WRITTEN{ get; set; }

			/// <summary>
			/// Data source for written language information.
    /// 			Values are defined in the L2 code set.
			/// </summary>
			public String LANG_WRITTEN_SRC{ get; set; }

			/// <summary>
			/// Other language needs. Values are defined in the LG code set.
			/// </summary>
			public String LANG_OTHER{ get; set; }

			/// <summary>
			/// Data source for other language information.
    /// 			Values are defined in the L2 code set
			/// </summary>
			public String LANG_OTHER_SRC{ get; set; }

			/// <summary>
			/// Counter incremented via trigger after any updates.
    /// 			Used by the JAVA to duplicate the 'record has changed
    /// 			since you found it' feature of AMISYS 3000 and
    /// 			AMISYS Advance Release 1 that is provided by the Cognos
    /// 			Quick screens.
			/// </summary>
			public String CHECKSUM_VALUE{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public DateTime RunTimeNow{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public String TBLNM{ get; set; }

	}
}
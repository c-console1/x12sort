using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// Used internally by Cobol routines.
		/// </summary>
		public class AMISYS_MEMBER_INFO_DETAILS{

			/// <summary>
			/// Used internally by Cobol routines.
			/// </summary>
			public Int32 IMAGE_RECNBR{ get; set; }

			/// <summary>
			/// dentification# assigned to members of the health plan.
    /// 			This element is a concatenation of the following:
    /// 			CONTRACT# X(9) + extension X(2)
			/// </summary>
			public String MEMBER_NBR{ get; set; }

			/// <summary>
			/// Number of credit hours for which a student is enrolled.
			/// </summary>
			public String ENRL_HRS{ get; set; }

			/// <summary>
			/// Is member supplemental information being requested?
    /// 			An entry of 'Y' indicates that the current supplemental
    /// 			information is questionable and is being requested from
    /// 			the member.
			/// </summary>
			public String ENRL_REQUEST{ get; set; }

			/// <summary>
			/// Previous legal last name
    /// 			Previous legal last name. Examples may include:
    /// 			Maiden name, prior to marriage
    /// 			Married name, prior to divorce
    /// 			Child's name, prior to adoption
			/// </summary>
			public String LASTNAME_P{ get; set; }

			/// <summary>
			/// Miscellaneous information.
    /// 			Miscellaneous information. The label for this field
    /// 			can be configured through MELABEL of the SUPER-TABLE.
    /// 			The values are not used or edited by AMISYS processes.
    /// 			The values are informational only.
			/// </summary>
			public String MISC_INFO{ get; set; }

			/// <summary>
			/// Operator ID# of individual performing transaction update.
			/// </summary>
			public String OP_NBR{ get; set; }

			/// <summary>
			/// Transaction code.
    /// 			For AUTHORIZATION, ADMISSION, and AUTH-DETAIL,
    /// 			TRANSCODE position 1: A (add), C (change)
    /// 			position 2: B (batch), blank (online)
			/// </summary>
			public String TRANSCODE{ get; set; }

			/// <summary>
			/// The end date of the current student enrollment period.
			/// </summary>
			public Int32 YMDEND_ENRL{ get; set; }

			/// <summary>
			/// The next date (see ?? for table specific definition).
    /// 			For ADMISSION / ADMISSION-REVIEW / AUTH-DETAIL:
    /// 			The next review date for the inpatient or outpatient
    /// 			evaluation (system assigned).
    /// 			For CONCURRENT-REVIEW / DISCHG-PLAN:
    /// 			The next review date for the inpatient or outpatient
    /// 			evaluation (manually assigned).
    /// 			For MEMBER-INFO:
    /// 			Date of next expected receipt of Student Declaration.
			/// </summary>
			public Int32 YMDNEXT{ get; set; }

			/// <summary>
			/// 
    /// 			For the MEMBER-INFO table:
    /// 			Date the Student Declaration was received.
    /// 			For the APPEAL table:
    /// 			The date the appeal was actually resolved by the
    /// 			health plan.
			/// </summary>
			public Int32 YMDRCVD{ get; set; }

			/// <summary>
			/// Date of most recent transaction.
			/// </summary>
			public Int32 YMDTRANS{ get; set; }

			/// <summary>
			/// This field is used to indicate the student status of a non-spouse
    /// 			dependent that is not handicapped and requires a qualifying condition
    /// 			for enrollment (e.g. being an active student). Valid values are:
    /// 			'F' - Full-time
    /// 			'N' - Not a Student
    /// 			'P' - Part-time.
			/// </summary>
			public String STUDENT_STATUS{ get; set; }

			/// <summary>
			/// Counter incremented via trigger after any updates.
    /// 			Used by the JAVA to duplicate the 'record has changed
    /// 			since you found it' feature of AMISYS 3000 and
    /// 			AMISYS Advance Release 1 that is provided by the Cognos
    /// 			Quick screens.
			/// </summary>
			public Int64 CHECKSUM_VALUE{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
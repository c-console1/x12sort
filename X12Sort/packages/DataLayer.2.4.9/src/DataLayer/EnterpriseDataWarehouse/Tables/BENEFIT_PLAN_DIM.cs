using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// 
		/// </summary>
		public class BENEFIT_PLAN_COMPONENT_DIM{

			/// <summary>
			/// 
			/// </summary>
			public Int64 BENEFIT_PLAN_COMPONENT_DIM_KEY{ get; set; }

			/// <summary>
			/// Description of the benefit plan component.
			/// </summary>
			public String BENEFIT_PLAN_COMP_DESC{ get; set; }

			/// <summary>
			/// Name of benefit plan component.
			/// </summary>
			public String BENEFIT_PLAN_COMP_NAME{ get; set; }

			/// <summary>
			/// Status code (a - Active, t - Terminated, u - Unknown).
			/// </summary>
			public String BENEFIT_PLAN_COMP_STATUS_NAME{ get; set; }

			/// <summary>
			/// Count of the records
			/// </summary>
			public Int64 BNFT_PLAN_COMP_HIST_FCT_COUNT{ get; set; }

			/// <summary>
			/// Indicator for no longer used components.
			/// </summary>
			public Decimal IS_DEPRECATED{ get; set; }

			/// <summary>
			/// No Description Found
			/// </summary>
			public Int64 TENANT_HCC_ID{ get; set; }

			/// <summary>
			/// Links to the ATTACHMENT_FACT_LINK table, linking to the ATTACHMENT_FACT table. Provides a unique ID for attachments linked to benefit plan component.
			/// </summary>
			public Int64 ATTACHMENT_SET_ID{ get; set; }

			/// <summary>
			/// Primary key for AUDIT_LOG table. Auto-generated surrogate key.
			/// </summary>
			public Int64 AUDIT_LOG_KEY{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
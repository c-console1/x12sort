using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Tables
{
		/// <summary>
		/// IMAGE_RECNBR is used internally by Cobol routines.
		/// </summary>
		public class AMISYS_AGECATEGORY_DIM{

			/// <summary>
			/// IMAGE_RECNBR is used internally by Cobol routines.
			/// </summary>
			public Int64 IMAGE_RECNBR{ get; set; }

			/// <summary>
			/// Defines a related set of AGECATEGORY records.
			/// </summary>
			public String AGECATDEF{ get; set; }

			/// <summary>
			/// A related set of Age ranges (0-5, 6-21, 22-55, ...).
    /// 			Defines a set of age ranges and is used to derive a
    /// 			premium rate.
			/// </summary>
			public String AGECAT{ get; set; }

			/// <summary>
			/// Starting age in a range.
    /// 			The starting age in a range used to define an age
    /// 			category for premium billing or store an age factor for
    /// 			calculating the amount of coverage.
			/// </summary>
			public Int64 AGEFROM{ get; set; }

			/// <summary>
			/// Ending age in a range.
    /// 			The ending age in a range used to define an age
    /// 			category for premium billing or store an age factor for
    /// 			calculating the amount of coverage.
			/// </summary>
			public Int64 AGETHRU{ get; set; }

			/// <summary>
			/// Age factor for calculating amount of coverage.
    /// 			This field contains the factor used to reduce the
    /// 			amount of coverage for a defined Age range.
			/// </summary>
			public Int64 FACTORAGE{ get; set; }

			/// <summary>
			/// Operator ID# of individual performing transaction update.
			/// </summary>
			public String OP_NBR{ get; set; }

			/// <summary>
			/// Transaction code For AUTHORIZATION, ADMISSION, and AUTH-DETAIL,
    /// 			TRANSCODE position 1: A (add), C (change)
    /// 			position 2: B (batch), blank (online)
			/// </summary>
			public String TRANSCODE{ get; set; }

			/// <summary>
			/// Date of most recent transaction.
			/// </summary>
			public Int32 YMDTRANS{ get; set; }

			/// <summary>
			/// Counter incremented via trigger after any updates.
    /// 			Used by the JAVA to duplicate the 'record has changed
    /// 			since you found it' feature of AMISYS 3000 and
    /// 			AMISYS Advance Release 1 that is provided by the Cognos
    /// 			Quick screens.
			/// </summary>
			public Decimal CHECKSUM_VALUE{ get; set; }

			/// <summary>
			/// The record which was modified by
			/// </summary>
			public String LAST_MODIFIED_BY{ get; set; }

			/// <summary>
			/// The record which was modified on
			/// </summary>
			public DateTime LAST_MODIFIED_DATE{ get; set; }

			/// <summary>
			/// Batch ID of the EDW Refresh
			/// </summary>
			public Int32 BATCH_ID{ get; set; }

	}
}
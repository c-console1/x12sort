using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.SQLs
{
    public class SQLQueries
    {
        internal string GetMemberSQL(string selectedPlaceHolder = "", string joinPlaceHolder = "", string wherePlaceHolder = "", string MemberNumber=null)
        {
            string member = $@"SELECT       m.MEMBER_DIM_KEY AS MEMBER_DIM_KEY,
                                            m.ACCOUNT_DIM_Key AS ACCOUNT_DIM_KEY,
                                            m.MAILING_ADDRESS_DIM_KEY,
                                            m.HOME_ADDRESS_DIM_KEY,
                                            m.ALT_PAYMENT_RECIPIENT_ADDRESS_DIM_KEY,
                                            m.SUBSCRIBER_DIM_KEY,
                                            m.MEMBER_HCC_ID AS MemberNumber,
                                            '' AS ContractNumber,
                                            TRIM(m.FIRST_NAME) AS FirstName,
                                            TRIM(m.LAST_NAME) AS LastName,
                                            TRIM(m.MIDDLE_NAME) AS MiddleName,
                                            TRIM(m.NAME_PREFIX) AS Prefix,
                                            TRIM(m.NAME_SUFFIX) AS Suffix,
                                            case TRIM(m.GENDER)
                                                 when 'M' then 'Male'
                                                 when 'F' then 'Female'
                                                 else 'Unknown'
                                            end AS Sex,
                                            TRIM(m.TAX_ID) AS SSN,
                                            CASE WHEN m.BIRTH_DATE<> -1 THEN CONVERT(datetime, CONVERT(varchar(8), m.BIRTH_DATE), 112) ELSE NULL END as BirthDate,
                                            CASE WHEN m.DEATH_DATE<> -1 THEN CONVERT(datetime, CONVERT(varchar(8), m.DEATH_DATE), 112) ELSE NULL END as DeathDate,
                                            CASE WHEN m.MEMBER_EFFECTIVE_DATE<> -1 THEN CONVERT(datetime, CONVERT(varchar(8), m.MEMBER_EFFECTIVE_DATE), 112) ELSE NULL END as StartDate,
                                            CASE WHEN m.MEMBER_TERMINATION_DATE<> -1 THEN CONVERT(datetime, CONVERT(varchar(8), m.MEMBER_TERMINATION_DATE), 112) ELSE NULL END as EndDate,
                                            TRIM('') AS RecordNumber,
                                            TRIM(medicare.IDENTIFICATION_NUMBER) AS MedicareNumber,
                                            TRIM(medicaid.IDENTIFICATION_NUMBER) AS MedicaidNumber,
                                            TRIM('') AS IncomingNumber,
                                            m.LAST_MODIFIED_BY AS UpdateDate
                                            {selectedPlaceHolder}
                                       from MEMBER_DIM m
                                        left join MEMBER_OTHER_ID_DETAILS medicaid
                                            on m.ACCOUNT_DIM_KEY = medicaid.MEMBER_DIM_KEY
                                            and medicaid.ID_TYPE = 'Medicaid Number'
                                        left Join MEMBER_OTHER_ID_DETAILS medicare
                                            on m.ACCOUNT_DIM_KEY = medicaid.MEMBER_DIM_KEY
                                            and medicaid.ID_TYPE = 'HCFA Plan ID'
                                            {joinPlaceHolder}
                                        WHERE m.MEMBER_DIM_KEY<>-1
                                        {wherePlaceHolder}";

            if (MemberNumber != null)
            {
                member += " AND RTRIM(MEMBER_HCC_ID) = @memberNumber";
            }


            return member;
        }

        internal string GetMemberPlusAddress(string memberNumber = null)
        {
            string selectAddress = @" ,d.Address_Dim_key                  AS ADDRESS_DIM_KEY                 
                                      ,CASE WHEN m.HOME_ADDRESS_DIM_KEY = d.ADDRESS_DIM_KEY THEN 'HOME'
                                           WHEN m.MAILING_ADDRESS_DIM_KEY = d.ADDRESS_DIM_KEY THEN 'MAILING'
                                       END AS AddressType
			                            ,TRIM(d.ADDRESS_LINE)            AS Address1
                                        ,TRIM(d.ADDRESS_LINE_2)          AS Address2
                                        ,TRIM(d.CITY)                    AS City
                                        ,TRIM(d.[STATE])                 AS[State]
			                            ,TRIM(d.ZIP_CODE)                AS ZipCode";

            string joinAddress = @"INNER JOIN ADDRESS_DIM d
	                               ON COALESCE(m.MAILING_ADDRESS_DIM_KEY,m.HOME_ADDRESS_DIM_KEY) = d.ADDRESS_DIM_KEY";

            string whereAddress = string.Empty;

            if (memberNumber != null)
            {
                whereAddress = @" AND RTRIM(m.MEMBER_HCC_ID) = @memberNumber";
            }

            return GetMemberSQL(selectAddress, joinAddress, whereAddress);
        }

        internal string GetSubcriberPlusAddress(string subcriberNumber = null)
        {
            string subcriber = $@"  SELECT m.MEMBER_DIM_KEY AS MEMBER_DIM_KEY
                                          ,m.ACCOUNT_DIM_Key AS ACCOUNT_DIM_KEY
                                          ,m.MAILING_ADDRESS_DIM_KEY
                                          ,m.HOME_ADDRESS_DIM_KEY
                                          ,m.ALT_PAYMENT_RECIPIENT_ADDRESS_DIM_KEY
                                          ,m.SUBSCRIBER_DIM_KEY
                                          ,TRIM(a.ACCOUNT_HCC_ID)                 AS AccountNumber
                                          ,TRIM(a.[NAME])                         AS AccountName
                                          ,TRIM(m.SUBSCRIPTION_HCC_ID)            AS ContractNumber
                                          ,TRIM(m.SUBSCRIBER_FIRST_NAME)          AS FirstName
	                                      ,TRIM(m.SUBSCRIBER_LAST_NAME)           AS LastName
	                                      ,CASE WHEN m.BIRTH_DATE <> -1 THEN CONVERT(datetime, CONVERT(varchar(8), m.BIRTH_DATE), 112) ELSE NULL END AS BirthDate
	                                      ,CASE WHEN m.HOME_ADDRESS_DIM_KEY = d.ADDRESS_DIM_KEY THEN 'HOME'
		                                        WHEN m.MAILING_ADDRESS_DIM_KEY= d.ADDRESS_DIM_KEY THEN 'MAILING'
	                                       END                                    AS AddressType
	                                      ,TRIM(d.ADDRESS_LINE)                   AS Address1 
	                                      ,TRIM(d.ADDRESS_LINE_2)                 AS Address2 
	                                      ,TRIM(d.CITY)                           AS City          
	                                      ,TRIM(d.[STATE])                        AS [State]   
	                                      ,TRIM(d.ZIP_CODE)                       AS ZipCode
                                          ,TRIM(d.COUNTY)                         AS County
                                          ,TRIM(d.COUNTRY)                        AS Country
	                                      ,TRIM(m.TELEPHONE_NUMBER)               AS PHONE
                                          ,TRIM(m.SUBSCRIBER_TAX_ID)              AS SSN
                                          ,CASE TRIM(m.GENDER)
                                                WHEN 'M' THEN 'Male'
                                                WHEN 'F' THEN 'Female'
                                                ELSE 'Unknown'
                                        END                                    AS Sex
                                        ,TRIM(m.SUBSCRIBER_FULL_NAME)           AS Name
                                    FROM MEMBER_DIM m
                                    INNER JOIN ADDRESS_DIM d   
                                    ON COALESCE(m.MAILING_ADDRESS_DIM_KEY,m.HOME_ADDRESS_DIM_KEY) = d.ADDRESS_DIM_KEY
                                    INNER JOIN ACCOUNT_DIM a
                                    ON m.ACCOUNT_DIM_KEY = a.ACCOUNT_DIM_KEY
                                    WHERE m.[STATUS] = 'Active'
                                    AND a.PARENT_ACCOUNT_NAMES IS NULL 
                                    AND a.[LEVEL] =0
                                    AND m.BIRTH_DATE <>-1";

            if (subcriberNumber != null)
            {
                subcriber += " AND m.SUBSCRIPTION_HCC_ID = @subcriberNumber";
            }

            return subcriber;
        }


        internal string GetAccountSQL(string accountNumber =null)
        {
            string account = $@"
                            SELECT   a.ACCOUNT_DIM_KEY         AS ACCOUNT_DIM_KEY
                                    ,a.ACCOUNT_ADDRESS_DIM_KEY AS ACCOUNT_ADDRESS_DIM_KEY  
                                    ,a.ACCOUNT_HCC_ID          AS AccountNumber 
                                    ,a.[NAME]                  AS AccountName
	                                ,b.ADDRESS_LINE            AS Address1
	                                ,b.ADDRESS_LINE_2          AS Address2
	                                ,b.CITY                    AS City
	                                ,b.[STATE]                 AS [State]
	                                ,b.ZIP_CODE                AS ZipCode
	                                ,c.TELEPHONE_NUMBER        AS Phone
                            FROM ACCOUNT_DIM a
                            INNER JOIN ADDRESS_DIM b
	                            ON a.ACCOUNT_ADDRESS_DIM_KEY = b.ADDRESS_DIM_KEY
                            LEFT JOIN ACCOUNT_CONTACT_DETAILS c
	                            ON a.ACCOUNT_DIM_KEY = c.ACCOUNT_DIM_KEY
                            WHERE [LEVEL] =0 
                            AND PARENT_ACCOUNT_NAMES IS NULL ";

            if (accountNumber != null)
            {
                account += " AND RTRIM(a.ACCOUNT_HCC_ID) = @accountNumber";
            }

            return account;
        }

        internal string GetSubAccountSQL(string subaccountNumber = null)
        {
            string subaccount = @"
                            /*
                               SELECT AccountNumber  LEFT '-' OF SUB-ACCOUNT
                               IF SUB-ACCOUNT FORMAT CHANGE NEEDS TO CHANGE THE SUB-QUERY

                               *** ADDRESS SELECTION CRITERIA ***

                                       IF IN ACCOUNT_DIM TABLE ACCOUNT_ADDRESS_DIM_KEY = -1  (SUB-ACCOUNT DO NOT HAVE ACCOUNT)
		                               THEN 
		                                  GET ADDRESS OF PARENT ACCOUNT
		                               ELSE 
		                                  GET THE ADDRESS OF SUB-ACCOUNT	  	    
                            */
                                SELECT   aa.ACCOUNT_DIM_KEY
                                        ,aa.ACCOUNT_ADDRESS_DIM_KEY
                                        ,aa.AccountNumber
                                        ,aa.SubAccountNumber
	                                    ,aa.[Name]
	                                    ,aa.Phone
	                                    ,b.ADDRESS_LINE                 AS Address1
	                                    ,b.ADDRESS_LINE_2               AS Address2
	                                    ,b.CITY                         AS City
	                                    ,b.[STATE]                      AS [State]
	                                    ,b.ZIP_CODE                     AS ZipCode 
                                FROM (
		                                SELECT LEFT(a.ACCOUNT_HCC_ID,CHARINDEX('-', a.ACCOUNT_HCC_ID)-1)  AS AccountNumber
				                                ,a.ACCOUNT_DIM_KEY                                         AS ACCOUNT_DIM_KEY  
                                                ,a.ACCOUNT_ADDRESS_DIM_KEY                                 AS ACCOUNT_ADDRESS_DIM_KEY
			                                    ,a.ACCOUNT_HCC_ID                                         AS SubAccountNumber
			                                    ,a.[NAME]                                                 AS [Name]
			                                    ,CASE
				                                    WHEN ACCOUNT_ADDRESS_DIM_KEY=-1 
				                                    THEN (SELECT ACCOUNT_ADDRESS_DIM_KEY FROM ACCOUNT_DIM WHERE ACCOUNT_HCC_ID =LEFT(a.ACCOUNT_HCC_ID,CHARINDEX('-', a.ACCOUNT_HCC_ID)-1))
				                                    ELSE ACCOUNT_ADDRESS_DIM_KEY
			                                    END                                                     AS ADDRESS_KEY
				                                ,c.TELEPHONE_NUMBER                                    AS Phone  
		                                FROM ACCOUNT_DIM a
		                                LEFT JOIN ACCOUNT_CONTACT_DETAILS c
			                                ON a.ACCOUNT_DIM_KEY = c.ACCOUNT_DIM_KEY
		                                WHERE a.ACCOUNT_HCC_ID LIKE '%-%'
                                ) aa
                                LEFT JOIN ADDRESS_DIM b
	                                ON aa.ADDRESS_KEY = b.ADDRESS_DIM_KEY";

            if(subaccountNumber != null)
            {
                subaccount += " WHERE aa.SubAccountNumber =@subAccountNumber";
            }

            return subaccount;
        }

    }
}

using DataLayer.Base;
using DataLayer.EnterpriseDataWarehouse.Models;
using DataLayer.EnterpriseDataWarehouse.SQLs;
using System.Collections.Generic;

namespace DataLayer.EnterpriseDataWarehouse
{
    public class AccountRepository : BaseRepository<EnterpriseDataWarehouseDatabase>
    {
        SQLQueries sqls = new SQLQueries();
        internal AccountRepository(EnterpriseDataWarehouseDatabase database) : base(database)
        {
        }

        /// <summary>
        /// Returns an enumerable collection of ALL ACCOUNT in the EDW system.
        /// </summary>
        /// <returns>Returned as an unbuffered results, means that the list is populated from database on demand. Calling `ToList()` will load the entire list from the database</returns>
        public IEnumerable<Account> GetAccounts()
        {
            string query = sqls.GetAccountSQL();
            return Database.Query<Account>(query, buffered: false);
        }

        /// <summary>
        /// Gets a single account from the account number
        /// </summary>
        /// <param name="accountNumber">The account number for the individual</param>
        /// <returns>A member object</returns>
        public Account GetAccount(string accountNumber)
        {
            string query = sqls.GetAccountSQL(accountNumber);
            return Database.QueryFirstOrDefault<Account>(query, new { accountNumber = accountNumber });
        }


        /// <summary>
        /// Returns an enumerable collection of ALL SubAccounts in the EDW system.
        /// </summary>
        /// <returns>Returned as an unbuffered results, means that the list is populated from database on demand. Calling `ToList()` will load the entire list from the database</returns>
        public IEnumerable<SubAccount> GetSubAccounts()
        {
            string query = sqls.GetSubAccountSQL();

            //string query = @"
            //                /*
            //                   SELECT AccountNumber  LEFT '-' OF SUB-ACCOUNT
            //                   IF SUB-ACCOUNT FORMAT CHANGE NEEDS TO CHANGE THE SUB-QUERY

            //                   *** ADDRESS SELECTION CRITERIA ***

            //                           IF IN ACCOUNT_DIM TABLE ACCOUNT_ADDRESS_DIM_KEY = -1  (SUB-ACCOUNT DO NOT HAVE ACCOUNT)
		          //                     THEN 
		          //                        GET ADDRESS OF PARENT ACCOUNT
		          //                     ELSE 
		          //                        GET THE ADDRESS OF SUB-ACCOUNT	  	    
            //                */

            //                SELECT aa.AccountNumber
            //                      ,aa.SubAccountNumber
	           //                   ,aa.[Name]
	           //                   ,aa.Phone
	           //                   ,b.ADDRESS_LINE                 AS Address1
	           //                   ,b.ADDRESS_LINE_2               AS Address2
	           //                   ,b.CITY_NAME                    AS City
	           //                   ,b.STATE_CODE                   AS [State]
	           //                   ,b.ZIP_CODE                     AS ZipCode 
            //                FROM (
		          //                  SELECT LEFT(a.ACCOUNT_HCC_ID,CHARINDEX('-', a.ACCOUNT_HCC_ID)-1)  AS AccountNumber
			         //                     ,a.ACCOUNT_HCC_ID                                           AS SubAccountNumber
			         //                     ,a.ACCOUNT_NAME                                             AS [Name]
			         //                     ,CASE
				        //                     WHEN ACCOUNT_ADDRESS_DIM_KEY=-1 
				        //                     THEN (SELECT ACCOUNT_ADDRESS_DIM_KEY FROM EDW.ACCOUNT_DIM WHERE ACCOUNT_HCC_ID =LEFT(a.ACCOUNT_HCC_ID,CHARINDEX('-', a.ACCOUNT_HCC_ID)-1))
				        //                     ELSE ACCOUNT_ADDRESS_DIM_KEY
			         //                      END                                                     AS ADDRESS_KEY
				        //                    ,c.TELEPHONE_NUMBER                                    AS Phone  
		          //                  FROM EDW.ACCOUNT_DIM a
		          //                  LEFT JOIN EDW.ACCOUNT_CONTACT_DETAILS c
			         //                   ON a.ACCOUNT_DIM_KEY = c.ACCOUNT_DIM_KEY
		          //                  WHERE a.ACCOUNT_HCC_ID LIKE '%-%'
            //                ) aa
            //                LEFT JOIN EDW.ADDRESS_DIM b
	           //                 ON aa.ADDRESS_KEY = b.ADDRESS_DIM_KEY";

            return Database.Query<SubAccount>(query, buffered: false);
        }


        /// <summary>
        /// GET SUBCRIBER
        /// </summary>
        /// <returns></returns>
        public SubAccount GetSubAccount(string subAccountNumber)
        {


            string query = sqls.GetSubAccountSQL(subAccountNumber);

            //string query = @"
            //                /*
            //                   SELECT AccountNumber  LEFT '-' OF SUB-ACCOUNT
            //                   IF SUB-ACCOUNT FORMAT CHANGE NEEDS TO CHANGE THE SUB-QUERY

            //                   *** ADDRESS SELECTION CRITERIA ***

            //                           IF IN ACCOUNT_DIM TABLE ACCOUNT_ADDRESS_DIM_KEY = -1  (SUB-ACCOUNT DO NOT HAVE ACCOUNT)
		          //                     THEN 
		          //                        GET ADDRESS OF PARENT ACCOUNT
		          //                     ELSE 
		          //                        GET THE ADDRESS OF SUB-ACCOUNT	  	    
            //                */

            //                SELECT aa.AccountNumber
            //                      ,aa.SubAccountNumber
	           //                   ,aa.[Name]
	           //                   ,aa.Phone
	           //                   ,b.ADDRESS_LINE                 AS Address1
	           //                   ,b.ADDRESS_LINE_2               AS Address2
	           //                   ,b.CITY_NAME                    AS City
	           //                   ,b.STATE_CODE                   AS [State]
	           //                   ,b.ZIP_CODE                     AS ZipCode 
            //                FROM (
		          //                  SELECT LEFT(ACCOUNT_HCC_ID,CHARINDEX('-', ACCOUNT_HCC_ID)-1)  AS AccountNumber
			         //                     ,ACCOUNT_HCC_ID                                         AS SubAccountNumber
			         //                     ,ACCOUNT_NAME                                           AS [Name]
			         //                     ,CASE
				        //                     WHEN ACCOUNT_ADDRESS_DIM_KEY=-1 
				        //                     THEN (SELECT ACCOUNT_ADDRESS_DIM_KEY FROM EDW.ACCOUNT_DIM WHERE ACCOUNT_HCC_ID =LEFT(a.ACCOUNT_HCC_ID,CHARINDEX('-', a.ACCOUNT_HCC_ID)-1))
				        //                     ELSE ACCOUNT_ADDRESS_DIM_KEY
			         //                      END                                                     AS ADDRESS_KEY
				        //                    ,c.TELEPHONE_NUMBER                                    AS Phone  
		          //                  FROM EDW.ACCOUNT_DIM a
		          //                  LEFT JOIN EDW.ACCOUNT_CONTACT_DETAILS c
			         //                   ON a.ACCOUNT_DIM_KEY = c.ACCOUNT_DIM_KEY
		          //                  WHERE a.ACCOUNT_HCC_ID LIKE '%-%'
            //                ) aa
            //                LEFT JOIN EDW.ADDRESS_DIM b
	           //                 ON aa.ADDRESS_KEY = b.ADDRESS_DIM_KEY
            //                WHERE aa.SubAccountNumber =@subAccountNumber";

            return Database.QueryFirstOrDefault<SubAccount>(query, new { subAccountNumber = subAccountNumber });
        }


    }
}

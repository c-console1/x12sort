using System;
using System.Collections.Generic;
using System.Text;
using DataLayer.Base;

namespace DataLayer.EnterpriseDataWarehouse
{
    public class EnterpriseDataWarehouseDatabase : BaseDatabase
    {
        /// <summary>
        /// Repository of queries for interacting with members in the billing system
        /// </summary>
        public MemberRepository Members { get; private set; }
        public AccountRepository Accounts { get; private set; }

        internal EnterpriseDataWarehouseDatabase(IDbExec dbExec) : base(dbExec)
        {
            Members = new MemberRepository(this);
            Accounts = new AccountRepository(this);

        }

        internal EnterpriseDataWarehouseDatabase(params IExecutor[] executors) : base(executors)
        {
            Members = new MemberRepository(this);
            Accounts = new AccountRepository(this);
        }
    }
}

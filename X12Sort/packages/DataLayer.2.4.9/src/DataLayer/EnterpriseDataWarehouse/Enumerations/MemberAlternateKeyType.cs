namespace DataLayer.EnterpriseDataWarehouse.Enumerations
{
    public enum MemberAlternateKeyType : int
    {
        AmisysNumber = 0,
        MedicaidID = 1,
        MedicareID = 2
    }
}

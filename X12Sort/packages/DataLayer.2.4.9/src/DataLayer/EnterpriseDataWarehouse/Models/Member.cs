using System;
using System.Collections.Generic;
using DataLayer.Base.Models;
using DataLayer.Base.Enumerations;
using DataLayer.EnterpriseDataWarehouse.Enumerations;

namespace DataLayer.EnterpriseDataWarehouse.Models
{
    public class Member : BaseEDWObject, IMember
    {
        #region Implement IMember
        public string MemberNumber { get; set; }
        public IEnumerable<string> Prefixes { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public IEnumerable<string> Suffixes { get; set; }
        public Sex Sex { get; set; }
        public DateTime? BirthDate { get; set; }
        public DateTime? DeathDate { get; set; }
        public string SSN { get; set; }
        public string Ethnicity { get; set; }
        public IEnumerable<Language> Languages { get; set; }
        #endregion

        #region MEMBER_DIM Primary and Foreign Keys
        /// <summary>
        /// Primary key of MEMBER_DIM table
        /// </summary>
        public string MEMBER_DIM_KEY { get; set; }

        /// <summary>
        /// Foreign key of Member_DIM table to ACCOUNT_DIM table
        /// </summary>
        public string ACCOUNT_DIM_KEY { get; set; }

        /// <summary>
        /// Foreign key of Member_DIM table to ADDRESS_DIM table
        /// </summary>
        public string ALT_PAYMENT_RECIPIENT_ADDRESS_DIM_KEY { get; set; }

        /// <summary>
        /// Foreign key of Member_DIM table to ADDRESS_DIM table
        /// </summary>
        public string MAILING_ADDRESS_DIM_KEY { get; set; }
        /// <summary>
        /// Foreign key of Member_DIM table to ADDRESS_DIM table
        /// </summary>
        public string HOME_ADDRESS_DIM_KEY { get; set; }
        /// <summary>
        /// Foreign key of Member_DIM table to SUBSCRIBER_DIM table
        /// </summary>
        public string SUBSCRIBER_DIM_KEY { get; set; }


        #endregion



        /// <summary>
        /// Member type
        /// </summary>
        public MemberType MemberType { get; set; }

        public IEnumerable<MemberAlternateKey> AlternateKeys { get; set; }

        /// <summary>
        /// Alternate names
        /// </summary>
        public IEnumerable<AlternateName> AlternateNames { get; set; }

        /// <summary>
        /// Current enrollement
        /// </summary>
        public Enrollment EnrollmentCurrent { get; set; }

        /// <summary>
        /// Enrollement history
        /// </summary>
        public IEnumerable<Enrollment> EnrollmentHistory { get; set; }

        /// <summary>
        /// Member claims
        /// </summary>
        public IEnumerable<IClaim> Claims { get; set; }

        /// <summary>
        /// Member numbers ordered newest to oldest
        /// </summary>
        public List<string> MemberNumbers { get; set; }
        

        /// <summary>
        /// Membership Start Date (EDW.MEMBER_DIM.MEMBER_EFFECTIVE_DATE)
        /// </summary>
        public DateTime? StartDate { get; set; }
        /// <summary>
        /// Membership End Date (EDW.MEMBER_DIM.MEMBER_TERMINATION_DATE)
        /// </summary>
        public DateTime? EndDate { get; set; }
    }
}

using System;
using DataLayer.Base.Models;
using DataLayer.EnterpriseDataWarehouse.Enumerations;

namespace DataLayer.EnterpriseDataWarehouse.Models
{
    public class MemberAlternateKey : BaseEDWObject, IStartEnd
    {
        #region Implements IStartEnd
        /// <summary>
        /// Start date
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// End date
        /// </summary>
        public DateTime? EndDate { get; set; }
        #endregion

        public MemberAlternateKeyType Type { get; set; }

        public string Value { get; set; }
    }
}

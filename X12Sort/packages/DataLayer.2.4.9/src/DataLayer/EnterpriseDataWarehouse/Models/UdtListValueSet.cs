using Newtonsoft.Json;

namespace DataLayer.EnterpriseDataWarehouse.Models
{
    public class UdtListValueSet
    {
        /// <summary>
        /// Attribute role / name
        /// </summary>
        [JsonProperty("attributeRoleName")]
        public string Name { get; set; }

        /// <summary>
        /// Attribute value
        /// </summary>
        [JsonProperty("attrValueAsString")]
        public string Value { get; set; }
    }
}

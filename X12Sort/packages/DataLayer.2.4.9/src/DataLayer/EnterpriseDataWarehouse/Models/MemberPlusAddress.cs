using DataLayer.Base.Enumerations;
using DataLayer.Base.Models;
using System;
using System.Collections.Generic;

namespace DataLayer.EnterpriseDataWarehouse.Models
{
    public class MemberPlusAddress : Member, IAddress, IStartEnd
    {
        /// <summary>
        /// MEMBER FULL NAME (EDW.MEMBER_DIM.MEMBER_FULL_NAME)
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Member Contract Number (EDW.MEMBER_DIM.SUBSCRIPTION_HCC_ID)
        /// </summary>
        public string ContractNumber { get; set; }

        /// <summary>
        /// Primary of Address_Dim table
        /// </summary>
        public string ADDRESS_DIM_KEY { get; set; }
        /// <summary>
        /// Member Address Type (Home OR Mailing)
        /// </summary>
        public string AddressType { get; set; }
        /// <summary>
        /// Member Address1 (EDW.ADDRESS_DIM.ADDRESS_LINE)
        /// BY FEDAULT MEMBER_MAILING_ADDRESS_KEY Otherwise MEMBER_HOME_ADDRESS_KEY
        /// of EDW.MEMBER_DIM table to join with EDW.ADDRESS_DIM
        /// </summary>
        public string Address1 { get; set; }
        /// <summary>
        /// Member Address2 (EDW.ADDRESS_DIM.ADDRESS_LINE_2)
        /// BY FEDAULT MEMBER_MAILING_ADDRESS_KEY Otherwise MEMBER_HOME_ADDRESS_KEY
        /// of EDW.MEMBER_DIM table to join with EDW.ADDRESS_DIM
        /// </summary>
        public string Address2 { get; set; }
        /// <summary>
        /// Member City (EDW.ADDRESS_DIM.CITY_NAME)
        /// BY FEDAULT MEMBER_MAILING_ADDRESS_KEY Otherwise MEMBER_HOME_ADDRESS_KEY
        /// of EDW.MEMBER_DIM table to join with EDW.ADDRESS_DIM
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// Member State (EDW.ADDRESS_DIM.STATE_CODE)
        /// BY FEDAULT MEMBER_MAILING_ADDRESS_KEY Otherwise MEMBER_HOME_ADDRESS_KEY
        /// of EDW.MEMBER_DIM table to join with EDW.ADDRESS_DIM
        /// </summary>
        public USState State { get; set; }
        /// <summary>
        /// Member Zip (EDW.ADDRESS_DIM.ZIP_CODE)
        /// BY FEDAULT MEMBER_MAILING_ADDRESS_KEY Otherwise MEMBER_HOME_ADDRESS_KEY
        /// of EDW.MEMBER_DIM table to join with EDW.ADDRESS_DIM
        /// </summary>
        public ZipCode ZipCode { get; set; }
        /// <summary>
        /// Member County (EDW.ADDRESS_DIM.COUNTY_NAME)
        /// BY FEDAULT MEMBER_MAILING_ADDRESS_KEY Otherwise MEMBER_HOME_ADDRESS_KEY
        /// of EDW.MEMBER_DIM table to join with EDW.ADDRESS_DIM
        /// </summary>
        public string County { get; set; }
        /// <summary>
        /// Member Country (EDW.ADDRESS_DIM.COUNTRY_NAME)
        /// BY FEDAULT MEMBER_MAILING_ADDRESS_KEY Otherwise MEMBER_HOME_ADDRESS_KEY
        /// of EDW.MEMBER_DIM table to join with EDW.ADDRESS_DIM
        /// </summary>
        public Country Country { get; set; }
        /// <summary>
        /// Member Phone Number (EDW.MEMBER_DIM.TELEPHONE_NUMBER)
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Member AltId (MEMBER_OTHER_ID_DETAILS.IDENTIFICATION_NUMBER)
        /// JOIN ON MEMBER_DIM_KEY AND ID_TYPE_NAME = 'Medicaid Number'
        /// </summary>
        public string AltId { get; set; }

    }
}

using System;
using System.Collections.Generic;
using System.Linq;

namespace DataLayer.EnterpriseDataWarehouse.Models
{
    public class BHPNode : BaseEDWObject
    {
        private static readonly int MIN_LEVEL = 1;
        private static readonly int MAX_LEVEL = 9;
        private List<string> _levels;

        public string FulPath { get { return ToString(); } set { SetPath(value); } }
        public string Level1 { get { return _levels[0]; } }
        public string Level2 { get { return _levels[1]; } }
        public string Level3 { get { return _levels[2]; } }
        public string Level4 { get { return _levels[3]; } }
        public string Level5 { get { return _levels[4]; } }
        public string Level6 { get { return _levels[5]; } }
        public string Level7 { get { return _levels[6]; } }
        public string Level8 { get { return _levels[7]; } }
        public string Level9 { get { return _levels[8]; } }
        public int Depth { get { return _levels.Count; } }
        public string Leaf { get { return _levels.Last(); } }

        public BHPNode()
        {
            _levels = new List<string>();
        }

        public BHPNode(string fullPath)
        {
            SetPath(fullPath);
        }

        public BHPNode(List<string> levels)
        {
            SetPath(levels);
        }

        public void SetPath(string fullPath)
        {
            SetPath(fullPath.Split('.').ToList());
        }

        public void SetPath(List<string> levels)
        {
            if (levels == null) throw new ArgumentNullException("levels");
            if (levels.Count < MIN_LEVEL) throw new ArgumentOutOfRangeException("levels", levels.Count, $"Argument must have at least {MIN_LEVEL} level(s)");
            if (levels.Count > MAX_LEVEL) throw new ArgumentOutOfRangeException("levels", levels.Count, $"Argument must not have more than {MAX_LEVEL} level(s)");

            _levels = levels;
        }

        public override string ToString()
        {
            return string.Join(".", _levels);
        }
    }
}

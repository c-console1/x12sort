using DataLayer.Base.Enumerations;
using DataLayer.Base.Models;
using System;

namespace DataLayer.EnterpriseDataWarehouse.Models
{
    public class Subcriber:Address
    {

        #region MEMBER_DIM Primary and Foreign Keys
        public string MEMBER_DIM_KEY { get; set; }

        /// <summary>
        /// Foreign key of Member_DIM table to ACCOUNT_DIM table
        /// </summary>
        public string ACCOUNT_DIM_KEY { get; set; }

        /// <summary>
        /// Foreign key of Member_DIM table to ADDRESS_DIM table
        /// </summary>
        public string ALT_PAYMENT_RECIPIENT_ADDRESS_DIM_KEY { get; set; }

        /// <summary>
        /// Foreign key of Member_DIM table to ADDRESS_DIM table
        /// </summary>
        public string MAILING_ADDRESS_DIM_KEY { get; set; }
        /// <summary>
        /// Foreign key of Member_DIM table to ADDRESS_DIM table
        /// </summary>
        public string HOME_ADDRESS_DIM_KEY { get; set; }
        /// <summary>
        /// Foreign key of Member_DIM table to SUBSCRIBER_DIM table
        /// </summary>
        public string SUBSCRIBER_DIM_KEY { get; set; }

        #endregion



        /// <summary>
        /// Subcriber Contract Number (EDW.MEMBER_DIM.SUBSCRIPTION_HCC_ID)
        /// </summary>
        public string ContractNumber { get; set; }
        /// <summary>
        /// Subcriber First Name (EDW.MEMBER_DIM.SUBSCRIBER_FIRST_NAME)
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Subcriber Last Name (EDW.MEMBER_DIM.SUBSCRIBER_LAST_NAME)
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Subcriber Birthdate (EDW.MEMBER_DIM.MEMBER_BIRTH_DATE)
        /// </summary>
        public DateTime? BirthDate { get; set; }
        /// <summary>
        /// Subcriber Account Number (EDW.ACCOUNT_DIM.ACCOUNT_HCC_ID)
        /// </summary>
        public string AccountNumber { get; set; }
        /// <summary>
        /// Subcriber Account Name (EDW.ACCOUNT_DIM.ACCOUNT_NAME)
        /// </summary>
        public string AccountName { get; set; }
        /// <summary>
        /// Subcriber Phone Number (EDW.MEMBER_DIM.TELEPHONE_NUMBER)
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// Subcriber SSN (EDW.MEMBER_DIM.SUBSCRIBER_TAX_ID)
        /// </summary>
        public string SSN { get; set; }
        /// <summary>
        /// Subcriber Gender (EDW.MEMBER_DIM.MEMBER_GENDER_NAME)
        /// </summary>
        public Sex Sex { get; set; }
        /// <summary>
        /// Subcriber Full Name (EDW.MEMBER_DIM.SUBSCRIBER_FULL_NAME)
        /// </summary>
               
    }
}

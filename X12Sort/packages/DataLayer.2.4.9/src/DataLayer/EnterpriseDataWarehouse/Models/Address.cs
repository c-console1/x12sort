using System.Collections.Generic;
using System.Linq;
using DataLayer.Base.Models;
using DataLayer.EnterpriseDataWarehouse.Enumerations;

namespace DataLayer.EnterpriseDataWarehouse.Models
{
    public class Address : BaseEDWObject, IAddress
    {
        #region Implements IAddress
        /// <summary>
        /// The human name of the address, ex: Toledo Hospital
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The first line of the address, ex: 2142 N Cove Blvd (EDW.ADDRESS_DIM.ADDRESS_LINE)
        /// BY FEDAULT MEMBER_HOME_ADDRESS_KEY Otherwise MEMBER_MAILING_ADDRESS_KEY
        /// </summary>
        public string Address1 { get; set; }

        /// <summary>
        /// The second line of the address (EDW.ADDRESS_DIM.ADDRESS_LINE_2)
        /// BY FEDAULT MEMBER_HOME_ADDRESS_KEY Otherwise MEMBER_MAILING_ADDRESS_KEY
        /// </summary>
        public string Address2 { get; set; }

        /// <summary>
        /// The city of the address, ex: Toledo (EDW.ADDRESS_DIM.CITY_NAME)
        /// BY FEDAULT MEMBER_MAILING_ADDRESS_KEY Otherwise MEMBER_HOME_ADDRESS_KEY 
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// The State object of the address, ex: OH (EDW.ADDRESS_DIM.STATE)
        /// BY FEDAULT MEMBER_MAILING_ADDRESS_KEY Otherwise MEMBER_HOME_ADDRESS_KEY
        /// </summary>
        public USState State { get; set; } = USState.Unknown;

        /// <summary>
        /// The county of the address, Lucas ((EDW.ADDRESS_DIM.COUNTY_NAME))
        /// BY FEDAULT MEMBER_MAILING_ADDRESS_KEY Otherwise MEMBER_HOME_ADDRESS_KEY
        /// </summary>
        public string County { get; set; }

        /// <summary>
        /// The country of the address, ex: USA (EDW.ADDRESS_DIM.COUNTRY_NAME)
        /// BY FEDAULT MEMBER_MAILING_ADDRESS_KEY Otherwise MEMBER_HOME_ADDRESS_KEY
        /// </summary>
        public Country Country { get; set; } = Country.UnitedStates;

        /// <summary>
        /// the zip/postal code of the address, ex: 43606 (EDW.ADDRESS_DIM.ZIP_CODE)
        /// BY FEDAULT MEMBER_MAILING_ADDRESS_KEY Otherwise MEMBER_HOME_ADDRESS_KEY
        /// </summary>
        public ZipCode ZipCode { get; set; } = ZipCode.Unknown;
        #endregion

        #region MEMBER_DIM Primary and Foreign Keys
        /// <summary>
        /// Primary key of ADDRESS_DIM table
        /// </summary>
        public string ADDRESS_DIM_KEY { get; set; }
        #endregion

        /// <summary>
        /// List of phone numbers associated with address
        /// </summary>
        public IEnumerable<PhoneNumber> PhoneNumbers { get; set; }

        /// <summary>
        /// List of fax numbers associated with address
        /// </summary>
        public IEnumerable<PhoneNumber> FaxNumbers { get; set; }

        /// <summary>
        /// List of email addresses associated with the address
        /// </summary>
        public IEnumerable<string> EmailAddresses { get; set; }

        /// <summary>
        /// Determines whether this address has any phone numbers
        /// </summary>
        public bool HasPhoneNumber { get { return PhoneNumbers == default ? false : PhoneNumbers.Count() > 0; } }

        /// <summary>
        /// Determines whether this address has any fax numbers
        /// </summary>
        public bool HasFaxNumber { get { return FaxNumbers == default ? false : FaxNumbers.Count() > 0; } }

        /// <summary>
        /// Determines whether this address has any email addresses
        /// </summary>
        public bool HasEmailAddress { get { return EmailAddresses == default ? false : EmailAddresses.Count() > 0; } }

        /// <summary>
        /// Address type
        /// </summary>
        public AddressType AddressType { get; set; } = AddressType.Unknown;

        /// <summary>
        /// The third line of the address
        /// </summary>
        public string Address3 { get; set; }

        /// <summary>
        /// Basic flag to indicate if something is active/void/obsolete/etc, defaults: true
        /// </summary>
        public bool IsActive { get; set; } = true;
    }
}

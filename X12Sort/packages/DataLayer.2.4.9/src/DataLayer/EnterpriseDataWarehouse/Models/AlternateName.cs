using DataLayer.EnterpriseDataWarehouse.Enumerations;

namespace DataLayer.EnterpriseDataWarehouse.Models
{
    public class AlternateName : BaseEDWObject
    {
        public AlternateNameType Type { get; set; }

        public string Name { get; set; }
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using DataLayer.Base.Models;

namespace DataLayer.EnterpriseDataWarehouse.Models
{
    public class Account : Address
    {
        #region MEMBER_DIM Primary and Foreign Keys
        /// <summary>
        /// Primary key of ACCOUNT_DIM table
        /// </summary>
        public string ACCOUNT_DIM_KEY { get; set; }

        /// <summary>
        /// Foreign key of ACCOUNT_DIM table to ADDRESS_DIM table
        /// </summary>
        public string ACCOUNT_ADDRESS_DIM_KEY { get; set; }
        #endregion

        /// <summary>
        /// Account Number (ACCOUNT_DIM.ACCOUNT_HCC_ID)
        /// </summary>
        public string AccountNumber { get; set; }
        /// <summary>
        /// Account name (ACCOUNT_DIM.ACCOUNT_NAME)
        /// </summary>
        public string AccountName { get; set; }

        /// <summary>
        /// Account phone (ACCOUNT_CONTACT_DETAILS.TELEPHONE_NUMBER)
        /// </summary>
        public string Phone { get; set; }

    }
}

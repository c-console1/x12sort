using System;
using System.Collections.Generic;
using DataLayer.Base.Models;

namespace DataLayer.EnterpriseDataWarehouse.Models
{
    public class Contact : BaseEDWObject, IPersonName, IStartEnd
    {
        #region Implements IPersonName
        /// <summary>
        /// Name prefixes, ex. 'Dr.'
        /// </summary>
        public IEnumerable<string> Prefixes { get; set; }

        /// <summary>
        /// Really??? You have to ask...
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Really??? You have to ask...
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Really??? You have to ask...
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Name sufixes, ex. 'Jr.'
        /// </summary>
        public IEnumerable<string> Suffixes { get; set; }
        #endregion

        #region Implements IStartEnd
        /// <summary>
        /// Wow, The Start Date!
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Wow, The End Date!
        /// </summary>
        public DateTime? EndDate { get; set; }
        #endregion

        /// <summary>
        /// Phone numbers
        /// </summary>
        public IEnumerable<PhoneNumber> PhoneNumbers { get; set; } = default;

        /// <summary>
        /// Addresses
        /// </summary>
        public IEnumerable<Address> Addresses { get; set; } = default;
    }
}

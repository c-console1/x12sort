using DataLayer.Base.Models;
using DataLayer.EnterpriseDataWarehouse.Enumerations;

namespace DataLayer.EnterpriseDataWarehouse.Models
{
    public class EDWPhoneNumber : BaseEDWObject
    {
        /// <summary>
        /// Phone # type
        /// </summary>
        public EDWPhoneNumberType PhoneType { get; set; }

        /// <summary>
        /// Phone #
        /// </summary>
        public PhoneNumber PhoneNBR { get; set; }




    }
}

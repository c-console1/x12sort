using System;
using System.Collections.Generic;
using System.Text;
using DataLayer.Base.Enumerations;
using DataLayer.Base.Models;

namespace DataLayer.EnterpriseDataWarehouse.Models
{
    public class BaseEDWObject : BaseDataObject, IAdditionalValues
    {
        #region Impliment IAdditionalValues
        /// <summary>
        /// General notes
        /// </summary>
        public string Notes { get; set; }

        /// <summary>
        /// User defined fields
        /// </summary>
        public IEnumerable<UdtListValueSet> UDFs { get; set; }

        /// <summary>
        /// Code types
        /// </summary>
        public IEnumerable<TypeCode> TypeCodes { get; set; }
        #endregion

        public override DataSource Source { get => DataSource.EDW; }
    }
}

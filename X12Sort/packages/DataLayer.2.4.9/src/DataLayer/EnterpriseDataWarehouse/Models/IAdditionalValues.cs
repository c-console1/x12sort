using System.Collections.Generic;

namespace DataLayer.EnterpriseDataWarehouse.Models
{
    interface IAdditionalValues
    {
        /// <summary>
        /// General notes
        /// </summary>
        string Notes { get; set; }

        /// <summary>
        /// User defined fields (or user defined terms)
        /// </summary>
        IEnumerable<UdtListValueSet> UDFs { get; set; }

        /// <summary>
        /// Type codes
        /// </summary>
        IEnumerable<TypeCode> TypeCodes { get; set; }

    }
}

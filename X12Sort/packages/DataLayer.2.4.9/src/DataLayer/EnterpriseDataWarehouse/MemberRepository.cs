using DataLayer.Base;
using DataLayer.EnterpriseDataWarehouse.Models;
using DataLayer.EnterpriseDataWarehouse.SQLs;
using System.Collections.Generic;

namespace DataLayer.EnterpriseDataWarehouse
{
    public class MemberRepository : BaseRepository<EnterpriseDataWarehouseDatabase>
    {
        SQLQueries sqls = new SQLQueries();
        internal MemberRepository(EnterpriseDataWarehouseDatabase database) : base(database)
        {
        }

        public IEnumerable<dynamic> GetMembers(bool address = false)
        {
            IEnumerable<dynamic> result = null;
            if (address == false)
            {
                result = Members();
            }
            if (address == true)
            {
                result = Members(address);
            }

            return result;
        }


        public IEnumerable<dynamic> GetMember(string memberNumber, bool address = false)
        {
            IEnumerable<dynamic> result = null;
            if (address == false)
            {
                result = Member(memberNumber);
            }
            if (address == true)
            {
                result = Member(memberNumber, address: true);
            }

            return result;
        }




        /// <summary>
        /// Returns an enumerable collection of ALL MEMBER in the EDW system.
        /// </summary>
        /// <returns>Returned as an unbuffered results, means that the list is populated from database on demand. Calling `ToList()` will load the entire list from the database</returns>
        private IEnumerable<Member> Members()
        {
            string query = sqls.GetMemberSQL();
            return Database.Query<Member>(query, buffered: false);
        }


        /// <summary>
        /// Returns an enumerable collection of ALL MEMBER in the EDW system.
        /// </summary>
        /// <returns>Returned as an unbuffered results, means that the list is populated from database on demand. Calling `ToList()` will load the entire list from the database</returns>
        private IEnumerable<MemberPlusAddress> Members(bool Address)
        {
            string query = sqls.GetMemberPlusAddress();
            return Database.Query<MemberPlusAddress>(query, buffered: false);
        }

        /// <summary>
        /// Gets a single member from the member number
        /// </summary>
        /// <param name="memberNumber">The member number for the individual</param>
        /// <returns>A member object</returns>
        private IEnumerable<Member> Member(string memberNumber)
        {
            string query = sqls.GetMemberSQL() + " AND RTRIM(MEMBER_HCC_ID) = @memberNumber";
            return Database.Query<Member>(query, new { memberNumber = memberNumber });
        }

        /// <summary>
        /// Gets a single member with address from the member number
        /// Look for mailing address if there is no mailing address will get the home address
        /// </summary>
        /// <param name="Address">Boolen value if need member address</param>
        /// <param name="memberNumber">The member number for the individual</param>
        /// <returns></returns>
        private IEnumerable<MemberPlusAddress> Member(string memberNumber, bool address)
        {
            string query = sqls.GetMemberPlusAddress(memberNumber);
            return Database.Query<MemberPlusAddress>(query, new { memberNumber = memberNumber });
        }


        /// <summary>
        /// GET SUBCRIBERS
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Subcriber> GetSubcribers()
        {

            string query = sqls.GetSubcriberPlusAddress();
            return Database.Query<Subcriber>(query, buffered: false);
        }

        /// <summary>
        /// GET SUBCRIBER
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Subcriber> GetSubcriber(string subcriberNumber)
        {

            string query = sqls.GetSubcriberPlusAddress();
            return Database.Query<Subcriber>(query, new { subcriberNumber = subcriberNumber });
        }

    }
}

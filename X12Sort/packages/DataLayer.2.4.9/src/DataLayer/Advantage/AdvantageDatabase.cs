using DataLayer.Base;

namespace DataLayer.Advantage
{
    /// <summary>
    /// Logical Database for the Advantage supplemental data
    /// </summary>
    public class AdvantageDatabase : BaseDatabase
    {
        public EVVRepository EVV { get; private set; }
        public MCPNRepository MCPN { get; private set; }
        public CCRRepository CCR { get; private set; }

        internal AdvantageDatabase(IDbExec dbExec) : base(dbExec)
        {
            EVV = new EVVRepository(this);
            MCPN = new MCPNRepository(this);
            CCR = new CCRRepository(this);
        }

        internal AdvantageDatabase(params IExecutor[] executors) : base(executors)
        {
            EVV = new EVVRepository(this);
            MCPN = new MCPNRepository(this);
            CCR = new CCRRepository(this);
        }
    }
}

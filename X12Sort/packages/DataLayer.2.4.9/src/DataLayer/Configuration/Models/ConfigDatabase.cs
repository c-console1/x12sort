using System;

namespace DataLayer.Configuration.Models
{
    /// <summary>
    /// Database Configuration Object This class links a connection string to a MachineApplication. Links to the logical databases in the DataAccess Object
    /// </summary>
    /// <remarks>
    /// <para>
    /// This item is internal to the is probably not what you want unless you are working with an application's data layer configuration
    /// </para>
    /// </remarks>
    public class ConfigDatabase : ConfigBaseItem, ILogicalDatabase
    {
        private ConfigConnectionString _connection;

        /// <summary>
        /// Internal Unique ID for the Database
        /// </summary>
        public int DatabaseID { get; internal set; }

        /// <summary>
        /// Internal Unique ID for the MachineApplication that this object belongs to
        /// </summary>
        public int MachineApplicationID { get; internal set; }

        /// <summary>
        /// Internal Unique ID for the ConnectionStringID that this object references
        /// </summary>
        public Guid ConnectionID { get; set; }

        /// <summary>
        /// Connection object to use as the connection for the logical database
        /// </summary>
        ILogicalConnection ILogicalDatabase.Connection => _connection;

        /// <summary>
        /// Connection object to use as the connection for the logical database
        /// </summary>
        public ConfigConnectionString Connection
        {
            get { return _connection; }
            set
            {
                if (value == null)
                {
                    _connection = null;
                    ConnectionID = Guid.Empty;
                }
                else
                {
                    _connection = value;
                    ConnectionID = value.ConnectionID;
                }
            }
        }

        /// <summary>
        /// Database name, should match what is in the DataAccess object
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Standard Constructor
        /// </summary>
        public ConfigDatabase()
        {
            _connection = null;
            DatabaseID = -1;
            MachineApplicationID = -1;
            ConnectionID = Guid.Empty;
        }

        /// <summary>
        /// Convert this object to an anonymous object for using as a parameter in query
        /// </summary>
        /// <returns> New object that can be used to save this object </returns>
        public override object ToParameterObject()
        {
            return new
            {
                MachineApplicationID,
                ConnectionStringID = ConnectionID,
                Name
            };
        }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString()
        {
            string cxn = Connection?.ToString() ?? "(Unknown Connection Information!)";
            return $"{Name} = {cxn}";
        }
    }
}

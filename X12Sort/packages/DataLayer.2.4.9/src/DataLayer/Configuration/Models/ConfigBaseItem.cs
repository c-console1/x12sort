using System;

namespace DataLayer.Configuration.Models
{
    /// <summary>
    /// Collection of field common to all Config tables
    /// </summary>
    /// <remarks>
    /// <para>
    /// This item is internal to the is probably not what you want unless you are working with an application's data layer configuration
    /// </para>
    /// </remarks>
    public abstract class ConfigBaseItem
    {
        /// <summary>
        /// The DateTime the object was created on from database
        /// </summary>
        public virtual DateTime CreatedOn { get; set; }

        /// <summary>
        /// The database system user who created the records
        /// </summary>
        public virtual string CreatedBy { get; set; }

        /// <summary>
        /// The DateTime the records was updated on from Database
        /// </summary>
        public virtual DateTime UpdatedOn { get; set; }

        /// <summary>
        /// The database system user who updated the records
        /// </summary>
        public virtual string UpdatedBy { get; set; }

        /// <summary>
        /// Convert this object to an anonymous object for using as a parameter in query
        /// </summary>
        public abstract object ToParameterObject();
    }
}

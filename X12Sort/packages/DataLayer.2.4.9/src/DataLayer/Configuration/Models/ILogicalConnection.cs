using System;

namespace DataLayer.Configuration.Models
{
    /// <summary>
    /// Logical connection interface for databases, APIs, etc.
    /// </summary>
    /// <remarks>
    /// <para>
    /// This interface is used to represent a connection of any type within the  DataLayer
    /// </para>
    /// <para>
    /// This item is internal to the is probably not what you want unless you are working with an application's data layer configuration
    /// </para>
    /// </remarks>
    /// <seealso cref="ConfigConnectionString"/>
    /// <seealso cref="ConfigApiConnection"/>
    public interface ILogicalConnection
    {
        /// <summary>
        /// Internal Unique ID for the ConnectionStringID
        /// </summary>
        Guid ConnectionID { get; }

        /// <summary>
        /// The logical database that this connection string belongs
        /// </summary>
        string DatabaseGroup { get; }

        /// <summary>
        /// Is this connection string the default for the logical database
        /// </summary>
        bool IsDefault { get; }

        /// <summary>
        /// Is this connection string referring to a deprecated system or login
        /// </summary>
        bool IsDeprecated { get; }

        /// <summary>
        /// Human friendly name to refer to this connection string
        /// </summary>
        string Name { get; }
    }
}

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Configuration.Models
{
    /// <summary>
    /// API connection configuration used by the DataLayer
    /// </summary>
    /// <remarks>
    /// This class should parallel the connection string configuration class, both items should work the same
    /// <para>
    /// This item is internal to the is probably not what you want unless you are working with an application's data layer configuration
    /// </para>
    /// </remarks>
    /// <seealso cref="ConfigConnectionString"/>
    /// <seealso cref="ILogicalConnection"/>
    public class ConfigApiConnection : ConfigBaseItem, ILogicalConnection, ICrudFunc
    {
        private Dictionary<string, string> _headers;

        /// <summary>
        /// public constructor
        /// </summary>
        public ConfigApiConnection() : base()
        {
            _headers = new Dictionary<string, string>();
        }

        /// <summary>
        /// Internal Unique ID for the ConnectionID
        /// </summary>
        public Guid ConnectionID { get; set; }

        /// <summary>
        /// Internal Unique ID for the ApiConnectionID
        /// </summary>
        [Obsolete("Use `ConnectionID` instead")]
        internal Guid ApiConnectionID
        {
            get { return ConnectionID; }
            set { ConnectionID = value; }
        }

        /// <summary>
        /// The logical database that this connection string belongs
        /// </summary>
        public string DatabaseGroup { get; set; }

        /// <summary>
        /// Is this connection string the default for the logical database
        /// </summary>
        public bool IsDefault { get; set; }

        /// <summary>
        /// Human friendly name to refer to this connection string
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Type of authentication required
        /// </summary>
        public ApiAuthenticationType Authentication { get; set; }

        /// <summary>
        /// The base URL of the service
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// The user to connect as
        /// </summary>
        public string UserID { get; set; }

        /// <summary>
        /// Is this connection string referring to a deprecated system or login
        /// </summary>
        public bool IsDeprecated { get; set; }

        /// <summary>
        /// Headers serialized into a JSON string
        /// </summary>
        /// <seealso cref="Headers"/>
        public string SerializedHeaders
        {
            get
            {
                return JsonConvert.SerializeObject(_headers);
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    _headers.Clear();
                }
                else
                {
                    _headers = JsonConvert.DeserializeObject<Dictionary<string, string>>(value);
                }
            }
        }

        /// <summary>
        /// The dictionary of Headers to use on all API requests by default
        /// </summary>
        /// <seealso cref="SerializedHeaders"/>
        public Dictionary<string, string> Headers
        {
            get { return _headers; }
            set { _headers = value; }
        }

        /// <summary>
        /// Creates a hard clone of this variable
        /// </summary>
        /// <returns> A clone of this object, still link to machine application, but id is cleared </returns>
        public override object ToParameterObject()
        {
            return new
            {
                DatabaseGroup,
                Name,
                Authentication = Authentication.ToString(),
                Url,
                UserID,
                SerializedHeaders
            };
        }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString()
        {
            string cxn = Url ?? "(null)";
            string security = Authentication.ToString();
            if (Authentication == ApiAuthenticationType.Basic)
            {
                security += $@"\User='{UserID}' ";
            }
            return $"{Name}, Connection: {cxn}, Security: {security}";
        }

        public string CreateQuery()
        {
            return @"
INSERT INTO [ConfigApiConnections]
    ([DatabaseGroup],[Name],[Authentication],[Url],[UserID],[SerializedHeaders])
VALUES
    (@DatabaseGroup,@Name,@Authentication,@Url,@UserID,@SerializedHeaders)
";
        }

        public string ReadQuery()
        {
            throw new NotImplementedException();
        }

        public string UpdateQuery()
        {
            throw new NotImplementedException();
        }

        public string DeleteQuery()
        {
            throw new NotImplementedException();
        }
    }
}

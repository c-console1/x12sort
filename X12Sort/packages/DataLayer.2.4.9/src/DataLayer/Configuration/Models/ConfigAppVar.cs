using System;

namespace DataLayer.Configuration.Models
{
    /// <summary>
    /// The Application Variable Object
    /// </summary>
    /// <remarks>
    /// <para>
    /// This item is internal to the is probably not what you want unless you are working with an application's data layer configuration
    /// </para>
    /// </remarks>
    public class ConfigAppVar : ConfigBaseItem
    {
        /// <summary>
        /// Standard Constructor
        /// </summary>
        public ConfigAppVar()
        {
            AppVarID = -1;
            MachineApplicationID = -1;
        }

        /// <summary>
        /// Internal Unique ID for the Variable
        /// </summary>
        public int AppVarID { get; internal set; }

        /// <summary>
        /// Internal Unique ID for the MachineApplication that this object belongs to
        /// </summary>
        public int MachineApplicationID { get; internal set; }

        /// <summary>
        /// The name of the variable, only A-z0-9_
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The Value of the Variable, stored as a string
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// A human description of what this variable is for
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString()
        {
            return $"{Name} = '{Value}' [{Description}]";
        }

        /// <summary>
        /// Does the object have IDs from the database
        /// </summary>
        /// <returns> True if the internal id are valid </returns>
        public bool IsLoaded()
        {
            return (AppVarID > -1) && (MachineApplicationID > -1);
        }

        /// <summary>
        /// Convert this object to an anonymous object for using as a parameter in query
        /// </summary>
        /// <returns> New object that can be used to save this object </returns>
        public override object ToParameterObject()
        {
            return new
            {
                MachineApplicationID,
                Name,
                Value,
                Description,
                AppVarID
            };
        }

        /// <summary>
        /// Creates a hard clone of this variable
        /// </summary>
        /// <returns> A clone of this object, still link to machine application, but id is cleared </returns>
        public ConfigAppVar Clone()
        {
            return new ConfigAppVar()
            {
                Name = Name,
                Description = Description,
                Value = Value,
                MachineApplicationID = MachineApplicationID,
                CreatedBy = Environment.UserName,
                CreatedOn = DateTime.Now,
                UpdatedBy = Environment.UserName,
                UpdatedOn = DateTime.Now,
            };
        }
    }
}

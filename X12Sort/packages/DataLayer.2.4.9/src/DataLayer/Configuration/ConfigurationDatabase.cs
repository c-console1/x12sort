using Dapper;
using DataLayer.Base;
using DataLayer.Configuration.Models;
using DataLayer.StaticUtilities;
using DataLayer.StaticUtilities.Extensions;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace DataLayer.Configuration
{
    /// <summary>
    /// Logical Database for the Application Configuration Database
    /// </summary>
    public class ConfigurationDatabase : BaseDatabase
    {
        private ConfigMachineApplication _currentConfig;

        private MachineRepository _machineRepository;
        private ApplicationRepository _applicationRepository;

        /// <summary>
        /// Current Loaded Configuration
        /// </summary>
        public ConfigMachineApplication Current { get { return _currentConfig; } }

        /// <summary>
        /// Repository for machine configuration
        /// </summary>
        public MachineRepository Machine { get { return _machineRepository; } }
        /// <summary>
        /// Repository for application configuration
        /// </summary>
        public ApplicationRepository Application { get { return _applicationRepository; } }

        internal ConfigurationDatabase(IDbExec dbExec) : base(dbExec)
        {
            _machineRepository = new MachineRepository(this);
            _applicationRepository = new ApplicationRepository(this);
        }

        internal ConfigurationDatabase(params IExecutor[] executors) : base(executors)
        {
            _machineRepository = new MachineRepository(this);
            _applicationRepository = new ApplicationRepository(this);
        }

        /// <summary>
        /// Gets a configuration from the database
        /// </summary>
        /// <param name="machineName"> Unique Machine Name </param>
        /// <param name="appName">     Application Name </param>
        /// <param name="useFallback"> Use the default machine if given machine not found </param>
        public ConfigMachineApplication GetConfiguration(string machineName, string appName, bool useFallback = true)
        {
            machineName.ThrowIfNullOrEmpty(nameof(machineName));
            appName.ThrowIfNullOrEmpty(nameof(appName));

            Logger.Info($"Loading Configuration: {machineName}/{appName}, using fallback machine: {useFallback}");

            object param = new { MachineName = machineName, ApplicationName = appName, UseFallBack = useFallback };
            var results = DbExec.QueryMultiple("EXECUTE dbo.sp_GetConfiguration_v2 @MachineName, @ApplicationName, @UseFallBack", param);
            ConfigMachine machine = results.ReadSingleOrDefault<ConfigMachine>();
            if (machine == null)
            {
                Logger.Warn($"Machine Not Found: {machine}");
                throw new InvalidOperationException($"Machine Not Found: {machine}");
            }
            Logger.Debug($" - Successfully Loaded Machine: {machine}");

            ConfigMachineApplication machApp = results.ReadSingleOrDefault<ConfigMachineApplication>();
            if (machApp == null)
            {
                Logger.Warn($"Application Machine Pair Not Found: ('{machineName}','{appName}')");
                throw new InvalidOperationException($"Application Machine Pair Not Found: ('{machineName}','{appName}')");
            }
            Logger.Debug($" - Successfully Loaded Application: {machApp}");


            machApp.Machine = machine;
            machApp.Variables = results.Read<ConfigAppVar>().ToList();
            Logger.Debug($" - Successfully Loaded Variables: {machApp.Variables.Count()}");
            machApp.Databases = results.Read<ConfigDatabase, ConfigConnectionString, ConfigDatabase>((d, c) => { d.Connection = c; return d; }, splitOn: "ConnectionStringID").ToList();
            machApp.ApiDatabases = results.Read<ConfigApiDatabase, ConfigApiConnection, ConfigApiDatabase>((d, c) => { d.Connection = c; return d; }, splitOn: "ApiConnectionID").ToList();
            Logger.Debug($" - Successfully Loaded Databases: {machApp.Databases.Count()}");
            Logger.Debug($" - Done With Configuration Load!");

            return machApp;
        }

        /// <summary>
        /// Gets a configuration from the database
        /// </summary>
        /// <param name="machineName"> Unique Machine Name </param>
        /// <param name="appName">     Application Name </param>
        /// <param name="useFallback"> Use the default machine if given machine not found </param>
        public async Task<ConfigMachineApplication> GetConfigurationAsync(string machineName, string appName, bool useFallback = true)
        {
            machineName.ThrowIfNullOrEmpty(nameof(machineName));
            appName.ThrowIfNullOrEmpty(nameof(appName));

            Logger.Info($"Loading Configuration: {machineName}/{appName}, using fallback machine: {useFallback}");

            object param = new { MachineName = machineName, ApplicationName = appName, UseFallBack = useFallback };
            var results = await DbExec.QueryMultipleAsync("EXECUTE dbo.sp_GetConfiguration_v2 @MachineName, @ApplicationName, @UseFallBack", param);
            ConfigMachine machine = await results.ReadSingleOrDefaultAsync<ConfigMachine>();
            if (machine == null)
            {
                Logger.Warn($"Machine Not Found: {machine}");
                throw new InvalidOperationException($"Machine Not Found: {machine}");
            }
            Logger.Debug($" - Successfully Loaded Machine: {machine}");

            ConfigMachineApplication machApp = await results.ReadSingleOrDefaultAsync<ConfigMachineApplication>();
            if (machApp == null)
            {
                Logger.Warn($"Application Machine Pair Not Found: ('{machineName}','{appName}')");
                throw new InvalidOperationException($"Application Machine Pair Not Found: ('{machineName}','{appName}')");
            }
            Logger.Debug($" - Successfully Loaded Application: {machApp}");


            machApp.Machine = machine;
            machApp.Variables = results.Read<ConfigAppVar>().ToList();
            Logger.Debug($" - Successfully Loaded Variables: {machApp.Variables.Count()}");
            //TODO there is no `ReadAsync<TFirst, TSecond, TReturn>()` yet in dapper
            await Task.Run(() =>
            {
                machApp.Databases = results.Read<ConfigDatabase, ConfigConnectionString, ConfigDatabase>((d, c) => { d.Connection = c; return d; }, splitOn: "ConnectionStringID").ToList();
                machApp.ApiDatabases = results.Read<ConfigApiDatabase, ConfigApiConnection, ConfigApiDatabase>((d, c) => { d.Connection = c; return d; }, splitOn: "ApiConnectionID").ToList();
            });
            Logger.Debug($" - Successfully Loaded Databases: {machApp.Databases.Count()}");
            Logger.Debug($" - Done With Configuration Load!");

            return machApp;
        }

        /// <summary>
        /// Loads a configuration from the database and sets Current property
        /// </summary>
        /// <param name="machineName"> Unique Machine Name </param>
        /// <param name="appName">     Application Name </param>
        /// <param name="useFallback"> Use the default machine if given machine not found </param>
        public ConfigMachineApplication LoadConfiguration(string machineName, string appName, bool useFallback = true)
        {
            _currentConfig = GetConfiguration(machineName, appName, useFallback);
            DataLayer.Base.Common.configMachineApplication = _currentConfig;
            return _currentConfig;
        }

        // <summary>
        /// Loads a configuration from the database and sets Current property
        /// </summary>
        /// <param name="machineName"> Unique Machine Name </param>
        /// <param name="appName">     Application Name </param>
        /// <param name="useFallback"> Use the default machine if given machine not found </param>
        public async Task<ConfigMachineApplication> LoadConfigurationAsync(string machineName, string appName, bool useFallback = true)
        {
            _currentConfig = await GetConfigurationAsync(machineName, appName, useFallback);
            DataLayer.Base.Common.configMachineApplication = _currentConfig;
            return _currentConfig;
        }

        /// <summary>
        /// Set the password on a configuration connection object
        /// </summary>
        /// <param name="connection"> The connection object to set the password to </param>
        /// <param name="password">  The new password </param>
        public void SetPassword(ILogicalConnection connection, string password)
        {
            connection.ThrowIfNull(nameof(connection));
            SetPassword(connection.ConnectionID, password);
        }

        /// <summary>
        /// Set the password on a configuration connection object
        /// </summary>
        /// <param name="connectionId"> The connection object to set the password to </param>
        /// <param name="password">  The new password </param>
        public void SetPassword(Guid connectionId, string password)
        {
            if (connectionId != Guid.Empty)
            {
                DbExec.Execute("EXEC das.sp_SetSecurityKey @ConnectionID, @Password", new
                {
                    ConnectionID = connectionId,
                    Password = password
                });
            }
            else
            {
                throw new InvalidOperationException("Invalid Connection String Object");
            }
        }

        /// <summary>
        /// Set the user ID on a configuration connection object
        /// </summary>
        /// <param name="connection"> The connection object to set the user ID to </param>
        /// <param name="userId">  The new user ID </param>
        public void SetUserID(ILogicalConnection connection, string userId)
        {
            connection.ThrowIfNull(nameof(connection));
            SetUserID(connection.ConnectionID, userId);
        }

        /// <summary>
        /// Set the user ID on a configuration connection object
        /// </summary>
        /// <param name="connectionId"> The connection object to set the user ID to </param>
        /// <param name="userId">  The new user ID </param>
        public void SetUserID(Guid connectionId, string userId)
        {
            if (connectionId != Guid.Empty)
            {
                DbExec.Execute("update [dbo].[ConfigApiConnections] set [UserID] = @userId where [ApiConnectionID] = @connectionId", new
                {
                    connectionId,
                    userId
                });
            }
            else
            {
                throw new InvalidOperationException("Invalid Connection String Object");
            }
        }

        /// <summary>
        /// Set the authorization type on a configuration connection object
        /// </summary>
        /// <param name="connection"> The connection object to set the authorization type to </param>
        /// <param name="authType">  The new authorization type </param>
        public void SetAuthenticationType(ILogicalConnection connection, string authType)
        {
            connection.ThrowIfNull(nameof(connection));
            SetAuthenticationType(connection.ConnectionID, authType);
        }

        /// <summary>
        /// Set the authorization type on a configuration connection object
        /// </summary>
        /// <param name="connectionId"> The connection object to set the authorization type to </param>
        /// <param name="authType">  The new authorization type </param>
        public void SetAuthenticationType(Guid connectionId, string authType)
        {
            if (connectionId != Guid.Empty)
            {
                ApiAuthenticationType authTypeEnum;
                if (!Enum.TryParse(authType, true, out authTypeEnum))
                {
                    throw new InvalidOperationException($"Invalid authentication type: {authType}");
                }

                DbExec.Execute("update [dbo].[ConfigApiConnections] set [Authentication] = @authType where [ApiConnectionID] = @connectionId", new
                {
                    connectionId,
                    authType = authTypeEnum.ToString()
                });
            }
            else
            {
                throw new InvalidOperationException("Invalid Connection String Object");
            }
        }
    }
}

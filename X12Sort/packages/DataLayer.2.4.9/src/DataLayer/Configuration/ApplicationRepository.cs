using DataLayer.Base;
using DataLayer.Configuration.Models;
using DataLayer.StaticUtilities;
using DataLayer.StaticUtilities.Extensions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DataLayer.Configuration
{
    /// <summary>
    /// Functions to manage applications, variables, and connections
    /// </summary>
    public class ApplicationRepository : BaseRepository<ConfigurationDatabase>
    {
        private string _queryMachineApplication = @"
SELECT TOP(1) MA.* FROM ConfigMachineApplications MA
JOIN ConfigMachines M ON M.MachineID = MA.MachineID
WHERE MachineName = @MachineName AND ApplicationName = @ApplicationName
ORDER BY MachineApplicationID DESC
";

        private string _insertMachineApplication = @"
INSERT INTO [ConfigMachineApplications] ([MachineID],[ApplicationName],[ApplicationType],[Description])
VALUES (@MachineID, @ApplicationName, @ApplicationType, @Description);
SELECT SCOPE_IDENTITY()
";
        private string _queryDbConnection = @"
SELECT TOP(1) * FROM (
    SELECT * FROM [ConfigConnectionStrings] WHERE [DatabaseGroup] = @DatabaseGroup AND [Name] = @Name
    UNION 
    SELECT * FROM [ConfigConnectionStrings] WHERE [DatabaseGroup] = @DatabaseGroup AND [IsDefault] = 1
) X";

        private string _queryApiConnection = @"
SELECT TOP(1) * FROM (
    SELECT * FROM [ConfigApiConnections] WHERE [DatabaseGroup] = @DatabaseGroup AND [Name] = @Name
    UNION 
    SELECT * FROM [ConfigApiConnections] WHERE [DatabaseGroup] = @DatabaseGroup AND [IsDefault] = 1
) X";

        private string _queryVariables = @"
SELECT * FROM [ConfigAppVars] WHERE [MachineApplicationID] = @MachineApplicationID";

        private string _insertVariable = @"
INSERT INTO [ConfigAppVars] ([MachineApplicationID],[Name],[Value],[Description])
VALUES (@MachineApplicationID, @Name, @Value, @Description);
SELECT SCOPE_IDENTITY()
";

        private string _deleteVariable = @"DELETE FROM [ConfigAppVars] WHERE [AppVarID] = @AppVarID";

        internal ApplicationRepository(ConfigurationDatabase database) : base(database)
        {
        }

        /// <summary>
        /// Creates a configuration for a brand new application.
        /// </summary>
        public ConfigMachineApplication Create(ConfigMachine machine, string appName, string appType, string description)
        {
            machine.ThrowIfNull(nameof(machine));
            appName.ThrowIfNullOrEmpty(nameof(appName));
            appType.ThrowIfNullOrEmpty(nameof(appType));
            description.ThrowIfNullOrEmpty(nameof(description));

            // Check to make sure the machine ID is loaded
            if (!machine.IsLoaded())
            {
                machine = Database.Machine.Get(machine.MachineName);
            }
            ConfigMachineApplication ma = new ConfigMachineApplication()
            {
                ApplicationName = appName,
                ApplicationType = appType,
                Description = description,
                MachineID = machine.MachineID,
                Machine = machine
            };

            int newId = Database.ExecuteScalar<int>(_insertMachineApplication, ma.ToParameterObject());
            // fill ID and return
            ma.MachineApplicationID = newId;
            return ma;
        }

        /// <summary>
        /// Get a MachineApplicaiton object from Database
        /// </summary>
        /// <param name="machine"> ConfigMachine instance </param>
        /// <param name="appName"> Application name </param>
        /// <returns>  </returns>
        public ConfigMachineApplication Get(ConfigMachine machine, string appName)
        {
            machine.ThrowIfNull(nameof(machine));
            appName.ThrowIfNullOrEmpty(nameof(appName));

            ConfigMachineApplication machineApp = Database.QueryFirstOrDefault<ConfigMachineApplication>(_queryMachineApplication, new
            {
                MachineName = machine.MachineName,
                ApplicationName = appName
            });
            if (machineApp != null)
            {
                machineApp.Machine = machine;
            }

            DataLayer.Base.Common.configMachineApplication = machineApp;

            return machineApp;
        }

        /// <summary>
        /// Creates a new application, then copies databases and variables
        /// </summary>
        /// <remarks>
        /// If <paramref name="reloadData"/> is set, it will reload variables and database before setting them.
        /// </remarks>
        /// <param name="source">      Machine application that is being copied </param>
        /// <param name="destination"> The Machine that clone will run on </param>
        /// <param name="reloadData">If true, reload database and variables from database before setting them</param>
        /// <returns>  </returns>
        public ConfigMachineApplication CopyTo(ConfigMachineApplication source, ConfigMachine destination, bool reloadData = true)
        {
            source.ThrowIfNull(nameof(source));
            destination.ThrowIfNull(nameof(destination));

            ConfigMachineApplication machineApp = Create(destination, source.ApplicationName, source.ApplicationType, source.Description);

            if (reloadData)
            {
                // the Get functions reload the data from the database
                machineApp.Variables = GetVariables(source).Select(q => SetVariable(machineApp, q));
                machineApp.Databases = GetDatabases(source).Select(q => SetDatabase(machineApp, q.Name, q.Connection));
                machineApp.ApiDatabases = GetApiDatabases(source).Select(q => SetDatabase(machineApp, q.Name, q.Connection));
            }
            else
            {
                // note, maybe not the best design, but the Set function will mutate the objects
                foreach (var v in source.Variables) { SetVariable(machineApp, v); }
                foreach (var db in source.LogicalDatabases)
                {
                    ILogicalConnection cxn = GetConnectionOrDefault(db, db.Connection.Name);
                    SetDatabase(machineApp, db.Name, cxn);
                }
            }

            DataLayer.Base.Common.configMachineApplication = machineApp;

            return machineApp;
        }
        #region Variables
        /// <summary>
        /// Loads the Machine Application variables
        /// </summary>
        /// <param name="machineApp"> Machine Application Instance </param>
        /// <returns> The variables for the Machine Application </returns>
        public IEnumerable<ConfigAppVar> GetVariables(ConfigMachineApplication machineApp)
        {
            return GetVariables(machineApp, true);
        }

        /// <summary>
        /// Loads the Machine Application variables
        /// </summary>
        /// <param name="machineApp"> Machine Application Instance </param>
        /// <param name="set">        Set the Variables property on the MachineApplication object </param>
        /// <returns> The variables for the Machine Application </returns>
        public IEnumerable<ConfigAppVar> GetVariables(ConfigMachineApplication machineApp, bool set)
        {
            machineApp.ThrowIfNull(nameof(machineApp));

            Database.Logger.Debug(_queryVariables);
            Database.Logger.Debug(JsonConvert.SerializeObject(machineApp.ToParameterObject()));

            IEnumerable<ConfigAppVar> variables = Database.Query<ConfigAppVar>(_queryVariables, machineApp.ToParameterObject());
            Database.Logger.Debug("Found: " + string.Join(", ", variables.Select(q => q.Name)));
            if (set)
            {
                machineApp.Variables = variables;
            }
            return variables;
        }

        /// <summary>
        /// Sets a variable on the given machine application instance.
        /// </summary>
        /// <param name="machineApp"> Machine Application Instance to set variable on </param>
        /// <param name="name">       New variable name to set </param>
        /// <param name="value">      The value to assign to the variable </param>
        /// <returns>  </returns>
        public ConfigAppVar SetVariable(ConfigMachineApplication machineApp, string name, string value)
        {
            return SetVariable(machineApp, name, value, null);
        }

        /// <summary>
        /// Sets a variable on the given machine application instance.
        /// </summary>
        /// <param name="machineApp">  Machine Application Instance to set variable on </param>
        /// <param name="name">        New variable name to set </param>
        /// <param name="value">       The value to assign to the variable </param>
        /// <param name="description"> The description of the variable, must be set if new variable </param>
        /// <returns>  </returns>
        public ConfigAppVar SetVariable(ConfigMachineApplication machineApp, string name, string value, string description)
        {
            machineApp.ThrowIfNull(nameof(machineApp));
            name.ThrowIfNullOrEmpty(nameof(name));

            // clean up the variable name
            name = name.Sanitize();
            string lowerName = name.ToLower();

            ConfigAppVar item = machineApp.Variables.FirstOrDefault(q => q.Name.ToLower() == lowerName);

            if (item == null)
            {
                //Set new variable
                item = new ConfigAppVar() { Name = name };
            }
            item.Value = value;
            item.Description = description ?? item.Description;
            Database.Logger.Debug(item);
            return SetVariable(machineApp, item);
        }

        /// <summary>
        /// Sets a variable on the given machine application instance.
        /// </summary>
        /// <param name="machineApp"> Machine Application Instance to set variable on </param>
        /// <param name="newVar">     Variable to set, can be from different machineApp </param>
        /// <returns>  </returns>
        public ConfigAppVar SetVariable(ConfigMachineApplication machineApp, ConfigAppVar newVar)
        {
            machineApp.ThrowIfNull(nameof(machineApp));
            newVar.ThrowIfNull(nameof(newVar));

            // This is a variable that was load from the given machineApp, we can just do an update
            if (newVar.IsLoaded() && newVar.MachineApplicationID == machineApp.MachineApplicationID)
            {
                // use the store proc so we can grant rights to process to be able to update vars without creating them
                Database.Logger.Info($"Updating Variable: '{newVar.Name}'");
                Database.Execute("EXECUTE dbo.sp_SetVariable @MachineApplicationID, @Name, @Value, @Description", newVar.ToParameterObject());
                return newVar;
            }
            else
            {
                // create a new variable
                Database.Logger.Info($"Creating Variable: '{newVar.Name}'");
                if (string.IsNullOrEmpty(newVar.Description))
                {
                    throw new ArgumentNullException($"Description Can Not be Null When Setting New Variable! {newVar}");
                }

                ConfigAppVar addVar = newVar.Clone();
                addVar.MachineApplicationID = machineApp.MachineApplicationID;
                addVar.AppVarID = Database.ExecuteScalar<int>(_insertVariable, addVar.ToParameterObject());
                machineApp.Variables = machineApp.Variables.Append(addVar);
                return addVar;
            }
        }

        /// <summary>
        /// Deletes a variable the given machine application instance. 
        /// </summary>
        /// <param name="machineApp"> Machine Application Instance to delete variable from </param>
        /// <param name="newVar">     Variable to delete</param>
        /// <returns>  </returns>
        public void DeleteVariable(ConfigMachineApplication machineApp, ConfigAppVar newVar)
        {
            machineApp.ThrowIfNull(nameof(machineApp));
            newVar.ThrowIfNull(nameof(newVar));

            // This is a variable that was load from the given machineApp, we can just do an update
            if (newVar.IsLoaded() && newVar.MachineApplicationID == machineApp.MachineApplicationID)
            {
                Database.Logger.Info($"Deleting Variable: '{newVar.Name}'");
                Database.Execute(_deleteVariable, newVar.ToParameterObject());
                machineApp.Variables = machineApp.Variables.Where(q => q.AppVarID != newVar.AppVarID).ToList();
            }
            else
            {
                throw new InvalidOperationException("Can't delete variable, it is either no loaded or doesn't belong to machine application");
            }
        }

        #endregion

        #region Databases & Connections
        /// <summary>
        /// Loads the connections strings for the current instance
        /// </summary>
        /// <param name="machineApp"> Machine Application Instance </param>
        /// <returns>  </returns>
        public IEnumerable<ConfigDatabase> GetDatabases(ConfigMachineApplication machineApp)
        {
            return GetDatabases(machineApp, true);
        }

        /// <summary>
        /// Loads the connections strings for the current instance
        /// </summary>
        /// <param name="machineApp"> Machine Application Instance </param>
        /// <param name="set">        Sets the Database Property on the machine application object </param>
        /// <returns>  </returns>
        public IEnumerable<ConfigDatabase> GetDatabases(ConfigMachineApplication machineApp, bool set)
        {
            machineApp.ThrowIfNull(nameof(machineApp));

            // Using dapper and sql because of the join
            string _queryConnectionStrings = @"
SELECT * FROM ConfigDatabases D
JOIN ConfigConnectionStrings  CS ON CS.ConnectionStringID = D.ConnectionStringID
WHERE CS.DatabaseGroup = D.Name
    AND MachineApplicationID = @MachineApplicationID
";
            machineApp.ThrowIfNull("machineApp");

            IEnumerable<ConfigDatabase> databases = Database.DbExec.Query<ConfigDatabase, ConfigConnectionString, ConfigDatabase>(_queryConnectionStrings,
                (d, cs) => { d.Connection = cs; return d; }, machineApp.ToParameterObject(), splitOn: "ConnectionStringID");

            if (set)
            {
                machineApp.Databases = databases;
            }
            return databases;
        }

        /// <summary>
        /// Loads the connections strings for the current instance
        /// </summary>
        /// <param name="machineApp"> Machine Application Instance </param>
        /// <returns>  </returns>
        public IEnumerable<ConfigApiDatabase> GetApiDatabases(ConfigMachineApplication machineApp)
        {
            return GetApiDatabases(machineApp, true);
        }

        /// <summary>
        /// Loads the connections strings for the current instance
        /// </summary>
        /// <param name="machineApp"> Machine Application Instance </param>
        /// <param name="set">        Sets the Database Property on the machine application object </param>
        /// <returns>  </returns>
        public IEnumerable<ConfigApiDatabase> GetApiDatabases(ConfigMachineApplication machineApp, bool set)
        {
            machineApp.ThrowIfNull(nameof(machineApp));

            // Using dapper and sql because of the join
            string _queryConnectionStrings = @"
SELECT * FROM ConfigApiDatabases D
JOIN ConfigApiConnections CS ON CS.ApiConnectionID = D.ApiConnectionID
WHERE CS.DatabaseGroup = D.Name
    AND MachineApplicationID = @MachineApplicationID
";
            machineApp.ThrowIfNull("machineApp");

            IEnumerable<ConfigApiDatabase> databases = Database.DbExec.Query<ConfigApiDatabase, ConfigApiConnection, ConfigApiDatabase>(_queryConnectionStrings,
                (d, cs) => { d.Connection = cs; return d; }, machineApp.ToParameterObject(), splitOn: "ApiConnectionID");

            if (set)
            {
                machineApp.ApiDatabases = databases;
            }
            return databases;
        }

        /// <summary>
        /// Updates or Creates a <c>ConfigDatabase</c> for the given <paramref name="machineApp"/>
        /// </summary>
        /// <param name="machineApp">The <c>ConfigMachineApplication</c> that the <c>ConfigDatabase</c> is being set for</param>
        /// <param name="name">The name of the logical database being set, i.e. Logging, billing, etc.</param>
        /// <param name="connectionString">The <c>ConfigConnectionString</c> object that is being used</param>
        /// <returns>The create/updated object</returns>
        public ConfigDatabase SetDatabase(ConfigMachineApplication machineApp, string name, ConfigConnectionString connectionString)
        {
            machineApp.ThrowIfNull(nameof(machineApp));
            name.ThrowIfNullOrEmpty(nameof(name));
            connectionString.ThrowIfNull(nameof(connectionString));

            if (connectionString.ConnectionID == Guid.Empty)
            {
                throw new ArgumentException("Connection String Object Is Not Valid! (Doesn't have an ConnectionStringID)");
            }

            bool addDatabase = false;
            string updateOrInsert = @"
UPDATE ConfigDatabases
SET ConnectionStringID = @ConnectionStringID
WHERE MachineApplicationID = @MachineApplicationID AND Name = @Name
IF @@ROWCOUNT = 0
    INSERT INTO ConfigDatabases (MachineApplicationID, Name, ConnectionStringID)
    VALUES (@MachineApplicationID, @Name, @ConnectionStringID);
SELECT SCOPE_IDENTITY()
";

            ConfigDatabase database = machineApp.Databases.FirstOrDefault(q => q.Name == name.ToUpper());
            // This is a variable that was load from the given machineApp, we can just do an update
            if (database == null)
            {
                addDatabase = true;
                Database.Logger.Info($"Creating Database Link: '{name}' => {connectionString}");
                database = new ConfigDatabase()
                {
                    MachineApplicationID = machineApp.MachineApplicationID,
                    Name = name.ToUpper(),
                    ConnectionID = connectionString.ConnectionID,
                    Connection = connectionString
                };
            }
            else
            {
                Database.Logger.Info($"Updating Database Link: '{name}'");
                database.Connection = connectionString;
            }
            database.DatabaseID = Database.Execute(updateOrInsert, database.ToParameterObject());
            if (addDatabase)
            {
                machineApp.Databases = machineApp.Databases.Append(database);
            }
            return database;
        }     

        /// <summary>
        /// Gets a connection by name or the default connection for <paramref name="database"/>
        /// </summary>
        /// <remarks>
        /// Looks for a connection for the database name with what was specified.
        /// Also looks for the connection that is default and return that if the names connection can't be found
        /// </remarks>
        /// <param name="database">The logical database, i.e. Logging, billing, etc.</param>
        /// <param name="connectionName">The name of the connection string that is begin looked for</param>
        /// <returns>The connection object</returns>
        /// <exception cref="InvalidOperationException">Throw if connection is not found</exception>
        public ILogicalConnection GetConnectionOrDefault(ILogicalDatabase database, string connectionName)
        {
            database.ThrowIfNull(nameof(database));
            connectionName.ThrowIfNullOrEmpty(nameof(connectionName));

            ILogicalConnection connection;
            var paramObj = new { DatabaseGroup = database.Name, Name = connectionName };

            switch (database)
            {
                case ConfigDatabase _:
                    connection = Database.QuerySingleOrDefault<ConfigConnectionString>(_queryDbConnection, paramObj);
                    break;
                case ConfigApiDatabase _:
                    connection = Database.QuerySingleOrDefault<ConfigApiConnection>(_queryApiConnection, paramObj);
                    break;
                default:
                    throw new InvalidOperationException("Database type not recognized");
            }
            if (connection == null)
            {
                throw new InvalidOperationException($"Can't find named or default connection for Database: {database.Name}");
            }
            else
            {
                return connection;
            }
        }

        /// <summary>
        /// Updates or Creates a <c>ConfigDatabase</c> for the given <paramref name="machineApp"/>
        /// </summary>
        /// <param name="machineApp">The <c>ConfigMachineApplication</c> that the <c>ConfigDatabase</c> is being set for</param>
        /// <param name="name">The name of the logical database being set, i.e. Logging, billing, etc.</param>
        /// <param name="apiConnection">The <c>ConfigConnectionString</c> object that is being used</param>
        /// <returns>The create/updated <c></c> object</returns>
        public ConfigApiDatabase SetDatabase(ConfigMachineApplication machineApp, string name, ConfigApiConnection apiConnection)
        {
            machineApp.ThrowIfNull(nameof(machineApp));
            name.ThrowIfNullOrEmpty(nameof(name));
            apiConnection.ThrowIfNull(nameof(apiConnection));

            if (apiConnection.ConnectionID == Guid.Empty)
            {
                throw new ArgumentException("API Connection Object Is Not Valid! (Doesn't have an ApiConnectionID)");
            }

            bool addDatabase = false;
            string updateOrInsert = @"
UPDATE ConfigApiDatabases
SET ApiConnectionID = @ApiConnectionID
WHERE MachineApplicationID = @MachineApplicationID AND Name = @Name
IF @@ROWCOUNT = 0
    INSERT INTO ConfigApiDatabases (MachineApplicationID, Name, ApiConnectionID)
    VALUES (@MachineApplicationID, @Name, @ApiConnectionID);
SELECT SCOPE_IDENTITY()
";

            ConfigApiDatabase database = machineApp.ApiDatabases.FirstOrDefault(q => q.Name == name.ToUpper());
            // This is a variable that was load from the given machineApp, we can just do an update
            if (database == null)
            {
                addDatabase = true;
                Database.Logger.Info($"Creating API Database Link: '{name}' => {apiConnection}");
                database = new ConfigApiDatabase()
                {
                    MachineApplicationID = machineApp.MachineApplicationID,
                    Name = name.ToUpper(),
                    ConnectionID = apiConnection.ConnectionID,
                    Connection = apiConnection
                };
            }
            else
            {
                Database.Logger.Info($"Updating Database Link: '{name}'");
                database.Connection = apiConnection;
            }
            database.DatabaseID = Database.Execute(updateOrInsert, database.ToParameterObject());
            if (addDatabase)
            {
                machineApp.ApiDatabases = machineApp.ApiDatabases.Append(database);
            }
            return database;
        }

        /// <summary>
        /// Updates or Creates a <c>ConfigDatabase</c> for the given <paramref name="machineApp"/>
        /// </summary>
        /// <param name="machineApp">The <c>ConfigMachineApplication</c> that the <c>ConfigDatabase</c> is being set for</param>
        /// <param name="name">The name of the logical database being set, i.e. Logging, billing, etc.</param>
        /// <param name="connection">The <c>ILogicalConnection</c> object that for the logical database</param>
        /// <returns>The create/updated <c>ILogicalDatabase</c> object</returns>
        public void SetDatabase(ConfigMachineApplication machineApp, string name, ILogicalConnection connection)
        {
            machineApp.ThrowIfNull(nameof(machineApp));
            name.ThrowIfNullOrEmpty(nameof(name));
            connection.ThrowIfNull(nameof(connection));

            if (connection.ConnectionID == Guid.Empty)
            {
                throw new ArgumentException("Connection Object Is Not Valid! (Doesn't have an Connection ID)");
            }

            if (connection is ConfigConnectionString)
            {
                SetDatabase(machineApp, name, connection as ConfigConnectionString);
                return;
            }

            if (connection is ConfigApiConnection)
            {
                SetDatabase(machineApp, name, connection as ConfigApiConnection);
                return;
            }
            throw new InvalidOperationException("Type not recognized");
        }
        #endregion
    }
}

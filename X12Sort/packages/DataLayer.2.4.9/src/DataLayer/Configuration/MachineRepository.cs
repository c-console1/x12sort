using DataLayer.Base;
using DataLayer.Configuration.Models;
using DataLayer.StaticUtilities;
using DataLayer.StaticUtilities.Extensions;
using System;

namespace DataLayer.Configuration
{
    /// <summary>
    /// Collection of function for configuring machines
    /// </summary>
    public class MachineRepository : BaseRepository<ConfigurationDatabase>
    {
        internal static readonly string ConfigMachineQuery = "SELECT * FROM [ConfigMachines] WHERE [MachineName] = @MachineName";

        internal static readonly string InsertMachineQuery = @"
INSERT INTO [ConfigMachines] ([MachineName],[Description],[IsProduction])
     VALUES (@MachineName, @Description, @IsProduction)
";

        internal MachineRepository(ConfigurationDatabase database) : base(database)
        {
        }

        /// <summary>
        /// Register a new machine in the configuration database
        /// </summary>
        /// <param name="machineName"> The system machine name </param>
        /// <param name="description"> A human readable description of the machine </param>
        public ConfigMachine Register(string machineName, string description)
        {
            return Register(machineName, description, false);
        }

        /// <summary>
        /// Register a new machine in the configuration database
        /// </summary>
        /// <param name="machineName">  The system machine name </param>
        /// <param name="description">  A human readable description of the machine </param>
        /// <param name="isProduction"> Flag indicating this is a production machine </param>
        public ConfigMachine Register(string machineName, string description, bool isProduction)
        {
            machineName.ThrowIfNullOrEmpty(nameof(machineName));
            description.ThrowIfNullOrEmpty(nameof(description));

            ConfigMachine newMachine = new ConfigMachine() { MachineName = machineName, Description = description, IsProduction = isProduction };
            try
            {
                newMachine.MachineID = Database.Execute(InsertMachineQuery, newMachine);
                return newMachine;
            }
            catch (Exception e)
            {
                Database.Logger.Error(e);
                throw;
            }
        }

        /// <summary>
        /// Get a Machine object from Database using machine name
        /// </summary>
        /// <param name="machineName"> Unique machine name </param>
        /// <returns>  </returns>
        public ConfigMachine Get(string machineName)
        {
            ConfigMachine machine;

            Database.Logger.Debug($"Loading Machine: {machineName}");
            machine = Database.QueryFirstOrDefault<ConfigMachine>(ConfigMachineQuery, new { MachineName = machineName });

            if (machine != null)
            {
                Database.Logger.Debug($" - Successfully Loaded Machine: {machine}");
            }
            else
            {
                Database.Logger.Warn($"Machine Name: '{machineName}' not found!");
            }
            return machine;
        }
    }
}

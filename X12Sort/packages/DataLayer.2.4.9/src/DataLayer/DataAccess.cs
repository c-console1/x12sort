using DataLayer.Advantage;
using DataLayer.Base;
using DataLayer.Billing;
using DataLayer.CaseManagement;
using DataLayer.CaseManagementAPI;
using DataLayer.CaseManagementConfiguration;
using DataLayer.Configuration;
using DataLayer.Configuration.Models;
using DataLayer.Credentialing;
using DataLayer.EnterpriseDataWarehouse;
using DataLayer.HealthEdgePIK;
using DataLayer.HealthRulesConnectors;
using DataLayer.Logging;
using DataLayer.MessageFaxing;
using DataLayer.ProviderHub;
using DataLayer.StaticUtilities;
using DataLayer.StaticUtilities.Extensions;
using DataLayer.Utility;
using NLog;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace DataLayer
{
    /// <summary>
    /// The main object in the data layer, this is the entry point for using the data layer
    /// </summary>
    /// <example>
    /// <code>
    /// <![CDATA[
    ///using (DataAccess dataAccess = new DataAccess(DbExec.Create()))
    ///{
    ///    dataAccess.LoadApplication(Environment.MachineName, "<app name>", logInstance: true);
    ///    //do stuff here
    ///    dataAccess.Logging.EndInstance();
    ///}
    ///]]>
    ///</code>
    ///</example>
    public class DataAccess : IDisposable
    {
        internal const string CONFIG_DB_CXN_NAME = "CONFIGURATION";

        /// <summary>
        /// A simple delegate for creating `IExecutor` instances
        /// </summary>
        /// <remarks>
        /// <para>**This is for testing**</para>
        /// <para>
        /// `DbExec` require a real connection to a database which is not ideal for unit testing.
        /// The testing library has an implementation of `IDbExec` that "fakes" the database. But in the internals of `DataAccess`, `IDbExec` are created.
        /// There needed to be a way to intercept the calls to create these `IDbExec`, so this simple create function is the mocking layer.
        /// In the test library, this function is re-assigned to create mock `IDbExec`
        /// </para>
        /// </remarks>
        /// <seealso cref="DbExec"/>
        internal Func<ILogicalConnection, IExecutor> CreateExecutorFunc;

        private readonly Dictionary<string, BaseDatabase> _databases;
        private readonly Logger _logger;

        #region Logical databases

        /// <summary>
        /// Get/Set `BaseDatabase` object by name
        /// </summary>
        /// <param name="name">Name of the database</param>
        /// <returns>BaseDatabse Object Which is common to all databases</returns>
        public BaseDatabase this[string name]
        {
            get
            {
                return _getDatabase(name);
            }
            set
            {
                name.ThrowIfNull(nameof(name));
                string internalName = name.Sanitize().ToUpper();
                _databases[internalName] = value;
            }
        }

        /// <summary>
        /// Access to the Advantage Database
        /// </summary>
        /// <value> Gets the <c>AdvatnageDatabase</c>, create it is needed. This will open the databse conneciton on first time access</value>
        public AdvantageDatabase Advantage => GetDatabase<AdvantageDatabase>();

        /// <summary>
        /// Access to the Billing Database
        /// </summary>
        /// <value>Gets the <c>BillingDatabase</c>, creating it is needed. This will open the database connection on first time access</value>
        public BillingDatabase Billing => GetDatabase<BillingDatabase>();


        /// <summary>
        /// Access to the Case Management Database
        /// </summary>
        /// <value>Gets the <c>CaseManagementDatabase</c>, creating it is needed. This will open the database connection on first time access</value>
        public CaseManagementDatabase CaseManagement => GetDatabase<CaseManagementDatabase>();

        public CaseManagementAPIDatabase CaseManagementAPI => GetDatabase<CaseManagementAPIDatabase>();

        /// <summary>
        /// Access to the Case Management Configuration Database
        /// </summary>
        /// <value>Gets the <c>CaseManagementDatabase</c>, creating it is needed. This will open the database connection on first time access</value>
        public CaseManagementConfigurationDatabase CaseManagementConfiguration => GetDatabase<CaseManagementConfigurationDatabase>();

        /// <summary>
        /// Access to the Configuration Database
        /// </summary>
        /// <value>Gets the <c>ConfigurationDatabase</c>, this is the same connection as the <c>DataAccess</c> object was created with</value>
        public ConfigurationDatabase Configuration => GetDatabase<ConfigurationDatabase>();

        /// <summary>
        /// Access to the Credentialing Database
        /// </summary>
        /// <value>Gets the <c>CredentialingDatabase</c>, creating it is needed. This will open the database connection on first time access</value>
        public CredentialingDatabase Credentialing => GetDatabase<CredentialingDatabase>();

        /// <summary>
        /// Access to the EDW Database
        /// </summary>
        /// <value>Gets the <c>EnterpriseDataWarehouseDatabase</c>, creating it is needed. This will open the database connection on first time access</value>
        public EnterpriseDataWarehouseDatabase EnterpriseDataWarehouse => GetDatabase<EnterpriseDataWarehouseDatabase>();

        /// <summary>
        /// Access to the HealthEdge PIK
        /// </summary>
        public HealthEdgePIKDatabase HealthEdgePIK => GetDatabase<HealthEdgePIKDatabase>();

        /// <summary>
        /// Access to the HealthRules connector SOAP services
        /// </summary>
        public HealthRulesConnectorsDatabase HealthRulesConnectors => GetDatabase<HealthRulesConnectorsDatabase>();

        /// <summary>
        /// Access to the Logging Database
        /// </summary>
        /// <value>Gets the <c>LoggingDatabase</c>, creating it is needed. This will open the database connection on first time access</value>
        public LoggingDatabase Logging => GetDatabase<LoggingDatabase>();

        /// <summary>
        /// Access to the Messaging/Faxing Database
        /// </summary>
        /// <value>Gets the <c>MessageFaxingDatabase</c>, creating it is needed. This will open the database connection on first time access</value>
        public MessageFaxingDatabase MessageFaxing => GetDatabase<MessageFaxingDatabase>();

        /// <summary>
        /// Access to the Provider Hub Database
        /// </summary>
        /// <value>Gets the <c>ProviderHubDatabase</c>, creating it is needed. This will open the database connection on first time access</value>
        public ProviderHubDatabase ProviderHub => GetDatabase<ProviderHubDatabase>();

        /// <summary>
        /// Access to the Utility Database
        /// </summary>
        /// <value>Gets the <c>UtilityDatabase</c>, creating it is needed. This will open the database connection on first time access</value>
        public UtilityDatabase Utility => GetDatabase<UtilityDatabase>();

        #endregion Logical databases

        /// <summary>
        /// Create a new <c>DataAccess</c> Object.
        /// </summary>
        /// <remarks>
        /// While this requires <paramref name="configDbExec"/>, it does not do any database operations.
        /// </remarks>
        /// <param name="configDbExec"> An database executor for the configuration database </param>
        public DataAccess(IDbExec configDbExec)
        {
            configDbExec.ThrowIfNull(nameof(configDbExec));

            _logger = LogManager.GetCurrentClassLogger();
            _logger.Debug($"Creating DataLayer Object");

            // Function to create executor from `ILogicalConnection`. Great for intercepting when testing
            CreateExecutorFunc = (c) =>
            {
                if (c is ConfigConnectionString)
                {
                    return new DbExec((DbExec)Configuration.DbExec, c as ConfigConnectionString);
                }
                if (c is ConfigApiConnection)
                {
                    return new ApiExec((DbExec)Configuration.DbExec, c as ConfigApiConnection);
                }
                throw new InvalidOperationException("Connection Type not known, Can't create executor");
            };
            _databases = new Dictionary<string, BaseDatabase>
            {
                { CONFIG_DB_CXN_NAME, new ConfigurationDatabase(configDbExec) }
            };
        }

        /// <summary>
        /// Loads the application configuration for machine/application pair
        /// </summary>
        /// <param name="machineName"> The Machine name to load </param>
        /// <param name="appName">     The Application to Load </param>
        /// <param name="logInstance"> By default, Data Layer creates a new logging instance, can be disabled </param>
        /// <param name="useFallback"> If the machine can't be found, use the fall back machine </param>
        public void LoadApplication(string machineName, string appName, bool logInstance, bool useFallback = true)
        {
            appName.ThrowIfNullOrEmpty(nameof(appName));
            machineName ??= Environment.MachineName;

            _logger.Info($"Loading Configuration for {machineName}/{appName}");

            Configuration.LoadConfiguration(machineName, appName, useFallback);
            if (logInstance)
            {
                Logging.NewInstance(Configuration.Current);
            }
            _logger.Info($"Load Complete: {Configuration.Current}");
        }

        /// <summary>
        /// Loads the application configuration for machine/application pair
        /// </summary>
        /// <param name="machineName"> The Machine name to load </param>
        /// <param name="appName">     The Application to Load </param>
        /// <param name="logInstance"> By default, Data Layer creates a new logging instance, can be disabled </param>
        /// <param name="useFallback"> If the machine can't be found, use the fall back machine </param>
        public async Task LoadApplicationAsync(string machineName, string appName, bool logInstance, bool useFallback = true)
        {
            appName.ThrowIfNullOrEmpty(nameof(appName));
            machineName ??= Environment.MachineName;

            _logger.Info($"Loading Configuration for {machineName}/{appName}");
            await Configuration.LoadConfigurationAsync(machineName, appName, useFallback);
            if (logInstance)
            {
                Logging.NewInstance(Configuration.Current);
            }
            _logger.Info($"Load Complete: {Configuration.Current}");
        }

        /// <summary>
        /// A function to get a list of executors for the given database name.
        /// </summary>
        /// <param name="databaseName"> A "logical" database name. i.e Billing, Credentialing, etc. </param>
        /// <remarks>
        /// This look up is done by `StartsWith` and the database name
        /// </remarks>
        /// <returns> Returns the IDbExec for the database </returns>
        private IEnumerable<IExecutor> _getExecutors(string databaseName)
        {
            databaseName.ThrowIfNullOrEmpty(nameof(databaseName));
            databaseName = databaseName.Sanitize().ToUpper();

            if (Configuration.Current == null)
            {
                throw new InvalidOperationException("Application Not Loaded, can't get database executor");
            }
            IEnumerable<ILogicalDatabase> databaseConfigs = Configuration.Current.LogicalDatabases.Where(q => q.Name.StartsWith(databaseName));
            if (databaseConfigs.Any())
            {
                return databaseConfigs.Select(q => CreateExecutorFunc(q.Connection));
            }
            else
            {
                throw new InvalidOperationException($"Can't Find Connection String For: {databaseName}. Check Application Configuration!");
            }
        }


        /// <summary>
        /// Get/Create a logical Database Object
        /// </summary>
        /// <typeparam name="T">The type DatabseObject, Must subclass `BaseDatabase`. The name of this class should end with Database</typeparam>
        /// <returns> Returns the IDbExec for the database </returns>
        /// <remarks>
        /// <para>
        /// This will create a new instance of the object. The creation of the `IDbExec` is internally handled as well.
        /// </para>
        /// <para>
        /// Internally this function uses `Activator.CreateInstance`.
        /// This only works with internal constructors with adjusted `BindingFlags`:  https://stackoverflow.com/a/4077327
        /// </para>
        /// </remarks>
        /// <example>
        /// <code>
        /// <![CDATA[
        /// LoggingDatabase Logging = dataAccess.GetDatabase<LoggingDatabase>();
        /// ]]>
        /// </code>
        /// </example>
        /// <seealso cref="this[string]"/>
        public T GetDatabase<T>() where T : BaseDatabase
        {
            string internalName = typeof(T).Name.ToUpper().Replace("DATABASE", "");
            if (!_databases.ContainsKey(internalName))
            {
                BindingFlags flags = BindingFlags.NonPublic | BindingFlags.Instance;
                CultureInfo culture = null;
                IEnumerable<IExecutor> executors = _getExecutors(internalName);
                _databases[internalName] = (BaseDatabase)Activator.CreateInstance(typeof(T), flags, null, new object[] { executors.ToArray() }, culture);
            }
            return _databases[internalName] as T;
        }


        /// <summary>
        /// A function to get a logical `BaseDatabase` object by class name
        /// </summary>
        /// <param name="databaseName"> A "logical" database name. i.e Billing, Credentialing, etc. </param>
        /// <returns> Returns the IDbExec for the database </returns>
        /// <remarks>
        /// This search for sub-classes of `BaseDatabse`.
        /// `Activator.CreateInstance` doesn't work with internal constructors unless you adjust the `BindingFlags`:  https://stackoverflow.com/a/4077327
        /// </remarks>
        /// <seealso cref="GetDatabase{T}"/>
        /// <seealso cref="this[string]"/>
        private BaseDatabase _getDatabase(string databaseName)
        {
            BindingFlags flags = BindingFlags.NonPublic | BindingFlags.Instance;
            CultureInfo culture = null;

            databaseName.ThrowIfNullOrEmpty(nameof(databaseName));
            string internalName = databaseName.Sanitize().ToUpper();

            if (!_databases.ContainsKey(internalName))
            {
                IEnumerable<IExecutor> executors = _getExecutors(internalName);
                Type type = typeof(BaseDatabase).Assembly.GetTypes()
                    .Where(q => q.IsSubclassOf(typeof(BaseDatabase)))
                    .FirstOrDefault(q => q.Name.ToUpper().StartsWith(internalName));
                if (type == null)
                {
                    throw new InvalidOperationException($"Invalid type given, can't find a database object for '{databaseName}'");
                }
                _databases[internalName] = (BaseDatabase)Activator.CreateInstance(type, flags, null, executors.ToArray(), culture);
            }
            return _databases[internalName];
        }


        #region IDisposable Support

        private bool disposedValue = false; // To detect redundant calls

        private void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                // log the disposing if the configuration is load (else we don't have database connection string)
                if (Configuration.Current != null)
                {
                    if (Logging.InstanceId != Guid.Empty)
                    {
                        Logging.UnexpectedCleanup();
                    }
                }
                _logger.Debug("Disposing the Main DataLayer Object ... ");
                if (disposing)
                {
                    // clean up any other database that were loaded
                    foreach (var db in _databases.Values)
                    {
                        db.Dispose();
                    }
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.
                _logger.Debug("Good Bye");
                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }

        #endregion IDisposable Support
    }
}

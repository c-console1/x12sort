namespace DataLayer.Base.Enumerations
{
    /// <summary>
    /// Claim type
    /// </summary>
    public enum ClaimType : int
    {
        /// <summary>
        /// Unknown claim type
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Professional/medical claim (HCFA-1500)
        /// </summary>
        Professional = 1,

        /// <summary>
        /// Institutional/hospital claim (UB-04)
        /// </summary>
        Institutional = 2,

        /// <summary>
        /// Dental claim
        /// </summary>
        Dental = 3,

        /// <summary>
        /// Pharmacy/drug/RX claim
        /// </summary>
        Pharmacy = 4,

        Other = 999
    }
}

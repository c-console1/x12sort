namespace DataLayer.Base.Enumerations
{
    /// <summary>
    /// Claim Status
    /// </summary>
    public enum ServiceLineStatus : int
    {
        Received = 0,
        Paid = 1,
        Denied = 2,
        Pended = 3
    }
}

using DataLayer.StaticUtilities.Attributes;

namespace DataLayer.Base.Enumerations
{
    public enum Sex : int
    {
        [EnumConverter("", true)]
        Unknown = 0,

        [EnumConverter("F")]
        Female = 1,

        [EnumConverter("M")]
        Male = 2,

        [EnumConverter("O")]
        Other = 999
    }
}

namespace DataLayer.Base.Enumerations
{
    /// <summary>
    /// Claim Status
    /// </summary>
    public enum ClaimStatus : int
    {
        Received = 0,
        Paid = 1,
        Denied = 2,
        Pended = 3
    }
}

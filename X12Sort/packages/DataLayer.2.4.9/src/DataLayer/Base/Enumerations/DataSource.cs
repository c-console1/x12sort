namespace DataLayer.Base.Enumerations
{
    /// <summary>
    /// Source this data was built from
    /// </summary>
    public enum DataSource : int
    {
        Unknown = 0,

        EDW = 1,
        HRPEDW = 2,
        HRPConnector = 3,
        AMISYS = 4,
        TruCare = 5,
        Cactus = 6,

        Other = 999
    }
}

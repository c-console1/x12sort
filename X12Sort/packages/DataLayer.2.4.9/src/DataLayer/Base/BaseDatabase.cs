using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace DataLayer.Base
{
    /// <summary>
    /// Class to abstract away a real database into an object. This class exposes an IDbExec which handles the connection to the real database.
    /// </summary>
    public abstract class BaseDatabase : IDisposable
    {
        private List<IExecutor> _executors;

        /// <summary>
        /// The Database Executer (AKA connection to the real database)
        /// </summary>
        /// <remarks>
        /// This is a reference to the first `IDbExec` in the constructor.
        /// Most of the time, a `BaseDatabase` will only ever have one database executor.
        /// Hence, this is correct in almost all cases when using the database executor
        /// Though on off chance where there is more than one, this will be the first one loaded.
        /// </remarks>
        internal IDbExec DbExec { get; }

        /// <summary>
        /// List all executors in the logical database object
        /// </summary>
        /// <remarks>
        /// In almost all cases, there will only be one executor per database
        /// </remarks>
        internal IEnumerable<IExecutor> Executors => _executors;

        /// <summary>
        /// The NLog Logger instance for this class
        /// </summary>
        internal Logger Logger { get; }

        /// <summary>
        /// Creates a new BaseDatabase, should be called by inheriting class
        /// </summary>
        /// <param name="dbExec"> The database executor for this database </param>
        protected BaseDatabase(IDbExec dbExec)
        {
            Logger = LogManager.GetLogger(GetType().FullName);
            Logger.Debug("Creating Database: " + GetType().Name);
            DbExec = dbExec;
            _executors = new List<IExecutor>() { dbExec };
        }

        /// <summary>
        /// Creates a new BaseDatabase, should be called by inheriting class
        /// </summary>
        /// <param name="executors"> The executor for this database </param>
        protected BaseDatabase(params IExecutor[] executors)
        {
            Logger = LogManager.GetLogger(GetType().FullName);
            Logger.Debug("Creating Database: " + GetType().Name);
            _executors = executors.ToList();
            DbExec = _executors.FirstOrDefault(q => q is IDbExec) as IDbExec;
        }

        #region IDisposable Support

        protected bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    Logger.Debug("Disposing Database: " + GetType().Name);
                    foreach (var exec in _executors)
                    {
                        exec.Dispose();
                    }
                }
                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }

        #endregion IDisposable Support

        #region Query Functions

        /// <summary>
        /// Query that returns a Key,Value hashtable.
        /// <para>
        /// <typeparamref name="TValue"/> can be either a simple or complex value.
        ///  * **Simple**: Assumes a query that returns two columns which should the second column should be name value
        ///  * **Complex**: Assumes the first column is the key and every other column is part of the type, *you must give a <paramref name="splitOn"/> value!*
        /// </para>
        /// <para>
        /// Look at the examples for usage
        /// </para>
        /// </summary>
        /// <typeparam name="TKey">   The type of Key </typeparam>
        /// <typeparam name="TValue"> The type of  Value </typeparam>
        /// <param name="sql">            The SQL to execute for this query. </param>
        /// <param name="param">          The parameters to use for this query. </param>
        /// <param name="transaction">    The transaction to use for this query. </param>
        /// <param name="splitOn">        The field we should split and read the second object from (default: "value"). </param>
        /// <param name="commandTimeout"> Number of seconds before command execution timeout. </param>
        /// <param name="commandType">    Is it a stored proc or a batch? </param>
        /// <returns> Returns a dictionary from the given query </returns>
        /// <remarks>
        /// <para>
        /// This function uses dapper multi-mapper underneath. If you are wondering where the whole <paramref name="splitOn"/> thing came from, that is where.
        /// </para>
        /// <para>
        /// The value of split on can be anything that works with your query, "value" is just an assumed as a useful default.
        /// </para>
        /// </remarks>
        /// <example>
        /// **Simple Dictionary** - You can see how the query has named columns
        /// <code>
        /// <![CDATA[
        /// var dict = dataAccess.Configuration.GetDictionary<int, string>(@"Select MachineID as key, MachineName as Value FROM ConfigMachines");
        /// ]]>
        /// </code>
        /// </example>
        /// <example>
        /// **Complex Dictionary** - A <paramref name="splitOn"/> value was given
        /// <code>
        /// <![CDATA[
        /// var dict = dataAccess.Configuration.GetDictionary<int, ConfigMachine>(@"Select m.MachineID as id, m.* FROM ConfigMachines m", splitOn: "MachineId");
        /// ]]>
        /// </code>
        /// </example>
        public Dictionary<TKey, TValue> GetDictionary<TKey, TValue>(string sql, object param = null, IDbTransaction transaction = null, string splitOn = "value", int? commandTimeout = null, CommandType? commandType = null)
        {
            try
            {
                Dictionary<TKey, TValue> result = new Dictionary<TKey, TValue>();
                Func<TKey, TValue, Dictionary<TKey, TValue>> map = (k, v) =>
                {
                    result.Add(k, v);
                    return result;
                };
                DbExec.Query(sql, map, param, transaction, true, splitOn, commandTimeout, commandType);
                return result;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Database query failed!");
                throw;
            }
        }

        /// <summary>
        /// Get a DataTable from the query
        /// </summary>
        /// <param name="sql">            The SQL to execute for this query. </param>
        /// <param name="param">          The parameters to use for this query. </param>
        /// <param name="transaction">    The transaction to use for this query. </param>
        /// <param name="commandTimeout"> Number of seconds before command execution timeout. </param>
        /// <param name="commandType">    Is it a stored proc or a batch? </param>
        /// <returns>  </returns>
        public DataTable GetDataTable(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null)
        {
            try
            {
                IDataReader reader = DbExec.ExecuteReader(sql, param, transaction, commandTimeout, commandType);
                DataTable dt = new DataTable();
                dt.Load(reader);
                return dt;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Database query failed!");
                throw e;
            }
        }

        #endregion Query Functions

        #region Dapper Functions

        /// <summary>
        /// Execute parameterized SQL.
        /// </summary>
        /// <param name="sql">            The SQL to execute for this query. </param>
        /// <param name="param">          The parameters to use for this query. </param>
        /// <param name="transaction">    The transaction to use for this query. </param>
        /// <param name="commandTimeout"> Number of seconds before command execution timeout. </param>
        /// <param name="commandType">    Is it a stored proc or a batch? </param>
        /// <returns> The number of rows affected. </returns>
        public int Execute(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.Execute(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute parameterized SQL that selects a single value.
        /// </summary>
        /// <param name="sql">            The SQL to execute. </param>
        /// <param name="param">          The parameters to use for this command. </param>
        /// <param name="transaction">    The transaction to use for this command. </param>
        /// <param name="commandTimeout"> Number of seconds before command execution timeout. </param>
        /// <param name="commandType">    Is it a stored proc or a batch? </param>
        /// <returns> The first cell selected as <see cref="object"/>. </returns>
        public object ExecuteScalar(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.ExecuteScalar(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute parameterized SQL that selects a single value.
        /// </summary>
        /// <typeparam name="T"> The type to return. </typeparam>
        /// <param name="sql">            The SQL to execute. </param>
        /// <param name="param">          The parameters to use for this command. </param>
        /// <param name="transaction">    The transaction to use for this command. </param>
        /// <param name="commandTimeout"> Number of seconds before command execution timeout. </param>
        /// <param name="commandType">    Is it a stored proc or a batch? </param>
        /// <returns> The first cell returned, as <typeparamref name="T"/>. </returns>
        public T ExecuteScalar<T>(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.ExecuteScalar<T>(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute parameterized SQL and return an <see cref="IDataReader"/>.
        /// </summary>
        /// <param name="sql">            The SQL to execute. </param>
        /// <param name="param">          The parameters to use for this command. </param>
        /// <param name="transaction">    The transaction to use for this command. </param>
        /// <param name="commandTimeout"> Number of seconds before command execution timeout. </param>
        /// <param name="commandType">    Is it a stored proc or a batch? </param>
        /// <returns> An <see cref="IDataReader"/> that can be used to iterate over the results of the SQL query. </returns>
        /// <remarks> This is typically used when the results of a query are not processed by Dapper, for example, used to fill a <see cref="DataTable"/> or <see cref="T:DataSet"/>. </remarks>
        /// <example>
        /// <code>
        /// <![CDATA[
        /// DataTable table = new DataTable("MyTable");
        /// using (var reader = ExecuteReader(cnn, sql, param))
        /// {
        /// table.Load(reader);
        /// }
        /// ]]>
        /// </code>
        /// </example>
        public IDataReader ExecuteReader(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.ExecuteReader(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Return a sequence of dynamic objects with properties matching the columns.
        /// </summary>
        /// <param name="sql">            The SQL to execute for the query. </param>
        /// <param name="param">          The parameters to pass, if any. </param>
        /// <param name="transaction">    The transaction to use, if any. </param>
        /// <param name="buffered">       Whether to buffer the results in memory. </param>
        /// <param name="commandTimeout"> The command timeout (in seconds). </param>
        /// <param name="commandType">    The type of command to execute. </param>
        /// <remarks> Note: each row can be accessed via "dynamic", or by casting to an IDictionary&lt;string,object&gt; </remarks>
        public IEnumerable<dynamic> Query(string sql, object param = null, IDbTransaction transaction = null, bool buffered = true, int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.Query<dynamic>(sql, param, transaction, buffered, commandTimeout, commandType);

        /// <summary>
        /// Return a dynamic object with properties matching the columns.
        /// </summary>
        /// <param name="sql">            The SQL to execute for the query. </param>
        /// <param name="param">          The parameters to pass, if any. </param>
        /// <param name="transaction">    The transaction to use, if any. </param>
        /// <param name="commandTimeout"> The command timeout (in seconds). </param>
        /// <param name="commandType">    The type of command to execute. </param>
        /// <remarks> Note: the row can be accessed via "dynamic", or by casting to an IDictionary&lt;string,object&gt; </remarks>
        public dynamic QueryFirst(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.QueryFirst<dynamic>(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Return a dynamic object with properties matching the columns.
        /// </summary>
        /// <param name="sql">            The SQL to execute for the query. </param>
        /// <param name="param">          The parameters to pass, if any. </param>
        /// <param name="transaction">    The transaction to use, if any. </param>
        /// <param name="commandTimeout"> The command timeout (in seconds). </param>
        /// <param name="commandType">    The type of command to execute. </param>
        /// <remarks> Note: the row can be accessed via "dynamic", or by casting to an IDictionary&lt;string,object&gt; </remarks>
        public dynamic QueryFirstOrDefault(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.QueryFirstOrDefault<dynamic>(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Return a dynamic object with properties matching the columns.
        /// </summary>
        /// <param name="sql">            The SQL to execute for the query. </param>
        /// <param name="param">          The parameters to pass, if any. </param>
        /// <param name="transaction">    The transaction to use, if any. </param>
        /// <param name="commandTimeout"> The command timeout (in seconds). </param>
        /// <param name="commandType">    The type of command to execute. </param>
        /// <remarks> Note: the row can be accessed via "dynamic", or by casting to an IDictionary&lt;string,object&gt; </remarks>
        public dynamic QuerySingle(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.QuerySingle<dynamic>(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Return a dynamic object with properties matching the columns.
        /// </summary>
        /// <param name="sql">            The SQL to execute for the query. </param>
        /// <param name="param">          The parameters to pass, if any. </param>
        /// <param name="transaction">    The transaction to use, if any. </param>
        /// <param name="commandTimeout"> The command timeout (in seconds). </param>
        /// <param name="commandType">    The type of command to execute. </param>
        /// <remarks> Note: the row can be accessed via "dynamic", or by casting to an IDictionary&lt;string,object&gt; </remarks>
        public dynamic QuerySingleOrDefault(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.QuerySingleOrDefault<dynamic>(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Executes a query, returning the data typed as <typeparamref name="T"/>.
        /// </summary>
        /// <typeparam name="T"> The type of results to return. </typeparam>
        /// <param name="sql">            The SQL to execute for the query. </param>
        /// <param name="param">          The parameters to pass, if any. </param>
        /// <param name="transaction">    The transaction to use, if any. </param>
        /// <param name="buffered">       Whether to buffer results in memory. </param>
        /// <param name="commandTimeout"> The command timeout (in seconds). </param>
        /// <param name="commandType">    The type of command to execute. </param>
        /// <returns>
        /// A sequence of data of the supplied type; if a basic type (int, string, etc) is queried then the data from the first column in assumed, otherwise an instance is created per row, and a direct column-name===member-name mapping is assumed (case insensitive).
        /// </returns>
        public IEnumerable<T> Query<T>(string sql, object param = null, IDbTransaction transaction = null, bool buffered = true, int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.Query<T>(sql, param, transaction, buffered, commandTimeout, commandType);

        /// <summary>
        /// Executes a single-row query, returning the data typed as <typeparamref name="T"/>.
        /// </summary>
        /// <typeparam name="T"> The type of result to return. </typeparam>
        /// <param name="sql">            The SQL to execute for the query. </param>
        /// <param name="param">          The parameters to pass, if any. </param>
        /// <param name="transaction">    The transaction to use, if any. </param>
        /// <param name="commandTimeout"> The command timeout (in seconds). </param>
        /// <param name="commandType">    The type of command to execute. </param>
        /// <returns>
        /// A sequence of data of the supplied type; if a basic type (int, string, etc) is queried then the data from the first column in assumed, otherwise an instance is created per row, and a direct column-name===member-name mapping is assumed (case insensitive).
        /// </returns>
        public T QueryFirst<T>(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.QueryFirst<T>(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Executes a single-row query, returning the data typed as <typeparamref name="T"/>.
        /// </summary>
        /// <typeparam name="T"> The type of result to return. </typeparam>
        /// <param name="sql">            The SQL to execute for the query. </param>
        /// <param name="param">          The parameters to pass, if any. </param>
        /// <param name="transaction">    The transaction to use, if any. </param>
        /// <param name="commandTimeout"> The command timeout (in seconds). </param>
        /// <param name="commandType">    The type of command to execute. </param>
        /// <returns>
        /// A sequence of data of the supplied type; if a basic type (int, string, etc) is queried then the data from the first column in assumed, otherwise an instance is created per row, and a direct column-name===member-name mapping is assumed (case insensitive).
        /// </returns>
        public T QueryFirstOrDefault<T>(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.QueryFirstOrDefault<T>(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Executes a single-row query, returning the data typed as <typeparamref name="T"/>.
        /// </summary>
        /// <typeparam name="T"> The type of result to return. </typeparam>
        /// <param name="sql">            The SQL to execute for the query. </param>
        /// <param name="param">          The parameters to pass, if any. </param>
        /// <param name="transaction">    The transaction to use, if any. </param>
        /// <param name="commandTimeout"> The command timeout (in seconds). </param>
        /// <param name="commandType">    The type of command to execute. </param>
        /// <returns>
        /// A sequence of data of the supplied type; if a basic type (int, string, etc) is queried then the data from the first column in assumed, otherwise an instance is created per row, and a direct column-name===member-name mapping is assumed (case insensitive).
        /// </returns>
        public T QuerySingle<T>(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.QuerySingle<T>(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Executes a single-row query, returning the data typed as <typeparamref name="T"/>.
        /// </summary>
        /// <typeparam name="T"> The type of result to return. </typeparam>
        /// <param name="sql">            The SQL to execute for the query. </param>
        /// <param name="param">          The parameters to pass, if any. </param>
        /// <param name="transaction">    The transaction to use, if any. </param>
        /// <param name="commandTimeout"> The command timeout (in seconds). </param>
        /// <param name="commandType">    The type of command to execute. </param>
        /// <returns>
        /// A sequence of data of the supplied type; if a basic type (int, string, etc) is queried then the data from the first column in assumed, otherwise an instance is created per row, and a direct column-name===member-name mapping is assumed (case insensitive).
        /// </returns>
        public T QuerySingleOrDefault<T>(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.QuerySingleOrDefault<T>(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Executes a single-row query, returning the data typed as <paramref name="type"/>.
        /// </summary>
        /// <param name="type">           The type to return. </param>
        /// <param name="sql">            The SQL to execute for the query. </param>
        /// <param name="param">          The parameters to pass, if any. </param>
        /// <param name="transaction">    The transaction to use, if any. </param>
        /// <param name="buffered">       Whether to buffer results in memory. </param>
        /// <param name="commandTimeout"> The command timeout (in seconds). </param>
        /// <param name="commandType">    The type of command to execute. </param>
        /// <exception cref="ArgumentNullException"> <paramref name="type"/> is <c> null </c>. </exception>
        /// <returns>
        /// A sequence of data of the supplied type; if a basic type (int, string, etc) is queried then the data from the first column in assumed, otherwise an instance is created per row, and a direct column-name===member-name mapping is assumed (case insensitive).
        /// </returns>
        public IEnumerable<object> Query(Type type, string sql, object param = null, IDbTransaction transaction = null, bool buffered = true, int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.Query(type, sql, param, transaction, buffered, commandTimeout, commandType);

        /// <summary>
        /// Executes a single-row query, returning the data typed as <paramref name="type"/>.
        /// </summary>
        /// <param name="type">           The type to return. </param>
        /// <param name="sql">            The SQL to execute for the query. </param>
        /// <param name="param">          The parameters to pass, if any. </param>
        /// <param name="transaction">    The transaction to use, if any. </param>
        /// <param name="commandTimeout"> The command timeout (in seconds). </param>
        /// <param name="commandType">    The type of command to execute. </param>
        /// <exception cref="ArgumentNullException"> <paramref name="type"/> is <c> null </c>. </exception>
        /// <returns>
        /// A sequence of data of the supplied type; if a basic type (int, string, etc) is queried then the data from the first column in assumed, otherwise an instance is created per row, and a direct column-name===member-name mapping is assumed (case insensitive).
        /// </returns>
        public object QueryFirst(Type type, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.QueryFirst(type, sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Executes a single-row query, returning the data typed as <paramref name="type"/>.
        /// </summary>
        /// <param name="type">           The type to return. </param>
        /// <param name="sql">            The SQL to execute for the query. </param>
        /// <param name="param">          The parameters to pass, if any. </param>
        /// <param name="transaction">    The transaction to use, if any. </param>
        /// <param name="commandTimeout"> The command timeout (in seconds). </param>
        /// <param name="commandType">    The type of command to execute. </param>
        /// <exception cref="ArgumentNullException"> <paramref name="type"/> is <c> null </c>. </exception>
        /// <returns>
        /// A sequence of data of the supplied type; if a basic type (int, string, etc) is queried then the data from the first column in assumed, otherwise an instance is created per row, and a direct column-name===member-name mapping is assumed (case insensitive).
        /// </returns>
        public object QueryFirstOrDefault(Type type, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.QueryFirstOrDefault(type, sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Executes a single-row query, returning the data typed as <paramref name="type"/>.
        /// </summary>
        /// <param name="type">           The type to return. </param>
        /// <param name="sql">            The SQL to execute for the query. </param>
        /// <param name="param">          The parameters to pass, if any. </param>
        /// <param name="transaction">    The transaction to use, if any. </param>
        /// <param name="commandTimeout"> The command timeout (in seconds). </param>
        /// <param name="commandType">    The type of command to execute. </param>
        /// <exception cref="ArgumentNullException"> <paramref name="type"/> is <c> null </c>. </exception>
        /// <returns>
        /// A sequence of data of the supplied type; if a basic type (int, string, etc) is queried then the data from the first column in assumed, otherwise an instance is created per row, and a direct column-name===member-name mapping is assumed (case insensitive).
        /// </returns>
        public object QuerySingle(Type type, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.QuerySingle(type, sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Executes a single-row query, returning the data typed as <paramref name="type"/>.
        /// </summary>
        /// <param name="type">           The type to return. </param>
        /// <param name="sql">            The SQL to execute for the query. </param>
        /// <param name="param">          The parameters to pass, if any. </param>
        /// <param name="transaction">    The transaction to use, if any. </param>
        /// <param name="commandTimeout"> The command timeout (in seconds). </param>
        /// <param name="commandType">    The type of command to execute. </param>
        /// <exception cref="ArgumentNullException"> <paramref name="type"/> is <c> null </c>. </exception>
        /// <returns>
        /// A sequence of data of the supplied type; if a basic type (int, string, etc) is queried then the data from the first column in assumed, otherwise an instance is created per row, and a direct column-name===member-name mapping is assumed (case insensitive).
        /// </returns>
        public object QuerySingleOrDefault(Type type, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.QuerySingleOrDefault(type, sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute a command that returns multiple result sets, and access each in turn.
        /// </summary>
        /// <param name="sql">            The SQL to execute for this query. </param>
        /// <param name="param">          The parameters to use for this query. </param>
        /// <param name="transaction">    The transaction to use for this query. </param>
        /// <param name="commandTimeout"> Number of seconds before command execution timeout. </param>
        /// <param name="commandType">    Is it a stored proc or a batch? </param>
        public IDbGridReader QueryMultiple(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.QueryMultiple(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Perform a multi-mapping query with 2 input types. This returns a single type, combined from the raw types via <paramref name="map"/>.
        /// </summary>
        /// <typeparam name="TFirst"> The first type in the recordset. </typeparam>
        /// <typeparam name="TSecond"> The second type in the recordset. </typeparam>
        /// <typeparam name="TReturn"> The combined type to return. </typeparam>
        /// <param name="sql">            The SQL to execute for this query. </param>
        /// <param name="map">            The function to map row types to the return type. </param>
        /// <param name="param">          The parameters to use for this query. </param>
        /// <param name="transaction">    The transaction to use for this query. </param>
        /// <param name="buffered">       Whether to buffer the results in memory. </param>
        /// <param name="splitOn">        The field we should split and read the second object from (default: "Id"). </param>
        /// <param name="commandTimeout"> Number of seconds before command execution timeout. </param>
        /// <param name="commandType">    Is it a stored proc or a batch? </param>
        /// <returns> An enumerable of <typeparamref name="TReturn"/>. </returns>
        public IEnumerable<TReturn> Query<TFirst, TSecond, TReturn>(string sql, Func<TFirst, TSecond, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.Query(sql, map, param, transaction, buffered, splitOn, commandTimeout, commandType);

        /// <summary>
        /// Perform a multi-mapping query with 3 input types. This returns a single type, combined from the raw types via <paramref name="map"/>.
        /// </summary>
        /// <typeparam name="TFirst"> The first type in the recordset. </typeparam>
        /// <typeparam name="TSecond"> The second type in the recordset. </typeparam>
        /// <typeparam name="TThird"> The third type in the recordset. </typeparam>
        /// <typeparam name="TReturn"> The combined type to return. </typeparam>
        /// <param name="sql">            The SQL to execute for this query. </param>
        /// <param name="map">            The function to map row types to the return type. </param>
        /// <param name="param">          The parameters to use for this query. </param>
        /// <param name="transaction">    The transaction to use for this query. </param>
        /// <param name="buffered">       Whether to buffer the results in memory. </param>
        /// <param name="splitOn">        The field we should split and read the second object from (default: "Id"). </param>
        /// <param name="commandTimeout"> Number of seconds before command execution timeout. </param>
        /// <param name="commandType">    Is it a stored proc or a batch? </param>
        /// <returns> An enumerable of <typeparamref name="TReturn"/>. </returns>
        public IEnumerable<TReturn> Query<TFirst, TSecond, TThird, TReturn>(string sql, Func<TFirst, TSecond, TThird, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.Query(sql, map, param, transaction, buffered, splitOn, commandTimeout, commandType);

        /// <summary>
        /// Perform a multi-mapping query with 4 input types. This returns a single type, combined from the raw types via <paramref name="map"/>.
        /// </summary>
        /// <typeparam name="TFirst"> The first type in the recordset. </typeparam>
        /// <typeparam name="TSecond"> The second type in the recordset. </typeparam>
        /// <typeparam name="TThird"> The third type in the recordset. </typeparam>
        /// <typeparam name="TFourth"> The fourth type in the recordset. </typeparam>
        /// <typeparam name="TReturn"> The combined type to return. </typeparam>
        /// <param name="sql">            The SQL to execute for this query. </param>
        /// <param name="map">            The function to map row types to the return type. </param>
        /// <param name="param">          The parameters to use for this query. </param>
        /// <param name="transaction">    The transaction to use for this query. </param>
        /// <param name="buffered">       Whether to buffer the results in memory. </param>
        /// <param name="splitOn">        The field we should split and read the second object from (default: "Id"). </param>
        /// <param name="commandTimeout"> Number of seconds before command execution timeout. </param>
        /// <param name="commandType">    Is it a stored proc or a batch? </param>
        /// <returns> An enumerable of <typeparamref name="TReturn"/>. </returns>
        public IEnumerable<TReturn> Query<TFirst, TSecond, TThird, TFourth, TReturn>(string sql, Func<TFirst, TSecond, TThird, TFourth, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.Query(sql, map, param, transaction, buffered, splitOn, commandTimeout, commandType);

        /// <summary>
        /// Perform a multi-mapping query with 5 input types. This returns a single type, combined from the raw types via <paramref name="map"/>.
        /// </summary>
        /// <typeparam name="TFirst"> The first type in the recordset. </typeparam>
        /// <typeparam name="TSecond"> The second type in the recordset. </typeparam>
        /// <typeparam name="TThird"> The third type in the recordset. </typeparam>
        /// <typeparam name="TFourth"> The fourth type in the recordset. </typeparam>
        /// <typeparam name="TFifth"> The fifth type in the recordset. </typeparam>
        /// <typeparam name="TReturn"> The combined type to return. </typeparam>
        /// <param name="sql">            The SQL to execute for this query. </param>
        /// <param name="map">            The function to map row types to the return type. </param>
        /// <param name="param">          The parameters to use for this query. </param>
        /// <param name="transaction">    The transaction to use for this query. </param>
        /// <param name="buffered">       Whether to buffer the results in memory. </param>
        /// <param name="splitOn">        The field we should split and read the second object from (default: "Id"). </param>
        /// <param name="commandTimeout"> Number of seconds before command execution timeout. </param>
        /// <param name="commandType">    Is it a stored proc or a batch? </param>
        /// <returns> An enumerable of <typeparamref name="TReturn"/>. </returns>
        public IEnumerable<TReturn> Query<TFirst, TSecond, TThird, TFourth, TFifth, TReturn>(string sql, Func<TFirst, TSecond, TThird, TFourth, TFifth, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.Query(sql, map, param, transaction, buffered, splitOn, commandTimeout, commandType);

        /// <summary>
        /// Perform a multi-mapping query with 6 input types. This returns a single type, combined from the raw types via <paramref name="map"/>.
        /// </summary>
        /// <typeparam name="TFirst"> The first type in the recordset. </typeparam>
        /// <typeparam name="TSecond"> The second type in the recordset. </typeparam>
        /// <typeparam name="TThird"> The third type in the recordset. </typeparam>
        /// <typeparam name="TFourth"> The fourth type in the recordset. </typeparam>
        /// <typeparam name="TFifth"> The fifth type in the recordset. </typeparam>
        /// <typeparam name="TSixth"> The sixth type in the recordset. </typeparam>
        /// <typeparam name="TReturn"> The combined type to return. </typeparam>
        /// <param name="sql">            The SQL to execute for this query. </param>
        /// <param name="map">            The function to map row types to the return type. </param>
        /// <param name="param">          The parameters to use for this query. </param>
        /// <param name="transaction">    The transaction to use for this query. </param>
        /// <param name="buffered">       Whether to buffer the results in memory. </param>
        /// <param name="splitOn">        The field we should split and read the second object from (default: "Id"). </param>
        /// <param name="commandTimeout"> Number of seconds before command execution timeout. </param>
        /// <param name="commandType">    Is it a stored proc or a batch? </param>
        /// <returns> An enumerable of <typeparamref name="TReturn"/>. </returns>
        public IEnumerable<TReturn> Query<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TReturn>(string sql, Func<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.Query(sql, map, param, transaction, buffered, splitOn, commandTimeout, commandType);

        /// <summary>
        /// Perform a multi-mapping query with 7 input types. If you need more types -&gt; use Query with Type[] parameter. This returns a single type, combined from the raw types via <paramref name="map"/>.
        /// </summary>
        /// <typeparam name="TFirst"> The first type in the recordset. </typeparam>
        /// <typeparam name="TSecond"> The second type in the recordset. </typeparam>
        /// <typeparam name="TThird"> The third type in the recordset. </typeparam>
        /// <typeparam name="TFourth"> The fourth type in the recordset. </typeparam>
        /// <typeparam name="TFifth"> The fifth type in the recordset. </typeparam>
        /// <typeparam name="TSixth"> The sixth type in the recordset. </typeparam>
        /// <typeparam name="TSeventh"> The seventh type in the recordset. </typeparam>
        /// <typeparam name="TReturn"> The combined type to return. </typeparam>
        /// <param name="sql">            The SQL to execute for this query. </param>
        /// <param name="map">            The function to map row types to the return type. </param>
        /// <param name="param">          The parameters to use for this query. </param>
        /// <param name="transaction">    The transaction to use for this query. </param>
        /// <param name="buffered">       Whether to buffer the results in memory. </param>
        /// <param name="splitOn">        The field we should split and read the second object from (default: "Id"). </param>
        /// <param name="commandTimeout"> Number of seconds before command execution timeout. </param>
        /// <param name="commandType">    Is it a stored proc or a batch? </param>
        /// <returns> An enumerable of <typeparamref name="TReturn"/>. </returns>
        public IEnumerable<TReturn> Query<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh, TReturn>(string sql, Func<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.Query(sql, map, param, transaction, buffered, splitOn, commandTimeout, commandType);

        /// <summary>
        /// Perform a multi-mapping query with an arbitrary number of input types. This returns a single type, combined from the raw types via <paramref name="map"/>.
        /// </summary>
        /// <typeparam name="TReturn"> The combined type to return. </typeparam>
        /// <param name="sql">            The SQL to execute for this query. </param>
        /// <param name="types">          Array of types in the recordset. </param>
        /// <param name="map">            The function to map row types to the return type. </param>
        /// <param name="param">          The parameters to use for this query. </param>
        /// <param name="transaction">    The transaction to use for this query. </param>
        /// <param name="buffered">       Whether to buffer the results in memory. </param>
        /// <param name="splitOn">        The field we should split and read the second object from (default: "Id"). </param>
        /// <param name="commandTimeout"> Number of seconds before command execution timeout. </param>
        /// <param name="commandType">    Is it a stored proc or a batch? </param>
        /// <returns> An enumerable of <typeparamref name="TReturn"/>. </returns>
        public IEnumerable<TReturn> Query<TReturn>(string sql, Type[] types, Func<object[], TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.Query(sql, types, map, param, transaction, buffered, splitOn, commandTimeout, commandType);

        #endregion Dapper Functions

        #region Async Dapper  Functions

        /// <summary>
        /// Execute a query asynchronously using Task.
        /// </summary>
        /// <param name="sql">The SQL to execute for the query.</param>
        /// <param name="param">The parameters to pass, if any.</param>
        /// <param name="transaction">The transaction to use, if any.</param>
        /// <param name="commandTimeout">The command timeout (in seconds).</param>
        /// <param name="commandType">The type of command to execute.</param>
        /// <remarks>Note: each row can be accessed via "dynamic", or by casting to an IDictionary&lt;string,object&gt;</remarks>
        public Task<IEnumerable<dynamic>> QueryAsync(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.QueryAsync(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute a query asynchronously using Task.
        /// </summary>
        /// <typeparam name="T">The type of results to return.</typeparam>
        /// <param name="sql">The SQL to execute for the query.</param>
        /// <param name="param">The parameters to pass, if any.</param>
        /// <param name="transaction">The transaction to use, if any.</param>
        /// <param name="commandTimeout">The command timeout (in seconds).</param>
        /// <param name="commandType">The type of command to execute.</param>
        /// <returns>
        /// A sequence of data of <typeparamref name="T"/>; if a basic type (int, string, etc) is queried then the data from the first column in assumed, otherwise an instance is
        /// created per row, and a direct column-name===member-name mapping is assumed (case insensitive).
        /// </returns>
        public Task<IEnumerable<T>> QueryAsync<T>(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
             DbExec.QueryAsync<T>(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute a single-row query asynchronously using Task.
        /// </summary>
        /// <typeparam name="T">The type of result to return.</typeparam>
        /// <param name="sql">The SQL to execute for the query.</param>
        /// <param name="param">The parameters to pass, if any.</param>
        /// <param name="transaction">The transaction to use, if any.</param>
        /// <param name="commandTimeout">The command timeout (in seconds).</param>
        /// <param name="commandType">The type of command to execute.</param>
        public Task<T> QueryFirstAsync<T>(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
             DbExec.QueryFirstAsync<T>(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute a single-row query asynchronously using Task.
        /// </summary>
        /// <typeparam name="T">The type of result to return.</typeparam>
        /// <param name="sql">The SQL to execute for the query.</param>
        /// <param name="param">The parameters to pass, if any.</param>
        /// <param name="transaction">The transaction to use, if any.</param>
        /// <param name="commandTimeout">The command timeout (in seconds).</param>
        /// <param name="commandType">The type of command to execute.</param>
        public Task<T> QueryFirstOrDefaultAsync<T>(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.QueryFirstOrDefaultAsync<T>(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute a single-row query asynchronously using Task.
        /// </summary>
        /// <typeparam name="T">The type of result to return.</typeparam>
        /// <param name="sql">The SQL to execute for the query.</param>
        /// <param name="param">The parameters to pass, if any.</param>
        /// <param name="transaction">The transaction to use, if any.</param>
        /// <param name="commandTimeout">The command timeout (in seconds).</param>
        /// <param name="commandType">The type of command to execute.</param>
        public Task<T> QuerySingleAsync<T>(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.QuerySingleAsync<T>(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute a single-row query asynchronously using Task.
        /// </summary>
        /// <typeparam name="T">The type to return.</typeparam>
        /// <param name="sql">The SQL to execute for the query.</param>
        /// <param name="param">The parameters to pass, if any.</param>
        /// <param name="transaction">The transaction to use, if any.</param>
        /// <param name="commandTimeout">The command timeout (in seconds).</param>
        /// <param name="commandType">The type of command to execute.</param>
        public Task<T> QuerySingleOrDefaultAsync<T>(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.QuerySingleOrDefaultAsync<T>(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute a single-row query asynchronously using Task.
        /// </summary>
        /// <param name="sql">The SQL to execute for the query.</param>
        /// <param name="param">The parameters to pass, if any.</param>
        /// <param name="transaction">The transaction to use, if any.</param>
        /// <param name="commandTimeout">The command timeout (in seconds).</param>
        /// <param name="commandType">The type of command to execute.</param>
        public Task<dynamic> QueryFirstAsync(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.QueryFirstAsync(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute a single-row query asynchronously using Task.
        /// </summary>
        /// <param name="sql">The SQL to execute for the query.</param>
        /// <param name="param">The parameters to pass, if any.</param>
        /// <param name="transaction">The transaction to use, if any.</param>
        /// <param name="commandTimeout">The command timeout (in seconds).</param>
        /// <param name="commandType">The type of command to execute.</param>
        public Task<dynamic> QueryFirstOrDefaultAsync(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.QueryFirstOrDefaultAsync(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute a single-row query asynchronously using Task.
        /// </summary>
        /// <param name="sql">The SQL to execute for the query.</param>
        /// <param name="param">The parameters to pass, if any.</param>
        /// <param name="transaction">The transaction to use, if any.</param>
        /// <param name="commandTimeout">The command timeout (in seconds).</param>
        /// <param name="commandType">The type of command to execute.</param>
        public Task<dynamic> QuerySingleAsync(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.QuerySingleAsync(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute a single-row query asynchronously using Task.
        /// </summary>
        /// <param name="sql">The SQL to execute for the query.</param>
        /// <param name="param">The parameters to pass, if any.</param>
        /// <param name="transaction">The transaction to use, if any.</param>
        /// <param name="commandTimeout">The command timeout (in seconds).</param>
        /// <param name="commandType">The type of command to execute.</param>
        public Task<dynamic> QuerySingleOrDefaultAsync(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.QuerySingleOrDefault(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute a query asynchronously using Task.
        /// </summary>
        /// <param name="type">The type to return.</param>
        /// <param name="sql">The SQL to execute for the query.</param>
        /// <param name="param">The parameters to pass, if any.</param>
        /// <param name="transaction">The transaction to use, if any.</param>
        /// <param name="commandTimeout">The command timeout (in seconds).</param>
        /// <param name="commandType">The type of command to execute.</param>
        /// <exception cref="ArgumentNullException"><paramref name="type"/> is <c>null</c>.</exception>
        public Task<IEnumerable<object>> QueryAsync(Type type, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
             DbExec.QueryAsync(type, sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute a single-row query asynchronously using Task.
        /// </summary>
        /// <param name="type">The type to return.</param>
        /// <param name="sql">The SQL to execute for the query.</param>
        /// <param name="param">The parameters to pass, if any.</param>
        /// <param name="transaction">The transaction to use, if any.</param>
        /// <param name="commandTimeout">The command timeout (in seconds).</param>
        /// <param name="commandType">The type of command to execute.</param>
        /// <exception cref="ArgumentNullException"><paramref name="type"/> is <c>null</c>.</exception>
        public Task<object> QueryFirstAsync(Type type, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.QueryFirstAsync(type, sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute a single-row query asynchronously using Task.
        /// </summary>
        /// <param name="type">The type to return.</param>
        /// <param name="sql">The SQL to execute for the query.</param>
        /// <param name="param">The parameters to pass, if any.</param>
        /// <param name="transaction">The transaction to use, if any.</param>
        /// <param name="commandTimeout">The command timeout (in seconds).</param>
        /// <param name="commandType">The type of command to execute.</param>
        /// <exception cref="ArgumentNullException"><paramref name="type"/> is <c>null</c>.</exception>
        public Task<object> QueryFirstOrDefaultAsync(Type type, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.QueryFirstOrDefaultAsync(type, sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute a single-row query asynchronously using Task.
        /// </summary>
        /// <param name="type">The type to return.</param>
        /// <param name="sql">The SQL to execute for the query.</param>
        /// <param name="param">The parameters to pass, if any.</param>
        /// <param name="transaction">The transaction to use, if any.</param>
        /// <param name="commandTimeout">The command timeout (in seconds).</param>
        /// <param name="commandType">The type of command to execute.</param>
        /// <exception cref="ArgumentNullException"><paramref name="type"/> is <c>null</c>.</exception>
        public Task<object> QuerySingleAsync(Type type, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.QuerySingleAsync(type, sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute a single-row query asynchronously using Task.
        /// </summary>
        /// <param name="type">The type to return.</param>
        /// <param name="sql">The SQL to execute for the query.</param>
        /// <param name="param">The parameters to pass, if any.</param>
        /// <param name="transaction">The transaction to use, if any.</param>
        /// <param name="commandTimeout">The command timeout (in seconds).</param>
        /// <param name="commandType">The type of command to execute.</param>
        /// <exception cref="ArgumentNullException"><paramref name="type"/> is <c>null</c>.</exception>
        public Task<object> QuerySingleOrDefaultAsync(Type type, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.QuerySingleOrDefaultAsync(type, sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute a command asynchronously using Task.
        /// </summary>
        /// <param name="sql">The SQL to execute for this query.</param>
        /// <param name="param">The parameters to use for this query.</param>
        /// <param name="transaction">The transaction to use for this query.</param>
        /// <param name="commandTimeout">Number of seconds before command execution timeout.</param>
        /// <param name="commandType">Is it a stored proc or a batch?</param>
        /// <returns>The number of rows affected.</returns>
        public Task<int> ExecuteAsync(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
             DbExec.ExecuteAsync(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Perform a asynchronous multi-mapping query with 2 input types.
        /// This returns a single type, combined from the raw types via <paramref name="map"/>.
        /// </summary>
        /// <typeparam name="TFirst">The first type in the recordset.</typeparam>
        /// <typeparam name="TSecond">The second type in the recordset.</typeparam>
        /// <typeparam name="TReturn">The combined type to return.</typeparam>
        /// <param name="sql">The SQL to execute for this query.</param>
        /// <param name="map">The function to map row types to the return type.</param>
        /// <param name="param">The parameters to use for this query.</param>
        /// <param name="transaction">The transaction to use for this query.</param>
        /// <param name="buffered">Whether to buffer the results in memory.</param>
        /// <param name="splitOn">The field we should split and read the second object from (default: "Id").</param>
        /// <param name="commandTimeout">Number of seconds before command execution timeout.</param>
        /// <param name="commandType">Is it a stored proc or a batch?</param>
        /// <returns>An enumerable of <typeparamref name="TReturn"/>.</returns>
        public Task<IEnumerable<TReturn>> QueryAsync<TFirst, TSecond, TReturn>(string sql, Func<TFirst, TSecond, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.QueryAsync(sql, map, param, transaction, buffered, splitOn, commandTimeout, commandType);

        /// <summary>
        /// Perform a asynchronous multi-mapping query with 3 input types.
        /// This returns a single type, combined from the raw types via <paramref name="map"/>.
        /// </summary>
        /// <typeparam name="TFirst">The first type in the recordset.</typeparam>
        /// <typeparam name="TSecond">The second type in the recordset.</typeparam>
        /// <typeparam name="TThird">The third type in the recordset.</typeparam>
        /// <typeparam name="TReturn">The combined type to return.</typeparam>
        /// <param name="sql">The SQL to execute for this query.</param>
        /// <param name="map">The function to map row types to the return type.</param>
        /// <param name="param">The parameters to use for this query.</param>
        /// <param name="transaction">The transaction to use for this query.</param>
        /// <param name="buffered">Whether to buffer the results in memory.</param>
        /// <param name="splitOn">The field we should split and read the second object from (default: "Id").</param>
        /// <param name="commandTimeout">Number of seconds before command execution timeout.</param>
        /// <param name="commandType">Is it a stored proc or a batch?</param>
        /// <returns>An enumerable of <typeparamref name="TReturn"/>.</returns>
        public Task<IEnumerable<TReturn>> QueryAsync<TFirst, TSecond, TThird, TReturn>(string sql, Func<TFirst, TSecond, TThird, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.QueryAsync(sql, map, param, transaction, buffered, splitOn, commandTimeout, commandType);

        /// <summary>
        /// Perform a asynchronous multi-mapping query with 4 input types.
        /// This returns a single type, combined from the raw types via <paramref name="map"/>.
        /// </summary>
        /// <typeparam name="TFirst">The first type in the recordset.</typeparam>
        /// <typeparam name="TSecond">The second type in the recordset.</typeparam>
        /// <typeparam name="TThird">The third type in the recordset.</typeparam>
        /// <typeparam name="TFourth">The fourth type in the recordset.</typeparam>
        /// <typeparam name="TReturn">The combined type to return.</typeparam>
        /// <param name="sql">The SQL to execute for this query.</param>
        /// <param name="map">The function to map row types to the return type.</param>
        /// <param name="param">The parameters to use for this query.</param>
        /// <param name="transaction">The transaction to use for this query.</param>
        /// <param name="buffered">Whether to buffer the results in memory.</param>
        /// <param name="splitOn">The field we should split and read the second object from (default: "Id").</param>
        /// <param name="commandTimeout">Number of seconds before command execution timeout.</param>
        /// <param name="commandType">Is it a stored proc or a batch?</param>
        /// <returns>An enumerable of <typeparamref name="TReturn"/>.</returns>
        public Task<IEnumerable<TReturn>> QueryAsync<TFirst, TSecond, TThird, TFourth, TReturn>(string sql, Func<TFirst, TSecond, TThird, TFourth, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.QueryAsync(sql, map, param, transaction, buffered, splitOn, commandTimeout, commandType);

        /// <summary>
        /// Perform a asynchronous multi-mapping query with 5 input types.
        /// This returns a single type, combined from the raw types via <paramref name="map"/>.
        /// </summary>
        /// <typeparam name="TFirst">The first type in the recordset.</typeparam>
        /// <typeparam name="TSecond">The second type in the recordset.</typeparam>
        /// <typeparam name="TThird">The third type in the recordset.</typeparam>
        /// <typeparam name="TFourth">The fourth type in the recordset.</typeparam>
        /// <typeparam name="TFifth">The fifth type in the recordset.</typeparam>
        /// <typeparam name="TReturn">The combined type to return.</typeparam>
        /// <param name="sql">The SQL to execute for this query.</param>
        /// <param name="map">The function to map row types to the return type.</param>
        /// <param name="param">The parameters to use for this query.</param>
        /// <param name="transaction">The transaction to use for this query.</param>
        /// <param name="buffered">Whether to buffer the results in memory.</param>
        /// <param name="splitOn">The field we should split and read the second object from (default: "Id").</param>
        /// <param name="commandTimeout">Number of seconds before command execution timeout.</param>
        /// <param name="commandType">Is it a stored proc or a batch?</param>
        /// <returns>An enumerable of <typeparamref name="TReturn"/>.</returns>
        public Task<IEnumerable<TReturn>> QueryAsync<TFirst, TSecond, TThird, TFourth, TFifth, TReturn>(string sql, Func<TFirst, TSecond, TThird, TFourth, TFifth, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.QueryAsync(sql, map, param, transaction, buffered, splitOn, commandTimeout, commandType);

        /// <summary>
        /// Perform a asynchronous multi-mapping query with 6 input types.
        /// This returns a single type, combined from the raw types via <paramref name="map"/>.
        /// </summary>
        /// <typeparam name="TFirst">The first type in the recordset.</typeparam>
        /// <typeparam name="TSecond">The second type in the recordset.</typeparam>
        /// <typeparam name="TThird">The third type in the recordset.</typeparam>
        /// <typeparam name="TFourth">The fourth type in the recordset.</typeparam>
        /// <typeparam name="TFifth">The fifth type in the recordset.</typeparam>
        /// <typeparam name="TSixth">The sixth type in the recordset.</typeparam>
        /// <typeparam name="TReturn">The combined type to return.</typeparam>
        /// <param name="sql">The SQL to execute for this query.</param>
        /// <param name="map">The function to map row types to the return type.</param>
        /// <param name="param">The parameters to use for this query.</param>
        /// <param name="transaction">The transaction to use for this query.</param>
        /// <param name="buffered">Whether to buffer the results in memory.</param>
        /// <param name="splitOn">The field we should split and read the second object from (default: "Id").</param>
        /// <param name="commandTimeout">Number of seconds before command execution timeout.</param>
        /// <param name="commandType">Is it a stored proc or a batch?</param>
        /// <returns>An enumerable of <typeparamref name="TReturn"/>.</returns>
        public Task<IEnumerable<TReturn>> QueryAsync<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TReturn>(string sql, Func<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.QueryAsync(sql, map, param, transaction, buffered, splitOn, commandTimeout, commandType);

        /// <summary>
        /// Perform a asynchronous multi-mapping query with 7 input types.
        /// This returns a single type, combined from the raw types via <paramref name="map"/>.
        /// </summary>
        /// <typeparam name="TFirst">The first type in the recordset.</typeparam>
        /// <typeparam name="TSecond">The second type in the recordset.</typeparam>
        /// <typeparam name="TThird">The third type in the recordset.</typeparam>
        /// <typeparam name="TFourth">The fourth type in the recordset.</typeparam>
        /// <typeparam name="TFifth">The fifth type in the recordset.</typeparam>
        /// <typeparam name="TSixth">The sixth type in the recordset.</typeparam>
        /// <typeparam name="TSeventh">The seventh type in the recordset.</typeparam>
        /// <typeparam name="TReturn">The combined type to return.</typeparam>
        /// <param name="sql">The SQL to execute for this query.</param>
        /// <param name="map">The function to map row types to the return type.</param>
        /// <param name="param">The parameters to use for this query.</param>
        /// <param name="transaction">The transaction to use for this query.</param>
        /// <param name="buffered">Whether to buffer the results in memory.</param>
        /// <param name="splitOn">The field we should split and read the second object from (default: "Id").</param>
        /// <param name="commandTimeout">Number of seconds before command execution timeout.</param>
        /// <param name="commandType">Is it a stored proc or a batch?</param>
        /// <returns>An enumerable of <typeparamref name="TReturn"/>.</returns>
        public Task<IEnumerable<TReturn>> QueryAsync<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh, TReturn>(string sql, Func<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh, TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.QueryAsync(sql, map, param, transaction, buffered, splitOn, commandTimeout, commandType);

        /// <summary>
        /// Perform a asynchronous multi-mapping query with an arbitrary number of input types.
        /// This returns a single type, combined from the raw types via <paramref name="map"/>.
        /// </summary>
        /// <typeparam name="TReturn">The combined type to return.</typeparam
        /// <param name="sql">The SQL to execute for this query.</param>
        /// <param name="types">Array of types in the recordset.</param>
        /// <param name="map">The function to map row types to the return type.</param>
        /// <param name="param">The parameters to use for this query.</param>
        /// <param name="transaction">The transaction to use for this query.</param>
        /// <param name="buffered">Whether to buffer the results in memory.</param>
        /// <param name="splitOn">The field we should split and read the second object from (default: "Id").</param>
        /// <param name="commandTimeout">Number of seconds before command execution timeout.</param>
        /// <param name="commandType">Is it a stored proc or a batch?</param>
        /// <returns>An enumerable of <typeparamref name="TReturn"/>.</returns>
        public Task<IEnumerable<TReturn>> QueryAsync<TReturn>(string sql, Type[] types, Func<object[], TReturn> map, object param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.QueryAsync(sql, types, map, param, transaction, buffered, splitOn, commandTimeout, commandType);

        /// <summary>
        /// Execute a command that returns multiple result sets, and access each in turn.
        /// </summary>
        /// <param name="sql">The SQL to execute for this query.</param>
        /// <param name="param">The parameters to use for this query.</param>
        /// <param name="transaction">The transaction to use for this query.</param>
        /// <param name="commandTimeout">Number of seconds before command execution timeout.</param>
        /// <param name="commandType">Is it a stored proc or a batch?</param>
        public Task<IDbGridReader> QueryMultipleAsync(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.QueryMultipleAsync(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute parameterized SQL and return an <see cref="IDataReader"/>.
        /// </summary>
        /// <param name="sql">The SQL to execute.</param>
        /// <param name="param">The parameters to use for this command.</param>
        /// <param name="transaction">The transaction to use for this command.</param>
        /// <param name="commandTimeout">Number of seconds before command execution timeout.</param>
        /// <param name="commandType">Is it a stored proc or a batch?</param>
        /// <returns>An <see cref="IDataReader"/> that can be used to iterate over the results of the SQL query.</returns>
        /// <remarks>
        /// This is typically used when the results of a query are not processed by Dapper, for example, used to fill a <see cref="DataTable"/>
        /// or <see cref="T:DataSet"/>.
        /// </remarks>
        /// <example>
        /// <code>
        /// <![CDATA[
        /// DataTable table = new DataTable("MyTable") =>
        /// using (var reader = ExecuteReader(cnn, sql, param))
        /// {
        ///     table.Load(reader) =>
        /// }
        /// ]]>
        /// </code>
        /// </example>
        public Task<IDataReader> ExecuteReaderAsync(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
             DbExec.ExecuteReaderAsync(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute parameterized SQL that selects a single value.
        /// </summary>
        /// <param name="sql">The SQL to execute.</param>
        /// <param name="param">The parameters to use for this command.</param>
        /// <param name="transaction">The transaction to use for this command.</param>
        /// <param name="commandTimeout">Number of seconds before command execution timeout.</param>
        /// <param name="commandType">Is it a stored proc or a batch?</param>
        /// <returns>The first cell returned, as <see cref="object"/>.</returns>
        public Task<object> ExecuteScalarAsync(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.ExecuteScalarAsync(sql, param, transaction, commandTimeout, commandType);

        /// <summary>
        /// Execute parameterized SQL that selects a single value.
        /// </summary>
        /// <typeparam name="T">The type to return.</typeparam>
        /// <param name="sql">The SQL to execute.</param>
        /// <param name="param">The parameters to use for this command.</param>
        /// <param name="transaction">The transaction to use for this command.</param>
        /// <param name="commandTimeout">Number of seconds before command execution timeout.</param>
        /// <param name="commandType">Is it a stored proc or a batch?</param>
        /// <returns>The first cell returned, as <typeparamref name="T"/>.</returns>
        public Task<T> ExecuteScalarAsync<T>(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) =>
            DbExec.ExecuteScalarAsync<T>(sql, param, transaction, commandTimeout, commandType);

        #endregion Async Dapper  Functions
    }
}

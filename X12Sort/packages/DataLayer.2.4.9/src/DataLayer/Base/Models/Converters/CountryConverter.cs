using Newtonsoft.Json;
using System;

namespace DataLayer.Base.Models.Converters
{
    /// <summary>
    /// The logic needed to convert a Country to and from JSON
    /// </summary>
    internal class CountryConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(Country);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return Country.FromString(serializer.Deserialize(reader) as string);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            serializer.Serialize(writer, ((Country)value).Code);
        }
    }
}

using Newtonsoft.Json;
using System;
using DataLayer.Base.Enumerations;
using DataLayer.StaticUtilities.Extensions;

namespace DataLayer.Base.Models.Converters
{
    /// <summary>
    /// The logic to convert Sex to and from JSON
    /// </summary>
    internal class SexConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(Sex);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return FromString(serializer.Deserialize(reader) as string);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            serializer.Serialize(writer, ((Sex)value).GetLabel());
        }

        public static Sex FromString(string value)
        {
            if (string.IsNullOrEmpty(value)) return Sex.Unknown;
            switch(value.ToLower().Trim())
            {
                case "m": return Sex.Male;
                case "f": return Sex.Female;
                case "o": return Sex.Other;
                case "male":return Sex.Male;
                case "female": return Sex.Female;
                case "other": return Sex.Other;
                default: return Sex.Unknown;
            }
        }
    }
}

using DataLayer.Base.Enumerations;
using Newtonsoft.Json;
using NLog;
using System;

namespace DataLayer.Base.Models
{
    /// <summary>
    /// Bottom Layer of all Database objects.
    /// </summary>
    /// <remarks>
    /// The Goal for this object is to have a common object across all data objects in the data layer
    /// </remarks>
    public abstract class BaseDataObject : IBaseDataObject
    {
        /// <summary>
        /// The primary key of the database record represented by this object
        /// </summary>
        public virtual string DatabaseKey { get; set; }

        /// <summary>
        /// The source of the data object
        /// </summary>
        public virtual DataSource Source { get => DataSource.Unknown; }

        /// <summary>
        /// Constructor for <c> BaseDataObject </c>, should be called by inheriting classes
        /// </summary>
        protected BaseDataObject()
        {
        }

        /// <summary>
        /// Optional function that will be called before update/insert to set any properties, such as update date and users
        /// </summary>
        public virtual void PrepForWrite() { return; }

        /// <summary>
        /// Converts this object to JSON string. XML is stupid and I don't want to use it
        /// </summary>
        public virtual string ToJson()
        {
            try
            {
                string json = JsonConvert.SerializeObject(this);
                return json;
            }
            catch (NullReferenceException e)
            {
                Logger logger = LogManager.GetCurrentClassLogger();
                logger.Warn("Failed to convert object to JSON, Error: " + e.Message);
                return null;
            }
        }
    }
}

using System;

namespace DataLayer.Base.Models
{
    public interface IAdmission : IStartEnd
    {
        /// <summary>
        /// Actual admission date
        /// </summary>
        public DateTime? AdmissionDate { get; set; }
        /// <summary>
        /// Source of admission i.e. ER, Referral, etc.
        /// </summary>
        public string AdmissionSource { get; set; }
        /// <summary>
        /// Type of admission i.e. Medical inpatient, Inpatient surgery, etc.
        /// </summary>
        public string AdmissionType { get; set; }
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.Base.Models
{
    /// <summary>
    /// An interface for a coordinate (latitude and longitude) combination.
    /// </summary>
    public interface ICoordiantes
    {
        /// <summary>
        /// 
        /// </summary>
        decimal? Longitude { get; set; }

        /// <summary>
        /// 
        /// </summary>
        decimal? Latitude { get; set; }
    }
}

using System;
using System.Collections.Generic;
using DataLayer.Base.Enumerations;

namespace DataLayer.Base.Models
{
    public interface IClaim : IStartEnd
    {
        /// <summary>
        /// Type of claim
        /// </summary>
        ClaimType Type { get; set; }

        /// <summary>
        /// Number representing claim in internal system
        /// </summary>
        string ClaimNumber { get; set; }

        /// <summary>
        /// Approval/denial status of claim
        /// </summary>
        ClaimStatus Status { get; set; }

        /// <summary>
        /// Number representing member in internal system
        /// </summary>
        string MemberNumber { get; set; }

        /// <summary>
        /// Date that the claim was received by the company
        /// </summary>
        DateTime? DateReceived { get; set; }

        /// <summary>
        /// Date that the claim was digitally entered into the system
        /// </summary>
        DateTime? DateEntered { get; set; }

        /// <summary>
        /// Date that the claim was paid
        /// </summary>
        DateTime? DatePaid { get; set; }

        /// <summary>
        /// Dollar ammount that was billed to the insurance company
        /// </summary>
        decimal AmountBilled { get; set; }

        /// <summary>
        /// Dollar amount allowed to be billed from servicing provider
        /// </summary>
        decimal AmountAllowed { get; set; }

        /// <summary>
        /// Dollar amount that was paid out by the insurance company
        /// </summary>
        decimal AmountPaid { get; set; }

        /// <summary>
        /// Dollar amount that was paid for by a third party
        /// </summary>
        decimal AmountPaidByThirdParty { get; set; }

        /// <summary>
        /// Dollar amount owed by member
        /// </summary>
        decimal AmountOwed { get; set; }

        /// <summary>
        /// List of service lines tied to claim
        /// </summary>
        List<IServiceLine> ServiceLines { get; set; }
    }
}

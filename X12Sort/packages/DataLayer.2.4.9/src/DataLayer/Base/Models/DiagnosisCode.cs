using DataLayer.Base.Enumerations;

namespace DataLayer.Base.Models
{
    /// <summary>
    /// Claim diagnosis code
    /// </summary>
    public class DiagnosisCode : BaseDataObject, ICode
    {
        /// <summary>
        /// Type of claim diagnosis code
        /// </summary>
        public DiagnosisCodeType Type { get; set; }

        /// <summary>
        /// Actual diagnosis code value
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Short description of diagnosis code
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Extended description of diagnosis code
        /// </summary>
        public string ExtendedDescription { get; set; }
}
}

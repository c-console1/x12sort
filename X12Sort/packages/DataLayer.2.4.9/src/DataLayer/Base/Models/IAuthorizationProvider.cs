namespace DataLayer.Base.Models
{
    public interface IAuthorizationProvider
    {
        /// <summary>
        /// Provider identifier
        /// </summary>
        public string ProviderID { get; set; }
        /// <summary>
        /// Name of provider
        /// </summary>
        public string ProviderName { get; set; }
        /// <summary>
        /// External id if present
        /// </summary>
        public string ProviderExternalNumber { get; set; }
        /// <summary>
        /// NPI
        /// </summary>
        public string ProviderNPI { get; set; }
        /// <summary>
        /// TIN
        /// </summary>
        public string ProviderTaxonomy { get; set; }
    }
}

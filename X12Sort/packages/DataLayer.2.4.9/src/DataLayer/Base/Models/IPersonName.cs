using System;
using System.Collections.Generic;

namespace DataLayer.Base.Models
{
    public interface IPersonName
    {
        /// <summary>
        /// A list of name prefixes such as 'Dr.'
        /// </summary>
        IEnumerable<string> Prefixes { get; set; }

        string FirstName { get; set; }

        string MiddleName { get; set; }

        string LastName { get; set; }

        /// <summary>
        /// A list of name suffixes such as 'Jr.'
        /// </summary>
        IEnumerable<string> Suffixes { get; set; }
    }
}

namespace DataLayer.Base.Models
{
    /// <summary>
    /// This is an interface for a standard postal address
    /// </summary>
    /// <remarks>
    /// Should work for international address, but your results may vary
    /// </remarks>
    public interface IAddress
    {
        /// <summary>
        /// A possible name for the Address
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Address line 1
        /// </summary>
        string Address1 { get; set; }

        /// <summary>
        /// Address line 2
        /// </summary>
        string Address2 { get; set; }

        /// <summary>
        /// Full name of the City
        /// </summary>
        string City { get; set; }

        /// <summary>
        /// Full name of the State
        /// </summary>
        USState State { get; set; }

        /// <summary>
        /// The full name of the county
        /// </summary>
        string County { get; set; }

        /// <summary>
        /// The Country for the address
        /// </summary>
        Country Country { get; set; }

        /// <summary>
        /// The string representation of a zip code, format not specified
        /// </summary>
        ZipCode ZipCode { get; set; }
    }
}

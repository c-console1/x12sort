namespace DataLayer.Base.Models
{
    public interface ICode
    {
        string Code { get; set; }

        string Description { get; set; }
    }
}

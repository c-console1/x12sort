namespace DataLayer.Base.Models
{
    //TODO: Do we need this class? Since we have the IAuthorization and it is implemented differrently in each "repository" - BWS
    //TODO: I am not sure what this would end up being since there is not really a universal Auth object - BWS
    //public class Authorization : IAuthorization
    //{

    //    /// <summary>
    //    /// Where this data was pulled from
    //    /// </summary>
    //    public DataSources WherePulled { get; set; } = DataSources.Unknown;

    //    /// <summary>
    //    /// Authorization ID
    //    /// </summary>
    //    public string AuthorizationID { get; set; }

    //    /// <summary>
    //    /// Authorization status
    //    /// </summary>
    //    public AuthorizationStatuses AuthorizationStatus { get; set; } = AuthorizationStatuses.Unknown;

    //    /// <summary>
    //    /// Date requested
    //    /// </summary>
    //    public DateTime? DateRequested { get; set; }

    //    /// <summary>
    //    /// Date finalized
    //    /// </summary>
    //    public DateTime? DateFinalized { get; set; }

    //    /// <summary>
    //    /// Authorizing practitioner
    //    /// </summary>
    //    public string AuthorizingPractitionerID { get; set; }

    //}
}

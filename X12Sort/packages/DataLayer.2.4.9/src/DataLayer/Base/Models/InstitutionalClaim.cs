using System;
using System.Linq;
using System.Collections.Generic;
using DataLayer.Base.Enumerations;
using DataLayer.StaticUtilities.Extensions;

namespace DataLayer.Base.Models
{
    public class InstitutionalClaim : BaseDataObject, IClaim
    {
        protected List<InstitutionalServiceLine> _serviceLines;

        #region Implements IClaim
        /// <summary>
        /// Type of claim
        /// </summary>
        public ClaimType Type { get; set; }

        /// <summary>
        /// Number representing claim in internal system
        /// </summary>
        public string ClaimNumber { get; set; }

        /// <summary>
        /// Approval/denial status of claim
        /// </summary>
        public ClaimStatus Status { get; set; }

        /// <summary>
        /// Number representing member in internal system
        /// </summary>
        public string MemberNumber { get; set; }

        /// <summary>
        /// Date that the claim was received by the company
        /// </summary>
        public DateTime? DateReceived { get; set; }

        /// <summary>
        /// Date that the claim was digitally entered into the system
        /// </summary>
        public DateTime? DateEntered { get; set; }

        /// <summary>
        /// Date that the claim was paid
        /// </summary>
        public DateTime? DatePaid { get; set; }

        /// <summary>
        /// Dollar ammount that was billed to the insurance company
        /// </summary>
        public decimal AmountBilled { get; set; }

        /// <summary>
        /// Dollar amount allowed to be billed from servicing provider
        /// </summary>
        public decimal AmountAllowed { get; set; }

        /// <summary>
        /// Dollar amount that was paid out by the insurance company
        /// </summary>
        public decimal AmountPaid { get; set; }

        /// <summary>
        /// Dollar amount that was paid for by a third party
        /// </summary>
        public decimal AmountPaidByThirdParty { get; set; }

        /// <summary>
        /// Dollar amount owed by member
        /// </summary>
        public decimal AmountOwed { get; set; }
        #endregion

        #region Implements IStartEnd
        /// <summary>
        /// Effective date of claim
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// End date of claim
        /// </summary>
        public DateTime? EndDate { get; set; }
        #endregion

        /// <summary>
        /// Dates of service for claim using StartDate and EndDate
        /// </summary>
        public StartEndRange DatesOfService { get { return this.GetRange(); } }

        /// <summary>
        /// Principal diagnosis code used to justify the claim
        /// </summary>
        public DiagnosisCode PrincipalDiagnosis { get { return DiagnosisCodes == default ? default : DiagnosisCodes.FirstOrDefault(); } }

        /// <summary>
        /// List of diagnosis codes
        /// </summary>
        public List<DiagnosisCode> DiagnosisCodes { get; set; }

        /// <summary>
        /// List of service lines tied to claim
        /// </summary>
        public List<InstitutionalServiceLine> ServiceLines
        {
            get { return _serviceLines; }
            set { _serviceLines = value; }
        }

        /// <summary>
        /// List of service lines tied to claim
        /// </summary>
        List<IServiceLine> IClaim.ServiceLines
        {
            get { return _serviceLines.Cast<IServiceLine>().ToList(); }
            set { _serviceLines = value.Cast<InstitutionalServiceLine>().ToList(); }
        }
    }
}

using DataLayer.Base.Models.Converters;
using DataLayer.StaticUtilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace DataLayer.Base.Models
{
    /// <summary>
    /// US State & Canada Providence value
    /// </summary>
    /// <remarks>
    /// <para>
    /// Using a struct because this is conceptually an immutable value type.
    /// </para>
    /// <para>
    /// Only allows assigning valid states, either state code or state name. If given an invalid value, the state will be empty.
    /// </para>
    /// <para>
    /// TODO: rename this class in v2
    /// </para>
    /// </remarks>
    /// <example>
    /// This is how you can assign the values to object
    /// <code>
    /// <![CDATA[
    /// USState ohio = "ohio";
    /// USState delaware = "DE";
    /// ]]>
    /// </code>
    /// The following can be used to get the value
    /// <code>
    /// <![CDATA[
    /// USState state = "Michigan";
    /// string stateString = state;
    /// Assert.That(stateString, Is.EqualTo("MI"));
    /// Assert.That(state.ToString(), Is.EqualTo("Michigan"));
    /// Assert.That(state.Name, Is.EqualTo("Michigan"));
    /// Assert.That(state.Code, Is.EqualTo("MI"));
    /// }
    /// ]]>
    /// </code>
    /// </example>
    [JsonConverter(typeof(USStateConverter))]
    public struct USState : IEquatable<USState>
    {
        public static readonly HashSet<string> ProvidanceCodes = new HashSet<string>()        {
            "AB","BC","MB", "NB", "NL", "NT", "NS", "NU", "ON", "PE", "QC", "SK", "YT"
        };

        /// <summary>
        /// The two letter state code
        /// </summary>
        public string Code { get; }

        /// <summary>
        /// The full name of the state
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Basic Constructor
        /// </summary>
        /// <param name="value">Should be a state name of code</param>
        public USState(string value)
        {
            Code = "";
            Name = "";
            if (!string.IsNullOrEmpty(value))
            {
                value = value.Trim();
                if (value.Length > 2)
                {
                    Code = AddressManipulation.StateVerboseToAbbreviation(value);
                    if (!string.IsNullOrEmpty(Code))
                    {
                        Name = value.ToTitleCase();
                    }
                }
                else
                {
                    Name = AddressManipulation.StateAbbreviationToVerbose(value).ToTitleCase();
                    if (!string.IsNullOrEmpty(Name))
                    {
                        Code = value;
                    }
                }
            }
        }

        /// <summary>
        /// Convert a string to state object
        /// </summary>
        /// <param name="value">The string that should be a state name</param>
        public static USState FromString(string value)
        {
            return new USState(value);
        }

        /// <summary>
        /// Convert a State to String, uses `this.Code`
        /// </summary>
        /// <param name="value">The state object</param>
        static public implicit operator string(USState value)
        {
            return value.Code;
        }

        /// <summary>
        /// Convert a string to state object
        /// </summary>
        /// <param name="value">The string that should be a state name</param>
        public static implicit operator USState(string value)
        {
            return FromString(value);
        }

        /// <summary>
        /// Compares the State's names for inequality
        /// </summary>
        /// <param name="left">State object</param>
        /// <param name="right">State to compare</param>
        /// <returns>Returns true if they are not equal strings</returns>
        public static bool operator !=(USState left, USState right)
        {
            return !left.Equals(right);
        }

        /// <summary>
        /// Compares the State's names for equality
        /// </summary>
        /// <param name="left">State object</param>
        /// <param name="right">State to compare</param>
        /// <returns>Returns true if they are equal strings</returns>
        public static bool operator ==(USState left, USState right)
        {
            return left.Equals(right);
        }

        /// <summary>
        /// Compares the State name to the object string value
        /// </summary>
        /// <param name="obj">Other value to compare</param>
        /// <returns>Returns true if they are equal strings</returns>
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            return Name == obj.ToString();
        }

        /// <summary>
        /// Get the hash code from the state's name
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }

        /// <summary>
        /// The full name of the state
        /// </summary>
        /// <returns>`this.Name`</returns>
        public override string ToString()
        {
            return Name;
        }

        /// <summary>
        /// Compares the State name to the state's name
        /// </summary>
        /// <param name="other">Other value to compare</param>
        /// <returns>Returns true if they are equal strings</returns>
        public bool Equals(USState other)
        {
            return Code == other.Code;
        }

        /// <summary>
        /// Is this a US State or Canada Providence?
        /// </summary>
        /// <returns></returns>
        public bool IsCanadaProvidence()
        {
            return ProvidanceCodes.Contains(Code);
        }

        /// <summary>
        /// An static read only value for an Unknown State
        /// </summary>
        public static USState Unknown => new USState("");
    }
}

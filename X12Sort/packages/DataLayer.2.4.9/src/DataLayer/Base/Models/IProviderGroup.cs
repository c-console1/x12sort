namespace DataLayer.Base.Models
{
    /// <summary>
    /// This is an interface to a generic provider group
    /// </summary>
    public interface IProviderGroup
    {
        /// <summary>
        /// Provider group number
        /// </summary>
        string GroupNumber { get; set; }
        /// <summary>
        /// Provider group name
        /// </summary>
        string Name { get; set; }
        /// <summary>
        /// Provider group npi
        /// </summary>
        string NPI { get; set; }
        /// <summary>
        /// Provider group tax id
        /// </summary>
        string TaxID { get; set; }
    }
}

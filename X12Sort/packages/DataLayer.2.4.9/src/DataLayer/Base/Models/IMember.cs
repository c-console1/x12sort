using DataLayer.Base.Enumerations;
using System;
using System.Collections.Generic;

namespace DataLayer.Base.Models
{
    public interface IMember : IPersonName
    {
        /// <summary>
        /// The current member number
        /// </summary>
        string MemberNumber { get; }

        /// <summary>
        /// The persons sex, represented by "M"/"F" (uppercase single char)
        /// </summary>
        Sex Sex { get; set; }

        /// <summary>
        /// The birth date of the person, should be `DateTime.MinValue` if unknown
        /// </summary>
        DateTime? BirthDate { get; set; }

        /// <summary>
        /// The death date of the person, should be `null` if they are not dead yet
        /// </summary>
        /// <remarks>
        /// > Bring out your dead!
        /// </remarks>
        DateTime? DeathDate { get; set; }

        /// <summary>
        /// The Social Security Number of the Person
        /// </summary>
        string SSN { get; set; }

        /// <summary>
        /// The Ethnicity, value is current just a string  
        /// </summary>
        string Ethnicity { get; set; }

        /// <summary>
        /// A list of languages used by the person
        /// </summary>
        IEnumerable<Language> Languages { get; set; }
    }
}

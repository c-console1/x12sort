using System.Collections.Generic;

namespace DataLayer.Base.Models
{
    public interface IAuthorization
    {
        /// <summary>
        /// Authorization Identifier
        /// </summary>
        public string AuthorizationNumber { get; set; }
        /// <summary>
        /// Diagnosis object of the authorization
        /// </summary>
        public IAuthorizationDiagnosis AuthorizationDiagnosis { get; set; }
        /// <summary>
        /// List of the authorized service objects
        /// </summary>
        public List<IAuthorizedService> AuthorizedServices { get; set; }
        /// <summary>
        /// Requesting provider object
        /// </summary>
        public IAuthorizationProvider RequestingProvider { get; set; }
    }
}

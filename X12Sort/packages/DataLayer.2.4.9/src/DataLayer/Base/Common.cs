using System;
using System.Collections.Generic;
using DataLayer.Configuration;
using DataLayer.Configuration.Models;
using System.Text;

namespace DataLayer.Base
{
    /// <summary>
    /// Common values used by multiple location in the DataLayer
    /// </summary>
    public static class Common
    {
        /// <summary>
        /// Common machine settings
        /// </summary>
        public static ConfigMachineApplication configMachineApplication { get; internal set; }

        #region Shared information

        /// <summary>
        /// Common value store
        /// </summary>
        private static List<SharedInformation> sharedInfo = new List<SharedInformation>();

        /// <summary>
        /// Get values with a primary and secondary key
        /// </summary>
        /// <param name="primaryKey"></param>
        /// <param name="secondaryKey"></param>
        /// <returns></returns>
        public static object GetSharedInformation(string primaryKey, string secondaryKey)
        {
            SharedInformation output = sharedInfo.Find(
                x => x.keyPrimary.ToUpper() == primaryKey.ToUpper()
                && x.keySecondary.ToUpper() == secondaryKey.ToUpper());

            if (output == null)
                return null;

            return output.value;
        }

        /// <summary>
        /// Get values with a primary and secondary key
        /// </summary>
        /// <param name="primaryKey"></param>
        /// <returns></returns>
        public static object GetSharedInformation(string primaryKey)
        {
            return GetSharedInformation(primaryKey, "");
        }

        /// <summary>
        /// Add a shared item
        /// </summary>
        /// <param name="primaryKey"></param>
        /// <param name="secondaryKey"></param>
        /// <param name="value"></param>
        public static void AddSharedInformation(string primaryKey, string secondaryKey, object value)
        {
            // Remove old item if it exists
            SharedInformation itemToRemove = sharedInfo.Find(
                x => x.keyPrimary.ToUpper() == primaryKey.ToUpper()
                && x.keySecondary.ToUpper() == secondaryKey.ToUpper());
            if(itemToRemove != null)
                sharedInfo.Remove(itemToRemove);

            SharedInformation output = new SharedInformation()
            {
                keyPrimary = primaryKey,
                keySecondary = secondaryKey,
                value = value
            };
            sharedInfo.Add(output);
        }

        /// <summary>
        /// Add a shared item
        /// </summary>
        /// <param name="primaryKey"></param>
        /// <param name="value"></param>
        public static void AddSharedInformation(string primaryKey, object value)
        {
            AddSharedInformation(primaryKey, "", value);
        }

        #endregion Shared information

    }

    /// <summary>
    /// Share information/objects across internal modules
    /// </summary>
    public class SharedInformation
    {
        /// <summary>
        /// Primary key
        /// </summary>
        public string keyPrimary { get; set; }

        /// <summary>
        /// Secondary key
        /// </summary>
        public string keySecondary { get; set; }

        /// <summary>
        /// Value object
        /// </summary>
        public object value { get; set;  }

    }


}

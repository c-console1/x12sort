using DataLayer.Base;
using DataLayer.Billing.Models;

namespace DataLayer.Billing
{
    public class TaxRepository : BaseRepository<BillingDatabase>
    {
        public TaxRepository(BillingDatabase database) : base(database)
        {
        }

        /// <summary>
        /// Get most recenet 1099 information for a payee and network
        /// </summary>
        /// <param name="payee">Payee to get 1099 for</param>
        /// <param name="network">Network to get 1099 for. String must be 4 characters.</param>
        /// <returns></returns>
        /// <remarks>
        /// Assume it is 2019 and we have a provider who did not receive a check for any paid claims in 1998. The provider's most current 1099 would be from 2017.
        /// The purpose of this function is to get the most recent 1099 information available. That information may not be in the current calendar year.
        /// </remarks>
        public Payee1099 GetMostRecent1099(string payee, string network)
        {
            if (network.Length != 4)
                return null;

            string sql = @"select rtrim(irs_name) as Name, rtrim(irs_address) as Address, rtrim(city) as City, rtrim(state) as State, rtrim(zip) as Zip
                from amiown.payee_1099
                where 1=1
                and payee_who = :payee
                and substr(process_1099_by, 0, 4) = :network
                order by payment_year desc";

            Payee1099 results = Database.QueryFirstOrDefault<Payee1099>(sql, new
            {
                payee = payee.PadRight(14),
                network = network
            });

            return results;
        }

    }
}

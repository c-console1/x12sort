﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Base;
using DataLayer.Billing.Models;


namespace DataLayer.Billing
{
    public class PharmacyRepository : BaseRepository<BillingDatabase>
    {
        public PharmacyRepository(BillingDatabase database) : base(database)
        {
        }

        /// <summary>
        /// Get all pharmacies
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Pharmacy> GetPharmacies()
        {
            string sql = @"select image_recnbr as DatabaseKey,
                trim(pharmacy_nbr)          as providernumber,
                rtrim(npi)                  as npi,
                case when pharmacy_name = ' '
                    then 'pharmacy #' || trim(pharmacy_nbr)
                    else trim(pharmacy_name)
                    end                     as pharmacyname,
                rtrim(pharmacy_addr)        as address1,
                rtrim(pharmacy_city)        as city,
                pharmacy_st                 as state,
                substr(rtrim(pharmacy_zip),1,5) as zip,
                pharmacy_phone              as phone1
                from pharmacy";

            IEnumerable<Pharmacy> results = Database.Query<Pharmacy>(sql);
            return results;
        }

    }
}

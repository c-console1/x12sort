using DataLayer.Base.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Billing.Models
{
    public class Group : BaseBillingObject, IProviderGroup, IStartEnd
    {
        public string GroupNumber { get; set; }
        public string IRSNumber { get; set; }
        public string Name { get; set; }
        public string MedicareNumber { get; set; }
        public string MedicaidNumber { get; set; }
        public string NPI { get; set; }
        public string TaxID { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}

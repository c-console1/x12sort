﻿using System;

namespace DataLayer.Billing.Models
{
    /// <summary>
    /// An object representing a alternative id for an object
    /// AMISYS: AMIOWN.ALT_IDENTIFIER
    /// </summary>
    public class AlternativeIdentifier : BaseBillingObject
    {
        /// <summary>
        /// Standard Constructor
        /// </summary>
        public AlternativeIdentifier() : base()
        {
            Void = false;
            EffectiveDate = DateTime.Now;
            EndDate = new DateTime(9999, 12, 31);
            UpdateUser = "JTL";
            UpdateType = "A";
        }
        public string ReferenceType { get; set; }
        public string ReferenceId { get; set; }
        public string Code { get; set; }
        public string Value { get; set; }
        public bool Void { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime EndDate { get; set; }
        public string UpdateUser { get; set; }
        public string UpdateType { get; set; }
    }
}

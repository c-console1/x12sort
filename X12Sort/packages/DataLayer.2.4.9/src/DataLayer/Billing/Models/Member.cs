using DataLayer.Base.Models;
using DataLayer.Base.Enumerations;
using DataLayer.StaticUtilities;
using System;
using System.Collections.Generic;

namespace DataLayer.Billing.Models
{
    public class Member : BaseBillingObject, IMember
    {

        public string MemberNumber { get; set; }
        public string ContractNumber { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public IEnumerable<string> Suffixes { get; set; }
        public IEnumerable<string> Prefixes { get; set; }
        public string Prefix { get; set; }
        public string Sex { get; set; }
        public string SSN { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime? DeathDate { get; set; }
        public string MedicaidNumber { get; set; }
        public string MedicareNumber { get; set; }
        public string RecordNumber { get; set; }
        public IEnumerable<MemberEnrollment> Enrollments { get; set; }
        public string IncomingNumber { get; set; }
        public string Ethnicity { get; set; }
        public IEnumerable<string> Languages { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set;}
        Sex IMember.Sex { get; set; }
        DateTime? IMember.BirthDate { get; set; }
        IEnumerable<Language> IMember.Languages { get; set; }
    }
}

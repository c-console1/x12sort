using DataLayer.Base.Models;
using System;

namespace DataLayer.Billing.Models
{
    public class ProviderSpecialty : BaseBillingObject, ICode, IStartEnd
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}

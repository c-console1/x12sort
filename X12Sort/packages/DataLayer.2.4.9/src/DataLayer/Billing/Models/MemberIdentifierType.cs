namespace DataLayer.Billing.Models
{
    /// <summary>
    /// Enum with all of the ID types a member can have
    /// </summary>
    public enum MemberIdentifierType
    {
        /// <summary>
        /// Paramount Internal Member Number. This usually starts with a P or A and followed by 10 digits
        /// </summary>
        /// <remarks>
        /// Matt Elder: WC is workers comp. I don't think that is active any more. E was another
        /// Medicare product that we covered for a year or so, they are no longer active. I would
        /// exclude the WC member and the E members.
        /// </remarks>
        Paramount,

        /// <summary>
        /// Paramount Internal Contract Number. This usually Member number minus the last two digits
        /// </summary>
        Contract,

        /// <summary>
        /// Health Insurance Claim Number (HICN) issued by the Social Security Administration (SSA).
        /// This is old Medicare Number, A.K.A. HFCA
        /// </summary>
        HICN,

        /// <summary>
        /// Health Insurance Claim Number (HICN) issued by the Railroad Retirement Board (RRB)
        /// </summary>
        RRB,

        /// <summary> The new (April 2018) Medicare Number </summary>
        /// <remarks>
        /// Each MBI is randomly generated. This makes MBIs different than HICNs, which are based on
        /// the Social Security Numbers (SSNs) of people with Medicare. The MBI’s characters are
        /// “non-intelligent” so they don’t have any hidden or special meaning. MBIs are numbers and
        /// upper-case letters. We'll use numbers 0-9 and all letters from A to Z, except for S, L,
        /// O, I, B, and Z. This will help the characters be easier to read.
        /// </remarks>
        MBI,

        /// <summary>
        /// Any other number format that is not recognized by the system
        /// </summary>
        Unknown,

        /// <summary>
        /// The 7 or 10 digit Medicaid# is referred to as the case# and identifies all of the members
        /// of a single household.
        /// </summary>
        CaseNumber,

        /// <summary>
        /// The 12 digit Medicaid Number
        /// \</summary>
        Medicaid,

        /// <summary>
        /// Social Security Number
        /// </summary>
        SSN
    }
}

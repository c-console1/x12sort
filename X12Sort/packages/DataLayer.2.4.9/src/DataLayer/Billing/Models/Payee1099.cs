﻿namespace DataLayer.Billing.Models
{
    public class Payee1099 : BaseBillingObject
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
    }
}

using DataLayer.Base.Enumerations;
using DataLayer.Base.Models;
using System;

namespace DataLayer.Billing.Models
{
    /// <summary> Base class for all billing models, extends BaseDataObject </summary>
    public abstract class BaseBillingObject : BaseDataObject
    {
        public override DataSource Source { get => DataSource.AMISYS; }

        /// <summary> The date this records was updated. Default/No Value: DateTime.MinValue </summary>
        public DateTime UpdateDate { get; set; }

        /// <summary>
        /// High Data Value
        /// </summary>
        /// <value>The date 12/31/9999 </value>
        public DateTime HighDate { get { return DateTime.Parse("12/31/9999"); } }

        /// <summary>
        /// Before Write, it updates the Updated date
        /// </summary>
        public override void PrepForWrite()
        {
            UpdateDate = DateTime.Now;
        }

        /// <summary>
        /// Standard Constructor
        /// </summary>
        protected BaseBillingObject() : base()
        {
            UpdateDate = DateTime.MinValue;
        }
    }
}

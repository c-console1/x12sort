using DataLayer.Base.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Billing.Models
{
    public class Address:BaseBillingObject, IAddress
    {
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public USState State { get; set; }
        public ZipCode ZipCode { get; set; }
        //public string Phone { get; set; }
        //public string Fax { get; set; }
        //public string TaxID { get; set; }

        //public string Name { get; set; }
        //public string County { get; set; }
        public Country Country { get; set; }
        public string Phone1 { get; set; }
        public string Phone1Type { get; set; }
        public string Phone2 { get; set; }
        public string Phone2Type { get; set; }
        public string Phone3 { get; set; }
        public string Phone3Type { get; set; }
        public string Email { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}

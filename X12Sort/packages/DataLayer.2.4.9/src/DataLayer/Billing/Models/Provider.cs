using DataLayer.Base.Enumerations;
using DataLayer.Base.Models;
using System;
using System.Collections.Generic;

namespace DataLayer.Billing.Models
{
    public class Provider : BaseBillingObject, IProvider, IStartEnd
    {
        private DateTime? _birthdate;
        private DateTime? _deathDate;

        /// <summary>
        /// The day the person was born
        /// </summary>
        public DateTime? BirthDate
        {
            get { return _birthdate; }
            set { _birthdate = value; }
        }

        /// <summary>
        /// The day the person was born
        /// </summary>
        /// <remarks>
        /// Yes, I know this sounds weird, but a person's lifespan has start end dates
        /// </remarks>
        DateTime? IStartEnd.StartDate
        {
            get { return _birthdate; }
            set { _birthdate = value; }
        }

        /// <summary>
        /// The day the person died
        /// </summary>
        public DateTime? DeathDate
        {
            get { return _deathDate; }
            set { _deathDate = value; }
        }

        /// <summary>
        /// The day the person died
        /// </summary>
        /// <remarks>
        /// Yes, I know this sounds weird, but a person's lifespan has start end dates
        /// </remarks>
        DateTime? IStartEnd.EndDate
        {
            get { return _deathDate; }
            set { _deathDate = value; }
        }

        //public IEnumerable<string> Prefixes { get; set; }        
        //public IEnumerable<string> Suffixes { get; set; }
        //public string Suffix { get; set; }
        //public string SSN { get; set; }
        //public string DEANumber { get; set; }
        //public IEnumerable<ProviderAffiliation> Affiliations { get; set; }
        //public IEnumerable<ProviderSpecialty> Specialties { get; set; }

        //public IEnumerable<IAddress> Addresses { get; set; }
        //public IEnumerable<IBoard> Boards { get; set; }
        //public IEnumerable<IGroup> Groups { get; set; }
        //public IEnumerable<string> Languages { get; set; }

        public string ProviderNumber { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Degree { get; set; }
        public string Title { get; set; }
        public Sex Sex { get; set; }
        public string Ethnicity { get; set; }
        public string FacilityIndividual { get; set; }
        public string InstitutionName { get; set; }
        public string MedicareNumber { get; set; }
        public string MedicaidNumber { get; set; }
        public string NPI { get; set; }
        public string TaxID { get; set; }
        public bool IsIndividual
        {
            get
            {
                return !string.IsNullOrEmpty(LastName);
            }
        }
        public bool IsInstitution
        {
            get
            {
                return !string.IsNullOrEmpty(InstitutionName);
            }
        }
        public IEnumerable<ProviderSpecialty> Specialties { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Billing.Models
{
    public class ProviderAffiliation : BaseBillingObject
    {
        public string IRSNumber { get; set; }
        public string MedicareNumber { get; set; }
        public string MedicaidNumber { get; set; }
        public string Class { get; set; }
        public string HatCode { get; set; }
        public string PracticeNumber { get; set; }
        public string Specialty { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool AcceptingNew { get; set; }
        /// <summary>
        /// Is this a par or nonpar affiliation?
        /// https://www.verywellhealth.com/par-vs-non-par-providers-2317177
        /// </summary>
        public bool IsPar { get; set; }
        public string ClaimType { get; set; }
        public string Network { get; set; }
        public string Status { get; set; }
        public int Rank { get; set; }
    }
}

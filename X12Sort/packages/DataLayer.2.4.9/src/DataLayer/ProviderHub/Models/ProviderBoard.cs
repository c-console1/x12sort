﻿using System;
using System.Collections.Generic;

namespace DataLayer.ProviderHub.Models
{
    public class ProviderBoard : BaseHubObject
    {
        public string CredentialingCertificationKey{get;set;}
        public string Description { get; set; }
        public string Status { get; set; }
        public DateTime StartDate { get; set; }
    }
}

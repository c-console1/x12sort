using System.Collections.Generic;
using DataLayer.Base;
using DataLayer.ProviderHub.Models;

namespace DataLayer.ProviderHub
{
    public class HubRepository : BaseRepository<ProviderHubDatabase>
    {
        public HubRepository(ProviderHubDatabase database) : base(database)
        {
        }

        #region provider
        /// <summary>
        /// Gets a provider by either its billing or credentialing number
        /// </summary>
        /// <param name="billingProviderNumber">Billing provider number</param>
        /// <param name="credentialingProviderNumber">Credentialing provider number</param>
        /// <returns></returns>
        public Provider GetProvider(string billingProviderNumber, string credentialingProviderNumber)
        {
            string sql = @"select su_provider_id as DatabaseKey, rtrim(prov_nbr) as BillingProviderNumber, rtrim(su_cactus_provider_id) as CredentialingProviderNumber, rtrim(firstname) as FirstName, rtrim(middlename) as MiddleName, rtrim(lastname) as LastName,
                rtrim(su_degree) as Degree, rtrim(sex) as Sex, rtrim(instname) as InstitutionName, rtrim(npi) as NPI, rtrim(tax_id) as TaxID, rtrim(medicare_nbr) as MedicareNumber, rtrim(medicaid_nbr) as MedicaidNumber
                from amiown.su_ph_provider
                where 1 = 1";

            if (!string.IsNullOrEmpty(billingProviderNumber))
            {
                billingProviderNumber = billingProviderNumber.PadRight(12);
                sql += "\r\nand prov_nbr = :billingProviderNumber";
            }
            if (!string.IsNullOrEmpty(credentialingProviderNumber))
            {
                credentialingProviderNumber = credentialingProviderNumber.PadRight(10);
                sql += "\r\nand su_cactus_provider_id = :credentialingProviderNumber";
            }

            Provider results = Database.QueryFirstOrDefault<Provider>(sql, new
            {
                billingProviderNumber = billingProviderNumber,
                credentialingProviderNumber = credentialingProviderNumber
            });
            return results;
        }

        /// <summary>
        /// Gets a provider by first name, last name, and npi
        /// </summary>
        /// <param name="firstName">Provider first name</param>
        /// <param name="lastName">Provider last name</param>
        /// <param name="npi">NPI</param>
        /// <returns></returns>
        public Provider GetProvider(string firstName, string lastName, string npi)
        {
            return null;
        }
        #endregion

        #region certification
        /// <summary>
        /// Gets a list of cerifying boards for a provider
        /// </summary>
        /// <param name="providerKey">Provider primary key to get languages for</param>
        /// <returns></returns>
        public IEnumerable<ProviderBoard> GetProviderBoards(string providerKey)
        {
            string sql = @"select DatabaseKey, CredentialingCertificationKey, Description, Status, max(startdate) as StartDate from(
                    select pcc.su_certification_id as DatabaseKey, rtrim(c.su_cactus_cert_id) as CredentialingCertificationKey,
                    rtrim(pcc.su_board_name) as Description, rtrim(pcc.status) as Status, 
                    case when rtrim(pcc.ymddate) is not null then to_char(to_date(pcc.ymddate, 'yyyymmdd'), 'mm/dd/yyyy') else null end as StartDate
                    from amiown.su_ph_providercertcross pcc
                    join amiown.su_ph_certification c on pcc.su_certification_id = c.su_certification_id
                    where 1=1
                    and su_provider_id = :providerKey
                ) x
                group by Description, DatabaseKey, CredentialingCertificationKey, Status";

            IEnumerable<ProviderBoard> results = Database.Query<ProviderBoard>(sql, new { providerKey = providerKey });
            return results;
        }
        #endregion

        #region language
        /// <summary>
        /// Gets a list of languages fora a provider
        /// </summary>
        /// <param name="providerKey">Provider primary key to get languages for</param>
        /// <returns></returns>
        public IEnumerable<ProviderLanguage> GetProviderLanguages(string providerKey)
        {
            string sql = @"select l.su_language_id as DatabaseKey, rtrim(l.name) as Description
                from amiown.su_ph_language l
                join amiown.su_ph_providerlanguagecross plc on l.su_language_id=plc.su_language_id
                where 1 = 1
                and plc.su_provider_id = :providerKey";

            IEnumerable<ProviderLanguage> results = Database.Query<ProviderLanguage>(sql, new { providerKey = providerKey });
            return results;
        }
        #endregion

        #region address
        /// <summary>
        /// Gets the default physical address for a hub provider
        /// </summary>
        /// <param name="providerKey">Provider primary key to get address for</param>
        /// <returns></returns>
        public ProviderAddress GetProviderDefaultPhysicalAddress(string providerKey)
        {
            string sql = @"
            select * from (
                select a.su_address_id as databasekey, a.address1, a.address2, a.city, a.state, a.zip, a.phone, a.fax,
                case plc.su_address_type when 'Primary Address' then '1'
                    when 'Alternate Address' then '2'
                    when 'Primary Address - Do No Publish' then '3'
                    when 'Alternate Address - Do Not Publish' then '4'
                end as priority
                from amiown.su_ph_provider p
                join amiown.su_ph_providerlocationcross plc on p.su_provider_id=plc.su_provider_id
                join amiown.su_ph_location l on plc.su_location_id=l.su_location_id
                join amiown.su_ph_address a on l.su_address_id=a.su_address_id
                where 1=1
                and p.su_provider_id = :providerKey
            ) a
            order by priority
            ";

            ProviderAddress results = Database.QueryFirstOrDefault<ProviderAddress>(sql, new { providerKey = providerKey });
            return results;
        }
        #endregion

    }
}

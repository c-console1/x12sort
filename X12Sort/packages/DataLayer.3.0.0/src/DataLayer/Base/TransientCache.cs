using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Base
{
    public class TransientCacheDictList<TIndexType, TValueType, TKey, TValue> : TransientCacheUntyped where TIndexType : IList<Dictionary<TKey, TValue>> where TValueType : class
    {
        public static TransientCacheDictList<TIndexType, TValueType, TKey, TValue> GetCacheByName(string cacheName)
        {
            TransientCacheUntyped result = TransientCacheUntyped.GetTransientCacheUntyped(cacheName);

            if (result == null)
            {
                result = new TransientCacheDictList<TIndexType, TValueType, TKey, TValue>(cacheName);
                cacheTables.Add(cacheName, result);
            }

            return result as TransientCacheDictList<TIndexType, TValueType, TKey, TValue>;
        }

        protected TransientCacheDictList(string cn) : base(cn) { }

        public void StoreEntry(TIndexType entryKey, string orderBy, TValueType content)
        {
            int hashCode = GetOrderIndependentHashCode(entryKey);
            base.StoreEntry(hashCode, orderBy, content);
        }

        public bool GetEntry(TIndexType entryKey, string orderBy, out TValueType content, out bool partial)
        {
            object rawContent;

            int hashCode = GetOrderIndependentHashCode(entryKey);
            bool result = base.GetEntry(hashCode, orderBy, out rawContent, out partial);

            content = rawContent as TValueType;
            return result;
        }

        public void GetAndCacheResult(Func<TIndexType, string, TValueType> lambda, TIndexType entryKey, string orderBy, out TValueType content, out bool partial)
        {
            bool found = GetEntry(entryKey, orderBy, out content, out partial);
            if (found)
                return;

            if (lambda == null)
                return;

            content = lambda(entryKey, orderBy);

            StoreEntry(entryKey, orderBy, content);
        }

        protected static int GetOrderIndependentHashCode(TIndexType source)
        {
            int hash = 0;
            int curHash;

            // The previous version of this code was inadequate for the "List of Dictionaries" type used for the Member cache

            int bitOffset = 0;
            // Stores number of occurences so far of each value's hash code.
            var valueCounts = new Dictionary<int, int>();

            foreach (var list in source)
            {
                foreach (var element in list)
                {
                    // TODO -- want a type-generic way to recurse if element is an enumerable...
                    curHash = EqualityComparer<TKey>.Default.GetHashCode(element.Key)
                            + EqualityComparer<TValue>.Default.GetHashCode(element.Value);

                    if (valueCounts.TryGetValue(curHash, out bitOffset))
                        valueCounts[curHash] = bitOffset + 1;
                    else
                        valueCounts.Add(curHash, bitOffset);

                    // The current hash code is shifted (with wrapping) one bit
                    // further left on each successive recurrence of a certain
                    // value to widen the distribution.
                    // 37 is an arbitrary low prime number that helps the
                    // algorithm to smooth out the distribution.
                    hash = unchecked(hash + ((curHash << bitOffset) |
                        (curHash >> (32 - bitOffset))) * 37);
                    hash = unchecked(hash + curHash);
                }
            }

            return hash;
        }
    }

    public class TransientCacheIDictList<TIndexType, TValueType, TKey, TValue> : TransientCacheUntyped where TIndexType : IList<IDictionary<TKey, TValue>> where TValueType : class
    {
        public static TransientCacheIDictList<TIndexType, TValueType, TKey, TValue> GetCacheByName(string cacheName)
        {
            TransientCacheUntyped result = TransientCacheUntyped.GetTransientCacheUntyped(cacheName);

            if (result == null)
            {
                result = new TransientCacheIDictList<TIndexType, TValueType, TKey, TValue>(cacheName);
                cacheTables.Add(cacheName, result);
            }

            return result as TransientCacheIDictList<TIndexType, TValueType, TKey, TValue>;
        }

        protected TransientCacheIDictList(string cn) : base(cn) { }

        public void StoreEntry(TIndexType entryKey, string orderBy, TValueType content)
        {
            int hashCode = GetOrderIndependentHashCode(entryKey);
            base.StoreEntry(hashCode, orderBy, content);
        }

        public bool GetEntry(TIndexType entryKey, string orderBy, out TValueType content, out bool partial)
        {
            object rawContent;

            int hashCode = GetOrderIndependentHashCode(entryKey);
            bool result = base.GetEntry(hashCode, orderBy, out rawContent, out partial);

            content = rawContent as TValueType;
            return result;
        }

        public void GetAndCacheResult(Func<TIndexType, string, TValueType> lambda, TIndexType entryKey, string orderBy, out TValueType content, out bool partial)
        {
            bool found = GetEntry(entryKey, orderBy, out content, out partial);
            if (found)
                return;

            if (lambda == null)
                return;

            content = lambda(entryKey, orderBy);

            StoreEntry(entryKey, orderBy, content);
        }

        protected static int GetOrderIndependentHashCode(TIndexType source)
        {
            int hash = 0;
            int curHash;

            // The previous version of this code was inadequate for the "List of Dictionaries" type used for the Member cache

            int bitOffset = 0;
            // Stores number of occurences so far of each value's hash code.
            var valueCounts = new Dictionary<int, int>();

            foreach (var list in source)
            {
                foreach (var element in list)
                {
                    // TODO -- want a type-generic way to recurse if element is an enumerable...
                    curHash = EqualityComparer<TKey>.Default.GetHashCode(element.Key)
                            + EqualityComparer<TValue>.Default.GetHashCode(element.Value);

                    if (valueCounts.TryGetValue(curHash, out bitOffset))
                        valueCounts[curHash] = bitOffset + 1;
                    else
                        valueCounts.Add(curHash, bitOffset);

                    // The current hash code is shifted (with wrapping) one bit
                    // further left on each successive recurrence of a certain
                    // value to widen the distribution.
                    // 37 is an arbitrary low prime number that helps the
                    // algorithm to smooth out the distribution.
                    hash = unchecked(hash + ((curHash << bitOffset) |
                        (curHash >> (32 - bitOffset))) * 37);
                    hash = unchecked(hash + curHash);
                }
            }

            return hash;
        }
    }

    public class TransientCacheDict<TIndexType, TValueType, TKey, TValue> : TransientCacheUntyped where TIndexType : IDictionary<TKey, TValue> where TValueType : class
    {
        public static TransientCacheDict<TIndexType, TValueType, TKey, TValue> GetCacheByName(string cacheName)
        {
            TransientCacheUntyped result = TransientCacheUntyped.GetTransientCacheUntyped(cacheName);

            if (result == null)
            {
                result = new TransientCacheDict<TIndexType, TValueType, TKey, TValue>(cacheName);
                cacheTables.Add(cacheName, result);
            }

            return result as TransientCacheDict<TIndexType, TValueType, TKey, TValue>;
        }

        protected TransientCacheDict(string cn) : base(cn) { }

        public void StoreEntry(TIndexType entryKey, string orderBy, TValueType content)
        {
            int hashCode = GetOrderIndependentHashCode(entryKey);
            base.StoreEntry(hashCode, orderBy, content);
        }

        public bool GetEntry(TIndexType entryKey, string orderBy, out TValueType content, out bool partial)
        {
            object rawContent;

            int hashCode = GetOrderIndependentHashCode(entryKey);
            bool result = base.GetEntry(hashCode, orderBy, out rawContent, out partial);

            content = rawContent as TValueType;
            return result;
        }

        public void GetAndCacheResult(Func<TIndexType, string, TValueType> lambda, TIndexType entryKey, string orderBy, out TValueType content, out bool partial)
        {
            bool found = GetEntry(entryKey, orderBy, out content, out partial);
            if (found)
                return;

            if (lambda == null)
                return;

            content = lambda(entryKey, orderBy);

            StoreEntry(entryKey, orderBy, content);
        }

        protected static int GetOrderIndependentHashCode(TIndexType source)
        {
            int hash = 0;
            int curHash;

            // The previous version of this code was inadequate for the "List of Dictionaries" type used for the Member cache

            int bitOffset = 0;
            // Stores number of occurences so far of each value's hash code.
            var valueCounts = new Dictionary<int, int>();

            foreach (KeyValuePair<TKey, TValue> element in source)
            {
                curHash = EqualityComparer<TKey>.Default.GetHashCode(element.Key)
                        + EqualityComparer<TValue>.Default.GetHashCode(element.Value);

                if (valueCounts.TryGetValue(curHash, out bitOffset))
                    valueCounts[curHash] = bitOffset + 1;
                else
                    valueCounts.Add(curHash, bitOffset);

                // The current hash code is shifted (with wrapping) one bit
                // further left on each successive recurrence of a certain
                // value to widen the distribution.
                // 37 is an arbitrary low prime number that helps the
                // algorithm to smooth out the distribution.
                hash = unchecked(hash + ((curHash << bitOffset) |
                    (curHash >> (32 - bitOffset))) * 37);
                hash = unchecked(hash + curHash);
            }

            return hash;
        }
    }

    public class TransientCacheUntyped
    {
        protected static TransientCacheUntyped GetTransientCacheUntyped(string cacheName)
        {
            TransientCacheUntyped result = cacheTables.ContainsKey(cacheName) ? cacheTables[cacheName] : null;

            return result;
        }

        protected TransientCacheUntyped(string cn)
        {
            cacheName = cn;
        }

        protected void StoreEntry(int hashCode, string secondaryKey, object content)
        {
            string cacheIndexWild = GetPartialKey(hashCode);
            StoreInCache(cacheIndexWild, content/*, purge: true*/);

            string cacheIndexExact = GetExactKey(hashCode, secondaryKey);
            StoreInCache(cacheIndexExact, content);
        }

        protected bool GetEntry(int hashCode, string secondaryKey, out object content, out bool partial)
        {
            partial = false;
            string cacheIndexExact = GetExactKey(hashCode, secondaryKey);
            string cacheIndexWild = GetPartialKey(hashCode);
            if (cacheContents.ContainsKey(cacheIndexExact))
            {
                content = cacheContents[cacheIndexExact].Content;
                return true;
            }
            else if (cacheContents.ContainsKey(cacheIndexWild))
            {
                content = cacheContents[cacheIndexWild].Content;
                partial = true;
                return true;
            }
            else
            {
                content = null;
                return false;
            }
        }

        #region Private Parts (some are protected for testing)
        protected static Dictionary<string, TransientCacheUntyped> cacheTables = new Dictionary<string, TransientCacheUntyped>();

        protected string cacheName;
        protected Dictionary<string, TransientCacheEntry> cacheContents = new Dictionary<string, TransientCacheEntry>();

        protected string GetExactKey(int hashCode, string secondaryKey) => $"{cacheName}:{hashCode}:{secondaryKey}";
        protected string GetPartialKey(int hashCode) => $"{cacheName}:{hashCode}:*";

        private void StoreInCache(string index, object content, bool purge = false)
        {
            TransientCacheEntry tec = new TransientCacheEntry(e: GetExpireTime(), content);
            if (cacheContents.ContainsKey(index))
            {
                cacheContents[index] = tec;
            }
            else
            {
                cacheContents.Add(index, tec);
            }

            if (purge)
            {
                var expiredKeys = cacheContents.Where(pair => pair.Value.Expires > DateTime.Now).Select(pair => pair.Key).ToArray();
                foreach (string key in expiredKeys)
                {
                    cacheContents.Remove(key);
                }
            }
        }

#if DEBUG
        protected static DateTime? falseClock = null;
        private static DateTime GetExpireTime() => (falseClock.HasValue ? falseClock.Value : DateTime.Now).AddHours(2);
#else
        private static DateTime GetExpireTime() => DateTime.Now.AddHours(2);
#endif
        #endregion Private Parts

        public struct TransientCacheEntry:IEquatable<TransientCacheEntry>
        {
            public TransientCacheEntry(DateTime e, object c) { Expires = e; Content = c; }

            public DateTime Expires { get; private set; }
            public object Content { get; private set; }

            /// <summary>
            /// Not equals comparison operator
            /// </summary>
            /// <param name="obj1"></param>
            /// <param name="obj2"></param>
            /// <returns></returns>
            public static bool operator !=(TransientCacheEntry obj1, TransientCacheEntry obj2)
            {
                return !(obj1 == obj2);
            }

            /// <summary>
            /// Equals comparison operator
            /// </summary>
            /// <param name="obj1"></param>
            /// <param name="obj2"></param>
            /// <returns></returns>
            public static bool operator ==(TransientCacheEntry obj1, TransientCacheEntry obj2)
            {
                if (ReferenceEquals(obj1, obj2))
                {
                    return true;
                }

                if (ReferenceEquals(obj1, null) || ReferenceEquals(obj2, null))
                {
                    return false;
                }

                bool nameMatch = obj1.Expires == obj2.Expires;
                bool codeMatch = obj1.Content == obj2.Content;

                return nameMatch && codeMatch;
            }

            /// <summary>
            /// Compares two Schema obeject to see if they are the same
            /// </summary>
            /// <param name="other">The other schema object</param>
            /// <returns></returns>
            public bool Equals(TransientCacheEntry other)
            {
                return (this == other);
            }

            /// <summary>
            /// Compares two Schema obeject to see if they are the same
            /// </summary>
            /// <param name="other">The other schema object</param>
            /// <returns></returns>
            public override bool Equals(object obj)
            {
                // validate the columns schemas
                if (obj == null || !(obj is TransientCacheEntry))
                    return false;

                return (this == (TransientCacheEntry)obj);
            }

            /// <summary>
            /// Gets the hash code for the current object
            /// </summary>
            /// <returns></returns>
            public override int GetHashCode()
            {
                //get hash code theory: https://blogs.msdn.microsoft.com/ericlippert/2011/02/28/guidelines-and-rules-for-gethashcode/
                //implementation idea borrowed from: https://rehansaeed.com/gethashcode-made-easy/
                unchecked
                {
                    int hashCode = 17;
                    hashCode = (hashCode * 23) + Expires.GetHashCode();
                    hashCode = (hashCode * 23) + Content.GetHashCode();

                    return hashCode;
                }
            }

        }
    }

    // This was a previous attempt before I gave up on a single cache class
    public static class ObsoleteHasher
    {
        // https://stackoverflow.com/questions/670063/getting-hash-of-a-list-of-strings-regardless-of-order

        public static int GetOrderIndependentHashCode<T>(IEnumerable<T> source)
        {
            int hash = 0;
            int curHash;

            // TODO - this is working fine for dictionaries, not so much for lists of dictionaries.

            // The commented-out lines attempt to improve the hash by shifting it each time the same element is
            // seen.  Because the input is a dictionary, this can't happen - the key part of the KeyValuePair is
            // unique.

            //int bitOffset = 0;
            //// Stores number of occurences so far of each value.
            //var valueCounts = new Dictionary<T, int>();
            if (source != null)
            {
                foreach (T element in source)
                {
                    curHash = EqualityComparer<T>.Default.GetHashCode(element);
                    //    if (valueCounts.TryGetValue(element, out bitOffset))
                    //        valueCounts[element] = bitOffset + 1;
                    //    else
                    //        valueCounts.Add(element, bitOffset);

                    //    // The current hash code is shifted (with wrapping) one bit
                    //    // further left on each successive recurrence of a certain
                    //    // value to widen the distribution.
                    //    // 37 is an arbitrary low prime number that helps the
                    //    // algorithm to smooth out the distribution.
                    //    hash = unchecked(hash + ((curHash << bitOffset) |
                    //        (curHash >> (32 - bitOffset))) * 37);
                    hash = unchecked(hash + curHash);
                }
            }

            return hash;
        }
    }
}

namespace DataLayer.Base.Models
{
    public interface IAuthorizationDiagnosis : IStartEnd, ICode
    {
        /// <summary>
        /// Type of diagnosis such as admitting, primary, etc.
        /// </summary>
        public string DiagnosisType { get; set; }
        /// <summary>
        /// Name of the diagnosis
        /// </summary>
        public string DiagnosisName { get; set; }
    }
}

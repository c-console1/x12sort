﻿using System;

namespace DataLayer.Base.Models
{
    /// <summary>
    /// An Item that has a start and end date
    /// </summary>
    /// <remarks>
    /// This is to represent all types of start and dates, even though other systems may name them differently
    /// </remarks>
    public interface IStartEnd
    {
        /// <summary>
        /// The date this item became valid
        /// </summary>
        DateTime? StartDate { get; set; }

        /// <summary>
        /// The data this item stops being valid
        /// </summary>
        DateTime? EndDate { get; set; }
    }
}
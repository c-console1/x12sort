using DataLayer.StaticUtilities.Extensions;
using System;

namespace DataLayer.Base.Models
{
    /// <summary>
    /// Basic class that represents a start and end date range
    /// </summary>
    /// <seelaso cref="IStartEnd" />
    public class StartEndRange : IStartEnd, IEquatable<IStartEnd>
    {
        /// <summary>
        /// Wow, The Start Date!
        /// </summary>
        public virtual DateTime? StartDate { get; set; }

        /// <summary>
        /// Wow, The End Date!
        /// </summary>
        public virtual DateTime? EndDate { get; set; }


        /// <summary>
        /// Compares to another `IStartEnd` range.
        /// </summary>
        /// <param name="other">The other range</param>
        /// <remarks>This is a very basic comparison just compared the equality of the dates</remarks>
        /// <returns></returns>
        public bool Equals(IStartEnd other)
        {
            return this.EqualTo(other);
        }
    }
    
}

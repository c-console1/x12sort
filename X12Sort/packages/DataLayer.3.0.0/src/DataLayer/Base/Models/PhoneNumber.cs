using DataLayer.Base.Models.Converters;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace DataLayer.Base.Models
{
    /// <summary>
    /// US Zip Code
    /// </summary>
    /// <remarks>
    /// <para>
    /// Using a struct because this is conceptually an immutable value type.
    /// </para>
    /// <para>
    /// Only allows assigning valid zip codes, either 5, or 9 digit. If given an invalid value, the zip will be empty.
    /// </para>
    /// <para>
    /// Regex from: https://stackoverflow.com/a/16702965
    /// </para>
    /// </remarks>
    /// <example>
    /// This is how you can assign the values to object
    /// <code>
    /// <![CDATA[
    /// PhoneNumber zip = 32456;
    /// PhoneNumber zip9 = "43537-4002";
    /// ]]>
    /// </code>
    /// </example>
    [JsonConverter(typeof(PhoneNumberConverter))]
    public struct PhoneNumber : IEquatable<PhoneNumber>
    {
        /// <summary>
        /// Regex for detecting the parts of a phone number
        /// </summary>
        private static readonly Regex _phoneNumRegex = new Regex(@"^(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?$");

        /// <summary> Area Code (ex: 800) </summary>
        public string Area { get; }

        /// <summary> Country Code (ex: 1 or 86) </summary>
        public string Country { get; }

        /// <summary>
        /// The digits of just the area code, exchange, and subscribe with no punctuation
        /// </summary>
        public string Digits => $"{Country.ToString()}{Area.ToString()}{Exchange.ToString()}{Subscriber.ToString()}";

        /// <summary> Exchange (ex: 555) </summary>
        public string Exchange { get; }

        /// <summary> Extension (ex: 5678) </summary>
        public string Extension { get; }

        /// <summary>
        /// The Full Human Formatted Phone Number
        /// </summary>
        public string Full => ToString();

        /// <summary> Subscriber Number (ex: 1234) </summary>
        public string Subscriber { get; }

        /// <summary>
        /// Basic Constructor
        /// </summary>
        /// <param name="value">Either 5 or 5+4 PhoneNumber</param>
        public PhoneNumber(string value)
        {
            Country = "";
            Area = "";
            Exchange = "";
            Subscriber = "";
            Extension = "";

            if (!string.IsNullOrEmpty(value))
            {
                value = value.Trim();

                // check for phone number which are all 0, those are empty
                if (!value.All(q => q == '0'))
                {
                    Match match = _phoneNumRegex.Match(value);
                    if (match.Success)
                    {
                        if (match.Groups[1].Success) Country = match.Groups[1].Value;
                        if (match.Groups[2].Success) Area = match.Groups[2].Value;
                        if (match.Groups[3].Success) Exchange = match.Groups[3].Value;
                        if (match.Groups[4].Success) Subscriber = match.Groups[4].Value;
                        if (match.Groups[5].Success) Extension = match.Groups[5].Value;
                    }
                }
            }
        }
        /// <summary>
        /// Convert a Int64 to `PhoneNumber`
        /// </summary>
        /// <param name="value">The int that represents a `PhoneNumber`</param>
        public static PhoneNumber FromInt64(long value)
        {
            return new PhoneNumber(value.ToString());
        }

        /// <summary>
        /// Convert a string to PhoneNumber
        /// </summary>
        /// <param name="value">The int that represents a `PhoneNumber`</param>
        public static PhoneNumber FromString(string value)
        {
            return new PhoneNumber(value);
        }

        /// <summary>
        /// Convert the digits of the `PhoneNumber` to Int64
        /// </summary>
        /// <param name="value">PhoneNumber object </param>
        static public implicit operator long(PhoneNumber value)
        {
            return value.ToInt64();
        }

        /// <summary>
        /// Convert a string to PhoneNumber
        /// </summary>
        /// <param name="value">The string that should be a `PhoneNumber`</param>
        public static implicit operator PhoneNumber(string value)
        {
            return FromString(value);
        }

        /// <summary>
        /// Convert a Int64 to `PhoneNumber`
        /// </summary>
        /// <param name="value">The int that represents a `PhoneNumber`</param>
        public static implicit operator PhoneNumber(long value)
        {
            return FromInt64(value);
        }

        /// <summary>
        /// Convert a PhoneNumber to String
        /// </summary>
        /// <param name="value">The `PhoneNumber` object</param>
        static public implicit operator string(PhoneNumber value)
        {
            return value.ToString();
        }

        /// <summary>
        /// Compares two phone numbers for inequality
        /// </summary>
        /// <param name="obj1">The starting phone number</param>
        /// <param name="obj2">The other phone number to compare</param>
        /// <returns></returns>
        public static bool operator !=(PhoneNumber obj1, PhoneNumber obj2)
        {
            return !obj1.Equals(obj2);
        }

        /// <summary>
        /// Compares two phone numbers together
        /// </summary>
        /// <param name="obj1">The starting phone number</param>
        /// <param name="obj2">The other phone number to compare</param>
        /// <returns></returns>
        public static bool operator ==(PhoneNumber obj1, PhoneNumber obj2)
        {
            return obj1.Equals(obj2);
        }

        /// <summary>
        /// Compares two phone numbers together
        /// </summary>
        /// <param name="other">The other phone number to compare</param>
        /// <returns></returns>
        public bool Equals(PhoneNumber other)
        {
            bool countryMatch = Country == other.Country;
            bool areaMatch = Area == other.Area;
            bool exchangeMatch = Exchange == other.Exchange;
            bool subscriberMatch = Subscriber == other.Subscriber;
            bool extensionMatch = Extension == other.Extension;
            return countryMatch && areaMatch && exchangeMatch && subscriberMatch && extensionMatch;
        }

        /// <summary>
        /// Compares two phone numbers together
        /// </summary>
        /// <param name="obj">The other phone number to compare</param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (obj is int) return (int)obj == this;
            if (obj is string) return (string)obj == ToString();
            return false;
        }

        /// <summary>
        /// Override of GetHashCode()
        /// </summary>
        /// <returns>
        /// Return the hash code of the `ToString()` function
        /// </returns>
        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        /// <summary>
        /// Convert the digits of the `PhoneNumber` to Int64
        /// </summary>
        public long ToInt64()
        {
            return Convert.ToInt64(Digits);
        }

        /// <summary>
        /// The PhoneNumber, full zip code if available
        /// </summary>
        /// <returns>`this.Name`</returns>
        public override string ToString()
        {
            string country = string.IsNullOrEmpty(Country) ? "" : $"+{Country} ";
            string area = string.IsNullOrEmpty(Area) ? "" : $"({Area})";
            string exchange = string.IsNullOrEmpty(Exchange) ? "" : $"{Exchange}-";
            string subscriber = string.IsNullOrEmpty(Subscriber) ? "" : $"{Subscriber}";
            string extension = string.IsNullOrEmpty(Extension) ? "" : $" x{Extension}";
            return $"{country}{area}{exchange}{subscriber}{extension}";
        }

        /// <summary>
        /// Default empty phone number
        /// </summary>
        public static PhoneNumber Empty => new PhoneNumber();
    }
}

namespace DataLayer.Base.Models
{
    public interface IAuthorizationProcedure : IStartEnd, ICode
    {
        /// <summary>
        /// Type of procedure authorized
        /// </summary>
        public string ProcedureType { get; set; }
        /// <summary>
        /// Name of the procedure authorized
        /// </summary>
        public string ProcedureName { get; set; }
    }
}

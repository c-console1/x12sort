using System.Collections.Generic;

namespace DataLayer.Base.Models
{
    public interface IAuthorization
    {
        /// <summary>
        /// Authorization Identifier
        /// </summary>
        public string AuthorizationNumber { get; set; }
        /// <summary>
        /// List of the authorized service objects
        /// </summary>
        public List<IAuthorizedService> AuthorizedServices { get; set; }
    }
}

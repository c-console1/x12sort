using DataLayer.Base.Enumerations;

namespace DataLayer.Base.Models
{
    /// <summary>
    /// Bottom Layer of all Database objects.
    /// </summary>
    /// <remarks>
    /// The Goal for this object is to have a common object across all data objects in the data layer
    /// </remarks>
    public interface IBaseDataObject
    {
        /// <summary>
        /// The primary key of the database record represented by this object
        /// </summary>
        string DatabaseKey { get; set; }

        /// <summary>
        /// The source of the data object
        /// </summary>
        DataSource Source { get; }
    }
}

using System.Collections.Generic;

namespace DataLayer.Base.Models
{
    /// <summary>
    /// This is an interface to a generic provider
    /// </summary>
    /// <remarks>
    /// <para>
    /// **Note:** Many of these fields may be empty, not every implementation will have a value for every property
    /// </para>
    /// </remarks>
    public interface IProvider
    {
        /// <summary>
        /// The company wide number for a provider, should be the same in all systems
        /// </summary>
        /// <reamkes>
        /// Between different databases, `DatabaseKey` may change. This number should be the same across all systems
        /// </reamkes>
        string ProviderNumber { get; set; }

        /// <summary>
        /// Provider's first name
        /// </summary>
        string FirstName { get; set; }

        /// <summary>
        /// Proivder's last name
        /// </summary>
        string LastName { get; set; }

        /// <summary>
        /// Provider's institutional name
        /// </summary>
        string InstitutionName { get; set; }

        /// <summary>
        /// The Medicare Number, if there is more than one, this should be the current one
        /// </summary>
        string MedicareNumber { get; set; }

        /// <summary>
        /// The Medicaid Number, if there is more than one, this should be the current one
        /// </summary>
        string MedicaidNumber { get; set; }

        /// <summary>
        /// The NPI of the provider
        /// </summary>
        string NPI { get; set; }

        /// <summary>
        /// The Tax Id of the Provider
        /// </summary>
        string TaxID { get; set; }

        /// <summary>
        /// True if provider is an individual
        /// </summary>
        bool IsIndividual { get; }

        /// <summary>
        /// True if provider is an institution
        /// </summary>
        bool IsInstitution { get; }
    }
}

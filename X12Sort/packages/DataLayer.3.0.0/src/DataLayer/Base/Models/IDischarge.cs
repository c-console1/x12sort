using System;

namespace DataLayer.Base.Models
{
    public interface IDischarge : IBaseDataObject, IStartEnd
    {
        /// <summary>
        /// Actual discharge date
        /// </summary>
        public DateTime? DischargeDate { get; set; }
        /// <summary>
        /// Discharge status
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// Discharge description
        /// </summary>
        public string Description { get; set; }
    }
}

using DataLayer.Base.Models.Converters;
using Newtonsoft.Json;
using System;

namespace DataLayer.Base.Models
{
    /// <summary>
    /// US State value
    /// </summary>
    /// <remarks>
    /// <para>
    /// Using a struct because this is conceptually an immutable value type.
    /// </para>
    /// <para>
    /// Only allows assigning valid states, either state code or state name. If given an invalid value, the state will be empty.
    /// </para>
    /// </remarks>
    /// <example>
    /// This is how you can assign the values to object
    /// <code>
    /// <![CDATA[
    /// Country ohio = "ohio";
    /// Country delaware = "DE";
    /// ]]>
    /// </code>
    /// The following can be used to get the value
    /// <code>
    /// <![CDATA[
    /// Country state = "Michigan";
    /// string stateString = state;
    /// Assert.That(stateString, Is.EqualTo("MI"));
    /// Assert.That(state.ToString(), Is.EqualTo("Michigan"));
    /// Assert.That(state.Name, Is.EqualTo("Michigan"));
    /// Assert.That(state.Code, Is.EqualTo("MI"));
    /// }
    /// ]]>
    /// </code>
    /// </example>
    [JsonConverter(typeof(CountryConverter))]
    public struct Country : IEquatable<Country>
    {

        /// <summary>
        /// The three letter country ISO code
        /// </summary>
        public string Code3 { get; }

        /// <summary>
        /// The two letter ISO country code
        /// </summary>
        public string Code { get; }

        /// <summary>
        /// The full name of the country
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Basic Constructor
        /// </summary>
        /// <param name="value">Should be a state name of code</param>
        public Country(string name, string code, string code3)
        {
            Name = name;
            Code = code;
            Code3 = code3;
        }

        /// <summary>
        /// Convert a string to state object
        /// </summary>
        /// <param name="value">The string that should be a state name</param>
        public static Country FromString(string value)
        {
            if (value == null)
                value = "";

            switch (value.Trim().ToUpper())
            {
                case "AFGHANISTAN": return new Country("Afghanistan", "AF", "AFG");
                case "ALBANIA": return new Country("Albania", "AL", "ALB");
                case "ALGERIA": return new Country("Algeria", "DZ", "DZA");
                case "AMERICAN SAMOA": return new Country("American Samoa", "AS", "ASM");
                case "ANDORRA": return new Country("Andorra", "AD", "AND");
                case "ANGOLA": return new Country("Angola", "AO", "AGO");
                case "ANGUILLA": return new Country("Anguilla", "AI", "AIA");
                case "ANTARCTICA": return new Country("Antarctica", "AQ", "ATA");
                case "ANTIGUA AND BARBUDA": return new Country("Antigua and Barbuda", "AG", "ATG");
                case "ARGENTINA": return new Country("Argentina", "AR", "ARG");
                case "ARMENIA": return new Country("Armenia", "AM", "ARM");
                case "ARUBA": return new Country("Aruba", "AW", "ABW");
                case "AUSTRALIA": return new Country("Australia", "AU", "AUS");
                case "AUSTRIA": return new Country("Austria", "AT", "AUT");
                case "AZERBAIJAN": return new Country("Azerbaijan", "AZ", "AZE");
                case "BAHAMAS": return new Country("Bahamas", "BS", "BHS");
                case "BAHRAIN": return new Country("Bahrain", "BH", "BHR");
                case "BANGLADESH": return new Country("Bangladesh", "BD", "BGD");
                case "BARBADOS": return new Country("Barbados", "BB", "BRB");
                case "BELARUS": return new Country("Belarus", "BY", "BLR");
                case "BELGIUM": return new Country("Belgium", "BE", "BEL");
                case "BELIZE": return new Country("Belize", "BZ", "BLZ");
                case "BENIN": return new Country("Benin", "BJ", "BEN");
                case "BERMUDA": return new Country("Bermuda", "BM", "BMU");
                case "BHUTAN": return new Country("Bhutan", "BT", "BTN");
                case "BOLIVIA": return new Country("Bolivia", "BO", "BOL");
                case "BOSNIA AND HERZEGOVINA": return new Country("Bosnia and Herzegovina", "BA", "BIH");
                case "BOTSWANA": return new Country("Botswana", "BW", "BWA");
                case "BRAZIL": return new Country("Brazil", "BR", "BRA");
                case "BRITISH INDIAN OCEAN TERRITORY": return new Country("British Indian Ocean Territory", "IO", "IOT");
                case "BRITISH VIRGIN ISLANDS": return new Country("British Virgin Islands", "VG", "VGB");
                case "BRUNEI": return new Country("Brunei", "BN", "BRN");
                case "BULGARIA": return new Country("Bulgaria", "BG", "BGR");
                case "BURKINA FASO": return new Country("Burkina Faso", "BF", "BFA");
                case "BURUNDI": return new Country("Burundi", "BI", "BDI");
                case "CAMBODIA": return new Country("Cambodia", "KH", "KHM");
                case "CAMEROON": return new Country("Cameroon", "CM", "CMR");
                case "CANADA": return new Country("Canada", "CA", "CAN");
                case "CAPE VERDE": return new Country("Cape Verde", "CV", "CPV");
                case "CAYMAN ISLANDS": return new Country("Cayman Islands", "KY", "CYM");
                case "CENTRAL AFRICAN REPUBLIC": return new Country("Central African Republic", "CF", "CAF");
                case "CHAD": return new Country("Chad", "TD", "TCD");
                case "CHILE": return new Country("Chile", "CL", "CHL");
                case "CHINA": return new Country("China", "CN", "CHN");
                case "CHRISTMAS ISLAND": return new Country("Christmas Island", "CX", "CXR");
                case "COCOS ISLANDS": return new Country("Cocos Islands", "CC", "CCK");
                case "COLOMBIA": return new Country("Colombia", "CO", "COL");
                case "COMOROS": return new Country("Comoros", "KM", "COM");
                case "COOK ISLANDS": return new Country("Cook Islands", "CK", "COK");
                case "COSTA RICA": return new Country("Costa Rica", "CR", "CRI");
                case "CROATIA": return new Country("Croatia", "HR", "HRV");
                case "CUBA": return new Country("Cuba", "CU", "CUB");
                case "CURACAO": return new Country("Curacao", "CW", "CUW");
                case "CYPRUS": return new Country("Cyprus", "CY", "CYP");
                case "CZECH REPUBLIC": return new Country("Czech Republic", "CZ", "CZE");
                case "DEMOCRATIC REPUBLIC OF THE CONGO": return new Country("Democratic Republic of the Congo", "CD", "COD");
                case "DENMARK": return new Country("Denmark", "DK", "DNK");
                case "DJIBOUTI": return new Country("Djibouti", "DJ", "DJI");
                case "DOMINICA": return new Country("Dominica", "DM", "DMA");
                case "DOMINICAN REPUBLIC": return new Country("Dominican Republic", "DO", "DOM");
                case "EAST TIMOR": return new Country("East Timor", "TL", "TLS");
                case "ECUADOR": return new Country("Ecuador", "EC", "ECU");
                case "EGYPT": return new Country("Egypt", "EG", "EGY");
                case "EL SALVADOR": return new Country("El Salvador", "SV", "SLV");
                case "EQUATORIAL GUINEA": return new Country("Equatorial Guinea", "GQ", "GNQ");
                case "ERITREA": return new Country("Eritrea", "ER", "ERI");
                case "ESTONIA": return new Country("Estonia", "EE", "EST");
                case "ETHIOPIA": return new Country("Ethiopia", "ET", "ETH");
                case "FALKLAND ISLANDS": return new Country("Falkland Islands", "FK", "FLK");
                case "FAROE ISLANDS": return new Country("Faroe Islands", "FO", "FRO");
                case "FIJI": return new Country("Fiji", "FJ", "FJI");
                case "FINLAND": return new Country("Finland", "FI", "FIN");
                case "FRANCE": return new Country("France", "FR", "FRA");
                case "FRENCH POLYNESIA": return new Country("French Polynesia", "PF", "PYF");
                case "GABON": return new Country("Gabon", "GA", "GAB");
                case "GAMBIA": return new Country("Gambia", "GM", "GMB");
                case "GEORGIA": return new Country("Georgia", "GE", "GEO");
                case "GERMANY": return new Country("Germany", "DE", "DEU");
                case "GHANA": return new Country("Ghana", "GH", "GHA");
                case "GIBRALTAR": return new Country("Gibraltar", "GI", "GIB");
                case "GREECE": return new Country("Greece", "GR", "GRC");
                case "GREENLAND": return new Country("Greenland", "GL", "GRL");
                case "GRENADA": return new Country("Grenada", "GD", "GRD");
                case "GUAM": return new Country("Guam", "GU", "GUM");
                case "GUATEMALA": return new Country("Guatemala", "GT", "GTM");
                case "GUERNSEY": return new Country("Guernsey", "GG", "GGY");
                case "GUINEA": return new Country("Guinea", "GN", "GIN");
                case "GUINEA-BISSAU": return new Country("Guinea-Bissau", "GW", "GNB");
                case "GUYANA": return new Country("Guyana", "GY", "GUY");
                case "HAITI": return new Country("Haiti", "HT", "HTI");
                case "HONDURAS": return new Country("Honduras", "HN", "HND");
                case "HONG KONG": return new Country("Hong Kong", "HK", "HKG");
                case "HUNGARY": return new Country("Hungary", "HU", "HUN");
                case "ICELAND": return new Country("Iceland", "IS", "ISL");
                case "INDIA": return new Country("India", "IN", "IND");
                case "INDONESIA": return new Country("Indonesia", "ID", "IDN");
                case "IRAN": return new Country("Iran", "IR", "IRN");
                case "IRAQ": return new Country("Iraq", "IQ", "IRQ");
                case "IRELAND": return new Country("Ireland", "IE", "IRL");
                case "ISLE OF MAN": return new Country("Isle of Man", "IM", "IMN");
                case "ISRAEL": return new Country("Israel", "IL", "ISR");
                case "ITALY": return new Country("Italy", "IT", "ITA");
                case "IVORY COAST": return new Country("Ivory Coast", "CI", "CIV");
                case "JAMAICA": return new Country("Jamaica", "JM", "JAM");
                case "JAPAN": return new Country("Japan", "JP", "JPN");
                case "JERSEY": return new Country("Jersey", "JE", "JEY");
                case "JORDAN": return new Country("Jordan", "JO", "JOR");
                case "KAZAKHSTAN": return new Country("Kazakhstan", "KZ", "KAZ");
                case "KENYA": return new Country("Kenya", "KE", "KEN");
                case "KIRIBATI": return new Country("Kiribati", "KI", "KIR");
                case "KOSOVO": return new Country("Kosovo", "XK", "XKX");
                case "KUWAIT": return new Country("Kuwait", "KW", "KWT");
                case "KYRGYZSTAN": return new Country("Kyrgyzstan", "KG", "KGZ");
                case "LAOS": return new Country("Laos", "LA", "LAO");
                case "LATVIA": return new Country("Latvia", "LV", "LVA");
                case "LEBANON": return new Country("Lebanon", "LB", "LBN");
                case "LESOTHO": return new Country("Lesotho", "LS", "LSO");
                case "LIBERIA": return new Country("Liberia", "LR", "LBR");
                case "LIBYA": return new Country("Libya", "LY", "LBY");
                case "LIECHTENSTEIN": return new Country("Liechtenstein", "LI", "LIE");
                case "LITHUANIA": return new Country("Lithuania", "LT", "LTU");
                case "LUXEMBOURG": return new Country("Luxembourg", "LU", "LUX");
                case "MACAU": return new Country("Macau", "MO", "MAC");
                case "MACEDONIA": return new Country("Macedonia", "MK", "MKD");
                case "MADAGASCAR": return new Country("Madagascar", "MG", "MDG");
                case "MALAWI": return new Country("Malawi", "MW", "MWI");
                case "MALAYSIA": return new Country("Malaysia", "MY", "MYS");
                case "MALDIVES": return new Country("Maldives", "MV", "MDV");
                case "MALI": return new Country("Mali", "ML", "MLI");
                case "MALTA": return new Country("Malta", "MT", "MLT");
                case "MARSHALL ISLANDS": return new Country("Marshall Islands", "MH", "MHL");
                case "MAURITANIA": return new Country("Mauritania", "MR", "MRT");
                case "MAURITIUS": return new Country("Mauritius", "MU", "MUS");
                case "MAYOTTE": return new Country("Mayotte", "YT", "MYT");
                case "MEXICO": return new Country("Mexico", "MX", "MEX");
                case "MICRONESIA": return new Country("Micronesia", "FM", "FSM");
                case "MOLDOVA": return new Country("Moldova", "MD", "MDA");
                case "MONACO": return new Country("Monaco", "MC", "MCO");
                case "MONGOLIA": return new Country("Mongolia", "MN", "MNG");
                case "MONTENEGRO": return new Country("Montenegro", "ME", "MNE");
                case "MONTSERRAT": return new Country("Montserrat", "MS", "MSR");
                case "MOROCCO": return new Country("Morocco", "MA", "MAR");
                case "MOZAMBIQUE": return new Country("Mozambique", "MZ", "MOZ");
                case "MYANMAR": return new Country("Myanmar", "MM", "MMR");
                case "NAMIBIA": return new Country("Namibia", "NA", "NAM");
                case "NAURU": return new Country("Nauru", "NR", "NRU");
                case "NEPAL": return new Country("Nepal", "NP", "NPL");
                case "NETHERLANDS": return new Country("Netherlands", "NL", "NLD");
                case "NETHERLANDS ANTILLES": return new Country("Netherlands Antilles", "AN", "ANT");
                case "NEW CALEDONIA": return new Country("New Caledonia", "NC", "NCL");
                case "NEW ZEALAND": return new Country("New Zealand", "NZ", "NZL");
                case "NICARAGUA": return new Country("Nicaragua", "NI", "NIC");
                case "NIGER": return new Country("Niger", "NE", "NER");
                case "NIGERIA": return new Country("Nigeria", "NG", "NGA");
                case "NIUE": return new Country("Niue", "NU", "NIU");
                case "NORTH KOREA": return new Country("North Korea", "KP", "PRK");
                case "NORTHERN MARIANA ISLANDS": return new Country("Northern Mariana Islands", "MP", "MNP");
                case "NORWAY": return new Country("Norway", "NO", "NOR");
                case "OMAN": return new Country("Oman", "OM", "OMN");
                case "PAKISTAN": return new Country("Pakistan", "PK", "PAK");
                case "PALAU": return new Country("Palau", "PW", "PLW");
                case "PALESTINE": return new Country("Palestine", "PS", "PSE");
                case "PANAMA": return new Country("Panama", "PA", "PAN");
                case "PAPUA NEW GUINEA": return new Country("Papua New Guinea", "PG", "PNG");
                case "PARAGUAY": return new Country("Paraguay", "PY", "PRY");
                case "PERU": return new Country("Peru", "PE", "PER");
                case "PHILIPPINES": return new Country("Philippines", "PH", "PHL");
                case "PITCAIRN": return new Country("Pitcairn", "PN", "PCN");
                case "POLAND": return new Country("Poland", "PL", "POL");
                case "PORTUGAL": return new Country("Portugal", "PT", "PRT");
                case "PUERTO RICO": return new Country("Puerto Rico", "PR", "PRI");
                case "QATAR": return new Country("Qatar", "QA", "QAT");
                case "REPUBLIC OF THE CONGO": return new Country("Republic of the Congo", "CG", "COG");
                case "REUNION": return new Country("Reunion", "RE", "REU");
                case "ROMANIA": return new Country("Romania", "RO", "ROU");
                case "RUSSIA": return new Country("Russia", "RU", "RUS");
                case "RWANDA": return new Country("Rwanda", "RW", "RWA");
                case "SAINT BARTHELEMY": return new Country("Saint Barthelemy", "BL", "BLM");
                case "SAINT HELENA": return new Country("Saint Helena", "SH", "SHN");
                case "SAINT KITTS AND NEVIS": return new Country("Saint Kitts and Nevis", "KN", "KNA");
                case "SAINT LUCIA": return new Country("Saint Lucia", "LC", "LCA");
                case "SAINT MARTIN": return new Country("Saint Martin", "MF", "MAF");
                case "SAINT PIERRE AND MIQUELON": return new Country("Saint Pierre and Miquelon", "PM", "SPM");
                case "SAINT VINCENT AND THE GRENADINES": return new Country("Saint Vincent and the Grenadines", "VC", "VCT");
                case "SAMOA": return new Country("Samoa", "WS", "WSM");
                case "SAN MARINO": return new Country("San Marino", "SM", "SMR");
                case "SAO TOME AND PRINCIPE": return new Country("Sao Tome and Principe", "ST", "STP");
                case "SAUDI ARABIA": return new Country("Saudi Arabia", "SA", "SAU");
                case "SENEGAL": return new Country("Senegal", "SN", "SEN");
                case "SERBIA": return new Country("Serbia", "RS", "SRB");
                case "SEYCHELLES": return new Country("Seychelles", "SC", "SYC");
                case "SIERRA LEONE": return new Country("Sierra Leone", "SL", "SLE");
                case "SINGAPORE": return new Country("Singapore", "SG", "SGP");
                case "SINT MAARTEN": return new Country("Sint Maarten", "SX", "SXM");
                case "SLOVAKIA": return new Country("Slovakia", "SK", "SVK");
                case "SLOVENIA": return new Country("Slovenia", "SI", "SVN");
                case "SOLOMON ISLANDS": return new Country("Solomon Islands", "SB", "SLB");
                case "SOMALIA": return new Country("Somalia", "SO", "SOM");
                case "SOUTH AFRICA": return new Country("South Africa", "ZA", "ZAF");
                case "SOUTH KOREA": return new Country("South Korea", "KR", "KOR");
                case "SOUTH SUDAN": return new Country("South Sudan", "SS", "SSD");
                case "SPAIN": return new Country("Spain", "ES", "ESP");
                case "SRI LANKA": return new Country("Sri Lanka", "LK", "LKA");
                case "SUDAN": return new Country("Sudan", "SD", "SDN");
                case "SURINAME": return new Country("Suriname", "SR", "SUR");
                case "SVALBARD AND JAN MAYEN": return new Country("Svalbard and Jan Mayen", "SJ", "SJM");
                case "SWAZILAND": return new Country("Swaziland", "SZ", "SWZ");
                case "SWEDEN": return new Country("Sweden", "SE", "SWE");
                case "SWITZERLAND": return new Country("Switzerland", "CH", "CHE");
                case "SYRIA": return new Country("Syria", "SY", "SYR");
                case "TAIWAN": return new Country("Taiwan", "TW", "TWN");
                case "TAJIKISTAN": return new Country("Tajikistan", "TJ", "TJK");
                case "TANZANIA": return new Country("Tanzania", "TZ", "TZA");
                case "THAILAND": return new Country("Thailand", "TH", "THA");
                case "TOGO": return new Country("Togo", "TG", "TGO");
                case "TOKELAU": return new Country("Tokelau", "TK", "TKL");
                case "TONGA": return new Country("Tonga", "TO", "TON");
                case "TRINIDAD AND TOBAGO": return new Country("Trinidad and Tobago", "TT", "TTO");
                case "TUNISIA": return new Country("Tunisia", "TN", "TUN");
                case "TURKEY": return new Country("Turkey", "TR", "TUR");
                case "TURKMENISTAN": return new Country("Turkmenistan", "TM", "TKM");
                case "TURKS AND CAICOS ISLANDS": return new Country("Turks and Caicos Islands", "TC", "TCA");
                case "TUVALU": return new Country("Tuvalu", "TV", "TUV");
                case "U.S. VIRGIN ISLANDS": return new Country("U.S. Virgin Islands", "VI", "VIR");
                case "UGANDA": return new Country("Uganda", "UG", "UGA");
                case "UKRAINE": return new Country("Ukraine", "UA", "UKR");
                case "UNITED ARAB EMIRATES": return new Country("United Arab Emirates", "AE", "ARE");
                case "UNITED KINGDOM": return new Country("United Kingdom", "GB", "GBR");
                case "UNITED STATES": return new Country("United States", "US", "USA");
                case "URUGUAY": return new Country("Uruguay", "UY", "URY");
                case "UZBEKISTAN": return new Country("Uzbekistan", "UZ", "UZB");
                case "VANUATU": return new Country("Vanuatu", "VU", "VUT");
                case "VATICAN": return new Country("Vatican", "VA", "VAT");
                case "VENEZUELA": return new Country("Venezuela", "VE", "VEN");
                case "VIETNAM": return new Country("Vietnam", "VN", "VNM");
                case "WALLIS AND FUTUNA": return new Country("Wallis and Futuna", "WF", "WLF");
                case "WESTERN SAHARA": return new Country("Western Sahara", "EH", "ESH");
                case "YEMEN": return new Country("Yemen", "YE", "YEM");
                case "ZAMBIA": return new Country("Zambia", "ZM", "ZMB");
                case "ZIMBABWE": return new Country("Zimbabwe", "ZW", "ZWE");

                // 2 code
                case "AF": return new Country("Afghanistan", "AF", "AFG");
                case "AL": return new Country("Albania", "AL", "ALB");
                case "DZ": return new Country("Algeria", "DZ", "DZA");
                case "AS": return new Country("American Samoa", "AS", "ASM");
                case "AD": return new Country("Andorra", "AD", "AND");
                case "AO": return new Country("Angola", "AO", "AGO");
                case "AI": return new Country("Anguilla", "AI", "AIA");
                case "AQ": return new Country("Antarctica", "AQ", "ATA");
                case "AG": return new Country("Antigua and Barbuda", "AG", "ATG");
                case "AR": return new Country("Argentina", "AR", "ARG");
                case "AM": return new Country("Armenia", "AM", "ARM");
                case "AW": return new Country("Aruba", "AW", "ABW");
                case "AU": return new Country("Australia", "AU", "AUS");
                case "AT": return new Country("Austria", "AT", "AUT");
                case "AZ": return new Country("Azerbaijan", "AZ", "AZE");
                case "BS": return new Country("Bahamas", "BS", "BHS");
                case "BH": return new Country("Bahrain", "BH", "BHR");
                case "BD": return new Country("Bangladesh", "BD", "BGD");
                case "BB": return new Country("Barbados", "BB", "BRB");
                case "BY": return new Country("Belarus", "BY", "BLR");
                case "BE": return new Country("Belgium", "BE", "BEL");
                case "BZ": return new Country("Belize", "BZ", "BLZ");
                case "BJ": return new Country("Benin", "BJ", "BEN");
                case "BM": return new Country("Bermuda", "BM", "BMU");
                case "BT": return new Country("Bhutan", "BT", "BTN");
                case "BO": return new Country("Bolivia", "BO", "BOL");
                case "BA": return new Country("Bosnia and Herzegovina", "BA", "BIH");
                case "BW": return new Country("Botswana", "BW", "BWA");
                case "BR": return new Country("Brazil", "BR", "BRA");
                case "IO": return new Country("British Indian Ocean Territory", "IO", "IOT");
                case "VG": return new Country("British Virgin Islands", "VG", "VGB");
                case "BN": return new Country("Brunei", "BN", "BRN");
                case "BG": return new Country("Bulgaria", "BG", "BGR");
                case "BF": return new Country("Burkina Faso", "BF", "BFA");
                case "BI": return new Country("Burundi", "BI", "BDI");
                case "KH": return new Country("Cambodia", "KH", "KHM");
                case "CM": return new Country("Cameroon", "CM", "CMR");
                case "CA": return new Country("Canada", "CA", "CAN");
                case "CV": return new Country("Cape Verde", "CV", "CPV");
                case "KY": return new Country("Cayman Islands", "KY", "CYM");
                case "CF": return new Country("Central African Republic", "CF", "CAF");
                case "TD": return new Country("Chad", "TD", "TCD");
                case "CL": return new Country("Chile", "CL", "CHL");
                case "CN": return new Country("China", "CN", "CHN");
                case "CX": return new Country("Christmas Island", "CX", "CXR");
                case "CC": return new Country("Cocos Islands", "CC", "CCK");
                case "CO": return new Country("Colombia", "CO", "COL");
                case "KM": return new Country("Comoros", "KM", "COM");
                case "CK": return new Country("Cook Islands", "CK", "COK");
                case "CR": return new Country("Costa Rica", "CR", "CRI");
                case "HR": return new Country("Croatia", "HR", "HRV");
                case "CU": return new Country("Cuba", "CU", "CUB");
                case "CW": return new Country("Curacao", "CW", "CUW");
                case "CY": return new Country("Cyprus", "CY", "CYP");
                case "CZ": return new Country("Czech Republic", "CZ", "CZE");
                case "CD": return new Country("Democratic Republic of the Congo", "CD", "COD");
                case "DK": return new Country("Denmark", "DK", "DNK");
                case "DJ": return new Country("Djibouti", "DJ", "DJI");
                case "DM": return new Country("Dominica", "DM", "DMA");
                case "DO": return new Country("Dominican Republic", "DO", "DOM");
                case "TL": return new Country("East Timor", "TL", "TLS");
                case "EC": return new Country("Ecuador", "EC", "ECU");
                case "EG": return new Country("Egypt", "EG", "EGY");
                case "SV": return new Country("El Salvador", "SV", "SLV");
                case "GQ": return new Country("Equatorial Guinea", "GQ", "GNQ");
                case "ER": return new Country("Eritrea", "ER", "ERI");
                case "EE": return new Country("Estonia", "EE", "EST");
                case "ET": return new Country("Ethiopia", "ET", "ETH");
                case "FK": return new Country("Falkland Islands", "FK", "FLK");
                case "FO": return new Country("Faroe Islands", "FO", "FRO");
                case "FJ": return new Country("Fiji", "FJ", "FJI");
                case "FI": return new Country("Finland", "FI", "FIN");
                case "FR": return new Country("France", "FR", "FRA");
                case "PF": return new Country("French Polynesia", "PF", "PYF");
                case "GA": return new Country("Gabon", "GA", "GAB");
                case "GM": return new Country("Gambia", "GM", "GMB");
                case "GE": return new Country("Georgia", "GE", "GEO");
                case "DE": return new Country("Germany", "DE", "DEU");
                case "GH": return new Country("Ghana", "GH", "GHA");
                case "GI": return new Country("Gibraltar", "GI", "GIB");
                case "GR": return new Country("Greece", "GR", "GRC");
                case "GL": return new Country("Greenland", "GL", "GRL");
                case "GD": return new Country("Grenada", "GD", "GRD");
                case "GU": return new Country("Guam", "GU", "GUM");
                case "GT": return new Country("Guatemala", "GT", "GTM");
                case "GG": return new Country("Guernsey", "GG", "GGY");
                case "GN": return new Country("Guinea", "GN", "GIN");
                case "GW": return new Country("Guinea-Bissau", "GW", "GNB");
                case "GY": return new Country("Guyana", "GY", "GUY");
                case "HT": return new Country("Haiti", "HT", "HTI");
                case "HN": return new Country("Honduras", "HN", "HND");
                case "HK": return new Country("Hong Kong", "HK", "HKG");
                case "HU": return new Country("Hungary", "HU", "HUN");
                case "IS": return new Country("Iceland", "IS", "ISL");
                case "IN": return new Country("India", "IN", "IND");
                case "ID": return new Country("Indonesia", "ID", "IDN");
                case "IR": return new Country("Iran", "IR", "IRN");
                case "IQ": return new Country("Iraq", "IQ", "IRQ");
                case "IE": return new Country("Ireland", "IE", "IRL");
                case "IM": return new Country("Isle of Man", "IM", "IMN");
                case "IL": return new Country("Israel", "IL", "ISR");
                case "IT": return new Country("Italy", "IT", "ITA");
                case "CI": return new Country("Ivory Coast", "CI", "CIV");
                case "JM": return new Country("Jamaica", "JM", "JAM");
                case "JP": return new Country("Japan", "JP", "JPN");
                case "JE": return new Country("Jersey", "JE", "JEY");
                case "JO": return new Country("Jordan", "JO", "JOR");
                case "KZ": return new Country("Kazakhstan", "KZ", "KAZ");
                case "KE": return new Country("Kenya", "KE", "KEN");
                case "KI": return new Country("Kiribati", "KI", "KIR");
                case "XK": return new Country("Kosovo", "XK", "XKX");
                case "KW": return new Country("Kuwait", "KW", "KWT");
                case "KG": return new Country("Kyrgyzstan", "KG", "KGZ");
                case "LA": return new Country("Laos", "LA", "LAO");
                case "LV": return new Country("Latvia", "LV", "LVA");
                case "LB": return new Country("Lebanon", "LB", "LBN");
                case "LS": return new Country("Lesotho", "LS", "LSO");
                case "LR": return new Country("Liberia", "LR", "LBR");
                case "LY": return new Country("Libya", "LY", "LBY");
                case "LI": return new Country("Liechtenstein", "LI", "LIE");
                case "LT": return new Country("Lithuania", "LT", "LTU");
                case "LU": return new Country("Luxembourg", "LU", "LUX");
                case "MO": return new Country("Macau", "MO", "MAC");
                case "MK": return new Country("Macedonia", "MK", "MKD");
                case "MG": return new Country("Madagascar", "MG", "MDG");
                case "MW": return new Country("Malawi", "MW", "MWI");
                case "MY": return new Country("Malaysia", "MY", "MYS");
                case "MV": return new Country("Maldives", "MV", "MDV");
                case "ML": return new Country("Mali", "ML", "MLI");
                case "MT": return new Country("Malta", "MT", "MLT");
                case "MH": return new Country("Marshall Islands", "MH", "MHL");
                case "MR": return new Country("Mauritania", "MR", "MRT");
                case "MU": return new Country("Mauritius", "MU", "MUS");
                case "YT": return new Country("Mayotte", "YT", "MYT");
                case "MX": return new Country("Mexico", "MX", "MEX");
                case "FM": return new Country("Micronesia", "FM", "FSM");
                case "MD": return new Country("Moldova", "MD", "MDA");
                case "MC": return new Country("Monaco", "MC", "MCO");
                case "MN": return new Country("Mongolia", "MN", "MNG");
                case "ME": return new Country("Montenegro", "ME", "MNE");
                case "MS": return new Country("Montserrat", "MS", "MSR");
                case "MA": return new Country("Morocco", "MA", "MAR");
                case "MZ": return new Country("Mozambique", "MZ", "MOZ");
                case "MM": return new Country("Myanmar", "MM", "MMR");
                case "NA": return new Country("Namibia", "NA", "NAM");
                case "NR": return new Country("Nauru", "NR", "NRU");
                case "NP": return new Country("Nepal", "NP", "NPL");
                case "NL": return new Country("Netherlands", "NL", "NLD");
                case "AN": return new Country("Netherlands Antilles", "AN", "ANT");
                case "NC": return new Country("New Caledonia", "NC", "NCL");
                case "NZ": return new Country("New Zealand", "NZ", "NZL");
                case "NI": return new Country("Nicaragua", "NI", "NIC");
                case "NE": return new Country("Niger", "NE", "NER");
                case "NG": return new Country("Nigeria", "NG", "NGA");
                case "NU": return new Country("Niue", "NU", "NIU");
                case "KP": return new Country("North Korea", "KP", "PRK");
                case "MP": return new Country("Northern Mariana Islands", "MP", "MNP");
                case "NO": return new Country("Norway", "NO", "NOR");
                case "OM": return new Country("Oman", "OM", "OMN");
                case "PK": return new Country("Pakistan", "PK", "PAK");
                case "PW": return new Country("Palau", "PW", "PLW");
                case "PS": return new Country("Palestine", "PS", "PSE");
                case "PA": return new Country("Panama", "PA", "PAN");
                case "PG": return new Country("Papua New Guinea", "PG", "PNG");
                case "PY": return new Country("Paraguay", "PY", "PRY");
                case "PE": return new Country("Peru", "PE", "PER");
                case "PH": return new Country("Philippines", "PH", "PHL");
                case "PN": return new Country("Pitcairn", "PN", "PCN");
                case "PL": return new Country("Poland", "PL", "POL");
                case "PT": return new Country("Portugal", "PT", "PRT");
                case "PR": return new Country("Puerto Rico", "PR", "PRI");
                case "QA": return new Country("Qatar", "QA", "QAT");
                case "CG": return new Country("Republic of the Congo", "CG", "COG");
                case "RE": return new Country("Reunion", "RE", "REU");
                case "RO": return new Country("Romania", "RO", "ROU");
                case "RU": return new Country("Russia", "RU", "RUS");
                case "RW": return new Country("Rwanda", "RW", "RWA");
                case "BL": return new Country("Saint Barthelemy", "BL", "BLM");
                case "SH": return new Country("Saint Helena", "SH", "SHN");
                case "KN": return new Country("Saint Kitts and Nevis", "KN", "KNA");
                case "LC": return new Country("Saint Lucia", "LC", "LCA");
                case "MF": return new Country("Saint Martin", "MF", "MAF");
                case "PM": return new Country("Saint Pierre and Miquelon", "PM", "SPM");
                case "VC": return new Country("Saint Vincent and the Grenadines", "VC", "VCT");
                case "WS": return new Country("Samoa", "WS", "WSM");
                case "SM": return new Country("San Marino", "SM", "SMR");
                case "ST": return new Country("Sao Tome and Principe", "ST", "STP");
                case "SA": return new Country("Saudi Arabia", "SA", "SAU");
                case "SN": return new Country("Senegal", "SN", "SEN");
                case "RS": return new Country("Serbia", "RS", "SRB");
                case "SC": return new Country("Seychelles", "SC", "SYC");
                case "SL": return new Country("Sierra Leone", "SL", "SLE");
                case "SG": return new Country("Singapore", "SG", "SGP");
                case "SX": return new Country("Sint Maarten", "SX", "SXM");
                case "SK": return new Country("Slovakia", "SK", "SVK");
                case "SI": return new Country("Slovenia", "SI", "SVN");
                case "SB": return new Country("Solomon Islands", "SB", "SLB");
                case "SO": return new Country("Somalia", "SO", "SOM");
                case "ZA": return new Country("South Africa", "ZA", "ZAF");
                case "KR": return new Country("South Korea", "KR", "KOR");
                case "SS": return new Country("South Sudan", "SS", "SSD");
                case "ES": return new Country("Spain", "ES", "ESP");
                case "LK": return new Country("Sri Lanka", "LK", "LKA");
                case "SD": return new Country("Sudan", "SD", "SDN");
                case "SR": return new Country("Suriname", "SR", "SUR");
                case "SJ": return new Country("Svalbard and Jan Mayen", "SJ", "SJM");
                case "SZ": return new Country("Swaziland", "SZ", "SWZ");
                case "SE": return new Country("Sweden", "SE", "SWE");
                case "CH": return new Country("Switzerland", "CH", "CHE");
                case "SY": return new Country("Syria", "SY", "SYR");
                case "TW": return new Country("Taiwan", "TW", "TWN");
                case "TJ": return new Country("Tajikistan", "TJ", "TJK");
                case "TZ": return new Country("Tanzania", "TZ", "TZA");
                case "TH": return new Country("Thailand", "TH", "THA");
                case "TG": return new Country("Togo", "TG", "TGO");
                case "TK": return new Country("Tokelau", "TK", "TKL");
                case "TO": return new Country("Tonga", "TO", "TON");
                case "TT": return new Country("Trinidad and Tobago", "TT", "TTO");
                case "TN": return new Country("Tunisia", "TN", "TUN");
                case "TR": return new Country("Turkey", "TR", "TUR");
                case "TM": return new Country("Turkmenistan", "TM", "TKM");
                case "TC": return new Country("Turks and Caicos Islands", "TC", "TCA");
                case "TV": return new Country("Tuvalu", "TV", "TUV");
                case "VI": return new Country("U.S. Virgin Islands", "VI", "VIR");
                case "UG": return new Country("Uganda", "UG", "UGA");
                case "UA": return new Country("Ukraine", "UA", "UKR");
                case "AE": return new Country("United Arab Emirates", "AE", "ARE");
                case "GB": return new Country("United Kingdom", "GB", "GBR");
                case "US": return new Country("United States", "US", "USA");
                case "UY": return new Country("Uruguay", "UY", "URY");
                case "UZ": return new Country("Uzbekistan", "UZ", "UZB");
                case "VU": return new Country("Vanuatu", "VU", "VUT");
                case "VA": return new Country("Vatican", "VA", "VAT");
                case "VE": return new Country("Venezuela", "VE", "VEN");
                case "VN": return new Country("Vietnam", "VN", "VNM");
                case "WF": return new Country("Wallis and Futuna", "WF", "WLF");
                case "EH": return new Country("Western Sahara", "EH", "ESH");
                case "YE": return new Country("Yemen", "YE", "YEM");
                case "ZM": return new Country("Zambia", "ZM", "ZMB");
                case "ZW": return new Country("Zimbabwe", "ZW", "ZWE");

                //3 code
                case "AFG": return new Country("Afghanistan", "AF", "AFG");
                case "ALB": return new Country("Albania", "AL", "ALB");
                case "DZA": return new Country("Algeria", "DZ", "DZA");
                case "ASM": return new Country("American Samoa", "AS", "ASM");
                case "AND": return new Country("Andorra", "AD", "AND");
                case "AGO": return new Country("Angola", "AO", "AGO");
                case "AIA": return new Country("Anguilla", "AI", "AIA");
                case "ATA": return new Country("Antarctica", "AQ", "ATA");
                case "ATG": return new Country("Antigua and Barbuda", "AG", "ATG");
                case "ARG": return new Country("Argentina", "AR", "ARG");
                case "ARM": return new Country("Armenia", "AM", "ARM");
                case "ABW": return new Country("Aruba", "AW", "ABW");
                case "AUS": return new Country("Australia", "AU", "AUS");
                case "AUT": return new Country("Austria", "AT", "AUT");
                case "AZE": return new Country("Azerbaijan", "AZ", "AZE");
                case "BHS": return new Country("Bahamas", "BS", "BHS");
                case "BHR": return new Country("Bahrain", "BH", "BHR");
                case "BGD": return new Country("Bangladesh", "BD", "BGD");
                case "BRB": return new Country("Barbados", "BB", "BRB");
                case "BLR": return new Country("Belarus", "BY", "BLR");
                case "BEL": return new Country("Belgium", "BE", "BEL");
                case "BLZ": return new Country("Belize", "BZ", "BLZ");
                case "BEN": return new Country("Benin", "BJ", "BEN");
                case "BMU": return new Country("Bermuda", "BM", "BMU");
                case "BTN": return new Country("Bhutan", "BT", "BTN");
                case "BOL": return new Country("Bolivia", "BO", "BOL");
                case "BIH": return new Country("Bosnia and Herzegovina", "BA", "BIH");
                case "BWA": return new Country("Botswana", "BW", "BWA");
                case "BRA": return new Country("Brazil", "BR", "BRA");
                case "IOT": return new Country("British Indian Ocean Territory", "IO", "IOT");
                case "VGB": return new Country("British Virgin Islands", "VG", "VGB");
                case "BRN": return new Country("Brunei", "BN", "BRN");
                case "BGR": return new Country("Bulgaria", "BG", "BGR");
                case "BFA": return new Country("Burkina Faso", "BF", "BFA");
                case "BDI": return new Country("Burundi", "BI", "BDI");
                case "KHM": return new Country("Cambodia", "KH", "KHM");
                case "CMR": return new Country("Cameroon", "CM", "CMR");
                case "CAN": return new Country("Canada", "CA", "CAN");
                case "CPV": return new Country("Cape Verde", "CV", "CPV");
                case "CYM": return new Country("Cayman Islands", "KY", "CYM");
                case "CAF": return new Country("Central African Republic", "CF", "CAF");
                case "TCD": return new Country("Chad", "TD", "TCD");
                case "CHL": return new Country("Chile", "CL", "CHL");
                case "CHN": return new Country("China", "CN", "CHN");
                case "CXR": return new Country("Christmas Island", "CX", "CXR");
                case "CCK": return new Country("Cocos Islands", "CC", "CCK");
                case "COL": return new Country("Colombia", "CO", "COL");
                case "COM": return new Country("Comoros", "KM", "COM");
                case "COK": return new Country("Cook Islands", "CK", "COK");
                case "CRI": return new Country("Costa Rica", "CR", "CRI");
                case "HRV": return new Country("Croatia", "HR", "HRV");
                case "CUB": return new Country("Cuba", "CU", "CUB");
                case "CUW": return new Country("Curacao", "CW", "CUW");
                case "CYP": return new Country("Cyprus", "CY", "CYP");
                case "CZE": return new Country("Czech Republic", "CZ", "CZE");
                case "COD": return new Country("Democratic Republic of the Congo", "CD", "COD");
                case "DNK": return new Country("Denmark", "DK", "DNK");
                case "DJI": return new Country("Djibouti", "DJ", "DJI");
                case "DMA": return new Country("Dominica", "DM", "DMA");
                case "DOM": return new Country("Dominican Republic", "DO", "DOM");
                case "TLS": return new Country("East Timor", "TL", "TLS");
                case "ECU": return new Country("Ecuador", "EC", "ECU");
                case "EGY": return new Country("Egypt", "EG", "EGY");
                case "SLV": return new Country("El Salvador", "SV", "SLV");
                case "GNQ": return new Country("Equatorial Guinea", "GQ", "GNQ");
                case "ERI": return new Country("Eritrea", "ER", "ERI");
                case "EST": return new Country("Estonia", "EE", "EST");
                case "ETH": return new Country("Ethiopia", "ET", "ETH");
                case "FLK": return new Country("Falkland Islands", "FK", "FLK");
                case "FRO": return new Country("Faroe Islands", "FO", "FRO");
                case "FJI": return new Country("Fiji", "FJ", "FJI");
                case "FIN": return new Country("Finland", "FI", "FIN");
                case "FRA": return new Country("France", "FR", "FRA");
                case "PYF": return new Country("French Polynesia", "PF", "PYF");
                case "GAB": return new Country("Gabon", "GA", "GAB");
                case "GMB": return new Country("Gambia", "GM", "GMB");
                case "GEO": return new Country("Georgia", "GE", "GEO");
                case "DEU": return new Country("Germany", "DE", "DEU");
                case "GHA": return new Country("Ghana", "GH", "GHA");
                case "GIB": return new Country("Gibraltar", "GI", "GIB");
                case "GRC": return new Country("Greece", "GR", "GRC");
                case "GRL": return new Country("Greenland", "GL", "GRL");
                case "GRD": return new Country("Grenada", "GD", "GRD");
                case "GUM": return new Country("Guam", "GU", "GUM");
                case "GTM": return new Country("Guatemala", "GT", "GTM");
                case "GGY": return new Country("Guernsey", "GG", "GGY");
                case "GIN": return new Country("Guinea", "GN", "GIN");
                case "GNB": return new Country("Guinea-Bissau", "GW", "GNB");
                case "GUY": return new Country("Guyana", "GY", "GUY");
                case "HTI": return new Country("Haiti", "HT", "HTI");
                case "HND": return new Country("Honduras", "HN", "HND");
                case "HKG": return new Country("Hong Kong", "HK", "HKG");
                case "HUN": return new Country("Hungary", "HU", "HUN");
                case "ISL": return new Country("Iceland", "IS", "ISL");
                case "IND": return new Country("India", "IN", "IND");
                case "IDN": return new Country("Indonesia", "ID", "IDN");
                case "IRN": return new Country("Iran", "IR", "IRN");
                case "IRQ": return new Country("Iraq", "IQ", "IRQ");
                case "IRL": return new Country("Ireland", "IE", "IRL");
                case "IMN": return new Country("Isle of Man", "IM", "IMN");
                case "ISR": return new Country("Israel", "IL", "ISR");
                case "ITA": return new Country("Italy", "IT", "ITA");
                case "CIV": return new Country("Ivory Coast", "CI", "CIV");
                case "JAM": return new Country("Jamaica", "JM", "JAM");
                case "JPN": return new Country("Japan", "JP", "JPN");
                case "JEY": return new Country("Jersey", "JE", "JEY");
                case "JOR": return new Country("Jordan", "JO", "JOR");
                case "KAZ": return new Country("Kazakhstan", "KZ", "KAZ");
                case "KEN": return new Country("Kenya", "KE", "KEN");
                case "KIR": return new Country("Kiribati", "KI", "KIR");
                case "XKX": return new Country("Kosovo", "XK", "XKX");
                case "KWT": return new Country("Kuwait", "KW", "KWT");
                case "KGZ": return new Country("Kyrgyzstan", "KG", "KGZ");
                case "LAO": return new Country("Laos", "LA", "LAO");
                case "LVA": return new Country("Latvia", "LV", "LVA");
                case "LBN": return new Country("Lebanon", "LB", "LBN");
                case "LSO": return new Country("Lesotho", "LS", "LSO");
                case "LBR": return new Country("Liberia", "LR", "LBR");
                case "LBY": return new Country("Libya", "LY", "LBY");
                case "LIE": return new Country("Liechtenstein", "LI", "LIE");
                case "LTU": return new Country("Lithuania", "LT", "LTU");
                case "LUX": return new Country("Luxembourg", "LU", "LUX");
                case "MAC": return new Country("Macau", "MO", "MAC");
                case "MKD": return new Country("Macedonia", "MK", "MKD");
                case "MDG": return new Country("Madagascar", "MG", "MDG");
                case "MWI": return new Country("Malawi", "MW", "MWI");
                case "MYS": return new Country("Malaysia", "MY", "MYS");
                case "MDV": return new Country("Maldives", "MV", "MDV");
                case "MLI": return new Country("Mali", "ML", "MLI");
                case "MLT": return new Country("Malta", "MT", "MLT");
                case "MHL": return new Country("Marshall Islands", "MH", "MHL");
                case "MRT": return new Country("Mauritania", "MR", "MRT");
                case "MUS": return new Country("Mauritius", "MU", "MUS");
                case "MYT": return new Country("Mayotte", "YT", "MYT");
                case "MEX": return new Country("Mexico", "MX", "MEX");
                case "FSM": return new Country("Micronesia", "FM", "FSM");
                case "MDA": return new Country("Moldova", "MD", "MDA");
                case "MCO": return new Country("Monaco", "MC", "MCO");
                case "MNG": return new Country("Mongolia", "MN", "MNG");
                case "MNE": return new Country("Montenegro", "ME", "MNE");
                case "MSR": return new Country("Montserrat", "MS", "MSR");
                case "MAR": return new Country("Morocco", "MA", "MAR");
                case "MOZ": return new Country("Mozambique", "MZ", "MOZ");
                case "MMR": return new Country("Myanmar", "MM", "MMR");
                case "NAM": return new Country("Namibia", "NA", "NAM");
                case "NRU": return new Country("Nauru", "NR", "NRU");
                case "NPL": return new Country("Nepal", "NP", "NPL");
                case "NLD": return new Country("Netherlands", "NL", "NLD");
                case "ANT": return new Country("Netherlands Antilles", "AN", "ANT");
                case "NCL": return new Country("New Caledonia", "NC", "NCL");
                case "NZL": return new Country("New Zealand", "NZ", "NZL");
                case "NIC": return new Country("Nicaragua", "NI", "NIC");
                case "NER": return new Country("Niger", "NE", "NER");
                case "NGA": return new Country("Nigeria", "NG", "NGA");
                case "NIU": return new Country("Niue", "NU", "NIU");
                case "PRK": return new Country("North Korea", "KP", "PRK");
                case "MNP": return new Country("Northern Mariana Islands", "MP", "MNP");
                case "NOR": return new Country("Norway", "NO", "NOR");
                case "OMN": return new Country("Oman", "OM", "OMN");
                case "PAK": return new Country("Pakistan", "PK", "PAK");
                case "PLW": return new Country("Palau", "PW", "PLW");
                case "PSE": return new Country("Palestine", "PS", "PSE");
                case "PAN": return new Country("Panama", "PA", "PAN");
                case "PNG": return new Country("Papua New Guinea", "PG", "PNG");
                case "PRY": return new Country("Paraguay", "PY", "PRY");
                case "PER": return new Country("Peru", "PE", "PER");
                case "PHL": return new Country("Philippines", "PH", "PHL");
                case "PCN": return new Country("Pitcairn", "PN", "PCN");
                case "POL": return new Country("Poland", "PL", "POL");
                case "PRT": return new Country("Portugal", "PT", "PRT");
                case "PRI": return new Country("Puerto Rico", "PR", "PRI");
                case "QAT": return new Country("Qatar", "QA", "QAT");
                case "COG": return new Country("Republic of the Congo", "CG", "COG");
                case "REU": return new Country("Reunion", "RE", "REU");
                case "ROU": return new Country("Romania", "RO", "ROU");
                case "RUS": return new Country("Russia", "RU", "RUS");
                case "RWA": return new Country("Rwanda", "RW", "RWA");
                case "BLM": return new Country("Saint Barthelemy", "BL", "BLM");
                case "SHN": return new Country("Saint Helena", "SH", "SHN");
                case "KNA": return new Country("Saint Kitts and Nevis", "KN", "KNA");
                case "LCA": return new Country("Saint Lucia", "LC", "LCA");
                case "MAF": return new Country("Saint Martin", "MF", "MAF");
                case "SPM": return new Country("Saint Pierre and Miquelon", "PM", "SPM");
                case "VCT": return new Country("Saint Vincent and the Grenadines", "VC", "VCT");
                case "WSM": return new Country("Samoa", "WS", "WSM");
                case "SMR": return new Country("San Marino", "SM", "SMR");
                case "STP": return new Country("Sao Tome and Principe", "ST", "STP");
                case "SAU": return new Country("Saudi Arabia", "SA", "SAU");
                case "SEN": return new Country("Senegal", "SN", "SEN");
                case "SRB": return new Country("Serbia", "RS", "SRB");
                case "SYC": return new Country("Seychelles", "SC", "SYC");
                case "SLE": return new Country("Sierra Leone", "SL", "SLE");
                case "SGP": return new Country("Singapore", "SG", "SGP");
                case "SXM": return new Country("Sint Maarten", "SX", "SXM");
                case "SVK": return new Country("Slovakia", "SK", "SVK");
                case "SVN": return new Country("Slovenia", "SI", "SVN");
                case "SLB": return new Country("Solomon Islands", "SB", "SLB");
                case "SOM": return new Country("Somalia", "SO", "SOM");
                case "ZAF": return new Country("South Africa", "ZA", "ZAF");
                case "KOR": return new Country("South Korea", "KR", "KOR");
                case "SSD": return new Country("South Sudan", "SS", "SSD");
                case "ESP": return new Country("Spain", "ES", "ESP");
                case "LKA": return new Country("Sri Lanka", "LK", "LKA");
                case "SDN": return new Country("Sudan", "SD", "SDN");
                case "SUR": return new Country("Suriname", "SR", "SUR");
                case "SJM": return new Country("Svalbard and Jan Mayen", "SJ", "SJM");
                case "SWZ": return new Country("Swaziland", "SZ", "SWZ");
                case "SWE": return new Country("Sweden", "SE", "SWE");
                case "CHE": return new Country("Switzerland", "CH", "CHE");
                case "SYR": return new Country("Syria", "SY", "SYR");
                case "TWN": return new Country("Taiwan", "TW", "TWN");
                case "TJK": return new Country("Tajikistan", "TJ", "TJK");
                case "TZA": return new Country("Tanzania", "TZ", "TZA");
                case "THA": return new Country("Thailand", "TH", "THA");
                case "TGO": return new Country("Togo", "TG", "TGO");
                case "TKL": return new Country("Tokelau", "TK", "TKL");
                case "TON": return new Country("Tonga", "TO", "TON");
                case "TTO": return new Country("Trinidad and Tobago", "TT", "TTO");
                case "TUN": return new Country("Tunisia", "TN", "TUN");
                case "TUR": return new Country("Turkey", "TR", "TUR");
                case "TKM": return new Country("Turkmenistan", "TM", "TKM");
                case "TCA": return new Country("Turks and Caicos Islands", "TC", "TCA");
                case "TUV": return new Country("Tuvalu", "TV", "TUV");
                case "VIR": return new Country("U.S. Virgin Islands", "VI", "VIR");
                case "UGA": return new Country("Uganda", "UG", "UGA");
                case "UKR": return new Country("Ukraine", "UA", "UKR");
                case "ARE": return new Country("United Arab Emirates", "AE", "ARE");
                case "GBR": return new Country("United Kingdom", "GB", "GBR");
                case "USA": return new Country("United States", "US", "USA");
                case "URY": return new Country("Uruguay", "UY", "URY");
                case "UZB": return new Country("Uzbekistan", "UZ", "UZB");
                case "VUT": return new Country("Vanuatu", "VU", "VUT");
                case "VAT": return new Country("Vatican", "VA", "VAT");
                case "VEN": return new Country("Venezuela", "VE", "VEN");
                case "VNM": return new Country("Vietnam", "VN", "VNM");
                case "WLF": return new Country("Wallis and Futuna", "WF", "WLF");
                case "ESH": return new Country("Western Sahara", "EH", "ESH");
                case "YEM": return new Country("Yemen", "YE", "YEM");
                case "ZMB": return new Country("Zambia", "ZM", "ZMB");
                case "ZWE": return new Country("Zimbabwe", "ZW", "ZWE");
                default: return Unknown;
            }
        }

        /// <summary>
        /// Convert a State to String, uses `this.Code`
        /// </summary>
        /// <param name="value">The state object</param>
        static public implicit operator string(Country value)
        {
            return value.Code;
        }

        /// <summary>
        /// Convert a string to state object
        /// </summary>
        /// <param name="value">The string that should be a state name</param>
        public static implicit operator Country(string value)
        {
            return FromString(value);
        }

        /// <summary>
        /// Compares the State's names for inequality
        /// </summary>
        /// <param name="left">State object</param>
        /// <param name="right">State to compare</param>
        /// <returns>Returns true if they are not equal strings</returns>
        public static bool operator !=(Country left, Country right)
        {
            return !left.Equals(right);
        }

        /// <summary>
        /// Compares the State's names for equality
        /// </summary>
        /// <param name="left">State object</param>
        /// <param name="right">State to compare</param>
        /// <returns>Returns true if they are equal strings</returns>
        public static bool operator ==(Country left, Country right)
        {
            return left.Equals(right);
        }

        /// <summary>
        /// Compares the State name to the object string value
        /// </summary>
        /// <param name="obj">Other value to compare</param>
        /// <returns>Returns true if they are equal strings</returns>
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            return Name == obj.ToString();
        }

        /// <summary>
        /// Get the hash code from the state's name
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }

        /// <summary>
        /// The full name of the state
        /// </summary>
        /// <returns>`this.Name`</returns>
        public override string ToString()
        {
            return Name;
        }

        /// <summary>
        /// Compares the State name to the state's name
        /// </summary>
        /// <param name="other">Other value to compare</param>
        /// <returns>Returns true if they are equal strings</returns>
        public bool Equals(Country other)
        {
            return Code == other.Code;
        }

        /// <summary>
        /// An static read only value for an Unknown County
        /// </summary>
        public static Country Unknown => new Country("UNKNOWN", "", "");

        /// <summary>
        /// An static read only value for the United States
        /// </summary>
        public static Country UnitedStates => new Country("United States", "US", "USA");

        /// <summary>
        /// An static read only value for the United States
        /// </summary>
        public static Country Canada => new Country("Canada", "CA", "CAN");
    }
}

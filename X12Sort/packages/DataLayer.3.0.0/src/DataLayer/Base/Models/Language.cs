namespace DataLayer.Base.Models
{
    /// <summary>
    /// Language object
    /// </summary>
    public class Language : BaseDataObject, ICode
    {
        /// <summary>
        /// Code representing language
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Name of language
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Is spoken?
        /// </summary>
        public bool? IsSpoken { get; set; }

        /// <summary>
        /// Can read this language?
        /// </summary>
        public bool? CanRead { get; set; }
    }
}

using DataLayer.Base.Models.Converters;
using DataLayer.StaticUtilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Base.Models
{
    [JsonConverter(typeof(OhioCountyConverter))]
    public struct OhioCounty : IEquatable<OhioCounty>
    {
        private string _name;
        private int _code;

        public string Code => _code.ToString("00");
        public string Name => _name;

        public OhioCounty(string value)
        {
            _name = "";
            _code = -1;
            if (!string.IsNullOrWhiteSpace(value))
            {
                value = value.ToLower();
                int code;
                string name;
                if (int.TryParse(value, out code))
                {
                    if (_codeToNameMap.TryGetValue(code.ToString("00"), out name))
                    {
                        _name = name.ToTitleCase();
                        _code = code;
                    }
                }
                else
                {
                    name = value;
                    if (_nameToCodeMap.TryGetValue(name, out code))
                    {
                        _name = name.ToTitleCase();
                        _code = code;
                    }
                }

            }
        }

        public OhioCounty(int value)
        {
            _name = "";
            _code = -1;
            string name;
            if (_codeToNameMap.TryGetValue(value.ToString("00"), out name))
            {
                _name = name.ToTitleCase();
                _code = value;
            }
        }


        public override string ToString()
        {
            return _name;
        }

        static public implicit operator string(OhioCounty county)
        {
            return county._name;
        }

        static public int ToInt32(OhioCounty county)
        {
            return county._code;
        }

        public static OhioCounty FromString(string value)
        {
            return new OhioCounty(value);
        }

        public static OhioCounty FromInt(int value)
        {
            return new OhioCounty(value);
        }

        private static Dictionary<string, int> _nameToCodeMap = new Dictionary<string, int>() {
            { "adams" ,01},
            { "allen" ,02},
            { "ashland" ,03},
            { "ashtabula" ,04},
            { "athens" ,05},
            { "auglaize" ,06},
            { "belmont" ,07},
            { "brown" ,08},
            { "butler" ,09},
            { "carroll" ,10},
            { "champaign" ,11},
            { "clark" ,12},
            { "clermont" ,13},
            { "clinton" ,14},
            { "columbiana" ,15},
            { "coshocton" ,16},
            { "crawford" ,17},
            { "cuyahoga" ,18},
            { "darke" ,19},
            { "defiance" ,20},
            { "delaware" ,21},
            { "erie" ,22},
            { "fairfield" ,23},
            { "fayette" ,24},
            { "franklin" ,25},
            { "fulton" ,26},
            { "gallia" ,27},
            { "geauga" ,28},
            { "greene" ,29},
            { "guernsey" ,30},
            { "hamilton" ,31},
            { "hancock" ,32},
            { "hardin" ,33},
            { "harrison" ,34},
            { "henry" ,35},
            { "highland" ,36},
            { "hocking" ,37},
            { "holmes" ,38},
            { "huron" ,39},
            { "jackson" ,40},
            { "jefferson" ,41},
            { "knox" ,42},
            { "lake" ,43},
            { "lawrence" ,44},
            { "licking" ,45},
            { "logan" ,46},
            { "lorain" ,47},
            { "lucas" ,48},
            { "madison" ,49},
            { "mahoning" ,50},
            { "marion" ,51},
            { "medina" ,52},
            { "meigs" ,53},
            { "mercer" ,54},
            { "miami" ,55},
            { "monroe" ,56},
            { "montgomery" ,57},
            { "morgan" ,58},
            { "morrow" ,59},
            { "muskingum" ,60},
            { "noble" ,61},
            { "ottawa" ,62},
            { "paulding" ,63},
            { "perry" ,64},
            { "pickaway" ,65},
            { "pike" ,66},
            { "portage" ,67},
            { "preble" ,68},
            { "putnam" ,69},
            { "richland" ,70},
            { "ross" ,71},
            { "sandusky" ,72},
            { "scioto" ,73},
            { "seneca" ,74},
            { "shelby" ,75},
            { "stark" ,76},
            { "summit" ,77},
            { "trumbull" ,78},
            { "tuscarawas" ,79},
            { "union" ,80},
            { "van wert" ,81},
            { "vinton" ,82},
            { "warren" ,83},
            { "washington" ,84},
            { "wayne" ,85},
            { "williams" ,86},
            { "wood" ,87},
            { "wyandot" ,88},
        };

        private static Dictionary<string, string> _codeToNameMap = new Dictionary<string, string>() {
            { "01" , "adams" },
            { "02" , "allen" },
            { "03" , "ashland" },
            { "04" , "ashtabula" },
            { "05" , "athens" },
            { "06" , "auglaize" },
            { "07" , "belmont" },
            { "08" , "brown" },
            { "09" , "butler" },
            { "10" , "carroll" },
            { "11" , "champaign" },
            { "12" , "clark" },
            { "13" , "clermont" },
            { "14" , "clinton" },
            { "15" , "columbiana" },
            { "16" , "coshocton" },
            { "17" , "crawford" },
            { "18" , "cuyahoga" },
            { "19" , "darke" },
            { "20" , "defiance" },
            { "21" , "delaware" },
            { "22" , "erie" },
            { "23" , "fairfield" },
            { "24" , "fayette" },
            { "25" , "franklin" },
            { "26" , "fulton" },
            { "27" , "gallia" },
            { "28" , "geauga" },
            { "29" , "greene" },
            { "30" , "guernsey" },
            { "31" , "hamilton" },
            { "32" , "hancock" },
            { "33" , "hardin" },
            { "34" , "harrison" },
            { "35" , "henry" },
            { "36" , "highland" },
            { "37" , "hocking" },
            { "38" , "holmes" },
            { "39" , "huron" },
            { "40" , "jackson" },
            { "41" , "jefferson" },
            { "42" , "knox" },
            { "43" , "lake" },
            { "44" , "lawrence" },
            { "45" , "licking" },
            { "46" , "logan" },
            { "47" , "lorain" },
            { "48" , "lucas" },
            { "49" , "madison" },
            { "50" , "mahoning" },
            { "51" , "marion" },
            { "52" , "medina" },
            { "53" , "meigs" },
            { "54" , "mercer" },
            { "55" , "miami" },
            { "56" , "monroe" },
            { "57" , "montgomery" },
            { "58" , "morgan" },
            { "59" , "morrow" },
            { "60" , "muskingum" },
            { "61" , "noble" },
            { "62" , "ottawa" },
            { "63" , "paulding" },
            { "64" , "perry" },
            { "65" , "pickaway" },
            { "66" , "pike" },
            { "67" , "portage" },
            { "68" , "preble" },
            { "69" , "putnam" },
            { "70" , "richland" },
            { "71" , "ross" },
            { "72" , "sandusky" },
            { "73" , "scioto" },
            { "74" , "seneca" },
            { "75" , "shelby" },
            { "76" , "stark" },
            { "77" , "summit" },
            { "78" , "trumbull" },
            { "79" , "tuscarawas" },
            { "80" , "union" },
            { "81" , "van wert" },
            { "82" , "vinton" },
            { "83" , "warren" },
            { "84" , "washington" },
            { "85" , "wayne" },
            { "86" , "williams" },
            { "87" , "wood" },
            { "88" , "wyandot" }
        };

        /// <summary>
        /// Not equals comparison operator
        /// </summary>
        /// <param name="obj1"></param>
        /// <param name="obj2"></param>
        /// <returns></returns>
        public static bool operator !=(OhioCounty obj1, OhioCounty obj2)
        {
            return !(obj1 == obj2);
        }

        /// <summary>
        /// Equals comparison operator
        /// </summary>
        /// <param name="obj1"></param>
        /// <param name="obj2"></param>
        /// <returns></returns>
        public static bool operator ==(OhioCounty obj1, OhioCounty obj2)
        {
            if (ReferenceEquals(obj1, obj2))
            {
                return true;
            }

            if (ReferenceEquals(obj1, null) || ReferenceEquals(obj2, null))
            {
                return false;
            }

            bool nameMatch = (obj1._name ?? "").ToLower() == (obj2._name ?? "").ToLower();
            bool codeMatch = obj1._code == obj2._code;

            return nameMatch && codeMatch;
        }

        /// <summary>
        /// Compares two Schema obeject to see if they are the same
        /// </summary>
        /// <param name="other">The other schema object</param>
        /// <returns></returns>
        public bool Equals(OhioCounty other)
        {
           return (this == other);
        }

        /// <summary>
        /// Compares two Schema obeject to see if they are the same
        /// </summary>
        /// <param name="other">The other schema object</param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            // validate the columns schemas
            if (obj == null || !(obj is OhioCounty))
                return false;

            return (this == (OhioCounty)obj);
        }

        /// <summary>
        /// Gets the hash code for the current object
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            //get hash code theory: https://blogs.msdn.microsoft.com/ericlippert/2011/02/28/guidelines-and-rules-for-gethashcode/
            //implementation idea borrowed from: https://rehansaeed.com/gethashcode-made-easy/
            unchecked
            {
                int hashCode = 17;
                hashCode = (hashCode * 23) + (_name == null ? 0 : _name.GetHashCode());
                hashCode = (hashCode * 23) + (_code.GetHashCode());

                return hashCode;
            }
        }

    }
}

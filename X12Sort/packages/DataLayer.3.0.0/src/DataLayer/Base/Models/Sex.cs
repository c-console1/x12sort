using DataLayer.Base.Models.Converters;
using DataLayer.StaticUtilities;
using DataLayer.StaticUtilities.Extensions;
using Newtonsoft.Json;
using System;
using System.Linq;

namespace DataLayer.Base.Models
{
    /// <summary>
    /// A struct to represent a person's sex
    /// </summary>
    /// <remarks>
    /// <para>
    /// This is built with the assumption that sex is represented as a binary: Male = `true`, Female = `False`
    /// </para>
    /// <para>
    /// Using a struct because this is conceptually an immutable value type.
    /// </para>
    /// </remarks>
    /// <example>
    /// This is how you can assign the values to object
    /// <code>
    /// <![CDATA[
    /// Sex female = "female";
    /// Sex male = true;
    /// Sex unknown = null
    /// Sex male == "F" // false
    /// ]]>
    /// </code>
    /// </example>
    [JsonConverter(typeof(SexConverter))]
    public struct Sex : IEquatable<Sex>
    {
        private readonly bool? _isMale;

        /// <summary>
        /// Single character representation: M, F, ""
        /// </summary>
        public string Code
        {
            get
            {
                if (_isMale.HasValue)
                {
                    return _isMale.Value ? "M" : "F";
                }
                else
                {
                    return "";
                }
            }
        }

        /// <summary>
        /// The full description of the sex: Male, Female, Unknown
        /// </summary>
        [JsonIgnore]
        public string Description
        {
            get
            {
                if (_isMale.HasValue)
                {
                    return _isMale.Value ? "Male" : "Female";
                }
                else
                {
                    return "Unknown";
                }
            }
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="value">binary value</param>
        public Sex(bool value)
        {
            _isMale = value;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="value">null-able binary value</param>
        public Sex(bool? value)
        {
            _isMale = value;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="value">string value that is converted</param>
        public Sex(string value)
        {
            _isMale = null;

            if (!string.IsNullOrEmpty(value))
            {
                value.ThrowIfNull(nameof(value));
                char c = value.Sanitize().ToLower().FirstOrDefault();

                switch (c)
                {
                    case 'm':
                        _isMale = true;
                        break;

                    case 'w':
                    case 'f':
                        _isMale = false;
                        break;

                    default:
                        // do nothing here, value set above
                        break;
                }
            }
        }

        public Sex(Sex basis)
        {
            _isMale = basis._isMale;
        }

        /// <summary>
        /// Convert boolean to Sex
        /// </summary>
        /// <param name="value">The boolean to convert, male = `true` </param>
        public static Sex FromBoolean(bool value)
        {
            return new Sex(value);
        }

        /// <summary>
        /// Convert null-able boolean to Sex
        /// </summary>
        /// <param name="value">The boolean to convert, male = `true` </param>
        public static Sex FromNullable(bool? value)
        {
            return new Sex(value);
        }

        /// <summary>
        /// Implicit convert string to Sex
        /// </summary>
        /// <param name="value">The string to convert, should be start with "m" or "f"</param>
        public static Sex FromString(string value)
        {
            return new Sex(value);
        }

        /// <summary>
        /// Implicit convert Sex to null-able boolean value
        /// </summary>
        /// <param name="value">The Sex instance to convert</param>
        public static implicit operator bool?(Sex value)
        {
            return value._isMale;
        }

        /// <summary>
        /// Implicit convert string to Sex
        /// </summary>
        /// <param name="value">The string to convert, should be start with "m" or "f"</param>
        public static implicit operator Sex(string value)
        {
            return FromString(value);
        }

        /// <summary>
        /// Implicit convert boolean to Sex
        /// </summary>
        /// <param name="value">The boolean to convert, male = `true` </param>
        public static implicit operator Sex(bool value)
        {
            return FromBoolean(value);
        }

        /// <summary>
        /// Implicit convert null-able boolean to Sex
        /// </summary>
        /// <param name="value">The boolean to convert, male = `true` </param>
        public static implicit operator Sex(bool? value)
        {
            return FromNullable(value);
        }

        /// <summary>
        /// Implicit convert Sex to string value
        /// </summary>
        /// <param name="value">The Sex instance to convert</param>
        public static implicit operator string(Sex value)
        {
            return value.Description;
        }

        /// <summary>
        /// Compares Sex objects for incompatibilities
        /// </summary>
        /// <param name="left">The Sex to compare to </param>
        /// <param name="right">The partner to compare</param>
        /// <returns>Returns true if the sexes are not compatible</returns>
        public static bool operator !=(Sex left, Sex right)
        {
            return !left.Equals(right);
        }

        /// <summary>
        /// Compares Sex objects
        /// </summary>
        /// <param name="left">The Sex to compare to </param>
        /// <param name="right">The partner to compare</param>
        /// <returns>Returns true if the sexes are compatible</returns>
        public static bool operator ==(Sex left, Sex right)
        {
            return left.Equals(right);
        }

        /// <summary>
        /// Compares Sex objects
        /// </summary>
        /// <param name="obj">The other Sex</param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            return Code == obj.ToString();
        }

        /// <summary>
        /// Get's the hash code for the Sex
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return Description.GetHashCode();
        }

        /// <summary>
        /// Convert Sex to null-able boolean value
        /// </summary>
        public bool? ToNullable()
        {
            return _isMale;
        }

        /// <summary>
        /// Custom ToString() implementation
        /// </summary>
        /// <returns>`this.Title`</returns>
        public override string ToString()
        {
            return Description;
        }

        /// <summary>
        /// Compares Sex objects
        /// </summary>
        /// <param name="other">The other Sex</param>
        /// <returns></returns>
        public bool Equals(Sex other)
        {
            return Description == other.Description;
        }
    }
}

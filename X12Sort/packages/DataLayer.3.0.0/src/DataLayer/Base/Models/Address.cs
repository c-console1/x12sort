using System;
using System.Collections.Generic;
using System.Linq;
using DataLayer.Base.Enumerations;
using DataLayer.StaticUtilities;
using DataLayer.StaticUtilities.Extensions;

namespace DataLayer.Base.Models
{
    /// <summary>
    /// Full featured address object that should handle most use cases
    /// </summary>
    /// <seealso cref="IAddress"/>
    /// <seealso cref="IAddressExtensions"/>
    public class Address : BaseDataObject, IAddress, ICoordiantes, IEquatable<IAddress>
    {
        #region Implements IAddress
        /// <summary>
        /// The human name of the address, ex: Toledo Hospital
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The first line of the address, ex: 2142 N Cove Blvd
        /// </summary>
        public string Address1 { get; set; }

        /// <summary>
        /// The second line of the address
        /// </summary>
        public string Address2 { get; set; }

        /// <summary>
        /// The city of the address, ex: Toledo
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// The State object of the address, ex: OH
        /// </summary>
        public USState State { get; set; } = USState.Unknown;

        /// <summary>
        /// The county of the address, Lucas
        /// </summary>
        public string County { get; set; }

        /// <summary>
        /// The country of the address, ex: USA
        /// </summary>
        public Country Country { get; set; } = Country.UnitedStates;

        /// <summary>
        /// the zip/postal code of the address, ex: 43606
        /// </summary>
        public ZipCode ZipCode { get; set; } = ZipCode.Unknown;
        #endregion

        #region Implements ICoordiantes
        /// <summary>
        /// Latitude of the address location, ex: 41.6711
        /// </summary>
        public decimal? Latitude { get; set; } = null;

        /// <summary>
        /// Longitude of the address location, ex: -83.59454
        /// </summary>
        public decimal? Longitude { get; set; } = null;
        #endregion

        /// <summary>
        /// List of phone numbers associated with address
        /// </summary>
        public IEnumerable<PhoneNumber> PhoneNumbers { get; set; }

        /// <summary>
        /// List of fax numbers associated with address
        /// </summary>
        public IEnumerable<PhoneNumber> FaxNumbers { get; set; }

        /// <summary>
        /// List of email addresses associated with the address
        /// </summary>
        public IEnumerable<string> EmailAddresses { get; set; }

        /// <summary>
        /// The priority of the address
        /// </summary>
        public AddressPriority Priority { get; set; } = AddressPriority.Unknown;

        /// <summary>
        /// Determines whether this address has any phone numbers
        /// </summary>
        public bool HasPhoneNumber { get { return PhoneNumbers == default ? false : PhoneNumbers.Any(); } }

        /// <summary>
        /// Determines whether this address has any fax numbers
        /// </summary>
        public bool HasFaxNumber { get { return FaxNumbers == default ? false : FaxNumbers.Any(); } }

        /// <summary>
        /// Determines whether this address has any email addresses
        /// </summary>
        public bool HasEmailAddress { get { return EmailAddresses == default ? false : EmailAddresses.Any(); } }

        /// <summary>
        /// Empty Constructor, initializes all the fields
        /// </summary>
        public Address() : base() { }

        /// <summary>
        /// Constructor, initialize with an address
        /// </summary>
        /// <param name="address">And address string</param>
        /// <remarks>
        /// <para>
        /// code consuming the results of this function will need AddressParser by Jim Counts from NuGet: https://www.nuget.org/packages/AddressParser/
        /// </para>
        /// </remarks>
        public Address(string address)
        {
            USAddress.AddressParseResult parserResult = AddressManipulation.Parser.ParseAddress(address);
            if (parserResult != null)
            {
                if (!string.IsNullOrEmpty(parserResult.Street))
                {
                    Address1 = (parserResult.Number + " " + parserResult.Predirectional + " " + parserResult.Street + " " + parserResult.Suffix + " " + parserResult.Postdirectional).StripMultipleWhitespace().ToTitleCase();
                }
                else
                {
                    Address1 = parserResult.StreetLine.StripMultipleWhitespace().ToTitleCase();
                }
                Address2 = (parserResult.SecondaryUnit + " " + parserResult.SecondaryNumber).StripMultipleWhitespace().ToTitleCase();
                City = parserResult.City.ToTitleCase();
                State = parserResult.State;
                if (parserResult.Zip.Length > 5)
                    ZipCode = parserResult.Zip.Substring(0, 5);
                else
                    ZipCode = parserResult.Zip;
            }
        }

        public static bool operator !=(Address obj1, IAddress obj2)
        {
            return !(obj1 == obj2);
        }

        public static bool operator ==(Address obj1, IAddress obj2)
        {
            if (ReferenceEquals(obj1, obj2))
            {
                return true;
            }

            if (ReferenceEquals(obj1, null) || ReferenceEquals(obj2, null))
            {
                return false;
            }

            bool address1Match = (obj1.Address1 ?? "").ToLower() == (obj2.Address1 ?? "").ToLower();
            bool address2Match = (obj1.Address2 ?? "").ToLower() == (obj2.Address2 ?? "").ToLower();
            bool cityMatch = (obj1.City ?? "").ToLower() == (obj2.City ?? "").ToLower();
            bool countyMatch = (obj1.County ?? "").ToLower() == (obj2.County ?? "").ToLower();
            bool stateMatch = obj1.State == obj2.State;
            bool zipcodeMatch = obj1.ZipCode == obj2.ZipCode;

            return address1Match && address2Match && cityMatch && countyMatch && stateMatch && zipcodeMatch;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is IAddress))
                return false;
            IAddress address = obj as IAddress;
            return (this == address);
        }

        public bool Equals(IAddress other)
        {
            {
                if (other == null || !(other is IAddress))
                    return false;
                return (this == other);
            }
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        /// <summary>
        /// Override of ToString, calls `Format()`
        /// </summary>
        /// <returns></returns>
        /// <seealso cref="IAddressExtensions.Format(IAddress, bool)"/>
        public override string ToString()
        {
            return this.Format();
        }
    }
}

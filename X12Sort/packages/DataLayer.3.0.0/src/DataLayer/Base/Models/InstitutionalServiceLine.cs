using System;
using System.Collections.Generic;
using System.Linq;
using DataLayer.Base.Enumerations;
using DataLayer.StaticUtilities.Extensions;

namespace DataLayer.Base.Models
{
    public class InstitutionalServiceLine : BaseDataObject, IServiceLine
    {
        #region Implement IServiceLine
        /// <summary>
        /// Type of claim
        /// </summary>
        public ClaimType Type { get; set; }

        /// <summary>
        /// Number representing service line in internal system
        /// </summary>
        public string ServiceLineNumber { get; set; }

        /// <summary>
        /// Approval/denial status of service line
        /// </summary>
        public ServiceLineStatus Status { get; set; }

        /// <summary>
        /// Date that the service line was received by the company
        /// </summary>
        public DateTime? DateReceived { get; set; }

        /// <summary>
        /// Date that the service line was digitally entered into the system
        /// </summary>
        public DateTime? DateEntered { get; set; }

        /// <summary>
        /// Date that the service line was paid
        /// </summary>
        public DateTime? DatePaid { get; set; }

        /// <summary>
        /// Dollar ammount that was billed to the insurance company
        /// </summary>
        public decimal AmountBilled { get; set; }

        /// <summary>
        /// Dollar amount allowed to be billed from servicing provider
        /// </summary>
        public decimal AmountAllowed { get; set; }

        /// <summary>
        /// Dollar amount that was paid out by the insurance company
        /// </summary>
        public decimal AmountPaid { get; set; }

        /// <summary>
        /// Dollar amount that was paid for by a third party
        /// </summary>
        public decimal AmountPaidByThirdParty { get; set; }

        /// <summary>
        /// Dollar amount owed by member
        /// </summary>
        public decimal AmountOwed { get; set; }
        #endregion

        #region Implements IStartEnd
        /// <summary>
        /// Effective date of service line
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// End date of service line
        /// </summary>
        public DateTime? EndDate { get; set; }
        #endregion

        /// <summary>
        /// Dates of service for service line using StartDate and EndDate
        /// </summary>
        public StartEndRange DatesOfService { get { return this.GetRange(); } }

        /// <summary>
        /// Servicing provider number
        /// </summary>
        public string ProviderNumber { get; set; }

        /// <summary>
        /// Principal diagnosis code used to justify the service line
        /// </summary>
        public DiagnosisCode PrincipalDiagnosis { get { return DiagnosisCodes == default ? default : DiagnosisCodes.FirstOrDefault(); } }

        /// <summary>
        /// List of diagnosis codes
        /// </summary>
        public List<DiagnosisCode> DiagnosisCodes { get; set; }

        /// <summary>
        /// List of procedure codes
        /// </summary>
        public List<ProcedureCode> ProcedureCodes { get; set; }
    }
}

using DataLayer.Base.Enumerations;

namespace DataLayer.Base.Models
{
    /// <summary>
    /// Claim procedure code
    /// </summary>
    public class ProcedureCode : BaseDataObject, ICode
    {
        /// <summary>
        /// Type of claim procedure code
        /// </summary>
        public ProcedureCodeType Type { get; set; }

        /// <summary>
        /// Actual procedure code value
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Short description of procedure code
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Extended description of procedure code
        /// </summary>
        public string ExtendedDescription { get; set; }
}
}

namespace DataLayer.Base.Models
{
    /// <summary>
    /// Bottom Layer of all Database objects.
    /// </summary>
    /// <remarks>
    /// The Goal for this object is to have a common object across all data objects in the data layer
    /// </remarks>
    public interface IForeignBaseDataObject
    {
        /// <summary>
        /// The foreign key of the database record
        /// </summary>
        string ForeignDatabaseKey { get; set; }
    }
}

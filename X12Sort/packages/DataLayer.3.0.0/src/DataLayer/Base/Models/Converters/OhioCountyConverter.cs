using Newtonsoft.Json;
using System;

namespace DataLayer.Base.Models.Converters
{
    /// <summary>
    /// Class that is used to convert the county to and from JSON
    /// </summary>
    internal class OhioCountyConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(OhioCounty);
        }
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return new OhioCounty(serializer.Deserialize(reader) as string);
        }
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            serializer.Serialize(writer, ((OhioCounty)value).Code);
        }
    }
}

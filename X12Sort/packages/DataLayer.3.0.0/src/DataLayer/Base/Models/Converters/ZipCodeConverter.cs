using Newtonsoft.Json;
using System;

namespace DataLayer.Base.Models.Converters
{
    /// <summary>
    /// Internal class used to convert ZipCode to and from JSON
    /// </summary>
    internal class ZipCodeConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(USState);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return ZipCode.FromString(serializer.Deserialize(reader) as string);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            serializer.Serialize(writer, ((ZipCode)value).ToString());
        }
    }
}

using Newtonsoft.Json;
using System;
using DataLayer.Base.Enumerations;
using DataLayer.StaticUtilities.Extensions;

namespace DataLayer.Base.Models.Converters
{
    /// <summary>
    /// The logic to convert Sex to and from JSON
    /// </summary>
    internal class SexConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(Sex);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return FromString(serializer.Deserialize(reader) as string);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            serializer.Serialize(writer, ((Sex)value).Code);
        }

        public static Sex FromString(string value)
        {
            Sex sex = new Sex();

            if (!string.IsNullOrEmpty(value))
            {

                switch (value.ToLower().Trim())
                {
                    case "m":
                    case "male":
                        sex = "m";
                        break;
                    case "f":
                    case "female":
                        sex="f";
                        break;
                    default:
                        break;
                }
            }

            return sex;
        }

    }
}

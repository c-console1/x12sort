namespace DataLayer.Base.Models
{
    public interface IAuthorizationProvider
    {
        /// <summary>
        /// Provider identifier
        /// </summary>
        public string ID { get; set; }
        /// <summary>
        /// Name of provider
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// External id if present
        /// </summary>
        public string ExternalNumber { get; set; }
        /// <summary>
        /// NPI
        /// </summary>
        public string NPI { get; set; }
        /// <summary>
        /// TIN
        /// </summary>
        public string TaxID { get; set; }
    }
}

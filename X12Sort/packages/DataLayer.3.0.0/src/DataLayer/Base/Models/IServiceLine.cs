using System;
using System.Collections.Generic;
using DataLayer.Base.Enumerations;

namespace DataLayer.Base.Models
{
    public interface IServiceLine : IStartEnd
    {
        /// <summary>
        /// Type of claim
        /// </summary>
        ClaimType Type { get; set; }

        /// <summary>
        /// Number representing service line in internal system
        /// </summary>
        string ServiceLineNumber { get; set; }

        /// <summary>
        /// Approval/denial status of service line
        /// </summary>
        ServiceLineStatus Status { get; set; }

        /// <summary>
        /// Date that the service line was received by the company
        /// </summary>
        DateTime? DateReceived { get; set; }

        /// <summary>
        /// Date that the service line was digitally entered into the system
        /// </summary>
        DateTime? DateEntered { get; set; }

        /// <summary>
        /// Date that the service line was paid
        /// </summary>
        DateTime? DatePaid { get; set; }

        /// <summary>
        /// Dollar ammount that was billed to the insurance company
        /// </summary>
        decimal AmountBilled { get; set; }

        /// <summary>
        /// Dollar amount allowed to be billed from servicing provider
        /// </summary>
        decimal AmountAllowed { get; set; }

        /// <summary>
        /// Dollar amount that was paid out by the insurance company
        /// </summary>
        decimal AmountPaid { get; set; }

        /// <summary>
        /// Dollar amount that was paid for by a third party
        /// </summary>
        decimal AmountPaidByThirdParty { get; set; }

        /// <summary>
        /// Dollar amount owed by member
        /// </summary>
        decimal AmountOwed { get; set; }
    }
}

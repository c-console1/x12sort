using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.Base.Models
{
    public interface IOptionValue
    {
        /// <summary>
        /// Option value ID
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// OptionValue label
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// Option value value
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Bool indicating if disabled or not
        /// </summary>
        public bool Disabled { get; set; }
    }
}

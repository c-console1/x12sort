using DataLayer.Base.Models.Converters;
using Newtonsoft.Json;
using System;
using System.Text.RegularExpressions;

namespace DataLayer.Base.Models
{
    /// <summary>
    /// US Zip Code
    /// </summary>
    /// <remarks>
    /// <para>
    /// Using a struct because this is conceptually an immutable value type.
    /// </para>
    /// <para>
    /// Only allows assigning valid zip codes, either 5, or 9 digit. If given an invalid value, the zip will be empty.
    /// </para>
    /// <para>
    /// Regex from: https://www.oreilly.com/library/view/regular-expressions-cookbook/9781449327453/ch04s14.html
    /// Modified to allow for optional zip code separator
    /// </para>
    /// </remarks>
    /// <example>
    /// This is how you can assign the values to object
    /// <code>
    /// <![CDATA[
    /// ZipCode zip = 32456;
    /// ZipCode zip9 = "43537-4002";
    /// ]]>
    /// </code>
    /// </example>
    [JsonConverter(typeof(ZipCodeConverter))]
    public struct ZipCode : IEquatable<ZipCode>
    {
        private static readonly Regex _zipCodeRegex = new Regex(@"^(?<five>[0-9]{5})(?:-?(?<four>[0-9]{4}))?$");

        private readonly bool _validUsCode;
        private readonly string _raw;

        /// <summary>
        /// The five digit zip code
        /// </summary>
        public string Five { get; }

        /// <summary>
        /// The four additional digits
        /// </summary>
        public string Four { get; }

        /// <summary>
        /// The 5+4 digit ZipCode
        /// </summary>
        public string Full => $"{Five}-{Four}";

        /// <summary>
        /// Basic Constructor
        /// </summary>
        /// <param name="value">Either 5 or 5+4 ZipCode</param>
        public ZipCode(string value)
        {
            Five = string.Empty;
            Four = string.Empty;
            _raw = value?.Trim();
            _validUsCode = false;
            if (!string.IsNullOrEmpty(_raw))
            {
                Match match = _zipCodeRegex.Match(_raw);
                if (match.Success)
                {
                    _validUsCode = true;
                    Five = match.Groups["five"].Value;
                    if (match.Groups["four"].Success)
                    {
                        Four = match.Groups["four"].Value;
                    }
                }
            }
        }

        /// <summary>
        /// Basic Constructor
        /// </summary>
        /// <param name="value">Either 5 or 5+4 ZipCode</param>
        public ZipCode(int value) : this(value.ToString())
        {
        }
        /// <summary>
        /// Convert a int to `ZipCode`
        /// </summary>
        /// <param name="value">The int that represents a `ZipCode`</param>
        public static ZipCode FromInt32(int value)
        {
            return new ZipCode(value.ToString());
        }

        /// <summary>
        /// Convert a string to ZipCode
        /// </summary>
        /// <param name="value">The string that should be a `ZipCode`</param>
        public static ZipCode FromString(string value)
        {
            return new ZipCode(value);
        }

        /// <summary>
        /// Convert a `ZipCode` to int, only the first 5 digits
        /// </summary>
        /// <param name="value">The state object</param>
        static public implicit operator int(ZipCode value)
        {
            return value.ToInt32();
        }

        /// <summary>
        /// Convert a ZipCode to String
        /// </summary>
        /// <param name="value">The `ZipCode` object</param>
        static public implicit operator string(ZipCode value)
        {
            return value.ToString();
        }

        /// <summary>
        /// Convert a string to ZipCode
        /// </summary>
        /// <param name="value">The string that should be a `ZipCode`</param>
        public static implicit operator ZipCode(string value)
        {
            return FromString(value);
        }

        /// <summary>
        /// Convert a int to `ZipCode`
        /// </summary>
        /// <param name="value">The int that represents a `ZipCode`</param>
        public static implicit operator ZipCode(int value)
        {
            return FromInt32(value);
        }

        /// <summary>
        /// Compares two ZipCode objects, only uses all nine digits if they both have them
        /// </summary>
        /// <param name="left">The zip code</param>
        /// <param name="right">The other zip code to compare</param>
        /// <returns></returns>
        public static bool operator !=(ZipCode left, ZipCode right)
        {
            return !left.Equals(right);
        }

        /// <summary>
        /// Compares two ZipCode objects, only uses all nine digits if they both have them
        /// </summary>
        /// <param name="left">The zip code</param>
        /// <param name="right">The other zip code to compare</param>
        /// <returns></returns>
        public static bool operator ==(ZipCode left, ZipCode right)
        {
            return left.Equals(right);
        }

        /// <summary>
        /// Compares two ZipCode objects, only uses all nine digits if they both have them
        /// </summary>
        /// <param name="other">The other zip code</param>
        /// <returns></returns>
        public bool Equals(ZipCode other)
        {
            if (string.IsNullOrEmpty(other.Four) || string.IsNullOrEmpty(other.Four))
            {
                return (Five == other.Five);
            }
            else
            {
                return (Five == other.Five) && (Four == other.Four);
            }
        }

        /// <summary>
        /// Compares two ZipCode objects, only uses all nine digits if they both have them
        /// </summary>
        /// <param name="obj">The other zip code</param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (obj is int) return (int)obj == this;
            if (obj is string) return (string)obj == ToString();
            return false;
        }

        /// <summary>
        /// Returns the hash code of `ToString()`
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        /// <summary>
        /// Convert a `ZipCode` to int, only the first 5 digits
        /// </summary>
        public int ToInt32()
        {
            return Convert.ToInt32(Five);
        }

        /// <summary>
        /// The ZipCode, full zip code if available
        /// </summary>
        /// <returns>`this.Name`</returns>
        public override string ToString()
        {
            if (_validUsCode)
            {
                if (string.IsNullOrEmpty(Four))
                {
                    return Five;
                }
                else
                {
                    return $"{Five}-{Four}";
                }
            } else
            {
                return _raw;
            }
        }

        /// <summary>
        /// default unknown/empty zip code
        /// </summary>
        public static ZipCode Unknown => new ZipCode();
    }
}

using System;
using System.Collections.Generic;

namespace DataLayer.Base.Models
{
    public interface IAuthorizedService : IStartEnd
    {
        /// <summary>
        /// Date the service was authorized
        /// </summary>
        public DateTime? AuthorizedDate { get; set; }

        /// <summary>
        /// Person who authorized the service
        /// </summary>
        public string AuthorizedBy { get; set; }


        /// <summary>
        /// Authorization Priority
        /// </summary>
        public string Priority { get; set; }

        /// <summary>
        /// Service status (APPROVE, DENY, etc)
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Units approved
        /// </summary>
        public int ApprovedCount { get; set; }

        /// <summary>
        /// Dollar amount approved
        /// </summary>
        public int ApprovedAmount { get; set; }

        /// <summary>
        /// Type of units
        /// </summary>
        public string UnitType { get; set; }

        /// <summary>
        /// Procedure codes
        /// </summary>
        public List<IAuthorizationProcedure> ProcedureCodes { get; set; }
    }
}

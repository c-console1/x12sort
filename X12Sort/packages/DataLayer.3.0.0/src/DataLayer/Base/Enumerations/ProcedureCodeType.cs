namespace DataLayer.Base.Enumerations
{
    /// <summary>
    /// Type of claim procedure code
    /// </summary>
    public enum ProcedureCodeType : int
    {
        Unknown = 0,

        /// <summary>
        /// International Classification of Diseases, Ninth Revision
        /// </summary>
        ICD9 = 1,

        /// <summary>
        /// International Classification of Diseases, Tenth Revision
        /// </summary>
        ICD10 = 2,

        /// <summary>
        /// Healthcare Common Procedure Coding System
        /// </summary>
        HCPCS = 3,

        /// <summary>
        /// Health Insurance Prospective Payment System
        /// </summary>
        HIPPS = 4,

        /// <summary>
        /// Institutional revenue code
        /// </summary>
        Revenue = 5,

        /// <summary>
        /// American Dental Association
        /// </summary>
        ADA = 6,

        /// <summary>
        /// Unknown procedure code
        /// </summary>
        Other = 999
    }
}

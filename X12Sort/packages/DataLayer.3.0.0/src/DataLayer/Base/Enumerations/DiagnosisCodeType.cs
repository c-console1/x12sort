namespace DataLayer.Base.Enumerations
{
    /// <summary>
    /// Type of claim diagnosis code
    /// </summary>
    public enum DiagnosisCodeType : int
    {
        Unknown = 0,

        /// <summary>
        /// International Classification of Diseases, Ninth Revision
        /// </summary>
        ICD9 = 1,

        /// <summary>
        /// International Classification of Diseases, Tenth Revision
        /// </summary>
        ICD10 = 2,

        /// <summary>
        /// Unknown diagnosis code
        /// </summary>
        Other = 999
    }
}

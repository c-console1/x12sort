namespace DataLayer.Base.Enumerations
{
    /// <summary>
    /// The priority of the address
    /// </summary>
    public enum AddressPriority : int
    {
        /// <summary>
        /// An unknown address priority
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// This the is primary address of the address type
        /// </summary>
        Primary = 1,

        /// <summary>
        /// This the is alternate address of the address type
        /// </summary>
        Alternate = 2,

        /// <summary>
        /// Address priority is not primary or alternate
        /// </summary>
        Other = 999
    }
}

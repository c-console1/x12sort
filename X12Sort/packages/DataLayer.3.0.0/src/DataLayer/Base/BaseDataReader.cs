using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using DataLayer.StaticUtilities;
using DataLayer.StaticUtilities.Extensions;

namespace DataLayer.Base
{
    public abstract class BaseDataReader<TDbType, TRecordType> : IDisposable where TDbType : BaseDatabase
    {
        #region Constants/Variables
        protected const int DefaultResultSetSize = 10000;
        #endregion

        #region Properties
        /// <summary>
        /// The Database from which this data reader is reading
        /// </summary>
        protected TDbType Database { get; private set; }

        /// <summary>
        /// SQL string to get data from the database
        /// </summary>
        protected string Sql { get; private set; }

        /// <summary>
        /// Dapper parameter object
        /// </summary>
        protected object Parameters { get; private set; }

        /// <summary>
        /// List of temp table names for search criteria
        /// </summary>
        protected IEnumerable<string> TempTableNames { get; private set; }

        /// <summary>
        /// Formatted records
        /// </summary>
        public List<TRecordType> Records { get; protected set; }

        /// <summary>
        /// Gets the running record count; increments every time NextResultSet is called
        /// </summary>
        public int RunningRecordCount { get; private set; } = 0;

        /// <summary>
        /// Gets the number for the current iteration; increments every time NextResultSet is called
        /// </summary>
        public int CurrentIteration { get; private set; } = 0;

        /// <summary>
        /// Whether there is any data left in the stream
        /// </summary>
        public bool EndOfStream { get; private set; } = false;
        #endregion

        #region Constructors
        /// <summary>
        /// Standard constructor; arguments cannot be null
        /// </summary>
        /// <param name="database">Reference to the database where the data is coming from</param>
        /// <param name="sql">SQL string to get data from the database</param>
        /// <param name="param">Dapper parameter object</param>
        /// <param name="filterTables">List of temp filter table names with corresponding lists of values</param>
        protected BaseDataReader(TDbType database, string sql, object param, Dictionary<string, IEnumerable<object>> filterTables)
        {
            // Validate incoming parameters
            if (database == null)
                throw new ArgumentNullException(nameof(database));
            if (string.IsNullOrWhiteSpace(sql))

                throw new ArgumentNullException(nameof(sql));
            if (filterTables == null)
                throw new ArgumentNullException(nameof(filterTables));
            ValidateMinMaxFields(param);

            // Collect parameters
            Database = database;
            Sql = sql;
            Parameters = param;
            TempTableNames = filterTables.Keys;

            // Reset the min and max row numbers
            ((dynamic)param).minRowNum = 0;
            ((dynamic)param).maxRowNum = 0;

            // Create temp filter tables (ignore return value)
            database.CreateTempFilterTables(filterTables);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Load records list with the next set of records
        /// </summary>
        /// <param name="recordCount">Maximum number of records to load into record list</param>
        /// <param name="commandTimeOut">The command timeout (in seconds)</param>
        /// <returns>True if more records have been loaded to Records; False if no records are remaining in the data reader</returns>
        public virtual bool NextResultSet(int recordCount = DefaultResultSetSize, int? commandTimeOut = null)
        {
            if (EndOfStream) return false;
            CurrentIteration++;

            // Calculate min and max row numbers
            ((dynamic)Parameters).minRowNum = ((dynamic)Parameters).maxRowNum + 1;
            ((dynamic)Parameters).maxRowNum += recordCount;

            // Get new set of records
            if (Records != null) Records.Clear();
            Records = Database.Query<TRecordType>(Sql, Parameters, commandTimeout: commandTimeOut).ToList();
            RunningRecordCount += Records.Count;

            // Return false if no more records were found
            EndOfStream = Records.Count == 0;
            return !EndOfStream;
        }

        /// <summary>
        /// Dispose of BaseDataReader object by dropping all of the temp tables
        /// </summary>
        //public void Dispose()
        //{
        //    foreach (string tableName in TempTableNames)
        //    {
        //        Database.DropTempTable(tableName);
        //    }
        //    GC.SuppressFinalize(this);
        //}

        /// <summary>
        /// Validates that the min and max row number fields exist in the Dapper parameter object
        /// </summary>
        /// <param name="param">Dapper parameter object</param>
        private void ValidateMinMaxFields(object param)
        {            
            if (param == null)
            {
                throw new ArgumentNullException(nameof(param), "The minRowNum and maxRowNum fields are missing from the param object because it is null");
            }
            if (param is ExpandoObject)
            {
                if (!((IDictionary<string, object>)param).ContainsKey("minRowNum"))
                    throw new MissingFieldException("The minRowNum field is missing from the param object");
                if (!((IDictionary<string, object>)param).ContainsKey("maxRowNum"))
                    throw new MissingFieldException("The maxRowNum field is missing from the param object");
            }
            else
            {
                if (param.GetType().GetProperty("minRowNum") == null)
                    throw new MissingFieldException("The minRowNum field is missing from the param object");
                if (param.GetType().GetProperty("maxRowNum") == null)
                    throw new MissingFieldException("The maxRowNum field is missing from the param object");
            }
        }

        #endregion

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        /// <summary>
        /// Dispose components.
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    foreach (string tableName in TempTableNames)
                    {
                        Database.DropTempTable(tableName);
                    }
                }

                disposedValue = true;
            }
        }

        /// <summary>
        /// This code added to correctly implement the disposable pattern.
        /// </summary>
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);

            GC.SuppressFinalize(this);
        }
        #endregion
    }
}

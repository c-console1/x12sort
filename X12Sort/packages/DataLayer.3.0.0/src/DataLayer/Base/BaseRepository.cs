using System;

namespace DataLayer.Base
{
    /// <summary>
    /// Repositories are sub classes to help organize the queries into logical groups
    /// </summary>
    /// <typeparam name="T"> The type of database this repository is part of </typeparam>
    public abstract class BaseRepository<T> : IDisposable where T : BaseDatabase
    {
        private T _database;

        /// <summary>
        /// The Database that this repository belongs to.
        /// </summary>
        protected T Database { get { return _database; } }

        /// <summary>
        /// Standard Constructor
        /// </summary>
        /// <param name="database"> Reference to the database that is repository belongs to</param>
        protected BaseRepository(T database)
        {
            _database = database;
        }

        #region IDisposable Support

        private bool disposedValue = false; // To detect redundant calls

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: What should this do???
                }
                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion IDisposable Support
    }
}

using System.Collections.Generic;

namespace DataLayer
{
    public class MachineApplicationInfo
    {
        /// <summary>
        /// Unique ID for the machine/application pair
        /// </summary>
        public int MachineApplicationID { get; internal set; }

        /// <summary>
        /// Machine ID for this machine/application pair
        /// </summary>
        public int MachineID { get; internal set; }

        /// <summary>
        /// Machine Name, should be what is obtained from `Environment.MachineName`
        /// </summary>
        public string MachineName { get; internal set; }

        /// <summary>
        /// Human description of the machine, i.e. Local development machine for Joseph Livecchi
        /// </summary>
        public string MachineDescription { get; internal set; }

        /// <summary>
        /// Flag indicating if this is a production run, from the Configuration Database
        /// </summary>
        public bool MachineIsProduction { get; internal set; }

        /// <summary>
        /// Name for this application, should be a AlphaNumeric
        /// </summary>
        public string ApplicationName { get; internal set; }

        /// <summary>
        /// What type of application: Batch job, Web App, Config Template
        /// </summary>
        public string ApplicationType { get; internal set; }

        /// <summary>
        /// Human description of the application
        /// </summary>
        public string ApplicationDescription { get; internal set; }

        /// <summary>
        /// Dictionary of DAM variables used by this application
        /// </summary>
        public Dictionary<string, string> Variables { get; internal set; }
    }
}

using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using static Dapper.SqlMapper;

namespace DataLayer
{
    /// <summary>
    /// Class that wraps the <c> dapper.GridReader </c> to allow for mocking, it should have the same API.
    /// </summary>
    /// <remarks> This class is a pure wrap, there isn't anything that is does except pass all the calls to the internal <c> GridReader </c> object. </remarks>
    /// <seealso cref="GridReader"/>
    public class DbGridReader : IDisposable, IDbGridReader
    {
        private GridReader _reader;

        internal DbGridReader(GridReader reader)
        {
            _reader = reader;
        }

        /// <summary>
        /// Read the next grid of results, returned as a dynamic object.
        /// </summary>
        /// <param name="buffered"> Whether the results should be buffered in memory. </param>
        /// <remarks> Note: each row can be accessed via "dynamic", or by casting to an IDictionary&lt;string,object&gt; </remarks>
        public IEnumerable<dynamic> Read(bool buffered = true) => _reader.Read(buffered);

        /// <summary>
        /// Read an individual row of the next grid of results, returned as a dynamic object.
        /// </summary>
        /// <remarks> Note: the row can be accessed via "dynamic", or by casting to an IDictionary&lt;string,object&gt; </remarks>
        public dynamic ReadFirst() => _reader.ReadFirst();

        /// <summary>
        /// Read an individual row of the next grid of results, returned as a dynamic object.
        /// </summary>
        /// <remarks> Note: the row can be accessed via "dynamic", or by casting to an IDictionary&lt;string,object&gt; </remarks>
        public dynamic ReadFirstOrDefault() => _reader.ReadFirstOrDefault();

        /// <summary>
        /// Read an individual row of the next grid of results, returned as a dynamic object.
        /// </summary>
        /// <remarks> Note: the row can be accessed via "dynamic", or by casting to an IDictionary&lt;string,object&gt; </remarks>
        public dynamic ReadSingle() => _reader.ReadSingle();

        /// <summary>
        /// Read an individual row of the next grid of results, returned as a dynamic object.
        /// </summary>
        /// <remarks> Note: the row can be accessed via "dynamic", or by casting to an IDictionary&lt;string,object&gt; </remarks>
        public dynamic ReadSingleOrDefault() => _reader.ReadSingleOrDefault();

        /// <summary>
        /// Read the next grid of results.
        /// </summary>
        /// <typeparam name="T"> The type to read. </typeparam>
        /// <param name="buffered"> Whether the results should be buffered in memory. </param>
        public IEnumerable<T> Read<T>(bool buffered = true) => _reader.Read<T>(buffered);

        /// <summary>
        /// Read an individual row of the next grid of results.
        /// </summary>
        /// <typeparam name="T"> The type to read. </typeparam>
        public T ReadFirst<T>() => _reader.ReadFirst<T>();

        /// <summary>
        /// Read an individual row of the next grid of results.
        /// </summary>
        /// <typeparam name="T"> The type to read. </typeparam>
        public T ReadFirstOrDefault<T>() => _reader.ReadFirstOrDefault<T>();

        /// <summary>
        /// Read an individual row of the next grid of results.
        /// </summary>
        /// <typeparam name="T"> The type to read. </typeparam>
        public T ReadSingle<T>() => _reader.ReadSingle<T>();

        /// <summary>
        /// Read an individual row of the next grid of results.
        /// </summary>
        /// <typeparam name="T"> The type to read. </typeparam>
        public T ReadSingleOrDefault<T>() => _reader.ReadSingleOrDefault<T>();

        /// <summary>
        /// Read the next grid of results.
        /// </summary>
        /// <param name="type">     The type to read. </param>
        /// <param name="buffered"> Whether to buffer the results. </param>
        /// <exception cref="ArgumentNullException"> <paramref name="type"/> is <c> null </c>. </exception>
        public IEnumerable<object> Read(Type type, bool buffered = true) => _reader.Read(type, buffered);

        /// <summary>
        /// Read an individual row of the next grid of results.
        /// </summary>
        /// <param name="type"> The type to read. </param>
        /// <exception cref="ArgumentNullException"> <paramref name="type"/> is <c> null </c>. </exception>
        public object ReadFirst(Type type) => _reader.ReadFirst(type);

        /// <summary>
        /// Read an individual row of the next grid of results.
        /// </summary>
        /// <param name="type"> The type to read. </param>
        /// <exception cref="ArgumentNullException"> <paramref name="type"/> is <c> null </c>. </exception>
        public object ReadFirstOrDefault(Type type) => _reader.ReadFirstOrDefault(type);

        /// <summary>
        /// Read an individual row of the next grid of results.
        /// </summary>
        /// <param name="type"> The type to read. </param>
        /// <exception cref="ArgumentNullException"> <paramref name="type"/> is <c> null </c>. </exception>
        public object ReadSingle(Type type) => _reader.ReadSingle(type);

        /// <summary>
        /// Read an individual row of the next grid of results.
        /// </summary>
        /// <param name="type"> The type to read. </param>
        /// <exception cref="ArgumentNullException"> <paramref name="type"/> is <c> null </c>. </exception>
        public object ReadSingleOrDefault(Type type) => _reader.ReadSingleOrDefault(type);

        /// <summary>
        /// Read multiple objects from a single record set on the grid.
        /// </summary>
        /// <typeparam name="TFirst"> The first type in the record set. </typeparam>
        /// <typeparam name="TSecond"> The second type in the record set. </typeparam>
        /// <typeparam name="TReturn"> The type to return from the record set. </typeparam>
        /// <param name="func">     The mapping function from the read types to the return type. </param>
        /// <param name="splitOn">  The field(s) we should split and read the second object from (defaults to "id") </param>
        /// <param name="buffered"> Whether to buffer results in memory. </param>
        public IEnumerable<TReturn> Read<TFirst, TSecond, TReturn>(Func<TFirst, TSecond, TReturn> func, string splitOn = "id", bool buffered = true) => _reader.Read(func, splitOn, buffered);

        /// <summary>
        /// Read multiple objects from a single record set on the grid.
        /// </summary>
        /// <typeparam name="TFirst"> The first type in the record set. </typeparam>
        /// <typeparam name="TSecond"> The second type in the record set. </typeparam>
        /// <typeparam name="TThird"> The third type in the record set. </typeparam>
        /// <typeparam name="TReturn"> The type to return from the record set. </typeparam>
        /// <param name="func">     The mapping function from the read types to the return type. </param>
        /// <param name="splitOn">  The field(s) we should split and read the second object from (defaults to "id") </param>
        /// <param name="buffered"> Whether to buffer results in memory. </param>
        public IEnumerable<TReturn> Read<TFirst, TSecond, TThird, TReturn>(Func<TFirst, TSecond, TThird, TReturn> func, string splitOn = "id", bool buffered = true) => _reader.Read(func, splitOn, buffered);

        /// <summary>
        /// Read multiple objects from a single record set on the grid
        /// </summary>
        /// <typeparam name="TFirst"> The first type in the record set. </typeparam>
        /// <typeparam name="TSecond"> The second type in the record set. </typeparam>
        /// <typeparam name="TThird"> The third type in the record set. </typeparam>
        /// <typeparam name="TFourth"> The fourth type in the record set. </typeparam>
        /// <typeparam name="TReturn"> The type to return from the record set. </typeparam>
        /// <param name="func">     The mapping function from the read types to the return type. </param>
        /// <param name="splitOn">  The field(s) we should split and read the second object from (defaults to "id") </param>
        /// <param name="buffered"> Whether to buffer results in memory. </param>
        public IEnumerable<TReturn> Read<TFirst, TSecond, TThird, TFourth, TReturn>(Func<TFirst, TSecond, TThird, TFourth, TReturn> func, string splitOn = "id", bool buffered = true) => _reader.Read(func, splitOn, buffered);

        /// <summary>
        /// Read multiple objects from a single record set on the grid
        /// </summary>
        /// <typeparam name="TFirst"> The first type in the record set. </typeparam>
        /// <typeparam name="TSecond"> The second type in the record set. </typeparam>
        /// <typeparam name="TThird"> The third type in the record set. </typeparam>
        /// <typeparam name="TFourth"> The fourth type in the record set. </typeparam>
        /// <typeparam name="TFifth"> The fifth type in the record set. </typeparam>
        /// <typeparam name="TReturn"> The type to return from the record set. </typeparam>
        /// <param name="func">     The mapping function from the read types to the return type. </param>
        /// <param name="splitOn">  The field(s) we should split and read the second object from (defaults to "id") </param>
        /// <param name="buffered"> Whether to buffer results in memory. </param>
        public IEnumerable<TReturn> Read<TFirst, TSecond, TThird, TFourth, TFifth, TReturn>(Func<TFirst, TSecond, TThird, TFourth, TFifth, TReturn> func, string splitOn = "id", bool buffered = true) => _reader.Read(func, splitOn, buffered);

        /// <summary>
        /// Read multiple objects from a single record set on the grid
        /// </summary>
        /// <typeparam name="TFirst"> The first type in the record set. </typeparam>
        /// <typeparam name="TSecond"> The second type in the record set. </typeparam>
        /// <typeparam name="TThird"> The third type in the record set. </typeparam>
        /// <typeparam name="TFourth"> The fourth type in the record set. </typeparam>
        /// <typeparam name="TFifth"> The fifth type in the record set. </typeparam>
        /// <typeparam name="TSixth"> The sixth type in the record set. </typeparam>
        /// <typeparam name="TReturn"> The type to return from the record set. </typeparam>
        /// <param name="func">     The mapping function from the read types to the return type. </param>
        /// <param name="splitOn">  The field(s) we should split and read the second object from (defaults to "id") </param>
        /// <param name="buffered"> Whether to buffer results in memory. </param>
        public IEnumerable<TReturn> Read<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TReturn>(Func<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TReturn> func, string splitOn = "id", bool buffered = true) => _reader.Read(func, splitOn, buffered);

        /// <summary>
        /// Read multiple objects from a single record set on the grid
        /// </summary>
        /// <typeparam name="TFirst"> The first type in the record set. </typeparam>
        /// <typeparam name="TSecond"> The second type in the record set. </typeparam>
        /// <typeparam name="TThird"> The third type in the record set. </typeparam>
        /// <typeparam name="TFourth"> The fourth type in the record set. </typeparam>
        /// <typeparam name="TFifth"> The fifth type in the record set. </typeparam>
        /// <typeparam name="TSixth"> The sixth type in the record set. </typeparam>
        /// <typeparam name="TSeventh"> The seventh type in the record set. </typeparam>
        /// <typeparam name="TReturn"> The type to return from the record set. </typeparam>
        /// <param name="func">     The mapping function from the read types to the return type. </param>
        /// <param name="splitOn">  The field(s) we should split and read the second object from (defaults to "id") </param>
        /// <param name="buffered"> Whether to buffer results in memory. </param>
        public IEnumerable<TReturn> Read<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh, TReturn>(Func<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh, TReturn> func, string splitOn = "id", bool buffered = true) => _reader.Read(func, splitOn, buffered);

        /// <summary>
        /// Read multiple objects from a single record set on the grid
        /// </summary>
        /// <typeparam name="TReturn"> The type to return from the record set. </typeparam>
        /// <param name="types">    The types to read from the result set. </param>
        /// <param name="map">      The mapping function from the read types to the return type. </param>
        /// <param name="splitOn">  The field(s) we should split and read the second object from (defaults to "id") </param>
        /// <param name="buffered"> Whether to buffer results in memory. </param>
        public IEnumerable<TReturn> Read<TReturn>(Type[] types, Func<object[], TReturn> map, string splitOn = "id", bool buffered = true) => _reader.Read(types, map, splitOn, buffered);

        /// <summary>
        /// Has the underlying reader been consumed?
        /// </summary>
        /// <value> Get the value from the internal <c> GridReader </c> </value>
        public bool IsConsumed => _reader.IsConsumed;

        /// <summary>
        /// The command associated with the reader
        /// </summary>
        /// <value> Get the value from the internal <c> GridReader </c> </value>
        public IDbCommand Command => _reader.Command;

        #region Async Functions
        /// <summary>
        /// Read the next grid of results, returned as a dynamic object
        /// </summary>
        /// <remarks>Note: each row can be accessed via "dynamic", or by casting to an IDictionary&lt;string,object&gt;</remarks>
        /// <param name="buffered">Whether to buffer the results.</param>
        public Task<IEnumerable<dynamic>> ReadAsync(bool buffered = true) => _reader.ReadAsync(buffered);

        /// <summary>
        /// Read an individual row of the next grid of results, returned as a dynamic object
        /// </summary>
        /// <remarks>Note: the row can be accessed via "dynamic", or by casting to an IDictionary&lt;string,object&gt;</remarks>
        public Task<dynamic> ReadFirstAsync() => _reader.ReadFirstAsync();

        /// <summary>
        /// Read an individual row of the next grid of results, returned as a dynamic object
        /// </summary>
        /// <remarks>Note: the row can be accessed via "dynamic", or by casting to an IDictionary&lt;string,object&gt;</remarks>
        public Task<dynamic> ReadFirstOrDefaultAsync() => _reader.ReadFirstOrDefaultAsync();

        /// <summary>
        /// Read an individual row of the next grid of results, returned as a dynamic object
        /// </summary>
        /// <remarks>Note: the row can be accessed via "dynamic", or by casting to an IDictionary&lt;string,object&gt;</remarks>
        public Task<dynamic> ReadSingleAsync() => _reader.ReadSingleAsync();

        /// <summary>
        /// Read an individual row of the next grid of results, returned as a dynamic object
        /// </summary>
        /// <remarks>Note: the row can be accessed via "dynamic", or by casting to an IDictionary&lt;string,object&gt;</remarks>
        public Task<dynamic> ReadSingleOrDefaultAsync() => _reader.ReadSingleOrDefaultAsync();

        /// <summary>
        /// Read the next grid of results
        /// </summary>
        /// <param name="type">The type to read.</param>
        /// <param name="buffered">Whether to buffer the results.</param>
        /// <exception cref="ArgumentNullException"><paramref name="type"/> is <c>null</c>.</exception>
        public Task<IEnumerable<object>> ReadAsync(Type type, bool buffered = true) => _reader.ReadAsync(type, buffered);

        /// <summary>
        /// Read an individual row of the next grid of results
        /// </summary>
        /// <param name="type">The type to read.</param>
        /// <exception cref="ArgumentNullException"><paramref name="type"/> is <c>null</c>.</exception>
        public Task<object> ReadFirstAsync(Type type) => _reader.ReadFirstAsync(type);

        /// <summary>
        /// Read an individual row of the next grid of results.
        /// </summary>
        /// <param name="type">The type to read.</param>
        /// <exception cref="ArgumentNullException"><paramref name="type"/> is <c>null</c>.</exception>
        public Task<object> ReadFirstOrDefaultAsync(Type type) => _reader.ReadFirstOrDefaultAsync(type);

        /// <summary>
        /// Read an individual row of the next grid of results.
        /// </summary>
        /// <param name="type">The type to read.</param>
        /// <exception cref="ArgumentNullException"><paramref name="type"/> is <c>null</c>.</exception>
        public Task<object> ReadSingleAsync(Type type) => _reader.ReadSingleAsync(type);

        /// <summary>
        /// Read an individual row of the next grid of results.
        /// </summary>
        /// <param name="type">The type to read.</param>
        /// <exception cref="ArgumentNullException"><paramref name="type"/> is <c>null</c>.</exception>
        public Task<object> ReadSingleOrDefaultAsync(Type type) => _reader.ReadSingleOrDefaultAsync(type);

        /// <summary>
        /// Read the next grid of results.
        /// </summary>
        /// <typeparam name="T">The type to read.</typeparam>
        /// <param name="buffered">Whether the results should be buffered in memory.</param>
        public Task<IEnumerable<T>> ReadAsync<T>(bool buffered = true) => _reader.ReadAsync<T>(buffered);

        /// <summary>
        /// Read an individual row of the next grid of results.
        /// </summary>
        /// <typeparam name="T">The type to read.</typeparam>
        public Task<T> ReadFirstAsync<T>() => _reader.ReadFirstAsync<T>();

        /// <summary>
        /// Read an individual row of the next grid of results.
        /// </summary>
        /// <typeparam name="T">The type to read.</typeparam>
        public Task<T> ReadFirstOrDefaultAsync<T>() => _reader.ReadFirstOrDefaultAsync<T>();

        /// <summary>
        /// Read an individual row of the next grid of results.
        /// </summary>
        /// <typeparam name="T">The type to read.</typeparam>
        public Task<T> ReadSingleAsync<T>() => _reader.ReadSingleAsync<T>();

        /// <summary>
        /// Read an individual row of the next grid of results.
        /// </summary>
        /// <typeparam name="T">The type to read.</typeparam>
        public Task<T> ReadSingleOrDefaultAsync<T>() => _reader.ReadSingleOrDefaultAsync<T>();

        #endregion

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        /// <summary>
        /// Dispose the grid, closing and disposing both the underlying reader and command.
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _reader.Dispose();
                }

                disposedValue = true;
            }
        }


        /// <summary>
        /// This code added to correctly implement the disposable pattern.
        /// </summary> 
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}

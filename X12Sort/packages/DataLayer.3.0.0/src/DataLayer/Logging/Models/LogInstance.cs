﻿using System;

namespace DataLayer.Logging.Models
{
    /// <summary>
    /// This class defines the instance of a log value
    /// </summary>
    public class LogInstance : ICrudFunc
    {
        /// <summary>
        /// Standard Constructor
        /// </summary>
        public LogInstance()
        {
            CreatedOn = DateTime.Now;
        }

        /// <summary>
        /// This is the instance id for the current executing job
        /// </summary>
        public Guid InstanceId { get; set; }

        /// <summary>
        /// The machine name that the job is running on. Should be Configuration.MachineName Value
        /// </summary>
        public string MachineName { get; set; }

        /// <summary>
        /// The application name that being executed. Should be Configuration.Application Value
        /// </summary>
        public string ApplicationName { get; set; }

        /// <summary>
        /// The type of application: Batch, Unix Batch, WebApp, etc.
        /// </summary>
        public string ApplicationType { get; set; }

        /// <summary>
        /// How often these records should be clean out of the database. Using bit masking 0001 - 0001 - daily; 0010 - Weekly; 0100 - Monthly; 1000 - Yearly
        /// </summary>
        public int DumpSchedule { get; set; }

        /// <summary>
        /// The User who Created the DataPoint
        /// </summary>
        public string CreatedBy { get; set; }

        /// <summary>
        /// The DateTime this data Point was created
        /// </summary>
        public DateTime CreatedOn { get; set; }

        #region CRUD Queries

        /// <summary>
        /// SQL Query to craete an object
        /// </summary>
        /// <returns> Returns a parameterized sql string </returns>

        public string CreateQuery()
        {
            return @"EXEC sp_NewLogInstance @MachineName, @ApplicationName, @ApplicationType";
        }

        /// <summary>
        /// SQL Query to read an object
        /// </summary>
        /// <returns> Returns a parameterized sql string </returns>
        public string ReadQuery()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// SQL Query to update an object
        /// </summary>
        /// <returns> Returns a parameterized sql string </returns>
        public string UpdateQuery()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// SQL Query to delete an object
        /// </summary>
        /// <returns> Returns a parameterized sql string </returns>
        public string DeleteQuery()
        {
            throw new NotImplementedException();
        }

        #endregion CRUD Queries
    }
}

﻿using Newtonsoft.Json;
using System;

namespace DataLayer.Logging.Models
{
    /// <summary>
    /// Basic structure of a the Value field in a LogDataPoint
    /// </summary>
    public interface ILogDataPointValue
    {
        /// <summary>
        /// Data type for this object
        /// </summary>
        string Type { get; }

        /// <summary>
        /// JOSN value that should be convertible to the Type Property
        /// </summary>
        object Value { get; }
    }

    /// <summary>
    /// A specific basic implementation of the ILogDataPointaValue
    /// </summary>
    public class LogDataPointValue : ILogDataPointValue
    {
        /// <summary>
        /// Default Construct for a new value
        /// </summary>
        public LogDataPointValue()
        {
        }

        /// <summary>
        /// Constructor for loading a JOSN string into a LogDataPointValue
        /// </summary>
        /// <param name="jsonValue"> ILogDataPointValue in a JOSN string </param>
        public LogDataPointValue(string jsonValue)
        {
            var castValue = JsonConvert.DeserializeAnonymousType(jsonValue, new { Type = "", Value = new object() });
            Type castTo = System.Type.GetType(castValue.Type);
            if (castTo != null)
            {
                Value = Convert.ChangeType(castValue.Value, castTo);
            }
            else
            {
                Value = castValue.Value;
            }
        }

        /// <summary>
        /// System Type of the Value Property
        /// </summary>
        public string Type
        {
            get { return Value == null ? "null" : Value.GetType().Name; }
        }

        /// <summary>
        /// The Logged object value
        /// </summary>
        public dynamic Value { get; set; }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    /// <summary>
    /// Individual data point connected to LogInstance
    /// </summary>
    public class LogDataPoint : ICrudFunc
    {
        private ILogDataPointValue _parsedvalue;

        /// <summary>
        /// Standard Constructor
        /// </summary>
        public LogDataPoint()
        {
        }

        /// <summary>
        /// Standard Constructor with initialization
        /// </summary>
        /// <param name="instanceId">The logging instance id</param>
        /// <param name="label">The English label for this data point</param>
        /// <param name="value">The dynamic object that is being logged</param>
        public LogDataPoint(Guid instanceId, string label, dynamic value)
        {
            InstanceId = instanceId;
            Label = label;
            _parsedvalue = new LogDataPointValue() { Value = value };
            CreatedOn = DateTime.Now;
        }

        /// <summary>
        /// This is the instance id for the current executing job. Link to LogInstance
        /// </summary>
        public Guid InstanceId { get; set; }

        /// <summary>
        /// The order of the Log Points
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Label for this point, will be used for processing
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// JSONifed string of data. Should be able to be decomposed into ILogDataPointaValue
        /// </summary>
        public string Value { get { return _parsedvalue.ToString(); } set { _parsedvalue = Decompose(value); } }

        /// <summary>
        /// Value Property Parsed from JSON
        /// </summary>
        public ILogDataPointValue ParsedValue { get { return _parsedvalue; } }

        /// <summary>
        /// The User who Created the DataPoint
        /// </summary>
        public string CreatedBy { get; set; }

        /// <summary>
        /// The DateTime this data Point was created
        /// </summary>
        public DateTime CreatedOn { get; set; }

        /// <summary>
        /// Decomposes the JSON string in the Value into an object
        /// </summary>
        protected virtual ILogDataPointValue Decompose(string value)
        {
            return new LogDataPointValue(value);
        }

        #region CRUD Queries

        /// <summary>
        /// SQL Query to create an object
        /// </summary>
        /// <returns>Returns a parameterized sql string</returns>
        public string CreateQuery()
        {
            return @"EXEC sp_AddDataPoint @InstanceID, @Label, @Value";
        }

        /// <summary>
        /// SQL Query to read an object
        /// </summary>
        /// <returns>Returns a parameterized sql string</returns>
        public string ReadQuery()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// SQL Query to update an object
        /// </summary>
        /// <returns>Returns a parameterized sql string</returns>
        public string UpdateQuery()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// SQL Query to delete an object
        /// </summary>
        /// <returns>Returns a parameterized sql string</returns>
        public string DeleteQuery()
        {
            throw new NotImplementedException();
        }

        #endregion CRUD Queries
    }
}

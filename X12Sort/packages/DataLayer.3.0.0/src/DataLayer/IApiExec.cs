using Flurl;
using Flurl.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    /// <summary>
    /// Rest API executor
    /// <para>Wrapper around Flurl to provider an easily mock-able layer.
    /// They executor will encapsulate all of the auth and default headers for the site
    /// </para>
    /// </summary>
    public interface IApiExec: IExecutor
    {
        /// <summary>
        /// Creates a new API Request
        /// </summary>
        /// <param name="UrlSegments">list of URL segments for this specific request</param>
        /// <returns></returns>
        IFlurlRequest Request(params object[] UrlSegments);


        /// <summary>
        /// Creates a new Soap Request to the given URL and wraps/unwraps the request and response objects
        /// </summary>
        /// <param name="request">The request object that is wrapped in a SOAP header</param>
        /// <returns></returns>
        Task<TResponse> ExecuteSoapAsync<TRequest, TResponse>(ISoapRequest<TRequest, TResponse> request) where TRequest : class where TResponse : class;
    }
}

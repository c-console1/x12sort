using DataLayer.Base;
using DataLayer.PortalADFS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// TODO -- make an entry point that works for provider users. ("users manageable by this user")
// TODO -- make another one for member agency users. ("users manageable by this user")

namespace DataLayer.PortalADFS
{
    public class PortalADFSRepository : BaseRepository<PortalADFSDatabase>
    {
        internal PortalADFSRepository(PortalADFSDatabase database) : base(database)
        {

        }

        #region Locate member users
        /// <summary>
        /// This will get a Portal User and their Access Controls with a Member Number
        /// If the portal user has any privileges beyond the provided member number, they are ignored.
        /// </summary>
        /// <param name="MemberNumber"></param>
        /// <returns></returns>
        public User GetMemberPortalUserByMemberNumber(string MemberNumber)
        {
            #region SQL Scripts

            string getUserQuery = @"
            SELECT
                UserApiIdentifier,
                LoginName,
                ADFS_Domain AS ADFSDomain,
                AD_User_ID AS ADUserID,
                EmailAddress,
                CASE 
                    WHEN EmailVerified = 'Y'
                        THEN 1
                    ELSE 0
                END AS EmailVerified,
                OldEmailAddress,
                SecurityQuestion,
                SecurityAnswer,
                CASE 
                    WHEN RequireTOS = 'Y'
                        THEN 1
                    ELSE 0
                END AS RequireTOS,
                FirstName,
                LastName,
                ContactPhone,
                [User].UpdateDate,
                [User].UpdateUser,
                CASE 
                    WHEN IsParamount = 'Y'
                        THEN 1
                    ELSE 0
                END AS IsParamount,
                CellPhone,
                CASE 
                    WHEN SecurityLocked = 'Y'
                        THEN 1
                    ELSE 0
                END AS SecurityLocked
            FROM
                dbo.[User]
                JOIN dbo.AccessControl
                ON AccessControl.ApiIdentifier = [User].UserApiIdentifier
            WHERE EntityKey = @memberNumber
                AND EntityType = 'Member'
            ";

            string getAccessControlQuery = @"
            SELECT
                AccessControlKey,
                ApiIdentifier,
                RoleName,
                EntityType,
                KeyType,
                EntityKey,
                EntitySubKey,
                CASE 
                    WHEN AdminRights = 'Y'
                        THEN 1
                    ELSE 0
                END AS AdminRights,
                UpdateDate,
                UpdateUser
            FROM AccessControl
            WHERE EntityKey = @memberNumber
                AND EntityType = 'Member'
            ";

            #endregion

            User User = Database.QueryFirstOrDefault<User>(getUserQuery, new { MemberNumber });
            if (User != null)
            {
                User.AccessControls = Database.Query<AccessControl>(getAccessControlQuery, new { MemberNumber }).ToList();
            }

            return User;
        }


        /// <summary>
        /// This will get a Portal User and their Access Controls with a Member Number
        /// If the portal user has any privileges beyond the provided member number, they are ignored.
        /// </summary>
        /// <param name="MemberNumber"></param>
        /// <returns></returns>
        public IEnumerable<User> GetMemberPortalUsersByMemberNumbers(IEnumerable<string> memberNumbers)
        {
            #region SQL Scripts

            string getUserQuery = @"
            SELECT
                UserApiIdentifier,
                LoginName,
                ADFS_Domain AS ADFSDomain,
                AD_User_ID AS ADUserID,
                EmailAddress,
                CASE 
                    WHEN EmailVerified = 'Y'
                        THEN 1
                    ELSE 0
                END AS EmailVerified,
                OldEmailAddress,
                SecurityQuestion,
                SecurityAnswer,
                CASE 
                    WHEN RequireTOS = 'Y'
                        THEN 1
                    ELSE 0
                END AS RequireTOS,
                FirstName,
                LastName,
                ContactPhone,
                [User].UpdateDate,
                [User].UpdateUser,
                CASE 
                    WHEN IsParamount = 'Y'
                        THEN 1
                    ELSE 0
                END AS IsParamount,
                CellPhone,
                CASE 
                    WHEN SecurityLocked = 'Y'
                        THEN 1
                    ELSE 0
                END AS SecurityLocked
            FROM
                dbo.[User]
                JOIN dbo.AccessControl
                ON AccessControl.ApiIdentifier = [User].UserApiIdentifier
            WHERE EntityKey in @memberNumbers
                AND EntityType = 'Member'
            ";

            string getAccessControlQuery = @"
            SELECT
                AccessControlKey,
                ApiIdentifier,
                RoleName,
                EntityType,
                KeyType,
                EntityKey,
                EntitySubKey,
                CASE 
                    WHEN AdminRights = 'Y'
                        THEN 1
                    ELSE 0
                END AS AdminRights,
                UpdateDate,
                UpdateUser
            FROM AccessControl
            WHERE EntityKey in @memberNumbers
                AND EntityType = 'Member'
            ";

            #endregion

            IEnumerable<User> Users = Database.Query<User>(getUserQuery, new { memberNumbers });

            if (Users != null && Users.Any())
            {
                var userAccessControls = Database.Query<AccessControl>(getAccessControlQuery, new { memberNumbers }).ToList();
                foreach(var u in Users)
                {
                    u.AccessControls = userAccessControls.Where(x => x.ApiIdentifier == u.UserApiIdentifier).ToList();
                }
            }

            return Users;
        }

        public IDictionary<string, User> GetMemberUserMapForMemberNumbers(IEnumerable<string> memberNumbers)
        {
            IEnumerable<User> users = GetMemberPortalUsersByMemberNumbers(memberNumbers);

            Dictionary<string, User> result = new Dictionary<string, User>();

            foreach (User u in users)
            {
                IEnumerable<(string EntityKey, User userLink)> members =
                    from a in u.AccessControls.Where(x => x.EntityType == "Member" && x.KeyType != "MemberClass") select (a.EntityKey, userLink: u);
                foreach ((string EntityKey, User userLink) m in members)
                {
                    if (result.ContainsKey(m.EntityKey))
                    {
                        // TODO -- log the original and the duplicate
                    }
                    else
                    {
                        result.Add(m.EntityKey, m.userLink);
                    }
                }
            }

            return result;
        }
        #endregion

        #region Locate single user by parameter
        /// <summary>
        /// This will get a Portal User and their Access Controls with a Active Directory User ID (SID)
        /// </summary>
        /// <param name="ADUserID"></param>
        /// <returns></returns>
        public User GetPortalUserByADUserID(string ADUserID)
        {
            User result = GetSingleUserWithCondition("WHERE AD_User_ID = @parameterString", parameterString: ADUserID);
            return result;
        }

        /// <summary>
        /// This will get a Portal User and their Access Controls with a login name
        /// </summary>
        /// <param name="loginName"></param>
        /// <returns></returns>
        public User GetPortalUserByLoginName(string loginName)
        {
            User result = GetSingleUserWithCondition("WHERE LoginName = @parameterString", parameterString: loginName);
            return result;
        }

        /// <summary>
        /// This will get a Portal User and their Access Controls with a user API identifier
        /// </summary>
        /// <param name="userApiIdentifier"></param>
        /// <returns></returns>
        public User GetPortalUserByApiIdentifier(int userApiIdentifier)
        {
            User result = GetSingleUserWithCondition("WHERE userApiIdentifier = @integerValue", integerValue: userApiIdentifier);
            return result;
        }
        #endregion

        #region Private methods
        private User GetSingleUserWithCondition(string myCondition, string parameterString)
        {
            #region SQL Scripts

            string getUserQuery = $@"
            SELECT
                UserApiIdentifier,
                LoginName,
                ADFS_Domain AS ADFSDomain,
                AD_User_ID AS ADUserID,
                EmailAddress,
                CASE 
                    WHEN EmailVerified = 'Y'
                        THEN 1
                    ELSE 0
                END AS EmailVerified,
                OldEmailAddress,
                SecurityQuestion,
                SecurityAnswer,
                CASE 
                    WHEN RequireTOS = 'Y'
                        THEN 1
                    ELSE 0
                END AS RequireTOS,
                FirstName,
                LastName,
                ContactPhone,
                [User].UpdateDate,
                [User].UpdateUser,
                CASE 
                    WHEN IsParamount = 'Y'
                        THEN 1
                    ELSE 0
                END AS IsParamount,
                CellPhone,
                CASE 
                    WHEN SecurityLocked = 'Y'
                        THEN 1
                    ELSE 0
                END AS SecurityLocked
            FROM
                dbo.[User]
                JOIN dbo.AccessControl
                ON AccessControl.ApiIdentifier = [User].UserApiIdentifier
            {myCondition}
            ";

            string getAccessControlQuery = $@"
            SELECT
                AccessControlKey,
                ApiIdentifier,
                RoleName,
                EntityType,
                KeyType,
                EntityKey,
                EntitySubKey,
                CASE 
                    WHEN AdminRights = 'Y'
                        THEN 1
                    ELSE 0
                END AS AdminRights,
                AccessControl.UpdateDate,
                AccessControl.UpdateUser
            FROM
                dbo.[User]
                JOIN dbo.AccessControl
                ON AccessControl.ApiIdentifier = [User].UserApiIdentifier
            {myCondition}
            UNION
            SELECT
                AccessControlKey,
                ApiIdentifier,
                RoleName,
                EntityType,
                KeyType,
                EntityKey,
                EntitySubKey,
                CASE 
                    WHEN AdminRights = 'Y'
                        THEN 1
                    ELSE 0
                END AS AdminRights,
                AccessControl.UpdateDate,
                AccessControl.UpdateUser
            FROM
                dbo.[User]
                JOIN dbo.GroupMember
                ON GroupMember.MemberIdentifier = [User].UserApiIdentifier
                JOIN dbo.AccessControl
                ON AccessControl.ApiIdentifier = GroupMember.GroupIdentifier
            {myCondition}
";

            #endregion

            User User = Database.QuerySingle<User>(getUserQuery, new { parameterString });
            if (User != null)
            {
                User.AccessControls = Database.Query<AccessControl>(getAccessControlQuery, new { parameterString }).ToList();
            }

            return User;
        }

        private User GetSingleUserWithCondition(string myCondition, int integerValue)
        {
            #region SQL Scripts

            string getUserQuery = $@"
            SELECT
                UserApiIdentifier,
                LoginName,
                ADFS_Domain AS ADFSDomain,
                AD_User_ID AS ADUserID,
                EmailAddress,
                CASE 
                    WHEN EmailVerified = 'Y'
                        THEN 1
                    ELSE 0
                END AS EmailVerified,
                OldEmailAddress,
                SecurityQuestion,
                SecurityAnswer,
                CASE 
                    WHEN RequireTOS = 'Y'
                        THEN 1
                    ELSE 0
                END AS RequireTOS,
                FirstName,
                LastName,
                ContactPhone,
                [User].UpdateDate,
                [User].UpdateUser,
                CASE 
                    WHEN IsParamount = 'Y'
                        THEN 1
                    ELSE 0
                END AS IsParamount,
                CellPhone,
                CASE 
                    WHEN SecurityLocked = 'Y'
                        THEN 1
                    ELSE 0
                END AS SecurityLocked
            FROM
                dbo.[User]
                JOIN dbo.AccessControl
                ON AccessControl.ApiIdentifier = [User].UserApiIdentifier
            {myCondition}
            ";

            string getAccessControlQuery = $@"
            SELECT
                AccessControlKey,
                ApiIdentifier,
                RoleName,
                EntityType,
                KeyType,
                EntityKey,
                EntitySubKey,
                CASE 
                    WHEN AdminRights = 'Y'
                        THEN 1
                    ELSE 0
                END AS AdminRights,
                AccessControl.UpdateDate,
                AccessControl.UpdateUser
            FROM
                dbo.[User]
                JOIN dbo.AccessControl
                ON AccessControl.ApiIdentifier = [User].UserApiIdentifier
            {myCondition}
            UNION
            SELECT
                AccessControlKey,
                ApiIdentifier,
                RoleName,
                EntityType,
                KeyType,
                EntityKey,
                EntitySubKey,
                CASE 
                    WHEN AdminRights = 'Y'
                        THEN 1
                    ELSE 0
                END AS AdminRights,
                AccessControl.UpdateDate,
                AccessControl.UpdateUser
            FROM
                dbo.[User]
                JOIN dbo.GroupMember
                ON GroupMember.MemberIdentifier = [User].UserApiIdentifier
                JOIN dbo.AccessControl
                ON AccessControl.ApiIdentifier = GroupMember.GroupIdentifier
            {myCondition}
";

            #endregion

            User User = Database.QueryFirstOrDefault<User>(getUserQuery, new { integerValue });
            if (User != null)
            {
                User.AccessControls = Database.Query<AccessControl>(getAccessControlQuery, new { integerValue }).ToList();
            }

            return User;
        }
        #endregion
    }
}

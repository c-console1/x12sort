using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.PortalADFS.Models
{
    public class AccessControl
    {
        /// <summary>
        /// PK of the table.
        /// </summary>
        [Key]
        public int AccessControlKey { get; set; }
        /// <summary>
        /// Links to UserApiIdentifier in the User table.
        /// </summary>
        public string ApiIdentifier { get; set; }
        /// <summary>
        /// Full role name the user has access to. Comes from the UserRoles table.
        /// </summary>
        public string RoleName { get; set; }
        /// <summary>
        /// Is the type of the user. Provider or Member.
        /// </summary>
        public string EntityType { get; set; }
        /// <summary>
        /// This is what type of entity they are.
        /// </summary>
        public string KeyType { get; set; }
        /// <summary>
        /// Either the member number or the provider ID/NPI
        /// </summary>
        public string EntityKey { get; set; }
        /// <summary>
        /// Either blank for members or TIN for providers.
        /// </summary>
        public string EntitySubKey { get; set; }
        /// <summary>
        /// Does the access have Admin rights.
        /// </summary>
        public bool AdminRights { get; set; }
        public DateTime UpdateDate { get; set; }
        public int UpdateUser { get; set; }
    }
}

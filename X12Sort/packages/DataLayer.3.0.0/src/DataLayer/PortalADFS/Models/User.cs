using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.PortalADFS.Models
{
    /// <summary>
    /// This is model based on the User table in the Portal_ADFS database.
    /// </summary>
    public class User
    {
        /// <summary>
        /// PK of the table.
        /// </summary>
        [Key]
        public string UserApiIdentifier { get; set;}
        public string LoginName { get; set; }
        /// <summary>
        /// This field is for which AD domain the user is in. PHSI (Internal) or PHCE (External).
        /// </summary>
        public string ADFSDomain { get; set; }
        /// <summary>
        /// GUID assigned to user's AD account.
        /// </summary>
        public string ADUserID { get; set; }
        public string EmailAddress { get; set; }
        public bool EmailVerified { get; set; }
        public string OldEmailAddress { get; set; }
        public string SecurityQuestion { get; set; }
        public string SecurityAnswer { get; set; }
        /// <summary>
        /// Is the user required to read a TOS update.
        /// </summary>
        public bool RequireTOS { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactPhone { get; set; }
        public DateTime UpdateDate { get; set; }
        public int UpdateUser { get; set; }
        /// <summary>
        /// Is the user a Paramount employee.
        /// </summary>
        public bool IsParamount { get; set; }
        public string CellPhone { get; set; }
        /// <summary>
        /// Has the account been locked and cannot be used.
        /// </summary>
        public bool SecurityLocked { get; set; }
        /// <summary>
        /// This is a list of all the members and providers the user can access.
        /// </summary>
        public List<AccessControl> AccessControls { get; set; }
    }
}

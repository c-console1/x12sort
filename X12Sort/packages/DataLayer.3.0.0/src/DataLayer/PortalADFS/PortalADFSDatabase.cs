using DataLayer.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.PortalADFS
{
    public class PortalADFSDatabase : BaseDatabase
    {
        public PortalADFSRepository PortalRepository { get; private set; }

        internal PortalADFSDatabase(IDbExec dbExec) : base(dbExec)
        {
            PortalRepository = new PortalADFSRepository(this);
        }

        internal PortalADFSDatabase(params IExecutor[] executors) : base(executors)
        {
            PortalRepository = new PortalADFSRepository(this);
        }
    }
}

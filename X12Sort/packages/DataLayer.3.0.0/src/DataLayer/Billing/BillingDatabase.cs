using DataLayer.Base;
using System;

namespace DataLayer.Billing
{
    /// <summary>
    /// The billing system database. For now, this is AMISYS
    /// </summary>
    public class BillingDatabase : BaseDatabase
    {
        /// <summary>
        /// Repository of queries for interacting with members in the billing system
        /// </summary>
        public AddressRepository Addresses { get; private set; }
        public GroupRepository Groups { get; private set; }
        public MemberRepository Members { get; private set; }
        public PharmacyRepository Pharmacies { get; private set; }
        public ProviderRepository Providers { get; private set; }
        public UtilityRepository Utilities { get; private set; }
        public TaxRepository Taxes { get; private set; }

        internal BillingDatabase(IDbExec dbExec) : base(dbExec)
        {
            Addresses = new AddressRepository(this);
            Groups = new GroupRepository(this);
            Members = new MemberRepository(this);
            Pharmacies = new PharmacyRepository(this);
            Providers = new ProviderRepository(this);
            Utilities = new UtilityRepository(this);
            Taxes = new TaxRepository(this);
        }

        internal BillingDatabase(params IExecutor[] executors) : base(executors)
        {
            Addresses = new AddressRepository(this);
            Groups = new GroupRepository(this);
            Members = new MemberRepository(this);
            Pharmacies = new PharmacyRepository(this);
            Providers = new ProviderRepository(this);
            Utilities = new UtilityRepository(this);
            Taxes = new TaxRepository(this);
        }
    }
}

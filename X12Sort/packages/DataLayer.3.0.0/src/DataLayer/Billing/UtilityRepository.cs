using System;
using System.Data;
using System.Text;
using DataLayer.Base;
using DataLayer.StaticUtilities;
using NLog;

namespace DataLayer.Billing
{
    public class UtilityRepository : BaseRepository<BillingDatabase>
    {
        public UtilityRepository(BillingDatabase database) : base(database)
        {
        }

        /// <summary>
        /// Creates an Oracle table represented by the provided datatable
        /// </summary>
        /// <param name="tableName">Name of table to be created</param>
        /// <param name="table">Datatable representing database structure to be created</param>
        /// <returns></returns>
        public int CreateTable(string tableName, DataTable table)
        {
            if (table == null)
                return 0;

            DropTable(tableName);

            StringBuilder sql = new StringBuilder();
            sql.AppendLine("CREATE TABLE " + tableName);

            sql.AppendLine("(");
            for (int i = 0; i < table.Columns.Count; i++)
            {
                string columnName = CleanUpColumnName(table.Columns[i].ColumnName);

                if (i == 0)
                    sql.Append("  ");
                else
                    sql.Append(", ");

                switch (table.Columns[i].DataType.FullName)
                {
                    case "System.String":
                        sql.Append(string.Format("{0} VARCHAR2(4000)", columnName));
                        break;
                    case "System.Int32":
                        sql.Append(string.Format("{0} NUMBER", columnName));
                        break;
                    case "System.DateTime":
                        sql.Append(string.Format("{0} TIMESTAMP", columnName));
                        break;
                    default:
                        sql.Append(string.Format("{0} VARCHAR2({1})", columnName, 255));
                        break;
               }
            }
            sql.AppendLine(")");

            ILogger logger = LogManager.GetLogger("TableManipulation");
            logger.Debug(sql.ToString());
            
            int result = Database.Execute(sql.ToString());
            return result;
        }

        /// <summary>
        /// Drops a database table
        /// </summary>
        /// <param name="tableName">Name of table to drop</param>
        /// <returns></returns>
        public int DropTable(string tableName)
        {
            string sql = @"BEGIN
                   EXECUTE IMMEDIATE 'DROP TABLE ' || :tableName;
                EXCEPTION
                   WHEN OTHERS THEN
                      IF SQLCODE != -942 THEN
                         RAISE;
                      END IF;
                END;";

            int result = Database.Execute(sql, new
            {
                tableName = tableName
            });
            return result;
        }

        /// <summary>
        /// Sanitizes column name for use in Oracle
        /// </summary>
        /// <param name="colName">Column name to sanitize</param>
        /// <returns></returns>
        private static string CleanUpColumnName(string colName)
        {

            colName = colName.Sanitize().ToUpper();
            if (colName.Length > 30)
                return colName.Substring(0, 30);
            return colName.PadRight(30);
        }

        /// <summary>
        /// Inserts data into an existing Oracle table
        /// </summary>
        /// <param name="tableName">Oracle table to insert to</param>
        /// <param name="table">Datatable to insert data from</param>
        /// <param name="dateFieldsAreNumber">True=yyyymmdd number, false=oracle timestamp</param>
        /// <returns></returns>
        public int InsertDataTable(string tableName, DataTable table, bool dateFieldsAreNumber)
        {
            if (table == null || table.Rows.Count == 0)
                return -1;

            int result = 0;

            foreach (DataRow row in table.Rows)
            {
                StringBuilder sql = new StringBuilder("insert into " + tableName + " values ( ");
                for (int i = 0; i < row.ItemArray.Length; i++)
                {
                    if (i > 0) sql.Append(", ");
                    if (row.ItemArray[i] == DBNull.Value) sql.Append("NULL");
                    else switch (row.Table.Columns[i].DataType.FullName)
                        {
                            case "System.String":
                                sql.Append(string.Format("'{0}'", row.ItemArray[i].ToString().Replace("'", "''")));
                                break;
                            case "System.Int32":
                                sql.Append(string.Format("{0}", row.ItemArray[i].ToString()));
                                break;
                            case "System.DateTime":
                                if (dateFieldsAreNumber)
                                    sql.Append(string.Format("{0:yyyyMMdd}", row.ItemArray[i]));
                                else
                                    sql.Append(string.Format("TO_TIMESTAMP('{0:yyyy-MM-dd HH:mm:ss}', 'YYYY-MM-DD HH24:MI:SS')", (DateTime)row.ItemArray[i]));
                                break;
                            default:
                                sql.Append(string.Format("'{0}'", row.ItemArray[i].ToString().Replace("'", "''")));
                                break;
                        }
                }
                sql.Append(" )");

                result += Database.Execute(sql.ToString());
            }

            return result;
        }

        /// <summary>
        /// Creates an Oracle index
        /// </summary>
        /// <param name="tableName">Table name to create index on</param>
        /// <param name="columns">Array of columns to index</param>
        /// <returns></returns>
        public int CreateIndex(string tableName, string[] columns)
        {
            string sql = "";
            int result = 0;

            if (columns == null)
                return result;

            for (int i = 0; i < columns.Length; i++)
            {
                sql = "CREATE INDEX {0}_IX{1} ON amiuser1.{0} ( {2} ) TABLESPACE AMISYS_USER";
                string columnList = "";


                columnList = string.Join(",", columns);

                sql = string.Format(sql, tableName, i, columnList);
                result += Database.Execute(sql);

            }

            sql = @"
                    BEGIN
                      SYS.DBMS_STATS.GATHER_TABLE_STATS
                      (
                         ownname          => 'AMIUSER1'
                        ,tabname          => '{0}'
                        ,estimate_percent => 20
                        ,method_opt       => 'for all indexed columns size 1'
                        ,cascade          => true
                      );
                    END;
                    ";

            sql = string.Format(sql, tableName);
            Database.Execute(sql);

            return result;
        }

    }
}

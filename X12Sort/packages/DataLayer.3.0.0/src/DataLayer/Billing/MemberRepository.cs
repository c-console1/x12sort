using DataLayer.Base;
using DataLayer.Billing.Models;
using System.Collections.Generic;

namespace DataLayer.Billing
{
    /// <summary>
    /// The Member Repository, a collection functions related to members in the billing system
    /// </summary>
    public class MemberRepository : BaseRepository<BillingDatabase>
    {
        internal MemberRepository(BillingDatabase database) : base(database)
        {
        }

        /// <summary>
        /// Returns an enumerable collection of ALL MEMBER in the billing system.
        /// </summary>
        /// <returns>Returned as an unbuffered results, means that the list is populated from database on demand. Calling `ToList()` will load the entire list from the database</returns>
        public IEnumerable<Member> GetMembers()
        {
            string query = @"
                SELECT
                    IMAGE_RECNBR AS DatabaseKey,
                    MEMBER_NBR AS MemberNumber,
                    CONTRACT_NBR AS ContractNumber,
                    TRIM(MEMBER.NAME_FIRST) AS FirstName,
                    TRIM(MEMBER.NAME_LAST) AS LastName,
                    TRIM(MEMBER.NAME_MIDDLE) AS MiddleName,
                    TRIM(MEMBER.NAME_PREFIX) AS Prefix,
                    TRIM(MEMBER.NAME_SUFFIX) AS Suffix,
                    TRIM(MEMBER.SEX) AS Sex,
                    TRIM(MEMBER.SSN) AS SSN,
                    CASE WHEN MEMBER.YMDBIRTH > 0 THEN to_char(to_date(MEMBER.YMDBIRTH, 'yyyymmdd'), 'mm/dd/yyyy') ELSE NULL END  AS BirthDate,
                    CASE WHEN MEMBER.YMDDEATH > 0 THEN to_char(to_date(MEMBER.YMDDEATH, 'yyyymmdd'), 'mm/dd/yyyy') ELSE NULL END  AS DeathDate,
                    TRIM(MEMBER.RECORD_NBR) AS RecordNumber,
                    TRIM(MEMBER.HCFA_NBR) AS MedicareNumber,
                    TRIM(MEMBER.ALT_KEY) AS MedicaidNumber,
                    TRIM(MEMBER.ALT_KEY2) AS IncomingNumber,
                    CASE WHEN MEMBER.YMDTRANS > 0 THEN to_char(to_date(MEMBER.YMDTRANS, 'yyyymmdd'), 'mm/dd/yyyy') ELSE NULL END  AS UpdateDate
                FROM MEMBER";

            return Database.Query<Member>(query, buffered: false);
        }

        /// <summary>
        /// Gets a single member from the member number
        /// </summary>
        /// <param name="memberNumber">The member number for the individual</param>
        /// <returns>A member object</returns>
        public Member GetMember(string memberNumber)
        {
            string query = @"
                SELECT
                    IMAGE_RECNBR AS DatabaseKey,
                    MEMBER_NBR AS MemberNumber,
                    CONTRACT_NBR AS ContractNumber,
                    TRIM(MEMBER.NAME_FIRST) AS FirstName,
                    TRIM(MEMBER.NAME_LAST) AS LastName,
                    TRIM(MEMBER.NAME_MIDDLE) AS MiddleName,
                    TRIM(MEMBER.NAME_PREFIX) AS Prefix,
                    TRIM(MEMBER.NAME_SUFFIX) AS Suffix,
                    TRIM(MEMBER.SEX) AS Sex,
                    TRIM(MEMBER.SSN) AS SSN,
                    CASE WHEN MEMBER.YMDBIRTH > 0 THEN to_char(to_date(MEMBER.YMDBIRTH, 'yyyymmdd'), 'mm/dd/yyyy') ELSE NULL END  AS BirthDate,
                    CASE WHEN MEMBER.YMDDEATH > 0 THEN to_char(to_date(MEMBER.YMDDEATH, 'yyyymmdd'), 'mm/dd/yyyy') ELSE NULL END  AS DeathDate,
                    TRIM(MEMBER.RECORD_NBR) AS RecordNumber,
                    TRIM(MEMBER.HCFA_NBR) AS MedicareNumber,
                    TRIM(MEMBER.ALT_KEY) AS MedicaidNumber,
                    TRIM(MEMBER.ALT_KEY2) AS IncomingNumber,
                    CASE WHEN MEMBER.YMDTRANS > 0 THEN to_char(to_date(MEMBER.YMDTRANS, 'yyyymmdd'), 'mm/dd/yyyy') ELSE NULL END  AS UpdateDate
                FROM MEMBER
                WHERE RTRIM(MEMBER_NBR) = :memberNumber";

            return Database.QueryFirstOrDefault<Member>(query, new { memberNumber = memberNumber});
        }
    }
}

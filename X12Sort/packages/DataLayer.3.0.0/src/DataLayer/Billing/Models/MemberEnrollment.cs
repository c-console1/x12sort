﻿using System;

namespace DataLayer.Billing.Models
{
    public class MemberEnrollment : BaseBillingObject
    {
        public MemberEnrollment() : base()
        {
        }

        public string MemberNumber { get; set; }
        public string PCPProviderId { get; set; }
        public string DivisionId { get; set; }
        public string GroupId { get; set; }
        public string ProductCode { get; set; }
        public bool Void { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}

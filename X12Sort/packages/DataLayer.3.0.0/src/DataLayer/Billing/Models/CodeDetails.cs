﻿using DataLayer.Base.Models;

namespace DataLayer.Billing.Models
{
    public class CodeDetails : BaseBillingObject
    {
        public CodeDetails() : base()
        {
        }

        public string Code { get; set; }
        public string Prefix { get; set; }
        public string Value { get; set; }
        public string Value2 { get; set; }
        public string Value3 { get; set; }

        public static string ConvertMemberType(MemberIdentifierType code)
        {
            switch (code)
            {
                case MemberIdentifierType.HICN:
                case MemberIdentifierType.RRB:
                    return "HI";
                case MemberIdentifierType.MBI:
                    return "MB";
                case MemberIdentifierType.CaseNumber:
                    return "CS";
                case MemberIdentifierType.Medicaid:
                    return "MD";
                case MemberIdentifierType.Unknown:
                    return "UN";
                case MemberIdentifierType.Paramount:
                case MemberIdentifierType.SSN:
                default:
                    return "";
            }
        }
    }
}

using DataLayer.Base.Enumerations;
using DataLayer.Base.Models;
using System;
using System.Collections.Generic;

namespace DataLayer.Billing.Models
{
    public class Provider : BaseBillingObject, IProvider
    {
        /// <summary>
        /// The day the person was born
        /// </summary>
        public DateTime? BirthDate { get; set; }

        /// <summary>
        /// The day the person died
        /// </summary>
        public DateTime? DeathDate { get; set; }

        public string ProviderNumber { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Degree { get; set; }
        public string Title { get; set; }
        public Sex Sex { get; set; }
        public string Ethnicity { get; set; }
        public string FacilityIndividual { get; set; }
        public string InstitutionName { get; set; }
        public string MedicareNumber { get; set; }
        public string MedicaidNumber { get; set; }
        public string NPI { get; set; }
        public string TaxID { get; set; }
        public bool IsIndividual
        {
            get
            {
                return !string.IsNullOrEmpty(LastName);
            }
        }
        public bool IsInstitution
        {
            get
            {
                return !string.IsNullOrEmpty(InstitutionName);
            }
        }
        public IEnumerable<ProviderSpecialty> Specialties { get; set; }

    }
}

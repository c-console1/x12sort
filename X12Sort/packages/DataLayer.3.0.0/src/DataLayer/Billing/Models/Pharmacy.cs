﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Billing.Models
{
    public class Pharmacy : BaseBillingObject
    {
        public string ProviderNumber { get; set; }
        public string PharmacyName { get; set; }
        public string NPI { get; set; }
        public string Address1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone1 { get; set; }
    }
}

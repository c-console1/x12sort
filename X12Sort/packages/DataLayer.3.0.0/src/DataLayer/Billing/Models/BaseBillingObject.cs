using DataLayer.Base.Enumerations;
using DataLayer.Base.Models;
using System;

namespace DataLayer.Billing.Models
{
    /// <summary> Base class for all billing models, extends BaseDataObject </summary>
    public abstract class BaseBillingObject : BaseDataObject
    {
        public override DataSource Source { get => DataSource.AMISYS; }

        /// <summary> The date this records was updated. Default/No Value: DateTime.MinValue </summary>
        public DateTime UpdateDate { get; set; }

        /// <summary>
        /// High date value based on Amisys standard
        /// </summary>
        /// <value>The date 12/31/9999 </value>
        public static DateTime HighDate { get { return DateTime.Parse("12/31/9999"); } }

        /// <summary>
        /// Low date value based on Amisys standard
        /// </summary>
        /// <value>The date 01/01/1900 </value>
        public static DateTime LowDate { get { return DateTime.Parse("01/01/1900"); } }

        /// <summary>
        /// Before Write, it updates the Updated date
        /// </summary>
        public override void PrepForWrite()
        {
            UpdateDate = DateTime.Now;
        }

        /// <summary>
        /// Standard Constructor
        /// </summary>
        protected BaseBillingObject() : base()
        {
            UpdateDate = DateTime.MinValue;
        }
    }
}

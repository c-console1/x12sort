using System;
using System.Collections.Generic;
using DataLayer.Base;
using DataLayer.Billing.Models;

namespace DataLayer.Billing
{
    public class GroupRepository : BaseRepository<BillingDatabase>
    {
        public GroupRepository(BillingDatabase database) : base(database)
        {
        }

        /// <summary>
        /// Gets a provider group by group practice number
        /// </summary>
        /// <param name="practiceNumber">Group practice number to get group for</param>
        /// <returns></returns>
        public Group GetProviderGroup(string practiceNumber)
        {
            string sql = @"select image_recnbr as DatabaseKey, prac_nbr as GroupNumber, rtrim(irs_nbr) as IRSNumber, rtrim(name_x) as Name,
                case when ymdeff > 0 then to_char(to_date(ymdeff, 'yyyymmdd'), 'mm/dd/yyyy') else null end as StartDate,
                case when ymdend > 0 then to_char(to_date(ymdend, 'yyyymmdd'), 'mm/dd/yyyy') else null end as EndDate
                from amiown.group_practice_m
                where 1 = 1
                and prac_nbr = :practiceNumber";

            Group results = Database.QueryFirstOrDefault<Group>(sql, new
            {
                practiceNumber = practiceNumber
            });

            return results;
        }

    }
}

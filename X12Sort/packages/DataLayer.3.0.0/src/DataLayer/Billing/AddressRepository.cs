using System;
using System.Collections.Generic;
using DataLayer.Base;
using DataLayer.Billing.Models;

namespace DataLayer.Billing
{
    public class AddressRepository : BaseRepository<BillingDatabase>
    {
        public AddressRepository(BillingDatabase database) : base(database)
        {
        }

        /// <summary>
        /// Gets the default address for a given group practice
        /// </summary>
        /// <param name="practiceNumber">Group practice to get address for</param>
        /// <returns></returns>
        public Address GetDefaultGroupAddress(string practiceNumber)
        {
            return GetDefaultGroupAddress(practiceNumber, null, null);
        }

        /// <summary>
        /// Gets the default address for a given group practice
        /// </summary>
        /// <param name="practiceNumber">Group practice to get address for</param>
        /// <returns>Default group address</returns>
        public Address GetCurrentDefaultGroupAddress(string practiceNumber)
        {
            DateTime? now = DateTime.Now;
            return GetDefaultGroupAddress(practiceNumber, now, now);
        }

        /// <summary>
        /// Gets the default address for a given group practice in a date span
        /// </summary>
        /// <param name="practiceNumber">Group practice to get address for</param>
        /// <param name="startDate">Address start date</param>
        /// <param name="endDate">Address end date</param>
        /// <returns>Default group address</returns>
        public Address GetDefaultGroupAddress(string practiceNumber, DateTime? startDate, DateTime? endDate)
        {
            string startDateStr = "", endDateStr = "";
            practiceNumber = ("D   G " + practiceNumber).PadRight(22);

            string sql = @"select a.image_recnbr as DatabaseKey, rtrim(a.address1) as Address1, rtrim(a.address2) as Address2, rtrim(a.city) as City, rtrim(a.county) as County, rtrim(a.state) as State,
                rtrim(a.zip) as Zip, rtrim(a.zip) as ZipCode, 'USA' as Country, rtrim(a.phone1) as Phone1,
                case when a.phone1 <> '                    ' and substr(cd1.description2, 9, 2) = 'FX' then 'Fax'
                    when a.phone1 <> '                    ' then 'Phone'
                    else null
                    end as Phone1Type,
                rtrim(a.phone2) as Phone2,
                case when a.phone2 <> '                    ' and substr(cd2.description2, 9, 2) = 'FX' then 'Fax'
                    when a.phone2 <> '                    ' then 'Phone'
                    else null
                    end as Phone2Type,
                rtrim(a.phone3) as Phone3, 
                case when a.phone3 <> '                    ' and substr(cd3.description2, 9, 2) = 'FX' then 'Fax'
                    when a.phone3 <> '                    ' then 'Phone'
                    else null
                    end as Phone3Type,
                rtrim(a.email) as Email,
                to_char(to_date(a.ymdeff, 'yyyymmdd'), 'mm/dd/yyyy') as StartDate,
                to_char(to_date(a.ymdend, 'yyyymmdd'), 'mm/dd/yyyy') as EndDate
                from amiown.address a
                left join amiown.code_detail cd1 on 'PI'||a.phone1info = cd1.detail_code_nbr
                left join amiown.code_detail cd2 on 'PI'||a.phone2info = cd2.detail_code_nbr
                left join amiown.code_detail cd3 on 'PI'||a.phone3info = cd3.detail_code_nbr
                where 1 = 1
                and addrtype_who = :practiceNumber";
            if (startDate != null)
            {
                sql += "\r\nand ymdeff <= :ymdeff";
                startDateStr = startDate?.ToString("yyyyMMdd");
            }
            if (endDate != null)
            {
                sql += "\r\nand ymdend >= :ymdend";
                endDateStr = endDate?.ToString("yyyyMMdd");
            };

            Address results = Database.QueryFirstOrDefault<Address>(sql, new
            {
                practiceNumber = practiceNumber,
                ymdEff = startDateStr,
                ymdEnd = endDateStr
            });
            return results;
        }

        /// <summary>
        /// Gets the default address for a given provider
        /// </summary>
        /// <param name="providerNumber">Provider to get address for</param>
        /// <returns>Default provider address</returns>
        public Address GetDefaultProviderAddress(string providerNumber)
        {
            return GetDefaultProviderAddress(providerNumber, null, null);
        }

        /// <summary>
        /// Gets the default address for a given provider
        /// </summary>
        /// <param name="providerNumber">Provider to get address for</param>
        /// <returns>Default provider address</returns>
        public Address GetCurrentDefaultProviderAddress(string providerNumber)
        {
            DateTime? now = DateTime.Now;
            return GetDefaultProviderAddress(providerNumber, now, now);
        }

        /// <summary>
        /// Gets the default address for a given provider in a date span
        /// </summary>
        /// <param name="providerNumber">Provider to get address for</param>
        /// <param name="startDate">Address start date</param>
        /// <param name="endDate">Address end date</param>
        /// <returns>Default provider address</returns>
        public Address GetDefaultProviderAddress(string providerNumber, DateTime? startDate, DateTime? endDate)
        {
            string startDateStr = "", endDateStr = "";
            providerNumber = ("D   P " + providerNumber).PadRight(22);

            string sql = @"select a.image_recnbr as DatabaseKey, rtrim(a.address1) as Address1, rtrim(a.address2) as Address2, rtrim(a.city) as City, rtrim(a.county) as County, rtrim(a.state) as State,
                rtrim(a.zip) as Zip, rtrim(a.zip) as ZipCode,
                'USA' as Country, rtrim(a.phone1) as Phone1,
                case when a.phone1 <> '                    ' and substr(cd1.description2, 9, 2) = 'FX' then 'Fax'
                    when a.phone1 <> '                    ' then 'Phone'
                    else null
                    end as Phone1Type,
                rtrim(a.phone2) as Phone2,
                case when a.phone2 <> '                    ' and substr(cd2.description2, 9, 2) = 'FX' then 'Fax'
                    when a.phone2 <> '                    ' then 'Phone'
                    else null
                    end as Phone2Type,
                rtrim(a.phone3) as Phone3, 
                case when a.phone3 <> '                    ' and substr(cd3.description2, 9, 2) = 'FX' then 'Fax'
                    when a.phone3 <> '                    ' then 'Phone'
                    else null
                    end as Phone3Type,
                rtrim(a.email) as Email,
                to_char(to_date(a.ymdeff, 'yyyymmdd'), 'mm/dd/yyyy') as StartDate,
                to_char(to_date(a.ymdend, 'yyyymmdd'), 'mm/dd/yyyy') as EndDate
                from amiown.address a
                left join amiown.code_detail cd1 on 'PI'||a.phone1info = cd1.detail_code_nbr
                left join amiown.code_detail cd2 on 'PI'||a.phone2info = cd2.detail_code_nbr
                left join amiown.code_detail cd3 on 'PI'||a.phone3info = cd3.detail_code_nbr
                where 1 = 1
                and addrtype_who = :providerNumber";
            if (startDate != null)
            {
                sql += "\r\nand ymdeff <= :ymdeff";
                startDateStr = startDate?.ToString("yyyyMMdd");
            }
            if (endDate != null)
            {
                sql += "\r\nand ymdend >= :ymdend";
                endDateStr = endDate?.ToString("yyyyMMdd");
            }

            Address results = Database.QueryFirstOrDefault<Address>(sql, new
            {
                providerNumber = providerNumber,
                ymdEff = startDateStr,
                ymdEnd = endDateStr
            });
            return results;
        }
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using DataLayer.Base;
using DataLayer.Billing.Models;

namespace DataLayer.Billing
{
    public class ProviderRepository : BaseRepository<BillingDatabase>
    {
        public ProviderRepository(BillingDatabase database) : base(database)
        {
        }

        #region provider
        /// <summary>
        /// Get a provider
        /// </summary>
        /// <param name="providerNumber">Provider number</param>
        /// <returns></returns>
        public Provider GetProvider(string providerNumber)
        {
            return GetProviders(providerNumber, "", "", "", null, null).FirstOrDefault();
        }

        /// <summary>
        /// Get a provider
        /// </summary>
        /// <param name="firstName">Provider first name</param>
        /// <param name="lastName">Provider last name</param>
        /// <param name="npi">NPI</param>
        /// <returns></returns>
        public Provider GetProvider(string firstName, string lastName, string npi)
        {
            return GetProviders("", firstName, lastName, npi, null, null).FirstOrDefault();
        }

        /// <summary>
        /// Get all providers
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Provider> GetProviders()
        {
            return GetProviders("", "", "", "", null, null);
        }

        /// <summary>
        /// Gets providers with affiliations, bounded by date
        /// </summary>
        /// <param name="startDate">Affiliation start date</param>
        /// <param name="endDate">Affiliation end date</param>
        /// <returns></returns>
        public IEnumerable<Provider> GetProviders(DateTime? startDate, DateTime? endDate)
        {
            return GetProviders("", "", "", "", startDate, endDate);
        }

        /// <summary>
        /// Gets a list of providers
        /// </summary>
        /// <param name="providerNumber">Provider number</param>
        /// <param name="firstName">Provider first name</param>
        /// <param name="lastName">Provider last name</param>
        /// <param name="npi">NPI</param>
        /// <param name="startDate">Affiliation start date</param>
        /// <param name="endDate">Affiliation end date</param>
        /// <returns></returns>
        private IEnumerable<Provider> GetProviders(string providerNumber, string firstName, string lastName, string npi, DateTime? startDate, DateTime? endDate)
        {
            string ymdEff = "";
            string ymdEnd = "";

            string sql = @"select image_recnbr as DatabaseKey, rtrim(prov_nbr) as ProviderNumber, rtrim(firstname) as FirstName, rtrim(lastname) as LastName, rtrim(degree) as Degree,
                rtrim(title) as Title, rtrim(sex) as Sex, rtrim(ethnic) as Ethnicity,
                case when ymdbirth > 0 then to_char(to_date(ymdbirth, 'yyyymmdd'), 'mm/dd/yyyy')
                    else null
                    end as BirthDate,
                rtrim(instname) as InstitutionName,
                case when rtrim(npi) is not null then rtrim(npi)
                else
                (   select substr(alt_id_who, 7, 10) as npi
                    from amiown.alt_identifier
                    where 1=1
                    and void = '  '
                    and (alt_ref_id_who = 'P ' || rpad(provider.prov_nbr, 16) || 'SP  ')
                    and ymdeff <= :ymdEff
                    and ymdend >= :ymdEnd
                    and rtrim(substr(alt_id_who, 7, 10)) is not null
                    and rownum = 1
                ) end as NPI,
                case when rtrim(spec1) in ('M6', 'C7', '10', '23', '01', 'A9', 'H7', 'F3', '11', '03', '22', 'VA') then 'Facility'
                    else 'Individual'
                    end as FacilityIndividual
                from amiown.provider
                where 1 = 1";

            if (!String.IsNullOrEmpty(providerNumber))
            {
                providerNumber = PadProviderNumber(providerNumber);
                sql += "\r\nand prov_nbr = :prov_nbr";
            }
            if (!String.IsNullOrEmpty(firstName))
                sql += "\r\nand firstname = :firstName";
            if (!String.IsNullOrEmpty(lastName))
                sql += "\r\nand lastname = :lastName";
            if (!String.IsNullOrEmpty(npi))
                sql += "\r\nand npi = :npi";

            if (startDate != null || endDate != null)
            {
                sql += @"and prov_nbr in(
                    select prov_nbr
                    from amiown.affiliation
                    where 1 = 1
                    and void = '  '";

                if (startDate != null)
                {
                    sql += "\r\nand ymdeff <= :ymdEff";
                    ymdEff = startDate?.ToString("yyyyMMdd");
                }
                if (endDate != null)
                {
                    sql += "\r\nand ymdend >= :ymdEnd";
                    ymdEnd = endDate?.ToString("yyyyMMdd");
                }
                
                sql += ")";
            }

            IEnumerable<Provider> results = Database.Query<Provider>(sql, new
            {
                prov_nbr = providerNumber,
                firstname = firstName,
                lastname = lastName,
                npi = npi,
                ymdeff = ymdEff,
                ymdend = ymdEnd
            });
            return results;
        }

        /// <summary>
        /// Gets providers with claims, bounded by date
        /// </summary>
        /// <param name="startDate">Claim start date</param>
        /// <param name="endDate">Claim end date</param>
        /// <returns></returns>
        public IEnumerable<Provider> GetProvidersWithClaimsBetween(DateTime startDate, DateTime endDate)
        {
            return GetProvidersWithClaimsBetween(startDate, endDate, true, true);
        }

        /// <summary>
        /// Gets providers with claims, bounded by date
        /// </summary>
        /// <param name="startDate">Claim start date</param>
        /// <param name="endDate">Claim end date</param>
        /// <param name="includePar">Include par providers</param>
        /// <param name="includeNonPar">Include non par providers</param>
        /// <returns></returns>
        public IEnumerable<Provider> GetProvidersWithClaimsBetween(DateTime startDate, DateTime endDate, bool includePar, bool includeNonPar)
        {
            return GetProvidersWithClaimsBetween(startDate, endDate, includePar, includeNonPar, true);
        }

        /// <summary>
        /// Gets providers with claims, bounded by date
        /// </summary>
        /// <param name="startDate">Claim start date</param>
        /// <param name="endDate">Claim end date</param>
        /// <param name="includePar">Include par providers</param>
        /// <param name="includeNonPar">Include non par providers</param>
        /// <param name="includeMedicaidReclamation">Include medicaid reclamation providers</param>
        /// <returns></returns>
        public IEnumerable<Provider> GetProvidersWithClaimsBetween(DateTime startDate, DateTime endDate, bool includePar, bool includeNonPar, bool includeMedicaidReclamation)
        {
            return GetProvidersWithClaimsBetween(startDate, endDate, includePar, includeNonPar, includeMedicaidReclamation, false);
        }

        /// <summary>
        /// Gets providers with claims, bounded by date
        /// </summary>
        /// <param name="startDate">Claim start date</param>
        /// <param name="endDate">Claim end date</param>
        /// <param name="includePar">Include par providers</param>
        /// <param name="includeNonPar">Include non par providers</param>
        /// <param name="includeMedicaidReclamation">Include medicaid reclamation providers</param>
        /// <param name="limitToMagellanDrugProcedureCodes">Limit claims to those used by Magellan drug procedures</param>
        /// <returns></returns>
        public IEnumerable<Provider> GetProvidersWithClaimsBetween(DateTime startDate, DateTime endDate, bool includePar, bool includeNonPar, bool includeMedicaidReclamation, bool limitToMagellanDrugProcedureCodes)
        {
            string claimStart = "";
            string claimEnd = "";

            //specialty type list used for determining individual/facility from michaele mccormick, credentialing
            string sql = @"select image_recnbr as DatabaseKey, rtrim(prov_nbr) as ProviderNumber, rtrim(firstname) as FirstName, rtrim(lastname) as LastName, rtrim(degree) as Degree,
                rtrim(title) as Title, rtrim(sex) as Sex, rtrim(ethnic) as Ethnicity,
                case when ymdbirth > 0 then to_char(to_date(ymdbirth, 'yyyymmdd'), 'mm/dd/yyyy')
                    else null
                    end as BirthDate,
                rtrim(instname) as InstitutionName,
                case when rtrim(npi) is not null then rtrim(npi)
                else
                (   select substr(alt_id_who, 7, 10) as npi
                    from amiown.alt_identifier
                	where 1=1
                    and void = '  '
                	and (alt_ref_id_who = 'P ' || rpad(provider.prov_nbr, 16) || 'SP  ')
                    and ymdeff <= :claimStart
                    and ymdend >= :claimEnd
                    and rtrim(substr(alt_id_who, 7, 10)) is not null
                    and rownum = 1
                ) end as NPI,
                case when rtrim(spec1) in ('02', '03', 'M6', 'C7', '01', '06', 'ED', 'F4', '09', '10', 'F3', 'F6', 'F5', 'A6', 'AT', 'EF', '11', 'B1', '12', '96', '19', 'A9', 'M8', '22', 'H7', '23', '92', 'A1', 'C9', 'R1', '07', 'B3', 'G9', 'VA') then 'Facility'
                    else 'Individual'
                    end as FacilityIndividual
                from amiown.provider
                where 1 = 1
                and prov_nbr in(
                    select distinct prov_nbr
                    from amiown.affiliation
                    where 1 = 1";

            //par status list from lisa line, provider database coordinator 
            string nonParStatus = "'FM','HB','LI','LU','MD','MI','MP','NF','NI','NM','NP','NS','NY','W9'";
            string medicaidReclamationStatus = "'MR'";

            if (includePar && !includeNonPar)
            {
                //return only par providers
                sql += " and status_x not in(" + nonParStatus;

                if (!includeMedicaidReclamation)
                    //medicaid reclemation not requested, exclude
                    sql += ", " + medicaidReclamationStatus;

                sql += ")";
            }
            else if (!includePar && includeNonPar)
            {
                //return only non par providers
                sql += " and status_x in(" + nonParStatus;

                if (includeMedicaidReclamation)
                    //medicaid reclemation requested, include
                    sql += ", " + medicaidReclamationStatus;

                sql += ")";
            }
            else if (!includePar && !includeNonPar && !includeMedicaidReclamation)
            {
                //return nothing
                sql += " and 0 = 1";
            }
            else if (!includeMedicaidReclamation)
            {
                //return only non medicaid reclamation providers
                sql += " and status_x not in(" + medicaidReclamationStatus + ")";
            }

            sql += @" and aff_nbr in(
                        select distinct aff_nbr
                        from amiown.service_x sx
                        join amiown.claim c on sx.claim_nbr = c.claim_nbr
                        where 1 = 1
                        and sx.ymdpaid >= :claimStart
                        and sx.ymdpaid <= :claimEnd";

            if (limitToMagellanDrugProcedureCodes)
                sql += @" and c.claim_type in ('H ', 'M ')
                          and exists(
                            select '1' from amiown.su_mrx_drug_proc_inc smdpi
                            where (smdpi.proc_nbr=sx.proc_nbr or smdpi.proc_nbr=sx.proc2_nbr)
                         )";

            sql += @"
                        )
                    )";

            claimStart = startDate.ToString("yyyyMMdd");
            claimEnd = endDate.ToString("yyyyMMdd");

            IEnumerable<Provider> results = Database.Query<Provider>(sql, new
            {
                claimStart = claimStart,
                claimEnd = claimEnd
            });

            return results;
        }
        #endregion

        #region provider affiliations
        /// <summary>
        /// Get all non void affiliations for a provider
        /// </summary>
        /// <param name="providerNumber">Provider to get affiliations for</param>
        /// <returns></returns>
        public IEnumerable<ProviderAffiliation> GetAffiliaitons(string providerNumber)
        {
            return GetAffiliaitons(providerNumber, false);
        }

        /// <summary>
        /// Get all affiliations for a provider
        /// </summary>
        /// <param name="providerNumber">Provider to get affiliations for</param>
        /// <param name="includeVoid">Include voided affiliations</param>
        /// <returns></returns>
        public IEnumerable<ProviderAffiliation> GetAffiliaitons(string providerNumber, bool includeVoid)
        {
            return GetAffiliaitons(providerNumber, includeVoid, null, null);
        }

        /// <summary>
        /// Get provider affiliations for a provider, bounded by date
        /// </summary>
        /// <param name="providerNumber">Provider to get affiliations for</param>
        /// <param name="includeVoid">Include voided affiliations</param>
        /// <param name="startDate">Affiliation start date</param>
        /// <param name="endDate">Affiliation end date</param>
        /// <returns></returns>
        public IEnumerable<ProviderAffiliation> GetAffiliaitons(string providerNumber, bool includeVoid, DateTime? startDate, DateTime? endDate)
        {
            providerNumber = PadProviderNumber(providerNumber);
            string ymdEff = "";
            string ymdEnd = "";

            string sql = @"select a.aff_nbr as DatabaseKey, rtrim(a.irs_nbr) as IRSNumber, rtrim(a.medicare_nbr) as MedicareNumber, rtrim(a.medicaid_nbr) as MedicaidNumber,
                rtrim(a.class_x) as Class, rtrim(a.hat_code) as HatCode, rtrim(a.prac_nbr) as PracticeNumber, a.business_unit || a.prog_nbr as Network, rtrim(a.spec1) as Specialty,
                case when a.ymdeff > 0 then to_char(to_date(a.ymdeff, 'yyyymmdd'), 'mm/dd/yyyy')
                    else null
                    end as StartDate,
                case when a.ymdend > 0 then to_char(to_date(a.ymdend, 'yyyymmdd'), 'mm/dd/yyyy')
                    else null
                    end as EndDate,
                case when substr(cd1.description2, 1, 1 ) = 'R' then 'false' else 'true' end as AcceptingNew,
                case when a.claim_type <> 'DX' then rtrim(claim_type)
                    else 'D'
                    end as claimtype,
                rtrim(cd2.description) as Status, case when a.status_x in('FM','HB','LI','LU','MD','MI','MP','NF','NI','NM','NP','NS','NY','W9') then 'false' else 'true' end as IsPar
                from amiown.affiliation a
                left join amiown.code_detail cd1 on 'CC'||a.accept = cd1.detail_code_nbr
                left join amiown.code_detail cd2 on 'PS'||a.status_x = cd2.detail_code_nbr
                where 1 = 1
                and prov_nbr = :providerNumber";

            if (!includeVoid)
                sql += "\r\nand void = '  '";
            if (startDate != null)
            {
                sql += "\r\nand ymdeff <= :ymdEff";
                ymdEff = startDate?.ToString("yyyyMMdd");
            }
            if (endDate != null)
            {
                sql += "\r\nand ymdend >= :ymdEnd";
                ymdEnd = endDate?.ToString("yyyyMMdd");
            }

            sql += "\r\norder by ymdend desc";

            IEnumerable<ProviderAffiliation> results = Database.Query<ProviderAffiliation>(sql, new
            {
                providerNumber = providerNumber,
                ymdeff = ymdEff,
                ymdend = ymdEnd
            });
            return results;
        }

        /// <summary>
        /// Gets a list of disctinct groups for a provider's current affiliations
        /// </summary>
        /// <param name="providerNumber">Provider to get group list for</param>
        /// <returns></returns>
        public IEnumerable<ProviderAffiliation> GetCurrentAffiliationDistinctGroups(string providerNumber)
        {
            DateTime? now = DateTime.Now;
            return GetAffiliationDistinctGroups(providerNumber, now, now);
        }

        /// <summary>
        /// Gets a list of distinct groups for a provider, no regard for date
        /// </summary>
        /// <param name="providerNumber">Provider to get group list for</param>
        /// <returns></returns>
        public IEnumerable<ProviderAffiliation> GetAffiliationDistinctGroups(string providerNumber)
        {
            return GetAffiliationDistinctGroups(providerNumber, null, null);
        }

        /// <summary>
        /// Gets a list of distinct groups for a  provider, bounded by date
        /// </summary>
        /// <param name="providerNumber">Provider to get group list for</param>
        /// <param name="startDate">Affiliation start date</param>
        /// <param name="endDate">Affiliation end date</param>
        /// <returns></returns>
        public IEnumerable<ProviderAffiliation> GetAffiliationDistinctGroups(string providerNumber, DateTime? startDate, DateTime? endDate)
        {
            providerNumber = PadProviderNumber(providerNumber);
            string ymdEff = "";
            string ymdEnd = "";

            string sql = @"
                select medicarenumber, medicaidnumber, practicenumber, network, startdate, enddate, status
                from(
                    select medicarenumber, medicaidnumber, practicenumber, network, startdate, enddate, status,
                        dense_rank() over(partition by network order by image_recnbr) as rank
                    from
                    (
                        select distinct rtrim(a.medicare_nbr) as MedicareNumber, rtrim(a.medicaid_nbr) as MedicaidNumber,
                            rtrim(a.prac_nbr) as PracticeNumber, a.business_unit || a.prog_nbr as Network,
                            case when a.ymdeff > 0 then to_char(to_date(a.ymdeff, 'yyyymmdd'), 'mm/dd/yyyy')
                                else null
                                end as StartDate,
                            case when a.ymdend > 0 then to_char(to_date(a.ymdend, 'yyyymmdd'), 'mm/dd/yyyy')
                                else null
                                end as EndDate,
                            rtrim(cd2.description) as Status, cd2.image_recnbr
                        from amiown.affiliation a
                        left join amiown.code_detail cd2 on 'PS'||a.status_x = cd2.detail_code_nbr
                        where 1 = 1
                        and void = '  '";
            if (startDate != null)
            {
                sql += "\r\nand ymdeff <= :ymdEff";
                ymdEff = startDate?.ToString("yyyyMMdd");
            }
            if (endDate != null)
            {
                sql += "\r\nand ymdend >= :ymdEnd";
                ymdEnd = endDate?.ToString("yyyyMMdd");
            }

            sql += "\r\n";
            sql += @"and prov_nbr = :providerNumber
                )
            )
            where rank = 1";

            IEnumerable<ProviderAffiliation> results = Database.Query<ProviderAffiliation>(sql, new
            {
                providerNumber = providerNumber,
                ymdeff = ymdEff,
                ymdend = ymdEnd
            });
            return results;
        }

        /// <summary>
        /// Gets current affiliations for a given provider and group
        /// </summary>
        /// <param name="providerNumber">Provider to get affiliations for</param>
        /// <param name="groupNumber">Group to get affiliations for</param>
        /// <returns></returns>
        public IEnumerable<ProviderAffiliation> GetCurrentAffiliationsForProviderGroup(string providerNumber, string groupNumber)
        {
            DateTime? now = DateTime.Now;

            return GetAffiliationsForProviderGroup(providerNumber, groupNumber, false, now, now);
        }

        /// <summary>
        /// Gets affiliations for a given provider and group
        /// </summary>
        /// <param name="providerNumber">Provider to get affiliations for</param>
        /// <param name="groupNumber">Group to get affiliations for</param>
        /// <param name="includeVoid">Include voided affiliations</param>
        /// <param name="startDate">Affiliation start date</param>
        /// <param name="endDate">Affiliation end date</param>
        /// <returns></returns>
        public IEnumerable<ProviderAffiliation> GetAffiliationsForProviderGroup(string providerNumber, string groupNumber, bool includeVoid, DateTime? startDate, DateTime? endDate)
        {
            providerNumber = PadProviderNumber(providerNumber);
            groupNumber = PadGroupNumber(groupNumber);
            string ymdEff = "";
            string ymdEnd = "";

            string sql = @"
                select MedicareNumber, MedicaidNumber, PracticeNumber, Network, StartDate, EndDate, status,
                dense_rank() over(partition by network order by enddate desc) as rank
                from (
                    select distinct a.medicare_nbr as MedicareNumber, a.medicaid_nbr as MedicaidNumber,
                    a.prac_nbr as PracticeNumber, a.business_unit || a.prog_nbr as Network,
                    case when a.ymdeff > 0 then to_char(to_date(a.ymdeff, 'yyyymmdd'), 'mm/dd/yyyy')
                        else null
                        end as StartDate,
                    case when a.ymdend > 0 then to_char(to_date(a.ymdend, 'yyyymmdd'), 'mm/dd/yyyy')
                        else null
                        end as EndDate,
                    rtrim(cd2.description) as status
                    from amiown.affiliation a
                    left join amiown.code_detail cd2 on 'PS'||a.status_x = cd2.detail_code_nbr
                    where 1 = 1
                    and prov_nbr = :providerNumber
                    and prac_nbr = :groupNumber";

            if (!includeVoid)
                sql += "\r\nand void = '  '";
            if (startDate != null)
            {
                sql += "\r\nand ymdeff <= :ymdEff";
                ymdEff = startDate?.ToString("yyyyMMdd");
            }
            if (endDate != null)
            {
                sql += "\r\nand ymdend >= :ymdEnd";
                ymdEnd = endDate?.ToString("yyyyMMdd");
            }

            sql += " )";

            IEnumerable<ProviderAffiliation> results = Database.Query<ProviderAffiliation>(sql, new
            {
                providerNumber = providerNumber,
                groupNumber = groupNumber,
                ymdeff = ymdEff,
                ymdend = ymdEnd
            });
            return results;
        }

        /// <summary>
        /// Gets the minimum affiliation start date for a provider
        /// </summary>
        /// <param name="providerNumber">Provider to get date for</param>
        /// <returns></returns>
        public DateTime GetMinAffiliationStartDate(string providerNumber)
        {
            providerNumber = PadProviderNumber(providerNumber);

            string sql = @"select to_char(to_date(min(ymdeff), 'yyyymmdd'), 'mm/dd/yyyy') as StartDate
                from amiown.affiliation
                where 1 = 1
                and prov_nbr = :providerNumber
                and void = '  '
                and ymdeff > 0
                and ymdend > 0";

            DateTime results = Database.QueryFirstOrDefault<DateTime>(sql, new { providerNumber = providerNumber });
            return results;
        }

        /// <summary>
        /// Gets the maximum affiliation end date for a provider
        /// </summary>
        /// <param name="providerNumber">Provider to get date for</param>
        /// <returns></returns>
        public DateTime GetMaxAffiliationEndDate(string providerNumber)
        {
            providerNumber = PadProviderNumber(providerNumber);

            string sql = @"select to_char(to_date(max(ymdend), 'yyyymmdd'), 'mm/dd/yyyy') as EndDate
                from amiown.affiliation
                where 1 = 1
                and prov_nbr = :providerNumber
                and void = '  '
                and ymdeff > 0
                and ymdend > 0";

            DateTime results = Database.QueryFirstOrDefault<DateTime>(sql, new { providerNumber = providerNumber });
            return results;
        }
        #endregion

        #region provider specialties
        /// <summary>
        /// Gets a current list of specialties for a provider
        /// </summary>
        /// <param name="providerNumber">Provider to get specialties for</param>
        /// <returns></returns>
        public IEnumerable<ProviderSpecialty> GetCurrentSpecialties(string providerNumber)
        {
            DateTime now = DateTime.Now;
            return GetSpecialties(providerNumber, now, now);
        }
        /// <summary>
        /// Gets a list of specialties for a providder
        /// </summary>
        /// <param name="providerNumber">Provider to get specialties for</param>
        /// <returns></returns>
        public IEnumerable<ProviderSpecialty> GetSpecialties(string providerNumber)
        {
            DateTime? now = DateTime.Now;
            return GetSpecialties(providerNumber, null, null);
        }
        /// <summary>
        /// Gets a list of specialties for a provider in a date range
        /// </summary>
        /// <param name="providerNumber">Provider to get specialties for</param>
        /// <param name="startDate">Affiliation start date</param>
        /// <param name="endDate">Affiliation end date</param>
        /// <returns></returns>
        public IEnumerable<ProviderSpecialty> GetSpecialties(string providerNumber, DateTime? startDate, DateTime? endDate)
        {
            string startDateStr = "", endDateStr = "";
            providerNumber = PadProviderNumber(providerNumber);

            string sql = @"select distinct spec1 as code, description, min(startdate) as StartDate, max(enddate) as EndDate from(
                    select a.spec1, cd.description,
                    case when a.ymdeff > 0 then to_char(to_date(a.ymdeff, 'yyyymmdd'), 'mm/dd/yyyy') else null end as StartDate,
                    case when a.ymdend > 0 then to_char(to_date(a.ymdend, 'yyyymmdd'), 'mm/dd/yyyy') else null end as EndDate
                    from amiown.affiliation a
                    join amiown.code_detail cd on 'SP' || a.spec1 = cd.code_nbr
                    where 1 = 1
                    and a.void = '  '
                    and a.spec1 <> '  '
                    and a.prov_nbr = :providerNumber";
            if (startDate != null)
            {
                sql += "\r\nand ymdeff <= :ymdeff";
                startDateStr = startDate?.ToString("yyyyMMdd");
            }
            if (endDate != null)
            {
                sql += "\r\nand ymdend >= :ymdend";
                endDateStr = endDate?.ToString("yyyyMMdd");
            }
            sql += "\r\n) x\r\ngroup by spec1, description";

            IEnumerable<ProviderSpecialty> results = Database.Query<ProviderSpecialty>(
                sql,
                new {
                    providerNumber = providerNumber,
                    ymdeff = startDateStr,
                    ymdend = endDateStr
                });
            return results;
        }
        #endregion

        #region provider types
        /// <summary>
        /// Gets the given provider's type
        /// </summary>
        /// <param name="providerNumber">Provider to get type for</param>
        /// <returns></returns>
        public string GetProviderType(string providerNumber)
        {
            return GetProviderType(providerNumber, null, null);
        }
        /// <summary>
        /// Gets the given provider's type
        /// </summary>
        /// <param name="providerNumber">Provider to get type for</param>
        /// <returns></returns>
        public string GetCurrentProviderType(string providerNumber)
        {
            DateTime? now = DateTime.Now;
            return GetProviderType(providerNumber, now, now);
        }
        /// <summary>
        /// Gets the given provider's type in a date span
        /// </summary>
        /// <param name="providerNumber">Provider to get type for</param>
        /// <param name="startDate">Affiliation start date</param>
        /// <param name="endDate">Affiliation end date</param>
        /// <returns></returns>
        public string GetProviderType(string providerNumber, DateTime? startDate, DateTime? endDate)
        {
            string startDateStr = "", endDateStr = "";
            providerNumber = PadProviderNumber(providerNumber);

            string sql = @"
                select case when class_x <> ' ' then class_x
                else hat_code
                end as provtype
                from(
                    select distinct class_x, hat_code
                    from amiown.affiliation
                    where 1 = 1
                    and void = '  '
                    and prov_nbr = :providerNumber";
            if (startDate != null)
            {
                sql += "\r\nand ymdeff <= :ymdeff";
                startDateStr = startDate?.ToString("yyyyMMdd");
            }
            if (endDate != null)
            {
                sql += "\r\nand ymdend >= :ymdend";
                endDateStr = endDate?.ToString("yyyyMMdd");
            }
            sql += "\r\n)x";

            string results = Database.QueryFirstOrDefault<string>(sql, new
            {
                providerNumber = providerNumber,
                ymdeff = startDateStr,
                ymdend = endDateStr
            });
            return results;
        }
        #endregion

        #region utility
        /// <summary>
        /// Pads the right side of a provider number to the standard width used by the database
        /// </summary>
        /// <param name="providerNumber">Provider number to pad</param>
        /// <returns></returns>
        private static string PadProviderNumber(string providerNumber)
        {
            if (string.IsNullOrEmpty(providerNumber))
                providerNumber = "";

            if (providerNumber.Length < 12)
                providerNumber = providerNumber.PadRight(12, ' ');

            return providerNumber;
        }

        /// <summary>
        /// Pads the right side of a group number to the standd width used by the database
        /// </summary>
        /// <param name="groupNumber"></param>
        /// <returns></returns>
        private static string PadGroupNumber(string groupNumber)
        {
            if (string.IsNullOrEmpty(groupNumber))
                groupNumber = "";

            if (groupNumber.Length < 4)
                groupNumber = groupNumber.PadRight(4, ' ');

            return groupNumber;
        }

        #endregion

    }
}


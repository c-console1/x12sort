using Dapper;
using DataLayer.Configuration.Models;
using DataLayer.StaticUtilities;
using DataLayer.StaticUtilities.Extensions;
using Flurl.Http;
using Flurl.Http.Xml;
using NLog;
using System;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace DataLayer
{
    /// <summary>
    /// API Executor Implementation.
    /// Wrapper around `FlurlClient` to provider an easily mock-able API handling
    /// </summary>
    /// <remarks>
    /// <para>
    /// This is build upon Flurl: https://flurl.dev/
    /// </para>
    /// <para>
    /// This class with also log API request start and end events to NLog at the debug level. Events are in `Client.Settings.BeforeCall` and `Client.Settings.AfterCall`
    /// </para>
    /// </remarks>
    /// <seealso cref="FlurlClient"/>
    public class ApiExec : IApiExec
    {
        /// <summary>
        /// Logging object
        /// </summary>
        private Logger _logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// private function that will setup the authentication for the API Request
        /// </summary>
        /// <param name="configDbExec">Configuration database executor</param>
        /// <param name="config">The API Connection Configuration Object</param>
        private void _setClientAuth(DbExec configDbExec, ConfigApiConnection config)
        {
            string key = configDbExec.Connection.QueryFirstOrDefault<string>(@"
EXECUTE AS USER = 'intrabit'
SELECT [das].fn_GetSecurityKey(@ConnectionID);
REVERT;
", new
            {
                config.ConnectionID
            });

            if (config.Authentication == ApiAuthenticationType.Basic)
            {
                Client = Client.WithBasicAuth(config.UserID, key);
                Password = key;
            }
            if (config.Authentication == ApiAuthenticationType.Token)
            {
                Client = Client.WithOAuthBearerToken(key);
            }
        }

        /// <summary>
        /// User ID for the call
        /// </summary>
        internal string UserID { get; private set; }

        [Obsolete("This is only being temporarily used by HealthRulesConnectorsDatabase and should not be used anywhere else. Will eventually be marked as Private.")]
        internal string Password { get; private set; }

        /// <summary>
        /// Get the 64-bit encoded username/password combination required for basic authentication (based on standard RFC 7617)
        /// </summary>
        public string EncodedAuthenticationCredentials { get { return Convert.ToBase64String(Encoding.ASCII.GetBytes($"{UserID}:{Password}")); } }

        /// <summary>
        /// Flurl URL client that is used by all request from this executor
        /// </summary>
        public IFlurlClient Client { get; set; }

        /// <summary>
        /// Name of the executor
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The logical database that this executor belongs to
        /// </summary>
        public string DatabaseGroup { get; private set; }

        /// <summary>
        /// public constructor
        /// </summary>
        /// <param name="configDbExec">Configuration database executor</param>
        /// <param name="config">The API Connection Configuration Object</param>
        public ApiExec(DbExec configDbExec, ConfigApiConnection config)
        {
            configDbExec.ThrowIfNull(nameof(configDbExec));
            config.ThrowIfNull(nameof(config));

            Name = config.Name.ToUpper();
            DatabaseGroup = config.DatabaseGroup;
            Client = new FlurlClient(config.Url);
            UserID = config.UserID;

            // add any header in the configuration database
            foreach (var kv in config.Headers)
            {
                Client.Headers[kv.Key] = kv.Value;
            }

            // log the beginning and end of the calls
            Client.Settings.BeforeCall += (HttpCall call) =>
            {
                _logger.Debug($"Starting Request {call.GetHashCode()} @ {DateTime.Now.ToString("T")}: '{call.ToString()}' ");
            };
            Client.Settings.AfterCall += (HttpCall call) =>
            {
                _logger.Debug($"Finished Request {call.GetHashCode()} @ {DateTime.Now.ToString("T")}, duration: {call.Duration}, status: {call.HttpStatus}");
            };

            _setClientAuth(configDbExec, config);
        }

        /// <summary>
        /// Create a new API Request objec6t
        /// </summary>
        /// <param name="UrlSegments">The segments that are appended to the URL</param>
        /// <returns>A new IFlurlRequest object for the given endpoint</returns>
        /// <example>
        /// This is a basic example for calling an endpoint. Assume that the `IApiExec` has a URL of "http://example.com"
        /// <code>
        /// <![CDATA[
        /// IApiExec apiExec = dataAccess.Database.Repo.ApiExec;
        /// IFlurlRequest request = apiExec.Request("api", "end", "point");
        /// request.Uri.ToString() // will be http://example.com/api/end/point
        /// ]]>
        /// </code>
        /// </example>
        public IFlurlRequest Request(params object[] UrlSegments)
        {
            return Client.Request(UrlSegments);
        }

        /// <summary>
        /// Creates a new Soap Request to the given URL and wraps/unwraps the request and response objects
        /// </summary>
        /// <param name="request">The request object that is wrapped in a SOAP header</param>
        /// <returns></returns>
        public async Task<TResponse> ExecuteSoapAsync<TRequest, TResponse>(ISoapRequest<TRequest, TResponse> request)
           where TRequest : class
           where TResponse : class
        {
            request.ThrowIfNull(nameof(request));

            XmlDocument envelope = request.MakeEnvelope();
            var requestTask = Request(request.EndPoint)
                .WithHeader("Content-Type", "text/xml")
                .WithHeader("Accept", "text/xml")
                .PostXmlAsync(envelope);
            var response = await requestTask.ReceiveXDocument();
            TResponse resBody = request.ReadEnvelope(response);
            return resBody;
        }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns> A string that represents the current object. </returns>
        public override string ToString()
        {
            return $"ApiExec: '{Client.BaseUrl}' - {DatabaseGroup}:{Name}";
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    Client.Dispose();
                }
                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion IDisposable Support
    }
}

using DataLayer.Base;
using DataLayer.CaseManagement.Models;
using System.Collections.Generic;
using System.Linq;
using System;
using DataLayer.StaticUtilities;
using DataLayer.StaticUtilities.Extensions;

namespace DataLayer.CaseManagement
{
    public class AssessmentRepository : BaseRepository<CaseManagementDatabase>
    {
        #region read only SQL Queries
        private const string _baseAssessmentQuery = @"
SELECT 
    Ass.assessment_id [ID],
    AM.assessment_name [AssessmentName],
    AM.assessment_meta_id  [DefinitionID],
    Ass.completed_date AS [CompletedDate],
    Ass.score AS [Score],
    M.external_member_id AS [MemberNumber],
    M.member_id as [MemberId]
FROM dbo.Assessment Ass
JOIN dbo.AssessmentMeta AM ON AM.assessment_meta_id = Ass.assessment_meta_id
JOIN dbo.Member M ON M.member_id = Ass.member_id
WHERE 1=1
    AND AM.meta_status = @PublishStatus
    AND AM.obsolete = @ObsoleteStatus
    AND Ass.obsolete = @ObsoleteStatus
";
        private const string _baseAnswerQuery = @"
SELECT
    A.answer_id as [ID],
    Ass.[MemberID],
    Ass.[MemberNumber],
    Ass.ID AS [AssessmentID],
    Ass.[CompletedDate],
    Ass.[Score],
    CASE 
        WHEN DAC.data_type = 'BOOLEAN'      THEN CAST(AP_B.boolean_answer AS VARCHAR)
        WHEN DAC.data_type = 'BOOLEAN_DATE' THEN CAST(AP_BD.boolean_answer   AS VARCHAR)
        WHEN DAC.data_type = 'DATE'         THEN CAST(AP_D.date_answer    AS VARCHAR)
        WHEN DAC.data_type = 'DIAGNOSIS'    THEN CAST(AP_Di.diagnosis_id  AS VARCHAR)
        -- Because TruCare is stupid, or may the people didn't make the assessment right
        -- but something the number answer is stored as a string...
        WHEN DAC.data_type = 'INTEGER'      THEN CAST(COALESCE(AP_N.number_answer, AP_S.string_answer)   AS VARCHAR)
        WHEN DAC.data_type = 'META_OPTION'  THEN CAST(AP_MOV.option_value AS VARCHAR)
        WHEN DAC.data_type = 'STRING'       THEN CAST(AP_S.string_answer  AS VARCHAR)
        ELSE NULL
    END AS [Value],
    COALESCE(CAST(AddAns_D.date_additional_answer AS varchar), CAST(AddAns_N.numeric_additional_answer as VARCHAR), CAST(AddAns_S.string_additional_Answer AS VARCHAR), null) AS [AnswerDetail],
    QD.question_id AS [QuestionID],
    AP_MO.meta_option_id AS [MetaOptionID],
    AP_BD.date_answer AS [BoolDate]
FROM dbo.QuestionDefinition QD
JOIN dbo.AnswerDefinition AD ON AD.question_id = QD.question_id 
JOIN dbo.DataTypeConfiguration DAC ON DAC.data_type_id= AD.data_type_id               
LEFT JOIN dbo.AnswerPart AP ON AP.answer_definition_id = AD.answer_definition_id
LEFT JOIN dbo.Answer A ON A.answer_id = AP.answer_id
LEFT JOIN _Assessments Ass ON Ass.[ID] = A.assessment_id
LEFT JOIN dbo.AnswerPartBoolean AP_B   ON AP.answer_part_id = AP_B.answer_part_id
LEFT JOIN dbo.AnswerPartBooleanDate AP_BD  ON AP.answer_part_id = AP_BD.answer_part_id
LEFT JOIN dbo.AnswerPartDate AP_D   ON AP.answer_part_id = AP_D.answer_part_id
LEFT JOIN dbo.AnswerPartDiagnosis AP_Di  ON AP.answer_part_id = AP_Di.answer_part_id
LEFT JOIN dbo.AnswerPartMetaOption AP_MO  ON AP.answer_part_id = AP_MO.answer_part_id
LEFT JOIN dbo.AnswerPartNumber AP_N   ON AP.answer_part_id = AP_N.answer_part_id
LEFT JOIN dbo.AnswerPartString AP_S   ON AP.answer_part_id = AP_S.answer_part_id
LEFT JOIN dbo.MetaOption AP_MOV ON AP_MO.meta_option_id = AP_MOV.meta_option_id
LEFT JOIN dbo.AdditionalAnswer       AddAns ON AP.answer_part_id = AddAns.answer_part_id
LEFT JOIN dbo.AdditionalAnswerDate   AddAns_D ON AddAns.additional_answer_id = AddAns_D.additional_answer_id
LEFT JOIN dbo.AdditionalAnswerNumber AddAns_N ON AddAns.additional_answer_id = AddAns_N.additional_answer_id
LEFT JOIN dbo.AdditionalAnswerString AddAns_S ON AddAns.additional_answer_id = AddAns_S.additional_answer_id
WHERE 1=1
    AND Ass.[DefinitionID] = @AssessmentDefinitionId
    AND QD.obsolete = @ObsoleteStatus
    AND QD.meta_status = @PublishStatus
    AND AD.obsolete = @ObsoleteStatus";

        private const string queryGetAssessmentDefinition = @"
                SELECT
                    AM.assessment_meta_id AS [ID],
                    AM.assessment_name AS [Name],
                    AM.assessment_type AS [Type],
                    AQGD.assessment_question_group_definition_id AS [ID],
                    QGD.qg_name AS [Name],
                    AQGD.ordinal AS [Order],
                    QD.question_id as [ID],
                    QGDQD.ordinal AS [Order],
                    0 as [IsConditional],
                    NULL AS [Condition],
                    NULL AS [ParentQuestionID]
                FROM dbo.AssessmentMeta AM 
                JOIN dbo.AssessmentQuestionGroupDefinition AQGD ON AQGD.assessment_meta_id = AM.assessment_meta_id
                JOIN dbo.QuestionGroupDefinition QGD ON QGD.question_group_definition_id = AQGD.question_group_definition_id
                JOIN dbo.QuestionGroupDefinition_QuestionDefinition QGDQD ON QGDQD.question_group_definition_id = AQGD.question_group_definition_id
                JOIN dbo.QuestionDefinition QD ON QD.question_id = QGDQD.question_id
                WHERE AM.meta_status = @PublishStatus
                    AND AM.obsolete = @ObsoleteStatus
                    AND AM.assessment_name = @AssessmentName";
        private const string queryGetQuestions = @"
                SELECT 
                    qd.question_id as [ID],
                    qd.question_text as [QuestionText],
                    qd.tool_tip as [ToolTip],
                    aqd.required as [IsRequired],
                    aqd.pre_populate as [IsPrePopulated],
                    DTC.display_type AS [DisplayedAs], 
                    DAC.data_type AS [DataType],
                    MOG.option_group_name AS [OptionGroupName],               
                    -- ActionableAnswers Objects (null-able)
                    ARD.assessment_response_definition_id AS [ID] ,
                    ARD.score as [Score],
                    CASE 
                        WHEN DAC.data_type = 'BOOLEAN'      THEN CAST(AAM_B.boolean_answer_meta AS VARCHAR)
                        WHEN DAC.data_type = 'BOOLEAN_DATE' THEN CAST(AAM_BD.date_answer_meta   AS VARCHAR)
                        WHEN DAC.data_type = 'DATE'         THEN CAST(AAM_D.date_answer_meta    AS VARCHAR)
                        WHEN DAC.data_type = 'DIAGNOSIS'    THEN CAST(AAM_Di.diagnosis_code_id  AS VARCHAR)
                        WHEN DAC.data_type = 'INTEGER'      THEN CAST(AAM_N.number_answer_meta  AS VARCHAR)
                        WHEN DAC.data_type = 'META_OPTION'  THEN CAST(MO.option_value           AS VARCHAR)
                        WHEN DAC.data_type = 'STRING'       THEN CAST(AAM_S.string_answer_meta  AS VARCHAR)
                        ELSE NULL
                    END AS [Value],
                    -- ConditionalQuestions Objects (null-able)
                    CRD_QD.question_id AS [ID] ,
                    CRD_QD.ordinal AS [Order],
                    CASE 
                        WHEN DAC.data_type = 'BOOLEAN'      THEN CAST(CRD_B.boolean_conditional_response    AS VARCHAR)
                        WHEN DAC.data_type = 'BOOLEAN_DATE' THEN CAST(CRD_BD.date_conditional_response      AS VARCHAR)
                        WHEN DAC.data_type = 'DATE'         THEN CAST(CRD_D.date_conditional_response       AS VARCHAR)
                        WHEN DAC.data_type = 'DIAGNOSIS'    THEN CAST(CRD_Di.diagnosis_code_id              AS VARCHAR)
                        WHEN DAC.data_type = 'INTEGER'      THEN CAST(CRD_N.number_conditional_response     AS VARCHAR)
                        WHEN DAC.data_type = 'META_OPTION'  THEN CAST(CRD_MOV.option_value                  AS VARCHAR)
                        WHEN DAC.data_type = 'STRING'       THEN CAST(CRD_S.string_conditional_response     AS VARCHAR)
                        ELSE NULL
                    END AS [Condition],
                    qd.question_id as [ParentID]
                FROM dbo.QuestionDefinition QD
                -- Build the question definition:
                --   - AQD: How is the question used in this assessment
                JOIN dbo.AssessmentQuestionDefinition AQD ON AQD.question_id = QD.question_id
                --   - What type of answer does the question have
                JOIN dbo.AnswerDefinition AD ON AD.question_id = QD.question_id 
                JOIN dbo.DisplayTypeConfiguration DTC ON DTC.display_type_id = AD.display_type_id
                JOIN dbo.DataTypeConfiguration DAC ON DAC.data_type_id= AD.data_type_id
                LEFT JOIN dbo.MetaOptionGroup MOG ON MOG.meta_option_group_id = AD.meta_option_group_id
                
                -- Questions can belong to different assessments, so we need to filter by what assessment we are looking for
                --   - AQGD relates question groups ('page' or 'groups' of questions ) to assessments, I think it should actually be named AssessmentMeta_QuestionGroupDefinition 
                -- - AQD references back to AQGD, regardless of nested question depth
                JOIN dbo.AssessmentQuestionGroupDefinition AQGD ON AQGD.assessment_question_group_definition_id = AQD.assessment_question_group_definition_id
                -- - A defined assessment, not the answered instances
                JOIN dbo.AssessmentMeta AM ON AM.assessment_meta_id = AQGD.assessment_meta_id
                
                -- Questions can have options for their answers, that is what this table specifies... can be null
                -- - ARD is answer response main object, there can be a records for each possible response
                LEFT JOIN dbo.AssessmentResponseDefinition ARD ON ARD.assessment_question_definition_id = AQD.assessment_question_definition_id
                
                AND AD.answer_definition_id = ARD.answer_definition_id
                -- - AAM_ * are the predefined answers that are specified and used by system, ... can be null
                LEFT JOIN dbo.AssessmentAnswerMetaBase AAMB ON AAMB.assessment_answer_meta_id = ARD.assessment_answer_meta_id
                LEFT JOIN dbo.AssessmentAnswerMetaBoolean AAM_B ON AAM_B.assessment_answer_meta_id = AAMB.assessment_answer_meta_id
                LEFT JOIN dbo.AssessmentAnswerMetaBooleanDate AAM_BD ON AAM_BD.assessment_answer_meta_id = AAMB.assessment_answer_meta_id
                LEFT JOIN dbo.AssessmentAnswerMetaDate AAM_D ON AAM_D.assessment_answer_meta_id = AAMB.assessment_answer_meta_id
                LEFT JOIN dbo.AssessmentAnswerMetaDiagnosis AAM_Di ON AAM_Di.assessment_answer_meta_id = AAMB.assessment_answer_meta_id
                LEFT JOIN dbo.AssessmentAnswerMetaMetaOption AAM_MO ON AAM_MO.assessment_answer_meta_id = AAMB.assessment_answer_meta_id
                LEFT JOIN dbo.AssessmentAnswerMetaNumber AAM_N ON AAM_N.assessment_answer_meta_id = AAMB.assessment_answer_meta_id
                LEFT JOIN dbo.AssessmentAnswerMetaString AAM_S ON AAM_S.assessment_answer_meta_id = AAMB.assessment_answer_meta_id
                
                -- Convert Meta Option into English, if the question is option group
                ---Must come after the AAM_MO because it needs to join to that table
                LEFT JOIN dbo.MetaOption MO ON MO.meta_option_group_id = MOG.meta_option_group_id
                    AND MO.meta_option_id = AAM_MO.meta_option_id
                
                -- Conditional Questions
                ---Conditional questions are connected to a ARD records, therefore different answers => different conditions
                LEFT JOIN dbo.ConditionalResponseDefinitionBase CRDB ON CRDB.conditional_response_definition_id = ARD.conditional_response_definition_id
                    AND CRDB.answer_definition_id = AD.answer_definition_id
                -- - Can be multiple questions for each conditional answer
                LEFT JOIN dbo.ConditionalResponseDefinition_QuestionDefinition CRD_QD ON CRD_QD.conditional_response_definition_id = CRDB.conditional_response_definition_id
                -- - CRD_ * are the values which trigger this condition
                LEFT JOIN dbo.ConditionalResponseDefinitionBoolean CRD_B ON CRD_B.conditional_response_definition_id = CRDB.conditional_response_definition_id
                LEFT JOIN dbo.ConditionalResponseDefinitionBooleanDate CRD_BD ON CRD_BD.conditional_response_definition_id = CRDB.conditional_response_definition_id
                LEFT JOIN dbo.ConditionalResponseDefinitionDate CRD_D ON CRD_D.conditional_response_definition_id = CRDB.conditional_response_definition_id
                LEFT JOIN dbo.ConditionalResponseDefinitionDiagnosis CRD_Di ON CRD_Di.conditional_response_definition_id = CRDB.conditional_response_definition_id
                LEFT JOIN dbo.ConditionalResponseDefinitionMetaOption CRD_MO ON CRD_MO.conditional_response_definition_id = CRDB.conditional_response_definition_id
                LEFT JOIN dbo.ConditionalResponseDefinitionNumber CRD_N ON CRD_N.conditional_response_definition_id = CRDB.conditional_response_definition_id
                LEFT JOIN dbo.ConditionalResponseDefinitionString CRD_S ON CRD_S.conditional_response_definition_id = CRDB.conditional_response_definition_id
                LEFT JOIN dbo.MetaOption CRD_MOV ON CRD_MOV.meta_option_id = CRD_MO.meta_option_id
                
                -- Answers
                LEFT JOIN dbo.AnswerPart AP ON AP.answer_definition_id = AD.answer_definition_id
                LEFT JOIN dbo.Answer A ON A.answer_id = AP.answer_id
                  AND A.assessment_question_definition_id = A.assessment_question_definition_id
                LEFT JOIN dbo.Assessment Ass ON Ass.assessment_id = A.assessment_id
                  AND Ass.assessment_meta_id = AM.assessment_meta_id
                
                WHERE
                  QD.question_id in @QuestionIDs
                  AND AM.assessment_name = @AssessmentName
                  AND QD.obsolete = @ObsoleteStatus
                  AND QD.meta_status = @PublishStatus
                  AND Am.meta_status = @PublishStatus
                  AND AD.obsolete = @ObsoleteStatus
                  AND DTC.obsolete = @ObsoleteStatus
                  AND DAC.obsolete = @ObsoleteStatus
                  AND AQD.obsolete = @ObsoleteStatus
                ";

        private readonly string queryAssessmentsByMonth = $@"
WITH _Assessments AS ({_baseAssessmentQuery})
SELECT * FROM _Assessments
WHERE [CompletedDate] BETWEEN @StartDate AND (DATEADD(MONTH, @Months, @StartDate))
    AND [DefinitionID] = @AssessmentDefinitionId
";
        private readonly string queryAssessmentsMemberNumber = $@"
WITH _Assessments AS ({_baseAssessmentQuery}),
     _AssessmentAnswers AS ({_baseAnswerQuery})
SELECT TOP(@Count)
    AssessmentID as [ID],
    [MemberID],
    [MemberNumber],
    [CompletedDate],
    [Score]
FROM _AssessmentAnswers
WHERE convert(datetime, [Value], 121) between @StartDate and @EndDate
    AND [QuestionID] = @DateQuestionId
    AND [MemberNumber] = @MemberNumber
";
        private readonly string queryAssessmentsMemberId = $@"
WITH _Assessments AS ({_baseAssessmentQuery}),
     _AssessmentAnswers AS ({_baseAnswerQuery})
SELECT TOP(@Count)
    AssessmentID as [ID],
    [MemberID],
    [MemberNumber],
    [CompletedDate],
    [Score]
FROM _AssessmentAnswers
WHERE convert(datetime, [Value], 121) between @StartDate and @EndDate
    AND [QuestionID] = @DateQuestionId
    AND [MemberID] = @MemberId
";


        private readonly string queryAnswersByAssessmentId = $@"
WITH _Assessments AS ({_baseAssessmentQuery}),
     _AssessmentAnswers AS ({_baseAnswerQuery})
SELECT * FROM _AssessmentAnswers
WHERE [QuestionID] IN @QuestionIDs
    AND [AssessmentID] IN @AssessmentIds
";

        #endregion

        public string PublishStatus { get; set; } = "PUBLISHED";
        public int ObsoleteStatus { get; set; } = 0;

        internal AssessmentRepository(CaseManagementDatabase database) : base(database)
        {

        }

        /// <summary>
        /// Loads an assessment definition from TruCare
        /// </summary>
        /// <param name="assessmentName">Assessment Name to Load</param>
        /// <returns></returns>
        public AssessmentDefintion LoadAssessmentDefinition(string assessmentName)
        {
            AssessmentDefintion assessmentDefiniton = null;
            object paramsGetAssessmentDefinition = new { AssessmentName = assessmentName, ObsoleteStatus, PublishStatus };

            // Using Dapper, each record is split into assessment definition, Group, and questions. We have to parse the 3 object and combine them
            Database.Query<AssessmentDefintion, AssessmentGroup, AssessmentQuestion, AssessmentDefintion>(queryGetAssessmentDefinition, (definition, group, question) =>
            {
                // Set assessment if it is null, it shouldn't change
                if (assessmentDefiniton == null) { assessmentDefiniton = definition; }
                assessmentDefiniton.AddUpdateGroupQuestion(group, question);
                return assessmentDefiniton;
            },
                param: paramsGetAssessmentDefinition,
                splitOn: "ID"
            );

            if (assessmentDefiniton != null)
                _populateQuestions(assessmentDefiniton.Name, assessmentDefiniton.Questions);

            return assessmentDefiniton;
        }

        /// <summary>
        /// Fills in the information for a question, does it recursively to load the whole question tree
        /// </summary>
        /// <param name="assessmentName">Assessment Name to Load</param>
        /// <param name="questions">list of question object to modify</param>
        private void _populateQuestions(string assessmentName, IEnumerable<AssessmentQuestion> questions)
        {
            List<AssessmentQuestion> children = new List<AssessmentQuestion>();
            var questionData = GetQuestions(assessmentName, questions.Select(q => q.ID)).ToDictionary(q => q.ID, q => q);
            foreach (var question in questions)
            {
                if (questionData.TryGetValue(question.ID, out AssessmentQuestion newData))
                {
                    question.UpdateData(newData);
                    children.AddRange(question.ChildConditionalQuestions);
                }
            }
            if (children.Any())
            {
                _populateQuestions(assessmentName, children);
            }
        }


        /// <summary>
        /// Loads question records from the TruCare DB 
        /// </summary>
        /// <param name="assessmentName">Assessment Name to Load</param>
        /// <param name="questionIds">zList of IDd of the question to load</param>
        /// <returns></returns>
        public IEnumerable<AssessmentQuestion> GetQuestions(string assessmentName, IEnumerable<string> questionIds)
        {
            // Set below on the first iteration
            Dictionary<string, AssessmentQuestion> questions = new Dictionary<string, AssessmentQuestion>();
            object paramsGetQuestions = new
            {
                AssessmentName = assessmentName,
                QuestionIDs = questionIds,
                ObsoleteStatus,
                PublishStatus
            };

            // Using Dapper, each record is split into assessment definition, Group, and questions. We have to parse the 3 object and combine them
            Database.Query<AssessmentQuestion, AssessmentActionableAnswer, AssessmentQuestion, AssessmentQuestion>(queryGetQuestions, (rawQuestion, posAns, condQ) =>
            {
                if (!questions.ContainsKey(rawQuestion.ID))
                {
                    questions.Add(rawQuestion.ID, rawQuestion);
                }
                questions[rawQuestion.ID].AddUpdateActionableAnswer(posAns, condQ);
                return rawQuestion;
            },
                param: paramsGetQuestions,
                splitOn: "ID"
            );
            return questions.Values;
        }

        /// <summary>
        /// Loads all of the answered assessment for the given definition in the month(s) range
        /// </summary>
        /// <param name="definition">Assessment Definition object to load from </param>
        /// <param name="monthsToLoad">How many months should the query look back on</param>
        /// <returns></returns>
        public IEnumerable<Assessment> LoadAssessmentByMonth(AssessmentDefintion definition, int monthsToLoad, DateTime startDate)
        {
            if (definition == null)
                definition = new AssessmentDefintion();

            object paramsGetAssessments = new
            {
                AssessmentDefinitionId = definition.ID,
                Months = monthsToLoad,
                StartDate = startDate,
                ObsoleteStatus,
                PublishStatus,
            };

            Dictionary<string, Assessment> assessments = Database.Query<Assessment>(queryAssessmentsByMonth, paramsGetAssessments)
                .ToDictionary(q => q.ID, q => q.LinkDefintion(definition));
            if (assessments.Any())
            {
                _loadAssessmentAnswers(definition, assessments);
            }
            return assessments.Values;
        }
        /// <summary>
        /// Loads a specific assessment for the given definition and member
        /// </summary>
        /// <param name="definition">The assessment definition object to load</param>
        /// <param name="memberNumber">The Member number of the member to load</param>
        /// <param name="dateQuestionId">The GUID of the question the has the date to filter on</param>
        /// <param name="startDate">The start date to look for assessment from</param>
        /// <param name="endDate">The end date to look for assessment to</param>
        /// <param name="endDate">The number of assessments to load</param>
        /// <returns></returns>
        public IEnumerable<Assessment> LoadAssessmentByMemberNumber(AssessmentDefintion definition, string memberNumber, string dateQuestionId, DateTime startDate, DateTime endDate, int count = 1)
        {
            if (definition == null)
                definition = new AssessmentDefintion();

            object paramsGetAssessments = new
            {
                AssessmentDefinitionId = definition.ID,
                MemberNumber = memberNumber,
                DateQuestionId = dateQuestionId,
                StartDate = startDate,
                EndDate = endDate,
                Count = count,
                ObsoleteStatus,
                PublishStatus,
            };

            Dictionary<string, Assessment> assessments = Database.Query<Assessment>(queryAssessmentsMemberNumber, paramsGetAssessments)
                .ToDictionary(q => q.ID, q => q.LinkDefintion(definition));

            if (assessments.Any())
            {
                _loadAssessmentAnswers(definition, assessments);
            }
            return assessments.Values;
        }
        /// <summary>
        /// Loads a specific assessment for the given definition and member
        /// </summary>
        /// <param name="definition">The assessment definition object to load</param>
        /// <param name="memberId">The TrueCare Member Id of the member to load</param>
        /// <param name="dateQuestionId">The GUID of the question the has the date to filter on</param>
        /// <param name="startDate">The start date to look for assessment from</param>
        /// <param name="endDate">The end date to look for assessment to</param>
        /// <param name="endDate">The number of assessments to load</param>
        /// <returns></returns>
        public IEnumerable<Assessment> LoadAssessmentByMemberId(AssessmentDefintion definition, string memberId, string dateQuestionId, DateTime startDate, DateTime endDate, int count = 1)
        {
            if (definition == null)
                definition = new AssessmentDefintion();

            object paramsGetAssessments = new
            {
                AssessmentDefinitionId = definition.ID,
                MemberId = memberId,
                DateQuestionId = dateQuestionId,
                StartDate = startDate,
                EndDate = endDate,
                Count = count,
                ObsoleteStatus,
                PublishStatus,
            };

            Dictionary<string, Assessment> assessments = Database.Query<Assessment>(queryAssessmentsMemberId, paramsGetAssessments)
                .ToDictionary(q => q.ID, q => q.LinkDefintion(definition));

            if (assessments.Any())
            {
                _loadAssessmentAnswers(definition, assessments);
            }
            return assessments.Values;
        }

        private void _loadAssessmentAnswers(AssessmentDefintion definition, Dictionary<string, Assessment> assessments)
        {
            // Get a dictionary of the valid questions
            Dictionary<string, AssessmentQuestion> questions = definition.Questions
                .Flatten(w => w.ChildConditionalQuestions)
                .Where(q => q.DisplayedAs != "LABEL")
                .ToDictionary(q => q.ID, q => q);
            object paramsGetAnswers = new
            {
                AssessmentDefinitionId = definition.ID,
                AssessmentIds = assessments.Values.Select(q => q.ID),
                QuestionIDs = questions.Keys,
                ObsoleteStatus,
                PublishStatus,
            };
            List<AssessmentAnswer> answers = Database.Query<AssessmentAnswer>(queryAnswersByAssessmentId, paramsGetAnswers).ToList();
            answers.ForEach(ans =>
            {
                questions[ans.QuestionID].AddAnswer(ans, linkAnswer: true);
                assessments[ans.AssessmentID]?.AddAnswer(ans);
            });
        }

    }
}

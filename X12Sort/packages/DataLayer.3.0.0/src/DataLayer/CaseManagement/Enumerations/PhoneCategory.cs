using System.Xml.Serialization;

namespace DataLayer.CaseManagement.Enumerations
{
    public enum PhoneCategory : int
    {
        [XmlEnum(Name = "")]
        Unknown = 0,

        [XmlEnum(Name = "Phone")]
        Phone = 1,

        [XmlEnum(Name = "Fax")]
        Fax = 2
    }
}

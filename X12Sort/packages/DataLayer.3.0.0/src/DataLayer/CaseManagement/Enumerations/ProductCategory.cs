using System.Xml.Serialization;

namespace DataLayer.CaseManagement.Enumerations
{
    public enum ProductCategory : int
    {
        [XmlEnum(Name = "")]
        Unknown = 0,

        [XmlEnum(Name = "Medicare")]
        Medicare = 1,

        [XmlEnum(Name = "Medicaid")]
        Medicaid = 2,

        [XmlEnum(Name = "Commercial")]
        Commercial = 3
    }
}

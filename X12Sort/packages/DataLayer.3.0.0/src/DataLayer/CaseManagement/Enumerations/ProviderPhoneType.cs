using System.Xml.Serialization;

namespace DataLayer.CaseManagement.Enumerations
{
    public enum ProviderPhoneType : int
    {
        [XmlEnum(Name = "")]
        Unknown = 0,

        #region Phone Types
        [XmlEnum(Name = "Office")]
        OfficePhone = 101,

        [XmlEnum(Name = "Emergency")]
        EmergencyPhone = 102,

        [XmlEnum(Name = "Direct")]
        DirectPhone = 103,

        [XmlEnum(Name = "Cell")]
        CellPhone = 104,

        [XmlEnum(Name = "Home")]
        HomePhone = 105,

        [XmlEnum(Name = "Billing")]
        BillingPhone = 106,
        #endregion

        #region Fax Types
        [XmlEnum(Name = "Office Primary")]
        OfficePrimaryFax = 201,

        [XmlEnum(Name = "Office Secondary")]
        OfficeSecondaryFax = 202,

        [XmlEnum(Name = "Direct")]
        DirectFax = 203,

        [XmlEnum(Name = "Billing")]
        BillingFax = 204,

        [XmlEnum(Name = "Office")]
        OfficeFax = 205
        #endregion
    }
}

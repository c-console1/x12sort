using System.Xml.Serialization;

namespace DataLayer.CaseManagement.Enumerations
{
    public enum ProviderAddressType : int
    {
        [XmlEnum("")]
        Unknown = 0,

        [XmlEnum("BI")]
        Billing = 1,

        [XmlEnum("SE")]
        Servicing = 2,

        [XmlEnum("MA")]
        Mailing = 3
    }
}

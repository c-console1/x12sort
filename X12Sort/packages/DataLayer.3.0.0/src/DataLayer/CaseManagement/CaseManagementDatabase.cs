using DataLayer.Base;

namespace DataLayer.CaseManagement
{
    /// <summary>
    /// Logical Database for the Case Management System
    /// </summary>
    public class CaseManagementDatabase : BaseDatabase
    {
        public AssessmentRepository Assessments { get; private set; }
        public AuthorizationRepository Authorizations { get; private set; }
        public MemberRepository Members { get; private set; }
        public ProviderRepository Providers { get; private set; }

        internal CaseManagementDatabase(IDbExec dbExec) : base(dbExec)
        {
            Assessments = new AssessmentRepository(this);
            Authorizations = new AuthorizationRepository(this);
            Members = new MemberRepository(this);
            Providers = new ProviderRepository(this);
        }

        internal CaseManagementDatabase(params IExecutor[] executors) : base(executors)
        {
            Assessments = new AssessmentRepository(this);
            Authorizations = new AuthorizationRepository(this);
            Members = new MemberRepository(this);
            Providers = new ProviderRepository(this);
        }
    }

}

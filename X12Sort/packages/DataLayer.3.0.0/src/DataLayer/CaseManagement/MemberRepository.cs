using DataLayer.Base;
using DataLayer.CaseManagement.Models;
using System.Collections.Generic;

namespace DataLayer.CaseManagement
{
	public class MemberRepository : BaseRepository<CaseManagementDatabase>
	{
		internal MemberRepository(CaseManagementDatabase database) : base(database)
		{

		}

        /// <summary>
        /// Get a list of member IDs for all members loaded into the database
        /// </summary>
        /// <returns>Collection of member IDs</returns>
		public IEnumerable<string> GetLoadedMemberIDsUnfiltered()
        {
            string sql = @"-- Pull external member ids from casenet
				select substring(ltrim(external_member_id), 1, 12)	
				from casenet.dbo.Member 
				order by external_member_id";

            return Database.Query<string>(sql);
        }

    }
}

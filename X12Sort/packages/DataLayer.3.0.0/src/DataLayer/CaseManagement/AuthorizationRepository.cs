using DataLayer.Base;
using DataLayer.Base.Models;
using DataLayer.CaseManagement.Models;
using System.Collections.Generic;

namespace DataLayer.CaseManagement
{
    public class AuthorizationRepository : BaseRepository<CaseManagementDatabase>
    {
        internal AuthorizationRepository(CaseManagementDatabase database) : base(database)
        {

        }

        /// <summary>
        /// Will return an inpatient auth with the most recent line item information
        /// </summary>
        /// <param name="authorizationNbr"></param>
        /// <returns></returns>
        public InpatientAuthorization GetIPAuthorization(string authorizationNbr)
        {
            InpatientAuthorization result = (InpatientAuthorization)GetAuthorization(authorizationNbr);

            return result;
        }

        /// <summary>
        /// Will return an outpatient auth with the most recent line item information
        /// </summary>
        /// <param name="authorizationNbr"></param>
        /// <returns></returns>
        public OutpatientAuthorization GetOPAuthorization(string authorizationNbr)
        {
            OutpatientAuthorization result = (OutpatientAuthorization)GetAuthorization(authorizationNbr);

            return result;
        }

        /// <summary>
        /// Will return a list of inpatient authorizations for the member
        /// </summary>
        /// <param name="memberNbr"></param>
        /// <returns></returns>
        public IEnumerable<InpatientAuthorization> GetIPAuthsByMember(string memberNbr)
        {
            IEnumerable<InpatientAuthorization> result = (List<InpatientAuthorization>)GetAuthorizationsByMember(memberNbr);

            return result;
        }

        /// <summary>
        /// Will return a list of outpatient authorizations for the member
        /// </summary>
        /// <param name="memberNbr"></param>
        /// <returns></returns>
        public IEnumerable<OutpatientAuthorization> GetOPAuthsByMember(string memberNbr)
        {
            IEnumerable<OutpatientAuthorization> result = (List<OutpatientAuthorization>)GetAuthorizationsByMember(memberNbr);

            return result;
        }

        /// <summary>
        /// Internal method to execute sql that returns generic auth object
        /// </summary>
        /// <param name="memberNbr"></param>
        /// <returns></returns>
        public IEnumerable<IAuthorization> GetAuthorizationsByMember(string memberNbr)
        {
            //TODO: Replace this place holder sql when mapping begins
            string sql = @"select *
from dbo.UMAuthorization a
join dbo.Member m on a.member_id= m.member_id and m.obsolete = 0
--most recent line item simply for the to_date
join (select authorization_id,
		line_item_id,
		from_date,
		to_date,
		determination_id,
		rank () over (partition by authorization_id order by to_date desc) as R
		from dbo.UMLineItem 
		where obsolete = 0		
) l on a.authorization_id = l.authorization_id and l.R = 1
join dbo.UMDetermination det on det.determination_id = l.determination_id and det.obsolete = 0 and det.status <> 'VOID'
join dbo.UMDecision dsc on dsc.determination_id = det.determination_id and dsc.obsolete = 0
where SUBSTRING(m.external_member_id,1,1) = 'A'
and m.external_member_id = @memberNbr
";

            IEnumerable<IAuthorization> result = Database.Query<IAuthorization>(sql, new { external_member_id = memberNbr });

            return result;
        }

        /// <summary>
        /// Internal method to execute sql that returns generic auth object
        /// </summary>
        /// <param name="authorizationNbr"></param>
        /// <returns></returns>
        public IAuthorization GetAuthorization(string authorizationNbr)
        {
            //TODO: Replace this place holder sql when mapping begins
            string sql = @"select *
from dbo.UMAuthorization a
join dbo.Member m on a.member_id= m.member_id and m.obsolete = 0
--most recent line item simply for the to_date
join (select authorization_id,
		line_item_id,
		from_date,
		to_date,
		determination_id,
		rank () over (partition by authorization_id order by to_date desc) as R
		from dbo.UMLineItem 
		where obsolete = 0		
) l on a.authorization_id = l.authorization_id and l.R = 1
join dbo.UMDetermination det on det.determination_id = l.determination_id and det.obsolete = 0 and det.status <> 'VOID'
join dbo.UMDecision dsc on dsc.determination_id = det.determination_id and dsc.obsolete = 0
where SUBSTRING(m.external_member_id,1,1) = 'A'
and a.authorization_num = @authorizationNbr
";
            IAuthorization result = Database.QueryFirstOrDefault<IAuthorization>(sql, new { authorization_num = authorizationNbr });

            return result;
        }

        //TODO: add methods for getting service lines
    }
}

using System;
using System.Xml.Serialization;
using DataLayer.Base.Models;
using DataLayer.CaseManagement.Enumerations;

namespace DataLayer.CaseManagement.Models
{
    [XmlRoot(ElementName = "address")]
    public class ProviderAddress : BaseCaseManagementStartEndXmlObject, IAddress
    {
        [XmlElement(ElementName = "external_address_id")]
        public string ExternalID { get; set; }

        [XmlElement(ElementName = "address_line_1")]
        public string Address1 { get; set; }

        [XmlElement(ElementName = "address_line_2")]
        public string Address2 { get; set; }

        [XmlElement(ElementName = "address_line_3")]
        public string Address3 { get; set; }

        [XmlElement(ElementName = "city")]
        public string City { get; set; }

        [XmlElement(ElementName = "county")]
        public string County { get; set; }

        [XmlElement(ElementName = "state")]
        public USState State { get; set; }

        [XmlElement(ElementName = "zip_code")]
        public ZipCode ZipCode { get; set; }

        [XmlElement(ElementName = "country")]
        public Country Country { get; set; }

        [XmlElement(ElementName = "address_type")]
        public ProviderAddressType Type { get; set; }

        [XmlElement(ElementName = "preferred_address")]
        public bool IsPreferred { get; set; }

        [XmlIgnore]
        public string Name { get; set; }
    }
}

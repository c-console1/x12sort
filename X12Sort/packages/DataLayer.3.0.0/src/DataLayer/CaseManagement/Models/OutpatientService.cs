using DataLayer.Base.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DataLayer.CaseManagement.Models
{
    public class OutpatientService : BaseCaseManagementObject, IAuthorizedService
    {
        #region Members
        private List<Procedure> _procs;
        #endregion
        #region Metadata
        /// <summary>
        /// Auth version
        /// </summary>
        public string AuthorizationVersion { get; set; }
        /// <summary>
        /// Date the service was authorized
        /// </summary>
        public DateTime? AuthorizedDate { get; set; }
        /// <summary>
        /// User who authorized the service
        /// </summary>
        public string AuthorizedBy { get; set; }
        /// <summary>
        /// Type of authorization
        /// </summary>
        public string AuthorizationServiceType { get; set; }
        /// <summary>
        /// Start date of the service
        /// </summary>
        public DateTime? StartDate { get; set; }
        /// <summary>
        /// End date of the service
        /// </summary>
        public DateTime? EndDate { get; set; }
        /// <summary>
        /// Urgency level of the service
        /// </summary>
        public string Priority { get; set; }
        /// <summary>
        /// Status of the service. Approved, denied, etc.
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// Type of the service authorized
        /// </summary>
        public string LineItemType { get; set; }
        /// <summary>
        /// Count of approved items (units, visits, etc)
        /// </summary>
        public int ApprovedCount { get; set; }
        /// <summary>
        /// Amount approved
        /// </summary>
        public int ApprovedAmount { get; set; }
        /// <summary>
        /// Type of unit approved (units, visits, days, etc)
        /// </summary>
        public string UnitType { get; set; }
        /// <summary>
        /// Service notes
        /// </summary>
        public List<dynamic> Notes { get; set; } //TODO: Use note object in existing datalayer?
        /// <summary>
        /// List of UDFs, can be anything
        /// </summary>
        public List<dynamic> UserDefined { get; set; }
        #endregion
        #region Proc codes
        /// <summary>
        /// List of procedure codes
        /// </summary>
        public List<Procedure> ProcedureCodes
        {
            get => _procs;
            set => _procs = value;
        }
        List<IAuthorizationProcedure> IAuthorizedService.ProcedureCodes
        {
            get => _procs.Select(proc => (IAuthorizationProcedure)proc).ToList();
            set => _procs = value.Cast<Procedure>().ToList();
        }
        /// <summary>
        /// Provider performing the service
        /// </summary>
        public ServicingProvider ServicingProvider { get; set; }
        #endregion
    }
}

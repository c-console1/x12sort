using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace DataLayer.CaseManagement.Models
{
    [XmlRoot(ElementName = "provider_location_to_network")]
    public class ProviderLocationNetworkReference : BaseCaseManagementStartEndXmlObject
    {
        [XmlElement(ElementName = "external_network_location_id")]
        public string ExternalID { get; set; }

        [XmlElement(ElementName = "external_network_id")]
        public string ExternalNetworkID { get; set; }

        [XmlElement(ElementName = "medicaid_par")]
        public bool IsMedicaidParticipating { get; set; }

        [XmlElement(ElementName = "medicare_par")]
        public bool IsMedicareParticipating { get; set; }

        [XmlElement(ElementName = "tricare_par")]
        public static bool IsTricareParticipating { get { return false; } }

        [XmlElement]
        public IEnumerable<UserDefinedField> UDFs { get; set; }

        [XmlIgnore]
        public bool ContainsUDFs
        {
            get
            {
                if (UDFs != null)
                    if (UDFs.Any())
                        return true;
                return false;
            }
        }
    }
}

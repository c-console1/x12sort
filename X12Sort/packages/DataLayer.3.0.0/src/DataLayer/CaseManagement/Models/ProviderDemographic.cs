using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using DataLayer.Base.Models;
using DataLayer.CaseManagement.Enumerations;

namespace DataLayer.CaseManagement.Models
{
    [XmlRoot(ElementName = "demographic")]
    public class ProviderDemographic : BaseCaseManagementStartEndXmlObject, IProvider
    {
        [XmlElement(ElementName = "provides_medical")]
        public static bool ProvidesMedical { get { return true; } }

        [XmlElement(ElementName = "provides_non_medical")]
        public static bool ProvidesNonMedical { get { return true; } }

        [XmlElement(ElementName = "provider_number")]
        public string ProviderNumber { get; set; }

        [XmlElement(ElementName = "provider_class")]
        public ProviderClass Class { get; set; }

        [XmlElement(ElementName = "first_name")]
        public string FirstName { get; set; }

        [XmlElement(ElementName = "last_name")]
        public string LastName { get; set; }

        [XmlElement(ElementName = "facility_name")]
        public string InstitutionName { get; set; }

        [XmlElement(ElementName = "tin")]
        public string TaxID { get; set; }

        [XmlElement(ElementName = "npi")]
        public string NPI { get; set; }

        [XmlElement(ElementName = "medicare_number")]
        public string MedicareNumber { get; set; }

        [XmlElement(ElementName = "medicaid_number")]
        public string MedicaidNumber { get; set; }

        [XmlElement(ElementName = "external_provider_group_id")]
        public string ExternalGroupID { get; set; }

        [XmlElement(ElementName = "provider_type_id")]
        public string TypeID { get; set; }

        [XmlIgnore]
        public bool IsIndividual { get { return Class == ProviderClass.Individual; } }

        [XmlIgnore]
        public bool IsInstitution { get { return Class == ProviderClass.Facility; } }

        [XmlElement]
        public IEnumerable<UserDefinedField> UDFs { get; set; }

        [XmlIgnore]
        public bool ContainsUDFs
        {
            get
            {
                if (UDFs != null)
                    if (UDFs.Any())
                        return true;
                return false;
            }
        }

        [XmlIgnore]
        public string DEANumber { get; set; }
    }
}

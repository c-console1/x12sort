using DataLayer.Base.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DataLayer.CaseManagement.Models
{
    /// <summary>
    /// Outpatient utilization management authorization object
    /// </summary>
    public class OutpatientAuthorization : BaseCaseManagementObject, IAuthorization, IStartEnd
    {
        #region Members
        private List<OutpatientService> _authorizedServices;
        #endregion

        #region Metadata
        /// <summary>
        /// Version of the authorization
        /// </summary>
        public int AuthVersion { get; set; }

        /// <summary>
        /// Authorization number
        /// </summary>
        public string AuthorizationNumber { get; set; }

        /// <summary>
        /// Member number
        /// </summary>
        public string MemberNumber { get; set; }

        /// <summary>
        /// Overall status of the authorization (approved, partial approved, denied, etc)
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Party from which the authorization was referred (PCP, self, specialist, etc)
        /// </summary>
        public string ReferralSource { get; set; }

        /// <summary>
        /// Date the authorization was requested
        /// </summary>
        public DateTime? RequestedDate { get; set; }

        /// <summary>
        /// Date the authorization was received
        /// </summary>
        public DateTime? ReceivedDate { get; set; }

        /// <summary>
        /// Input source of the authorization
        /// </summary>
        public string InputSource { get; set; }

        /// <summary>
        /// Overall start date of the authorization
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Overall end date of the authorization
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// User defined elements to an authorization. Can be anything.
        /// </summary>
        public List<dynamic> UserDefined { get; set; }
        #endregion

        #region Diagnosis
        /// <summary>
        /// Diagnosis information for the authorization
        /// </summary>
        public AuthorizationDiagnosis AuthorizationDiagnosis { get; set; }
        #endregion

        #region Services
        /// <summary>
        /// Collection of the service or services for the authorization
        /// </summary>
        public List<OutpatientService> AuthorizedServices
        {
            get => _authorizedServices;
            set => _authorizedServices = value;
        }

        List<IAuthorizedService> IAuthorization.AuthorizedServices
        {
            get => _authorizedServices.Select(serv => (IAuthorizedService)serv).ToList();
            set => _authorizedServices = value.Cast<OutpatientService>().ToList();
        }
        #endregion

        #region Providers
        /// <summary>
        /// Provider information for the provider whom requested the services
        /// </summary>
        public RequestingProvider RequestingProvider { get; set; }
        #endregion
    }
}

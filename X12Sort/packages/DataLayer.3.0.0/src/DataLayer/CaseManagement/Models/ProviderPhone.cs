using System;
using System.Linq;
using System.Xml.Serialization;
using DataLayer.Base.Models;
using DataLayer.CaseManagement.Enumerations;

namespace DataLayer.CaseManagement.Models
{
    [XmlRoot(ElementName = "phone")]
    public class ProviderPhone : BaseCaseManagementStartEndXmlObject
    {
        private PhoneNumber _phone;

        [XmlElement(ElementName = "external_phone_number_id")]
        public string ExternalID { get; set; }

        [XmlElement(ElementName = "phone_number_type")]
        public ProviderPhoneType Type { get; set; }

        [XmlElement(ElementName = "phone_fax_type")]
        public PhoneCategory Category { get; set; }

        [XmlElement(ElementName = "phone_number")]
        public string Number
        {
            get { return _phone.Digits; }
            set { _phone = CombineParts(value, _phone.Extension); }
        }

        [XmlElement(ElementName = "phone_extension")]
        public string Extension
        {
            get { return _phone.Extension; }
            set { _phone = CombineParts(_phone.Digits, value); }
        }

        [XmlElement(ElementName = "preferred_phone")]
        public bool IsPreferred { get; set; }

        [XmlIgnore]
        public bool IsFax { get { return Category == PhoneCategory.Fax; } }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="phone"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        public static string CombineParts(string phone, string extension)
        {
            int dummy;

            if (phone == null)
                throw new FormatException("Phone number string is null");

            // Validate and sanitize phone number
            string newPhone = phone.Trim();
            if (newPhone.Length != 10)
            {
                if (newPhone.Count(c => c == '-') != 2 || newPhone.Length != 12)
                    throw new FormatException($"Phone number string is improperly formatted (should be 1234567890 or 123-456-7890): {phone}");
            }
            newPhone = newPhone.Replace("-", "");
            if (!int.TryParse(newPhone, out dummy))
                throw new FormatException($"Phone number string is improperly formatted (should be 1234567890 or 123-456-7890): {phone}");

            // Validate and sanitize phone extension
            if (extension == null)
                extension = "";

            string newExtension = extension.Trim().ToLower().Replace("x", "");
            if (!int.TryParse(newExtension, out dummy))
                throw new FormatException($"Phone number extension string is improperly formatted (should be 1234 or x1234): {extension}");

            return $"{newPhone.Substring(0, 3)}-{newPhone.Substring(3, 3)}-{newPhone.Substring(6, 4)} x{newExtension}";
        }
    }
}

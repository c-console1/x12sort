using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using DataLayer.Base.Models;

namespace DataLayer.CaseManagement.Models
{
    [XmlRoot(ElementName = "provider_boardcertification")]
    public class ProviderBoardCertification : BaseCaseManagementStartEndXmlObject
    {
        [XmlElement(ElementName = "external_provider_boardcertification_id")]
        public string ExternalID { get; set; }

        [XmlElement(ElementName = "board_certification")]
        public string Name { get; set; }

        [XmlElement(ElementName = "effective_date")]
        public override string XmlStartDate { get { return base.XmlStartDate; } set { base.XmlStartDate = value; } }

        [XmlElement(ElementName = "termination_date")]
        public override string XmlEndDate { get { return base.XmlEndDate; } set { base.XmlEndDate = value; } }
    }
}

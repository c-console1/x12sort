using DataLayer.Base.Models;

namespace DataLayer.CaseManagement.Models
{
    //TODO: replace temp types with clases
    public class ServicingProvider : BaseCaseManagementObject, IAuthorizationProvider
    {
        public string ID { get; set; }
        public string ExternalNumber { get; set; }
        public string Name { get; set; }
        public string NPI { get; set; }
        public string TaxID { get; set; }
        /// <summary>
        /// Name of contact at provider location
        /// </summary>
        public string ContactName { get; set; }
        public dynamic Phone { get; set; } //TODO: utilize datalayer phone object
        public dynamic ContactPhone { get; set; } //TODO: utilize datalayer phone object
    }
}

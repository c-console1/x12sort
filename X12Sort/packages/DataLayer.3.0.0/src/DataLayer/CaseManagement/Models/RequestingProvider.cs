using DataLayer.Base.Models;

namespace DataLayer.CaseManagement.Models
{
    public class RequestingProvider : BaseCaseManagementObject, IAuthorizationProvider
    {
        public string ID { get; set; }
        public string ExternalNumber { get; set; }
        public string Name { get; set; }
        public string NPI { get; set; }
        public string TaxID { get; set; }
        /// <summary>
        /// Name of contact at provider location
        /// </summary>
        public string ContactName { get; set; }
        /// <summary>
        /// Provider phone
        /// </summary>
        public dynamic Phone { get; set; } //TODO: utilize datalayer phone object
        /// <summary>
        /// Provider contact phone
        /// </summary>
        public dynamic ContactPhone { get; set; } //TODO: utilize datalayer phone object
    }
}

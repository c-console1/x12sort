using System;
using System.Xml.Serialization;
using DataLayer.Base.Models;
using DataLayer.CaseManagement.Enumerations;

namespace DataLayer.CaseManagement.Models
{
    [XmlRoot(ElementName = "email")]
    public class ProviderEmail : BaseCaseManagementStartEndXmlObject
    {
        [XmlElement(ElementName = "external_email_id")]
        public string ExternalID { get; set; }

        [XmlElement(ElementName = "email_type")]
        public ProviderEmailType Type { get; set; }

        [XmlElement(ElementName = "email_address")]
        public string EmailAddress { get; set; }

        [XmlElement(ElementName = "preferred_email")]
        public bool IsPreferred { get; set; }
    }
}

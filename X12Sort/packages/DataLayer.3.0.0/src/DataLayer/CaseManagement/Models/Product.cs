using System.Collections.Generic;
using System.Xml.Serialization;
using DataLayer.CaseManagement.Enumerations;

namespace DataLayer.CaseManagement.Models
{
    [XmlRoot(ElementName = "product")]
    public class Product : BaseCaseManagementObject
    {
        [XmlElement(ElementName = "external_product_id")]
        public string ExternalID { get; set; }

        [XmlElement(ElementName = "product_name")]
        public string Name { get; set; }

        [XmlElement(ElementName = "product_category_id")]
        public ProductCategory Category { get; set; }

        [XmlElement(ElementName = "product_contract_year")]
        public string ContractYear { get; set; }

        [XmlArray(ElementName = "product_network_sets")]
        public IEnumerable<ProductNetworkSetReference> NetworkSets { get; set; }
}
}

using System;
using System.Xml.Serialization;
using DataLayer.Base.Models;

namespace DataLayer.CaseManagement.Models
{
    public class BaseCaseManagementStartEndXmlObject : BaseCaseManagementObject, IStartEnd
    {
        [XmlIgnore]
        public virtual DateTime? StartDate { get; set; }

        [XmlIgnore]
        public virtual DateTime? EndDate { get; set; }

        [XmlElement(ElementName = "start_date")]
        public virtual string XmlStartDate
        {
            get { return StartDate == null ? null : ((DateTime)StartDate).ToString("yyyy-MM-dd"); }
            set { StartDate = value == null ? (DateTime?)null : DateTime.Parse(value); }
        }

        [XmlElement(ElementName = "end_date")]
        public virtual string XmlEndDate
        {
            get { return EndDate == null ? null : ((DateTime)EndDate).ToString("yyyy-MM-dd"); }
            set { EndDate = value == null ? (DateTime?)null : DateTime.Parse(value); }
        }
    }
}

using System.Xml.Serialization;
using DataLayer.Base.Enumerations;
using DataLayer.Base.Models;

namespace DataLayer.CaseManagement.Models
{
    /// <summary>
    /// Base object for all Case Management Objects
    /// </summary>
    public abstract class BaseCaseManagementObject : BaseDataObject
    {
        [XmlIgnore]
        public override string DatabaseKey { get => base.DatabaseKey; set => base.DatabaseKey = value; }

        [XmlIgnore]
        public override DataSource Source { get => DataSource.TruCare; }

        [XmlElement(ElementName = "obsolete")]
        public bool Obsolete { get; set; }

        /// <summary>
        /// Standard Constructor, Should be called by all inheriting objects
        /// </summary>
        protected BaseCaseManagementObject() : base()
        {
        }
    }
}

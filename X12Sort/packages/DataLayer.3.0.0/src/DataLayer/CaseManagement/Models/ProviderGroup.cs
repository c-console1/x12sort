using System.Xml.Serialization;

namespace DataLayer.CaseManagement.Models
{
    public class ProviderGroup : BaseCaseManagementObject
    {
        [XmlElement(ElementName = "external_provider_group_id")]
        public string ExternalID { get; set; }

        [XmlElement(ElementName = "provides_medical")]
        public static bool ProvidesMedical { get { return true; } }

        [XmlElement(ElementName = "provides_non_medical")]
        public static bool ProvidesNonMedical { get { return true; } }

        [XmlElement(ElementName = "group_id")]
        public string GroupNumber { get; set; }

        [XmlElement(ElementName = "group_name")]
        public string Name { get; set; }
    }
}

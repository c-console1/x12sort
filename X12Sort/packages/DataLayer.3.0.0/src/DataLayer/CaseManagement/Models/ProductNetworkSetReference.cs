using System.Xml.Serialization;

namespace DataLayer.CaseManagement.Models
{
    [XmlRoot(ElementName = "product_network_set")]
    public class ProductNetworkSetReference : BaseCaseManagementStartEndXmlObject
    {
        [XmlElement(ElementName = "external_product_network_set_id")]
        public string ExternalID { get; set; }

        [XmlElement(ElementName = "external_network_set_id")]
        public string ExternalNetworkSetID { get; set; }
    }
}

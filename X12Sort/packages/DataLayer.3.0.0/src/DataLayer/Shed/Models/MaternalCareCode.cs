using DataLayer.Base.Models;
using DataLayer.Shed.Enumerations;
using DataLayer.StaticUtilities;

namespace DataLayer.Shed.Models
{
    public class MaternalCareCode : BaseShedObject, ICode
    {
        public MaternalCareIndicator Indicator { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string AmisysLabCode { get; set; }

        public string TruCareKeyMetricCode { get; set; }

        public string Code {
            get { return Indicator.ToValue(); }
            set { Indicator = EnumConverter.ToEnum<MaternalCareIndicator>(value); }
        }
    }
}

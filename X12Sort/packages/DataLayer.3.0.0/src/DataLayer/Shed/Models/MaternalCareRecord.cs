using System;
using DataLayer.Shed.Enumerations;

namespace DataLayer.Shed.Models
{
    public class MaternalCareRecord : BaseShedObject
    {
        public string ClaimNumber { get; set; }

        public string ServiceLineNumber { get; set; }

        public string MemberNumber { get; set; }

        public string MemberMedicaidID { get; set; }

        public DateTime RecordDate { get; set; }

        public MaternalCareIndicator Indicator { get; set; }
    }
}

using DataLayer.Base.Enumerations;
using DataLayer.Base.Models;

namespace DataLayer.Shed.Models
{
    /// <summary>
    /// Base object for all Shed Objects
    /// </summary>
    public abstract class BaseShedObject : BaseDataObject
    {
        public override string DatabaseKey { get => base.DatabaseKey; set => base.DatabaseKey = value; }

        public override DataSource Source { get => DataSource.Shed; }

        /// <summary>
        /// Standard Constructor, Should be called by all inheriting objects
        /// </summary>
        protected BaseShedObject() : base()
        {
        }
    }
}

using DataLayer.Base;

namespace DataLayer.Shed
{
    /// <summary>
    /// Logical Database for the Data Shed
    /// </summary>
    public class ShedDatabase : BaseDatabase
    {
        public MedicaidRepository Medicaid { get; private set; }
        public TruCareRepository TruCare { get; private set; }

        internal ShedDatabase(IDbExec dbExec) : base(dbExec)
        {
            Medicaid = new MedicaidRepository(this);
            TruCare = new TruCareRepository(this);
        }

        internal ShedDatabase(params IExecutor[] executors) : base(executors)
        {
            Medicaid = new MedicaidRepository(this);
            TruCare = new TruCareRepository(this);
        }
    }

}

using System.Collections.Generic;
using DataLayer.Base;
using DataLayer.Shed.Models;

namespace DataLayer.Shed
{
    public class MaternalCareRecordDataReader : BaseDataReader<ShedDatabase, MaternalCareRecord>
    {
        #region Constructors
        /// <summary>
        /// Standard Constructor
        /// </summary>
        /// <param name="database">Reference to the database where the data is coming from</param>
        /// <param name="sql">SQL string to get data from the database</param>
        /// <param name="filterTables">List of temp filter table names with corresponding lists of values</param>
        internal MaternalCareRecordDataReader(ShedDatabase database, string sql, object param, Dictionary<string, IEnumerable<object>> filterTables)
            : base(database, sql, param, filterTables)
        {

        }
        #endregion
    }
}

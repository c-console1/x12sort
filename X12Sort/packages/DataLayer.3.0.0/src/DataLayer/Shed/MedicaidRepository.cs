using System;
using System.Collections.Generic;
using DataLayer.Base;
using DataLayer.Shed.Models;
using DataLayer.Shed.Enumerations;
using DataLayer.Base.Models;
using System.Linq;
using System.Text;
using DataLayer.StaticUtilities.Extensions;

namespace DataLayer.Shed
{
    public class MedicaidRepository : BaseRepository<ShedDatabase>
    {
        #region Constants/Variables
        const bool DEFAULT_ERROR_ON_MULTIPLE = true;

        private static readonly DateTime LOW_DATE = new DateTime(1900, 1, 1);
        private static readonly DateTime HIGH_DATE = new DateTime(1900, 1, 1);

        private static Dictionary<MaternalCareIndicator, MaternalCareCode> _maternalCareCodes = null;
        #endregion

        #region Properties
        public Dictionary<MaternalCareIndicator, MaternalCareCode> MaternalCareCodes
        {
            get
            {
                if (_maternalCareCodes == null)
                    _maternalCareCodes = GetMaternalCareCodes().ToDictionary(c => c.Indicator, c => c);
                return _maternalCareCodes;
            }
        }
        #endregion

        internal MedicaidRepository(ShedDatabase database) : base(database)
        {

        }

        ///////////////////////////////////////////////////////////////////////////////////////////

        #region Maternal Codes Methods

        public IEnumerable<MaternalCareCode> GetMaternalCareCodes()
        {
            return Database.Query<MaternalCareCode>(@"
select [Indicator]            as [Indicator]
      ,[Name]                 as [Name]
      ,[Description]          as [Description]
      ,[AmisysLabCode]        as [AmisysLabCode]
	  ,[TruCareKeyMetricCode] as [TruCareKeyMetricCode]
  from [medicaid].[MaternalCareCodes]
");
        }

        #endregion

        ///////////////////////////////////////////////////////////////////////////////////////////

        #region Public Maternal Care Record Methods


        #region Get Maternal Care Record(s) By Claim Number(s)
        /// <summary>
        /// Get a single maternal care record from the Shed by claim number
        /// </summary>
        /// <param name="claimNumber">Claim number for maternal care record</param>
        /// <param name="errorOnMultiple">Whether the method should generate an error when multiple records are returned</param>
        /// <returns>Single maternal care record</returns>
        public MaternalCareRecord GetMaternalCareRecordByClaimNumber(string claimNumber, bool errorOnMultiple = DEFAULT_ERROR_ON_MULTIPLE)
        {
            return GetMaternalCareRecord(
                new Dictionary<string, object>()
                {
                    { "claimNumber", claimNumber }
                },
                errorOnMultiple
                );
        }

        /// <summary>
        /// Get a collection maternal care records from the Shed by claim numbers
        /// </summary>
        /// <param name="claimNumbers">Claim numbers for maternal care records</param>
        /// <returns>Collection of maternal care records</returns>
        public IEnumerable<MaternalCareRecord> GetMaternalCareRecordsByClaimNumbers(IEnumerable<string> claimNumbers)
        {
            return GetMaternalCareRecords(
                new Dictionary<string, object>()
                {
                    { "claimNumbers", claimNumbers }
                });
        }
        #endregion

        #region Get Maternal Care Records By Member Number(s)
        /// <summary>
        /// Get a collection maternal care records from the Shed by member number
        /// </summary>
        /// <param name="memberNumber">Member number for maternal care records</param>
        /// <returns>Collection of maternal care records</returns>
        public IEnumerable<MaternalCareRecord> GetMaternalCareRecordsByMemberNumber(string memberNumber)
        {
            return GetMaternalCareRecords(
                new Dictionary<string, object>()
                {
                    { "memberNumber", memberNumber }
                });
        }

        /// <summary>
        /// Get a collection maternal care records from the Shed by member numbers
        /// </summary>
        /// <param name="memberNumbers">Member numbers for maternal care records</param>
        /// <returns>Collection of maternal care records</returns>
        public IEnumerable<MaternalCareRecord> GetMaternalCareRecordsByMemberNumbers(IEnumerable<string> memberNumbers)
        {
            return GetMaternalCareRecords(
                new Dictionary<string, object>()
                {
                    { "memberNumbers", memberNumbers }
                });
        }

        /// <summary>
        /// Get a maternal care record data reader from the Shed, with no filtering applied
        /// </summary>
        /// <param name="memberNumbers">Member numbers for maternal care records</param>
        /// <returns>Data reader for maternal care records</returns>
        public MaternalCareRecordDataReader GetMaternalCareRecordDataReaderByMemberNumbers(IEnumerable<string> memberNumbers)
        {
            return GetMaternalCareRecordDataReader(
                new Dictionary<string, object>()
                {
                    { "memberNumbers", memberNumbers }
                });
        }
        #endregion

        #region Get Maternal Care Records By Date
        /// <summary>
        /// Get a collection maternal care records from the Shed by date
        /// </summary>
        /// <param name="itemWithDateRange">Item with a date range</param>
        /// <returns>Collection of maternal care records</returns>
        public IEnumerable<MaternalCareRecord> GetMaternalCareRecordsByDateRange(IStartEnd itemWithDateRange)
        {
            return GetMaternalCareRecordsByDateRange(itemWithDateRange.StartDate, itemWithDateRange.EndDate);
        }

        /// <summary>
        /// Get a collection maternal care records from the Shed by date
        /// </summary>
        /// <param name="startDate">Start date of range</param>
        /// <param name="endDate">End date of range</param>
        /// <returns>Collection of maternal care records</returns>
        public IEnumerable<MaternalCareRecord> GetMaternalCareRecordsByDateRange(DateTime? startDate = null, DateTime? endDate = null)
        {
            if (startDate == null) startDate = LOW_DATE;
            if (endDate == null) endDate = HIGH_DATE;

            return GetMaternalCareRecords(
                new Dictionary<string, object>()
                {
                    { "startDate", startDate },
                    { "endDate", endDate }
                });
        }

        /// <summary>
        /// Get a maternal care record data reader from the Shed by date
        /// </summary>
        /// <param name="itemWithDateRange">Item with a date range</param>
        /// <returns>Data reader for maternal care records</returns>
        public MaternalCareRecordDataReader GetMaternalCareRecordDataReaderByDateRange(IStartEnd itemWithDateRange)
        {
            return GetMaternalCareRecordDataReaderByDateRange(itemWithDateRange.StartDate, itemWithDateRange.EndDate);
        }

        /// <summary>
        /// Get a maternal care record data reader from the Shed by date
        /// </summary>
        /// <param name="startDate">Start date of range</param>
        /// <param name="endDate">End date of range</param>
        /// <returns>Data reader for maternal care records</returns>
        public MaternalCareRecordDataReader GetMaternalCareRecordDataReaderByDateRange(DateTime? startDate = null, DateTime? endDate = null)
        {
            if (startDate == null) startDate = LOW_DATE;
            if (endDate == null) endDate = HIGH_DATE;

            return GetMaternalCareRecordDataReader(
                new Dictionary<string, object>()
                {
                    { "startDate", startDate },
                    { "endDate", endDate }
                });
        }
        #endregion

        #region Get Maternal Care Records Unfiltered
        /// <summary>
        /// Get a collection maternal care records from the Shed, with no filtering applied
        /// </summary>
        /// <returns>Collection of maternal care records</returns>
        public IEnumerable<MaternalCareRecord> GetMaternalCareRecordsUnfiltered()
        {
            return GetMaternalCareRecords(null);
        }

        /// <summary>
        /// Get a maternal care record data reader from the Shed, with no filtering applied
        /// </summary>
        /// <returns>Data reader for maternal care records</returns>
        public MaternalCareRecordDataReader GetMaternalCareRecordDataReaderUnfiltered()
        {
            return GetMaternalCareRecordDataReader(null);
        }
        #endregion


        #endregion

        ///////////////////////////////////////////////////////////////////////////////////////////

        #region Private Methods


        #region Get Maternal Care Record(s)
        /// <summary>
        /// Get a single maternal care record from the Shed
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <param name="errorOnMultiple">Whether the method should generate an error when multiple records are returned</param>
        /// <returns>Single maternal care record</returns>
        private MaternalCareRecord GetMaternalCareRecord(Dictionary<string, object> criteria, bool errorOnMultiple)
        {
            IEnumerable<MaternalCareRecord> mcRecords = GetMaternalCareRecords(criteria);
            if (mcRecords.Count() > 1 && errorOnMultiple)
                throw new Exception($"The query returned multiple results: {mcRecords.Count()} maternal care records found");
            else if (mcRecords.Any())
                return mcRecords.First();
            else
                return null;
        }

        /// <summary>
        /// Get a collection of maternal care records from the Shed
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <returns>Collection of maternal care records</returns>
        public IEnumerable<MaternalCareRecord> GetMaternalCareRecords(Dictionary<string, object> criteria)
        {
            // Build maternal care record query
            (string sql, object param, Dictionary<string, IEnumerable<object>> filterTables) = BuildMaternalCareRecordQuery(criteria);

            // Get base maternal care record information
            return Database.QueryWithFilters<MaternalCareRecord>(sql, param, filterTables);
        }

        /// <summary>
        /// Get a maternal care record data reader from the Shed
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <returns>Data reader for maternal care records</returns>
        public MaternalCareRecordDataReader GetMaternalCareRecordDataReader(Dictionary<string, object> criteria)
        {
            // Build maternal care record query
            (string sql, object param, Dictionary<string, IEnumerable<object>> filterTables) = BuildMaternalCareRecordQuery(criteria);

            // Get data reader
            return new MaternalCareRecordDataReader(Database, sql, param, filterTables);
        }

        /// <summary>
        /// Generates the SQL and parameters necessary to get maternal care records from the Shed using the specified criteria
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <returns>
        /// 1 - SQL string to get maternal care records from the Shed
        /// 2 - param object for Dapper query parameter substitution
        /// 3 - list of tables to filter by with table contents
        /// </returns>
        private (string sql, object param, Dictionary<string, IEnumerable<object>> filterTables) BuildMaternalCareRecordQuery(Dictionary<string, object> criteria)
        {
            StringBuilder sql = new StringBuilder();
            Dictionary<string, object> param = new Dictionary<string, object>();
            Dictionary<string, IEnumerable<object>> filterTables = new Dictionary<string, IEnumerable<object>>();

            // Build sql to get base provider information
            // TODO: write maternal care record query
            sql.AppendLine($@"
select * from (

select [ID]                as [DatabaseKey]
      ,[ClaimNumber]       as [ClaimNumber]
      ,[ServiceLineNumber] as [ServiceLineNumber]
      ,[MemberNumber]      as [MemberNumber]
      ,[MemberMedicaidID]  as [MemberMedicaidID]
      ,[RecordDate]        as [RecordDate]
      ,[Indicator]         as [Indicator]
      ,row_number() over (order by [ID] asc) as [rowNum]
  from [medicaid].[MaternalCareRecords]

) mcr
            ");

            #region Add limiting criteria

            #region Add join statements
            if (criteria != null)
            {
                string tableName;
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "claimNumbers":
                            tableName = Database.GenerateTempTableName($"mcr_{key}");
                            filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                            sql.AppendLine($"  join {tableName} tmp_{key} on mcr.[ClaimNumber] = tmp_{key}.[value]");
                            break;
                        case "memberNumbers":
                            tableName = Database.GenerateTempTableName($"mcr_{key}");
                            filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                            sql.AppendLine($"  join {tableName} tmp_{key} on mcr.[MemberNumber] = tmp_{key}.[value]");
                            break;
                    }
                }
            }
            #endregion

            #region Add row number limiter
            sql.AppendLine(" where mcr.[rowNum] between @minRowNum and @maxRowNum");

            // Add min and max row numbers values (will be overwritten if this isdestined for a data reader)
            param.Add("minRowNum", 0);
            param.Add("maxRowNum", int.MaxValue);
            #endregion

            #region Add where clauses
            if (criteria != null)
            {
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "claimNumber":
                            param.Add(key, criteria[key]);
                            sql.AppendLine("   and mcr.[ClaimNumber] = @claimNumber");
                            break;
                        case "memberNumber":
                            param.Add(key, criteria[key]);
                            sql.AppendLine("   and mcr.[MemberNumber] = @id");
                            break;
                        case "startDate":
                            if (criteria[key] != null)
                            {
                                param.Add(key, criteria[key]);
                                sql.AppendLine("   and mcr.[RecordDate] >= @startDate");
                            }
                            break;
                        case "endDate":
                            if (criteria[key] != null)
                            {
                                param.Add(key, criteria[key]);
                                sql.AppendLine("   and mcr.[RecordDate] <= @endDate");
                            }
                            break;
                    }
                }
            }
            #endregion

            #endregion

            /// 1 - SQL string to get maternal care records from the Shed
            /// 2 - param object for Dapper query parameter substitution
            /// 3 - list of tables to filter by with table contents
            return (sql.ToString(), param.ToQueryParamObject(), filterTables);
        }
        #endregion


        #endregion
    }
}

using DataLayer.StaticUtilities.Attributes;

namespace DataLayer.Shed.Enumerations
{
    public enum MaternalCareIndicator : int
    {
        [EnumConverter("", true)]
        Unknown = 0,

        C = 1,  // Chronic Condition
        H = 2,  // High Risk
        M = 3,  // Medium Risk
        P = 4,  // Prior Poor Outcome
        S = 5,  // Still Birth

        Other = 999
    }
}

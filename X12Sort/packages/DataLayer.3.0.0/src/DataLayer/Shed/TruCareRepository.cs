using System.Collections.Generic;
using DataLayer.Base;

namespace DataLayer.Shed
{
    public class TruCareRepository : BaseRepository<ShedDatabase>
    {
        internal TruCareRepository(ShedDatabase database) : base(database)
        {

        }

        /// <summary>
        /// Gets a lookup table from the database
        /// </summary>
        /// <param name="FeedName"></param>
        /// <param name="LookupName"></param>
        /// <returns></returns>
        public Dictionary<string, string> GetLookupTable(string mapName)
        {
            string sql = @"
select vm.[Key]
      ,vm.[Value]
  from [trucare].[DataFeedValueMaps] vm
  join [trucare].[DataFeedValueMapDefinitions] vmd
    on vmd.[MapDefinitionID] = vm.[MapDefinitionID]
   and vmd.[Name] = @mapName
";

            return Database.GetDictionary<string, string>(sql, new { mapName });
        }

        /// <summary>
        /// Saves a lookup value to the database
        /// </summary>
        /// <param name="FeedName"></param>
        /// <param name="LookupName"></param>
        /// <param name="Key"></param>
        /// <param name="Value"></param>
        /// <param name="Description"></param>
        public void SetLookupValue(string mapName, string key, string value)
        {
            string getMapIdSql = @"
select [MapDefinitionID]
  from [trucare].[DataFeedValueMapDefinitions]
 where [Name] = @mapName
";
            string updateValueSql = @"
declare @itemId [int] = (
   select [ID]
     from [trucare].[DataFeedValueMaps]
    where [MapDefinitionID] = @mapId
      and [Key] = @key )

if @itemId is null
begin
   insert into [trucare].[DataFeedValueMaps] ( [MapDefinitionID], [Key], [Value] )
   values ( @mapId, @key, @value )
end

else
begin
   update [trucare].[DataFeedValueMaps]
      set [Value] = @value
    where [ID] = @itemId
end
";

            int mapId = Database.ExecuteScalar<int>(getMapIdSql, new { mapName });
            Database.Execute(updateValueSql, new { mapId, key, value });
        }
    }
}

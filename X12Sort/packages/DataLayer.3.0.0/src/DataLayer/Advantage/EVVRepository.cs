using System.Linq;
using DataLayer.Base;
using DataLayer.StaticUtilities;

namespace DataLayer.Advantage
{
    /// <summary>
    /// The EVV (Electronic Vist Verification) Repository, a collection functions related to EVV data in the Advantage database
    /// </summary>
    public class EVVRepository : BaseRepository<AdvantageDatabase>
    {
        internal EVVRepository(AdvantageDatabase database) : base(database)
		{

        }
    }
}

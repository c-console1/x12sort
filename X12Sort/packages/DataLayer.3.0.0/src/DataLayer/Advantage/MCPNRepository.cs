using Dapper;
using DataLayer.Base;

namespace DataLayer.Advantage
{
    /// <summary>
    /// The MCPN (Managed Care Provider Network) Repository, a collection functions related to MCPN data in the Advantage database
    /// </summary>
    public class MCPNRepository : BaseRepository<AdvantageDatabase>
    {
        internal MCPNRepository(AdvantageDatabase database) : base(database)
		{

        }
    }
}

using System;
using DataLayer.Base;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.Advantage
{
    /// <summary>
    /// The CCR (Consumer Contact Record) Repository, a collection functions related to CCR data in the Advantage database
    /// </summary>
    public class CCRRepository : BaseRepository<AdvantageDatabase>
    {
        internal CCRRepository(AdvantageDatabase database) : base(database)
        {

        }
    }
}

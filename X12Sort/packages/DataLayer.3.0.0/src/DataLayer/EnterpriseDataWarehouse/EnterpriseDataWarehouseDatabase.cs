using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer.Base;

namespace DataLayer.EnterpriseDataWarehouse
{
    public class EnterpriseDataWarehouseDatabase : BaseDatabase
    {
        public AccountRepository Accounts { get; private set; }
        public BenefitRepository Benefits { get; private set; }
        public MemberRepository Members { get; private set; }
        public ProviderRepository Providers { get; private set; }

        internal EnterpriseDataWarehouseDatabase(MachineApplicationInfo info, params IExecutor[] executors) : base(info, executors)
        {
            Accounts = new AccountRepository(this);
            Benefits = new BenefitRepository(this);
            Members = new MemberRepository(this);
            Providers = new ProviderRepository(this);
        }
    }
}

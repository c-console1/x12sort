namespace DataLayer.EnterpriseDataWarehouse.Enumerations
{
    /// <summary>
    /// Address types
    /// </summary>
    public enum AddressType : int
    {
        Unknown = 0,
        PrimaryResidence = 1,
        Mailing = 2,
        Billing = 3,
        Seasonal = 4,
        ExternalDependant = 5,

        Other = 999
    }
}

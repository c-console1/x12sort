namespace DataLayer.EnterpriseDataWarehouse.Enumerations
{
    public enum MemberComponent : int
    {
        Addresses = 1,
        [System.Obsolete("Not yet implemented", false)]
        Claim = 2,
        [System.Obsolete("Not yet implemented", false)]
        Authorization = 3,
        MemberSpan = 4,
        PCPInformation = 5,
    }
}

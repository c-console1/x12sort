namespace DataLayer.EnterpriseDataWarehouse.Enumerations
{
    public enum ProviderNumberType : int
    {
        Unknown = 0,

        BillingID = 1,
        CredentialingID = 2,
        LegacyBillingID = 3,
        NPI = 4,
        SSN = 5,
        MedicaidNumber = 6,
        MedicareNumber = 7,
        DatabaseKey = 8,

        Other = 999
    }
}

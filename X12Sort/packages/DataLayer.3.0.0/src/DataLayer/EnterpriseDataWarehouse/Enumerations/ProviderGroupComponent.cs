namespace DataLayer.EnterpriseDataWarehouse.Enumerations
{
    public enum ProviderGroupComponent : int
    {
        Providers = 1,
        Addresses = 2
    }
}

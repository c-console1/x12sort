namespace DataLayer.EnterpriseDataWarehouse.Enumerations
{
    /// <summary>
    /// Member types
    /// </summary>
    public enum MemberType : int
    {
        Unknown = 0,
        Subscriber = 1,
        Member = 2
    }
}

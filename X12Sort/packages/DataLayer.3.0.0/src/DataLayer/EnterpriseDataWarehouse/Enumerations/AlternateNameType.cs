namespace DataLayer.EnterpriseDataWarehouse.Enumerations
{
    /// <summary>
    /// Alter name types
    /// </summary>
    public enum AlternateNameType : int
    {
        Unknown = 0,
        Madien = 1,
        Changed = 2,
        IncorrectlyEntered = 3,
        Primary = 4,

        Other = 999
    }
}

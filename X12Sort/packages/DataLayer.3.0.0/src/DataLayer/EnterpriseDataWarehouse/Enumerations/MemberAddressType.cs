namespace DataLayer.EnterpriseDataWarehouse.Enumerations
{
    /// <summary>
    /// Address types
    /// </summary>
    public enum MemberAddressType : int
    {
        Unknown = 0,
        Residential = 1,
        Mailing = 2,
        Other = 999
    }
}

namespace DataLayer.EnterpriseDataWarehouse.Enumerations
{
    /// <summary>
    /// Phone types
    /// </summary>
    public enum EDWPhoneNumberType : int
    {
        Unknown = 0,
        Home = 1,
        Office = 2,
        Cell = 3,
        AltNumber = 4,
        Fax = 5,

        Other = 999
    }
}

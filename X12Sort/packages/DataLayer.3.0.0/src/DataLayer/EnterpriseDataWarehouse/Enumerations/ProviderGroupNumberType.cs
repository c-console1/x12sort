namespace DataLayer.EnterpriseDataWarehouse.Enumerations
{
    public enum ProviderGroupNumberType : int
    {
        Unknown = 0,

        CredentialingID = 1,
        NPI = 2,
        TaxID = 3,
        DatabaseKey = 4,

        Other = 999
    }
}

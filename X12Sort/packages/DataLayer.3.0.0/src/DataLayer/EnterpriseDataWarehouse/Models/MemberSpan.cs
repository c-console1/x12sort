using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Models
{
    public class MemberSpan
    {
        public string MemberNumber { get; set; }

        #region MEMBER_DIM Primary and Foreign Keys
        /// <summary>
        /// Foreign key referencing MEMBER_DIM table
        /// </summary>
#pragma warning disable CA1707 // Identifiers should not contain underscores
        public string MEMBER_DIM_KEY { get; set; }

        public string BENEFIT_PLAN_DIM { get; set; }
#pragma warning restore CA1707 // Identifiers should not contain underscores
        #endregion

        /// <summary>
        /// Member Plan Name (benefit_plan_dim.benefit_plan_hcc_id)
        /// </summary>
        public string PlanName { get; set; } = null;
        /// <summary>
        /// Member Plan Description (benefit_plan_dim.description)
        /// </summary>
        public string PlanDescription { get; set; } = null;
        /// <summary>
        /// Member Plan Start Date (benefit_plan_dim.plan_effective_date)
        /// </summary>
        public DateTime? PlanStartDate { get; set; } = null;
        /// <summary>
        /// Member Plan End Date (benefit_plan_dim.plan_termination_date)
        /// </summary>
        public DateTime? PlanEndDate { get; set; } = null;
        /// <summary>
        /// Span status
        /// </summary>
        public string Status { get; set; }
    }
}

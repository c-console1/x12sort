using System.Collections.Generic;
using Newtonsoft.Json;

namespace DataLayer.EnterpriseDataWarehouse.Models
{
    public class TypeCode
    {
        /// <summary>
        /// Code set name - key of the code set
        /// </summary>
        [JsonProperty("codeSetName")]
        public string CodeSetName { get; set; }

        /// <summary>
        /// Code entry - specfic code
        /// </summary>
        [JsonProperty("codeEntry")]
        public string CodeEntry { get; set; }

        /// <summary>
        /// Short descriptive name
        /// </summary>
        [JsonProperty("shortName")]
        public string ShortName { get; set; }

        /// <summary>
        /// Additional values
        /// </summary>
        [JsonProperty("additionalValues")]
        public IEnumerable<string> AdditionalValues { get; set; }
    }
}

using DataLayer.Base.Models;
using System;

namespace DataLayer.EnterpriseDataWarehouse.Models
{
    /// <summary>
    /// This is a class for cactus provider specialties
    /// </summary>
    public class ProviderSpecialty : BaseEDWObject, IForeignBaseDataObject, IStartEnd
    {
        /// <summary>
        /// Foreign key relating to the parent provider record
        /// </summary>
        public string ForeignDatabaseKey { get; set; }

        /// <summary>
        /// Specialty description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Specialty start date
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Specialty end date
        /// </summary>
        public DateTime? EndDate { get; set; }
    }

}

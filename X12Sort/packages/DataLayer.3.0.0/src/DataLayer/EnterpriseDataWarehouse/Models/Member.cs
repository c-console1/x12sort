using System;
using System.Collections.Generic;
using DataLayer.Base.Models;
using DataLayer.Base.Enumerations;
using DataLayer.EnterpriseDataWarehouse.Enumerations;

namespace DataLayer.EnterpriseDataWarehouse.Models
{
    /* Desired result fields from Portal:
     *
     * GroupID              -- "Account ID" is currently returned
     * PcpId                -- Data are spotty but code is correct.
     * GroupName            -- "Account name" is currently returned
     * DivisionNumber       -- No idea what to do here
     * IsOBAGroup           -- No idea what to do here
     * QualifiedMedicareBeneficiary
     *                      -- No idea what to do here
     *
     * EmployerName         -- "Account name" is currently returned
     * EffectiveDate        -- Need to verify this is what's meant.
     * 
     */
    public class Member : BaseEDWObject, IMember
    {
        #region Constructors
        public Member() { }

        public Member(Member basis)
        {
            #region IMember fields
            if (basis != null)
            {
                MemberNumber = basis.MemberNumber;
                Prefixes = basis.Prefixes == null ? null : new List<string>(basis.Prefixes);
                FirstName = basis.FirstName;
                MiddleName = basis.MiddleName;
                LastName = basis.LastName;
                Suffixes = basis.Suffixes == null ? null : new List<string>(basis.Suffixes);
                Sex = new Sex(basis.Sex);
                BirthDate = basis.BirthDate;
                DeathDate = basis.DeathDate;
                SSN = basis.SSN;
                Ethnicity = basis.Ethnicity;
                Languages = basis.Languages == null ? null : new List<Language>(basis.Languages);
                #endregion

                MEMBER_DIM_KEY = basis.MEMBER_DIM_KEY;
                ACCOUNT_DIM_KEY = basis.ACCOUNT_DIM_KEY;
                SUBSCRIBER_DIM_KEY = basis.SUBSCRIBER_DIM_KEY;
                FullName = basis.FullName;
                ContractNumber = basis.ContractNumber;
                AccountNumber = basis.AccountNumber;
                MedicareNumber = basis.MedicareNumber;
                MedicaidNumber = basis.MedicaidNumber;
                CaseNumber = basis.CaseNumber;
                AmisysId = basis.AmisysId;
                GroupNumber = basis.GroupNumber;
                MemberType = basis.MemberType;
                AlternateKeys = basis.AlternateKeys == null ? null : new List<MemberAlternateKey>(basis.AlternateKeys);   // TODO -- deep list copy
                AlternateNames = basis.AlternateNames == null ? null : new List<AlternateName>(basis.AlternateNames);     // TODO -- deep list copy
                                                                                                                          //EnrollmentCurrent = basis.EnrollmentCurrent == null ? null : basis.EnrollmentCurrent;                   // TODO -- copy constructor
                                                                                                                          //EnrollmentHistory = basis.EnrollmentHistory == null ? null : basis.EnrollmentHistory;                   // TODO -- deep list copy
                Claims = basis.Claims == null ? null : new List<IClaim>(basis.Claims);                                    // TODO -- deep list copy
                MemberNumbers = basis.MemberNumbers == null ? null : new List<string>(basis.MemberNumbers);
                StartDate = basis.StartDate;
                EndDate = basis.EndDate;
                Phone = basis.Phone;
                Addresses = basis.Addresses == null ? null : new List<MemberAddress>(basis.Addresses);                    // TODO -- deep list copy
                CoverageSpans = basis.CoverageSpans == null ? null : new List<MemberSpan>(basis.CoverageSpans);           // TODO -- deep list copy
            }
        }
        #endregion

        #region Implement IMember
        public string MemberNumber { get; set; }
        public IEnumerable<string> Prefixes { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public IEnumerable<string> Suffixes { get; set; }
        public Sex Sex { get; set; }
        public DateTime? BirthDate { get; set; }
        public DateTime? DeathDate { get; set; }
        public string SSN { get; set; }
        public string Ethnicity { get; set; }
        public IEnumerable<Language> Languages { get; set; }
        #endregion

        #region MEMBER_DIM Primary and Foreign Keys
#pragma warning disable CA1707 // Identifiers should not contain underscores

        /// <summary>
        /// Primary key of MEMBER_DIM table
        /// </summary>
        public string MEMBER_DIM_KEY { get; set; }
        /// <summary>
        /// Foreign key of Member_DIM table to ACCOUNT_DIM table
        /// </summary>
        public string ACCOUNT_DIM_KEY { get; set; }
        /// <summary>
        /// Foreign key of Member_DIM table to SUBSCRIBER_DIM table
        /// </summary>
        public string SUBSCRIBER_DIM_KEY { get; set; }
#pragma warning restore CA1707 // Identifiers should not contain underscores

        #endregion

        /// <summary>
        /// Member full name 
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Member Contract Number (MEMBER_DIM.SUBSCRIPTION_HCC_ID)
        /// </summary>
        public string ContractNumber { get; set; }


        /// <summary>
        /// Member Account Number (ACCOUNT_DIM.ACCOUNT_HCC_ID)
        /// </summary>
        public string AccountNumber { get; set; }


        /// <summary>
        /// Member Medicare Number (MEMBER_OTHER_ID_DETAILS.IDENTIFICATION_NUMBER)
        /// IF MEMBER_OTHER_ID_DETAILS.ID_TYPE = 'HCFA Plan ID' 
        /// </summary>
        public string MedicareNumber { get; set; }

        /// <summary>
        /// Member Medicaid Number (MEMBER_OTHER_ID_DETAILS.IDENTIFICATION_NUMBER)
        /// WHERE MEMBER_OTHER_ID_DETAILS.ID_TYPE = 'Medicaid Number' 
        /// </summary>
        public string MedicaidNumber { get; set; }

        /// <summary>
        /// Member Case Number (MEMBER_OTHER_ID_DETAILS.IDENTIFICATION_NUMBER)
        /// IF MEMBER_OTHER_ID_DETAILS.ID_TYPE = 'Medicaid Case#'
        /// </summary>
        public string CaseNumber { get; set; }

        /// <summary>
        /// Member Amisys Id (MEMBER_OTHER_ID_DETAILS.IDENTIFICATION_NUMBER)
        /// IF MEMBER_OTHER_ID_DETAILS.ID_TYPE = 'Amisys ID'
        /// </summary>
        public string AmisysId { get; set; }

        /// <summary>
        /// Member Group NUmber (ACCOUNT_DIM.NAME)
        /// IF LEVEL =0
        /// </summary>
        public string GroupNumber { get; set; }

        /// <summary>
        /// Member type
        /// </summary>
        public MemberType MemberType { get; set; }

        /// <summary>
        /// Alternative keys
        /// </summary>
        [System.Obsolete("Not implemented", false)]
        public IEnumerable<MemberAlternateKey> AlternateKeys { get; set; }

        /// <summary>
        /// Alternate names
        /// </summary>
        [System.Obsolete("Not implemented", false)]
        public IEnumerable<AlternateName> AlternateNames { get; set; }

        /// <summary>
        /// Current enrollement
        /// </summary>
        [System.Obsolete("Not implemented", true)]
        public Enrollment EnrollmentCurrent { get; set; }

        /// <summary>
        /// Enrollement history
        /// </summary>
        [System.Obsolete("Not implemented", true)]
        public IEnumerable<Enrollment> EnrollmentHistory { get; set; }

        /// <summary>
        /// Member claims
        /// </summary>
        public IEnumerable<IClaim> Claims { get; set; }

        /// <summary>
        /// Member numbers ordered newest to oldest
        /// </summary>
        public List<string> MemberNumbers { get; set; }

        /// <summary>
        /// Membership Start Date (EDW.MEMBER_DIM.MEMBER_EFFECTIVE_DATE)
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Membership End Date (EDW.MEMBER_DIM.MEMBER_TERMINATION_DATE)
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Member Phone
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// Member Cap Rate Value
        /// </summary>
        public string CapRateValue { get; set; }
        /// <summary>
        /// Member Cap Rate Description
        /// </summary>
        public string CapRateDescription { get; set; }
        /// <summary>
        /// MEMBER ADDRESSES
        /// </summary>
        public IEnumerable<MemberAddress> Addresses { get; set; }

        public IEnumerable<PCPProvider> Providers { get; set; }
        
        /// <summary>
        /// MEMBER SPANS
        /// </summary>
        public IEnumerable<MemberSpan> CoverageSpans { get; set; } = null;
    }
}

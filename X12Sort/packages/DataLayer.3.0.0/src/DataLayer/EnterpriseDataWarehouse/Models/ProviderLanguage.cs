using DataLayer.Base.Models;

namespace DataLayer.EnterpriseDataWarehouse.Models
{
    /// <summary>
    /// This is a class for cactus provider languages
    /// </summary>
    public class ProviderLanguage : BaseEDWObject, IForeignBaseDataObject
    {
        /// <summary>
        /// Foreign key relating to the parent provider record
        /// </summary>
        public string ForeignDatabaseKey { get; set; }

        /// <summary>
        /// Language description
        /// </summary>
        public string Description { get; set; }
    }

}

using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Models
{
    public class Pharmacy : BaseEDWObject
    {
        public string Name { get; set; }

        public string PharmacyNumber { get; set; }

        public string TaxID { get; set; }

        public string NPI { get; set; }

        public ProviderAddress Address { get; set; }
    }
}

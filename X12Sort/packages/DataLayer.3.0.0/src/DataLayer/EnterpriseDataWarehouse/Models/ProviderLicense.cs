using DataLayer.Base.Models;
using System;

namespace DataLayer.EnterpriseDataWarehouse.Models
{
/// <summary>
/// This is a class for cactus provider licenses
/// </summary>
    public class ProviderLicense : BaseEDWObject, IForeignBaseDataObject, IStartEnd
    {
        /// <summary>
        /// Foreign key relating to the parent provider record
        /// </summary>
        public string ForeignDatabaseKey { get; set; }

        /// <summary>
        /// License description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Issuing institution name
        /// </summary>
        public string InstitutionName { get; set; }

        /// <summary>
        /// License status
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// License number
        /// </summary>
        public string LicenseNumber { get; set; }

        /// <summary>
        /// Licensing state
        /// </summary>
        public USState LicenseState { get; set; }

        /// <summary>
        /// License start date
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// License end date
        /// </summary>
        public DateTime? EndDate { get; set; }
    }
}

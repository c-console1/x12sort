using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Models
{
    public struct MemberSearchParam
    {
        public string MemberNumber;
        public IEnumerable<string> MemberNumbers;
        public string MemberNumberOrMMIS;
        public string FirstName;
        public string LastName;
        public DateTime? DateOfBirth;
        public string Gender;                  // settle list of values once and for all?
        public string City;
        public string ZipCode;
        public string PhoneNumber;
        public DateTime? EligibilityStart;
        public string MemberStatus;            // Make an enum

        #region IEquatable
        public static bool operator !=(MemberSearchParam obj1, MemberSearchParam obj2)
        {
            return !(obj1 == obj2);
        }

        public static bool operator ==(MemberSearchParam obj1, MemberSearchParam obj2)
        {
            if (ReferenceEquals(obj1, obj2))
            {
                return true;
            }

            if (ReferenceEquals(obj1, null) || ReferenceEquals(obj2, null))
            {
                return false;
            }

            return obj1.Equals(obj2);
        }

        public override bool Equals(object obj)
        {
            return obj is MemberSearchParam parms &&
                MemberNumber == parms.MemberNumber &&
                MemberNumbers == parms.MemberNumbers &&
                MemberNumberOrMMIS == parms.MemberNumberOrMMIS &&
                FirstName == parms.FirstName &&
                LastName == parms.LastName &&
                DateOfBirth == parms.DateOfBirth &&
                Gender == parms.Gender &&
                City == parms.City &&
                ZipCode == parms.ZipCode &&
                PhoneNumber == parms.PhoneNumber &&
                EligibilityStart == parms.EligibilityStart &&
                MemberStatus == parms.MemberStatus;
        }

        public bool Equals(MemberSearchParam obj)
        {
            return Equals(obj as object);
        }

        public override int GetHashCode()
        {
            var hashCode = -2049519706;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(MemberNumber);
            hashCode = hashCode * -1521134295 + EqualityComparer<IEnumerable<string>>.Default.GetHashCode(MemberNumbers);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(MemberNumberOrMMIS);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(FirstName);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(LastName);
            hashCode = hashCode * -1521134295 + EqualityComparer<DateTime?>.Default.GetHashCode(DateOfBirth);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Gender);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(City);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ZipCode);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(PhoneNumber);
            hashCode = hashCode * -1521134295 + EqualityComparer<DateTime?>.Default.GetHashCode(EligibilityStart);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(MemberStatus);
            return hashCode;
        }
        #endregion

    }
}

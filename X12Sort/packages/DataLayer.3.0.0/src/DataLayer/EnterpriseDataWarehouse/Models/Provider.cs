using DataLayer.Base.Enumerations;
using DataLayer.Base.Models;
using DataLayer.EnterpriseDataWarehouse.Enumerations;
using DataLayer.StaticUtilities;
using System;
using System.Collections.Generic;
using System.Data;

namespace DataLayer.EnterpriseDataWarehouse.Models
{
    /// <summary>
    /// This is a class for cactus providers
    /// </summary>
    public class Provider : BaseEDWObject, IProvider, IEquatable<Provider>
    {
        /// <summary>
        /// This is the best way to identify this provider as there is no one identifier that ALL providers have
        /// </summary>
        public string ProviderNumber
        {
            get
            {
                switch(ProviderNumberType)
                {
                    case ProviderNumberType.BillingID: return BillingID;
                    case ProviderNumberType.CredentialingID: return CredentialingID;
                    case ProviderNumberType.LegacyBillingID: return LegacyBillingID;
                    case ProviderNumberType.NPI: return NPI;
                    case ProviderNumberType.SSN: return SSN;
                    case ProviderNumberType.MedicaidNumber: return MedicaidNumber;
                    case ProviderNumberType.MedicareNumber: return MedicareNumber;
                    default: return DatabaseKey;
                }
            }
            set
            {
                switch (ProviderNumberType)
                {
                    case ProviderNumberType.BillingID: BillingID = value; break;
                    case ProviderNumberType.CredentialingID: CredentialingID = value; break;
                    case ProviderNumberType.LegacyBillingID: LegacyBillingID = value; break;
                    case ProviderNumberType.NPI: NPI = value; break;
                    case ProviderNumberType.SSN: SSN = value; break;
                    case ProviderNumberType.MedicaidNumber: MedicaidNumber = value; break;
                    case ProviderNumberType.MedicareNumber: MedicareNumber = value; break;
                    default: DatabaseKey = value; break;
                }
            }
        }
        /// <summary>
        /// The type of ID specified by ProviderNumber
        /// </summary>
        public ProviderNumberType ProviderNumberType
        {
            get
            {
                // In the hierarchy of provider IDs, the current billing ID is the best identifier, while the EDW database key is the worst
                // This returns the type of the best identifier available for this record
                if (!string.IsNullOrWhiteSpace(BillingID)) return ProviderNumberType.BillingID;
                else if (!string.IsNullOrWhiteSpace(CredentialingID)) return ProviderNumberType.CredentialingID;
                else if (!string.IsNullOrWhiteSpace(LegacyBillingID)) return ProviderNumberType.LegacyBillingID;
                else if (!string.IsNullOrWhiteSpace(NPI)) return ProviderNumberType.NPI;
                else if (!string.IsNullOrWhiteSpace(SSN)) return ProviderNumberType.SSN;
                else if (!string.IsNullOrWhiteSpace(MedicaidNumber)) return ProviderNumberType.MedicaidNumber;
                else if (!string.IsNullOrWhiteSpace(MedicareNumber)) return ProviderNumberType.MedicareNumber;
                else return ProviderNumberType.DatabaseKey;
            }
        }
        /// <summary>
        /// Provider ID from Health Rules
        /// </summary>
        public string BillingID { get; set; }
        /// <summary>
        /// Provider ID from Amisys
        /// </summary>
        public string LegacyBillingID { get; set; }
        /// <summary>
        /// Provider ID from Cactus
        /// </summary>
        public string CredentialingID { get; set; }
        /// <summary>
        /// Provider's first name
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Provider's middle name
        /// </summary>
        public string MiddleName { get; set; }
        /// <summary>
        /// Provider's last name
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Provider's Full name
        /// </summary>
        public string FullName { get; set; }
        /// <summary>
        /// Provider's name suffix
        /// </summary>
        public string Suffix { get; set; }
        /// <summary>
        /// Provider's degrees
        /// </summary>
        public string InstitutionName { get; set; }
        /// <summary>
        /// Is this provider an individual?
        /// </summary>
        public bool IsIndividual { get; set; }
        /// <summary>
        /// Is this provider an institution?
        /// </summary>
        public bool IsInstitution
        {
            get
            {
                return !IsIndividual;
            }
        }
        /// <summary>
        /// Provider's medicare number
        /// </summary>
        public string MedicareNumber { get; set; }
        /// <summary>
        /// Provider's medicaid number
        /// </summary>
        public string MedicaidNumber { get; set; }
        /// <summary>
        /// Provider's ssn
        /// </summary>
        public string SSN { get; set; }
        /// <summary>
        /// Provider's upin
        /// </summary>
        public string UPIN { get; set; }
        /// <summary>
        /// Provider's npi
        /// </summary>
        public string NPI { get; set; }
        /// <summary>
        /// Provider's tax id
        /// </summary>
        public string TaxID { get; set; }
        /// <summary>
        /// Provider's birth date
        /// </summary>
        public DateTime? BirthDate { get; set; }
        /// <summary>
        /// Provider's gender
        /// </summary>
        public Sex Sex { get; set; }
        /// <summary>
        /// List of addresses owned by this provider
        /// </summary>
        public IEnumerable<ProviderAddress> Addresses { get; set; }
        /// <summary>
        /// List of boards certifying this provider
        /// </summary>
        public IEnumerable<ProviderBoard> Boards { get; set; }
        /// <summary>
        /// List of provider contracts
        /// </summary>
        public IEnumerable<ProviderEntityAssignment> EntityAssignments { get; set; }
        /// <summary>
        /// List of provider groups this provider is a member of
        /// </summary>
        public IEnumerable<ProviderGroup> Groups { get; set; }
        /// <summary>
        /// List of languages spoken by this provider
        /// </summary>
        public IEnumerable<ProviderLanguage> Languages { get; set; }
        /// <summary>
        /// List of licenses owned by this provider
        /// </summary>
        public IEnumerable<ProviderLicense> Licenses { get; set; }
        /// <summary>
        /// List of this provider's education
        /// </summary>
        public IEnumerable<ProviderEducation> Education { get; set; }
        /// <summary>
        /// List of this provider's specialties
        /// </summary>
        public IEnumerable<ProviderSpecialty> Specialties { get; set; }
        /// <summary>
        /// List of this provider's network affiliations
        /// </summary>
        public IEnumerable<BenefitNetwork> Networks { get; set; }
        /// <summary>
        /// Provider ID from the source system (not EDW)
        /// </summary>
        public string SourceKey { get; set; }

        #region IEquatable
        public override bool Equals(object obj)
        {
            return obj is Provider provider &&
                   ProviderNumber == provider.ProviderNumber &&
                   FirstName == provider.FirstName &&
                   MiddleName == provider.MiddleName &&
                   LastName == provider.LastName &&
                   FullName == provider.FullName &&
                   Suffix == provider.Suffix &&
                   InstitutionName == provider.InstitutionName &&
                   IsIndividual == provider.IsIndividual &&
                   IsInstitution == provider.IsInstitution &&
                   MedicareNumber == provider.MedicareNumber &&
                   MedicaidNumber == provider.MedicaidNumber &&
                   SSN == provider.SSN &&
                   UPIN == provider.UPIN &&
                   NPI == provider.NPI &&
                   TaxID == provider.TaxID &&
                   BirthDate == provider.BirthDate &&
                   Sex.Equals(provider.Sex) &&
                   EqualityComparer<IEnumerable<ProviderAddress>>.Default.Equals(Addresses, provider.Addresses) &&
                   EqualityComparer<IEnumerable<ProviderBoard>>.Default.Equals(Boards, provider.Boards) &&
                   EqualityComparer<IEnumerable<ProviderEntityAssignment>>.Default.Equals(EntityAssignments, provider.EntityAssignments) &&
                   EqualityComparer<IEnumerable<ProviderGroup>>.Default.Equals(Groups, provider.Groups) &&
                   EqualityComparer<IEnumerable<ProviderLanguage>>.Default.Equals(Languages, provider.Languages) &&
                   EqualityComparer<IEnumerable<ProviderLicense>>.Default.Equals(Licenses, provider.Licenses) &&
                   EqualityComparer<IEnumerable<ProviderEducation>>.Default.Equals(Education, provider.Education) &&
                   EqualityComparer<IEnumerable<ProviderSpecialty>>.Default.Equals(Specialties, provider.Specialties);
        }

        public bool Equals(Provider obj)
        {
            return Equals(obj as object);
        }

        public override int GetHashCode()
        {
            var hashCode = -2049519706;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ProviderNumber);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(FirstName);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(MiddleName);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(LastName);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(FullName);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Suffix);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(InstitutionName);
            hashCode = hashCode * -1521134295 + IsIndividual.GetHashCode();
            hashCode = hashCode * -1521134295 + IsInstitution.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(MedicareNumber);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(MedicaidNumber);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(SSN);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(UPIN);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(NPI);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(TaxID);
            hashCode = hashCode * -1521134295 + BirthDate.GetHashCode();
            hashCode = hashCode * -1521134295 + Sex.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<IEnumerable<ProviderAddress>>.Default.GetHashCode(Addresses);
            hashCode = hashCode * -1521134295 + EqualityComparer<IEnumerable<ProviderBoard>>.Default.GetHashCode(Boards);
            hashCode = hashCode * -1521134295 + EqualityComparer<IEnumerable<ProviderEntityAssignment>>.Default.GetHashCode(EntityAssignments);
            hashCode = hashCode * -1521134295 + EqualityComparer<IEnumerable<ProviderGroup>>.Default.GetHashCode(Groups);
            hashCode = hashCode * -1521134295 + EqualityComparer<IEnumerable<ProviderLanguage>>.Default.GetHashCode(Languages);
            hashCode = hashCode * -1521134295 + EqualityComparer<IEnumerable<ProviderLicense>>.Default.GetHashCode(Licenses);
            hashCode = hashCode * -1521134295 + EqualityComparer<IEnumerable<ProviderEducation>>.Default.GetHashCode(Education);
            hashCode = hashCode * -1521134295 + EqualityComparer<IEnumerable<ProviderSpecialty>>.Default.GetHashCode(Specialties);
            return hashCode;
        }
        #endregion

    }
}

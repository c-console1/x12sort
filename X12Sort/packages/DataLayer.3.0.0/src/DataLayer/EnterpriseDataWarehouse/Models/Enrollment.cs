using System;
using System.Collections.Generic;
using DataLayer.Base.Models;

namespace DataLayer.EnterpriseDataWarehouse.Models
{
    public class Enrollment : BaseEDWObject, IStartEnd
    {
        #region Impliment IStartEnd
        /// <summary>
        /// The date this item became valid
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// The data this item stops being valid
        /// </summary>
        public DateTime? EndDate { get; set; }
        #endregion

        /// <summary>
        /// Enrollment ID
        /// </summary>
        public string EnrollmentID { get; set; }

        /// <summary>
        /// Program #
        /// </summary>
        public string ProgramNbr { get; set; }

        /// <summary>
        /// Division #
        /// </summary>
        public string DivisionNbr { get; set; }

        /// <summary>
        /// BHP Node
        /// </summary>
        public BHPNode BHPNode { get; set; }
    }
}

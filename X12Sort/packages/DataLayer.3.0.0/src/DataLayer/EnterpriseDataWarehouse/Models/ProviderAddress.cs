using System;
using System.Collections.Generic;
using System.Linq;
using DataLayer.Base.Models;
using DataLayer.CaseManagement.Models;
using DataLayer.EnterpriseDataWarehouse.Enumerations;

namespace DataLayer.EnterpriseDataWarehouse.Models
{
    public class ProviderAddress : BaseEDWObject, IForeignBaseDataObject, IAddress, IEquatable<ProviderAddress>
    {
        /// <summary>
        /// Foreign key relating to the parent provider/pharmacy record
        /// </summary>
        public string ForeignDatabaseKey { get; set; }

        /// <summary>
        /// Address type
        /// </summary>
        public string AddressType { get; set; }

        /// <summary>
        /// The human name of the address
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The first line of the address
        /// </summary>
        public string Address1 { get; set; }

        /// <summary>
        /// The second line of the address
        /// </summary>
        public string Address2 { get; set; }

        /// <summary>
        /// The third line of the address
        /// </summary>
        public string Address3 { get; set; }

        /// <summary>
        /// The city of the address
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// The State object of the address
        /// </summary>
        public USState State { get; set; } = USState.Unknown;

        /// <summary>
        /// The county of the address
        /// </summary>
        public string County { get; set; }

        /// <summary>
        /// The country of the address
        /// </summary>
        public Country Country { get; set; } = Country.UnitedStates;

        /// <summary>
        /// The zip/postal code of the address
        /// </summary>
        public ZipCode ZipCode { get; set; } = ZipCode.Unknown;

        /// <summary>
        /// ID from source system (not EDW)
        /// </summary>
        public string SourceKey { get; set; }

        /// <summary>
        /// List of phone numbers associated with address
        /// </summary>
        public IEnumerable<PhoneNumber> PhoneNumbers { get; set; }

        /// <summary>
        /// List of fax numbers associated with address
        /// </summary>
        public IEnumerable<PhoneNumber> FaxNumbers { get; set; }

        /// <summary>
        /// List of email addresses associated with the address
        /// </summary>
        public IEnumerable<string> EmailAddresses { get; set; }

        /// <summary>
        /// Determines whether this address has any phone numbers
        /// </summary>
        public bool HasPhoneNumber { get { return PhoneNumbers == default ? false : PhoneNumbers.Any(); } }

        /// <summary>
        /// Determines whether this address has any fax numbers
        /// </summary>
        public bool HasFaxNumber { get { return FaxNumbers == default ? false : FaxNumbers.Any(); } }

        /// <summary>
        /// Determines whether this address has any email addresses
        /// </summary>
        public bool HasEmailAddress { get { return EmailAddresses == default ? false : EmailAddresses.Any(); } }

        /// <summary>
        /// Basic flag to indicate if something is active/void/obsolete/etc, defaults: true
        /// </summary>
        public bool IsActive { get; set; } = true;

        #region IEquatable
        public override bool Equals(object obj)
        {
            return obj is ProviderAddress providerAddress &&
                ForeignDatabaseKey == providerAddress.ForeignDatabaseKey &&
                AddressType == providerAddress.AddressType &&
                Name == providerAddress.Name &&
                Address1 == providerAddress.Address1 &&
                Address2 == providerAddress.Address2 &&
                Address3 == providerAddress.Address3 &&
                City == providerAddress.City &&
                State == providerAddress.State &&
                County == providerAddress.County &&
                Country == providerAddress.Country &&
                ZipCode == providerAddress.ZipCode &&
                SourceKey == providerAddress.SourceKey &&
                PhoneNumbers == providerAddress.PhoneNumbers &&
                FaxNumbers == providerAddress.FaxNumbers &&
                EmailAddresses == providerAddress.EmailAddresses &&
                IsActive == providerAddress.IsActive;
        }

        public bool Equals(ProviderAddress obj)
        {
            return Equals(obj as object);
        }

        public override int GetHashCode()
        {
            var hashCode = -2049519706;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ForeignDatabaseKey);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(AddressType);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Name);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Address1);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Address2);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Address3);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(City);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(State);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(County);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Country);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ZipCode);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(SourceKey);
            hashCode = hashCode * -1521134295 + EqualityComparer<IEnumerable<PhoneNumber>>.Default.GetHashCode(PhoneNumbers);
            hashCode = hashCode * -1521134295 + EqualityComparer<IEnumerable<PhoneNumber>>.Default.GetHashCode(FaxNumbers);
            hashCode = hashCode * -1521134295 + EqualityComparer<IEnumerable<string>>.Default.GetHashCode(EmailAddresses);
            hashCode = hashCode * -1521134295 + EqualityComparer<bool>.Default.GetHashCode(IsActive);
            return hashCode;
        }
        #endregion

    }
}

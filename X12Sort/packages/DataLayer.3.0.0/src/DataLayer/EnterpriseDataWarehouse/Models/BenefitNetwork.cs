using System;
using System.Collections.Generic;
using DataLayer.Base.Models;

namespace DataLayer.EnterpriseDataWarehouse.Models
{
    public class BenefitNetwork : BaseEDWObject, IForeignBaseDataObject, IStartEnd
    {
        /// <summary>
        /// Foreign key relating to the parent provider record
        /// </summary>
        public string ForeignDatabaseKey { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? EffectiveDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? TerminationDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<BenefitPlan> Plans { get; set; }

        #region IStartEnd Implementation
        /// <summary>
        /// 
        /// </summary>
        DateTime? IStartEnd.StartDate
        {
            get { return EffectiveDate; }
            set { EffectiveDate = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        DateTime? IStartEnd.EndDate
        {
            get { return TerminationDate; }
            set { TerminationDate = value; }
        }
        #endregion
    }
}

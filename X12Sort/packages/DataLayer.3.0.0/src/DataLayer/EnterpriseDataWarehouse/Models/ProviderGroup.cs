using DataLayer.Base.Models;
using DataLayer.EnterpriseDataWarehouse.Enumerations;
using System.Collections.Generic;

namespace DataLayer.EnterpriseDataWarehouse.Models
{
    /// <summary>
    /// This is a class for EDW provider groups
    /// </summary>
    public class ProviderGroup : BaseEDWObject, IForeignBaseDataObject, IProviderGroup
    {
        /// <summary>
        /// Foreign key relating to the parent provider record
        /// </summary>
        public string ForeignDatabaseKey { get; set; }

        /// <summary>
        /// This is the best way to identify this provider group as there is no one identifier that ALL provider groups have
        /// </summary>
        public string GroupNumber
        {
            get
            {
                switch (GroupNumberType)
                {
                    case ProviderGroupNumberType.CredentialingID: return CredentialingID;
                    case ProviderGroupNumberType.NPI: return NPI;
                    case ProviderGroupNumberType.TaxID: return TaxID;
                    default: return DatabaseKey;
                }
            }
            set
            {
                switch (GroupNumberType)
                {
                    case ProviderGroupNumberType.CredentialingID: CredentialingID = value; break;
                    case ProviderGroupNumberType.NPI: NPI = value; break;
                    case ProviderGroupNumberType.TaxID: TaxID = value; break;
                    default: DatabaseKey = value; break;
                }
            }
        }

        /// <summary>
        /// The type of ID specified by GroupNumber
        /// </summary>
        public ProviderGroupNumberType GroupNumberType
        {
            get
            {
                // In the hierarchy of provider group IDs, the current credentialing ID is the best identifier, while the EDW database key is the worst
                // This returns the type of the best identifier available for this record
                if (!string.IsNullOrWhiteSpace(CredentialingID)) return ProviderGroupNumberType.CredentialingID;
                else if (!string.IsNullOrWhiteSpace(NPI)) return ProviderGroupNumberType.NPI;
                else if (!string.IsNullOrWhiteSpace(TaxID)) return ProviderGroupNumberType.TaxID;
                else return ProviderGroupNumberType.DatabaseKey;
            }
        }

        /// <summary>
        /// Provider group ID from Cactus
        /// </summary>
        public string CredentialingID { get; set; }

        /// <summary>
        /// Provider group name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Provider group npi
        /// </summary>
        public string NPI { get; set; }

        /// <summary>
        /// Provider group tax id
        /// </summary>
        public string TaxID { get; set; }

        /// <summary>
        /// List of providers belonging to this group
        /// </summary>
        public IEnumerable<Provider> Providers { get; set; }

        /// <summary>
        /// List of addresses belonging to this group
        /// </summary>
        public IEnumerable<ProviderAddress> Addresses { get; set; }

    }
}

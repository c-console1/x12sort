using DataLayer.Base.Models;
using System;

namespace DataLayer.EnterpriseDataWarehouse.Models
{
    /// <summary>
    /// This is a class for cactus provider entity assignments / contracts
    /// </summary>
    public class ProviderEntityAssignment : BaseEDWObject, IForeignBaseDataObject, IStartEnd
    {
        /// <summary>
        /// Foreign key relating to the parent provider record
        /// </summary>
        public string ForeignDatabaseKey { get; set; }
        
        /// <summary>
        /// Record type. E = the provider, A = a contract
        /// </summary>
        public string RecordType { get; set; }

        /// <summary>
        /// Contract assignment (advantage, elite, etc)
        /// </summary>
        public string Assignment { get; set; }

        /// <summary>
        /// Contract status
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Contract category
        /// </summary>
        public string Category { get; set; }

        /// <summary>
        /// Contract Category Code
        /// </summary>
        public string CategoryCode { get; set; }

        /// <summary>
        /// Provider ID from Health Rules
        /// </summary>
        public string HealthRulesProviderID { get; set; }

        /// <summary>
        /// Provider ID from Amisys
        /// </summary>
        public string AmisysProviderID { get; set; }

        /// <summary>
        /// Contract start date
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Contract end date
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Record key from source system
        /// </summary>
        public string SourceKey { get; set; }
    }

}

using DataLayer.Base.Models;
using System.Collections.Generic;

namespace DataLayer.EnterpriseDataWarehouse.Models
{
    /// <summary>
    /// This is a crossreference for provider groups to provider records (internal only)
    /// </summary>
    internal class ProviderGroupToProvider
    {
        /// <summary>
        /// Key for the group record in the database
        /// </summary>
        public string GroupDatabaseKey { get; set; }

        /// <summary>
        /// Provider group credentialing ID
        /// </summary>
        public string GroupCredentialingID { get; set; }

        /// <summary>
        /// Key for the provider record in the database
        /// </summary>
        public string ProviderDatabaseKey { get; set; }

        /// <summary>
        /// Provider credentialing ID
        /// </summary>
        public string ProviderCredentialingID { get; set; }
    }
}

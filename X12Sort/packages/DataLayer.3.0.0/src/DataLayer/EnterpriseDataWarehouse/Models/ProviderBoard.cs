using System;
using DataLayer.Base.Models;

namespace DataLayer.EnterpriseDataWarehouse.Models
{
    /// <summary>
    /// This is a class for provider certifying boards
    /// </summary>
    public class ProviderBoard : BaseEDWObject, IForeignBaseDataObject
    {
        /// <summary>
        /// Foreign key relating to the parent provider record
        /// </summary>
        public string ForeignDatabaseKey { get; set; }
        
        /// <summary>
        /// Board description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Certification status
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Certification number
        /// </summary>
        public string CertificationNumber { get; set; }

        /// <summary>
        /// Recertification date
        /// </summary>
        public DateTime RecertificationDate { get; set; }

        /// <summary>
        /// Reverification date
        /// </summary>
        public DateTime ReverificationDate { get; set; }

        /// <summary>
        /// Is this certification good for life?
        /// </summary>
        public bool IsLifetimeCertification { get; set; }

        /// <summary>
        /// Certification start date
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Certification end date
        /// </summary>
        public DateTime? EndDate { get; set; }
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.EnterpriseDataWarehouse.Models
{
    public class PCPProvider : Provider
    {
        /// <summary>
        /// MEMBER DIM KEY
        /// </summary>
        public string MEMBER_DIM_KEY { get; set; }

        /// <summary>
        /// Date when PCP became effective
        /// </summary>
        public DateTime? PCPEffectiveDate { get; set; }
    }
}

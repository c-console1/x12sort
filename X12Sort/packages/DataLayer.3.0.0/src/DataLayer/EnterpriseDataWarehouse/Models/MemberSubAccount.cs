using System;
using System.Collections.Generic;
using System.Text;
using DataLayer.Base.Models;

namespace DataLayer.EnterpriseDataWarehouse.Models
{
    public class MemberSubAccount : MemberAddress
    {
        #region MEMBER_DIM Primary and Foreign Keys
        /// <summary>
        /// Primary key of ACCOUNT_DIM table
        /// </summary>

#pragma warning disable CA1707 // Identifiers should not contain underscores
        public string ACCOUNT_DIM_KEY { get; set; }
        /// <summary>
        /// Foreign key of ACCOUNT_DIM table to ADDRESS_DIM table
        /// </summary>
        public string ACCOUNT_ADDRESS_DIM_KEY { get; set; }
#pragma warning restore CA1707 // Identifiers should not contain underscores
        #endregion
        /// <summary>
        /// Account number (ACCOUNT_DIM.ACCOUNT_HCC_ID)
        /// </summary>
        public string AccountNumber { get; set; }
        /// <summary>
        /// Account number (ACCOUNT_DIM.ACCOUNT_HCC_ID)
        /// If ACCOUNT_LEVEL=0 and Parent_ACCOUNT_NAMES IS NULL
        /// </summary>
        public string SubAccountNumber { get; set; }


        /// <summary>
        /// SubAccount phone (ACCOUNT_CONTACT_DETAILS.TELEPHONE_NUMBER)
        /// </summary>
        public string Phone { get; set; }

    }
}

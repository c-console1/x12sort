using DataLayer.Base;
using DataLayer.EnterpriseDataWarehouse.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DataLayer.EnterpriseDataWarehouse
{
    public class AccountRepository : BaseRepository<EnterpriseDataWarehouseDatabase>
    {
        //MemberSQLQueries sqls = new MemberSQLQueries();
        internal AccountRepository(EnterpriseDataWarehouseDatabase database) : base(database)
        {
        }

        /// <summary>
        /// Returns an enumerable collection of ALL ACCOUNT in the EDW system.
        /// </summary>
        /// <returns>Returned as an unbuffered results, means that the list is populated from database on demand. Calling `ToList()` will load the entire list from the database</returns>
        public IEnumerable<MemberAccount> GetAccounts()
        {
            return GetAllAccounts();
        }

        /// <summary>
        /// Gets a single account from the account number
        /// </summary>
        /// <param name="accountNumber">The account number for the individual</param>
        /// <returns>A member object</returns>
        public MemberAccount GetAccount(string accountNumber)
        {
            IEnumerable<MemberAccount> accounts = GetAllAccounts(accountNumber);
            if (accounts.Count() > 1)
            {
                throw new Exception($"The query returned multiple results: {accounts.Count()} accounts found");
            }
            else if (accounts.Any())
            {
                return null;
            }
            else
            {
                return accounts.FirstOrDefault();
            }
        }


        /// <summary>
        /// Returns an enumerable collection of ALL SubAccounts in the EDW system.
        /// </summary>
        /// <returns>Returned as an unbuffered results, means that the list is populated from database on demand. Calling `ToList()` will load the entire list from the database</returns>
        public IEnumerable<MemberSubAccount> GetSubAccounts()
        {
            return GetAllSubAccounts();
        }


        /// <summary>
        /// GET SUB-ACCOUNT
        /// </summary>
        /// <returns></returns>
        public MemberSubAccount GetSubAccount(string subAccountNumber)
        {

            IEnumerable<MemberSubAccount> subaccounts = GetAllSubAccounts(subAccountNumber);
            if (subaccounts.Count() > 1)
            {
                throw new Exception($"The query returned multiple results: {subaccounts.Count()} accounts found");
            }
            else if (subaccounts.Any())
            {
                return null;
            }
            else
            {
                return subaccounts.FirstOrDefault();
            }
        }




        #region Base Methods

        /// <summary>
        /// Get all account #'s
        /// Mirrors a Connector call
        /// </summary>
        /// <returns>List of HCC account #'s</returns>
        public IEnumerable<string> GetAllAccountNumbers()
        {
            string query = @"
                SELECT  a.ACCOUNT_HCC_ID AS AccountNumber
                FROM ACCOUNT_DIM a
                WHERE [LEVEL] =0 ";

            var results = Database.Query<string>(query);
            return results;
        }

        /// <summary>
        /// GET ALL ACCOUNTS INFORMATION
        /// </summary>
        /// <param name="accountNumber"></param>
        /// <returns></returns>
        private IEnumerable<MemberAccount> GetAllAccounts(string accountNumber = null)
        {
            string query = @"
                            SELECT   a.ACCOUNT_DIM_KEY         AS ACCOUNT_DIM_KEY
                                    ,a.ACCOUNT_ADDRESS_DIM_KEY AS ACCOUNT_ADDRESS_DIM_KEY  
                                    ,a.ACCOUNT_HCC_ID          AS AccountNumber 
                                    ,a.[NAME]                  AS AccountName
	                                ,b.ADDRESS_LINE            AS Address1
	                                ,b.ADDRESS_LINE_2          AS Address2
	                                ,b.CITY                    AS City
	                                ,b.[STATE]                 AS [State]
	                                ,b.ZIP_CODE                AS ZipCode
	                                ,c.TELEPHONE_NUMBER        AS Phone
                            FROM ACCOUNT_DIM a
                            INNER JOIN ADDRESS_DIM b
	                            ON a.ACCOUNT_ADDRESS_DIM_KEY = b.ADDRESS_DIM_KEY
                            LEFT JOIN ACCOUNT_CONTACT_DETAILS c
	                            ON a.ACCOUNT_DIM_KEY = c.ACCOUNT_DIM_KEY
                            WHERE [LEVEL] =0 
                            /*AND PARENT_ACCOUNT_NAMES IS NULL*/ ";

            if (accountNumber != null)
            {
                query += $" AND RTRIM(a.ACCOUNT_HCC_ID) = @accountNumber";
            }

            return Database.Query<MemberAccount>(query, new { accountNumber = accountNumber });
        }

        /// <summary>
        /// Get all sub-account,
        /// Mirroes function in Connector
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetAllSubAccountsNumbers()
        {
            string sql = @"
                SELECT  DISTINCT aa.SubAccountNumber
                FROM (
		                SELECT LEFT(a.ACCOUNT_HCC_ID,CHARINDEX('-', a.ACCOUNT_HCC_ID)-1)  AS AccountNumber
				                ,a.ACCOUNT_DIM_KEY                                         AS ACCOUNT_DIM_KEY  
                                ,a.ACCOUNT_ADDRESS_DIM_KEY                                 AS ACCOUNT_ADDRESS_DIM_KEY
			                    ,a.ACCOUNT_HCC_ID                                         AS SubAccountNumber
			                    ,a.[NAME]                                                 AS [Name]
			                    ,CASE
				                    WHEN ACCOUNT_ADDRESS_DIM_KEY=-1 
				                    THEN (SELECT ACCOUNT_ADDRESS_DIM_KEY FROM ACCOUNT_DIM WHERE ACCOUNT_HCC_ID =LEFT(a.ACCOUNT_HCC_ID,CHARINDEX('-', a.ACCOUNT_HCC_ID)-1))
				                    ELSE ACCOUNT_ADDRESS_DIM_KEY
			                    END                                                     AS ADDRESS_KEY
				                ,c.TELEPHONE_NUMBER                                    AS Phone  
		                FROM ACCOUNT_DIM a
		                LEFT JOIN ACCOUNT_CONTACT_DETAILS c
			                ON a.ACCOUNT_DIM_KEY = c.ACCOUNT_DIM_KEY
		                WHERE a.ACCOUNT_HCC_ID LIKE '%-%'
                ) aa";

            IEnumerable<string> output = Database.Query<string>(sql);

            return output;

        }

        /// <summary>
        /// GET ALL SUB ACCOUNTS INFORMATION 
        /// </summary>
        /// <param name="subAccountNumber"></param>
        /// <returns></returns>
        private IEnumerable<MemberSubAccount> GetAllSubAccounts(string subAccountNumber = null)
        {
            string query = @"
                             /*
                               SELECT AccountNumber  LEFT '-' OF SUB-ACCOUNT
                               IF SUB-ACCOUNT FORMAT CHANGE NEEDS TO CHANGE THE SUB-QUERY

                               *** ADDRESS SELECTION CRITERIA ***

                                       IF IN ACCOUNT_DIM TABLE ACCOUNT_ADDRESS_DIM_KEY = -1  (SUB-ACCOUNT DO NOT HAVE ACCOUNT)
		                               THEN 
		                                  GET ADDRESS OF PARENT ACCOUNT
		                               ELSE 
		                                  GET THE ADDRESS OF SUB-ACCOUNT	  	    
                            */
                                SELECT   aa.ACCOUNT_DIM_KEY
                                        ,aa.ACCOUNT_ADDRESS_DIM_KEY
                                        ,aa.AccountNumber
                                        ,aa.SubAccountNumber
	                                    ,aa.[Name]
	                                    ,aa.Phone
	                                    ,b.ADDRESS_LINE                 AS Address1
	                                    ,b.ADDRESS_LINE_2               AS Address2
	                                    ,b.CITY                         AS City
	                                    ,b.[STATE]                      AS [State]
	                                    ,b.ZIP_CODE                     AS ZipCode 
                                FROM (
		                                SELECT LEFT(a.ACCOUNT_HCC_ID,CHARINDEX('-', a.ACCOUNT_HCC_ID)-1)  AS AccountNumber
				                                ,a.ACCOUNT_DIM_KEY                                         AS ACCOUNT_DIM_KEY  
                                                ,a.ACCOUNT_ADDRESS_DIM_KEY                                 AS ACCOUNT_ADDRESS_DIM_KEY
			                                    ,a.ACCOUNT_HCC_ID                                         AS SubAccountNumber
			                                    ,a.[NAME]                                                 AS [Name]
			                                    ,CASE
				                                    WHEN ACCOUNT_ADDRESS_DIM_KEY=-1 
				                                    THEN (SELECT ACCOUNT_ADDRESS_DIM_KEY FROM ACCOUNT_DIM WHERE ACCOUNT_HCC_ID =LEFT(a.ACCOUNT_HCC_ID,CHARINDEX('-', a.ACCOUNT_HCC_ID)-1))
				                                    ELSE ACCOUNT_ADDRESS_DIM_KEY
			                                    END                                                     AS ADDRESS_KEY
				                                ,c.TELEPHONE_NUMBER                                    AS Phone  
		                                FROM ACCOUNT_DIM a
		                                LEFT JOIN ACCOUNT_CONTACT_DETAILS c
			                                ON a.ACCOUNT_DIM_KEY = c.ACCOUNT_DIM_KEY
		                                WHERE a.ACCOUNT_HCC_ID LIKE '%-%'
                                ) aa
                                LEFT JOIN ADDRESS_DIM b
	                                ON aa.ADDRESS_KEY = b.ADDRESS_DIM_KEY";

            if (subAccountNumber != null)
            {
                query += " WHERE aa.SubAccountNumber =@subAccountNumber";
            }

            return Database.Query<MemberSubAccount>(query, new { subAccountNumber = subAccountNumber });
        }


        #endregion


    }
}

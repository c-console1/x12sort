using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer.Base;
using DataLayer.EnterpriseDataWarehouse.Enumerations;
using DataLayer.EnterpriseDataWarehouse.Models;
using DataLayer.StaticUtilities.Extensions;

namespace DataLayer.EnterpriseDataWarehouse
{
    public class BenefitRepository : BaseRepository<EnterpriseDataWarehouseDatabase>
    {
        const bool DEFAULT_ERROR_ON_MULTIPLE = true;

        internal BenefitRepository(EnterpriseDataWarehouseDatabase database) : base(database)
        {

        }

        ///////////////////////////////////////////////////////////////////////////////////////////

        #region Public Benefit Plan Methods
        /// <summary>
        /// Get a single benefit plan from the EDW by ID
        /// </summary>
        /// <param name="id">Benefit plan ID</param>
        /// <param name="errorOnMultiple">Whether the method should generate an error when multiple records are returned</param>
        /// <returns>Single benefit plan record</returns>
        public BenefitPlan GetBenefitPlanByID(string id, bool errorOnMultiple = DEFAULT_ERROR_ON_MULTIPLE)
        {
            return GetBenefitPlan(
                new Dictionary<string, object>()
                {
                    { "id", id }
                },
                errorOnMultiple
                );
        }

        /// <summary>
        /// Get a collection of benefit plans from the EDW using a list of IDs
        /// </summary>
        /// <param name="ids">Collection of benefit plan IDs</param>
        /// <returns>Collection of benefit plan records</returns>
        public IEnumerable<BenefitPlan> GetBenefitPlansByIDs(IEnumerable<string> ids)
        {
            return GetBenefitPlans(
                new Dictionary<string, object>()
                {
                    { "ids", ids }
                });
        }

        /// <summary>
        /// Get a single benefit plan from the EDW by name
        /// </summary>
        /// <param name="name">Benefit plan name</param>
        /// <param name="errorOnMultiple">Whether the method should generate an error when multiple records are returned</param>
        /// <returns>Single benefit plan record</returns>
        public BenefitPlan GetBenefitPlanByName(string name, bool errorOnMultiple = DEFAULT_ERROR_ON_MULTIPLE)
        {
            return GetBenefitPlan(
                new Dictionary<string, object>()
                {
                    { "name", name }
                },
                errorOnMultiple
                );
        }

        /// <summary>
        /// Get a collection of benefit plans from the EDW using a list of benefit network keys
        /// </summary>
        /// <param name="networkKeys">Collection of benefit network keys</param>
        /// <returns>Collection of benefit plan records</returns>
        public IEnumerable<BenefitPlan> GetBenefitPlansByNetworkDatabaseKeys(IEnumerable<string> networkKeys)
        {
            return GetBenefitPlans(
                new Dictionary<string, object>()
                {
                    { "networkKeys", networkKeys }
                });
        }

        /// <summary>
        /// Gets a collection of benefit plans from the EDW, with no filtering applied
        /// </summary>
        /// <returns>Collection of benefit plan records</returns>
        public IEnumerable<BenefitPlan> GetBenefitPlansUnfiltered()
        {
            return GetBenefitPlans(null);
        }
        #endregion

        ///////////////////////////////////////////////////////////////////////////////////////////

        #region Public Benefit Network Methods
        /// <summary>
        /// Get a single benefit network from the EDW by ID
        /// </summary>
        /// <param name="id">Benefit network ID</param>
        /// <param name="errorOnMultiple">Whether the method should generate an error when multiple records are returned</param>
        /// <returns>Single benefit network record</returns>
        public BenefitNetwork GetBenefitNetworkByID(string id, IEnumerable<BenefitNetworkComponent> includedComponents = null, bool errorOnMultiple = DEFAULT_ERROR_ON_MULTIPLE)
        {
            return GetBenefitNetwork(
                new Dictionary<string, object>()
                {
                    { "id", id }
                },
                includedComponents,
                errorOnMultiple
                );
        }

        /// <summary>
        /// Get a collection of benefit networks from the EDW using a list of IDs
        /// </summary>
        /// <param name="ids">Collection of benefit network IDs</param>
        /// <returns>Collection of benefit network records</returns>
        public IEnumerable<BenefitNetwork> GetBenefitNetworksByIDs(IEnumerable<string> ids, IEnumerable<BenefitNetworkComponent> includedComponents = null)
        {
            return GetBenefitNetworks(
                new Dictionary<string, object>()
                {
                    { "ids", ids }
                },
                includedComponents
                );
        }

        /// <summary>
        /// Get a single benefit network from the EDW by name
        /// </summary>
        /// <param name="name">Benefit network name</param>
        /// <param name="errorOnMultiple">Whether the method should generate an error when multiple records are returned</param>
        /// <returns>Single benefit network record</returns>
        public BenefitNetwork GetBenefitNetworkByName(string name, IEnumerable<BenefitNetworkComponent> includedComponents = null, bool errorOnMultiple = DEFAULT_ERROR_ON_MULTIPLE)
        {
            return GetBenefitNetwork(
                new Dictionary<string, object>()
                {
                    { "name", name }
                },
                includedComponents,
                errorOnMultiple
                );
        }

        /// <summary>
        /// Gets a collection of benefit networks from the EDW, with no filtering applied
        /// </summary>
        /// <returns>Collection of benefit network records</returns>
        public IEnumerable<BenefitNetwork> GetBenefitNetworksUnfiltered(IEnumerable<BenefitNetworkComponent> includedComponents = null)
        {
            return GetBenefitNetworks(null, includedComponents);
        }
        #endregion

        ///////////////////////////////////////////////////////////////////////////////////////////

        #region Internal Benefit Component Methods
        /// <summary>
        /// Add components to a list of benefit networks
        /// </summary>
        /// <param name="networks">List of benefit networks to add components to</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        internal void AddComponentsToBenefitNetworks(IEnumerable<BenefitNetwork> networks, IEnumerable<BenefitNetworkComponent> includedComponents)
        {
            if (includedComponents != null)
            {
                IEnumerable<string> netDatabaseKeys = networks.Select(g => g.DatabaseKey);
                foreach (BenefitNetworkComponent component in includedComponents)
                    switch (component)
                    {
                        case BenefitNetworkComponent.Plans:
                            IEnumerable<BenefitPlan> plans = Database.Benefits.GetBenefitPlansByNetworkDatabaseKeys(netDatabaseKeys);
                            foreach (BenefitNetwork network in networks)
                                network.Plans = plans.Where(p => p.ForeignDatabaseKey == network.DatabaseKey);
                            break;
                        default:
                            throw new ArgumentException($"The included component is not valid: {component}", "includedComponents");
                    }
            }

        }
        #endregion

        ///////////////////////////////////////////////////////////////////////////////////////////

        #region Private Methods

        #region Get Benefit Plan(s) - Base Method
        /// <summary>
        /// Get a single benefit plan from the EDW
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <param name="errorOnMultiple">Whether the method should generate an error when multiple records are returned</param>
        /// <returns>Single benefit plan record</returns>
        private BenefitPlan GetBenefitPlan(Dictionary<string, object> criteria, bool errorOnMultiple)
        {
            IEnumerable<BenefitPlan> plans = GetBenefitPlans(criteria);
            if (plans.Count() > 1 && errorOnMultiple)
                throw new Exception($"The query returned multiple results: {plans.Count()} benefit plans found");
            else if (plans.Any())
                return plans.First();
            else
                return null;
        }

        /// <summary>
        /// Get a collection of benefit plans from the EDW
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <returns>Collection of benefit plan records</returns>
        private IEnumerable<BenefitPlan> GetBenefitPlans(Dictionary<string, object> criteria)
        {
            StringBuilder sql = new StringBuilder();
            Dictionary<string, object> param = new Dictionary<string, object>();
            Dictionary<string, IEnumerable<object>> filterTables = new Dictionary<string, IEnumerable<object>>();

            // Build sql to get benefit plans
            sql.AppendLine(@"
select bp.[BENEFIT_PLAN_KEY]     as [DatabaseKey]
      ,bn_sub.[NetworkID]        as [ForeignDatabaseKey]
      ,bp.[BENEFIT_PLAN_NAME]    as [Name]
      ,bp.[BENEFIT_PLAN_DESC]    as [Description]
      ,bp.[BENEFIT_PLAN_STATUS]  as [Status]
      ,bp_eff_date.[DATE_VALUE]  as [EffectiveDate]
      ,bp_term_date.[DATE_VALUE] as [TerminationDate]
  from [landing].[BENEFIT_PLAN_HISTORY_FACT] bp
  left join [landing].[DATE_DIMENSION] bp_eff_date
    on bp_eff_date.[DATE_KEY] = bp.[PLAN_EFFECTIVE_DATE_KEY]
  left join [landing].[DATE_DIMENSION] bp_term_date
    on bp_term_date.[DATE_KEY] = bp.[PLAN_TERMINATION_DATE_KEY]
  join
      (
         select distinct
                bp_sub.[BENEFIT_PLAN_KEY] as [ProductID]
               ,bp_sub.[NAME]             as [ProductName]
               ,bn.[BENEFIT_NETWORK_KEY]  as [NetworkID]
               ,bn.[BENEFIT_NETWORK_NAME] as [NetworkName]
           from [dbo].[BENEFIT_PLAN_DIM] bp_sub
           join [landing].[MEMBER_PLAN_SEL_HIST_FACT] mem_bp
             on mem_bp.[BENEFIT_PLAN_KEY] = bp_sub.[BENEFIT_PLAN_KEY]
           join [landing].[MEMBER_PLAN_SEL_TO_BNFT_NTWRK] mem_bn
             on mem_bn.[MEMBER_PLAN_SEL_HIST_FACT_KEY] = mem_bp.[MEMBER_PLAN_SEL_HIST_FACT_KEY]
           join [landing].[BENEFIT_NETWORK_HISTORY_FACT] bn
             on bn.[BENEFIT_NETWORK_KEY] = mem_bn.[BENEFIT_NETWORK_KEY]
      ) bn_sub
    on bn_sub.[ProductID] = bp.[BENEFIT_PLAN_KEY]
");

            #region Add limiting criteria

            #region Add join statements
            if (criteria != null)
            {
                string tableName;
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "ids":
                            tableName = Database.GenerateTempTableName($"benplan_{key}");
                            filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                            sql.AppendLine($"  join {tableName} tmp_{key} on bp.[BENEFIT_PLAN_KEY] = tmp_{key}.[value]");
                            break;
                        case "networkKeys":
                            tableName = Database.GenerateTempTableName($"benplannet_{key}");
                            filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                            sql.AppendLine($"  join {tableName} tmp_{key} on bn_sub.[NetworkID] = tmp_{key}.[value]");
                            break;
                    }
                }
            }
            #endregion

            sql.AppendLine(" where 1 = 1");

            #region Add where clauses
            if (criteria != null)
            {
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "id":
                            param.Add(key, criteria[key]);
                            sql.AppendLine("   and bp.[BENEFIT_PLAN_KEY] = @id");
                            break;
                        case "name":
                            param.Add(key, criteria[key]);
                            sql.AppendLine("   and bp.[NAME] = @name");
                            break;
                    }
                }
            }
            #endregion

            #endregion

            // Get benefit plans
            return Database.QueryWithFilters<BenefitPlan>(sql.ToString(), param.ToQueryParamObject(), filterTables);
        }
        #endregion

        #region Get Benefit Network(s) - Base Method
        /// <summary>
        /// Get a single benefit network from the EDW
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <param name="errorOnMultiple">Whether the method should generate an error when multiple records are returned</param>
        /// <returns>Single benefit network record</returns>
        private BenefitNetwork GetBenefitNetwork(Dictionary<string, object> criteria, IEnumerable<BenefitNetworkComponent> includedComponents, bool errorOnMultiple)
        {
            IEnumerable<BenefitNetwork> networks = GetBenefitNetworks(criteria, includedComponents);
            if (networks.Count() > 1 && errorOnMultiple)
                throw new Exception($"The query returned multiple results: {networks.Count()} benefit networks found");
            else if (networks.Any())
                return networks.First();
            else
                return null;
        }

        /// <summary>
        /// Get a collection of benefit networks from the EDW
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <returns>Collection of benefit network records</returns>
        private IEnumerable<BenefitNetwork> GetBenefitNetworks(Dictionary<string, object> criteria, IEnumerable<BenefitNetworkComponent> includedComponents)
        {
            StringBuilder sql = new StringBuilder();
            Dictionary<string, object> param = new Dictionary<string, object>();
            Dictionary<string, IEnumerable<object>> filterTables = new Dictionary<string, IEnumerable<object>>();

            // Build sql to get benefit networks
            sql.AppendLine(@"
select bn.[BENEFIT_NETWORK_KEY]    as [DatabaseKey]
      ,bn.[BENEFIT_NETWORK_NAME]   as [Name]
      ,bn.[BENEFIT_NETWORK_DESC]   as [Description]
      ,bn.[BENEFIT_NETWORK_STATUS] as [Status]
      ,bn_eff_date.[DATE_VALUE]    as [EffectiveDate]
      ,bn_term_date.[DATE_VALUE]   as [TerminationDate]
  from [landing].[BENEFIT_NETWORK_HISTORY_FACT] bn
  left join [landing].[DATE_DIMENSION] bn_eff_date
    on bn_eff_date.[DATE_KEY] = bn.[BENEFIT_NETWORK_EFF_DATE_KEY]
  left join [landing].[DATE_DIMENSION] bn_term_date
    on bn_term_date.[DATE_KEY] = bn.[BENEFIT_NETWORK_TERM_DATE_KEY]
");

            #region Add limiting criteria

            #region Add join statements
            if (criteria != null)
            {
                string tableName;
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "ids":
                            tableName = Database.GenerateTempTableName($"bennetwork_{key}");
                            filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                            sql.AppendLine($"  join {tableName} tmp_{key} on bn.[BENEFIT_NETWORK_KEY] = tmp_{key}.[value]");
                            break;
                    }
                }
            }
            #endregion

            sql.AppendLine(" where 1 = 1");

            #region Add where clauses
            if (criteria != null)
            {
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "id":
                            param.Add(key, criteria[key]);
                            sql.AppendLine("   and bn.[BENEFIT_NETWORK_KEY] = @id");
                            break;
                        case "name":
                            param.Add(key, criteria[key]);
                            sql.AppendLine("   and bn.[NAME] = @name");
                            break;
                    }
                }
            }
            #endregion

            #endregion

            // Get benefit networks
            IEnumerable<BenefitNetwork> networks = Database.QueryWithFilters<BenefitNetwork>(sql.ToString(), param.ToQueryParamObject(), filterTables);

            // Include other components
            AddComponentsToBenefitNetworks(networks, includedComponents);

            return networks;
        }
        #endregion

        #endregion
    }
}

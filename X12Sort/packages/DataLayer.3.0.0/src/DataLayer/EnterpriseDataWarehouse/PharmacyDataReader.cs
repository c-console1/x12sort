using System.Collections.Generic;
using DataLayer.Base;
using DataLayer.EnterpriseDataWarehouse.Models;

namespace DataLayer.EnterpriseDataWarehouse
{
    public class PharmacyDataReader : BaseDataReader<EnterpriseDataWarehouseDatabase, Pharmacy>
    {
        #region Constructors
        /// <summary>
        /// Standard Constructor
        /// </summary>
        /// <param name="database">Reference to the database where the data is coming from</param>
        /// <param name="sql">SQL string to get data from the database</param>
        /// <param name="filterTables">List of temp filter table names with corresponding lists of values</param>
        internal PharmacyDataReader(EnterpriseDataWarehouseDatabase database, string sql, object param, Dictionary<string, IEnumerable<object>> filterTables)
            : base (database, sql, param, filterTables)
        {
            
        }
        #endregion
    }
}

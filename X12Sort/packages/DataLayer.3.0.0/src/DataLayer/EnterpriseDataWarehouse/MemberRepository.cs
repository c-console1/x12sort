using DataLayer.Base;
using DataLayer.Base.Models;
using DataLayer.EnterpriseDataWarehouse.Enumerations;
using DataLayer.EnterpriseDataWarehouse.Models;
using DataLayer.StaticUtilities.Extensions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;

// TODO: Summary blocks for various methods
// TODO: May need to do the "temp table" thing that's in providers to avoid performance issues.
// TODO: Adjust interface to expose "currentOnly", "asOfDate", and "historySince" -- default is the first one - other two take a date.
//          For now, define the calling sequence and don't worry about implementing it.

namespace DataLayer.EnterpriseDataWarehouse
{
    public class MemberRepository : BaseRepository<EnterpriseDataWarehouseDatabase>
    {
        internal MemberRepository(EnterpriseDataWarehouseDatabase database) : base(database)
        {
        }

        #region Public Methods

        #region Get members
        /// <summary>
        /// Get all members from the EDW
        /// </summary>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Collection of member records</returns>
        public IEnumerable<Member> GetMembersAll(IEnumerable<MemberComponent> includedComponents = null)
        {
            return GetMembers(null, includedComponents);
        }

        /// <summary>
        /// Get a collection of members from the EDW, automatically include span data
        /// </summary>
        /// <param name="memberNumber">Member number to search for</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Collection of member records</returns>
        public IEnumerable<Member> GetMembersWithSpans(IEnumerable<MemberComponent> includedComponents = null, Dictionary<string, object> criteria = null)
        {
            if (includedComponents == null || !includedComponents.Contains(MemberComponent.MemberSpan))
            {
                var actualIncludes = includedComponents == null
                    ? new List<MemberComponent>()
                    : new List<MemberComponent>(includedComponents);
                actualIncludes.Add(MemberComponent.MemberSpan);
                includedComponents = actualIncludes;
            }

            return GetMembers(criteria, includedComponents);
        }

        /// <summary>
        /// Get a member from the EDW
        /// </summary>
        /// <param name="memberNumber">Member number to search for</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Single member record</returns>
        public Member GetMemberWithSpansByNumber(string memberNumber, IEnumerable<MemberComponent> includedComponents = null)
        {
            if (includedComponents == null || !includedComponents.Contains(MemberComponent.MemberSpan))
            {
                var actualIncludes = includedComponents == null
                    ? new List<MemberComponent>()
                    : new List<MemberComponent>(includedComponents);
                actualIncludes.Add(MemberComponent.MemberSpan);
                includedComponents = actualIncludes;
            }

            Member member = GetMember(
                new Dictionary<string, object>()
                {
                    { "memberNumber", memberNumber }
                },
                includedComponents,
                true
                );

            return member;
        }

        /// <summary>
        /// Get a member from the EDW
        /// </summary>
        /// <param name="memberNumber">Member number to search for</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Single member record</returns>
        public Member GetMemberByMemberNumber(string memberNumber, IEnumerable<MemberComponent> includedComponents = null)
        {
            Member member = GetMember(
                new Dictionary<string, object>()
                {
                    { "memberNumber", memberNumber }
                },
                includedComponents,
                true
                );

            return member;
        }

        /// <summary>
        /// Get a collection of members from the EDW
        /// </summary>
        /// <param name="memberNumbers">Member numbers to search for</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Collection of member pcp records</returns>
        public IEnumerable<Member> GetMembersByMemberNumbers(IEnumerable<string> memberNumbers, IEnumerable<MemberComponent> includedComponents = null)
        {
            Dictionary<string, object> dictionary = new Dictionary<string, object>();
            dictionary.Add("memberNumbers", memberNumbers);

            IEnumerable<Member> members = GetMembers(
                new Dictionary<string, object>()
                {
                    { "memberNumbers", memberNumbers }
                }
                , includedComponents
                );


            return members;
        }
        #endregion

        #region SearchMembers
        /// <summary>
        /// Search for members
        /// </summary>
        /// <param name="msp">Member search parameter</param>
        /// <returns></returns>
        public IEnumerable<Member> SearchMembers(MemberSearchParam msp)
        {
            Dictionary<string, object> criteria = ProcessSearchCriteria(msp);

            IEnumerable<Member> result = GetMembers(criteria, includedComponents: null);            // includedComponents: null for now

            return result;
        }

        /// <summary>
        /// Search for members
        /// </summary>
        /// <param name="mspList">List of member search parameters</param>
        /// <returns></returns>
        public IEnumerable<Member> SearchMembers(IEnumerable<MemberSearchParam> mspList)
        {
            List<Dictionary<string, object>> criteria = new List<Dictionary<string, object>>();

            if (mspList != null && mspList.Any()) {
                foreach (var msp in mspList) {
                    Dictionary<string, object> criterion = ProcessSearchCriteria(msp);
                    criteria.Add(criterion);
                }
            }

            if (criteria.Count == 0) { criteria = null; }
            IEnumerable<Member> result = GetMembersMulti(criteria, includedComponents: null);            // includedComponents: null for now

            return result;
        }
        #endregion

        #region Get subscribers
        /// <summary>
        /// Get a collection of subscribers from the EDW
        /// </summary>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Collection of subscriber records</returns>
        public IEnumerable<Member> GetSubscribers(IEnumerable<MemberComponent> includedComponents = null)
        {
            IEnumerable<Member> result = GetMembers(
                new Dictionary<string, object>
                {
                    { "Subscriber", "Self" }
                },
                includedComponents
                );

            return result;
        }

        /// <summary>
        /// Get a subscriber from the EDW
        /// </summary>
        /// <param name="subscriptionNumber">Subscriber number to search for</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Collection of member pcp records</returns>
        public IEnumerable<Member> GetSubscriberBySubscriptionNumber(string subscriptionNumber, IEnumerable<MemberComponent> includedComponents = null)
        {
            IEnumerable<Member> subscribers = GetMembers(
                new Dictionary<string, object>
                {
                    { "Subscriber", "Self" },
                    { "SubscriptionNumber", subscriptionNumber }
                },
                includedComponents
                );
            return subscribers;
        }
        #endregion

        #region Get member addresses
        /// <summary>
        /// Get a collection of member addresses from the EDW
        /// </summary>
        /// <param name="ids">Limiting criteria for the query</param>
        /// <returns>Collection of member addresses records</returns>
        private IEnumerable<MemberAddress> GetMemberAddressesByIDs(IEnumerable<string> ids)
        {
            return GetMemberAddresses(
               new Dictionary<string, object>()
               {
                    { "ids", ids }
               });
        }
        #endregion

        #region Get member spans
        /// <summary>
        /// Get a collection of member spans from the EDW
        /// </summary>
        /// <param name="ids">Limiting criteria for the query</param>
        /// <returns>Collection of member span records</returns>
        private IEnumerable<MemberSpan> GetMemberSpansByIDs(IEnumerable<string> ids)
        {
            return GetMemberSpans(
               new Dictionary<string, object>()
               {
                    { "ids", ids }
               });
        }
        #endregion

        #region Get member pcp
        /// <summary>
        /// Get a collection of member PCPs from the EDW
        /// </summary>
        /// <param name="ids">Limiting criteria for the query</param>
        /// <returns>Collection of member pcp records</returns>
        private IEnumerable<PCPProvider> GetMemberPCPByIDs(IEnumerable<string> ids)
        {
            return GetMemberPCPs(
               new Dictionary<string, object>()
               {
                    { "ids", ids }
               });
        }
        #endregion

        #endregion Public Methods

        #region Internal Member Component Methods
        /// <summary>
        /// Add components to a list of members
        /// </summary>
        /// <param name="members">List of members to add components to</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        internal void AddComponentsToMembers(IEnumerable<Member> members, IEnumerable<MemberComponent> includedComponents)
        {
            if (includedComponents != null && includedComponents.Any())
            {
                if (includedComponents != null)
                {
                    IEnumerable<string> ids = members.Select(p => p.MEMBER_DIM_KEY);
                    foreach (MemberComponent component in includedComponents)
                        switch (component)
                        {
                            case MemberComponent.Addresses:
                                IEnumerable<MemberAddress> addresses = Database.Members.GetMemberAddressesByIDs(ids);
                                foreach (Member member in members)
                                    member.Addresses = addresses.Where(a => a.MEMBER_DIM_KEY == member.MEMBER_DIM_KEY);
                                break;
                            case MemberComponent.MemberSpan:
                                IEnumerable<MemberSpan> spans = Database.Members.GetMemberSpansByIDs(ids);
                                foreach (Member member in members)
                                    member.CoverageSpans = spans.Where(s => s.MEMBER_DIM_KEY == member.MEMBER_DIM_KEY);
                                break;
                            case MemberComponent.PCPInformation:
                                IEnumerable<PCPProvider> pcps = Database.Members.GetMemberPCPByIDs(ids);
                                foreach (Member member in members)
                                    member.Providers = pcps.Where(p => p.MEMBER_DIM_KEY == member.MEMBER_DIM_KEY);
                                break;
                            default:
                                throw new ArgumentException($"The included component is not valid: {component}", "includedComponents");
                        }
                }
            }
        }
        #endregion Internal Member Component Methods
 
        #region Private Methods

        #region Get Member(s) - Base Methods
        /// <summary>
        /// Get a single member from the EDW
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <param name="errorOnMultiple">Whether the method should generate an error when multiple records are returned</param>
        /// <returns>Single member record</returns>
        private Member GetMember(Dictionary<string, object> criteria, IEnumerable<MemberComponent> includedComponents, bool errorOnMultiple)
        {
            IEnumerable<Member> members = GetMembers(criteria, includedComponents);
            if (members.Count() > 1 && errorOnMultiple)
                throw new Exception($"The query returned multiple results: {members.Count()} members found");
            else if (members.Any())
                return members.First();
            else
                return null;
        }

        /// <summary>
        /// Get a collection of members from the EDW
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Collection of member records</returns>
        private IEnumerable<Member> GetMembers(Dictionary<string, object> criteria, IEnumerable<MemberComponent> includedComponents)
        {
            // Build member query
            List<Dictionary<string, object>> criteriaParameter = criteria == null
                ? null
                : new List<Dictionary<string, object>>() { criteria };

            (string sql, object param, Dictionary<string, IEnumerable<object>> filterTables) = BuildMemberQuery(criteriaParameter);

            // Get base member information
            IEnumerable<Member> members = Database.QueryWithFilters<Member>(sql, param, filterTables);

            // Include other components
            AddComponentsToMembers(members, includedComponents);

            return members;
        }

        /// <summary>
        /// Gets a collection of members from the EDW
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns></returns>
        private IEnumerable<Member> GetMembersMulti(List<Dictionary<string, object>> criteria, IEnumerable<MemberComponent> includedComponents)
        {
            // Build member query
            (string sql, object param, Dictionary<string, IEnumerable<object>> filterTables) = BuildMemberQuery(criteria);

            // Get base member information
            IEnumerable<Member> members = Database.QueryWithFilters<Member>(sql, param, filterTables);

            // Include other components
            AddComponentsToMembers(members, includedComponents);

            return members;
        }

        /// <summary>
        /// Generates the SQL and parameters necessary to get member data from the EDW using the specified criteria
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <returns>
        /// 1 - SQL string to get member base data from EDW
        /// 2 - param object for Dapper query parameter substitution
        /// 3 - list of tables to filter by with table contents
        /// </returns>
        private static (string sql, object param, Dictionary<string, IEnumerable<object>>) BuildMemberQuery(IList<Dictionary<string, object>> criteria)
        {
            // Prepare result variables.
            StringBuilder sql = new StringBuilder();
            Dictionary<string, object> param = new Dictionary<string, object>();
            Dictionary<string, IEnumerable<object>> filterTables = new Dictionary<string, IEnumerable<object>>();

            #region Set default values

            string currentVersionDate = DateTime.Now.ToString("MM/dd/yyyy");
            string currentVersionDateINT = DateTime.Now.ToString("yyyyMMdd");

            //foreach (var cl in criteria)
            //{
            //    foreach (var p in cl)
            //    {
            //        switch (p.Key)
            //        {
            //            // Override default values
            //            case "CurrentVersionDate":
            //                // SQL parameter already in SQL object, just update values
            //                currentVersionDate = DateTime.Parse(p.Value.ToString()).ToString("MM/dd/yyyy");
            //                currentVersionDateINT = DateTime.Parse(p.Value.ToString()).ToString("yyyyMMdd");
            //                asOfDate = true;
            //                break;

            //        }
            //    }
            //}

            // Current Date because of member versioning causing issues
            // We had a bad member load that is causing use to get multiple versions
            // of a member
            // TODO - we actually need this in general, not just because of bad loads.
            // TODO - we also will want an "all members during this date range" that includes non-current members.
            //if (asOfDate)
            //{
            //    param["CurrentVersionDate"] = currentVersionDate;
            //    param["CurrentVersionDateINT"] = currentVersionDateINT;
            //}

            // MemberStatus
            param["MemberStatus"] = "Active";

            #endregion

            #region SQL Blob
            // TODO - "DISTINCT" is a sign of weakness...
            // TODO - Review once ETL for MEMBER_DIM load is finalized - erv added the "present day" filter but we need to be able to 'turn it off' for the portal.
            // TODO - will also need a "members since xxx including those that termed" version for reporting
            sql.AppendLine(@$"/*select * from (*/
                      SELECT DISTINCT MEMBER_HCC_ID
                      , mem.MEMBER_DIM_KEY AS MEMBER_DIM_KEY
                      , mem.ACCOUNT_DIM_Key AS ACCOUNT_DIM_KEY
                      , mem.SUBSCRIBER_DIM_KEY
                      , mem.MEMBER_HCC_ID AS MemberNumber
					  , mem.Status MemberStatus
					  , isnull(acct.STATUS, div.DIV_STATUS) AccountStatus		-- needs mapping
                      , TRIM(mem.SUBSCRIPTION_HCC_ID) AS ContractNumber
                      , TRIM(isnull(acct.ACCOUNT_HCC_ID, div.DIVISION_NBR)) AS AccountNumber
                      , TRIM(mem.FULL_NAME) AS FullName
                      , TRIM(mem.FIRST_NAME) AS FirstName
                      , TRIM(mem.LAST_NAME) AS LastName
                      , TRIM(mem.MIDDLE_NAME) AS MiddleName
                      , TRIM(mem.NAME_PREFIX) AS Prefix
                      , TRIM(mem.NAME_SUFFIX) AS Suffix
                      , mem.TELEPHONE_NUMBER AS Phone
                      , CASE TRIM(mem.GENDER) WHEN 'M' THEN 'Male' WHEN 'F' THEN 'Female' ELSE TRIM(mem.GENDER) END AS Sex
                      , TRIM(mem.TAX_ID) AS SSN
                      , CASE
                        WHEN mem.BIRTH_DATE_KEY = - 1 THEN NULL
                        ELSE dateMemDOB.DATE_NAME_2
                        END AS BirthDate
                      , CASE
                        WHEN mem.DEATH_DATE_KEY IN (0, -1) THEN NULL				-- Amisys is loaded differently (4/30/2020)
                        ELSE dateMemDOD.DATE_NAME_2
                        END AS DeathDate
                      , CASE mem.MEMBER_EFFECTIVE_DATE_KEY
                        WHEN 0        THEN '01/01/2019'				-- Amisys is loaded differently (4/30/2020)
                        WHEN -1       THEN '01/01/2019'
                        WHEN 18000101 THEN '01/01/1800'             -- This is garbage - database has 'Start of Time'
                        WHEN 30000101 THEN '01/01/3000'             -- This is garbage - database has 'End of Time'
                        WHEN 99991231 THEN '12/31/9999'             -- This is garbage - database has 'End of Time'
                        ELSE menreff.DATE_NAME_2
                        END AS StartDate
                      , CASE mem.MEMBER_TERMINATION_DATE_KEY
                        WHEN 0        THEN '12/31/9999'				-- Amisys is loaded differently (4/30/2020)
                        WHEN -1       THEN '12/31/9999'
                        WHEN 18000101 THEN '01/01/1800'             -- This is garbage - database has 'Start of Time'
                        WHEN 30000101 THEN '01/01/3000'             -- This is garbage - database has 'End of Time'
                        WHEN 99991231 THEN '12/31/9999'             -- This is garbage - database has 'End of Time'
                        ELSE menrtrm.DATE_NAME_2
                        END EndDate
                        --TRIM('') AS RecordNumber,
                      , TRIM(medicare.IDENTIFICATION_NUMBER) AS MedicareNumber
                      , TRIM(medicaid.IDENTIFICATION_NUMBER) AS MedicaidNumber
                      , TRIM(casenumber.IDENTIFICATION_NUMBER) AS CaseNumber
                      , TRIM(amisysId.IDENTIFICATION_NUMBER) AS AmisysId
                      , ethnic.ETHNICITY_NAME EthnicityID
                      , lang.LANGUAGE AS PrimaryLanguageID
                      , isnull(acct.[NAME], TRIM(div.[NAME])) AS GroupNumber
                      , mud.ATTRIBUTE_VALUE AS CapRateValue
                      , mud.VALUE_LONG_NAME AS CapRateDescription
                      --row_number() over (order by mem.member_dim_key asc) as rowNum

                    FROM MEMBER_DIM mem
                    --INNER JOIN ALL_MEMBER_ADDRESS_DETAILS memaddr -- not used yet and causing duplicates.
                    --  ON mem.MEMBER_DIM_KEY = memaddr.MEMBER_DIM_KEY
                    left JOIN [ACCOUNT_DIM] acct
                      ON mem.ACCOUNT_DIM_KEY = acct.ACCOUNT_DIM_KEY and mem.SOURCE_SYSTEM = 'HRP'
					left join AMISYS_DIVISION_DIM div
                      ON mem.ACCOUNT_DIM_KEY = div.AMISYS_DIVISION_DIM_KEY and mem.SOURCE_SYSTEM = 'AMISYS'
                    -- Date Keys
                    -- BIRTH_DATE_KEY
                    left JOIN [DATE_DIMENSION] dateMemDOB
                      ON dateMemDOB.DATE_DIM_KEY = mem.BIRTH_DATE_KEY
                    -- DEATH_DATE_KEY
                    left JOIN [DATE_DIMENSION] dateMemDOD
                      ON dateMemDOD.DATE_DIM_KEY = mem.DEATH_DATE_KEY
					-- Versioning effective dates
					left JOIN dbo.date_dimension mvereff ON
						mem.version_eff_date_key = mvereff.date_dim_key
					-- Versioning expiration dates
					left JOIN dbo.date_dimension mverexp on
						mem.version_exp_date_key = mverexp.date_dim_key
					-- Member effective dates
					left JOIN dbo.date_dimension menreff ON
						mem.MEMBER_EFFECTIVE_DATE_KEY = menreff.date_dim_key
					-- Member termination dates
					left JOIN dbo.date_dimension menrtrm on
						mem.MEMBER_TERMINATION_DATE_KEY = menrtrm.date_dim_key
                    LEFT JOIN MEMBER_OTHER_ID_DETAILS medicaid
                      ON mem.MEMBER_DIM_KEY = medicaid.MEMBER_DIM_KEY
                        AND medicaid.ID_TYPE = 'Medicaid Number'
                    LEFT JOIN MEMBER_OTHER_ID_DETAILS medicare
                      ON mem.MEMBER_DIM_KEY = medicare.MEMBER_DIM_KEY
                        AND medicare.ID_TYPE = 'HCFA Plan ID'
                    LEFT JOIN MEMBER_OTHER_ID_DETAILS casenumber
                      ON mem.MEMBER_DIM_KEY = casenumber.MEMBER_DIM_KEY
                        AND casenumber.ID_TYPE = 'Medicaid Case#'
                    LEFT JOIN MEMBER_OTHER_ID_DETAILS amisysId
                      ON mem.MEMBER_DIM_KEY = amisysId.MEMBER_DIM_KEY
                        AND amisysId.ID_TYPE = 'Amisys ID'
                    LEFT JOIN ALL_MEMBER_ETHNICITY_DETAILS ethnic
                      ON mem.MEMBER_DIM_KEY = ethnic.MEMBER_DIM_KEY
                        AND ethnic.SOURCE_SYSTEM = CASE MEMBER_HCC_ID WHEN amisysId.IDENTIFICATION_NUMBER THEN 'AMISYS' ELSE 'HRP' END
                    LEFT JOIN ALL_MEMBER_LANGUAGE_DETAILS lang
                      ON mem.MEMBER_DIM_KEY = lang.MEMBER_DIM_KEY
                        AND lang.LANGUAGE_TYPE = 'PRIMARY'
                    LEFT JOIN [dbo].[MEMBER_UDT_DETAILS] mud
                      ON mem.MEMBER_DIM_KEY = mud.MEMBER_DIM_KEY
                    WHERE mem.MEMBER_DIM_KEY <> -1
");
            // TODO -- Status = active should be optional, maybe?  Or does it stay on and "activemembersonly" means current span?
            //                if (asOfDate)
            //                {
            //                    sql.AppendLine(@"
            //					  -- Force to current 'span', without this multiple spans may show up
            //					  AND mem.CURRENT_IND = 1
            //					  -- Force the current version
            //					  -- AND mvereff.DATE_VALUE <=  @CurrentVersionDate
            //					  -- AND mverexp.DATE_VALUE >   @CurrentVersionDate
            //");
            //                }

            sql.AppendLine("and mem.current_ind=1");
            #endregion SQL Blob

            #region Add limiting criteria

            #region Add where clauses
            if (criteria != null)
            {
                int criterionIndex = 0;
                foreach (var criterion in criteria)
                {
                    if (criterion == null || criterion.Count == 0) continue;

                        string groupStart = ++criterionIndex == 1 ? "AND ((" : ") OR (";
                        sql.AppendLine(groupStart);
                        string andText = "";
                        foreach (string key in criterion.Keys)
                        {
                            string paramName = $"{key}{criterionIndex}";
                            switch (key)
                            {
                                // Override default values (Current date is handled above)
                                case "MemberStatus":
                                    // SQL already in SQL object
                                    param.Add("MemberStatus", criterion[key]);
                                    break;
                                case "AccountStatus":
                                    param.Add(paramName, criterion[key]);
                                    sql.AppendLine(" AND acct.STATUS = @{paramName}");
                                    break;
                                case "AccountHCCID":
                                    param.Add(paramName, criterion[key]);
                                    sql.AppendLine($" {andText} acct.ACCOUNT_HCC_ID = @{paramName}");
                                    break;
                                case "AccountKey":
                                    param.Add(paramName, criterion[key]);
                                    sql.AppendLine($" {andText} acct.ACCOUNT_KEY = @{paramName}");
                                    break;
                                case "memberOrMMIS":
                                    param.Add(paramName, criterion[key]);
                                    sql.AppendLine($" {andText} (MEMBER_HCC_ID = @{paramName} OR amisysId.IDENTIFICATION_NUMBER = @{paramName} OR medicaid.IDENTIFICATION_NUMBER = @{paramName})");
                                    break;
                                case "memberNumber":
                                    param.Add(paramName, criterion[key]);
                                    sql.AppendLine($" {andText} (MEMBER_HCC_ID = @{paramName} OR amisysId.IDENTIFICATION_NUMBER = @{paramName})");
                                    break;
                                case "memberNumbers":
                                    param.Add(paramName, criterion[key]);
                                    sql.AppendLine($" {andText} (MEMBER_HCC_ID IN @{paramName} OR amisysId.IDENTIFICATION_NUMBER IN @{paramName})");
                                    break;
                                case "FirstName":
                                    string firstNameCriterion = criterion[key].ToString().Replace("%", " ").Replace("_", " ").Trim();     // Kill wildcard characters
                                    param.Add(paramName, $"{firstNameCriterion}%");
                                    sql.AppendLine($" {andText} mem.FIRST_NAME LIKE @{paramName}");
                                    break;
                                case "LastName":
                                    string lastNameCriterion = criterion[key].ToString().Replace("%", " ").Replace("_", " ").Trim();     // Kill wildcard characters
                                    param.Add(paramName, $"{lastNameCriterion}%");
                                    sql.AppendLine($" {andText} mem.LAST_NAME LIKE @{paramName}");
                                    break;
                                case "FullName":
                                    param.Add(paramName, criterion[key]);
                                    sql.AppendLine($" {andText} mem.FULL_NAME = @{paramName}");
                                    break;
                                case "Subscriber":
                                    param.Add(paramName, criterion[key]);
                                    sql.AppendLine($" {andText} mem.RLTP_TO_SUBSCRIBER = @{paramName}");
                                    break;
                                case "SubscriptionNumber":
                                    param.Add(paramName, criterion[key]);
                                    sql.AppendLine($" {andText} mem.SUBSCRIPTION_HCC_ID = @{paramName}");
                                    break;
                                case "SubscriberLastName":
                                    param.Add(paramName, criterion[key]);
                                    sql.AppendLine($" {andText} mem.SUBSCRIBER_LAST_NAME = @{paramName}");
                                    break;
                                case "SubscriberFirstName":
                                    param.Add(paramName, criterion[key]);
                                    sql.AppendLine($" {andText} mem.SUBSCRIBER_FIRST_NAME = @{paramName}");
                                    break;
                                case "SubscriberName":
                                    param.Add(paramName, criterion[key]);
                                    sql.AppendLine($" {andText} mem.SUBSCRIBER_FULL_NAME = @{paramName}");
                                    break;
                                case "DateOfBirth":
                                    // Can use any date string format that DateTime can parse
                                    // Match to dateMemDOB.[DATE_VALUE]
                                    string tempDOB = DateTime.Parse(criterion[key].ToString()).ToShortDateString();
                                    param.Add(paramName, tempDOB);
                                    sql.AppendLine($" {andText} dateMemDOB.[DATE_VALUE] = @{paramName}");
                                    break;
                                case "DateOfDeath":
                                    // Can use any date string format that DateTime can parse
                                    // Match to dateMemDOB.[DATE_VALUE]
                                    string tempDOD = DateTime.Parse(criterion[key].ToString()).ToShortDateString();
                                    param.Add(paramName, tempDOD);
                                    sql.AppendLine($" {andText} dateMemDOD.[DATE_VALUE] = @{paramName}");
                                    break;
                                case "Gender":
                                    // Accept "M" male "W"/"F" female "U" unknown
                                    // Database has: Male/Female/Unknown
                                    string sexCode = null;
                                    switch (criterion[key].ToString().ToUpper().Trim())
                                    {
                                        case "MALE":
                                        case "MAN":
                                        case "M": sexCode = "Male"; break;
                                        case "FEMALE":
                                        case "WOMAN":
                                        case "F":
                                        case "W": sexCode = "Female"; break;
                                        case "UNKNOWN":
                                        case "U": sexCode = "Unknown"; break;
                                        default: sexCode = null; break;
                                    }
                                    if (!string.IsNullOrWhiteSpace(sexCode))
                                    {
                                        param.Add(paramName, sexCode);
                                        sql.AppendLine($" {andText} CASE mem.Gender WHEN 'M' THEN 'Male' WHEN 'F' THEN 'Female' ELSE mem.Gender END = @{paramName}");
                                    }
                                    break;
                                case "City":
                                    string cityName = $"{criterion[key]}%";
                                    param.Add(paramName, cityName);
                                    sql.AppendLine($" {andText} mem.MEMBER_DIM_KEY in (select MEMBER_DIM_KEY from ALL_MEMBER_ADDRESS_DETAILS mad where mad.CITY LIKE @{paramName} and MEMBER_ADDRESS_TYPE = 'Residential')");
                                    break;
                                case "ZipCode":
                                    param.Add(paramName, criterion[key]);
                                    sql.AppendLine($" {andText} mem.MEMBER_DIM_KEY in (select MEMBER_DIM_KEY from ALL_MEMBER_ADDRESS_DETAILS mad where mad.ZIP_CODE = @{paramName} and MEMBER_ADDRESS_TYPE = 'Residential')");
                                    break;
                                case "Phone":
                                    string phoneCriterion = Regex.Replace(criterion[key].ToString(), "[^0-9]", "");
                                    phoneCriterion = phoneCriterion.Insert(3, "-").Insert(7, "-");
                                    param.Add(paramName, phoneCriterion);
                                    sql.AppendLine($" {andText} mem.TELEPHONE_NUMBER = @{paramName}");
                                    break;
                                case "EligibilityStart":
                                    // TODO -- eligibility start/stop (eventually, per plan year, initially maybe "global"
                                    // TODO -- soft blocker - date dimension crap
                                    break;
                                case "EligibilityEnd":
                                    // Portal UI doesn't actually send this, but it's a possible way to state we want a range...
                                    break;
                                case "ActiveOnly":
                                    // TODO -- active-only (pass true (default) or false)
                                    break;
                                default:
                                    throw new ArgumentException($"The criteria contains an invalid key: {key}", "criteria");
                            }

                        andText = " AND ";
                        // TODO -- new thought:  DAL returns members optionally w/spans - Portal glue layer expands out the search result.
                    }
                }
                if (criterionIndex > 0) sql.AppendLine("))");
            }
            #endregion

            #region Add row number limiter
            //sql.AppendLine(") foo where rowNum between @minRowNum and @maxRowNum");

            // Add min and max row numbers values (will be overwritten if this isdestined for a data reader)
            param.Add("minRowNum", 0);
            param.Add("maxRowNum", int.MaxValue);
            #endregion

            #endregion Add limiting criteria

            return (sql.ToString(), param.ToQueryParamObject(), filterTables);
        }
        #endregion

        #region Get Member Addresses - Base Method
        /// <summary>
        /// Get a collection of member addresses from the EDW
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <returns>Collection of member address records</returns>
        private IEnumerable<MemberAddress> GetMemberAddresses(Dictionary<string, object> criteria)
        {
            StringBuilder sql = new StringBuilder();
            Dictionary<string, object> param = new Dictionary<string, object>();
            Dictionary<string, IEnumerable<object>> filterTables = new Dictionary<string, IEnumerable<object>>();

            // Build sql to get member addresses
            sql.AppendLine(@"
                SELECT addr.MEMBER_DIM_KEY, ADDRESS_LINE AS Address1, ADDRESS_LINE_2 AS Address2, ADDRESS_LINE_3 AS Address3, CITY AS City, [STATE] AS [State],
                    COUNTY AS County, COUNTRY AS Country, ZIP_CODE AS ZipCode,
                    CASE
                        WHEN MEMBER_ADDRESS_TYPE='MAILING ADDRESS' THEN 2
                        WHEN MEMBER_ADDRESS_TYPE='Residential' THEN 1
                    END AS AddressType
                 FROM ALL_MEMBER_ADDRESS_DETAILS addr");

            #region Add limiting criteria

            #region Add join statements
            if (criteria != null)
            {
                string tableName;
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "ids":
                            tableName = Database.GenerateTempTableName($"members_{key}");
                            filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                            sql.AppendLine($"  join {tableName} tmp_{key} on rtrim(addr.member_dim_key) = tmp_{key}.[value]");
                            break;
                    }
                }
            }
            #endregion
            sql.AppendLine(@"
                where 1=1
                and MEMBER_ADDRESS_TYPE IN ('MAILING ADDRESS','Residential')");

            #region Add where clauses
            #endregion

            #endregion

            // Get member addresses
            return Database.QueryWithFilters<MemberAddress>(sql.ToString(), param.ToQueryParamObject(), filterTables);
        }
        #endregion

        #region Get Member Spans - Base Method
        /// <summary>
        /// Get a collection of member spans from the EDW
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <returns>Collection of member span records</returns>
        private IEnumerable<MemberSpan> GetMemberSpans(Dictionary<string, object> criteria)
        {
            StringBuilder sql = new StringBuilder();
            Dictionary<string, object> param = new Dictionary<string, object>();
            Dictionary<string, IEnumerable<object>> filterTables = new Dictionary<string, IEnumerable<object>>();

            if (criteria == null)
                criteria = new Dictionary<string, object>();

            sql.AppendLine(@"
                select md.member_dim_key,bpd.benefit_plan_dim_key, bpd.name as planname, bpd.description as plandescription,
                ams.effective_date as planstartdate, ams.termination_date as planenddate,
                case when (getdate() between ams.effective_date and ams.termination_date) then 'active' else 'terminated' end as status
                from dm.all_member_span ams
                join dbo.member_dim md on ams.member_hcc_id=md.member_hcc_id
                join dbo.benefit_plan_dim bpd on ams.benefit_plan_hcc_id=bpd.benefit_plan_hcc_id
            ");

            #region Add limiting criteria

            #region Add join statements
            if (criteria != null)
            {
                string tableName;
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "ids":
                            tableName = Database.GenerateTempTableName($"members_{key}");
                            filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                            sql.AppendLine($"  join {tableName} tmp_{key} on rtrim(md.member_dim_key) = tmp_{key}.[value]");
                            break;
                    }
                }
            }
            #endregion
            sql.AppendLine(" where 1=1");

            #region Add where clauses
            #endregion

            #endregion

            // Get member addresses
            return Database.QueryWithFilters<MemberSpan>(sql.ToString(), param.ToQueryParamObject(), filterTables);
        }
        #endregion

        #region Get Member PCPs - Base Method
        /// <summary>
        /// Get a collection of member PCPs from the EDW
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <returns>Collection of member pcp records</returns>
        private IEnumerable<PCPProvider> GetMemberPCPs(Dictionary<string, object> criteria)
        {
            StringBuilder sql = new StringBuilder();
            Dictionary<string, object> param = new Dictionary<string, object>();
            Dictionary<string, IEnumerable<object>> filterTables = new Dictionary<string, IEnumerable<object>>();

            sql.AppendLine(@"SELECT  PCP.MEMBER_DIM_KEY
                                    ,PCP.PROVIDER_DIM_KEY  AS Databasekey
                                    ,PROV.NPI              AS ProviderNumber
	                                ,PROV.FIRST_NAME       AS FirstName
	                                ,PROV.LAST_NAME        AS LastName
	                                ,PROV.MIDDLE_NAME      AS MiddleName
	                                ,PROV.FULL_NAME        AS FullName
	                                ,PROV.NAME_SUFFIX      AS Suffix
	                                ,PROV.MEDICAID_NUMBER  AS MedicaidNumber
	                                ,PROV.MEDICAID_NUMBER  AS MedicareNumber
	                                ,PROV.SSN              AS SSN
	                                ,PROV.UPIN             AS UPIN
	                                ,PROV.NPI              AS NPI
	                                ,PROV.TAX_ID           AS TaxID
                                    ,DDIM.DATE_VALUE       AS BirthDate     -- was complex CASE stmt
                                    ,CASE TRIM(PROV.GENDER)
                                        WHEN 'M' THEN 'Male'
                                        WHEN 'F' THEN 'Female'
                                        ELSE 'Unknown'
                                    END                    AS Sex
	                                ,DDIM2.DATE_VALUE      AS PCPEffectiveDate
	                                ,PROV.SOURCE_KEY       AS CredentialingDatabaseKey
	                                ,CASE WHEN provider_type='p' THEN 'true' ELSE 'false' END AS IsIndividual
                                    ,CASE WHEN provider_type='p' THEN '' ELSE rtrim(full_name) END AS InstitutionName
                            FROM PROVIDER_DIM PROV
                            JOIN ALL_MEMBER_PCP_DETAILS PCP
	                          ON PROV.PROVIDER_DIM_KEY = PCP.PROVIDER_DIM_KEY
                            LEFT JOIN DATE_DIMENSION DDIM
	                            ON PROV.BIRTH_DATE_KEY = DDIM.DATE_DIM_KEY
                            LEFT JOIN DATE_DIMENSION DDIM2
	                            ON PCP.VERSION_EFF_DATE_KEY = DDIM2.DATE_DIM_KEY
                            ");

            #region Add limiting criteria

            #region Add join statements
            if (criteria != null)
            {
                string tableName;
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "ids":
                            tableName = Database.GenerateTempTableName($"members_{key}");
                            filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                            sql.AppendLine($"  join {tableName} tmp_{key} on rtrim(PCP.member_dim_key) = tmp_{key}.[value]");
                            break;
                    }
                }
            }
            #endregion
            sql.AppendLine(@"
                where 1=1
                and PROV.PROVIDER_TYPE='P'
                AND PCP.MEMBER_DIM_KEY IN @MemberDimKeys");

            #region Add where clauses
            #endregion

            #endregion

            IEnumerable<PCPProvider> providers = Database.QueryWithFilters<PCPProvider>(sql.ToString(), param.ToQueryParamObject(), filterTables);

            return providers;
        }
        #endregion Get Member PCP(s)

        #region Convert search parameters to criteria dictionary
        /// <summary>
        /// Builds dictionary of search criteria
        /// </summary>
        /// <param name="msp">Member search parameter</param>
        /// <returns></returns>
        private static Dictionary<string, object> ProcessSearchCriteria(MemberSearchParam msp)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();

            AddCriterion(result, msp.MemberNumber, "memberNumber");
            AddCriterion(result, msp.MemberNumbers, "memberNumbers");
            AddCriterion(result, msp.MemberNumberOrMMIS, "memberOrMMIS");
            AddCriterion(result, msp.FirstName, "FirstName");
            AddCriterion(result, msp.LastName, "LastName");
            AddCriterion(result, msp.DateOfBirth, "DateOfBirth");
            AddCriterion(result, msp.Gender, "Gender");
            AddCriterion(result, msp.City, "City");
            AddCriterion(result, msp.ZipCode, "ZipCode");
            AddCriterion(result, msp.PhoneNumber, "Phone");
            AddCriterion(result, msp.EligibilityStart, "EligibilityStart");
            AddCriterion(result, msp.MemberStatus, "MemberStatus");

            if (result.Count == 0) { return null; }

            return result;
        }

        /// <summary>
        /// Adds individual criterion to search critera dictionary
        /// </summary>
        /// <param name="result">Output dictionary</param>
        /// <param name="value">Criterion value</param>
        /// <param name="key">Criterion key</param>
        private static void AddCriterion(Dictionary<string, object> result, IEnumerable<string> value, string key)
        {
            if (value != null && value.Any()) { result.Add(key, value); }
        }

        /// <summary>
        /// Adds individual criterion to search critera dictionary
        /// </summary>
        /// <param name="result">Output dictionary</param>
        /// <param name="value">Criterion value</param>
        /// <param name="key">Criterion key</param>
        private static void AddCriterion(Dictionary<string, object> result, string value, string key)
        {
            if (!String.IsNullOrEmpty(value)) { result.Add(key, value); }
        }

        /// <summary>
        /// Adds individual criterion to search critera dictionary
        /// </summary>
        /// <param name="result">Output dictionary</param>
        /// <param name="value">Criterion value</param>
        /// <param name="key">Criterion key</param>
        private static void AddCriterion(Dictionary<string, object> result, DateTime? value, string key)
        {
            if (value.HasValue) { result.Add(key, value.Value); }
        }
        #endregion

        #endregion Private Methods
    }
}

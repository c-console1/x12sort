using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer.Base;
using DataLayer.EnterpriseDataWarehouse.Enumerations;
using DataLayer.EnterpriseDataWarehouse.Models;
using DataLayer.StaticUtilities.Extensions;

namespace DataLayer.EnterpriseDataWarehouse
{
    public class ProviderRepository : BaseRepository<EnterpriseDataWarehouseDatabase>
    {
        const bool DEFAULT_ERROR_ON_MULTIPLE = true;

        internal ProviderRepository(EnterpriseDataWarehouseDatabase database) : base(database)
        {
            
        }

        ///////////////////////////////////////////////////////////////////////////////////////////

        #region Public Provider Methods


        #region Get Provider(s) By Database Key
        /// <summary>
        /// Get a single provider from the EDW by database key
        /// </summary>
        /// <param name="databaseKey">Provider database key</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <param name="errorOnMultiple">Whether the method should generate an error when multiple records are returned</param>
        /// <returns>Single provider record</returns>
        public Provider GetProviderByDatabaseKey(string databaseKey, IEnumerable<ProviderComponent> includedComponents = null, bool errorOnMultiple = DEFAULT_ERROR_ON_MULTIPLE)
        {
            return GetProviderByDatabaseKey(int.Parse(databaseKey), includedComponents, errorOnMultiple);
        }

        /// <summary>
        /// Get a single provider from the EDW by database key
        /// </summary>
        /// <param name="databaseKey">Provider database key</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <param name="errorOnMultiple">Whether the method should generate an error when multiple records are returned</param>
        /// <returns>Single provider record</returns>
        public Provider GetProviderByDatabaseKey(int databaseKey, IEnumerable<ProviderComponent> includedComponents = null, bool errorOnMultiple = DEFAULT_ERROR_ON_MULTIPLE)
        {
            return GetProvider(
                new Dictionary<string, object>()
                {
                    { "id", databaseKey },
                    { "idType", ProviderNumberType.DatabaseKey }
                },
                includedComponents,
                errorOnMultiple
                );
        }

        /// <summary>
        /// Get a collection of providers from the EDW using a list of database keys
        /// </summary>
        /// <param name="databaseKeys">Collection of provider database keys</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Collection of provider records</returns>
        public IEnumerable<Provider> GetProvidersByDatabaseKeys(IEnumerable<string> databaseKeys, IEnumerable<ProviderComponent> includedComponents = null)
        {
            return GetProvidersByDatabaseKeys(databaseKeys.Select(dk => int.Parse(dk)), includedComponents);
        }

        /// <summary>
        /// Get a collection of providers from the EDW using a list of database keys
        /// </summary>
        /// <param name="databaseKeys">Collection of provider database keys</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Collection of provider records</returns>
        public IEnumerable<Provider> GetProvidersByDatabaseKeys(IEnumerable<int> databaseKeys, IEnumerable<ProviderComponent> includedComponents = null)
        {
            return GetProviders(
                new Dictionary<string, object>()
                {
                    { "ids", databaseKeys },
                    { "idType", ProviderNumberType.DatabaseKey }
                },
                includedComponents
                );
        }

        /// <summary>
        /// Get a data reader for providers from the EDW using a list of database keys
        /// </summary>
        /// <param name="databaseKeys">Collection of provider database keys</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Data reader for provider records</returns>
        public ProviderDataReader GetProviderDataReaderByDatabaseKeys(IEnumerable<string> databaseKeys, IEnumerable<ProviderComponent> includedComponents = null)
        {
            return GetProviderDataReaderByDatabaseKeys(databaseKeys.Select(dk => int.Parse(dk)), includedComponents);
        }

        /// <summary>
        /// Get a data reader for providers from the EDW using a list of database keys
        /// </summary>
        /// <param name="databaseKeys">Collection of provider database keys</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Data reader for provider records</returns>
        public ProviderDataReader GetProviderDataReaderByDatabaseKeys(IEnumerable<int> databaseKeys, IEnumerable<ProviderComponent> includedComponents = null)
        {
            return GetProviderDataReader(
                new Dictionary<string, object>()
                {
                    { "ids", databaseKeys },
                    { "idType", ProviderNumberType.DatabaseKey }
                },
                includedComponents
                );
        }
        #endregion

        #region Get Provider(s) By ID
        /// <summary>
        /// Get a single provider from the EDW by ID
        /// </summary>
        /// <param name="id">Provider ID</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <param name="idType">Type of provider ID to lookup</param>
        /// <param name="errorOnMultiple">Whether the method should generate an error when multiple records are returned</param>
        /// <returns>Single provider record</returns>
        public Provider GetProviderByID(string id, IEnumerable<ProviderComponent> includedComponents = null, ProviderNumberType idType = ProviderNumberType.BillingID, bool errorOnMultiple = DEFAULT_ERROR_ON_MULTIPLE)
        {
            if (idType == ProviderNumberType.DatabaseKey)
                return GetProviderByDatabaseKey(id, includedComponents: includedComponents, errorOnMultiple: errorOnMultiple);
            else
                return GetProvider(
                    new Dictionary<string, object>()
                    {
                        { "id", id },
                        { "idType", idType }
                    },
                    includedComponents,
                    errorOnMultiple
                    );
        }

        /// <summary>
        /// Get a collection of providers from the EDW using a list of IDs
        /// </summary>
        /// <param name="ids">Collection of provider IDs</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <param name="idType">Type of provider ID to lookup</param>
        /// <returns>Collection of provider records</returns>
        public IEnumerable<Provider> GetProvidersByIDs(IEnumerable<string> ids, IEnumerable<ProviderComponent> includedComponents = null, ProviderNumberType idType = ProviderNumberType.BillingID)
        {
            if (idType == ProviderNumberType.DatabaseKey)
                return GetProvidersByDatabaseKeys(ids, includedComponents: includedComponents);
            else
                return GetProviders(
                    new Dictionary<string, object>()
                    {
                        { "ids", ids },
                        { "idType", idType }
                    },
                    includedComponents
                    );
        }

        /// <summary>
        /// Get a data reader for providers from the EDW using a list of IDs
        /// </summary>
        /// <param name="ids">Collection of provider IDs</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <param name="idType">Type of provider ID to lookup</param>
        /// <returns>Data reader for provider records</returns>
        public ProviderDataReader GetProviderDataReaderByIDs(IEnumerable<string> ids, IEnumerable<ProviderComponent> includedComponents = null, ProviderNumberType idType = ProviderNumberType.BillingID)
        {
            if (idType == ProviderNumberType.DatabaseKey)
                return GetProviderDataReaderByDatabaseKeys(ids, includedComponents: includedComponents);
            else
                return GetProviderDataReader(
                    new Dictionary<string, object>()
                    {
                        { "ids", ids },
                        { "idType", idType }
                    },
                    includedComponents
                    );
        }
        #endregion

        #region Get Provider(s) By Name
        /// <summary>
        /// Get a single provider from the EDW by facility name
        /// </summary>
        /// <param name="facilityName">Provider facility name</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <param name="errorOnMultiple">Whether the method should generate an error when multiple records are returned</param>
        /// <returns>Single provider record</returns>
        public Provider GetProviderByName(string facilityName, IEnumerable<ProviderComponent> includedComponents = null, bool errorOnMultiple = DEFAULT_ERROR_ON_MULTIPLE)
        {
            return GetProvider(
                new Dictionary<string, object>()
                {
                    { "facilityName", facilityName }
                },
                includedComponents,
                errorOnMultiple
                );
        }

        /// <summary>
        /// Get a single provider from the EDW by first name and last name
        /// </summary>
        /// <param name="firstName">Provider first name</param>
        /// <param name="lastName">Provider last name</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <param name="errorOnMultiple">Whether the method should generate an error when multiple records are returned</param>
        /// <returns>Single provider record</returns>
        public Provider GetProviderByName(string firstName, string lastName, IEnumerable<ProviderComponent> includedComponents = null, bool errorOnMultiple = DEFAULT_ERROR_ON_MULTIPLE)
        {
            return GetProvider(
                new Dictionary<string, object>()
                {
                    { "firstName", firstName },
                    { "lastName", lastName }
                },
                includedComponents,
                errorOnMultiple
                );
        }

        /// <summary>
        /// Get a collection of providers from the EDW by facility name
        /// </summary>
        /// <param name="facilityName">Provider facility name</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Collection of provider records</returns>
        public IEnumerable<Provider> GetProvidersByName(string facilityName, IEnumerable<ProviderComponent> includedComponents = null)
        {
            return GetProviders(
                new Dictionary<string, object>()
                {
                    { "facilityName", facilityName }
                },
                includedComponents
                );
        }

        /// <summary>
        /// Get a collection of providers from the EDW by first name and last name
        /// </summary>
        /// <param name="firstName">Provider first name</param>
        /// <param name="lastName">Provider last name</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Collection of provider records</returns>
        public IEnumerable<Provider> GetProvidersByName(string firstName, string lastName, IEnumerable<ProviderComponent> includedComponents = null)
        {
            return GetProviders(
                new Dictionary<string, object>()
                {
                    { "firstName", firstName },
                    { "lastName", lastName }
                },
                includedComponents
                );
        }
        #endregion

        #region Get Provider(s) By Name Like
        /// <summary>
        /// Get a single provider from the EDW by facility name using like comparison
        /// </summary>
        /// <param name="facilityName">Provider facility name</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <param name="errorOnMultiple">Whether the method should generate an error when multiple records are returned</param>
        /// <returns>Single provider record</returns>
        public Provider GetProviderByNameLike(string facilityName, IEnumerable<ProviderComponent> includedComponents = null, bool errorOnMultiple = DEFAULT_ERROR_ON_MULTIPLE)
        {
            return GetProvider(
                new Dictionary<string, object>()
                {
                    { "facilityNameLike", facilityName }
                },
                includedComponents,
                errorOnMultiple
                );
        }

        /// <summary>
        /// Get a single provider from the EDW by first name and last name using like comparison
        /// </summary>
        /// <param name="firstName">Provider first name</param>
        /// <param name="lastName">Provider last name</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <param name="errorOnMultiple">Whether the method should generate an error when multiple records are returned</param>
        /// <returns>Single provider record</returns>
        public Provider GetProviderByNameLike(string firstName, string lastName, IEnumerable<ProviderComponent> includedComponents = null, bool errorOnMultiple = DEFAULT_ERROR_ON_MULTIPLE)
        {
            return GetProvider(
                new Dictionary<string, object>()
                {
                    { "firstNameLike", firstName },
                    { "lastNameLike", lastName }
                },
                includedComponents,
                errorOnMultiple
                );
        }

        /// <summary>
        /// Get a collection of providers from the EDW by facility name using like comparison
        /// </summary>
        /// <param name="facilityName">Provider facility name</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Collection of provider records</returns>
        public IEnumerable<Provider> GetProvidersByNameLike(string facilityName, IEnumerable<ProviderComponent> includedComponents = null)
        {
            return GetProviders(
                new Dictionary<string, object>()
                {
                    { "facilityNameLike", facilityName }
                },
                includedComponents
                );
        }

        /// <summary>
        /// Get a collection of providers from the EDW by first name and last name using like comparison
        /// </summary>
        /// <param name="firstName">Provider first name</param>
        /// <param name="lastName">Provider last name</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Collection of provider records</returns>
        public IEnumerable<Provider> GetProvidersByNameLike(string firstName, string lastName, IEnumerable<ProviderComponent> includedComponents = null)
        {
            return GetProviders(
                new Dictionary<string, object>()
                {
                    { "firstNameLike", firstName },
                    { "lastNameLike", lastName }
                },
                includedComponents
                );
        }
        #endregion

        #region Get Provider(s) By NPI
        /// <summary>
        /// Get a single provider from the EDW by NPI
        /// </summary>
        /// <param name="npi">Provider NPI</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <param name="errorOnMultiple">Whether the method should generate an error when multiple records are returned</param>
        /// <returns>Single provider record</returns>
        public Provider GetProviderByNPI(string npi, IEnumerable<ProviderComponent> includedComponents = null, bool errorOnMultiple = DEFAULT_ERROR_ON_MULTIPLE)
        {
            return GetProvider(
                new Dictionary<string, object>()
                {
                    { "id", npi },
                    { "idType", ProviderNumberType.NPI }
                },
                includedComponents,
                errorOnMultiple
                );
        }

        /// <summary>
        /// Get a collection of providers from the EDW using a list of NPIs
        /// </summary>
        /// <param name="npis">Collection of provider NPIs</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Collection of provider records</returns>
        public IEnumerable<Provider> GetProvidersByNPIs(IEnumerable<string> npis, IEnumerable<ProviderComponent> includedComponents = null)
        {
            return GetProviders(
                new Dictionary<string, object>()
                {
                    { "ids", npis },
                    { "idType", ProviderNumberType.NPI }
                },
                includedComponents
                );
        }

        /// <summary>
        /// Get a data reader for providers from the EDW using a list of NPIs
        /// </summary>
        /// <param name="npis">Collection of provider NPIs</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Data reader for provider records</returns>
        public ProviderDataReader GetProviderDataReaderByNPIs(IEnumerable<string> npis, IEnumerable<ProviderComponent> includedComponents = null)
        {
            return GetProviderDataReader(
                new Dictionary<string, object>()
                {
                    { "ids", npis },
                    { "idType", ProviderNumberType.NPI }
                },
                includedComponents
                );
        }
        #endregion

        #region Get Providers Unfiltered
        /// <summary>
        /// Gets a collection of providers from the EDW, with no filtering applied
        /// </summary>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Collection of provider records</returns>
        public IEnumerable<Provider> GetProvidersUnfiltered(IEnumerable<ProviderComponent> includedComponents = null)
        {
            return GetProviders(null, includedComponents);
        }

        /// <summary>
        /// Gets a data reader for providers from the EDW, with no filtering applied
        /// </summary>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Data reader for provider records</returns>
        public ProviderDataReader GetProviderDataReaderUnfiltered(IEnumerable<ProviderComponent> includedComponents = null)
        {
            return GetProviderDataReader(null, includedComponents);
        }
        #endregion


        #endregion

        ///////////////////////////////////////////////////////////////////////////////////////////

        #region Public Provider Address Methods


        #region Get Addresses by Provider ID
        /// <summary>
        /// Get a collection of provider addresses from the EDW by a provider ID
        /// </summary>
        /// <param name="id">Provider ID</param>
        /// <param name="idType">Type of provider ID to lookup</param>
        /// <returns>Collection of provider address records</returns>
        public IEnumerable<ProviderAddress> GetProviderAddressesByID(string id, ProviderNumberType idType = ProviderNumberType.BillingID)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object>();
            if (idType == ProviderNumberType.DatabaseKey)
                criteria.Add("id", int.Parse(id));
            else
                criteria.Add("id", id);
            criteria.Add("idType", idType);

            return GetProviderAddresses(criteria);
        }

        /// <summary>
        /// Get a collection of provider addresses from the EDW using a list of IDs
        /// </summary>
        /// <param name="ids">Collection of provider IDs</param>
        /// <param name="idType">Type of provider ID to lookup</param>
        /// <returns>Collection of provider address records</returns>
        public IEnumerable<ProviderAddress> GetProviderAddressesByID(IEnumerable<string> ids, ProviderNumberType idType = ProviderNumberType.BillingID)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object>();
            if (idType == ProviderNumberType.DatabaseKey)
                criteria.Add("ids", ids.Select(id => int.Parse(id)));
            else
                criteria.Add("ids", ids);
            criteria.Add("idType", idType);

            return GetProviderAddresses(criteria);
        }
        #endregion

        #region Get Addresses by Provider NPI
        /// <summary>
        /// Get a collection of provider addresses from the EDW by a provider NPI
        /// </summary>
        /// <param name="npi">Provider NPI</param>
        /// <returns>Collection of provider address records</returns>
        public IEnumerable<ProviderAddress> GetProviderAddressesByNPI(string npi)
        {
            return GetProviderAddresses(
                new Dictionary<string, object>()
                {
                    { "id", npi },
                    { "idType", ProviderNumberType.NPI }
                });
        }
        #endregion

        #region Get Provider Addresses By Group Database Key
        /// <summary>
        /// Get a collection of provider addresses from the EDW by group database key
        /// </summary>
        /// <param name="grpDatabaseKey">Provider group database key</param>
        /// <returns>Collection of provider address records</returns>
        public IEnumerable<ProviderAddress> GetProviderAddressesByGroupDatabaseKey(string grpDatabaseKey)
        {
            return GetProviderAddressesByGroupDatabaseKey(int.Parse(grpDatabaseKey));
        }

        /// <summary>
        /// Get a collection of provider addresses from the EDW by group database key
        /// </summary>
        /// <param name="grpDatabaseKey">Provider group database key</param>
        /// <returns>Collection of provider address records</returns>
        public IEnumerable<ProviderAddress> GetProviderAddressesByGroupDatabaseKey(int grpDatabaseKey)
        {
            return GetProviderGroupAddresses(
                new Dictionary<string, object>()
                {
                    { "groupId", grpDatabaseKey },
                    { "idType", ProviderGroupNumberType.DatabaseKey }
                });
        }

        /// <summary>
        /// Get a collection of provider addresses from the EDW using a list of group database keys
        /// </summary>
        /// <param name="grpDatabaseKeys">Provider group database keys</param>
        /// <returns>Collection of provider address records</returns>
        public IEnumerable<ProviderAddress> GetProviderAddressesByGroupDatabaseKeys(IEnumerable<string> grpDatabaseKeys)
        {
            return GetProviderAddressesByGroupDatabaseKeys(grpDatabaseKeys.Select(key => int.Parse(key)));
        }

        /// <summary>
        /// Get a collection of provider addresses from the EDW using a list of group database keys
        /// </summary>
        /// <param name="grpDatabaseKeys">Provider group database keys</param>
        /// <returns>Collection of provider address records</returns>
        public IEnumerable<ProviderAddress> GetProviderAddressesByGroupDatabaseKeys(IEnumerable<int> grpDatabaseKeys)
        {
            return GetProviderGroupAddresses(
                new Dictionary<string, object>()
                {
                    { "groupIds", grpDatabaseKeys },
                    { "idType", ProviderGroupNumberType.DatabaseKey }
                });
        }
        #endregion

        #region Get Provider Addresses By Group ID
        /// <summary>
        /// Get a collection of provider addresses from the EDW by group database key
        /// </summary>
        /// <param name="groupId">Provider group ID</param>
        /// <param name="idType">Type of provider group ID to lookup</param>
        /// <returns>Collection of provider address records</returns>
        public IEnumerable<ProviderAddress> GetProviderAddressesByGroupID(string groupId, ProviderGroupNumberType idType = ProviderGroupNumberType.CredentialingID)
        {
            if (idType == ProviderGroupNumberType.DatabaseKey)
                return GetProviderAddressesByGroupDatabaseKey(groupId);
            else
                return GetProviderGroupAddresses(
                    new Dictionary<string, object>()
                    {
                        { "groupId", groupId },
                        { "idType", idType }
                    });
        }

        /// <summary>
        /// Get a collection of provider addresses from the EDW using a list of group database keys
        /// </summary>
        /// <param name="groupIds">Provider group IDs</param>
        /// <param name="idType">Type of provider group ID to lookup</param>
        /// <returns>Collection of provider address records</returns>
        public IEnumerable<ProviderAddress> GetProviderAddressesByGroupIDs(IEnumerable<string> groupIds, ProviderGroupNumberType idType = ProviderGroupNumberType.CredentialingID)
        {
            if (idType == ProviderGroupNumberType.DatabaseKey)
                return GetProviderAddressesByGroupDatabaseKeys(groupIds);
            else
                return GetProviderGroupAddresses(
                    new Dictionary<string, object>()
                    {
                        { "groupIds", groupIds },
                        { "idType", idType }
                    });
        }
        #endregion


        #endregion

        ///////////////////////////////////////////////////////////////////////////////////////////

        #region Public Provider Board Methods


        #region Get Boards by Provider ID
        /// <summary>
        /// Get a collection of provider boards from the EDW by a provider ID
        /// </summary>
        /// <param name="id">Provider ID</param>
        /// <param name="idType">Type of provider ID to lookup</param>
        /// <returns>Collection of provider board records</returns>
        public IEnumerable<ProviderBoard> GetProviderBoardsByID(string id, ProviderNumberType idType = ProviderNumberType.BillingID)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object>();
            if (idType == ProviderNumberType.DatabaseKey)
                criteria.Add("id", int.Parse(id));
            else
                criteria.Add("id", id);
            criteria.Add("idType", idType);

            return GetProviderBoards(criteria);
        }

        /// <summary>
        /// Get a collection of provider boards from the EDW using a list of IDs
        /// </summary>
        /// <param name="ids">Collection of provider IDs</param>
        /// <param name="idType">Type of provider ID to lookup</param>
        /// <returns>Collection of provider board records</returns>
        public IEnumerable<ProviderBoard> GetProviderBoardsByID(IEnumerable<string> ids, ProviderNumberType idType = ProviderNumberType.BillingID)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object>();
            if (idType == ProviderNumberType.DatabaseKey)
                criteria.Add("ids", ids.Select(id => int.Parse(id)));
            else
                criteria.Add("ids", ids);
            criteria.Add("idType", idType);

            return GetProviderBoards(criteria);
        }
        #endregion

        #region Get Boards by Provider NPI
        /// <summary>
        /// Get a collection of provider boards from the EDW by a provider NPI
        /// </summary>
        /// <param name="npi">Provider NPI</param>
        /// <returns>Collection of provider board records</returns>
        public IEnumerable<ProviderBoard> GetProviderBoardsByNPI(string npi)
        {
            return GetProviderBoards(
                new Dictionary<string, object>()
                {
                    { "id", npi },
                    { "idType", ProviderNumberType.NPI }
                });
        }
        #endregion


        #endregion

        ///////////////////////////////////////////////////////////////////////////////////////////

        #region Public Provider Entity Assignment Methods


        #region Get Entity Assignments by Provider ID
        /// <summary>
        /// Get a collection of provider entity assignments from the EDW by a provider ID
        /// </summary>
        /// <param name="id">Provider ID</param>
        /// <param name="idType">Type of provider ID to lookup</param>
        /// <returns>Collection of provider entity assignment records</returns>
        public IEnumerable<ProviderEntityAssignment> GetProviderEntityAssignmentsByID(string id, ProviderNumberType idType = ProviderNumberType.BillingID)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object>();
            if (idType == ProviderNumberType.DatabaseKey)
                criteria.Add("id", int.Parse(id));
            else
                criteria.Add("id", id);
            criteria.Add("idType", idType);

            return GetProviderEntityAssignments(criteria);
        }

        /// <summary>
        /// Get a collection of provider entity assignments from the EDW using a list of IDs
        /// </summary>
        /// <param name="ids">Collection of provider IDs</param>
        /// <param name="idType">Type of provider ID to lookup</param>
        /// <returns>Collection of provider entity assignment records</returns>
        public IEnumerable<ProviderEntityAssignment> GetProviderEntityAssignmentsByID(IEnumerable<string> ids, ProviderNumberType idType = ProviderNumberType.BillingID)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object>();
            if (idType == ProviderNumberType.DatabaseKey)
                criteria.Add("ids", ids.Select(id => int.Parse(id)));
            else
                criteria.Add("ids", ids);
            criteria.Add("idType", idType);

            return GetProviderEntityAssignments(criteria);
        }
        #endregion

        #region Get Entity Assignments by Provider NPI
        /// <summary>
        /// Get a collection of provider entity assignments from the EDW by a provider NPI
        /// </summary>
        /// <param name="npi">Provider NPI</param>
        /// <returns>Collection of provider entity assignment records</returns>
        public IEnumerable<ProviderEntityAssignment> GetProviderEntityAssignmentsByNPI(string npi)
        { 
            return GetProviderEntityAssignments(
                new Dictionary<string, object>()
                {
                    { "id", npi },
                    { "idType", ProviderNumberType.NPI }
                });
        }
        #endregion


        #endregion

        ///////////////////////////////////////////////////////////////////////////////////////////

        #region Public Provider Group Methods


        #region Get Provider Group(s) By Database Key
        /// <summary>
        /// Get a single provider group from the EDW by database key
        /// </summary>
        /// <param name="databaseKey">Provider group database key</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <param name="errorOnMultiple">Whether the method should generate an error when multiple records are returned</param>
        /// <returns>Single provider group record</returns>
        public ProviderGroup GetProviderGroupByDatabaseKey(string databaseKey, IEnumerable<ProviderGroupComponent> includedComponents = null, bool errorOnMultiple = DEFAULT_ERROR_ON_MULTIPLE)
        {
            return GetProviderGroupByDatabaseKey(int.Parse(databaseKey), includedComponents, errorOnMultiple);
        }

        /// <summary>
        /// Get a single provider group from the EDW by database key
        /// </summary>
        /// <param name="databaseKey">Provider group database key</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <param name="errorOnMultiple">Whether the method should generate an error when multiple records are returned</param>
        /// <returns>Single provider group record</returns>
        public ProviderGroup GetProviderGroupByDatabaseKey(int databaseKey, IEnumerable<ProviderGroupComponent> includedComponents = null, bool errorOnMultiple = DEFAULT_ERROR_ON_MULTIPLE)
        {
            return GetProviderGroup(
                new Dictionary<string, object>()
                {
                    { "id", databaseKey },
                    { "idType", ProviderGroupNumberType.DatabaseKey }
                },
                includedComponents,
                errorOnMultiple
                );
        }

        /// <summary>
        /// Get a collection of provider groups from the EDW using a list of database keys
        /// </summary>
        /// <param name="databaseKeys">Collection of provider group database keys</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Collection of provider group records</returns>
        public IEnumerable<ProviderGroup> GetProviderGroupsByDatabaseKeys(IEnumerable<string> databaseKeys, IEnumerable<ProviderGroupComponent> includedComponents = null)
        {
            return GetProviderGroupsByDatabaseKeys(databaseKeys.Select(key => int.Parse(key)), includedComponents);
        }

        /// <summary>
        /// Get a collection of provider groups from the EDW using a list of database keys
        /// </summary>
        /// <param name="databaseKeys">Collection of provider group database keys</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Collection of provider group records</returns>
        public IEnumerable<ProviderGroup> GetProviderGroupsByDatabaseKeys(IEnumerable<int> databaseKeys, IEnumerable<ProviderGroupComponent> includedComponents = null)
        {
            return GetProviderGroups(
                new Dictionary<string, object>()
                {
                    { "ids", databaseKeys },
                    { "idType", ProviderGroupNumberType.DatabaseKey }
                },
                includedComponents
                );
        }

        /// <summary>
        /// Get a data reader for provider groups from the EDW using a list of database keys
        /// </summary>
        /// <param name="databaseKeys">Collection of provider group database keys</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Data reader for provider group records</returns>
        public ProviderGroupDataReader GetProviderGroupDataReaderByDatabaseKeys(IEnumerable<string> databaseKeys, IEnumerable<ProviderGroupComponent> includedComponents = null)
        {
            return GetProviderGroupDataReaderByDatabaseKeys(databaseKeys.Select(key => int.Parse(key)), includedComponents);
        }

        /// <summary>
        /// Get a data reader for provider groups from the EDW using a list of database keys
        /// </summary>
        /// <param name="databaseKeys">Collection of provider group database keys</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Data reader for provider group records</returns>
        public ProviderGroupDataReader GetProviderGroupDataReaderByDatabaseKeys(IEnumerable<int> databaseKeys, IEnumerable<ProviderGroupComponent> includedComponents = null)
        {
            return GetProviderGroupDataReader(
                new Dictionary<string, object>()
                {
                    { "ids", databaseKeys },
                    { "idType", ProviderGroupNumberType.DatabaseKey }
                },
                includedComponents
                );
        }
        #endregion

        #region Get Provider Group(s) By ID
        /// <summary>
        /// Get a single provider group from the EDW by ID
        /// </summary>
        /// <param name="id">Provider group ID</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <param name="idType">Type of provider group ID to lookup</param>
        /// <param name="errorOnMultiple">Whether the method should generate an error when multiple records are returned</param>
        /// <returns>Single provider group record</returns>
        public ProviderGroup GetProviderGroupByID(string id, IEnumerable<ProviderGroupComponent> includedComponents = null, ProviderGroupNumberType idType = ProviderGroupNumberType.CredentialingID, bool errorOnMultiple = DEFAULT_ERROR_ON_MULTIPLE)
        {
            if (idType == ProviderGroupNumberType.DatabaseKey)
                return GetProviderGroupByDatabaseKey(id, includedComponents: includedComponents, errorOnMultiple: errorOnMultiple);
            else
                return GetProviderGroup(
                    new Dictionary<string, object>()
                    {
                        { "id", id },
                        { "idType", idType }
                    },
                    includedComponents,
                    errorOnMultiple
                    );
        }

        /// <summary>
        /// Get a collection of provider groups from the EDW using a list of IDs
        /// </summary>
        /// <param name="ids">Collection of provider group IDs</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <param name="idType">Type of provider group ID to lookup</param>
        /// <returns>Collection of provider group records</returns>
        public IEnumerable<ProviderGroup> GetProviderGroupsByIDs(IEnumerable<string> ids, IEnumerable<ProviderGroupComponent> includedComponents = null, ProviderGroupNumberType idType = ProviderGroupNumberType.CredentialingID)
        {
            if (idType == ProviderGroupNumberType.DatabaseKey)
                return GetProviderGroupsByDatabaseKeys(ids, includedComponents: includedComponents);
            else
                return GetProviderGroups(
                    new Dictionary<string, object>()
                    {
                        { "ids", ids },
                        { "idType", idType }
                    },
                    includedComponents
                    );
        }

        /// <summary>
        /// Get a data reader for provider groups from the EDW using a list of IDs
        /// </summary>
        /// <param name="ids">Collection of provider group IDs</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <param name="idType">Type of provider group ID to lookup</param>
        /// <returns>Data reader for provider group records</returns>
        public ProviderGroupDataReader GetProviderGroupDataReaderByIDs(IEnumerable<string> ids, IEnumerable<ProviderGroupComponent> includedComponents = null, ProviderGroupNumberType idType = ProviderGroupNumberType.CredentialingID)
        {
            if (idType == ProviderGroupNumberType.DatabaseKey)
                return GetProviderGroupDataReaderByDatabaseKeys(ids, includedComponents: includedComponents);
            else
                return GetProviderGroupDataReader(
                    new Dictionary<string, object>()
                    {
                        { "ids", ids },
                        { "idType", idType }
                    },
                    includedComponents
                    );
        }
        #endregion

        #region Get Provider Group(s) By Name
        /// <summary>
        /// Get a single provider group from the EDW by group name
        /// </summary>
        /// <param name="name">Provider group name</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <param name="errorOnMultiple">Whether the method should generate an error when multiple records are returned</param>
        /// <returns>Single provider group record</returns>
        public ProviderGroup GetProviderGroupByName(string name, IEnumerable<ProviderGroupComponent> includedComponents = null, bool errorOnMultiple = DEFAULT_ERROR_ON_MULTIPLE)
        {
            return GetProviderGroup(
                new Dictionary<string, object>()
                {
                    { "name", name }
                },
                includedComponents,
                errorOnMultiple
                );
        }

        /// <summary>
        /// Get a collection of provider groups from the EDW by group name
        /// </summary>
        /// <param name="name">Provider group name</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Collection of provider group records</returns>
        public IEnumerable<ProviderGroup> GetProviderGroupsByName(string name, IEnumerable<ProviderGroupComponent> includedComponents = null)
        {
            return GetProviderGroups(
                new Dictionary<string, object>()
                {
                    { "name", name }
                },
                includedComponents
                );
        }
        #endregion

        #region Get Provider Group(s) By Name Like
        /// <summary>
        /// Get a single provider group from the EDW by group name using like comparison
        /// </summary>
        /// <param name="name">Provider group name</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <param name="errorOnMultiple">Whether the method should generate an error when multiple records are returned</param>
        /// <returns>Single provider group record</returns>
        public ProviderGroup GetProviderGroupByNameLike(string name, IEnumerable<ProviderGroupComponent> includedComponents = null, bool errorOnMultiple = DEFAULT_ERROR_ON_MULTIPLE)
        {
            return GetProviderGroup(
                new Dictionary<string, object>()
                {
                    { "nameLike", name }
                },
                includedComponents,
                errorOnMultiple
                );
        }

        /// <summary>
        /// Get a collection of provider groups from the EDW by group name using like comparison
        /// </summary>
        /// <param name="name">Provider group name</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Collection of provider group records</returns>
        public IEnumerable<ProviderGroup> GetProviderGroupsByNameLike(string name, IEnumerable<ProviderGroupComponent> includedComponents = null)
        {
            return GetProviderGroups(
                new Dictionary<string, object>()
                {
                    { "name", name }
                },
                includedComponents
                );
        }
        #endregion

        #region Get Provider Group(s) By NPI
        /// <summary>
        /// Get a single provider group from the EDW by NPI
        /// </summary>
        /// <param name="npi">Provider group NPI</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <param name="errorOnMultiple">Whether the method should generate an error when multiple records are returned</param>
        /// <returns>Single provider group record</returns>
        public ProviderGroup GetProviderGroupByNPI(string npi, IEnumerable<ProviderGroupComponent> includedComponents = null, bool errorOnMultiple = DEFAULT_ERROR_ON_MULTIPLE)
        {
            return GetProviderGroup(
                new Dictionary<string, object>()
                {
                    { "id", npi },
                    { "idType", ProviderGroupNumberType.NPI }
                },
                includedComponents,
                errorOnMultiple
                );
        }

        /// <summary>
        /// Get a collection of provider groups from the EDW using a list of NPIs
        /// </summary>
        /// <param name="npis">Collection of provider group NPIs</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Collection of provider group records</returns>
        public IEnumerable<ProviderGroup> GetProviderGroupsByNPIs(IEnumerable<string> npis, IEnumerable<ProviderGroupComponent> includedComponents = null)
        {
            return GetProviderGroups(
                new Dictionary<string, object>()
                {
                    { "ids", npis },
                    { "idType", ProviderGroupNumberType.NPI }
                },
                includedComponents
                );
        }

        /// <summary>
        /// Get a data reader for provider groups from the EDW using a list of NPIs
        /// </summary>
        /// <param name="npis">Collection of provider group NPIs</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Data reader for provider group records</returns>
        public ProviderGroupDataReader GetProviderGroupDataReaderByNPIs(IEnumerable<string> npis, IEnumerable<ProviderGroupComponent> includedComponents = null)
        {
            return GetProviderGroupDataReader(
                new Dictionary<string, object>()
                {
                    { "ids", npis },
                    { "idType", ProviderGroupNumberType.NPI }
                },
                includedComponents
                );
        }
        #endregion

        #region Get Provider Group(s) By Provider ID
        /// <summary>
        /// Get a single provider group from the EDW by provider ID
        /// </summary>
        /// <param name="providerId">Provider group ID</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <param name="idType">Type of provider ID to lookup</param>
        /// <param name="errorOnMultiple">Whether the method should generate an error when multiple records are returned</param>
        /// <returns>Single provider group record</returns>
        public IEnumerable<ProviderGroup> GetProviderGroupsByProviderID(string providerId, IEnumerable<ProviderGroupComponent> includedComponents = null, ProviderNumberType idType = ProviderNumberType.BillingID)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object>();
            if (idType == ProviderNumberType.DatabaseKey)
                criteria.Add("providerId", int.Parse(providerId));
            else
                criteria.Add("providerId", providerId);
            criteria.Add("idType", idType);

            return GetProviderGroups(criteria, includedComponents);
        }

        /// <summary>
        /// Get a collection of provider groups from the EDW using a list of provider IDs
        /// </summary>
        /// <param name="providerIds">Collection of provider IDs</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <param name="idType">Type of provider ID to lookup</param>
        /// <returns>Collection of provider board records</returns>
        public IEnumerable<ProviderGroup> GetProviderGroupsByProviderIDs(IEnumerable<string> providerIds, IEnumerable<ProviderGroupComponent> includedComponents = null, ProviderNumberType idType = ProviderNumberType.BillingID)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object>();
            if (idType == ProviderNumberType.DatabaseKey)
                criteria.Add("providerIds", providerIds.Select(providerId => int.Parse(providerId)));
            else
                criteria.Add("providerIds", providerIds);
            criteria.Add("idType", idType);

            return GetProviderGroups(criteria, includedComponents);
        }
        #endregion

        #region Get Provider Groups Unfiltered
        /// <summary>
        /// Gets a collection of provider groups from the EDW, with no filtering applied
        /// </summary>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Collection of provider group records</returns>
        public IEnumerable<ProviderGroup> GetProviderGroupsUnfiltered(IEnumerable<ProviderGroupComponent> includedComponents = null)
        {
            return GetProviderGroups(null, includedComponents);
        }

        /// <summary>
        /// Gets a data reader for provider groups from the EDW, with no filtering applied
        /// </summary>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Data reader for provider group records</returns>
        public ProviderGroupDataReader GetProviderGroupDataReaderUnfiltered(IEnumerable<ProviderGroupComponent> includedComponents = null)
        {
            return GetProviderGroupDataReader(null, includedComponents);
        }
        #endregion


        #endregion

        ///////////////////////////////////////////////////////////////////////////////////////////

        #region Public Provider Language Methods


        #region Get Languages by Provider ID
        /// <summary>
        /// Get a collection of provider languages from the EDW by a provider ID
        /// </summary>
        /// <param name="id">Provider ID</param>
        /// <param name="idType">Type of provider ID to lookup</param>
        /// <returns>Collection of provider language records</returns>
        public IEnumerable<ProviderLanguage> GetProviderLanguagesByID(string id, ProviderNumberType idType = ProviderNumberType.BillingID)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object>();
            if (idType == ProviderNumberType.DatabaseKey)
                criteria.Add("id", int.Parse(id));
            else
                criteria.Add("id", id);
            criteria.Add("idType", idType);

            return GetProviderLanguages(criteria);
        }

        /// <summary>
        /// Get a collection of provider languages from the EDW using a list of IDs
        /// </summary>
        /// <param name="ids">Collection of provider IDs</param>
        /// <param name="idType">Type of provider ID to lookup</param>
        /// <returns>Collection of provider language records</returns>
        public IEnumerable<ProviderLanguage> GetProviderLanguagesByID(IEnumerable<string> ids, ProviderNumberType idType = ProviderNumberType.BillingID)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object>();
            if (idType == ProviderNumberType.DatabaseKey)
                criteria.Add("ids", ids.Select(id => int.Parse(id)));
            else
                criteria.Add("ids", ids);
            criteria.Add("idType", idType);

            return GetProviderLanguages(criteria);
        }
        #endregion

        #region Get Language by Provider NPI
        /// <summary>
        /// Get a collection of provider languages from the EDW by a provider NPI
        /// </summary>
        /// <param name="npi">Provider NPI</param>
        /// <returns>Collection of provider language records</returns>
        public IEnumerable<ProviderLanguage> GetProviderLanguagesByNPI(string npi)
        {
            return GetProviderLanguages(
                new Dictionary<string, object>()
                {
                    { "id", npi },
                    { "idType", ProviderNumberType.NPI }
                });
        }
        #endregion


        #endregion

        ///////////////////////////////////////////////////////////////////////////////////////////

        #region Public Provider License Methods


        #region Get Licenses by Provider ID
        /// <summary>
        /// Get a collection of provider licenses from the EDW by a provider ID
        /// </summary>
        /// <param name="id">Provider ID</param>
        /// <param name="idType">Type of provider ID to lookup</param>
        /// <returns>Collection of provider license records</returns>
        public IEnumerable<ProviderLicense> GetProviderLicensesByID(string id, ProviderNumberType idType = ProviderNumberType.BillingID)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object>();
            if (idType == ProviderNumberType.DatabaseKey)
                criteria.Add("id", int.Parse(id));
            else
                criteria.Add("id", id);
            criteria.Add("idType", idType);

            return GetProviderLicenses(criteria);
        }

        /// <summary>
        /// Get a collection of provider licenses from the EDW using a list of IDs
        /// </summary>
        /// <param name="ids">Collection of provider IDs</param>
        /// <param name="idType">Type of provider ID to lookup</param>
        /// <returns>Collection of provider license records</returns>
        public IEnumerable<ProviderLicense> GetProviderLicensesByID(IEnumerable<string> ids, ProviderNumberType idType = ProviderNumberType.BillingID)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object>();
            if (idType == ProviderNumberType.DatabaseKey)
                criteria.Add("ids", ids.Select(id => int.Parse(id)));
            else
                criteria.Add("ids", ids);
            criteria.Add("idType", idType);

            return GetProviderLicenses(criteria);
        }
        #endregion

        #region Get Licenses by Provider NPI
        /// <summary>
        /// Get a collection of provider licenses from the EDW by a provider NPI
        /// </summary>
        /// <param name="npi">Provider NPI</param>
        /// <returns>Collection of provider license records</returns>
        public IEnumerable<ProviderLicense> GetProviderLicensesByNPI(string npi)
        {
            return GetProviderLicenses(
                new Dictionary<string, object>()
                {
                    { "id", npi },
                    { "idType", ProviderNumberType.NPI }
                });
        }
        #endregion


        #endregion

        ///////////////////////////////////////////////////////////////////////////////////////////

        #region Public Provider Education Methods


        #region Get Education by Provider ID
        /// <summary>
        /// Get a collection of provider education from the EDW by a provider ID
        /// </summary>
        /// <param name="id">Provider ID</param>
        /// <param name="idType">Type of provider ID to lookup</param>
        /// <returns>Collection of provider education records</returns>
        public IEnumerable<ProviderEducation> GetProviderEducationByID(string id, ProviderNumberType idType = ProviderNumberType.BillingID)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object>();
            if (idType == ProviderNumberType.DatabaseKey)
                criteria.Add("id", int.Parse(id));
            else
                criteria.Add("id", id);
            criteria.Add("idType", idType);

            return GetProviderEducation(criteria);
        }

        /// <summary>
        /// Get a collection of provider education from the EDW using a list of IDs
        /// </summary>
        /// <param name="ids">Collection of provider IDs</param>
        /// <param name="idType">Type of provider ID to lookup</param>
        /// <returns>Collection of provider education records</returns>
        public IEnumerable<ProviderEducation> GetProviderEducationByID(IEnumerable<string> ids, ProviderNumberType idType = ProviderNumberType.BillingID)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object>();
            if (idType == ProviderNumberType.DatabaseKey)
                criteria.Add("ids", ids.Select(id => int.Parse(id)));
            else
                criteria.Add("ids", ids);
            criteria.Add("idType", idType);

            return GetProviderEducation(criteria);
        }
        #endregion

        #region Get Education by Provider NPI
        /// <summary>
        /// Get a collection of provider education from the EDW by a provider NPI
        /// </summary>
        /// <param name="npi">Provider NPI</param>
        /// <returns>Collection of provider education records</returns>
        public IEnumerable<ProviderEducation> GetProviderEducationByNPI(string npi)
        {
            return GetProviderEducation(
                new Dictionary<string, object>()
                {
                    { "id", npi },
                    { "idType", ProviderNumberType.NPI }
                });
        }
        #endregion


        #endregion

        ///////////////////////////////////////////////////////////////////////////////////////////

        #region Public Provider Specialty Methods


        #region Get Specialties by Provider ID
        /// <summary>
        /// Get a collection of provider specialties from the EDW by a provider ID
        /// </summary>
        /// <param name="id">Provider ID</param>
        /// <param name="idType">Type of provider ID to lookup</param>
        /// <returns>Collection of provider specialty records</returns>
        public IEnumerable<ProviderSpecialty> GetProviderSpecialtiesByID(string id, ProviderNumberType idType = ProviderNumberType.BillingID)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object>();
            if (idType == ProviderNumberType.DatabaseKey)
                criteria.Add("id", int.Parse(id));
            else
                criteria.Add("id", id);
            criteria.Add("idType", idType);

            return GetProviderSpecialties(criteria);
        }

        /// <summary>
        /// Get a collection of provider specialties from the EDW using a list of IDs
        /// </summary>
        /// <param name="ids">Collection of provider IDs</param>
        /// <param name="idType">Type of provider ID to lookup</param>
        /// <returns>Collection of provider specialty records</returns>
        public IEnumerable<ProviderSpecialty> GetProviderSpecialtiesByID(IEnumerable<string> ids, ProviderNumberType idType = ProviderNumberType.BillingID)
        {
            Dictionary<string, object> criteria = new Dictionary<string, object>();
            if (idType == ProviderNumberType.DatabaseKey)
                criteria.Add("ids", ids.Select(id => int.Parse(id)));
            else
                criteria.Add("ids", ids);
            criteria.Add("idType", idType);

            return GetProviderSpecialties(criteria);
        }
        #endregion

        #region Get Addresses by Provider NPI
        /// <summary>
        /// Get a collection of provider specialties from the EDW by a provider NPI
        /// </summary>
        /// <param name="npi">Provider NPI</param>
        /// <returns>Collection of provider specialty records</returns>
        public IEnumerable<ProviderSpecialty> GetProviderSpecialtiesByNPI(string npi)
        {
            return GetProviderSpecialties(
                new Dictionary<string, object>()
                {
                    { "id", npi },
                    { "idType", ProviderNumberType.NPI }
                });
        }
        #endregion


        #endregion

        ///////////////////////////////////////////////////////////////////////////////////////////

        #region Public Provider Benefit Networks Methods


        #region Get Benefit Networks by Provider ID
        /// <summary>
        /// Get a collection of benefit networks from the EDW by a provider ID
        /// </summary>
        /// <param name="id">Provider ID</param>
        /// <param name="idType">Type of provider ID to lookup</param>
        /// <returns>Collection of provider benefit network records</returns>
        public IEnumerable<BenefitNetwork> GetProviderBenefitNetworksByID(string id, ProviderNumberType idType = ProviderNumberType.BillingID)
        {
            return GetProviderBenefitNetworks(
                new Dictionary<string, object>()
                {
                    { "id", id },
                    { "idType", idType }
                });
        }

        /// <summary>
        /// Get a collection of provider benefit networks from the EDW using a list of IDs
        /// </summary>
        /// <param name="ids">Collection of provider IDs</param>
        /// <param name="idType">Type of provider ID to lookup</param>
        /// <returns>Collection of provider benefit network records</returns>
        public IEnumerable<BenefitNetwork> GetProviderBenefitNetworksByIDs(IEnumerable<string> ids, ProviderNumberType idType = ProviderNumberType.BillingID)
        {
            return GetProviderBenefitNetworks(
                new Dictionary<string, object>()
                {
                    { "ids", ids },
                    { "idType", idType }
                });
        }
        #endregion

        #region Get Benefit Networks by Provider NPI
        /// <summary>
        /// Get a collection of provider benefit networks from the EDW by a provider NPI
        /// </summary>
        /// <param name="npi">Provider NPI</param>
        /// <returns>Collection of provider benefit network records</returns>
        public IEnumerable<BenefitNetwork> GetProviderBenefitNetworksByNPI(string npi)
        {
            return GetProviderBenefitNetworks(
                new Dictionary<string, object>()
                {
                    { "id", npi },
                    { "idType", ProviderNumberType.NPI }
                });
        }
        #endregion


        #endregion

        ///////////////////////////////////////////////////////////////////////////////////////////

        #region Public Pharmacy Methods


        #region Get Pharmacies by Pharmacy ID
        /// <summary>
        /// Get a single pharmacy from the EDW by ID
        /// </summary>
        /// <param name="id">Pharmacy ID</param>
        /// <param name="errorOnMultiple">Whether the method should generate an error when multiple records are returned</param>
        /// <returns>Single pharmacy record</returns>
        public Pharmacy GetPharmacyByID(string id, bool errorOnMultiple = DEFAULT_ERROR_ON_MULTIPLE)
        {
            throw new NotImplementedException();

            //return GetPharmacy(
            //    new Dictionary<string, object>()
            //    {
            //        { "id", id }
            //    },
            //    errorOnMultiple
            //    );
        }

        /// <summary>
        /// Get a collection of pharmacies from the EDW using a list of IDs
        /// </summary>
        /// <param name="ids">Collection of pharmacy IDs</param>
        /// <returns>Collection of pharmacy records</returns>
        public IEnumerable<Pharmacy> GetPharmaciesByIDs(IEnumerable<string> ids)
        {
            throw new NotImplementedException();

            //return GetPharmacies(
            //    new Dictionary<string, object>()
            //    {
            //        { "ids", ids }
            //    });
        }

        /// <summary>
        /// Get a data reader for pharmacies from the EDW using a list of IDs
        /// </summary>
        /// <param name="ids">Collection of pharmacy IDs</param>
        /// <returns>Data reader for pharmacy records</returns>
        public PharmacyDataReader GetPharmacyDataReaderByIDs(IEnumerable<string> ids)
        {
            throw new NotImplementedException();

            //return GetPharmacyDataReader(
            //    new Dictionary<string, object>()
            //    {
            //        { "ids", ids }
            //    });
        }
        #endregion

        #region Get Pharmacies Unfiltered
        /// <summary>
        /// Gets a collection of pharmacies from the EDW, with no filtering applied
        /// </summary>
        /// <returns>Collection of pharmacy records</returns>
        public IEnumerable<Pharmacy> GetPharmaciesUnfiltered()
        {
            throw new NotImplementedException();

            //return GetPharmacies(null);
        }

        /// <summary>
        /// Gets a data reader for pharmacies from the EDW, with no filtering applied
        /// </summary>
        /// <returns>Data reader for pharmacy records</returns>
        public PharmacyDataReader GetPharmacyDataReaderUnfiltered()
        {
            throw new NotImplementedException();

            //return GetPharmacyDataReader(null);
        }
        #endregion


        #endregion

        ///////////////////////////////////////////////////////////////////////////////////////////

        #region Internal Provider Component Methods

        /// <summary>
        /// Add components to a list of providers
        /// </summary>
        /// <param name="providers">List of providers to add components to</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        internal void AddComponentsToProviders(IEnumerable<Provider> providers, IEnumerable<ProviderComponent> includedComponents)
        {
            if (includedComponents != null)
            {
                IEnumerable<string> databaseKeys = providers.Select(p => p.DatabaseKey);
                IEnumerable<string> billingIds = providers.Where(p => p.BillingID != null).Select(p => p.BillingID);
                foreach (ProviderComponent component in includedComponents)
                    switch(component)
                    {
                        case ProviderComponent.Addresses:
                            IEnumerable<ProviderAddress> addresses = Database.Providers.GetProviderAddressesByID(databaseKeys, idType: ProviderNumberType.DatabaseKey);
                            foreach (Provider provider in providers)
                                provider.Addresses = addresses.Where(a => a.ForeignDatabaseKey == provider.DatabaseKey);
                            break;
                        case ProviderComponent.Boards:
                            IEnumerable<ProviderBoard> boards = Database.Providers.GetProviderBoardsByID(databaseKeys, idType: ProviderNumberType.DatabaseKey);
                            foreach (Provider provider in providers)
                                provider.Boards = boards.Where(b => b.ForeignDatabaseKey == provider.DatabaseKey);
                            break;
                        case ProviderComponent.Education:
                            IEnumerable<ProviderEducation> education = Database.Providers.GetProviderEducationByID(databaseKeys, idType: ProviderNumberType.DatabaseKey);
                            foreach (Provider provider in providers)
                                provider.Education = education.Where(e => e.ForeignDatabaseKey == provider.DatabaseKey);
                            break;
                        case ProviderComponent.EntityAssignments:
                            IEnumerable<ProviderEntityAssignment> entityAssignments = Database.Providers.GetProviderEntityAssignmentsByID(databaseKeys, idType: ProviderNumberType.DatabaseKey);
                            foreach (Provider provider in providers)
                                provider.EntityAssignments = entityAssignments.Where(e => e.ForeignDatabaseKey == provider.DatabaseKey);
                            break;
                        case ProviderComponent.Groups:
                            IEnumerable<ProviderGroup> groups = Database.Providers.GetProviderGroupsByProviderIDs(databaseKeys, includedComponents: null, idType: ProviderNumberType.DatabaseKey);
                            foreach (Provider provider in providers)
                                provider.Groups = groups.Where(e => e.ForeignDatabaseKey == provider.DatabaseKey);
                            break;
                        case ProviderComponent.Languages:
                            IEnumerable<ProviderLanguage> languages = Database.Providers.GetProviderLanguagesByID(databaseKeys, idType: ProviderNumberType.DatabaseKey);
                            foreach (Provider provider in providers)
                                provider.Languages = languages.Where(l => l.ForeignDatabaseKey == provider.DatabaseKey);
                            break;
                        case ProviderComponent.Licenses:
                            IEnumerable<ProviderLicense> licenses = Database.Providers.GetProviderLicensesByID(databaseKeys, idType: ProviderNumberType.DatabaseKey);
                            foreach (Provider provider in providers)
                                provider.Licenses = licenses.Where(l => l.ForeignDatabaseKey == provider.DatabaseKey);
                            break;
                        case ProviderComponent.Specialties:
                            IEnumerable<ProviderSpecialty> specialties = Database.Providers.GetProviderSpecialtiesByID(databaseKeys, idType: ProviderNumberType.DatabaseKey);
                            foreach (Provider provider in providers)
                                provider.Specialties = specialties.Where(s => s.ForeignDatabaseKey == provider.DatabaseKey);
                            break;
                        case ProviderComponent.Networks:
                            IEnumerable<BenefitNetwork> networks = Database.Providers.GetProviderBenefitNetworksByIDs(billingIds, idType: ProviderNumberType.BillingID);
                            foreach (Provider provider in providers)
                                provider.Networks = networks.Where(n => n.ForeignDatabaseKey == provider.DatabaseKey);
                            break;
                        default:
                            throw new ArgumentException($"The included component is not valid: {component}", nameof(includedComponents));
                    }
            }
        }

        /// <summary>
        /// Add components to a list of provider groups
        /// </summary>
        /// <param name="groups">List of provider groups to add components to</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        internal void AddComponentsToProviderGroups(IEnumerable<ProviderGroup> groups, IEnumerable<ProviderGroupComponent> includedComponents)
        {
            if (includedComponents != null)
            {
                IEnumerable<string> grpDatabaseKeys = groups.Select(g => g.DatabaseKey);
                foreach (ProviderGroupComponent component in includedComponents)
                    switch (component)
                    {
                        case ProviderGroupComponent.Addresses:
                            IEnumerable<ProviderAddress> addresses = Database.Providers.GetProviderAddressesByGroupIDs(grpDatabaseKeys, idType: ProviderGroupNumberType.DatabaseKey);
                            foreach (ProviderGroup group in groups)
                                group.Addresses = addresses.Where(a => a.ForeignDatabaseKey == group.DatabaseKey);
                            break;
                        case ProviderGroupComponent.Providers:
                            // Get provider group to provider crosswalk records as a dictionary of lists
                            // The key of the dictionary is the group database key
                            // The value of the dictionary is a list of associated provider database keys
                            Dictionary<string, IEnumerable<string>> providerGroupToProviders = GetProviderGroupToProviderRecords(
                                new Dictionary<string, object>() { { "grpDatabaseKeys", grpDatabaseKeys.Select(key => int.Parse(key)) } })
                                .GroupBy(pgtp => pgtp.GroupDatabaseKey)
                                .ToDictionary(pgtp => pgtp.Key, pgtp => pgtp.Select(derp => derp.ProviderDatabaseKey));

                            // Get all of the unique provider records related to any of the groups (base data only)
                            IEnumerable<Provider> providers = Database.Providers.GetProvidersByIDs(providerGroupToProviders.Values.SelectMany(p => p).Distinct(), includedComponents: null, idType: ProviderNumberType.DatabaseKey);

                            // Merge the provider records into the existing group records
                            foreach (ProviderGroup group in groups)
                                group.Providers = providers.Where(p => providerGroupToProviders[group.DatabaseKey].Contains(p.DatabaseKey));
                            break;
                        default:
                            throw new ArgumentException($"The included component is not valid: {component}", nameof(includedComponents));
                    }
            }

        }

        #endregion

        ///////////////////////////////////////////////////////////////////////////////////////////

        #region Private Methods


        #region Get Provider(s) - Base Methods
        /// <summary>
        /// Get a single provider from the EDW
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <param name="errorOnMultiple">Whether the method should generate an error when multiple records are returned</param>
        /// <returns>Single provider record</returns>
        private Provider GetProvider(Dictionary<string, object> criteria, IEnumerable<ProviderComponent> includedComponents, bool errorOnMultiple)
        {
            IEnumerable<Provider> providers = GetProviders(criteria, includedComponents);
            if (providers.Count() > 1 && errorOnMultiple)
                throw new Exception($"The query returned multiple results: {providers.Count()} providers found");
            else if (providers.Any())
                return providers.First();
            else
                return null;
        }

        /// <summary>
        /// Get a collection of providers from the EDW
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Collection of provider records</returns>
        private IEnumerable<Provider> GetProviders(Dictionary<string, object> criteria, IEnumerable<ProviderComponent> includedComponents)
        {
            // Build provider query
            (string sql, object param, Dictionary<string, IEnumerable<object>> filterTables) = BuildProviderQuery(criteria);

            // Get base provider information
            IEnumerable<Provider> providers = Database.QueryWithFilters<Provider>(sql, param, filterTables);

            // Include other components
            AddComponentsToProviders(providers, includedComponents);

            return providers;
        }

        /// <summary>
        /// Get a data reader for providers from the EDW
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Provider data reader for EDW providers</returns>
        private ProviderDataReader GetProviderDataReader(Dictionary<string, object> criteria, IEnumerable<ProviderComponent> includedComponents)
        {
            // Build provider query
            (string sql, object param, Dictionary<string, IEnumerable<object>> filterTables) = BuildProviderQuery(criteria);

            // Get data reader
            return new ProviderDataReader(Database, sql, param, filterTables, includedComponents);
        }

        /// <summary>
        /// Generates the SQL and parameters necessary to get provider data from the EDW using the specified criteria
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <returns>
        /// 1 - SQL string to get provider base data from EDW
        /// 2 - param object for Dapper query parameter substitution
        /// 3 - list of tables to filter by with table contents
        /// </returns>
        private (string sql, object param, Dictionary<string, IEnumerable<object>> filterTables) BuildProviderQuery(Dictionary<string, object> criteria)
        {
            StringBuilder sql = new StringBuilder();
            Dictionary<string, object> param = new Dictionary<string, object>();
            Dictionary<string, IEnumerable<object>> filterTables = new Dictionary<string, IEnumerable<object>>();

            // Build sql to get base provider information
            sql.AppendLine($@"
select *
  from
      (
         select p.[provider_dim_key]       as [DatabaseKey]
               ,b_id.[BillingID]           as [BillingID]
               ,lb_id.[LegacyBillingID]    as [LegacyBillingID]
               ,case when rtrim(p.[other_id]) = '' then null else rtrim(p.[other_id]) end as [CredentialingID]
               ,rtrim(p.[first_name])      as [FirstName]
               ,rtrim(p.[middle_name])     as [MiddleName]
               ,rtrim(p.[last_name])       as [LastName]
               ,rtrim(p.[full_name])       as [FullName]
               ,rtrim(p.[name_suffix])     as [Suffix]
               ,case when rtrim(p.[medicare_number]) = '' then null else rtrim(p.[medicare_number]) end as [MedicareNumber]
               ,case when rtrim(p.[medicaid_number]) = '' then null else rtrim(p.[medicaid_number]) end as [MedicaidNumber]
               ,case when rtrim(p.[ssn]) = '' then null else rtrim(p.[ssn]) end as [SSN]
               ,case when rtrim(p.[upin]) = '' then null else rtrim(p.[upin]) end as [UPIN]
               ,case when rtrim(p.[npi]) = '' then null else rtrim(p.[npi]) end as [NPI]
               ,case when rtrim(p.[tax_id]) = '' then null else rtrim(p.[tax_id]) end as [TaxID]
               ,case when p.[birth_date_key] > 0
                     then convert([datetime], convert([varchar](10), p.[birth_date_key]))
                     else null
                end                        as [BirthDate]
               ,p.[gender]                 as [Sex]
               ,convert(bit, case when p.[provider_type] = 'p'
                                  then 'true'
                                  else 'false'
                              end)         as [IsIndividual]
               ,case when p.[provider_type] = 'p'
                     then null
                     else rtrim(p.[full_name])
                end                        as [InstitutionName]
               ,rtrim(p.[source_key])      as [SourceKey]
               ,row_number() over (order by p.[provider_dim_key] asc) as [rowNum]
           from [dbo].[provider_dim] p
           left join
               (
                  select b_ass.[provider_dim_key]       as [DatabaseKey]
                        ,rtrim(b_ass.[provider_hcc_id]) as [BillingID]
                        ,row_number() over
                        (
                            partition by b_ass.[provider_dim_key]
                                order by b_ass.[last_modified_date] desc
                                        ,b_ass.[provider_assignment_key] desc
                        )                               as [rowNum]
                    from [dbo].[provider_assignment_details] b_ass
                   where b_ass.[provider_hcc_id] is not null
                     and rtrim(b_ass.[provider_hcc_id]) <> ''
                     and substring(b_ass.[provider_hcc_id], 1, 2) = 'PR'
               ) b_id
             on b_id.[DatabaseKey] = p.[provider_dim_key]
            and b_id.[rowNum] = 1
           left join
               (
                  select lb_ass.[provider_dim_key]            as [DatabaseKey]
                        ,rtrim(lb_ass.[amisys_legacy_number]) as [LegacyBillingID]
                        ,row_number() over
                        (
                            partition by lb_ass.[provider_dim_key]
                                order by lb_ass.[last_modified_date] desc
                                        ,lb_ass.[provider_assignment_key] desc
                        )                                     as [rowNum]
                    from [dbo].[provider_assignment_details] lb_ass
                   where lb_ass.[amisys_legacy_number] is not null
                     and rtrim(lb_ass.[amisys_legacy_number]) <> ''
               ) lb_id
             on lb_id.[DatabaseKey] = p.[provider_dim_key]
            and lb_id.[rowNum] = 1
            ");

            #region Add limiting criteria

            #region Add join statements
            if (criteria != null)
            {
                string tableName;
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "ids":
                            switch ((ProviderNumberType)criteria["idType"])
                            {
                                case ProviderNumberType.BillingID:
                                    tableName = Database.GenerateTempTableName($"providers_{key}");
                                    filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                                    sql.AppendLine($"           join {tableName} tmp_{key}");
                                    sql.AppendLine($"             on b_id.[BillingID] = tmp_{key}.[value]");
                                    break;
                                case ProviderNumberType.CredentialingID:
                                    tableName = Database.GenerateTempTableName($"providers_{key}");
                                    filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                                    sql.AppendLine($"           join {tableName} tmp_{key}");
                                    sql.AppendLine($"             on rtrim(p.[other_id]) = tmp_{key}.[value]");
                                    break;
                                case ProviderNumberType.LegacyBillingID:
                                    tableName = Database.GenerateTempTableName($"providers_{key}");
                                    filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                                    sql.AppendLine($"           join {tableName} tmp_{key}");
                                    sql.AppendLine($"             on lb_id.[LegacyBillingID] = tmp_{key}.[value]");
                                    break;
                                case ProviderNumberType.NPI:
                                    tableName = Database.GenerateTempTableName($"providers_{key}");
                                    filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                                    sql.AppendLine($"           join {tableName} tmp_{key}");
                                    sql.AppendLine($"             on rtrim(p.[npi]) = tmp_{key}.[value]");
                                    break;
                                case ProviderNumberType.DatabaseKey:
                                    tableName = Database.GenerateTempTableName($"providers_{key}");
                                    filterTables.Add(tableName, ((IEnumerable<int>)criteria[key]).Cast<object>());
                                    sql.AppendLine($"           join {tableName} tmp_{key}");
                                    sql.AppendLine($"             on p.[provider_dim_key] = tmp_{key}.[value]");
                                    break;
                                default:
                                    throw new NotImplementedException($"Filtering by provider number type '{(ProviderNumberType)criteria["idType"]}' is not currently implemented");
                            }
                            break;
                    }
                }
            }
            #endregion

            sql.AppendLine("          where p.[provider_dim_key] >= 0");
            sql.AppendLine("            and p.[current_ind] = 1");

            #region Add where clauses
            if (criteria != null)
            {
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "id":
                            switch ((ProviderNumberType)criteria["idType"])
                            {
                                case ProviderNumberType.BillingID:
                                    param.Add(key, criteria[key]);
                                    sql.AppendLine("            and b_id.[BillingID] = @id");
                                    break;
                                case ProviderNumberType.CredentialingID:
                                    param.Add(key, criteria[key]);
                                    sql.AppendLine("            and rtrim(p.[other_id]) = @id");
                                    break;
                                case ProviderNumberType.LegacyBillingID:
                                    param.Add(key, criteria[key]);
                                    sql.AppendLine("            and lb_id.[LegacyBillingID] = @id");
                                    break;
                                case ProviderNumberType.NPI:
                                    param.Add(key, criteria[key]);
                                    sql.AppendLine("            and rtrim(p.[npi]) = @id");
                                    break;
                                case ProviderNumberType.DatabaseKey:
                                    param.Add(key, criteria[key]);
                                    sql.AppendLine("            and p.[provider_dim_key] = @id");
                                    break;
                                default:
                                    throw new NotImplementedException($"Filtering by provider number type '{(ProviderNumberType)criteria["idType"]}' is not currently implemented");
                            }
                            break;
                        case "facilityName":
                            param.Add(key, criteria[key]);
                            sql.AppendLine("            and rtrim(p.[full_name]) = @facilityName");
                            break;
                        case "firstName":
                            if (!string.IsNullOrWhiteSpace((string)criteria[key]))
                            {
                                param.Add(key, criteria[key]);
                                sql.AppendLine("            and rtrim(p.[first_name]) = @firstName");
                            }
                            break;
                        case "lastName":
                            if (!string.IsNullOrWhiteSpace((string)criteria[key]))
                            {
                                param.Add(key, criteria[key]);
                                sql.AppendLine("            and rtrim(p.[last_name]) = @lastName");
                            }
                            break;
                        case "facilityNameLike":
                            param.Add(key, criteria[key]);
                            sql.AppendLine("            and rtrim(p.[full_name]) like @facilityNameLike");
                            break;
                        case "firstNameLike":
                            if (!string.IsNullOrWhiteSpace((string)criteria[key]))
                            {
                                param.Add(key, criteria[key]);
                                sql.AppendLine("            and rtrim(p.[first_name]) like @firstNameLike");
                            }
                            break;
                        case "lastNameLike":
                            if (!string.IsNullOrWhiteSpace((string)criteria[key]))
                            {
                                param.Add(key, criteria[key]);
                                sql.AppendLine("            and rtrim(p.[last_name]) like @lastNameLike");
                            }
                            break;
                    }
                }
            }
            #endregion

            #region Add row number limiter
            sql.AppendLine("      ) prov");
            sql.AppendLine(" where prov.[rowNum] between @minRowNum and @maxRowNum");

            // Add min and max row numbers values (will be overwritten if this isdestined for a data reader)
            param.Add("minRowNum", 0);
            param.Add("maxRowNum", int.MaxValue);
            #endregion

            #endregion

            /// 1 - SQL string to get provider base data from EDW
            /// 2 - param object for Dapper query parameter substitution
            /// 3 - list of tables to filter by with table contents
            return (sql.ToString(), param.ToQueryParamObject(), filterTables);
        }
        #endregion

        #region Get Provider Addresses - Base Method
        /// <summary>
        /// Get a collection of provider addresses from the EDW
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <returns>Collection of provider address records</returns>
        private IEnumerable<ProviderAddress> GetProviderAddresses(Dictionary<string, object> criteria)
        {
            StringBuilder sql = new StringBuilder();
            Dictionary<string, object> param = new Dictionary<string, object>();
            Dictionary<string, IEnumerable<object>> filterTables = new Dictionary<string, IEnumerable<object>>();

            // Build sql to get provider addresses
            sql.AppendLine(@"
                select pa.provider_address_key as databasekey, pa.provider_dim_key as foreigndatabasekey, rtrim(pa.address_type) as addresstype, rtrim(pa.address_line1) as address1,
                rtrim(pa.address_line2) as address2, rtrim(pa.city) as city, rtrim(pa.state_code) as state, rtrim(pa.county) as county, rtrim(pa.zip_code) as zipcode, pa.source_key as sourcekey
                from provider_address_details pa
                join provider_dim p on pa.provider_dim_key = p.provider_dim_key
            ");

            #region Add limiting criteria

            #region Add join statements
            if (criteria != null)
            {
                string tableName;
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "ids":
                            switch ((ProviderNumberType)criteria["idType"])
                            {
                                case ProviderNumberType.CredentialingID:
                                    tableName = Database.GenerateTempTableName($"provaddresses_{key}");
                                    filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                                    sql.AppendLine($"  join {tableName} tmp_{key} on rtrim(p.other_id) = tmp_{key}.[value]");
                                    break;
                                case ProviderNumberType.NPI:
                                    tableName = Database.GenerateTempTableName($"provaddresses_{key}");
                                    filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                                    sql.AppendLine($"  join {tableName} tmp_{key} on rtrim(p.npi) = tmp_{key}.[value]");
                                    break;
                                case ProviderNumberType.DatabaseKey:
                                    tableName = Database.GenerateTempTableName($"provaddresses_{key}");
                                    filterTables.Add(tableName, ((IEnumerable<int>)criteria[key]).Cast<object>());
                                    sql.AppendLine($"  join {tableName} tmp_{key} on p.provider_dim_key = tmp_{key}.[value]");
                                    break;
                                default:
                                    throw new NotImplementedException($"Filtering by provider number type '{(ProviderNumberType)criteria["idType"]}' is not currently implemented");
                            }
                            break;
                    }
                }
            }
            #endregion

            sql.AppendLine(" where pa.provider_address_key >= 0");

            #region Add where clauses
            if (criteria != null)
            {
                foreach (string key in criteria.Keys)
                    switch (key)
                    {
                        case "id":
                            switch ((ProviderNumberType)criteria["idType"])
                            {
                                case ProviderNumberType.CredentialingID:
                                    param.Add(key, criteria[key]);
                                    sql.AppendLine("   and rtrim(p.other_id) = @id");
                                    break;
                                case ProviderNumberType.NPI:
                                    param.Add(key, criteria[key]);
                                    sql.AppendLine("   and rtrim(p.npi) = @id");
                                    break;
                                case ProviderNumberType.DatabaseKey:
                                    param.Add(key, criteria[key]);
                                    sql.AppendLine("   and p.provider_dim_key = @id");
                                    break;
                                default:
                                    throw new NotImplementedException($"Filtering by provider number type '{(ProviderNumberType)criteria["idType"]}' is not currently implemented");
                            }
                            break;
                    }
            }
            #endregion

            #endregion

            // Get provider addresses
            return Database.QueryWithFilters<ProviderAddress>(sql.ToString(), param.ToQueryParamObject(), filterTables);
        }
        #endregion

        #region Get Provider Board - Base Method
        /// <summary>
        /// Get a collection of provider boards from the EDW
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <returns>Collection of provider board records</returns>
        private IEnumerable<ProviderBoard> GetProviderBoards(Dictionary<string, object> criteria)
        {
            StringBuilder sql = new StringBuilder();
            Dictionary<string, object> param = new Dictionary<string, object>();
            Dictionary<string, IEnumerable<object>> filterTables = new Dictionary<string, IEnumerable<object>>();

            // Build sql to get provider boards
            sql.AppendLine(@"
                select pb.provider_board_key as databasekey, pb.provider_dim_key as foreigndatabasekey, rtrim(pb.name) as description, rtrim(pb.status) as status,
                rtrim(pb.certification_number) as certificationnumber,
                case when pb.recertification_date_key > 0 then convert(datetime, convert(varchar(10), pb.recertification_date_key)) else null end as recertificationdate,
                case when pb.certification_date_key > 0 then convert(datetime, convert(varchar(10), pb.certification_date_key)) else null end as startdate,
                case when pb.expiration_date_key > 0 then convert(datetime, convert(varchar(10), pb.expiration_date_key)) else null end as enddate
                from provider_board_details pb
                join provider_dim p on pb.provider_dim_key = p.provider_dim_key
            ");

            #region Add limiting criteria

            #region Add join statements
            if (criteria != null)
            {
                string tableName;
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "ids":
                            switch ((ProviderNumberType)criteria["idType"])
                            {
                                case ProviderNumberType.CredentialingID:
                                    tableName = Database.GenerateTempTableName($"provboards_{key}");
                                    filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                                    sql.AppendLine($"  join {tableName} tmp_{key} on rtrim(p.other_id) = tmp_{key}.[value]");
                                    break;
                                case ProviderNumberType.NPI:
                                    tableName = Database.GenerateTempTableName($"provboards_{key}");
                                    filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                                    sql.AppendLine($"  join {tableName} tmp_{key} on rtrim(p.npi) = tmp_{key}.[value]");
                                    break;
                                case ProviderNumberType.DatabaseKey:
                                    tableName = Database.GenerateTempTableName($"provboards_{key}");
                                    filterTables.Add(tableName, ((IEnumerable<int>)criteria[key]).Cast<object>());
                                    sql.AppendLine($"  join {tableName} tmp_{key} on p.provider_dim_key = tmp_{key}.[value]");
                                    break;
                                default:
                                    throw new NotImplementedException($"Filtering by provider number type '{(ProviderNumberType)criteria["idType"]}' is not currently implemented");
                            }
                            break;
                    }
                }
            }
            #endregion

            sql.AppendLine(" where pb.provider_board_key >= 0");

            #region Add where clauses
            if (criteria != null)
            {
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "id":
                            switch ((ProviderNumberType)criteria["idType"])
                            {
                                case ProviderNumberType.CredentialingID:
                                    param.Add(key, criteria[key]);
                                    sql.AppendLine("   and rtrim(p.other_id) = @id");
                                    break;
                                case ProviderNumberType.NPI:
                                    param.Add(key, criteria[key]);
                                    sql.AppendLine("   and rtrim(p.npi) = @id");
                                    break;
                                case ProviderNumberType.DatabaseKey:
                                    param.Add(key, criteria[key]);
                                    sql.AppendLine("   and p.provider_dim_key = @id");
                                    break;
                                default:
                                    throw new NotImplementedException($"Filtering by provider number type '{(ProviderNumberType)criteria["idType"]}' is not currently implemented");
                            }
                            break;
                    }
                }
            }
            #endregion

            #endregion

            // Get provider boards
            return Database.QueryWithFilters<ProviderBoard>(sql.ToString(), param.ToQueryParamObject(), filterTables);
        }
        #endregion

        #region Get Provider Entity Assignment - Base Method
        /// <summary>
        /// Get a collection of provider entity assignments from the EDW
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <returns>Collection of provider entity assignment records</returns>
        private IEnumerable<ProviderEntityAssignment> GetProviderEntityAssignments(Dictionary<string, object> criteria)
        {
            StringBuilder sql = new StringBuilder();
            Dictionary<string, object> param = new Dictionary<string, object>();
            Dictionary<string, IEnumerable<object>> filterTables = new Dictionary<string, IEnumerable<object>>();

            // Build sql to get provider entity assignments
            sql.AppendLine(@"
                select pa.provider_assignment_key as databasekey, pa.provider_dim_key as foreigndatabasekey, pa.provider_record_type as recordtype, rtrim(provider_record_desc) as assignment,
                rtrim(credential_status) as status, rtrim(assignment_category) as category, rtrim(assignment_category_code) as categorycode, rtrim(pa.source_key) as sourcekey,
                rtrim(pa.provider_hcc_id) as healthrulesproviderid, rtrim(pa.amisys_legacy_number) as amisysproviderid
                from provider_assignment_details pa
                join provider_dim p on pa.provider_dim_key = p.provider_dim_key
            ");

            #region Add limiting criteria

            #region Add join statements
            if (criteria != null)
            {
                string tableName;
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "ids":
                            switch ((ProviderNumberType)criteria["idType"])
                            {
                                case ProviderNumberType.CredentialingID:
                                    tableName = Database.GenerateTempTableName($"proveass_{key}");
                                    filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                                    sql.AppendLine($"  join {tableName} tmp_{key} on rtrim(p.other_id) = tmp_{key}.[value]");
                                    break;
                                case ProviderNumberType.NPI:
                                    tableName = Database.GenerateTempTableName($"proveass_{key}");
                                    filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                                    sql.AppendLine($"  join {tableName} tmp_{key} on rtrim(p.npi) = tmp_{key}.[value]");
                                    break;
                                case ProviderNumberType.DatabaseKey:
                                    tableName = Database.GenerateTempTableName($"proveass_{key}");
                                    filterTables.Add(tableName, ((IEnumerable<int>)criteria[key]).Cast<object>());
                                    sql.AppendLine($"  join {tableName} tmp_{key} on p.provider_dim_key = tmp_{key}.[value]");
                                    break;
                                default:
                                    throw new NotImplementedException($"Filtering by provider number type '{(ProviderNumberType)criteria["idType"]}' is not currently implemented");
                            }
                            break;
                    }
                }
            }
            #endregion

            sql.AppendLine(" where pa.provider_assignment_key >= 0");

            #region Add where clauses
            if (criteria != null)
            {
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "id":
                            switch ((ProviderNumberType)criteria["idType"])
                            {
                                case ProviderNumberType.CredentialingID:
                                    param.Add(key, criteria[key]);
                                    sql.AppendLine("   and rtrim(p.other_id) = @id");
                                    break;
                                case ProviderNumberType.NPI:
                                    param.Add(key, criteria[key]);
                                    sql.AppendLine("   and rtrim(p.npi) = @id");
                                    break;
                                case ProviderNumberType.DatabaseKey:
                                    param.Add(key, criteria[key]);
                                    sql.AppendLine("   and p.provider_dim_key = @id");
                                    break;
                                default:
                                    throw new NotImplementedException($"Filtering by provider number type '{(ProviderNumberType)criteria["idType"]}' is not currently implemented");
                            }
                            break;
                    }
                }
            }
            #endregion

            #endregion

            // Get provider entity assignments
            return Database.QueryWithFilters<ProviderEntityAssignment>(sql.ToString(), param.ToQueryParamObject(), filterTables);
        }
        #endregion

        #region Get Provider Group(s) - Base Methods
        /// <summary>
        /// Get a single provider group from the EDW
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <param name="errorOnMultiple">Whether the method should generate an error when multiple records are returned</param>
        /// <returns>Single provider group record</returns>
        private ProviderGroup GetProviderGroup(Dictionary<string, object> criteria, IEnumerable<ProviderGroupComponent> includedComponents, bool errorOnMultiple)
        {
            IEnumerable<ProviderGroup> groups = GetProviderGroups(criteria, includedComponents);
            if (groups.Count() > 1 && errorOnMultiple)
                throw new Exception($"The query returned multiple results: {groups.Count()} provider groups found");
            else if (groups.Any())
                return null;
            else
                return groups.First();
        }

        /// <summary>
        /// Get a collection of provider groups from the EDW
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Collection of provider group records</returns>
        private IEnumerable<ProviderGroup> GetProviderGroups(Dictionary<string, object> criteria, IEnumerable<ProviderGroupComponent> includedComponents)
        {
            // Build provider query
            (string sql, object param, Dictionary<string, IEnumerable<object>> filterTables) = BuildProviderGroupQuery(criteria);

            // Get base provider group information
            IEnumerable<ProviderGroup> groups = Database.QueryWithFilters<ProviderGroup>(sql.ToString(), param, filterTables);

            // Include other components
            AddComponentsToProviderGroups(groups, includedComponents);

            return groups;
        }

        /// <summary>
        /// Get a data reader for provider groups from the EDW
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Provider group data reader for EDW providers</returns>
        private ProviderGroupDataReader GetProviderGroupDataReader(Dictionary<string, object> criteria, IEnumerable<ProviderGroupComponent> includedComponents)
        {
            // Build provider query
            (string sql, object param, Dictionary<string, IEnumerable<object>> filterTables) = BuildProviderGroupQuery(criteria);

            // Get data reader
            return new ProviderGroupDataReader(Database, sql, param, filterTables, includedComponents);
        }

        /// <summary>
        /// Generates the SQL and parameters necessary to get provider group data from the EDW using the specified criteria
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <returns>
        /// 1 - SQL string to get provider group base data from EDW
        /// 2 - param object for Dapper query parameter substitution
        /// 3 - list of tables to filter by with table contents
        /// </returns>
        private (string sql, object param, Dictionary<string, IEnumerable<object>> filterTables) BuildProviderGroupQuery(Dictionary<string, object> criteria)
        {
            StringBuilder sql = new StringBuilder();
            Dictionary<string, object> param = new Dictionary<string, object>();
            Dictionary<string, IEnumerable<object>> filterTables = new Dictionary<string, IEnumerable<object>>();

            // Build sql to get base provider group information
            sql.AppendLine(@"
select *
  from
      (
         select sub.*
		       ,row_number() over ( order by sub.[ForeignDatabaseKey] asc, sub.[DatabaseKey] asc ) as [rowNum]
	       from
		       (
                  select distinct
	                     pg.[provider_group_dim_key] as [DatabaseKey]
	      	            ,p.[provider_dim_key]        as [ForeignDatabaseKey]
	      	            ,case when rtrim(pg.[group_id]) = '' then null else rtrim(pg.[group_id]) end as [CredentialingID]
                        ,rtrim(pg.[name])            as [Name]
	      	            ,case when rtrim(pg.[npi]) = '' then null else rtrim(pg.[npi]) end as [NPI]
	      	            ,case when rtrim(pg.[tax_id]) = '' then null else rtrim(pg.[tax_id]) end as [TaxID]
	      	            ,case when rtrim(p.[other_id]) = '' then null else rtrim(p.[other_id]) end as [ProviderCredentialingID]
	      	            ,case when rtrim(p.[npi]) = '' then null else rtrim(p.[npi]) end as [ProviderNPI]
                    from [provider_group_dim] pg
                    join [provider_to_group_details] pgd
	                  on pg.[provider_group_dim_key] = pgd.[provider_group_dim_key]
                    join [provider_dim] p
	                  on pgd.[provider_dim_key] = p.[provider_dim_key]
					 and p.[provider_dim_key] >= 0
                   where pg.[provider_group_dim_key] >= 0
               ) sub
            ");

            #region Add limiting criteria

            #region Add join statements
            if (criteria != null)
            {
                string tableName;
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "ids":
                            switch ((ProviderGroupNumberType)criteria["idType"])
                            {
                                case ProviderGroupNumberType.CredentialingID:
                                    tableName = Database.GenerateTempTableName($"provgrp_{key}");
                                    filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                                    sql.AppendLine($"            join {tableName} tmp_{key}");
                                    sql.AppendLine($"              on sub.[CredentialingID] = tmp_{key}.[value]");
                                    break;
                                case ProviderGroupNumberType.NPI:
                                    tableName = Database.GenerateTempTableName($"provgrp_{key}");
                                    filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                                    sql.AppendLine($"            join {tableName} tmp_{key}");
                                    sql.AppendLine($"              on sub.[NPI] = tmp_{key}.[value]");
                                    break;
                                case ProviderGroupNumberType.DatabaseKey:
                                    tableName = Database.GenerateTempTableName($"provgrp_{key}");
                                    filterTables.Add(tableName, ((IEnumerable<int>)criteria[key]).Cast<object>());
                                    sql.AppendLine($"            join {tableName} tmp_{key}");
                                    sql.AppendLine($"              on sub.[DatabaseKey] = tmp_{key}.[value]");
                                    break;
                                default:
                                    throw new NotImplementedException($"Filtering by provider group number type '{(ProviderGroupNumberType)criteria["idType"]}' is not currently implemented");
                            }
                            break;
                        case "providerIds":
                            switch ((ProviderNumberType)criteria["idType"])
                            {
                                case ProviderNumberType.CredentialingID:
                                    tableName = Database.GenerateTempTableName($"provgrp_{key}");
                                    filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                                    sql.AppendLine($"            join {tableName} tmp_{key}");
                                    sql.AppendLine($"              on sub.[ProviderCredentialingID] = tmp_{key}.[value]");
                                    break;
                                case ProviderNumberType.NPI:
                                    tableName = Database.GenerateTempTableName($"provgrp_{key}");
                                    filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                                    sql.AppendLine($"            join {tableName} tmp_{key}");
                                    sql.AppendLine($"              on sub.[ProviderNPI] = tmp_{key}.[value]");
                                    break;
                                case ProviderNumberType.DatabaseKey:
                                    tableName = Database.GenerateTempTableName($"provgrp_{key}");
                                    filterTables.Add(tableName, ((IEnumerable<int>)criteria[key]).Cast<object>());
                                    sql.AppendLine($"            join {tableName} tmp_{key}");
                                    sql.AppendLine($"              on sub.[ForeignDatabaseKey] = tmp_{key}.[value]");
                                    break;
                                default:
                                    throw new NotImplementedException($"Filtering by provider number type '{(ProviderNumberType)criteria["idType"]}' is not currently implemented");
                            }
                            break;
                    }
                }
            }
            #endregion

            sql.AppendLine("          where 1 = 1");

            #region Add where clauses
            if (criteria != null)
            {
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "id":
                            switch ((ProviderGroupNumberType)criteria["idType"])
                            {
                                case ProviderGroupNumberType.CredentialingID:
                                    param.Add(key, criteria[key]);
                                    sql.AppendLine("   and sub.[CredentialingID] = @id");
                                    break;
                                case ProviderGroupNumberType.NPI:
                                    param.Add(key, criteria[key]);
                                    sql.AppendLine("   and sub.[NPI] = @id");
                                    break;
                                case ProviderGroupNumberType.DatabaseKey:
                                    param.Add(key, criteria[key]);
                                    sql.AppendLine("   and sub.[DatabaseKey] = @id");
                                    break;
                                default:
                                    throw new NotImplementedException($"Filtering by provider group number type '{(ProviderNumberType)criteria["idType"]}' is not currently implemented");
                            }
                            break;
                        case "name":
                            param.Add(key, criteria[key]);
                            sql.AppendLine("            and sub.[Name] = @name");
                            break;
                        case "nameLike":
                            param.Add(key, criteria[key]);
                            sql.AppendLine("            and sub.[Name] like @nameLike");
                            break;
                        case "providerId":
                            switch ((ProviderNumberType)criteria["idType"])
                            {
                                case ProviderNumberType.CredentialingID:
                                    param.Add(key, criteria[key]);
                                    sql.AppendLine("   and sub.[ProviderCredentialingID] = @providerId");
                                    break;
                                case ProviderNumberType.NPI:
                                    param.Add(key, criteria[key]);
                                    sql.AppendLine("   and sub.[ProviderNPI] = @providerId");
                                    break;
                                case ProviderNumberType.DatabaseKey:
                                    param.Add(key, criteria[key]);
                                    sql.AppendLine("   and sub.[ForeignDatabaseKey] = @providerId");
                                    break;
                                default:
                                    throw new NotImplementedException($"Filtering by provider number type '{(ProviderNumberType)criteria["idType"]}' is not currently implemented");
                            }
                            break;
                    }
                }
            }
            #endregion

            #region Add row number limiter
            sql.AppendLine("      ) grp");
            sql.AppendLine(" where grp.[rowNum] between @minRowNum and @maxRowNum");

            // Add min and max row numbers values (will be overwritten if this isdestined for a data reader)
            param.Add("minRowNum", 0);
            param.Add("maxRowNum", int.MaxValue);
            #endregion

            #endregion

            /// 1 - SQL string to get provider group base data from EDW
            /// 2 - param object for Dapper query parameter substitution
            /// 3 - list of tables to filter by with table contents
            return (sql.ToString(), param.ToQueryParamObject(), filterTables);
        }
        #endregion

        #region Get Provider Language - Base Method
        /// <summary>
        /// Get a collection of provider languages from the EDW
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <returns>Collection of provider language records</returns>
        private IEnumerable<ProviderLanguage> GetProviderLanguages(Dictionary<string, object> criteria)
        {
            StringBuilder sql = new StringBuilder();
            Dictionary<string, object> param = new Dictionary<string, object>();
            Dictionary<string, IEnumerable<object>> filterTables = new Dictionary<string, IEnumerable<object>>();

            // Build sql to get provider languages
            sql.AppendLine(@"
                select pl.provider_language_key as databasekey, pl.provider_dim_key as foreigndatabasekey, rtrim(pl.language) as description
                from provider_language_details pl
                join provider_dim p on pl.provider_dim_key = p.provider_dim_key
            ");

            #region Add limiting criteria

            #region Add join statements
            if (criteria != null)
            {
                string tableName;
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "ids":
                            switch ((ProviderNumberType)criteria["idType"])
                            {
                                case ProviderNumberType.CredentialingID:
                                    tableName = Database.GenerateTempTableName($"provlang_{key}");
                                    filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                                    sql.AppendLine($"  join {tableName} tmp_{key} on rtrim(p.other_id) = tmp_{key}.[value]");
                                    break;
                                case ProviderNumberType.NPI:
                                    tableName = Database.GenerateTempTableName($"provlang_{key}");
                                    filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                                    sql.AppendLine($"  join {tableName} tmp_{key} on rtrim(p.npi) = tmp_{key}.[value]");
                                    break;
                                case ProviderNumberType.DatabaseKey:
                                    tableName = Database.GenerateTempTableName($"provlang_{key}");
                                    filterTables.Add(tableName, ((IEnumerable<int>)criteria[key]).Cast<object>());
                                    sql.AppendLine($"  join {tableName} tmp_{key} on p.provider_dim_key = tmp_{key}.[value]");
                                    break;
                                default:
                                    throw new NotImplementedException($"Filtering by provider number type '{(ProviderNumberType)criteria["idType"]}' is not currently implemented");
                            }
                            break;
                    }
                }
            }
            #endregion

            sql.AppendLine(" where pl.provider_language_key >= 0");

            #region Add where clauses
            if (criteria != null)
            {
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "id":
                            switch ((ProviderNumberType)criteria["idType"])
                            {
                                case ProviderNumberType.CredentialingID:
                                    param.Add(key, criteria[key]);
                                    sql.AppendLine("   and rtrim(p.other_id) = @id");
                                    break;
                                case ProviderNumberType.NPI:
                                    param.Add(key, criteria[key]);
                                    sql.AppendLine("   and rtrim(p.npi) = @id");
                                    break;
                                case ProviderNumberType.DatabaseKey:
                                    param.Add(key, criteria[key]);
                                    sql.AppendLine("   and p.provider_dim_key = @id");
                                    break;
                                default:
                                    throw new NotImplementedException($"Filtering by provider number type '{(ProviderNumberType)criteria["idType"]}' is not currently implemented");
                            }
                            break;
                    }
                }
            }
            #endregion

            #endregion

            // Get provider languages
            return Database.QueryWithFilters<ProviderLanguage>(sql.ToString(), param.ToQueryParamObject(), filterTables);
        }
        #endregion

        #region Get Provider License - Base Method
        /// <summary>
        /// Get a collection of provider licenses from the EDW
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <returns>Collection of provider license records</returns>
        private IEnumerable<ProviderLicense> GetProviderLicenses(Dictionary<string, object> criteria)
        {
            StringBuilder sql = new StringBuilder();
            Dictionary<string, object> param = new Dictionary<string, object>();
            Dictionary<string, IEnumerable<object>> filterTables = new Dictionary<string, IEnumerable<object>>();

            // Build sql to get provider licenses
            sql.AppendLine(@"
                select pl.provider_license_key as databasekey, pl.provider_dim_key as foreigndatabasekey, rtrim(pl.license_name) as description, rtrim(i.name) as institutionname, rtrim(pl.status) as status,
                rtrim(pl.license_number) as licensenumber, rtrim(pl.state) as licensestate,
                case when pl.awarded_date_key > 0 then convert(datetime, convert(varchar(10), pl.awarded_date_key)) else null end as startdate,
                case when pl.expiration_date_key > 0 then convert(datetime, convert(varchar(10), pl.expiration_date_key)) else null end as enddate
                from provider_licenses_details pl
                join provider_dim p on pl.provider_dim_key = p.provider_dim_key
                join institution_dim i on pl.institution_dim_key = i.institution_dim_key
            ");

            #region Add limiting criteria

            #region Add join statements
            if (criteria != null)
            {
                string tableName;
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "ids":
                            switch ((ProviderNumberType)criteria["idType"])
                            {
                                case ProviderNumberType.CredentialingID:
                                    tableName = Database.GenerateTempTableName($"provlic_{key}");
                                    filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                                    sql.AppendLine($"  join {tableName} tmp_{key} on rtrim(p.other_id) = tmp_{key}.[value]");
                                    break;
                                case ProviderNumberType.NPI:
                                    tableName = Database.GenerateTempTableName($"provlic_{key}");
                                    filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                                    sql.AppendLine($"  join {tableName} tmp_{key} on rtrim(p.npi) = tmp_{key}.[value]");
                                    break;
                                case ProviderNumberType.DatabaseKey:
                                    tableName = Database.GenerateTempTableName($"provlic_{key}");
                                    filterTables.Add(tableName, ((IEnumerable<int>)criteria[key]).Cast<object>());
                                    sql.AppendLine($"  join {tableName} tmp_{key} on p.provider_dim_key = tmp_{key}.[value]");
                                    break;
                                default:
                                    throw new NotImplementedException($"Filtering by provider number type '{(ProviderNumberType)criteria["idType"]}' is not currently implemented");
                            }
                            break;
                    }
                }
            }
            #endregion

            sql.AppendLine(" where pl.provider_license_key >= 0");

            #region Add where clauses
            if (criteria != null)
            {
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "id":
                            switch ((ProviderNumberType)criteria["idType"])
                            {
                                case ProviderNumberType.CredentialingID:
                                    param.Add(key, criteria[key]);
                                    sql.AppendLine("   and rtrim(p.other_id) = @id");
                                    break;
                                case ProviderNumberType.NPI:
                                    param.Add(key, criteria[key]);
                                    sql.AppendLine("   and rtrim(p.npi) = @id");
                                    break;
                                case ProviderNumberType.DatabaseKey:
                                    param.Add(key, criteria[key]);
                                    sql.AppendLine("   and p.provider_dim_key = @id");
                                    break;
                                default:
                                    throw new NotImplementedException($"Filtering by provider number type '{(ProviderNumberType)criteria["idType"]}' is not currently implemented");
                            }
                            break;
                    }
                }
            }
            #endregion

            #endregion

            // Get provider licenses
            return Database.QueryWithFilters<ProviderLicense>(sql.ToString(), param.ToQueryParamObject(), filterTables);
        }
        #endregion

        #region Get Provider Education - Base Method
        /// <summary>
        /// Get a collection of provider education from the EDW
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <returns>Collection of provider education records</returns>
        private IEnumerable<ProviderEducation> GetProviderEducation(Dictionary<string, object> criteria)
        {
            StringBuilder sql = new StringBuilder();
            Dictionary<string, object> param = new Dictionary<string, object>();
            Dictionary<string, IEnumerable<object>> filterTables = new Dictionary<string, IEnumerable<object>>();

            // Build sql to get provider education
            sql.AppendLine(@"
                select pe.provider_education_key as databasekey, pe.provider_dim_key as foreigndatabasekey, rtrim(pe.degree_name) as description,
                rtrim(pe.specialty) as specialty, rtrim(pe.program) as program, rtrim(i.name) as institution,
                case when start_date_key > 0 then convert(datetime, convert(varchar(10), start_date_key)) else null end as startdate,
                case when finish_date_key > 0 then convert(datetime, convert(varchar(10), finish_date_key)) else null end as enddate
                from provider_education_details pe
                join provider_dim p on pe.provider_dim_key = p.provider_dim_key
                join institution_dim i on pe.institution_dim_key = i.institution_dim_key
            ");

            #region Add limiting criteria

            #region Add join statements
            if (criteria != null)
            {
                string tableName;
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "ids":
                            switch ((ProviderNumberType)criteria["idType"])
                            {
                                case ProviderNumberType.CredentialingID:
                                    tableName = Database.GenerateTempTableName($"proved_{key}");
                                    filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                                    sql.AppendLine($"  join {tableName} tmp_{key} on rtrim(p.other_id) = tmp_{key}.[value]");
                                    break;
                                case ProviderNumberType.NPI:
                                    tableName = Database.GenerateTempTableName($"proved_{key}");
                                    filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                                    sql.AppendLine($"  join {tableName} tmp_{key} on rtrim(p.npi) = tmp_{key}.[value]");
                                    break;
                                case ProviderNumberType.DatabaseKey:
                                    tableName = Database.GenerateTempTableName($"proved_{key}");
                                    filterTables.Add(tableName, ((IEnumerable<int>)criteria[key]).Cast<object>());
                                    sql.AppendLine($"  join {tableName} tmp_{key} on p.provider_dim_key = tmp_{key}.[value]");
                                    break;
                                default:
                                    throw new NotImplementedException($"Filtering by provider number type '{(ProviderNumberType)criteria["idType"]}' is not currently implemented");
                            }
                            break;
                    }
                }
            }
            #endregion

            sql.AppendLine(" where pe.provider_education_key >= 0");

            #region Add where clauses
            if (criteria != null)
            {
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "id":
                            switch ((ProviderNumberType)criteria["idType"])
                            {
                                case ProviderNumberType.CredentialingID:
                                    param.Add(key, criteria[key]);
                                    sql.AppendLine("   and rtrim(p.other_id) = @id");
                                    break;
                                case ProviderNumberType.NPI:
                                    param.Add(key, criteria[key]);
                                    sql.AppendLine("   and rtrim(p.npi) = @id");
                                    break;
                                case ProviderNumberType.DatabaseKey:
                                    param.Add(key, criteria[key]);
                                    sql.AppendLine("   and p.provider_dim_key = @id");
                                    break;
                                default:
                                    throw new NotImplementedException($"Filtering by provider number type '{(ProviderNumberType)criteria["idType"]}' is not currently implemented");
                            }
                            break;
                    }
                }
            }
            #endregion

            #endregion

            // Get provider education
            return Database.QueryWithFilters<ProviderEducation>(sql.ToString(), param.ToQueryParamObject(), filterTables);
        }
        #endregion

        #region Get Provider Specialty - Base Method
        /// <summary>
        /// Get a collection of provider specialties from the EDW
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <returns>Collection of provider specialty records</returns>
        private IEnumerable<ProviderSpecialty> GetProviderSpecialties(Dictionary<string, object> criteria)
        {
            StringBuilder sql = new StringBuilder();
            Dictionary<string, object> param = new Dictionary<string, object>();
            Dictionary<string, IEnumerable<object>> filterTables = new Dictionary<string, IEnumerable<object>>();

            // Build sql to get provider specialties
            sql.AppendLine(@"
                select ps.provider_specialty_key as databasekey, ps.provider_dim_key as foreigndatabasekey, rtrim(ps.specialty) as description
                from provider_specialty_details ps
                join provider_dim p on ps.provider_dim_key = p.provider_dim_key
            ");

            #region Add limiting criteria

            #region Add join statements
            if (criteria != null)
            {
                string tableName;
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "ids":
                            switch ((ProviderNumberType)criteria["idType"])
                            {
                                case ProviderNumberType.CredentialingID:
                                    tableName = Database.GenerateTempTableName($"provspec_{key}");
                                    filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                                    sql.AppendLine($"  join {tableName} tmp_{key} on rtrim(p.other_id) = tmp_{key}.[value]");
                                    break;
                                case ProviderNumberType.NPI:
                                    tableName = Database.GenerateTempTableName($"provspec_{key}");
                                    filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                                    sql.AppendLine($"  join {tableName} tmp_{key} on rtrim(p.npi) = tmp_{key}.[value]");
                                    break;
                                case ProviderNumberType.DatabaseKey:
                                    tableName = Database.GenerateTempTableName($"provspec_{key}");
                                    filterTables.Add(tableName, ((IEnumerable<int>)criteria[key]).Cast<object>());
                                    sql.AppendLine($"  join {tableName} tmp_{key} on p.provider_dim_key = tmp_{key}.[value]");
                                    break;
                                default:
                                    throw new NotImplementedException($"Filtering by provider number type '{(ProviderNumberType)criteria["idType"]}' is not currently implemented");
                            }
                            break;
                    }
                }
            }
            #endregion

            sql.AppendLine(" where ps.provider_specialty_key >= 0");

            #region Add where clauses
            if (criteria != null)
            {
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "id":
                            switch ((ProviderNumberType)criteria["idType"])
                            {
                                case ProviderNumberType.CredentialingID:
                                    param.Add(key, criteria[key]);
                                    sql.AppendLine("   and rtrim(p.other_id) = @id");
                                    break;
                                case ProviderNumberType.NPI:
                                    param.Add(key, criteria[key]);
                                    sql.AppendLine("   and rtrim(p.npi) = @id");
                                    break;
                                case ProviderNumberType.DatabaseKey:
                                    param.Add(key, criteria[key]);
                                    sql.AppendLine("   and p.provider_dim_key = @id");
                                    break;
                                default:
                                    throw new NotImplementedException($"Filtering by provider number type '{(ProviderNumberType)criteria["idType"]}' is not currently implemented");
                            }
                            break;
                    }
                }
            }
            #endregion

            #endregion

            // Get provider specialties
            return Database.QueryWithFilters<ProviderSpecialty>(sql.ToString(), param.ToQueryParamObject(), filterTables);
        }
        #endregion

        #region Get Provider Benefit Network - Base Method
        /// <summary>
        /// Get a collection of provider benefit networks from the EDW
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <returns>Collection of provider benefit network records</returns>
        private IEnumerable<BenefitNetwork> GetProviderBenefitNetworks(Dictionary<string, object> criteria)
        {
            StringBuilder sql = new StringBuilder();
            Dictionary<string, object> param = new Dictionary<string, object>();
            Dictionary<string, IEnumerable<object>> filterTables = new Dictionary<string, IEnumerable<object>>();

            // Build sql to get provider benefit networks
            sql.AppendLine(@"
select ltrim(str(p_to_bn.[PRACTITIONER_HISTORY_FACT_KEY])) + '.' +
       ltrim(str(p_to_bn.[BENEFIT_NETWORK_KEY])) as [DatabaseKey]
      ,rtrim(p.[PRACTITIONER_HCC_ID])            as [ForeignDatabaseKey] /* Provider.BillingID */
      ,rtrim(bn.[BENEFIT_NETWORK_NAME])          as [Name]
      ,rtrim(bn.[BENEFIT_NETWORK_DESC])          as [Description]
      ,rtrim(bn.[BENEFIT_NETWORK_STATUS])        as [Status]
      ,bn_eff_date.[DATE_VALUE]                  as [EffectiveDate]
      ,bn_term_date.[DATE_VALUE]                 as [TerminationDate]
  from [landing].[PRACT_HIST_TO_BENEFIT_NETWORK] p_to_bn
  join [landing].[ALL_PRACTITIONER_HISTORY_FACT] p
    on p.[PRACTITIONER_HIST_FACT_KEY] = p_to_bn.[PRACTITIONER_HISTORY_FACT_KEY]
  join [landing].[BENEFIT_NETWORK_HISTORY_FACT] bn
    on bn.[BENEFIT_NETWORK_KEY] = p_to_bn.[BENEFIT_NETWORK_KEY]
  left join [landing].[DATE_DIMENSION] bn_eff_date
    on bn_eff_date.[DATE_KEY] = bn.[BENEFIT_NETWORK_EFF_DATE_KEY]
  left join [landing].[DATE_DIMENSION] bn_term_date
    on bn_term_date.[DATE_KEY] = bn.[BENEFIT_NETWORK_TERM_DATE_KEY]
            ");

            #region Add limiting criteria

            #region Add join statements
            if (criteria != null)
            {
                string tableName;
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "ids":
                            switch ((ProviderNumberType)criteria["idType"])
                            {
                                case ProviderNumberType.BillingID:
                                    tableName = Database.GenerateTempTableName($"provbennet_{key}");
                                    filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                                    sql.AppendLine($"  join {tableName} tmp_{key} on rtrim(p.[PRACTITIONER_HCC_ID]) = tmp_{key}.[value]");
                                    break;
                                default:
                                    throw new NotImplementedException($"Filtering by provider number type '{(ProviderNumberType)criteria["idType"]}' is not currently implemented");
                            }
                            break;
                    }
                }
            }
            #endregion

            sql.AppendLine(" where p_to_bn.[PRACTITIONER_HISTORY_FACT_KEY] >= 0");
            sql.AppendLine("   and p_to_bn.[BENEFIT_NETWORK_KEY] >= 0");

            #region Add where clauses
            if (criteria != null)
            {
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "id":
                            switch ((ProviderNumberType)criteria["idType"])
                            {
                                case ProviderNumberType.BillingID:
                                    param.Add(key, criteria[key]);
                                    sql.AppendLine("   and rtrim(p.[PRACTITIONER_HCC_ID]) = @id");
                                    break;
                                default:
                                    throw new NotImplementedException($"Filtering by provider number type '{(ProviderNumberType)criteria["idType"]}' is not currently implemented");
                            }
                            break;
                    }
                }
            }
            #endregion

            #endregion

            // Get provider benefit networks
            return new List<BenefitNetwork>(); // Database.QueryWithFilters<BenefitNetwork>(sql.ToString(), param.ToQueryParamObject(), filterTables);
        }
        #endregion

        #region Get Provider Group Addresses - Base Method
        /// <summary>
        /// Get a collection of provider group addresses from the EDW
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <returns>Collection of provider group address records</returns>
        private IEnumerable<ProviderAddress> GetProviderGroupAddresses(Dictionary<string, object> criteria)
        {
            StringBuilder sql = new StringBuilder();
            Dictionary<string, object> param = new Dictionary<string, object>();
            Dictionary<string, IEnumerable<object>> filterTables = new Dictionary<string, IEnumerable<object>>();

            // Build sql to get provider group addresses
            sql.AppendLine(@"
select ga.[provider_group_address_key] as [DatabaseKey]
      ,ga.[provider_group_dim_key]     as [ForeignDatabaseKey]
      ,rtrim(ga.[address_line_1])      as [Address1]
      ,rtrim(ga.[address_line_2])      as [Address2]
      ,rtrim(ga.[city])                as [City]
      ,rtrim(ga.[state])               as [State]
      ,rtrim(ga.[county])              as [County]
      ,rtrim(ga.[zip_code])            as [ZipCode]
      ,ga.[source_key]                 as [SourceKey]
  from [dbo].[provider_group_address_details] ga
  join [dbo].[provider_group_dim] pg
    on ga.[provider_group_dim_key] = pg.[provider_group_dim_key]
            ");

            #region Add limiting criteria

            #region Add join statements
            if (criteria != null)
            {
                string tableName;
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "groupIds":
                            switch ((ProviderGroupNumberType)criteria["idType"])
                            {
                                case ProviderGroupNumberType.CredentialingID:
                                    tableName = Database.GenerateTempTableName($"provgrpaddr_{key}");
                                    filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                                    sql.AppendLine($"            join {tableName} tmp_{key}");
                                    sql.AppendLine($"              on rtrim(pg.[other_id]) = tmp_{key}.[value]");
                                    break;
                                case ProviderGroupNumberType.NPI:
                                    tableName = Database.GenerateTempTableName($"provgrpaddr_{key}");
                                    filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                                    sql.AppendLine($"            join {tableName} tmp_{key}");
                                    sql.AppendLine($"              on rtrim(pg.[npi]) = tmp_{key}.[value]");
                                    break;
                                case ProviderGroupNumberType.DatabaseKey:
                                    tableName = Database.GenerateTempTableName($"provgrpaddr_{key}");
                                    filterTables.Add(tableName, ((IEnumerable<int>)criteria[key]).Cast<object>());
                                    sql.AppendLine($"            join {tableName} tmp_{key}");
                                    sql.AppendLine($"              on pg.[provider_group_dim_key] = tmp_{key}.[value]");
                                    break;
                                default:
                                    throw new NotImplementedException($"Filtering by provider group number type '{(ProviderGroupNumberType)criteria["idType"]}' is not currently implemented");
                            }
                            break;
                    }
                }
            }
            #endregion

            sql.AppendLine(" where ga.[provider_group_address_key] >= 0");

            #region Add where clauses
            if (criteria != null)
            {
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "groupId":
                            switch ((ProviderGroupNumberType)criteria["idType"])
                            {
                                case ProviderGroupNumberType.CredentialingID:
                                    param.Add(key, criteria[key]);
                                    sql.AppendLine("   and rtrim(pg.[other_id]) = @groupId");
                                    break;
                                case ProviderGroupNumberType.NPI:
                                    param.Add(key, criteria[key]);
                                    sql.AppendLine("   and rtrim(pg.[npi]) = @groupId");
                                    break;
                                case ProviderGroupNumberType.DatabaseKey:
                                    param.Add(key, criteria[key]);
                                    sql.AppendLine("   and pg.[provider_group_dim_key] = @groupId");
                                    break;
                                default:
                                    throw new NotImplementedException($"Filtering by provider group number type '{(ProviderNumberType)criteria["idType"]}' is not currently implemented");
                            }
                            break;
                    }
                }
            }
            #endregion

            #endregion

            // Get provider group addresses
            return Database.QueryWithFilters<ProviderAddress>(sql.ToString(), param.ToQueryParamObject(), filterTables);
        }
        #endregion

        #region Get Provider Group to Provider - Base Method
        /// <summary>
        /// Get a collection of provider group to provider records from the EDW
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <returns>Collection of provider group to provider records</returns>
        private IEnumerable<ProviderGroupToProvider> GetProviderGroupToProviderRecords(Dictionary<string, object> criteria)
        {
            StringBuilder sql = new StringBuilder();
            Dictionary<string, object> param = new Dictionary<string, object>();
            Dictionary<string, IEnumerable<object>> filterTables = new Dictionary<string, IEnumerable<object>>();

            // Build sql to get provider group to provider records
            sql.AppendLine(@"
select pg.[PROVIDER_GROUP_DIM_KEY] as [GroupDatabaseKey]
	  ,rtrim(pg.[OTHER_ID])        as [GroupCredentialingID]
      ,p.[PROVIDER_DIM_KEY]        as [ProviderDatabaseKey]
      ,rtrim(p.[OTHER_ID])         as [ProviderCredentialingID]
  from [dbo].[PROVIDER_TO_GROUP_DETAILS] pgd
  join [dbo].[PROVIDER_DIM] p
    on p.[PROVIDER_DIM_KEY] = pgd.[PROVIDER_DIM_KEY]
   and p.[PROVIDER_DIM_KEY] >= 0
  join [dbo].[PROVIDER_GROUP_DIM] pg
    on pg.[PROVIDER_GROUP_DIM_KEY] = pgd.[PROVIDER_GROUP_DIM_KEY]
   and pg.[PROVIDER_GROUP_DIM_KEY] >= 0
            ");

            #region Add limiting criteria

            #region Add join statements
            if (criteria != null)
            {
                string tableName;
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "grpDatabaseKeys":
                            tableName = Database.GenerateTempTableName($"grptoprov_{key}");
                            filterTables.Add(tableName, ((IEnumerable<int>)criteria[key]).Cast<object>());
                            sql.AppendLine($"  join {tableName} tmp_{key} on pg.[PROVIDER_GROUP_DIM_KEY] = tmp_{key}.[value]");
                            break;
                    }
                }
            }
            #endregion

            sql.AppendLine(" where pg.[PROVIDER_GROUP_DIM_KEY] >= 0");

            #region Add where clauses
            if (criteria != null)
            {
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "grpDatabaseKey":
                            param.Add(key, criteria[key]);
                            sql.AppendLine("   and pg.[PROVIDER_GROUP_DIM_KEY] = @grpDatabaseKey");
                            break;
                    }
                }
            }
            #endregion

            #endregion

            // Get provider group to provider records
            return Database.QueryWithFilters<ProviderGroupToProvider>(sql.ToString(), param.ToQueryParamObject(), filterTables);
        }
        #endregion

        #region Get Pharmacies - Base Methods
        /// <summary>
        /// Get a single pharmacy from the EDW
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <param name="errorOnMultiple">Whether the method should generate an error when multiple records are returned</param>
        /// <returns>Single pharmacy record</returns>
        private Pharmacy GetPharmacy(Dictionary<string, object> criteria, bool errorOnMultiple)
        {
            throw new NotImplementedException();

            //IEnumerable<Pharmacy> pharmacies = GetPharmacies(criteria);
            //if (pharmacies.Count() > 1 && errorOnMultiple)
            //    throw new Exception($"The query returned multiple results: {pharmacies.Count()} pharmacies found");
            //else if (pharmacies.Count() == 0)
            //    return null;
            //else
            //    return pharmacies.First();
        }

        /// <summary>
        /// Get a collection of pharmacies from the EDW
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <returns>Collection of pharmacy records</returns>
        private IEnumerable<Pharmacy> GetPharmacies(Dictionary<string, object> criteria)
        {
            throw new NotImplementedException();

            // Build Pharmacy query
            //(string sql, object param, Dictionary<string, IEnumerable<object>> filterTables) = BuildPharmacyQuery(criteria);

            // Get pharmacy information
            //return Database.QueryWithFilters<Pharmacy>(sql, param, filterTables);
        }

        /// <summary>
        /// Get a data reader for pharmacies from the EDW
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <returns>Pharmacy data reader for EDW pharmacies</returns>
        private PharmacyDataReader GetPharmacyDataReader(Dictionary<string, object> criteria)
        {
            throw new NotImplementedException();

            // Build pharmacy query
            //(string sql, object param, Dictionary<string, IEnumerable<object>> filterTables) = BuildPharmacyQuery(criteria);

            // Get data reader
            //return new PharmacyDataReader(Database, sql, param, filterTables);
        }

        /// <summary>
        /// Generates the SQL and parameters necessary to get provider data from the EDW using the specified criteria
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <returns>
        /// 1 - SQL string to get pharmacy data from EDW
        /// 2 - param object for Dapper query parameter substitution
        /// 3 - list of tables to filter by with table contents
        /// </returns>
        private (string sql, object param, Dictionary<string, IEnumerable<object>> filterTables) BuildPharmacyQuery(Dictionary<string, object> criteria)
        {
            throw new NotImplementedException();

            //StringBuilder sql = new StringBuilder();
            //Dictionary<string, object> param = new Dictionary<string, object>();
            //Dictionary<string, IEnumerable<object>> filterTables = new Dictionary<string, IEnumerable<object>>();

            //// Build sql to get pharmacy information
            //// TODO: retool for pharmacies
            ////sql.AppendLine($@"
            ////    select * from (

            ////    select provider_dim_key as databasekey, rtrim(other_id) as providernumber, rtrim(first_name) as firstname, rtrim(middle_name) as middlename, rtrim(last_name) as lastname,
            ////    rtrim(name_suffix) as suffix, rtrim(medicare_number) as medicarenumber, rtrim(medicaid_number) as medicaidnumber, rtrim(ssn) as ssn, rtrim(upin) as upin, rtrim(npi) as npi, rtrim(tax_id) as taxid,
            ////    case when birth_date_key > 0 then convert(datetime, convert(varchar(10), birth_date_key)) else null end as birthdate,
            ////    gender as sex,
            ////    convert(bit, case when provider_type='p' then 'true' else 'false' end) as isindividual,
            ////    case when provider_type='p' then '' else rtrim(full_name) end as institutionname,
            ////    row_number() over (order by provider_dim_key asc) as rowNum
            ////    from provider_dim

            ////    ) prov
            ////");

            //#region Add limiting criteria

            //#region Add join statements
            //if (criteria != null)
            //{
            //    string tableName;
            //    foreach (string key in criteria.Keys)
            //    {
            //        switch (key)
            //        {
            //            case "ids":
            //                // TODO: retool for pharmacies
            //                //tableName = Database.GenerateTempTableName($"providers_{key}");
            //                //filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
            //                //sql.AppendLine($"  join {tableName} tmp_{key} on rtrim(prov.[providernumber]) = tmp_{key}.[value]");
            //                break;
            //        }
            //    }
            //}
            //#endregion

            //#region Add row number limiter
            //// TODO: retool for pharmacies
            ////sql.AppendLine(" where prov.rowNum between @minRowNum and @maxRowNum");

            //// Add min and max row numbers values (will be overwritten if this is destined for a data reader)
            //param.Add("minRowNum", 0);
            //param.Add("maxRowNum", int.MaxValue);
            //#endregion

            //#region Add where clauses
            //if (criteria != null)
            //{
            //    foreach (string key in criteria.Keys)
            //    {
            //        switch (key)
            //        {
            //            case "id":
            //                // TODO: retool for pharmacies
            //                //param.Add(key, criteria[key]);
            //                //sql.AppendLine("   and rtrim(prov.other_id) = @id");
            //                break;
            //        }
            //    }
            //}
            //#endregion

            //#endregion

            ///// 1 - SQL string to get pharmacy data from EDW
            ///// 2 - param object for Dapper query parameter substitution
            ///// 3 - list of tables to filter by with table contents
            //return (sql.ToString(), param.ToQueryParamObject(), filterTables);
        }
        #endregion


        #endregion
    }
}

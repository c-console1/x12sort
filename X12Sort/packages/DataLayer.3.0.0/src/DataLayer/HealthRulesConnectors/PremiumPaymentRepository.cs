using System.Collections.Generic;
using System.Xml;
using DataLayer.Base;
using DataLayer.HealthRulesConnectors.Enumerations;
using DataLayer.HealthRulesConnectors.Models;
using DataLayer.HealthRulesConnectors.Models.Requests;
using DataLayer.HealthRulesConnectors.Models.Responses;

namespace DataLayer.HealthRulesConnectors
{
    public class PremiumPaymentRepository : BaseRepository<HealthRulesConnectorsDatabase>
    {

        /// <summary>
        /// Instaniate Premium Payment Call object
        /// </summary>
        /// <param name="database"></param>
        internal PremiumPaymentRepository(HealthRulesConnectorsDatabase database) : base(database)
        {

        }

        #region External Premium Payment Call - Submit

        /// <summary>
        /// Add/Update a Member's External Premium Payment
        /// </summary>
        /// <param name="source">.NET Request object that contains the add/update object</param>
        /// <returns>XML formated status string</returns>
        public string ExternalPremiumPaymentText(ExternalPremiumPaymentCoreRequest source)
        {
            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "ExternalPremiumPaymentCore"
            };

            string xml = objSoap.callObject.Get(conn, source.Get());
            objSoap = null;

            return xml;

        }

        /// <summary>
        /// Add/Update a Member's External Premium Payment
        /// </summary>
        /// <param name="source">.NET Request object that contains the add/update object</param>
        /// <returns>XML Document</returns>
        public XmlDocument ExternalPremiumPaymentXML(ExternalPremiumPaymentCoreRequest source)
        {
            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "ExternalPremiumPaymentCore"
            };

            string xml = objSoap.callObject.Get(conn, source.Get());
            objSoap = null;

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            return doc;

        }

        #endregion

    }
}

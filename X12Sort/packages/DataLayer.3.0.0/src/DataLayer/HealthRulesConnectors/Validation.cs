using System;
using System.Collections.Generic;
using System.Globalization;

namespace DataLayer.HealthRulesConnectors
{
    /// <summary>
    /// Format and validate data
    /// </summary>
    public static class Validation
    {
        private static CultureInfo ciEnglist = new CultureInfo("en-US", false);

        #region String Formatters

        /// <summary>
        /// Replace a value from a possible set of values
        /// </summary>
        /// <param name="source">Soure string</param>
        /// <param name="values">Value set</param>
        /// <param name="defaultValue">Default value is not matches were made</param>
        /// <returns></returns>
        public static string Replace(string source, Dictionary<string, string> values, string defaultValue = "")
        {
            if (source == null)
                throw new Exception("source is null");

            if (values == null)
                throw new Exception("values is null");

            string output = defaultValue;
            foreach (KeyValuePair<string, string> item in values)
                output = source == item.Key ? item.Value : output;

            return output;
        }

        /// <summary>
        /// Trim a string if possible, otherwise return null
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string TrimNull(string source)
        {
            if (source == null)
                return null;

            return source.Trim();
        }

        /// <summary>
        /// Throw an exception is the string is NULL ir whitespace
        /// </summary>
        /// <param name="source">Source string</param>
        /// <param name="exceptionText">Exception text</param>
        /// <returns>sTrimmed string</returns>
        public static string IsEmptyString(string source, string exceptionText)
        {
            if (string.IsNullOrWhiteSpace(source))
                throw new Exception(exceptionText);

            return source.Trim();
        }

        /// <summary>
        /// Is Integer?
        /// </summary>
        /// <param name="source"></param>
        /// <param name="errorField"></param>
        /// <returns></returns>
        public static string IsInt(string source, string errorField)
        {
            if (string.IsNullOrWhiteSpace(source))
                return null;

            int value;
            if (!int.TryParse(source.Trim(), out value))
                throw new Exception($"{errorField} is not an integer");

            return value.ToString(ciEnglist);
        }

        /// <summary>
        /// Is Integer?
        /// </summary>
        /// <param name="source"></param>
        /// <param name="errorField"></param>
        /// <returns></returns>
        public static string IsDecimal(string source, string errorField)
        {
            if (string.IsNullOrWhiteSpace(source))
                return null;

            if (!Decimal.TryParse(source.Trim(), out decimal value))
                throw new Exception($"{errorField} is not an decimal");

            return value.ToString();
        }

        /// <summary>
        /// Is Date?
        /// </summary>
        /// <param name="source"></param>
        /// <param name="errorField"></param>
        /// <returns></returns>
        public static string IsDate(string source, string errorField)
        {
            if (string.IsNullOrWhiteSpace(source))
                return null;

            if (!DateTime.TryParse(source.Trim(), out DateTime value))
                throw new Exception($"{errorField} is not an date");

            return value.ToString("yyyy-MM-dd");
        }

        /// <summary>
        /// Validate gender
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string Gender(
            string source, string errorField)
        {
            // errorField cannot be null
            if (errorField == null)
                throw new ArgumentNullException(nameof(errorField));

            // Not null, process
            if (!String.IsNullOrEmpty(source))
            {
                return source.Trim().ToUpper() switch
                {
                    // Female
                    "FEMALE" => "Female",
                    "F" => "Female",
                    "FEMAIL" => "Female",
                    // Male
                    "MALE" => "Male",
                    "M" => "Male",
                    "MAIL" => "Male",
                    // Unknown
                    "UNKNOWN" => "Unknown",
                    "U" => "Unknown",
                    // Blank
                    _ => ""
                };
            }

            return "";
        }

        #endregion

        #region Format Date / Time

        /// <summary>
        /// Format Date value to HealthRules standard
        /// </summary>
        /// <param name="source">Source date/time value - this can support most
        /// date formats</param>
        /// <param name="defaultValue">Value if it cannot be evaluated</param>
        /// <returns>If this source value can be evaluted change to HealthRule date
        /// standard. Otherwise return the default value</returns>
        public static string DateFormat(
            string source, string defaultValue = "")
        {
            // Blank entry, return default
            if (string.IsNullOrWhiteSpace(source))
                return defaultValue;

            // Try to parse date
            if (!DateTime.TryParse(source, out DateTime value))
                return defaultValue;
            return value.ToString("yyyy-MM-dd");
        }

        /// <summary>
        /// FOrmat time value
        /// </summary>
        /// <param name="source">Source date/time value - this can support most
        /// date/time formats</param>
        /// <param name="defaultValue">Value if it cannot be evaluated</param>
        /// <returns>If this source value can be evaluted change to HealthRule time
        /// standard. Otherwise return the default value</returns>
        public static string TimeFormat(
            string source, string defaultValue = "")
        {
            // Blank entry, return default
            if (string.IsNullOrWhiteSpace(source))
                return defaultValue;

            // Try to parse time
            if (!DateTime.TryParse("1/1/2000 " + source, out DateTime value))
                return defaultValue;

            return value.ToString("hh:mm:ss");
        }

        /// <summary>
        /// Parse date / time - used to get around a ref error
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static DateTime? ParseDate(
            string source)
        {
            if (DateTime.TryParse(source, out DateTime output))
            {
                return output;
            }

            return null;
        }

        /// <summary>
        /// Validate and format a date
        /// </summary>
        /// <param name="dateValue">String to evaluate</param>
        /// <returns></returns>
        public static string ValidateDate(
            string dateValue, bool allowBlankDates = true)
        {
            if (string.IsNullOrWhiteSpace(value: dateValue))
            {
                if (allowBlankDates)
                    return "";

                throw new Exception($"Blank dates are not allowed");
            }

            try
            {
                DateTime dateTime = DateTime.Parse(dateValue);
                return dateTime.ToString("yyyy-MM-dd");
            }
            catch (Exception)
            {
                throw new Exception($"'{dateValue}' is an invalid date.");
            }
        }

        /// <summary>
        /// Parse int - used to get around a ref error
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static int? ParseInt(string source)
        {
            if (source != null)
            {
                if (int.TryParse(source, out int output))
                {
                    return output;
                }
                return null;
            }

            return null;
        }

        #endregion

        #region True / False validation

        /// <summary>
        /// Format a string as a HealthRules 'true' or 'false'
        /// </summary>
        /// <param name="source">Source flag</param>
        /// <param name="defaultValue">Default value, default value is "" or blank</param>
        /// <returns></returns>
        public static string FormatTrueFalse(string source, string defaultValue = "")
        {
            if (string.IsNullOrWhiteSpace(source))
                return defaultValue;

            if (string.Equals("true", source, StringComparison.CurrentCultureIgnoreCase))
                return "true";

            if (string.Equals("false", source, StringComparison.CurrentCultureIgnoreCase))
                return "false";

            return defaultValue;
        }

        #endregion

        #region Validate for custom objects on XML that does nto have a concrete structure

        /// <summary>
        /// Validate test for building custom object from Raw XML
        /// </summary>
        /// <param name="targetName">Target name to evaulate a new for new</param>
        /// <param name="sourceName">Current XML tag's name</param>
        /// <param name="sourceValue">XML inner text</param>
        /// <param name="currentValue">Current value</param>
        /// <returns></returns>
        public static bool SeValueByNameBool(
            string targetName, string sourceName, string sourceValue, bool currentValue)
        {
            if (targetName is null)
            {
                throw new ArgumentNullException(nameof(targetName));
            }

            if (sourceName is null)
            {
                throw new ArgumentNullException(nameof(sourceName));
            }

            if (!string.Equals(targetName, sourceName, StringComparison.InvariantCultureIgnoreCase))
            {
                return currentValue;
            }

            return string.Equals(sourceValue, "true", StringComparison.InvariantCultureIgnoreCase);
        }

        /// <summary>
        /// Validate test for building custom object from Raw XML
        /// </summary>
        /// <param name="targetName">Target name to evaulate a new for new</param>
        /// <param name="sourceName">Current XML tag's name</param>
        /// <param name="sourceValue">XML inner text</param>
        /// <param name="currentValue">Current value</param>
        /// <returns></returns>
        public static int? SeValueByNameInt(
            string targetName, string sourceName, string sourceValue, int? currentValue)
        {
            if (targetName is null)
            {
                throw new ArgumentNullException(nameof(targetName));
            }

            if (sourceName is null)
            {
                throw new ArgumentNullException(nameof(sourceName));
            }

            if (targetName == sourceName)
                return ParseInt(sourceValue);

            return currentValue;
        }

        /// <summary>
        /// Validate test for building custom object from Raw XML
        /// </summary>
        /// <param name="targetName">Target name to evaulate a new for new</param>
        /// <param name="sourceName">Current XML tag's name</param>
        /// <param name="sourceValue">XML inner text</param>
        /// <param name="currentValue">Current value</param>
        /// <returns></returns>
        public static DateTime? SeValueByNameDateTime(
            string targetName, string sourceName, string sourceValue, DateTime? currentValue)
        {
            if (targetName != sourceName)
                return currentValue;

            return ParseDate(sourceValue);
        }

        /// <summary>
        /// Validate test for building custom object from Raw XML
        /// </summary>
        /// <param name="targetName">Target name to evaulate a new for new</param>
        /// <param name="sourceName">Current XML tag's name</param>
        /// <param name="sourceValue">XML inner text</param>
        /// <param name="currentValue">Current value</param>
        /// <returns></returns>
        public static string SeValueByNameString(string targetName, string sourceName, string sourceValue,
                                                 string currentValue)
        {
            if (targetName != sourceName)
                return currentValue;

            return sourceValue;
        }

        #endregion

        #region US State

        /// <summary>
        /// List of US States and related information
        /// Values are stored as: usStates.Add(new string[] { "AK", "ALASKA" });
        /// </summary>
        private static List<string[]> usStates = new List<string[]>();

        /// <summary>
        /// Find US state long name by 2 letter short code
        /// </summary>
        /// <param name="shortCode"></param>
        /// <param name="returnNull">Return value as a 'null' when no results are found
        /// True = yes, return 'null', False = no, return empty space</param>
        /// <returns></returns>
        public static string USStateLongName(string shortCode, bool returnNull = false)
        {
            // Fill usStates varible
            USStateFill();

            string result = null;
            usStates.ForEach(x => result = x[0] == shortCode.Trim().ToUpper() ? x[1] : result);

            //If returnNull is true then return the raw result of the Linq query, even if it's null.  Otherwise do further filtering on the results.
            if (returnNull)
            {
                return result;
            }
            else if (result == null)
            {
                return "";
            }
            else
            {
                return result;
            }
        }

        /// <summary>
        /// Find US state long name by 2 letter short code
        /// </summary>
        /// <param name="longName">Long name</param>
        /// <param name="returnNull">Return value as a 'null' when no results are found
        /// True = yes, return 'null', False = no, return empty space</param>
        /// <returns></returns>
        public static string USStateShortCode(string longName, bool returnNull = false)
        {
            // Fill usStates varible
            USStateFill();

            string result = null;
            usStates.ForEach(x => result = x[1] == longName.Trim().ToUpper() ? x[0] : result);
            if (returnNull)
            {
                return result;
            }
            else if (result == null)
            {
                return "";
            }
            else
            {
                return result;
            }
        }

        /// <summary>
        /// Fill the 'usStates' variable
        /// </summary>
        private static void USStateFill()
        {
            // Values based on HealthRules: http://100.112.32.164:9191/connector/services/v4/MemberCoverageEventLookup?xsd=us-state-code.xsd
            // Only fill if empty, don't waste memory and CPU cycles
            if (usStates.Count == 0)
            {
                usStates.Add(new string[] { "AK", "ALASKA" });
                usStates.Add(new string[] { "AL", "ALABAMA" });
                usStates.Add(new string[] { "AR", "ARKANSAS" });
                usStates.Add(new string[] { "AS", "AMERICAN SAMOA" });
                usStates.Add(new string[] { "AZ", "ARIZONA" });
                usStates.Add(new string[] { "CA", "CALIFORNIA" });
                usStates.Add(new string[] { "CO", "COLORADO" });
                usStates.Add(new string[] { "CT", "CONNECTICUT" });
                usStates.Add(new string[] { "DC", "DISTRICT OF COLUMBIA" });
                usStates.Add(new string[] { "DE", "DELAWARE" });
                usStates.Add(new string[] { "FL", "FLORIDA" });
                usStates.Add(new string[] { "FM", "FEDERATED STATES OF MICRONESIA" });
                usStates.Add(new string[] { "GA", "GEORGIA" });
                usStates.Add(new string[] { "GU", "GUAM" });
                usStates.Add(new string[] { "HI", "HAWAII" });
                usStates.Add(new string[] { "IA", "IOWA" });
                usStates.Add(new string[] { "ID", "IDAHO" });
                usStates.Add(new string[] { "IL", "ILLINOIS" });
                usStates.Add(new string[] { "IN", "INDIANA" });
                usStates.Add(new string[] { "KS", "KANSAS" });
                usStates.Add(new string[] { "KY", "KENTUCKY" });
                usStates.Add(new string[] { "LA", "LOUISIANA" });
                usStates.Add(new string[] { "MA", "MASSACHUSETTS" });
                usStates.Add(new string[] { "MD", "MARYLAND" });
                usStates.Add(new string[] { "ME", "MAINE" });
                usStates.Add(new string[] { "MH", "MARSHALL ISLANDS" });
                usStates.Add(new string[] { "MI", "MICHIGAN" });
                usStates.Add(new string[] { "MN", "MINNESOTA" });
                usStates.Add(new string[] { "MO", "MISSOURI" });
                usStates.Add(new string[] { "MP", "NORTHERN MARIANA ISLANDS" });
                usStates.Add(new string[] { "MS", "MISSISSIPPI" });
                usStates.Add(new string[] { "MT", "MONTANA" });
                usStates.Add(new string[] { "NC", "NORTH CAROLINA" });
                usStates.Add(new string[] { "ND", "NORTH DAKOTA" });
                usStates.Add(new string[] { "NE", "NEBRASKA" });
                usStates.Add(new string[] { "NH", "NEW HAMPSHIRE" });
                usStates.Add(new string[] { "NJ", "NEW JERSEY" });
                usStates.Add(new string[] { "NM", "NEW MEXICO" });
                usStates.Add(new string[] { "NV", "NEVADA" });
                usStates.Add(new string[] { "NY", "NEW YORK" });
                usStates.Add(new string[] { "OH", "OHIO" });
                usStates.Add(new string[] { "OK", "OKLAHOMA" });
                usStates.Add(new string[] { "OR", "OREGON" });
                usStates.Add(new string[] { "PA", "PENNSYLVANIA" });
                usStates.Add(new string[] { "PR", "PUERTO RICO" });
                usStates.Add(new string[] { "PW", "PALAU" });
                usStates.Add(new string[] { "RI", "RHODE ISLAND" });
                usStates.Add(new string[] { "SC", "SOUTH CAROLINA" });
                usStates.Add(new string[] { "SD", "SOUTH DAKOTA" });
                usStates.Add(new string[] { "TN", "TENNESSEE" });
                usStates.Add(new string[] { "TX", "TEXAS" });
                usStates.Add(new string[] { "UT", "UTAH" });
                usStates.Add(new string[] { "VA", "VIRGINIA" });
                usStates.Add(new string[] { "VI", "VIRGIN ISLANDS" });
                usStates.Add(new string[] { "VT", "VERMONT" });
                usStates.Add(new string[] { "WA", "WASHINGTON" });
                usStates.Add(new string[] { "WI", "WISCONSIN" });
                usStates.Add(new string[] { "WV", "WEST VIRGINIA" });
                usStates.Add(new string[] { "WY", "WYOMING" });
            }
        }

        #endregion

        /// <summary>
        /// Validate TypeCode object
        /// </summary>
        /// <param name="typeCode"></param>
        /// <returns></returns>
        public static bool ValidValueSet(DataLayer.HealthRulesConnectors.TypeCode typeCode, bool isOptional = false)
        {
            if (typeCode == null && isOptional)
                return true;

            return !(string.IsNullOrEmpty(typeCode.codeEntry)
                && string.IsNullOrEmpty(typeCode.shortName));
        }

        /// <summary>
        /// Enumerated Type based 20.2_HealthRules_Connector_Guide_Vol5_Extracts_and_Services.pdf
        /// Chapter 19: Enumerated Types Appendix, page 1184
        /// </summary>
        /// <returns></returns>
        public static List<Models.EnumeratedType> GetEnumeratedTypes()
        {
            List<Models.EnumeratedType> output = new List<Models.EnumeratedType>();

            // GenderCodeType
            output.Add(new Models.EnumeratedType("GenderCodeType", "Male", "M"));
            output.Add(new Models.EnumeratedType("GenderCodeType", "Female", "F"));
            output.Add(new Models.EnumeratedType("GenderCodeType", "Undifferentiated"));
            output.Add(new Models.EnumeratedType("GenderCodeType", "Unknown"));

            // MaritalStatusCodeType
            output.Add(new Models.EnumeratedType("MaritalStatusCodeType", "Separated"));
            output.Add(new Models.EnumeratedType("MaritalStatusCodeType", "Unmarried"));
            output.Add(new Models.EnumeratedType("MaritalStatusCodeType", "Common Law"));
            output.Add(new Models.EnumeratedType("MaritalStatusCodeType", "Divorced"));
            output.Add(new Models.EnumeratedType("MaritalStatusCodeType", "Legally Separated"));
            output.Add(new Models.EnumeratedType("MaritalStatusCodeType", "Living together"));
            output.Add(new Models.EnumeratedType("MaritalStatusCodeType", "Interlocutory"));
            output.Add(new Models.EnumeratedType("MaritalStatusCodeType", "Married"));
            output.Add(new Models.EnumeratedType("MaritalStatusCodeType", "Annulled"));
            output.Add(new Models.EnumeratedType("MaritalStatusCodeType", "Other"));
            output.Add(new Models.EnumeratedType("MaritalStatusCodeType", "Domestic partner"));
            output.Add(new Models.EnumeratedType("MaritalStatusCodeType", "Single"));
            output.Add(new Models.EnumeratedType("MaritalStatusCodeType", "Unreported"));
            output.Add(new Models.EnumeratedType("MaritalStatusCodeType", "Unknown"));
            output.Add(new Models.EnumeratedType("MaritalStatusCodeType", "Widowed", "Widow", "Widower"));

            // EthnicityCodeType
            output.Add(new Models.EnumeratedType("EthnicityCodeType", "Not Provided"));
            output.Add(new Models.EnumeratedType("EthnicityCodeType", "Not Applicable"));
            output.Add(new Models.EnumeratedType("EthnicityCodeType", "Asian or Pacific Islander"));
            output.Add(new Models.EnumeratedType("EthnicityCodeType", "Black"));
            output.Add(new Models.EnumeratedType("EthnicityCodeType", "Caucasian"));
            output.Add(new Models.EnumeratedType("EthnicityCodeType", "Subcontinent Asian American"));
            output.Add(new Models.EnumeratedType("EthnicityCodeType", "Other Race or Ethnicity"));
            output.Add(new Models.EnumeratedType("EthnicityCodeType", "Asian Pacific American"));
            output.Add(new Models.EnumeratedType("EthnicityCodeType", "Native American"));
            output.Add(new Models.EnumeratedType("EthnicityCodeType", "Hispanic"));
            output.Add(new Models.EnumeratedType("EthnicityCodeType", "American Indian or Alaskan Native"));
            output.Add(new Models.EnumeratedType("EthnicityCodeType", "Native Hawaiian"));
            output.Add(new Models.EnumeratedType("EthnicityCodeType", "Black (Non-Hispanic)"));
            output.Add(new Models.EnumeratedType("EthnicityCodeType", "White (Non-Hispanic)"));
            output.Add(new Models.EnumeratedType("EthnicityCodeType", "Pacific Islander"));
            output.Add(new Models.EnumeratedType("EthnicityCodeType", "Mutually Defined"));

            // EmploymentStatusCodeType"
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", ""));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Leave of Absence with Pay"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Leave of Absence without Pay"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Active"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Apprenticeship Full-time"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Active Reserve"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Flexible Work Plan"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Employee/Employer defined work hours"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Alerted"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Assigned"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Affiliated with Outside Organization"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Adjunct"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Active Military - Overseas"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Apprenticeship Part-time"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Apprenticeship"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Academy Student"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Presidential Appointee"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Active Military - USA"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Non-applicable Employment Status Category"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Contractor"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Consolidated Omnibus Budget Reconciliation"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Act (COBRA)"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Continued"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Discharged or Terminated for Cause"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Dishonorably Discharged"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Deceased"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Disqualified: Medical or Physical Condition"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Disqualified: Other"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Disabled"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Employed by Outside Organization"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Furloughed: Job Abolished, Force Reduction"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Furloughed: Bumped or Displaced"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Furloughed: Facility Closed"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Furloughed: Other"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Full-time"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Honorably Discharged"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Inactive"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Inactive Reserves"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Leave of Absence"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Administrative Leave of Absence"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Annual Leave of Absence"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Leave of Absence due to Bereavement"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Jury Duty"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Suspension"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Sabbatical Leave of Absence"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Leave of Absence: Personal"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Leave of Absence: Education"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Leave of Absence: Family Medical Leave Act"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "(FMLA)"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Leave of Absence: Maternity"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Leave of Absence for Non-Military Government"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Request Other Than Jury Duty"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Leave of Absence: Sickness"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Leave of Absence: Union"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Leave of Absence: Without Permission, Unauthorized"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Leave of Absence: Military"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Not Employed"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "On Strike"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Other"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Promoted"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Part-time Contractual"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Plan to Enlist"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Permanent"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Part-time Noncontractual"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Probationary"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Part-time"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Previous"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Piece Worker"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Resigned: Retired"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Relocated"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Reassigned"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Resigned: Moved"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Recommissioned"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Resigned: Injury"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Retired Military - Overseas"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Resigned: Personal Reasons"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Retired Without Recall"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Retired"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Retired Military - USA"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Dual Retired Status Retiree is working"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Resigned: Accepted Separation Allowance"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Separated"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Self-Employed"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Seasonal"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Suspended"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Terminated"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Temporary Full-Time"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Temporary"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Tenured"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Temporary Part-Time"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Transferred"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Unknown"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Volunteer"));
            output.Add(new Models.EnumeratedType("EmploymentStatusCodeType", "Extra Duties Not Requiring Certification"));

            // PhoneCountryCodeType
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "93"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "355"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "213"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "684"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "376"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "244"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "1-264"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "672"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "1-268"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "54"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "374"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "297"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "247"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "61"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "43"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "994"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "1-242"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "973"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "880"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "1-246"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "375"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "32"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "501"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "229"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "1-441"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "975"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "591"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "387"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "267"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "55"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "1-284"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "673"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "359"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "226"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "257"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "855"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "237"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "238"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "1-345"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "236"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "235"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "56"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "86"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "61-8"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "57"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "269"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "242"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "243"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "682"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "506"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "385"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "53"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "5399"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "357"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "420"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "45"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "246"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "253"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "1-767"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "1-809"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "670"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "593"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "20"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "503"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "88213"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "240"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "291"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "372"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "251"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "298"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "500"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "679"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "358"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "33"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "596"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "594"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "689"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "241"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "220"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "995"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "49"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "233"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "350"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "881"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "30"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "299"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "1-473"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "590"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "1-671"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "502"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "224"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "245"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "592"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "509"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "504"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "852"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "36"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "354"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "91"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "62"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "871"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "874"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "873"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "872"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "870"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "98"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "964"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "353"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "8816"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "8817"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "972"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "39"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "225"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "1-876"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "81"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "962"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "254"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "686"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "850"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "82"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "965"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "996"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "856"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "371"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "961"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "266"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "231"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "218"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "423"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "370"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "352"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "853"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "389"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "261"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "265"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "60"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "960"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "223"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "356"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "692"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "222"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "230"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "52"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "691"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "1-808"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "373"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "377"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "976"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "1-664"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "212"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "258"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "95"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "264"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "674"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "977"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "31"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "599"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "687"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "64"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "505"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "227"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "234"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "683"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "1-670"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "47"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "968"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "92"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "680"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "970"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "507"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "675"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "595"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "51"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "63"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "48"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "351"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "1-787"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "1-939"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "974"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "262"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "40"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "7"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "250"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "378"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "239"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "966"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "221"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "381"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "248"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "232"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "65"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "421"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "386"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "677"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "252"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "27"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "34"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "94"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "290"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "1-869"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "1-758"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "508"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "1-784"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "249"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "597"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "268"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "46"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "41"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "963"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "886"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "992"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "255"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "66"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "88216"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "228"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "690"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "676"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "1-868"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "216"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "90"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "993"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "1-649"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "688"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "256"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "380"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "971"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "44"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "1"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "878"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "598"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "1-340"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "998"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "678"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "58"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "84"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "808"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "681"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "685"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "967"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "260"));
            output.Add(new Models.EnumeratedType("PhoneCountryCodeType", "263"));

            // ToothNameDomainType
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "1"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "2"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "3"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "4"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "5"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "6"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "7"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "8"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "9"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "10"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "11"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "12"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "13"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "14"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "15"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "16"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "17"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "18"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "19"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "20"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "21"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "22"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "23"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "24"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "25"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "26"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "27"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "28"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "29"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "30"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "31"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "32"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "A"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "B"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "C"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "D"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "E"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "F"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "G"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "H"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "I"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "J"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "K"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "L"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "M"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "N"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "O"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "P"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "Q"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "R"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "S"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "T"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "51"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "52"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "53"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "54"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "55"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "56"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "57"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "58"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "59"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "60"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "61"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "62"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "63"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "64"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "65"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "66"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "67"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "68"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "69"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "70"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "71"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "72"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "73"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "74"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "75"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "76"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "77"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "78"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "79"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "80"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "81"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "82"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "AS"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "BS"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "CS"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "DS"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "ES"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "FS"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "GS"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "HS"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "IS"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "JS"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "KS"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "LS"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "MS"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "NS"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "OS"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "PS"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "QS"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "RS"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "SS"));
            output.Add(new Models.EnumeratedType("ToothNameDomainType", "TS"));

            // USStateTerritoryCode
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "AL"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "AK"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "AS"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "AZ"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "AR"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "CA"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "CO"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "CT"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "DE"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "DC"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "FM"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "FL"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "GA"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "GU"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "HI"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "ID"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "IL"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "IN"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "IA"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "KS"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "KY"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "LA"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "ME"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "MH"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "MD"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "MA"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "MI"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "MN"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "MS"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "MO"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "MT"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "NE"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "NV"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "NH"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "NJ"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "NM"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "NY"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "NC"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "ND"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "MP"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "OH"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "OK"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "OR"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "PW"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "PA"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "PR"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "RI"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "SC"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "SD"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "TN"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "TX"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "UT"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "VT"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "VI"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "VA"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "WA"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "WV"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "WI"));
            output.Add(new Models.EnumeratedType("USStateTerritoryCode", "WY"));

            // ResponsibilitySequenceType
            output.Add(new Models.EnumeratedType("ResponsibilitySequenceType", "Primary", "Prime", "Main", "First"));
            output.Add(new Models.EnumeratedType("ResponsibilitySequenceType", "Secondary", "Second"));
            output.Add(new Models.EnumeratedType("ResponsibilitySequenceType", "Tertiary", "Third"));
            output.Add(new Models.EnumeratedType("ResponsibilitySequenceType", "Equal Split"));
            output.Add(new Models.EnumeratedType("ResponsibilitySequenceType", "Unknown"));

            return output;
        }

        /// <summary>
        /// Try to match an enumerated attribute
        /// </summary>
        /// <param name="attributeType"></param>
        /// <param name="value"></param>
        /// <returns>If the value is blank, a blank value will me returned,
        /// if no matches can be found a null will be returned/returns>
        public static string MatchEnumeratedType(string attributeType, string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return "";

            // Load all attribute types
            List<Models.EnumeratedType> selectedValues = GetEnumeratedTypes().FindAll(
                x => x.AttributeType.ToUpper() == attributeType.ToUpper().Trim());

            // No validation object, throw error
            if (selectedValues.Count == 0)
                throw new Exception($"Invalid Attribute Type '{attributeType}' - DataLayer.HealthRulesConnectors.Validation.MatchEnumeratedType()");

            // Match on values
            List<Models.EnumeratedType> myMatches = selectedValues.FindAll(x => x.MatchValue(value));

            if (myMatches.Count > 0)
                return myMatches[0].Value;

            // Match on alternate values
            myMatches = selectedValues.FindAll(x => x.MatchAltValue(value));

            if (myMatches.Count > 0)
                return myMatches[0].Value;

            // Nothing found, return null
            return null;
        }

        /// <summary>
        /// Get a string of all possible values for an attributeType
        /// </summary>
        /// <param name="attributeType">Attribute type to search</param>
        /// <returns>Comma sepoerated list of possible values</returns>
        public static string GetEnumeratedTypes(string attributeType)
        {
            List<string> output = new List<string>();

            // Get values and put into a list;
            List<Models.EnumeratedType> myValues = GetEnumeratedTypes().FindAll(
                x => x.AttributeType.ToUpper() == attributeType.ToUpper().Trim());

            myValues.ForEach(
                x => {
                    output.Add(x.Value);
                    x.AltValues.ForEach(y => output.Add(y));
                }
            );

            // Join and return values
            return string.Join(", ", myValues);
        }

        /// <summary>
        /// Attempt to parse a string as a DateTime object
        /// </summary>
        /// <param name="source">DateTime object, null if it could not be parsed</param>
        /// <returns></returns>
        public static DateTime? ParseDateTime(string source)
        {
            if (string.IsNullOrWhiteSpace(source))
                return null;

            if (!DateTime.TryParse(source, out DateTime output))
                return null;

            return output;
        }

        /// <summary>
        /// Validate if Yes / No / True / False and be found
        /// </summary>
        /// <param name="source"></param>
        /// <returns>Yes if True of Yes is found, No if False or No is found, Unknown for all other values</returns>
        public static YesNo YesNoValidation(string source)
        {
            if (string.IsNullOrWhiteSpace(source))
                return YesNo.Unknown;

            return source.Trim().ToUpper() switch
            {
                "TRUE" => YesNo.Yes,
                "YES" => YesNo.Yes,
                "FALSE" => YesNo.No,
                "NO" => YesNo.No,
                _ => YesNo.Unknown
            };
        }

        /// <summary>
        /// Validate Action Mode, maily used by member enrollemnt related Connectors
        /// </summary>
        /// <param name="source">Source</param>
        /// <returns>Yes if True of Yes is found, No if False or No is found, Unknown for all other values</returns>
        public static ActionMode ActionModeValidation(string source)
        {
            if (string.IsNullOrWhiteSpace(source))
                return ActionMode.Unknown;

            return source.Trim().ToUpper() switch {
                "FULL" => ActionMode.Full,
                "SPARSE" => ActionMode.Sparse,
                "AUDIT" => ActionMode.Audit,
                "REPLACE" => ActionMode.Replace,
                _ => ActionMode.Unknown
            };
        }
    }
}

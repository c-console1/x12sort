using System.Collections.Generic;
using System.Xml;
using DataLayer.Base;
using DataLayer.HealthRulesConnectors.Enumerations;
using DataLayer.HealthRulesConnectors.Models;
using DataLayer.HealthRulesConnectors.Models.Requests;
using DataLayer.HealthRulesConnectors.Models.Responses;

namespace DataLayer.HealthRulesConnectors
{
    public class CorrespondenceRepository : BaseRepository<HealthRulesConnectorsDatabase>
    {
        /// <summary>
        /// Initialize the Correspondence call object
        /// </summary>
        /// <param name="database"></param>
        internal CorrespondenceRepository(HealthRulesConnectorsDatabase database) : base(database)
        {

        }

        #region CorrespondenceServiceStronglyTyped - GetCorrespondence

        /// <summary>
        /// Get the content of a single correspondence
        /// </summary>
        /// <param name="source">.NET Request object</param>
        /// <returns>XML format string</returns>
        public string GetCorrespondenceText(string correspondenceId)
        {
            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.Classic);

            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "CorrespondenceServiceStronglyTyped"
            };

            string xml = objSoap.callObject.Get(conn, string.Format(resRequests.GetCorrespondence, correspondenceId));
            objSoap = null;

            return xml;
        }

        /// <summary>
        /// Get the content of a single correspondence
        /// </summary>
        /// <param name="source">.NET Request object</param>
        /// <returns>XML Document</returns>
        public XmlDocument GetCorrespondenceXML(string correspondenceId)
        {
            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.Classic);

            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "CorrespondenceServiceStronglyTyped"
            };

            string xml = objSoap.callObject.Get(conn, string.Format(resRequests.GetCorrespondence, correspondenceId));
            objSoap = null;

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            return doc;
        }

        /// <summary>
        /// Get the content of a single correspondence
        /// </summary>
        /// <param name="source">.NET Request object</param>
        /// <returns>.NET Response with corispondence information</returns>
        public GetCorrespondenceResponse.Response GetCorrespondence(string correspondenceId)
        {

            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.Classic);

            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "CorrespondenceServiceStronglyTyped"
            };

            // If a single XML object comes in and gets desialized to an object that is a list
            // it will create an error unless our deserilizing knows which field to treat as
            // lists.
            List<string> forceJSONList = new List<string>() { "correspondenceId" };

            GetCorrespondenceResponse.Response result = objSoap.callObject.GetXML
                <GetCorrespondenceResponse.Response>(conn, string.Format(resRequests.GetCorrespondence, correspondenceId), forceJSONList);
            objSoap = null;

            return result;

        }

        #endregion CorrespondenceServiceStronglyTyped - GetCorrespondence

        #region CorrespondenceServiceStronglyTyped - FindCorrespondence

        /// <summary>
        /// Find a List of Correspondence ID's for a member
        /// </summary>
        /// <param name="source">.NET Request object</param>
        /// <returns>XML format string</returns>
        public string FindCorrespondenceText(FindCorrespondenceRequest source)
        {
            string xml = "";

            if (source != null)
            {
                // Base SOAP call object
                GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.Classic);

                ExternalCallsProperties conn = new ExternalCallsProperties()
                {
                    username = objSoap.username,
                    password = objSoap.password,
                    url = "CorrespondenceServiceStronglyTyped"
                };

                xml = objSoap.callObject.Get(conn, source.Get());
            }

            return xml;
        }

        /// <summary>
        /// Find a List of Correspondence ID's for a member
        /// </summary>
        /// <param name="source">.NET Request object</param>
        /// <returns>XML Document</returns>
        public XmlDocument FindCorrespondenceXML(FindCorrespondenceRequest source)
        {
            XmlDocument doc = new XmlDocument();

            if (source != null)
            {
                // Base SOAP call object
                GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.Classic);

                ExternalCallsProperties conn = new ExternalCallsProperties()
                {
                    username = objSoap.username,
                    password = objSoap.password,
                    url = "CorrespondenceServiceStronglyTyped"
                };

                string xml = objSoap.callObject.Get(conn, source.Get());
                doc.LoadXml(xml);
            }

            return doc;
        }

        /// <summary>
        /// Find a List of Correspondence ID's for a member
        /// </summary>
        /// <param name="source">.NET Request object</param>
        /// <returns>.NET Response with corispondence information</returns>
        public FindCorrespondenceResponse.Response FindCorrespondence(FindCorrespondenceRequest source)
        {
            FindCorrespondenceResponse.Response result = new FindCorrespondenceResponse.Response();

            if (source != null)
            {
                // Base SOAP call object
                GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.Classic);

                ExternalCallsProperties conn = new ExternalCallsProperties()
                {
                    username = objSoap.username,
                    password = objSoap.password,
                    url = "CorrespondenceServiceStronglyTyped"
                };

                // If a single XML object comes in and gets desialized to an object that is a list
                // it will create an error unless our deserilizing knows which field to treat as
                // lists.
                List<string> forceJSONList = new List<string>() { "statusTimeList", "manuallyEnteredFields" };

                result = objSoap.callObject.GetXML
                    <FindCorrespondenceResponse.Response>(conn, source.Get(), forceJSONList);
            }

            return result;
        }

        #endregion CorrespondenceServiceStronglyTyped - FindCorrespondence
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.HealthRulesConnectors
{
    /// <summary>
    /// Backing fields for any kind of object
    /// </summary>
    public class BackingFieldsObject
    {
        /// <summary>
        /// Inner values to hold everything
        /// </summary>
        private Dictionary<string, object> keyValuePairs =
            new Dictionary<string, object>(StringComparer.CurrentCultureIgnoreCase);

        /// <summary>
        /// Process values
        /// </summary>
        /// <param name="index">Index key</param>
        /// <returns></returns>
        public object this[string index]
        {
            get
            {
                if (keyValuePairs.ContainsKey(index))
                    return keyValuePairs[index];
                return null;
            }

            set
            {
                keyValuePairs[index] = value;
            }
        }

        /// <summary>
        /// Does this contain a value by key?
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool ContainsKey(string key)
        {
            return keyValuePairs.ContainsKey(key);
        }

        /// <summary>
        /// Remove item by key
        /// </summary>
        /// <param name="key">Key name</param>
        /// <returns>True if remvoed, false if key does not exists</returns>
        public bool Remove(string key)
        {
            if (keyValuePairs.ContainsKey(key))
            {
                keyValuePairs.Remove(key);
                return true;
            }
            return false;
        }

    }
}

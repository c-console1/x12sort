using DataLayer.HealthRulesConnectors.Models;
using System.Net.Http;
using System.Xml;

namespace DataLayer.HealthRulesConnectors.Requests
{
    public class SubmitAuthRequest<TReq, TResp> : ISoapRequest<serviceAuthorization, HttpResponseMessage>
    {
        public string EndPoint { get; set; }

        public serviceAuthorization Body { get; set; }

        public IXmlNamespaceResolver NamespaceManager { get; set; }
    }
}

using System;
using System.Collections.Generic;
using System.Xml;
using DataLayer.Base;
using DataLayer.HealthRulesConnectors.Enumerations;
using DataLayer.HealthRulesConnectors.Models;
using DataLayer.HealthRulesConnectors.Models.Requests;
using DataLayer.HealthRulesConnectors.Models.Responses;

namespace DataLayer.HealthRulesConnectors
{
    public class PractitionerRepository : BaseRepository<HealthRulesConnectorsDatabase>
    {
        internal PractitionerRepository(HealthRulesConnectorsDatabase database) : base(database)
        {

        }

        #region PractitionerLookup

        /// <summary>
        /// Practitioner Lookup
        /// </summary>
        /// <param name="source">.NET Request object</param>
        /// <returns>XML Formatted string</returns>
        public string PractitionerLookupText(PractitionerLookupRequest source)
        {
            string result = "";

            if (source != null)
            {
                // Base SOAP call object
                GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

                ExternalCallsProperties conn = new ExternalCallsProperties()
                {
                    username = objSoap.username,
                    password = objSoap.password,
                    url = "PractitionerLookup"
                };

                result = objSoap.callObject.Get(conn, source.Get());
            }

            return result;
        }

        /// <summary>
        /// Practitioner Lookup
        /// </summary>
        /// <param name="source">.NET Request object</param>
        /// <returns>XML Document Response Object</returns>
        public XmlDocument PractitionerLookupXML(PractitionerLookupRequest source)
        {
            string xml = PractitionerLookupText(source);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            return doc;
        }

        /// <summary>
        /// Practitioner Lookup
        /// </summary>
        /// <param name="source">.NET Request object</param>
        /// <returns>.NET Response object</returns>
        public PractitionerLookup.Response PractitionerLookup(PractitionerLookupRequest source)
        {
            PractitionerLookup.Response result = new PractitionerLookup.Response();

            if (source != null)
            {
                // Base SOAP call object
                GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

                ExternalCallsProperties conn = new ExternalCallsProperties()
                {
                    username = objSoap.username,
                    password = objSoap.password,
                    url = "PractitionerLookup"
                };

                List<string> forceJSONList = new List<string>() {
                "practitionerDetail", "practitionerCredential",
                "practitionerLanguage", "practitionerIdDetail", "practitionerLicense",
                "suffix", "CredentialSuffix", "phoneNumber", "specialty", "medicalSchoolInfo",
                "residencyInfo", "udtList", "udtListValueSet", "error"
            };

                // Raw XML so it can be preserved into the Response object
                string xmlResponse = "";
                result = objSoap.callObject.GetXML
                    <PractitionerLookup.Response>(conn, source.Get(), forceJSONList, out xmlResponse);

                // Peserve Request and Response object for debugging
                result.XMLRequest = source.Get();
                result.XMLResponse = xmlResponse;
            }

            return result;
        }

        /// <summary>
        /// Practitioner Lookup
        /// </summary>
        /// <param name="AsOfDate">As Of this Last Update - Format 'YYYY-MM-DD'</param>
        /// <param name="PractitionerId">Practitioner ID assigned ID, not the HCC identity ID</param>
        /// <param name="FirstName">First Name</param>
        /// <param name="LastName">Last Name</param>
        /// <returns>.NET Lookup Response</returns>
        public PractitionerLookup.Response PractitionerLookup(DateTime AsOfDate, string PractitionerId = "", string FirstName = "", string LastName = "")
        {
            PractitionerLookupRequest source = new PractitionerLookupRequest();
            if (!string.IsNullOrWhiteSpace(PractitionerId))
                source.PractitionerId = PractitionerId;
            if (!string.IsNullOrWhiteSpace(FirstName))
                source.PractitionerFirstName = FirstName;
            if (!string.IsNullOrWhiteSpace(LastName))
                source.PractitionerLastName = LastName;
            if (AsOfDate == null)
                source.AsOfDate = DateTime.Now.ToString("yyyy-MM-dd");
            else
                source.AsOfDate = AsOfDate.ToString("yyyy-MM-dd");
            return PractitionerLookup(source);
        }

        /// <summary>
        /// Practitioner Lookup 
        /// </summary>
        /// <param name="PractitionerId">Practitioner ID assigned ID, not the HCC identity ID</param>
        /// <param name="FirstName">First Name</param>
        /// <param name="LastName">Last Name</param>
        /// <returns>.NET Lookup Response</returns>
        public PractitionerLookup.Response PractitionerLookup(string PractitionerId = "", string FirstName = "", string LastName = "")
        {
            PractitionerLookupRequest source = new PractitionerLookupRequest();
            if (!string.IsNullOrWhiteSpace(PractitionerId))
                source.PractitionerId = PractitionerId;
            if (!string.IsNullOrWhiteSpace(FirstName))
                source.PractitionerFirstName = FirstName;
            if (!string.IsNullOrWhiteSpace(LastName))
                source.PractitionerLastName = LastName;
            return PractitionerLookup(source);
        }

        #endregion PractitionerLookup
    }
}

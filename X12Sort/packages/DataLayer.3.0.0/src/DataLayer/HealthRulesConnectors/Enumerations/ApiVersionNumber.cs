namespace DataLayer.HealthRulesConnectors.Enumerations
{
    /// <summary>
    /// API version #
    /// </summary>
    public enum ApiVersionNumber : int
    {
        V1 = 1,
        V2 = 2,
        V3 = 3,
        V4 = 4,
        V5 = 5,
        V6 = 6,
        V7 = 7,
        V8 = 8,
        V9 = 9,
        Classic = 0
    }
}

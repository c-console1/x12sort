using System.Collections.Generic;
using System.Xml;
using DataLayer.Base;
using DataLayer.HealthRulesConnectors.Enumerations;
using DataLayer.HealthRulesConnectors.Models;
using DataLayer.HealthRulesConnectors.Models.Requests;
using DataLayer.HealthRulesConnectors.Models.Responses;

namespace DataLayer.HealthRulesConnectors
{
    public class AuthorizationRepository : BaseRepository<HealthRulesConnectorsDatabase>
    {
        /// <summary>
        /// Initialize the Authorization Connector object
        /// </summary>
        /// <param name="database"></param>
        internal AuthorizationRepository(HealthRulesConnectorsDatabase database) : base(database)
        {

        }

        #region Authorization Call

        /// <summary>
        /// Authorization Connector Call that returns an XML string
        /// </summary>
        /// <param name="source">.NET Authorization Request object</param>
        /// <returns>.NET Authorization Response object</returns>
        public string AuthorizationText(AuthorizationRequest source)
        {
            string xml = "";

            if (source != null)
            {
                // Base SOAP call object
                GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V5);

                ExternalCallsProperties conn = new ExternalCallsProperties()
                {
                    username = objSoap.username,
                    password = objSoap.password,
                    url = "Authorization"
                };

                xml = objSoap.callObject.Get(conn, source.Get());
            }

            return xml;
        }

        /// <summary>
        /// Authorization Connector Call that returns an XMLDocument Response object
        /// </summary>
        /// <param name="source">.NET Authorization Request object</param>
        /// <returns>.NET Authorization Response object</returns>
        public XmlDocument AuthorizationXML(AuthorizationRequest source)
        {
            string xml = AuthorizationText(source);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            return doc;

        }

        /// <summary>
        /// Authorization Connector Call that returns an .NET Response object
        /// </summary>
        /// <param name="source">.NET Authorization Request object</param>
        /// <returns>.NET Authorization Response object</returns>
        public AuthorizationResponse.Response Authorization(AuthorizationRequest source)
        {
            AuthorizationResponse.Response result = null;

            if (source != null)
            {
                // Base SOAP call object
                GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V5);

                ExternalCallsProperties conn = new ExternalCallsProperties()
                {
                    username = objSoap.username,
                    password = objSoap.password,
                    url = "Authorization"
                };

                // If a single XML object comes in and gets desialized to an object that is a list
                // it will create an error unless our deserilizing knows which field to treat as
                // lists.
                List<string> forceJSONArray = new List<string>() { "services", "exceptions", "error" };

                result = objSoap.callObject.GetXML
                    <AuthorizationResponse.Response>(conn, source.Get(), forceJSONArray);
            }

            return result;
        }

        #endregion Authorization Call

        #region AuthorizationLookup Call

        /// <summary>
        /// AuthorizationLookup Connector Call that returns an XML string 
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public string AuthorizationLookupText(AuthorizationLookupRequest source)
        {
            string xml = "";

            if (source != null)
            {
                // Base SOAP call object
                GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V5);

                ExternalCallsProperties conn = new ExternalCallsProperties()
                {
                    username = objSoap.username,
                    password = objSoap.password,
                    url = "AuthorizationLookup"
                };

                xml = objSoap.callObject.Get(conn, source.Get());
            }

            return xml;
        }

        /// <summary>
        /// AuthorizationLookup Connector Call that returns an XMLDocument object
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public XmlDocument AuthorizationLookupXML(AuthorizationLookupRequest source)
        {
            string xml = AuthorizationLookupText(source);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            return doc;
        }

        /// <summary>
        /// AuthorizationLookup Connector Call that returns an .NET object
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public AuthorizationLookupResponse.Response AuthorizationLookup(AuthorizationLookupRequest source)
        {
            AuthorizationLookupResponse.Response result = new AuthorizationLookupResponse.Response();

            if (source != null)
            {
                // Base SOAP call object
                GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V5);

                ExternalCallsProperties conn = new ExternalCallsProperties()
                {
                    username = objSoap.username,
                    password = objSoap.password,
                    url = "AuthorizationLookup"
                };

                // If a single XML object comes in and gets desialized to an object that is a list
                // it will create an error unless our deserilizing knows which field to treat as
                // lists.
                List<string> forceJSONArray = new List<string>() { "services", "exceptions", "error" };

                result = objSoap.callObject.GetXML
                    <AuthorizationLookupResponse.Response>(conn, source.Get(), forceJSONArray);
            }
            return result;
        }


        #endregion AuthorizationLookup Call

    }
}

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Newtonsoft.Json;
using DataLayer.StaticUtilities;

namespace DataLayer.HealthRulesConnectors
{
    /// <summary>
    /// Generic object to call to external web services
    /// </summary>
   public class ExternalCalls
    {

        /// <summary>
        /// base URL for all calls
        /// </summary>
        public string BaseURL = "";

        /// <summary>
        /// Call to an external web server and convert the JSON text content to a .NET object
        /// </summary>
        /// <typeparam name="T">Target object type</typeparam>
        /// <param name="properties">Call properties</param>
        /// <param name="envelope">Text envolope</param>
        /// <returns></returns>
        public T GetJSON<T>(ExternalCallsProperties properties, string envelope)
        {
            string source = Get(properties, envelope);
            return JsonConvert.DeserializeObject<T>(source);
        }

        /// <summary>
        /// Get XML from and convert it to a C# object
        /// </summary>
        /// <typeparam name="T">Target object type</typeparam>
        /// <param name="properties">Call properties</param>
        /// <param name="envelope">Text envolope</param>
        /// <param name="xmlTagsAsArray">Force Newton JSON to see XML as Arrays when there is only 1 instance of an object</param>
        /// <param name="xml">output the raw SOAP XML</param>
        /// <returns>Serialized object</returns>
        public T GetXML<T>(ExternalCallsProperties properties, string envelope, List<string> xmlTagsAsArray, out string xml)
        {
            string source = Get(properties, envelope);
            source = XMLSerialization.RemoveEmptyTags(source);
            xml = source;
            string json = XMLSerialization.XML2JSON(source, xmlTagsAsArray);

            return JsonConvert.DeserializeObject<T>(json);
        }

        /// <summary>
        /// Get XML from and convert it to a C# object
        /// </summary>
        /// <typeparam name="T">Target object type</typeparam>
        /// <param name="properties">Call properties</param>
        /// <param name="envelope">Text envolope</param>
        /// <param name="xmlTagsAsArray">Force Newton JSON to see XML as Arrays when there is only 1 instance of an object</param>
        /// <returns></returns>
        public T GetXML<T>(ExternalCallsProperties properties, string envelope, List<string> xmlTagsAsArray)
        {
            string source = Get(properties, envelope);
            source = XMLSerialization.RemoveEmptyTags(source);
            string json = XMLSerialization.XML2JSON(source, xmlTagsAsArray);
            return JsonConvert.DeserializeObject<T>(json);
        }

        /// <summary>
        /// Get XML from and convert it to a C# object
        /// </summary>
        /// <typeparam name="T">Target object type</typeparam>
        /// <param name="properties">Call properties</param>
        /// <param name="envelope">Text envolope</param>    
        /// <returns></returns>
        public T GetXML<T>(ExternalCallsProperties properties, string envelope)
        {
            return GetXML<T>(properties, envelope, new List<string> { });
        }

        /// <summary>
        /// Get the raw XML from a call and return it as an XmlDocument
        /// </summary>
        /// <param name="properties">Call properties</param>
        /// <param name="envelope">Text envolope</param>  
        /// <returns></returns>
        public XmlDocument GetXML(ExternalCallsProperties properties, string envelope)
        {
            string source = "";

            try
            {
                source = Get(properties, envelope);
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(source);
                return xml;
            }
            catch (Exception ex)
            {
                string error = string.Format("Exception: {0}\r\nValue returned: {1}",
                    ex.Message, source);
                throw new Exception(error, ex.InnerException);
            }
        }

        /// <summary>
        /// Get raw text from a call
        /// </summary>
        /// <param name="properties">Set request properties like POST verb and headers</param>
        /// <param name="envelope">Text envolope you want to send</param>
        /// <returns></returns>
        public string Get(ExternalCallsProperties properties, string envelope)
        {
            if (properties == null)
                return "";

            HttpWebRequest request = CreateWebRequest(properties);

            // Write text array
            byte[] byteArray = Encoding.UTF8.GetBytes(envelope);

            if (properties.verb.ToUpper() != "GET")
            {
                Stream stream = request.GetRequestStream();
                stream.Write(byteArray, 0, byteArray.Length);
            }

            StringBuilder output = new StringBuilder();

            try
            {

                using (WebResponse response = request.GetResponse())
                {
                    using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                    {
                        string soapResult = rd.ReadToEnd();
                        output.Append(soapResult);
                    }
                }

            }
            catch (WebException e)
            {
                // Capture base exception
                string webExp = e.Message;

                // To grab any output from the response
                try
                {
                    using (WebResponse response = e.Response)
                    {
                        HttpWebResponse httpResponse = (HttpWebResponse)response;
                        using (Stream data = response.GetResponseStream())
                        using (var reader = new StreamReader(data))
                        {
                            output.Append(reader.ReadToEnd());
                        }
                    }

                    if(output == null)
                    {
                        request = null;
                        webExp += "\r\nStatus Code: " + GetWebExceptionStatus(e.Status);
                        if (e.Status == WebExceptionStatus.ProtocolError)
                        {
                            webExp += "\r\nStatus Description: " + ((HttpWebResponse)e.Response).StatusDescription;
                        }
                        throw new Exception(webExp);
                    }

                    request = null;
                    return output.ToString();
                }
                catch (Exception exLastChance)
                {
                    request = null;
                    webExp += "\r\nStatus Code: " + GetWebExceptionStatus(e.Status);
                    if (e.Status == WebExceptionStatus.ProtocolError)
                        webExp += "\r\nStatus Description: " + ((HttpWebResponse)e.Response).StatusDescription;
                    webExp += "\r\n" + exLastChance.Message;
                    throw new Exception(webExp);
                }
            }

            if (output == null)
            {
                request = null;
                string error = string.Format(
                    "ExternalCalls.Get() - No results returned - {0} - {1}", properties.username, properties.url
                    );
                throw new Exception(error);
            }

            return output.ToString();

        }

        /// <summary>
        /// Get the raw text from a SOAP call
        /// </summary>
        /// <param name="url">URL appended to base URL to pull from</param>
        /// <param name="requestEnv">Request envolope</param>
        /// <param name="verb">Verb (ex. POST, GET, PUT)</param>
        /// <returns>Result of the response</returns>
        public string GetSOAP(ExternalCallsProperties properties, string envelope)
        {
            return Get(properties, envelope);
        }


        /// <summary>
        /// Create a webrequest to URL, this will ONLY do SOAP request
        /// </summary>
        /// <param name="url">URL to call</param>
        /// <param name="username">Basic authtication username, if left blank authication will happen</param>
        /// <param name="password">Basic authtication password</param>
        /// <param name="verb">Method to access the URL, default verb is POST</param>
        /// <returns>.NET Web Request</returns>
        private HttpWebRequest CreateWebRequest(ExternalCallsProperties properties)
        {
            // Was a null value passed?
            if (properties is null)
            {
                throw new ArgumentNullException(nameof(properties));
            }

            // Create web request
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(BaseURL + properties.url);

            // Content Type
            if(!string.IsNullOrWhiteSpace(properties.contentType))
                webRequest.ContentType = properties.contentType;

            // PreAuthenticate - default = false / no
            properties.preAuthenticate = webRequest.PreAuthenticate;


            // Use Basic Authentication with a username and password
            if (!string.IsNullOrWhiteSpace(properties.username))
            {
                string encoded = System.Convert.ToBase64String(
                    System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(properties.username + ":" + properties.password));
                webRequest.Headers.Add("Authorization", "Basic " + encoded);
            }

            // Use Token Authentication
            if (!string.IsNullOrWhiteSpace(properties.accessToken))
            {
                webRequest.PreAuthenticate = true;
                webRequest.Headers.Add("Authorization", "Bearer " + properties.accessToken);
            }

            // Add Individual Headers
            foreach (KeyValuePair<string, string> headerItem in properties.headers)
            {
                if (string.IsNullOrEmpty(headerItem.Value))
                    webRequest.Headers.Add(headerItem.Key);
                else
                    webRequest.Headers.Add(headerItem.Key, headerItem.Value);
            }

            // Accept response type, default is "text/xml" for SOAP requests
            if(!string.IsNullOrWhiteSpace(properties.acceptType))
                webRequest.Accept = properties.acceptType;

            // Method to send data as, default is 'POST' for SOAP requests
            if (!string.IsNullOrWhiteSpace(properties.verb))
                webRequest.Method = properties.verb.Trim().ToUpper();
            else
                webRequest.Method = "POST";

            return webRequest;
        }

        /// <summary>
        /// Get a easy to read error
        /// </summary>
        /// <param name="source">WebExceptionStatus object</param>
        /// <returns>Easy to read exception value</returns>
        private static string GetWebExceptionStatus(WebExceptionStatus source = WebExceptionStatus.UnknownError)
        {
            string output = "Unknown Error - No Key Found";
            switch (source)
            {
                case WebExceptionStatus.CacheEntryNotFound:
                    return "Cache Entry Not Found";
                case WebExceptionStatus.ConnectFailure:
                    return "Connect Failure";
                case WebExceptionStatus.ConnectionClosed:
                    return "Connection Closed";
                case WebExceptionStatus.KeepAliveFailure:
                    return "KeepAliveFailure";
                case WebExceptionStatus.MessageLengthLimitExceeded:
                    return "Message Length Limit Exceeded";
                case WebExceptionStatus.NameResolutionFailure:
                    return "Name Resolution Failure";
                case WebExceptionStatus.Pending:
                    return "Pending";
                case WebExceptionStatus.PipelineFailure:
                    return "Pipeline Failure";
                case WebExceptionStatus.ProtocolError:
                    return "Protocol Error";
                case WebExceptionStatus.ProxyNameResolutionFailure:
                    return "Proxy Name Resolution Failure";
                case WebExceptionStatus.ReceiveFailure:
                    return "ReceiveFailure";
                case WebExceptionStatus.RequestCanceled:
                    return "Request Canceled";
                case WebExceptionStatus.RequestProhibitedByCachePolicy:
                    return "Request Prohibited By Cache Policy";
                case WebExceptionStatus.RequestProhibitedByProxy:
                    return "Request Prohibited By Proxy";
                case WebExceptionStatus.SecureChannelFailure:
                    return "Secure Channel Failure";
                case WebExceptionStatus.SendFailure:
                    return "Send Failure";
                case WebExceptionStatus.ServerProtocolViolation:
                    return "Server Protocol Violation";
                case WebExceptionStatus.Success:
                    return "Success";
                case WebExceptionStatus.Timeout:
                    return "Timeout";
                case WebExceptionStatus.TrustFailure:
                    return "Trust Failure";
                case WebExceptionStatus.UnknownError:
                    return "Unknown Error";
                default:
                    return output;
            }

        }

    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.HealthRulesConnectors
{
    public class BackfillValues
    {
        /// <summary>
        /// Inner dictionary
        /// </summary>
        private Dictionary<string, string> dictionary =
            new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

        /// <summary>
        /// Initialize object
        /// </summary>
        public BackfillValues() { }

        /// <summary>
        /// Initialize object with dictionary
        /// </summary>
        /// <param name="values">Dictionary object</param>
        public BackfillValues(Dictionary<string, string> values)
        {
            foreach (KeyValuePair<string, string> set in values)
                this[set.Key] = set.Value;
        }

        /// <summary>
        /// Process values
        /// </summary>
        /// <param name="index">Index key</param>
        /// <returns></returns>
        public string this[string index]
        {
            get
            {
                if(dictionary.ContainsKey(index))
                    return dictionary[index];
                return null;
            }

            set
            {
                dictionary[index] = value;
            }
        }

        /// <summary>
        /// Does this contain a value by key?
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool ContainsKey(string key)
        {
            return dictionary.ContainsKey(key);
        }

        /// <summary>
        /// Remove item by key
        /// </summary>
        /// <param name="key">Key name</param>
        /// <returns>True if remvoed, false if key does not exists</returns>
        public bool Remove(string key)
        {
            if (dictionary.ContainsKey(key))
            {
                dictionary.Remove(key);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Add value with string validation
        /// </summary>
        /// <param name="key">Key</param>
        /// <param name="value">Value</param>
        public void AddString(string key, string value)
        {
            dictionary[key] = Validation.TrimNull(value);
        }

        /// <summary>
        /// Add value with string validation
        /// </summary>
        /// <param name="key">Key</param>
        /// <param name="value">Value</param>
        /// <param name="defaultValue">Default value is null or whitespace</param>
        public void AddString(string key, string value, string defaultValue)
        {
            dictionary[key] = Validation.TrimNull(value);
            if (string.IsNullOrWhiteSpace(dictionary[key]))
                dictionary[key] = defaultValue;
        }

        /// <summary>
        /// Add value with integer validation
        /// </summary>
        /// <param name="key">Key</param>
        /// <param name="value">Value</param>
        public void AddInt(string key, string value)
        {
            AddInt(key, value, key);
        }

        /// <summary>
        /// Add value with integer validation
        /// </summary>
        /// <param name="key">Key</param>
        /// <param name="value">Value</param>
        /// <param name="fieldName">Field name of the error</param>
        public void AddInt(string key, string value, string fieldName)
        {
            dictionary[key] = Validation.IsInt(value, fieldName);
        }

        /// <summary>
        /// Add value with decimal validation
        /// </summary>
        /// <param name="key">Key</param>
        /// <param name="value">Value</param>
        public void AddDecimal(string key, string value)
        {
            AddDecimal(key, value, key);
        }

        /// <summary>
        /// Add value with decimal validation
        /// </summary>
        /// <param name="key">Key</param>
        /// <param name="value">Value</param>
        /// <param name="fieldName">Field name of the error</param>
        public void AddDecimal(string key, string value, string fieldName)
        {
            dictionary[key] = Validation.IsDecimal(value, fieldName);
        }

        /// <summary>
        /// Add gender value with validation
        /// </summary>
        /// <param name="key">Key</param>
        /// <param name="value">Value</param>
        public void AddGender(string key, string value)
        {
            AddGender(key, value, key);
        }


        /// <summary>
        /// Add gender value with validation
        /// </summary>
        /// <param name="key">Key</param>
        /// <param name="value">Value</param>
        /// <param name="fieldName">Field name of the error</param>
        public void AddGender(string key, string value, string fieldName)
        {
            dictionary[key] = Validation.Gender(value, "key");
        }

        /// <summary>
        /// Add value with date validation
        /// </summary>
        /// <param name="key">Key</param>
        /// <param name="value">Value</param>
        public void AddDate(string key, string value)
        {
            AddDate(key, value, key);
        }

        /// <summary>
        /// Add value with date validation
        /// </summary>
        /// <param name="key">Key</param>
        /// <param name="value">Value</param>
        /// <param name="fieldName">Field name of the error</param>
        public void AddDate(string key, string value, string fieldName)
        {
            dictionary[key] = Validation.IsDate(value, fieldName);
        }

    }
}

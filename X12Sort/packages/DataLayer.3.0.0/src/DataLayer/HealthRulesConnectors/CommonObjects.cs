using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace DataLayer.HealthRulesConnectors
{
    /// <summary>
    /// Action mode
    /// </summary>
    public enum ActionMode : int
    {
        /// <summary>
        /// Reserved for future use
        /// </summary>
        Full = 1,

        /// <summary>
        /// Perform a sparse enrollment or update
        /// </summary>
        Sparse = 2,

        /// <summary>
        /// Check the submitted enrollment file for processing errors
        /// </summary>
        Audit = 3,

        /// <summary>
        /// Used by the HMEM module
        /// </summary>
        Replace = 4,

        /// <summary>
        /// Unknown status, assume Sparse
        /// </summary>
        Unknown = -1
    }

    /// <summary>
    /// Yes / No enumeration
    /// </summary>
    public enum YesNo : int
    {
        Yes = 1,
        No = 0,
        Unknown = -1
    }

    /// <summary>
    /// Type Code - common generic key/value/value set that HealthRule API uses
    /// </summary>
    public class TypeCode
    {
        /// <summary>
        /// Code set name- primary key
        /// </summary>
        public string codeSetName { get; set; }

        /// <summary>
        /// Code set entry = code values for this set
        /// Either codeEntry or shortName must be set
        /// </summary>
        public string codeEntry { get; set; }

        /// <summary>
        /// Short name for this set
        /// Either codeEntry or shortName must be set
        /// </summary>
        public string shortName { get; set; }
    }

    /// <summary>
    /// Common list of name suffixes, ex. Jr.
    /// </summary>
    public class NameSuffixList
    {
        /// <summary>
        /// List mode for how HealthRules should treat this list 
        /// of values
        /// </summary>
        public string listMode { get; set; }

        /// <summary>
        /// Suffix text list
        /// </summary>
        public List<string> suffix = new List<string>();
    }

    /// <summary>
    /// Common list of credential suffixes, ex. RN, LPN
    /// </summary>
    public class CredentialSuffixList
    {
        /// <summary>
        /// List mode for how HealthRules should treat this list 
        /// of values
        /// </summary>
        public string listMode { get; set; }

        /// <summary>
        /// Suffix text list
        /// </summary>
        public List<TypeCode> CredentialSuffix = new List<TypeCode>();
    }

    /// <summary>
    /// Multiple other names
    /// </summary>
    public class OtherNames
    {
        // TBD
        // This may need to be converted to a <List> object
        public string listMode { get; set; }
        // The object below will most likely need to put into a <List>
        // TBD
        public List<OtherName> otherName = new List<OtherName>();
    }

    /// <summary>
    /// Other name information
    /// </summary>
    public class OtherName
    {
        /// <summary>
        /// Extended other name information
        /// </summary>
        public IndividualName otherNameInfo = new IndividualName();

        /// <summary>
        /// Other name types
        /// </summary>
        public TypeCode otherNameTypeCode = new TypeCode();
    }

    /// <summary>
    /// Individual name
    /// </summary>
    public class IndividualName
    {
        /// <summary>
        /// Last name
        /// </summary>
        public string lastName { get; set; }

        /// <summary>
        /// First name
        /// </summary>
        public string firstName { get; set; }

        /// <summary>
        /// Middle name
        /// </summary>
        public string middleName { get; set; }

        /// <summary>
        /// Name prefix - singlar instance
        /// </summary>
        public string namePrefix { get; set; }

        /// <summary>
        /// Name suffix - singlar instance
        /// </summary>
        public string nameSuffix { get; set; }

        /// <summary>
        /// Name suffix - List
        /// </summary>
        public List<NameSuffixList> nameSuffixList = new List<NameSuffixList>();

        // Credential Suffix - list        
        public CredentialSuffixList credentialSuffixList = new CredentialSuffixList();
    }

    /// <summary>
    /// Language information
    /// </summary>
    public class Language
    {
        /// <summary>
        /// Primary language
        /// </summary>
        public string primaryLanguage { get; set; }

        /// <summary>
        /// Native language
        /// </summary>
        public string nativeLanguage { get; set; }

        // Language Domain Code
        public TypeCode languageDomainCode = new TypeCode();
    }

    /// <summary>
    /// Address information
    /// </summary>
    public class AddressInfo
    {
        /// <summary>
        /// Postal address information
        /// </summary>
        public AddressLong postalAddress { get; set; }

        /// <summary>
        /// Address phone list
        /// </summary>
        public List<AddressPhoneList> addressPhoneList { get; set; }
    }

    /// <summary>
    /// Address values
    /// </summary>
    public class AddressLong
    {
        /// <summary>
        /// Address line 1
        /// </summary>
        public string address { get; set; }

        /// <summary>
        /// Address line 2
        /// </summary>
        public string address2 { get; set; }

        /// <summary>
        /// Address line 3
        /// </summary>
        public string address3 { get; set; }

        /// <summary>
        /// City name
        /// </summary>
        public string cityName { get; set; }

        /// <summary>
        /// State code
        /// </summary>
        public string stateCode { get; set; }

        /// <summary>
        /// 5-6 character zip code
        /// </summary>
        public string zipCode { get; set; }

        /// <summary>
        /// 4 character zip code extension
        /// </summary>
        public string zipExtensionCode { get; set; }

        /// <summary>
        /// Country code
        /// </summary>
        public CountryCode countryCode { get; set; }

        /// <summary>
        /// County code
        /// </summary>
        public string countyCode { get; set; }

        /// <summary>
        /// Longitude cordinate
        /// </summary>
        public string longitude { get; set; }

        /// <summary>
        /// Longitude cordinate
        /// </summary>
        public string latitude { get; set; }

        /// <summary>
        /// Ignore address check
        /// </summary>
        public string ignoreAddressCheck { get; set; }

        /// <summary>
        /// Is this an undeliverable address?
        /// </summary>
        public string undeliverableAddress { get; set; }

    }

    /// <summary>
    /// Fewer fields and slightly custom fields when compared
    /// to the HealthRulesConnectors.Address
    /// Full serialization control in both XML and Newton JSON
    /// </summary>
    public class AddressShortCapitals
    {
        /// <summary>
        /// address
        /// Optional:
        /// <summary>
        [JsonProperty("address")]
        [XmlElement(ElementName = "address")]
        public string Address { get; set; }

        /// <summary>
        /// cityName
        /// Optional:
        /// <summary>
        [JsonProperty("cityName")]
        [XmlElement(ElementName = "cityName")]
        public string CityName { get; set; }

        /// <summary>
        /// stateCode
        /// Optional:
        /// <summary>
        [JsonProperty("stateCode")]
        [XmlElement(ElementName = "stateCode")]
        public string StateCode { get; set; }

        /// <summary>
        /// zipCode
        /// Optional:
        /// <summary>
        [JsonProperty("zipCode")]
        [XmlElement(ElementName = "zipCode")]
        public string ZipCode { get; set; }

        /// <summary>
        /// zipExtensionCode
        /// Optional:
        /// <summary>
        [JsonProperty("zipExtensionCode")]
        [XmlElement(ElementName = "zipExtensionCode")]
        public string ZipExtensionCode { get; set; }

        /// <summary>
        /// countryCode
        /// Optional:
        /// <summary>
        [JsonProperty("countryCode")]
        [XmlElement(ElementName = "countryCode")]
        public string CountryCode { get; set; }

    }

    /// <summary>
    /// Contry code
    /// </summary>
    public class CountryCode
    {
        /// <summary>
        /// Initialize object - HRP assumes 'US' as teh default
        /// </summary>
        public CountryCode() { countryCode = "US"; }

        /// <summary>
        /// Initialize object with value
        /// </summary>
        public CountryCode(string value)
        {
            countryCode = value;
        }

        /// <summary>
        /// 2 letter country code
        /// </summary>
        public string countryCode { get; set; }
    }

    /// <summary>
    /// User Defined Value Set Item
    /// </summary>
    public class UdtListValueSet
    {
        /// <summary>
        /// Attribute Role Name
        /// </summary>
        public string attributeRoleName { get; set; }

        /// <summary>
        /// Attribute value as string
        /// </summary>
        public string attrValueAsString { get; set; }
    }


    /// <summary>
    /// Phone number list
    /// </summary>
    public class AddressPhoneList
    {
        /// <summary>
        /// List mode
        /// </summary>
        public string listMode { get; set; }

        /// <summary>
        /// List of telephone numbers and thier meta information
        /// </summary>
        public List<TelephoneNumber> telephoneNumber = new List<TelephoneNumber>();
    }

    /// <summary>
    /// ID Code
    /// </summary>
    public class IDCode
    {
        /// <summary>
        /// Initialize ID Code
        /// </summary>
        public IDCode() { }

        /// <summary>
        /// Initialize ID Code with value
        /// </summary>
        public IDCode(string value) { ID = value; }

        /// <summary>
        /// ID Value
        /// </summary>
        public string ID { get; set; }
    }

    /// <summary>
    /// Individual phone number
    /// </summary>
    public class TelephoneNumber
    {
        /// <summary>
        /// Phone country code - default is '01'
        /// </summary>
        public string phoneCountryCode { get; set; }

        /// <summary>
        /// 3 number area code
        /// </summary>
        public string phoneAreaCode { get; set; }

        /// <summary>
        /// Phone #
        /// </summary>
        public string phoneNumber { get; set; }

        /// <summary>
        /// Phone # extension
        /// </summary>
        public string phoneExtensionNumber { get; set; }

        /// <summary>
        /// Individual phone type code set
        /// </summary>
        public TypeCode individualPhoneTypeCode = new TypeCode();

        /// <summary>
        /// Individual organization type code set
        /// </summary>
        public TypeCode organizationPhoneTypeCode = new TypeCode();
    }

    #region Member / Subscriber Search

    public class MemberMatch
    {
        public string definitionName { get; set; }
        public string id { get; set; }
        public string topAccount { get; set; }
        public string customString1 { get; set; }
        public string customString2 { get; set; }
        public string customString3 { get; set; }
        public string customString4 { get; set; }
        public string customInteger1 { get; set; }
        public string customInteger2 { get; set; }
        public string customInteger3 { get; set; }
        public string customInteger4 { get; set; }
        public string customDate1 { get; set; }
        public string customDate2 { get; set; }
        public string customDate3 { get; set; }
        public string customDate4 { get; set; }
        public string customDecimal1 { get; set; }
        public string customDecimal2 { get; set; }
        public string customDecimal3 { get; set; }
        public string customDecimal4 { get; set; }
        public DataLayer.HealthRulesConnectors.Member member { get; set; }
        public DataLayer.HealthRulesConnectors.Subscriber subscriber { get; set; }
        public string asOfDate { get; set; }
    }

    public class Member
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string fullName { get; set; }
        public string gender { get; set; }
        public string dateOfBirth { get; set; }
        public string taxId { get; set; }
        public DataLayer.HealthRulesConnectors.AddressShortCapitals address { get; set; }
    }

    public class Subscriber
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string fullName { get; set; }
        public string gender { get; set; }
        public string dateOfBirth { get; set; }
        public string taxId { get; set; }
        public AddressLong address { get; set; }
    }
    #endregion

}

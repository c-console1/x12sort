using System.Collections.Generic;
using System.Xml;
using DataLayer.Base;
using DataLayer.HealthRulesConnectors.Enumerations;
using DataLayer.HealthRulesConnectors.Models;
using DataLayer.HealthRulesConnectors.Models.Requests;
using DataLayer.HealthRulesConnectors.Models.Responses;

namespace DataLayer.HealthRulesConnectors
{
    public class AccountRepository : BaseRepository<HealthRulesConnectorsDatabase>
    {
        /// <summary>
        /// Instaniate Account Connector Call object
        /// </summary>
        /// <param name="database"></param>
        internal AccountRepository(HealthRulesConnectorsDatabase database) : base(database)
        {

        }

        #region Account Call

        /// <summary>
        /// Add/Update a Member's Account
        /// </summary>
        /// <param name="source">.NET Request object that contains the add/update object</param>
        /// <returns>XML formated status string</returns>
        public string AccountText(AccountRequest source)
        {
            string xml = "";

            if (source != null)
            {
                // Base SOAP call object
                GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

                ExternalCallsProperties conn = new ExternalCallsProperties()
                {
                    username = objSoap.username,
                    password = objSoap.password,
                    url = "Account"
                };

                xml = objSoap.callObject.Get(conn, source.Get());
            }

            return xml;
        }

        /// <summary>
        /// Add/Update a Member's Account
        /// </summary>
        /// <param name="source">.NET Request object that contains the add/update object</param>
        /// <returns>XML Document status string</returns>
        public XmlDocument AccountXML(AccountRequest source)
        {
            string xml = AccountText(source);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            return doc;

        }

        /// <summary>
        /// Add/Update a Member's Account
        /// </summary>
        /// <param name="source">.NET Request object that contains the add/update object</param>
        /// <returns>.NET status object</returns>
        public AccountResponse.Response Account(AccountRequest source)
        {
            AccountResponse.Response result = null;
            if (source != null)
            {
                // Base SOAP call object
                GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

                ExternalCallsProperties conn = new ExternalCallsProperties()
                {
                    username = objSoap.username,
                    password = objSoap.password,
                    url = "Account"
                };

                // If a single XML object comes in and gets desialized to an object that is a list
                // it will create an error unless our deserilizing knows which field to treat as
                // lists.
                List<string> forceJSONArray = new List<string>() { "exceptions", "error" };

                result = objSoap.callObject.GetXML
                    <AccountResponse.Response>(conn, source.Get(), forceJSONArray);
            }

            return result;
        }

        #endregion Account Call

        #region AccountProperty Call

        /// <summary>
        /// Allows you to add or remove a premium billing hold from a specific account
        /// </summary>
        /// <param name="source">.NET object alter premium billing hold</param>
        /// <returns>XML formatted response string</returns>
        public string AccountPropertyText(AccountPropertyRequest source)
        {
            string xml = "";

            if (source != null)
            {
                // Base SOAP call object
                GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

                ExternalCallsProperties conn = new ExternalCallsProperties()
                {
                    username = objSoap.username,
                    password = objSoap.password,
                    url = "AccountProperty"
                };

                xml = objSoap.callObject.Get(conn, source.Get());
            }

            return xml;
        }

        /// <summary>
        /// Allows you to add or remove a premium billing hold from a specific account
        /// </summary>
        /// <param name="source">.NET object alter premium billing hold</param>
        /// <returns>XML Document response object</returns>
        public XmlDocument AccountPropertyXML(AccountPropertyRequest source)
        {
            string xml = AccountPropertyText(source);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            return doc;

        }

        /// <summary>
        /// Allows you to add or remove a premium billing hold from a specific account
        /// </summary>
        /// <param name="source">.NET object alter premium billing hold</param>
        /// <returns>.NET response object</returns>
        public AccountPropertyResponse.Response AccountProperty(AccountPropertyRequest source)
        {
            AccountPropertyResponse.Response result = new AccountPropertyResponse.Response();

            if (source != null)
            {
                // Base SOAP call object
                GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

                ExternalCallsProperties conn = new ExternalCallsProperties()
                {
                    username = objSoap.username,
                    password = objSoap.password,
                    url = "AccountProperty"
                };

                // If a single XML object comes in and gets desialized to an object that is a list
                // it will create an error unless our deserilizing knows which field to treat as
                // lists.
                List<string> forceJSONArray = new List<string>() { "exceptions", "error" };

                result = objSoap.callObject.GetXML
                    <AccountPropertyResponse.Response>(conn, source.Get(), forceJSONArray);
            }

            return result;

        }

        #endregion AccountProperty Call

        #region AccountLookup Call

        /// <summary>
        /// Account Lookup
        /// </summary>
        /// <param name="source">.NET Request Object</param>
        /// <returns>XML formatted Response string</returns>
        public string AccountLookupText(AccountLookupRequest source)
        {
            string xml = "";

            if (source != null)
            {
                // Base SOAP call object
                GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

                ExternalCallsProperties conn = new ExternalCallsProperties()
                {
                    username = objSoap.username,
                    password = objSoap.password,
                    url = "AccountLookupCriteria"
                };

                xml = objSoap.callObject.Get(conn, source.Get());
            }

            return xml;
        }

        /// <summary>
        /// Account Lookup
        /// </summary>
        /// <param name="source"></param>
        /// <returns>XML Document Response object</returns>
        public XmlDocument AccountLookupXML(AccountLookupRequest source)
        {
            string xml = AccountLookupText(source);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            return doc;

        }

        /// <summary>
        /// Account Lookup
        /// </summary>
        /// <param name="source">.NET Request Object</param>
        /// <returns>.NET Response object</returns>
        public AccountLookupResponse.Response AccountLookup(AccountLookupRequest source)
        {
            AccountLookupResponse.Response result = new AccountLookupResponse.Response();

            if (source != null)
            {
                // Base SOAP call object
                GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

                ExternalCallsProperties conn = new ExternalCallsProperties()
                {
                    username = objSoap.username,
                    password = objSoap.password,
                    url = "AccountLookupCriteria"
                };

                // If a single XML object comes in and gets desialized to an object that is a list
                // it will create an error unless our deserilizing knows which field to treat as
                // lists.
                List<string> forceJSONArray = new List<string>() { "account", "otherNames", "otherIDs", "error" };

                result = objSoap.callObject.GetXML
                    <AccountLookupResponse.Response>(conn, source.Get(), forceJSONArray);
            }

            return result;

        }

        #endregion AccountLookupCriteria Call
    }
}

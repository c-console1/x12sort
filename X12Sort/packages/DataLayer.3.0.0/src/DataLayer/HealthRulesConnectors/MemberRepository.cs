using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;
using DataLayer.Base;
using DataLayer.StaticUtilities;
using DataLayer.HealthRulesConnectors.Enumerations;
using DataLayer.HealthRulesConnectors.Models;
using DataLayer.HealthRulesConnectors.Models.Requests;
using DataLayer.HealthRulesConnectors.Models.Responses;

namespace DataLayer.HealthRulesConnectors
{
    public class MemberRepository : BaseRepository<HealthRulesConnectorsDatabase>
    {
        internal MemberRepository(HealthRulesConnectorsDatabase database) : base(database)
        {

        }

        #region CobPolicyList - Update COB Information
        // See HealthRules Connector Guides - Vol 5: Extracts and Services -

        /// <summary>
        /// Update COB Policy Information
        /// </summary>
        /// <param name="source">.NET Request Object</param>
        /// <returns>XML formatted string</returns>
        public string CobPolicyListText(CobPolicyListRequest source)
        {
            string xml = "";

            if (source != null)
            {
                // Base SOAP call object
                GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

                ExternalCallsProperties conn = new ExternalCallsProperties()
                {
                    username = objSoap.username,
                    password = objSoap.password,
                    url = "COBPolicy"
                };

                xml = objSoap.callObject.Get(conn, source.Get());
            }

            return xml;
        }

        /// <summary>
        /// Update COB Policy Information
        /// </summary>
        /// <param name="source">.NET Request Object</param>
        /// <returns>XML Document Response object</returns>
        public XmlDocument CobPolicyListXML(CobPolicyListRequest source)
        {
            XmlDocument doc = new XmlDocument();

            if (source != null)
            {
                // Base SOAP call object
                GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

                ExternalCallsProperties conn = new ExternalCallsProperties()
                {
                    username = objSoap.username,
                    password = objSoap.password,
                    url = "COBPolicy"
                };

                string xml = objSoap.callObject.Get(conn, source.Get());
                doc.LoadXml(xml);
            }

            return doc;
        }

        /// <summary>
        /// Update COB Policy Information
        /// </summary>
        /// <param name="source">.NET Request Object</param>
        /// <returns>.NET Response object</returns>
        public CobPolicyListResponse.Response CobPolicyList(CobPolicyListRequest source)
        {
            CobPolicyListResponse.Response result = new CobPolicyListResponse.Response();

            if (source != null)
            {
                // Base SOAP call object
                GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

                ExternalCallsProperties conn = new ExternalCallsProperties()
                {
                    username = objSoap.username,
                    password = objSoap.password,
                    url = "COBPolicy"
                };

                // Singlular instances of an XML object into a List or IEnuermable will deserialize as
                // singlular object. IE what should be seen as a List<string> will deserialize as string
                // and break the model.
                List<string> forceJSONList = new List<string>()
            {
                "cobPolicy", "error"
            };

                result = objSoap.callObject.GetXML
                    <CobPolicyListResponse.Response>(conn, source.Get(), forceJSONList);
            }

            return result;
        }

        /// <summary>
        /// Update COB Policy Information
        /// </summary>
        /// <param name="memberHccID">Member HCC ID</param>
        /// <param name="startDate">Start Date</param>
        /// <param name="endDate">End Date</param>
        /// <param name="cobPolicyId">Policy ID</param>
        /// <param name="cobPolicyIdentifier">Policy Identifer</param>
        /// <returns>.NET Response object</returns>
        public CobPolicyListResponse.Response CobPolicyList(string memberHccID, string startDate, string endDate, string cobPolicyId = "", string cobPolicyIdentifier = "")
        {
            CobPolicyListRequest source = new CobPolicyListRequest
            {

                // Attach call to a member
                memberHccID = memberHccID,

                // Initialize the list of policies
                cobPolicy = new List<CobPolicyListRequest.CobPolicy>()
            };

            // Create a single to add, add basic information
            CobPolicyListRequest.CobPolicy indPolicy = new CobPolicyListRequest.CobPolicy
            {
                cobPolicyId = cobPolicyId,
                cobPolicyIdentifier = cobPolicyIdentifier,

                // Initialize the list of data ranges this policy will cover
                dateRanges = new List<CobPolicyListRequest.DateRanges>()
            };

            // Set an individual date range for the policy
            CobPolicyListRequest.DateRanges indDateRange = new CobPolicyListRequest.DateRanges
            {
                startDate = startDate,
                endDate = endDate
            };

            // Add the date range to the policy
            indPolicy.dateRanges.Add(indDateRange);

            // Add the individual policy to the list of policies
            source.cobPolicy.Add(indPolicy);

            // Get the response
            return CobPolicyList(source);
        }

        #endregion CobPolicyList

        #region CobPolicyLookup

        /// <summary>
        /// CobPolicyLookup Connector Call that returns an XML string
        /// </summary>
        /// <param name="source">.NET Policy Lookup Request object</param>
        /// <returns>XML formmatted string</returns>
        public string CobPolicyLookupText(CobPolicyLookupRequest source)
        {
            string xml = "";

            if (source != null)
            {
                // Base SOAP call object
                GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

                ExternalCallsProperties conn = new ExternalCallsProperties()
                {
                    username = objSoap.username,
                    password = objSoap.password,
                    url = "COBPolicyLookup"
                };

                xml = objSoap.callObject.Get(conn, source.Get());
            }

            return xml;
        }

        /// <summary>
        /// CobPolicyLookup Connector Call that returns an XMLDocument object
        /// </summary>
        /// <param name="source">.NET Policy Lookup Request object</param>
        /// <returns>XML Document object</returns>
        public XmlDocument CobPolicyLookupXML(CobPolicyLookupRequest source)
        {
            string xml = CobPolicyLookupText(source);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            return doc;

        }

        /// <summary>
        /// CobPolicyLookup Connector Call that returns an .NET object
        /// </summary>
        /// <param name="source">.NET Policy Lookup Request object</param>
        /// <returns>.NET Response object</returns>
        public CobPolicyLookupResponse.Response CobPolicyLookup(CobPolicyLookupRequest source)
        {
            CobPolicyLookupResponse.Response result = new CobPolicyLookupResponse.Response();

            if (source != null)
            {
                // Base SOAP call object
                GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

                ExternalCallsProperties conn = new ExternalCallsProperties()
                {
                    username = objSoap.username,
                    password = objSoap.password,
                    url = "COBPolicyLookup"
                };

                // If a single XML object comes in and gets desialized to an object that is a list
                // it will create an error unless our deserilizing knows which field to treat as
                // lists.
                List<string> forceJSONArray = new List<string>() { "cobPolicyId", "error" };

                result = objSoap.callObject.GetXML
                    <CobPolicyLookupResponse.Response>(conn, source.Get(), forceJSONArray);
            }

            return result;
        }

        /// <summary>
        /// CobPolicyLookup Connector Call that returns an .NET object
        /// </summary>
        /// <param name="memberHccId">HealthRules ID</param>
        /// <param name="asOfDate">Display COB ID's equal to or later than this date</param>
        /// <returns>.NET Response object</returns>
        public CobPolicyLookupResponse.Response cobPolicyLookup(string memberHccId = "", string asOfDate = "")
        {
            CobPolicyLookupRequest source = new CobPolicyLookupRequest()
            {
                asOfDate = asOfDate,
                memberHccId = memberHccId
            };

            return CobPolicyLookup(source);
        }

        #endregion CobPolicyLookup

        #region EnrollmentLookup

        /// <summary>
        /// EnrollmentLookup Connector Call that returns an XML string
        /// and the XML of Request object
        /// </summary>
        /// <param name="requestObject">.NET Request object</param>
        /// <param name="request">XML of the Request object</param>
        /// <returns>XML formmatted string</returns>
        public string EnrollmentLookupText(EnrollmentLookupRequest requestObject, out string request)
        {
            request = requestObject.Get();

            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "EnrollmentSparseLookup"
            };

            string xml = objSoap.callObject.Get(conn, request);
            objSoap = null;
            return xml;
        }

        /// <summary>
        /// EnrollmentLookup Connector Call that returns an XML string
        /// </summary>
        /// <param name="requestObject">.NET Request object</param>
        /// <returns>XML formmatted string</returns>
        public string EnrollmentLookupText(EnrollmentLookupRequest requestObject)
        {
            string request;
            string output = EnrollmentLookupText(requestObject, out request);
            return output;
        }

        /// <summary>
        /// EnrollmentLookup Connector Call that returns an XMLDocument object
        /// and XML text of .NET text of the Request object
        /// </summary>
        /// <param name="requestObject">NET Request object</param>
        /// <param name="requestXML">XML string of the Request Object</param>
        /// <returns>XMLDocument object</returns>
        public XmlDocument EnrollmentLookupXML(EnrollmentLookupRequest requestObject, out string requestXML)
        {
            // Return the SOAP XML of the request object
            requestXML = requestObject.Get();

            // Get string content from EnrollmentLookupText(source) object
            string xml = EnrollmentLookupText(requestObject);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            return doc;
        }

        /// <summary>
        /// EnrollmentLookup Connector Call that returns an XMLDocument object
        /// </summary>
        /// <param name="requestObject">NET Request object</param>
        /// <returns>XMLDocument object</returns>
        public XmlDocument EnrollmentLookupXML(EnrollmentLookupRequest requestObject)
        {
            string requestXML;
            XmlDocument doc = EnrollmentLookupXML(requestObject, out requestXML);
            return doc;

        }


        /// <summary>
        /// EnrollmentLookup Connector Call that returns a .NET Response object
        /// , XML of the Request object, and XML of the Response object
        /// </summary>
        /// <param name="source">.NET Policy Lookup Request object</param>
        /// <param name="requestXML">XML string of the Request Object</param>
        /// <param name="responseXML">.NET Policy Lookup Request object</param>
        /// <returns>XML formmatted string</returns>
        public EnrollmentLookupResponse.Envelope EnrollmentLookup(
            EnrollmentLookupRequest source, out string requestXML, out string responseXML)
        {

            // Get content from the EnrollmentLookupText(..) method
            string xml = EnrollmentLookupText(source, out requestXML);
            responseXML = xml;

            // Create a root attribute for HealthRules Connectors. This will
            // almost always be 'Envelope'
            XmlRootAttribute xRoot = new XmlRootAttribute
            {
                ElementName = "Envelope",
                Namespace = "http://schemas.xmlsoap.org/soap/envelope/",
                IsNullable = true
            };

            // Put the SOAP string into a String Reader
            StringReader reader = new StringReader(xml);

            // Deserialize string into object
            EnrollmentLookupResponse.Envelope output =
                (EnrollmentLookupResponse.Envelope)new XmlSerializer(typeof(EnrollmentLookupResponse.Envelope), xRoot)
                .Deserialize(reader);

            reader.Dispose();

            return output;
        }

        /// <summary>
        /// EnrollmentLookup Connector Call that returns a .NET Response object
        /// , XML of the Request object, and XML of the Response object
        /// </summary>
        /// <param name="source">.NET Policy Lookup Request object</param>
        /// <returns>XML formmatted string</returns>
        public EnrollmentLookupResponse.Envelope EnrollmentLookup(EnrollmentLookupRequest source)
        {
            string requestXML;
            string responseXML;

            EnrollmentLookupResponse.Envelope output = EnrollmentLookup(
                source, out requestXML, out responseXML);
            return output;
        }

        /// <summary>
        /// Child call that returns just the member information of the EnrollmentLookup
        /// Connector Call, XML of the Request object, and XML of the Response object
        /// </summary>
        /// <param name="source">.NET Request object</param>
        /// <returns>.NET Member Object</returns>
        public EnrollmentLookupResponse.member[] EnrollmentCOB(EnrollmentLookupRequest source
            , out string requestXML, out string responseXML)
        {
            requestXML = "";
            responseXML = "";

            try
            {
                EnrollmentLookupResponse.Envelope result =
                    EnrollmentLookup(source, out requestXML, out responseXML);
                return result.Body.enrollmentSparseLookupResponse.enrollment.member;
            }
            catch (Exception)
            {
                return null;
            }

        }

        /// <summary>
        /// Child call that returns just the member information of the EnrollmentLookup
        /// Connector Call, XML of the Request object, and XML of the Response object
        /// </summary>
        /// <param name="source">.NET Request object</param>
        /// <returns>.NET Member Object</returns>
        public EnrollmentLookupResponse.member[] EnrollmentCOB(EnrollmentLookupRequest source)
        {

            try
            {
                // Get the member information if it exists
                EnrollmentLookupResponse.Envelope result = EnrollmentLookup(source);
                return result.Body.enrollmentSparseLookupResponse.enrollment.member;
            }
            catch (Exception)
            {
                // Cannot get information, return null value
                return null;
            }

        }


        #endregion EnrollmentLookup

        #region MemberCoverageEventLookup

        /// <summary>
        /// Member Coverage Event Lookup
        /// </summary>
        /// <param name="source">.NET Request object</param>
        /// <returns>XML formatted string</returns>
        public string MemberCoverageEventLookupText(MemberCoverageEventLookupRequest source)
        {
            string result = "";

            if (source != null)
            {
                // Base SOAP call object
                GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

                ExternalCallsProperties conn = new ExternalCallsProperties()
                {
                    username = objSoap.username,
                    password = objSoap.password,
                    url = "MemberCoverageEventLookup"
                };

                result = objSoap.callObject.Get(conn, source.Get());
            }

            return result;
        }

        /// <summary>
        /// Member Coverage Event Lookup
        /// </summary>
        /// <param name="source">.NET Request object</param>
        /// <returns>XML Document object</returns>
        public XmlDocument MemberCoverageEventLookupXml(MemberCoverageEventLookupRequest source)
        {
            string xml = MemberCoverageEventLookupText(source);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            return doc;
        }

        /// <summary>
        /// Member Coverage Event Lookup
        /// </summary>
        /// <param name="source">.NET Request object</param>
        /// <returns>.NET Response object</returns>
        public MemberCoverageEventLookupResponse.Response MemberCoverageEventLookup(MemberCoverageEventLookupRequest source)
        {
            MemberCoverageEventLookupResponse.Response result = new MemberCoverageEventLookupResponse.Response();

            if (source != null)
            {
                // Base SOAP call object
                GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

                ExternalCallsProperties conn = new ExternalCallsProperties()
                {
                    username = objSoap.username,
                    password = objSoap.password,
                    url = "MemberCoverageEventLookup"
                };

                result = objSoap.callObject.GetXML
                    <MemberCoverageEventLookupResponse.Response>(conn, source.Get());
            }

            return result;
        }

        /// <summary>
        /// Member Coverage Event Lookup
        /// </summary>
        /// <param name="startDate">Event Start Date - Formatted as 'YYYY-MM-DD'</param>
        /// <param name="eventsTypeList">List of Event Type Codes</param>
        /// <returns>.NET Response object</returns>
        public MemberCoverageEventLookupResponse.Response MemberCoverageEventLookup(DateTime startDate, List<string> eventsTypeList)
        {
            if (eventsTypeList == null || eventsTypeList.Count == 0)
                throw new Exception("At least 1 event type must be set");

            MemberCoverageEventLookupRequest source = new MemberCoverageEventLookupRequest
            {
                startDate = startDate.ToString("yyyy-MM-dd"),
                eventsTypeList = eventsTypeList
            };

            return MemberCoverageEventLookup(source);
        }

        #endregion MemberCoverageEventLookup

        #region MembershipLookup

        /// <summary>
        /// Lookup Member's records
        /// </summary>
        /// <param name="source">.NET Request object</param>
        /// <returns>XML formatted string</returns>
        public string MembershipLookupText(MembershipLookupRequest source)
        {
            string result = "";

            if (source != null)
            {
                // Base SOAP call object
                GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

                ExternalCallsProperties conn = new ExternalCallsProperties()
                {
                    username = objSoap.username,
                    password = objSoap.password,
                    url = "MembershipLookup"
                };

                result = objSoap.callObject.Get(conn, source.Get());
            }

            return result;
        }

        /// <summary>
        /// Lookup Member's records
        /// </summary>
        /// <param name="source">.NET Request object</param>
        /// <returns>XML Document Response object</returns>
        public XmlDocument MembershipLookupXML(MembershipLookupRequest source)
        {
            XmlDocument xml = new XmlDocument();

            if (source != null)
            {
                // Base SOAP call object
                GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

                ExternalCallsProperties conn = new ExternalCallsProperties()
                {
                    username = objSoap.username,
                    password = objSoap.password,
                    url = "MembershipLookup"
                };

                string result = objSoap.callObject.Get(conn, source.Get());
                xml = new XmlDocument();
                xml.LoadXml(result);
            }

            return xml;
        }


        /// <summary>
        /// Lookup Member's records
        /// </summary>
        /// <param name="source">.NET Request object</param>
        /// <returns>.NET Response object</returns>
        public MembershipLookupResponse.Response MembershipLookup(MembershipLookupRequest source)
        {
            MembershipLookupResponse.Response result = new MembershipLookupResponse.Response();

            if (source != null)
            {
                // Base SOAP call object
                GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

                ExternalCallsProperties conn = new ExternalCallsProperties()
                {
                    username = objSoap.username,
                    password = objSoap.password,
                    url = "MembershipLookup"
                };


                // Fields that that our deserilizers HAS to see as arrays
                List<string> forceJSONList = new List<string>() {
                "authorizedIndividual", "confidentialCommunicationDirective", "CredentialSuffix",
                "dependentVerification", "diagnosisReference", "error", "HIPAAAuthorization",
                "HIPAAPermission", "HIPAARestriction", "identificationNumber", "language",
                "memberPhysicalAddress", "membershipUDT", "otherName",
                "otherResponsiblePersonInfo", "preExDiagnosisCodeRangeCondition",
                "preExistingConditionInfo", "preExProcedureCodeRangeCondition",
                "raceOrEthnicityCodes", "serviceReference", "suffix", "telephoneNumber",
                "toothName", "udtListValueSet"
            };

                result = objSoap.callObject.GetXML
                    <MembershipLookupResponse.Response>(conn, source.Get(), forceJSONList);
            }

            return result;
        }

        /// <summary>
        /// Lookup Member's records
        /// </summary>
        /// <param name="asOfDate">As of date</param>
        /// <param name="id">Source ID</param>
        /// <param name="memberFirstName">Member first name</param>
        /// <param name="memberLastName">Member last name</param>
        /// <param name="definitionName">Search type definition</param>
        /// <returns>.NET Response object</returns>
        public MembershipLookupResponse.Response MembershipLookup(string asOfDate = "", string id = "", string memberFirstName = "", string memberLastName = "", string definitionName = "MemberMatchDefinition")
        {
            // Generate soap envolope
            MembershipLookupRequest source = new MembershipLookupRequest
            {
                definitionName = definitionName
            };
            if (!string.IsNullOrWhiteSpace(asOfDate))
                source.asOfDate = DateTime.Parse(asOfDate).ToString("yyyy-MM-dd");
            source.id = id;
            source.member = new MembershipLookupRequest.Member
            {
                firstName = memberFirstName,
                lastName = memberLastName
            };

            // Make the call
            return MembershipLookup(source);
        }

        /// <summary>
        /// Member lookup, raw XML string
        /// </summary>
        /// <param name="asOfDate">As of date</param>
        /// <param name="id">Source ID</param>
        /// <param name="memberFirstName">Member first name</param>
        /// <param name="memberLastName">Member last name</param>
        /// <param name="definitionName">Search type definition</param>
        /// <returns></returns>
        public string MembershipLookupText(string asOfDate = "", string id = "", string memberFirstName = "", string memberLastName = "", string definitionName = "MemberMatchDefinition")
        {
            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

            // Generate soap envolope
            MembershipLookupRequest source = new MembershipLookupRequest
            {
                definitionName = definitionName
            };
            if (!string.IsNullOrWhiteSpace(asOfDate))
                source.asOfDate = DateTime.Parse(asOfDate).ToString("yyyy-MM-dd");
            source.id = id;
            source.member = new MembershipLookupRequest.Member
            {
                firstName = memberFirstName,
                lastName = memberLastName
            };

            // Create a connection object that describes how to connect to the SOAP call
            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "MembershipLookup"
            };

            // Get the output
            string output = objSoap.callObject.Get(conn, source.Get());
            objSoap = null;

            return output;
        }

        /// <summary>
        /// Member lookup, returns XML Document so you can run XPath queries
        /// </summary>
        /// <param name="asOfDate">As of date</param>
        /// <param name="id">Source ID</param>
        /// <param name="memberFirstName">Member first name</param>
        /// <param name="memberLastName">Member last name</param>
        /// <param name="definitionName">Search type definition</param>
        /// <returns></returns>
        public XmlDocument MembershipLookupXML(string asOfDate = "", string id = "", string memberFirstName = "", string memberLastName = "", string definitionName = "MemberMatchDefinition")
        {
            string xml = MembershipLookupText(asOfDate, id, memberFirstName, memberLastName, definitionName);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            return doc;
        }

        #endregion MembershipLookup

        #region MembershipSparseLookup

        /// <summary>
        /// MembershipSparseLookup Connector Call that returns an XML string
        /// and the XML of Request object
        /// </summary>
        /// <param name="requestObject">.NET Request object</param>
        /// <param name="request">XML of the Request object</param>
        /// <returns>XML formmatted string</returns>
        public string MembershipSparseLookupText(MembershipSparseLookupRequest requestObject, out string request)
        {
            request = requestObject.Get();

            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "MembershipSparseLookup"
            };

            string xml = objSoap.callObject.Get(conn, request);
            objSoap = null;

            return xml;
        }

        /// <summary>
        /// MembershipSparseLookup Connector Call that returns an XML string
        /// </summary>
        /// <param name="requestObject">.NET Request object</param>
        /// <returns>XML formmatted string</returns>
        public string MembershipSparseLookupText(MembershipSparseLookupRequest requestObject)
        {
            string request;
            string output = MembershipSparseLookupText(requestObject, out request);
            return output;
        }

        /// <summary>
        /// MembershipSparseLookup Connector Call that returns an XMLDocument object
        /// and XML text of .NET text of the Request object
        /// </summary>
        /// <param name="requestObject">NET Request object</param>
        /// <param name="requestXML">XML string of the Request Object</param>
        /// <returns>XMLDocument object</returns>
        public XmlDocument MembershipSparseLookupXML(MembershipSparseLookupRequest requestObject, out string requestXML)
        {
            // Return the SOAP XML of the request object
            requestXML = requestObject.Get();

            // Get string content from MembershipSparseLookupText(source) object
            string xml = MembershipSparseLookupText(requestObject);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            return doc;
        }

        /// <summary>
        /// MembershipSparseLookup Connector Call that returns an XMLDocument object
        /// </summary>
        /// <param name="requestObject">NET Request object</param>
        /// <returns>XMLDocument object</returns>
        public XmlDocument MembershipSparseLookupXML(MembershipSparseLookupRequest requestObject)
        {
            string requestXML;
            XmlDocument doc = MembershipSparseLookupXML(requestObject, out requestXML);
            return doc;

        }

        /// <summary>
        /// MembershipSparseLookup Connector Call that returns a .NET Response object
        /// , XML of the Request object, and XML of the Response object
        /// </summary>
        /// <param name="source">.NET Policy Lookup Request object</param>
        /// <param name="requestXML">XML string of the Request Object</param>
        /// <param name="responseXML">.NET Policy Lookup Request object</param>
        /// <returns>XML formmatted string</returns>
        public MembershipSparseLookupResponse.Envelope MembershipSparseLookup(
            MembershipSparseLookupRequest source, out string requestXML, out string responseXML)
        {

            // Get content from the MembershipSparseLookupText(..) method
            string xml = MembershipSparseLookupText(source, out requestXML);
            responseXML = xml;

            // Create a root attribute for HealthRules Connectors. This will
            // almost always be 'Envelope'
            XmlRootAttribute xRoot = new XmlRootAttribute
            {
                ElementName = "Envelope",
                Namespace = "http://schemas.xmlsoap.org/soap/envelope/",
                IsNullable = true
            };

            // Put the SOAP string into a String Reader
            StringReader reader = new StringReader(xml);

            // Deserialize string into object
            MembershipSparseLookupResponse.Envelope output =
                (MembershipSparseLookupResponse.Envelope)new XmlSerializer(typeof(MembershipSparseLookupResponse.Envelope), xRoot)
                .Deserialize(reader);

            reader.Dispose();

            return output;
        }

        /// <summary>
        /// MembershipSparseLookup Connector Call that returns a .NET Response object
        /// , XML of the Request object, and XML of the Response object
        /// </summary>
        /// <param name="source">.NET Policy Lookup Request object</param>
        /// <returns>XML formmatted string</returns>
        public MembershipSparseLookupResponse.Envelope MembershipSparseLookup(MembershipSparseLookupRequest source)
        {
            string requestXML;
            string responseXML;

            MembershipSparseLookupResponse.Envelope output = MembershipSparseLookup(
                source, out requestXML, out responseXML);
            return output;
        }

        /// <summary>
        /// Child call that returns just the member information of the MembershipSparseLookup
        /// Connector Call, XML of the Request object, and XML of the Response object
        /// </summary>
        /// <param name="source">.NET Request object</param>
        /// <returns>.NET Member Object</returns>
        public MembershipSparseLookupResponse.cobPolicy[] MembershipCOB(MembershipSparseLookupRequest source
            , out string requestXML, out string responseXML)
        {
            requestXML = "";
            responseXML = "";

            try
            {
                MembershipSparseLookupResponse.Envelope result =
                    MembershipSparseLookup(source, out requestXML, out responseXML);
                return result.COBPolicies;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Child call that returns just the member information of the MembershipSparseLookup
        /// Connector Call, XML of the Request object, and XML of the Response object
        /// </summary>
        /// <param name="source">.NET Request object</param>
        /// <returns>.NET Member Object</returns>
        public MembershipSparseLookupResponse.cobPolicy[] MembershipCOB(MembershipSparseLookupRequest source)
        {

            try
            {
                // Get the member information if it exists
                MembershipSparseLookupResponse.Envelope result = MembershipSparseLookup(source);
                return result.COBPolicies;
            }
            catch (Exception)
            {
                // Cannot get information, return null value
                return null;
            }

        }

        #endregion MembershipSparseLookup

        #region MembershipReferenceLookup

        /// <summary>
        /// Membership reference lookup
        /// </summary>
        /// <param name="source">.NET Request object</param>
        /// <returns>XML Formated </returns>
        public string MembershipReferenceLookupText(MembershipReferenceLookupRequest source)
        {

            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

            // Build connection object
            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "MembershipReferenceLookup"
            };

            // Get raw XML and then format it as a human readable format
            string xml = objSoap.callObject.Get(conn, source.Get());
            objSoap = null;

            xml = DataLayer.StaticUtilities.XMLSerialization.PrintXML(xml);

            return xml;
        }

        /// <summary>
        /// Membership reference lookup
        /// </summary>
        /// <param name="asOfDate">As Of Last Change Date</param>
        /// <param name="id">Member ID</param>
        /// <param name="memberFirstName">Member First Name</param>
        /// <param name="memberLastName">Member Last Name</param>
        /// <param name="definitionName">Search Type Defination</param>
        /// <returns>XML Formated </returns>
        public string MembershipReferenceLookupText(string asOfDate = "", string id = "", string memberFirstName = "", string memberLastName = "", string searchDefinitionName = "MemberMatchDefinition")
        {
            MembershipReferenceLookupRequest source = new MembershipReferenceLookupRequest
            {
                definitionName = searchDefinitionName
            };
            if (!string.IsNullOrWhiteSpace(asOfDate))
                source.asOfDate = DateTime.Parse(asOfDate).ToString("yyyy-MM-dd");
            source.id = id;
            source.member = new MembershipReferenceLookupRequest.Member
            {
                firstName = memberFirstName,
                lastName = memberLastName
            };

            string xml = MembershipReferenceLookupText(source);
            return xml;
        }

        /// <summary>
        /// Membership reference lookup
        /// </summary>
        /// <param name="asOfDate">As Of Last Change Date</param>
        /// <param name="id">Member ID</param>
        /// <param name="memberFirstName">Member First Name</param>
        /// <param name="memberLastName">Member Last Name</param>
        /// <param name="definitionName">Search Type Defination</param>
        /// <returns>XML Document object </returns>
        public XmlDocument MembershipReferenceLookupXML(MembershipReferenceLookupRequest source)
        {
            string xml = MembershipReferenceLookupText(source);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            return doc;
        }

        /// <summary>
        /// Membership reference lookup
        /// </summary>
        /// <param name="asOfDate">As Of Last Change Date</param>
        /// <param name="id">Member ID</param>
        /// <param name="memberFirstName">Member First Name</param>
        /// <param name="memberLastName">Member Last Name</param>
        /// <param name="definitionName">Search Type Defination</param>
        /// <returns>XML Document object </returns>
        public XmlDocument MembershipReferenceLookupXML(string asOfDate = "", string id = "", string memberFirstName = "", string memberLastName = "", string definitionName = "MemberMatchDefinition")
        {
            string xml = MembershipReferenceLookupText(asOfDate, id, memberFirstName, memberLastName, definitionName);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            return doc;
        }

        /// <summary>
        /// Membership reference lookup
        /// </summary>
        /// <param name="source">.NET Request object</param>
        /// <returns>.NET Response object</returns>
        public MembershipReferenceLookupResponse.Response MembershipReferenceLookup(MembershipReferenceLookupRequest source)
        {
            MembershipReferenceLookupResponse.Response result = new MembershipReferenceLookupResponse.Response();

            if (source != null)
            {
                // Base SOAP call object
                GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

                // Build connection object
                ExternalCallsProperties conn = new ExternalCallsProperties()
                {
                    username = objSoap.username,
                    password = objSoap.password,
                    url = "MembershipReferenceLookup"
                };

                // Fields that that our deserilizers HAS to see as arrays
                List<string> forceJSONList = new List<string>()
            {
                "error"
            };

                // Get a C# object
                result = objSoap.callObject.GetXML
                    <MembershipReferenceLookupResponse.Response>(conn, source.Get(), forceJSONList);
            }

            return result;
        }

        /// <summary>
        /// Membership reference lookup
        /// </summary>
        /// <param name="asOfDate">As Of Last Change Date</param>
        /// <param name="id">Member ID</param>
        /// <param name="memberFirstName">Member First Name</param>
        /// <param name="memberLastName">Member Last Name</param>
        /// <param name="definitionName">Search Type Defination</param>
        /// <returns></returns>
        public MembershipReferenceLookupResponse.Response MembershipReferenceLookup(string asOfDate = "", string id = "", string memberFirstName = "", string memberLastName = "", string definitionName = "MemberMatchDefinition")
        {
            MembershipReferenceLookupRequest source = new MembershipReferenceLookupRequest
            {
                definitionName = string.IsNullOrWhiteSpace(definitionName) ? "MemberMatchDefinition" : definitionName
            };

            if (!string.IsNullOrWhiteSpace(asOfDate))
                source.asOfDate = DateTime.Parse(asOfDate).ToString("yyyy-MM-dd");
            source.id = id;
            source.member = new MembershipReferenceLookupRequest.Member
            {
                firstName = string.IsNullOrWhiteSpace(memberFirstName) ? "" : memberFirstName,
                lastName = string.IsNullOrWhiteSpace(memberFirstName) ? "" : memberFirstName
            };

            return MembershipReferenceLookup(source);

        }

        #endregion MembershipReferenceLookup

        #region MemberServiceStronglyTyped - FindMember

        /// <summary>
        /// Find member - raw request object
        /// </summary>
        /// <param name="source"></param>
        /// <returns>XML string of Member #'s that match criteria</returns>
        public string FindMemberText(FindMemberRequest source)
        {
            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.Classic);

            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "MemberServiceStronglyTyped"
            };

            string result = objSoap.callObject.Get(conn, source.Get());
            objSoap = null;

            return result;
        }

        /// <summary>
        /// Find member - raw request object
        /// </summary>
        /// <param name="source"></param>
        /// <returns>XmlDocument of Member #'s that match criteria</returns>
        public XmlDocument FindMemberXML(FindMemberRequest source)
        {
            string xml = FindMemberText(source);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            return doc;
        }

        /// <summary>
        /// Find member - raw request object
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public FindMemberResponse.Response FindMember(FindMemberRequest source)
        {
            FindMemberResponse.Response result = new FindMemberResponse.Response();

            if (source != null)
            {
                // Base SOAP call object
                GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.Classic);

                ExternalCallsProperties conn = new ExternalCallsProperties()
                {
                    username = objSoap.username,
                    password = objSoap.password,
                    url = "MemberServiceStronglyTyped"
                };

                List<string> forceJSONList = new List<string>() { "memberId" };

                result = objSoap.callObject.GetXML
                    <FindMemberResponse.Response>(conn, source.Get(), forceJSONList);
            }

            return result;
        }

        /// <summary>
        /// Find member
        /// </summary>
        /// <param name="hccBenefitPlanID"></param>
        /// <param name="benefitPlanType"></param>
        /// <param name="benefitPlanName"></param>
        /// <param name="asOfDate"></param>
        /// <returns></returns>
        public FindMemberResponse.Response FindMember(string hccBenefitPlanID = "", string benefitPlanType = "", string benefitPlanName = "", string asOfDate = "")
        {
            FindMemberRequest source = new FindMemberRequest
            {
                criteria = new FindMemberRequest.Criteria
                {
                    hccBenefitPlanID = hccBenefitPlanID,
                    benefitPlanType = benefitPlanType,
                    benefitPlanName = benefitPlanName,
                    asOfDate = asOfDate
                }
            };

            return FindMember(source);
        }

        /// <summary>
        /// Find member
        /// </summary>
        /// <param name="hccBenefitPlanID"></param>
        /// <param name="benefitPlanType"></param>
        /// <param name="benefitPlanName"></param>
        /// <param name="asOfDate"></param>
        /// <returns>Return just a list of ID's</returns>
        public List<string> FindMemberList(string hccBenefitPlanID = "", string benefitPlanType = "", string benefitPlanName = "", string asOfDate = "")
        {
            FindMemberRequest source = new FindMemberRequest
            {
                criteria = new FindMemberRequest.Criteria
                {
                    hccBenefitPlanID = hccBenefitPlanID,
                    benefitPlanType = benefitPlanType,
                    benefitPlanName = benefitPlanName,
                    asOfDate = asOfDate
                }
            };

            FindMemberResponse.Response response = FindMember(source);

            try
            {
                return response.memberIds;
            }
            catch (Exception)
            {
                return null;
            }
        }

        #endregion MemberServiceStronglyTyped - FindMember

        #region MemberServiceStronglyTyped - GetMemberDetailsByHccId

        /// <summary>
        /// Get raw XML document, results are freeform
        /// </summary>
        /// <param name="source">.NET Request object</param>
        /// <returns>Raw XML string</returns>
        public string GetMemberDetailsByHccIdText(GetMemberDetailsByHccIdRequest source)
        {
            string result = "";

            if (source != null)
            {
                // Base SOAP call object
                GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.Classic);

                ExternalCallsProperties conn = new ExternalCallsProperties()
                {
                    username = objSoap.username,
                    password = objSoap.password,
                    url = "MemberServiceStronglyTyped"
                };

                result = objSoap.callObject.Get(conn, source.Get());
            }

            return result;

        }

        /// <summary>
        /// Get raw XML document, results are freeform
        /// </summary>
        /// <param name="memberNumber">HealthRules Member Number</param>
        /// <returns>Raw XML string</returns>
        public string GetMemberDetailsByHccIdText(string memberNumber)
        {
            GetMemberDetailsByHccIdRequest request = new GetMemberDetailsByHccIdRequest
            {
                memberHccId = memberNumber
            };
            return GetMemberDetailsByHccIdText(request);
        }

        /// <summary>
        /// Get freeform member details in a raw XML document
        /// </summary>
        /// <param name="source">.NET Request object</param>
        /// <returns>XML Document</returns>
        public XmlDocument GetMemberDetailsByHccIdXML(GetMemberDetailsByHccIdRequest source)
        {
            string result = GetMemberDetailsByHccIdText(source);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(result);
            return doc;
        }

        /// <summary>
        /// Get freeform member details in a raw XML document
        /// </summary>
        /// <param name="memberNumber">HealthRules Member Number</param>
        /// <returns>XML Document</returns>
        public XmlDocument GetMemberDetailsByHccIdXML(string memberNumber)
        {
            GetMemberDetailsByHccIdRequest request = new GetMemberDetailsByHccIdRequest
            {
                memberHccId = memberNumber
            };

            string result = GetMemberDetailsByHccIdText(request);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(result);
            return doc;
        }

        /// <summary>
        /// Get member details in a .NET object.  Due to free form nature of the data
        /// not all fields or section will be filled out.  You may have to custom parse the XML
        /// that was broken out.
        /// </summary>
        /// <param name="source">.NET Request object</param>
        /// <returns>.NET response object</returns>
        public GetMemberDetailsByHccResponse GetMemberDetailsByHccId(GetMemberDetailsByHccIdRequest source)
        {
            // Instantiate object
            GetMemberDetailsByHccResponse output = new GetMemberDetailsByHccResponse();

            // Try to parse object
            try
            {
                string xml = GetMemberDetailsByHccIdText(source);
                output.Parse(xml);
                return output;
            }
            catch (Exception ex)
            {
                //  Could not parse object
                // Set invalid flag and return object with exception values
                output.IsValid = false;
                output.ParseException = ex;
                return output;
            }

        }

        /// <summary>
        /// Get member by Member #
        /// Get member details in a .NET object.  Due to free form nature of the data
        /// not all fields or section will be filled out.  You may have to custom parse the XML
        /// that was broken out.
        /// </summary>
        /// <param name="memberNumber">HealthRules Member Number</param>
        /// <returns>.NET response object</returns>
        public GetMemberDetailsByHccResponse GetMemberDetailsByHccId(string memberNumber)
        {
            GetMemberDetailsByHccIdRequest request = new GetMemberDetailsByHccIdRequest
            {
                memberHccId = memberNumber
            };

            return GetMemberDetailsByHccId(request);
        }


        #endregion MemberServiceStronglyTyped - GetMemberDetailsByHccId

        #region MemberServiceStronglyTyped - UpdateProviderSelection

        /// <summary>
        /// Update Provider Selection
        /// </summary>
        /// <param name="source">NET Request object</param>
        /// <returns>XML formatted string</returns>
        public string UpdateProviderSelectionText(UpdateProviderSelectionRequest source)
        {
            string result = "";

            if (source != null)
            {
                // Base SOAP call object
                GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.Classic);

                ExternalCallsProperties conn = new ExternalCallsProperties()
                {
                    username = objSoap.username,
                    password = objSoap.password,
                    url = "MemberServiceStronglyTyped"
                };

                //List<string> forceJSONList = new List<string>() { "memberId" };

                result = objSoap.callObject.Get(conn, source.Get());
            }

            return result;
        }

        /// <summary>
        /// Update Provider Selection
        /// </summary>
        /// <param name="source">NET Request object</param>
        /// <returns>XML Document response</returns>
        public XmlDocument UpdateProviderSelectionXML(UpdateProviderSelectionRequest source)
        {
            string result = UpdateProviderSelectionText(source);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(result);
            return doc;
        }

        /// <summary>
        /// Update Provider Selection
        /// </summary>
        /// <param name="memberHccId">Member HCC ID</param>
        /// <param name="selectionType">Selection Type</param>
        /// <param name="providerSelectionReasonCode">Provider Selection Reason Code</param>
        /// <param name="forceToWorkbasket">Force to Workbasket</param>
        /// <param name="messageCode">Message Code</param>
        /// <param name="messageDescription">Message scription</param>
        /// <returns>>NET Response object</returns>
        public UpdateProviderSelectionResponse.Response UpdateProviderSelection(string memberHccId, string selectionType, string providerSelectionReasonCode, string forceToWorkbasket, string messageCode, string messageDescription)
        {
            UpdateProviderSelectionRequest source = new UpdateProviderSelectionRequest
            {
                memberHccId = memberHccId,
                selectionType = selectionType,
                providerSelectionReasonCode = providerSelectionReasonCode,
                forceToWorkbasket = forceToWorkbasket
            };
            source.auditLogInfo.messageCode = messageCode;
            source.auditLogInfo.messageDescription = messageDescription;
            return UpdateProviderSelection(source);
        }

        /// <summary>
        /// Update Provider Selection
        /// </summary>
        /// <param name="source">NET Request object</param>
        /// <returns>>NET Response object</returns>
        public UpdateProviderSelectionResponse.Response UpdateProviderSelection(UpdateProviderSelectionRequest source)
        {
            UpdateProviderSelectionResponse.Response result = new UpdateProviderSelectionResponse.Response();

            if (source != null)
            {
                // Base SOAP call object
                GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.Classic);

                ExternalCallsProperties conn = new ExternalCallsProperties()
                {
                    username = objSoap.username,
                    password = objSoap.password,
                    url = "MemberServiceStronglyTyped"
                };

                List<string> forceJSONList = new List<string>() { "exceptions" };

                result = objSoap.callObject.GetXML
                    <UpdateProviderSelectionResponse.Response>(conn, source.Get(), forceJSONList);
            }

            return result;
        }

        #endregion MemberServiceStronglyTyped - UpdateProviderSelection

        #region Update Member's Address

        /// <summary>
        /// Update Member's address
        /// </summary>
        /// <param name="source">Raw XML Envolpe</param>
        /// <returns>XML formatted string</returns>
        public string UpdateMemberAddressText(string source)
        {
            // Base SOAP call object
            GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.Classic);

            ExternalCallsProperties conn = new ExternalCallsProperties()
            {
                username = objSoap.username,
                password = objSoap.password,
                url = "MemberServiceStronglyTyped"
            };

            string result = objSoap.callObject.Get(conn, source);
            objSoap = null;

            return result;
        }

        /// <summary>
        /// Update Member's address
        /// </summary>
        /// <param name="source">.NET Address Update Request</param>
        /// <returns>XML formatted string</returns>
        public string UpdateMemberAddressText(MembershipLookupRequest source)
        {
            string result = "";

            if (source != null)
            {
                // Base SOAP call object
                GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.Classic);

                ExternalCallsProperties conn = new ExternalCallsProperties()
                {
                    username = objSoap.username,
                    password = objSoap.password,
                    url = "MemberServiceStronglyTyped"
                };

                result = objSoap.callObject.Get(conn, source.Get());
            }

            return result;
        }

        /// <summary>
        /// Update Member's address
        /// </summary>
        /// <param name="source">.NET Address Update Request</param>
        /// <returns>XMLDocument object</returns>
        public XmlDocument UpdateMemberAddressXML(MembershipLookupRequest source)
        {
            string xml = UpdateMemberAddressText(source);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            return doc;
        }


        #endregion Update Member's Address
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Newtonsoft.Json;
using DataLayer.StaticUtilities;

namespace DataLayer.HealthRulesConnectors
{

    /// <summary>
    /// External call settings
    /// </summary>
    public class ExternalCallsProperties
    {

        /// <summary>
        /// Content type
        /// </summary>
        public string contentType = "text/xml;charset=\"utf-8\""; // JSON should be "application/json"

        /// <summary>
        /// Base URL
        /// </summary>
        public string url { get; set; }

        /// <summary>
        /// Username
        /// </summary>
        public string username { get; set; }

        /// <summary>
        /// Password
        /// </summary>
        public string password { get; set; }

        /// <summary>
        /// Access token - ussually an encoded username and password
        /// </summary>
        public string accessToken { get; set; }

        /// <summary>
        /// Specialized headers
        /// </summary>
        public Dictionary<string, string> headers = new Dictionary<string, string>();

        /// <summary>
        /// Is preauthication required?
        /// </summary>
        public bool preAuthenticate = false;

        /// <summary>
        /// Accept type
        /// </summary>
        public string acceptType = "text/xml"; // JSON should be "application/json"

        /// <summary>
        /// 
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// Verb (GET, POST, PUT, DELETE, or custom request that REST may have)
        /// </summary>
        public string verb = "POST";

    }
}

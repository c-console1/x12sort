using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataLayer.HealthRulesConnectors.Models
{
    /// <summary>
    /// Enumerated Type based 20.2_HealthRules_Connector_Guide_Vol5_Extracts_and_Services.pdf
    /// Chapter 19: Enumerated Types Appendix, page 1184
    /// </summary>
    public class EnumeratedType
    {
        /// <summary>
        /// Initalize value
        /// </summary>
        public EnumeratedType(string attributeType, string value, params string [] altValues)
        {
            AttributeType = attributeType;
            Value = value;
            if (altValues == null)
                AltValues = new List<string>();
            else
                AltValues = altValues.ToList();
        }

        /// <summary>
        /// Enumerated Attribute Type
        /// </summary>
        public string AttributeType { get; protected set; }

        /// <summary>
        /// Value to match with
        /// </summary>
        public string Value { get; protected set; }

        /// <summary>
        /// Altername value such as 'm' for 'Male'
        /// </summary>
        public List<string> AltValues { get; protected set; }

        #region Helper functions

        /// <summary>
        /// Does the requested value match this object's value?
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool MatchValue(string value)
        {
            if (value == null)
                return false;

            return value.Trim().ToUpper() == Value.ToUpper();
        }

        /// <summary>
        /// Does the requested value match this object's value?
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool MatchAltValue(string value)
        {
            if (value == null)
                return false;

            return AltValues.Any(x => x.ToUpper() == value.Trim().ToUpper());
        }

        #endregion

    }
}

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using Newtonsoft.Json;
using DataLayer.StaticUtilities;

namespace DataLayer.HealthRulesConnectors.Models.Requests
{
    public class AccountLookupRequest
    {
        /// <summary>
        /// Generate the serialized response object
        /// </summary>
        /// <returns></returns>
        public string Get()
        {
            // Format values
            AsOfDate = Validation.DateFormat(AsOfDate);

            // Serialize response and send
            return XMLSerialization.BuildRequestObject(this, resRequests.AccountLookup);
        }

        [JsonProperty("accountIdentifier")]
        [XmlElement(ElementName = "accountIdentifier")]
        public AccountIdentifierObject AccountIdentifier { get; set; }

        [JsonProperty("parentAccountIdentifier")]
        [XmlElement(ElementName = "parentAccountIdentifier")]
        public ParentAccountIdentifierObject ParentAccountIdentifier { get; set; }

        [JsonProperty("topAccountIdentifier")]
        [XmlElement(ElementName = "topAccountIdentifier")]
        public TopAccountIdentifierObject TopAccountIdentifier { get; set; }

        [JsonProperty("taxId")]
        [XmlElement(ElementName = "taxId")]
        public string TaxID { get; set; }

        [JsonProperty("asOfDate")]
        [XmlElement(ElementName = "asOfDate")]
        public string AsOfDate { get; set; }

        [JsonProperty("otherIdentificationNumber")]
        [XmlElement(ElementName = "otherIdentificationNumber")]
        public string OtherIdentificationNumber { get; set; }

        [JsonProperty("identificationType")]
        [XmlElement(ElementName = "identificationType")]
        public IdentificationTypeObject IdentificationType { get; set; }

        public class AccountIdentifierObject
        {
            [JsonProperty("hccIdentificationNumber")]
            [XmlElement(ElementName = "hccIdentificationNumber")]
            public string HCCIdentificationNumber { get; set; }

            [JsonProperty("referenceIdentificationNumber")]
            [XmlElement(ElementName = "referenceIdentificationNumber")]
            public string ReferenceIdentificationNumber { get; set; }

            [JsonProperty("name")]
            [XmlElement(ElementName = "name")]
            public string Name { get; set; }

        }

        public class ParentAccountIdentifierObject
        {
            [JsonProperty("hccIdentificationNumber")]
            [XmlElement(ElementName = "hccIdentificationNumber")]
            public string HCCIdentificationNumber { get; set; }

            [JsonProperty("name")]
            [XmlElement(ElementName = "name")]
            public string Name { get; set; }
        }

        public class TopAccountIdentifierObject
        {
            [JsonProperty("hccIdentificationNumber")]
            [XmlElement(ElementName = "hccIdentificationNumber")]
            public string HCCIdentificationNumber { get; set; }

            [JsonProperty("referenceIdentificationNumber")]
            [XmlElement(ElementName = "referenceIdentificationNumber")]
            public string ReferenceIdentificationNumber { get; set; }

            [JsonProperty("name")]
            [XmlElement(ElementName = "name")]
            public string Name { get; set; }
        }

        public class IdentificationTypeObject
        {
            [JsonProperty("codeSetName")]
            [XmlElement(ElementName = "codeSetName")]
            public string CodeSetName { get; set; }

            [JsonProperty("codeEntry")]
            [XmlElement(ElementName = "codeEntry")]
            public string CodeEntry { get; set; }

            [JsonProperty("shortName")]
            [XmlElement(ElementName = "shortName")]
            public string ShortName { get; set; }
        }
    }
}

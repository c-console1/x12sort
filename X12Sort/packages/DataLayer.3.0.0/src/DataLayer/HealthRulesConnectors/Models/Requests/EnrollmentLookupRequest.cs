using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using DataLayer.StaticUtilities;
using Newtonsoft.Json;

namespace DataLayer.HealthRulesConnectors.Models.Requests
{
    /// <summary>
    /// Enrollment Lookup Request
    /// </summary>
    public class EnrollmentLookupRequest
    {
        /// <summary>
        /// Get the XML Request object
        /// </summary>
        /// <returns>XML Request object</returns>
        public string Get()
        {
            // Serialize response and send
            return XMLSerialization.BuildRequestObject(this, resRequests.EnrollmentLookup);
        }

        /// <summary>
        /// Backing fields for properties
        /// </summary>
        private BackfillValues backingFields = new BackfillValues();

        /// <summary>
        /// definitionName
        /// <summary>
        [JsonProperty("definitionName")]
        [XmlElement(ElementName = "definitionName")]
        public string DefinitionName
        {
            get {
                return
                  string.IsNullOrWhiteSpace(backingFields["DefinitionName"])
                  ? "MemberMatchDefinition" : backingFields["DefinitionName"];
            }
            set { backingFields.AddString("DefinitionName", value, "MemberMatchDefinition"); }
        }

        /// <summary>
        /// Account ID
        /// Optional:
        /// <summary>
        [JsonProperty("id")]
        [XmlElement(ElementName = "id")]
        public string ID
        {
            get { return backingFields["ID"]; }
            set { backingFields.AddString("ID", value); }
        }

        /// <summary>
        /// Top Account ID
        /// Optional:
        /// <summary>
        [JsonProperty("topAccount")]
        [XmlElement(ElementName = "topAccount")]
        public string TopAccount
        {
            get { return backingFields["TopAccount"]; }
            set { backingFields.AddString("TopAccount", value); }
        }

        /// <summary>
        /// customString1
        /// Optional:
        /// <summary>
        [JsonProperty("customString1")]
        [XmlElement(ElementName = "customString1")]
        public string CustomString1
        {
            get { return backingFields["CustomString1"]; }
            set { backingFields.AddString("CustomString1", value); }
        }

        /// <summary>
        /// customString2
        /// Optional:
        /// <summary>
        [JsonProperty("customString2")]
        [XmlElement(ElementName = "customString2")]
        public string CustomString2
        {
            get { return backingFields["CustomString2"]; }
            set { backingFields.AddString("CustomString2", value); }
        }

        /// <summary>
        /// customString3
        /// Optional:
        /// <summary>
        [JsonProperty("customString3")]
        [XmlElement(ElementName = "customString3")]
        public string CustomString3
        {
            get { return backingFields["CustomString3"]; }
            set { backingFields.AddString("CustomString3", value); }
        }

        /// <summary>
        /// customString4
        /// Optional:
        /// <summary>
        [JsonProperty("customString4")]
        [XmlElement(ElementName = "customString4")]
        public string CustomString4
        {
            get { return backingFields["CustomString4"]; }
            set { backingFields.AddString("CustomString4", value); }
        }

        /// <summary>
        /// customInteger1
        /// Optional:
        /// <summary>
        [JsonProperty("customInteger1")]
        [XmlElement(ElementName = "customInteger1")]
        public string CustomInteger1
        {
            get { return backingFields["CustomInteger1"]; }
            set { backingFields.AddInt("CustomInteger1", value); }
        }

        /// <summary>
        /// customInteger2
        /// Optional:
        /// <summary>
        [JsonProperty("customInteger2")]
        [XmlElement(ElementName = "customInteger2")]
        public string CustomInteger2
        {
            get { return backingFields["CustomInteger2"]; }
            set { backingFields.AddInt("CustomInteger2", value); }
        }

        /// <summary>
        /// customInteger3
        /// Optional:
        /// <summary>
        [JsonProperty("customInteger3")]
        [XmlElement(ElementName = "customInteger3")]
        public string CustomInteger3
        {
            get { return backingFields["CustomInteger3"]; }
            set { backingFields.AddInt("CustomInteger3", value); }
        }

        /// <summary>
        /// customInteger4
        /// Optional:
        /// <summary>
        [JsonProperty("customInteger4")]
        [XmlElement(ElementName = "customInteger4")]
        public string CustomInteger4
        {
            get { return backingFields["CustomInteger4"]; }
            set { backingFields.AddInt("CustomInteger4", value); }
        }

        /// <summary>
        /// customDate1
        /// Optional:
        /// <summary>
        [JsonProperty("customDate1")]
        [XmlElement(ElementName = "customDate1")]
        public string CustomDate1
        {
            get { return backingFields["CustomDate1"]; }
            set { backingFields.AddDate("CustomDate1", value); }
        }

        /// <summary>
        /// customDate2
        /// Optional:
        /// <summary>
        [JsonProperty("customDate2")]
        [XmlElement(ElementName = "customDate2")]
        public string CustomDate2
        {
            get { return backingFields["CustomDate2"]; }
            set { backingFields.AddDate("CustomDate2", value); }
        }

        /// <summary>
        /// customDate3
        /// Optional:
        /// <summary>
        [JsonProperty("customDate3")]
        [XmlElement(ElementName = "customDate3")]
        public string CustomDate3
        {
            get { return backingFields["CustomDate3"]; }
            set { backingFields.AddDate("CustomDate3", value); }
        }

        /// <summary>
        /// customDate4
        /// Optional:
        /// <summary>
        [JsonProperty("customDate4")]
        [XmlElement(ElementName = "customDate4")]
        public string CustomDate4
        {
            get { return backingFields["CustomDate4"]; }
            set { backingFields.AddDate("CustomDate4", value); }
        }

        /// <summary>
        /// customDecimal1
        /// Optional:
        /// <summary>
        [JsonProperty("customDecimal1")]
        [XmlElement(ElementName = "customDecimal1")]
        public string CustomDecimal1
        {
            get { return backingFields["CustomDecimal1"]; }
            set { backingFields.AddDecimal("CustomDecimal1", value); }
        }

        /// <summary>
        /// customDecimal2
        /// Optional:
        /// <summary>
        [JsonProperty("customDecimal2")]
        [XmlElement(ElementName = "customDecimal2")]
        public string CustomDecimal2
        {
            get { return backingFields["CustomDecimal2"]; }
            set { backingFields.AddDecimal("CustomDecimal2", value); }
        }

        /// <summary>
        /// customDecimal3
        /// Optional:
        /// <summary>
        [JsonProperty("customDecimal3")]
        [XmlElement(ElementName = "customDecimal3")]
        public string CustomDecimal3
        {
            get { return backingFields["CustomDecimal3"]; }
            set { backingFields.AddDecimal("CustomDecimal3", value); }
        }

        /// <summary>
        /// customDecimal4
        /// Optional:
        /// <summary>
        [JsonProperty("customDecimal4")]
        [XmlElement(ElementName = "customDecimal4")]
        public string CustomDecimal4
        {
            get { return backingFields["CustomDecimal4"]; }
            set { backingFields.AddDecimal("CustomDecimal4", value); }
        }

        /// <summary>
        /// asOfDate
        /// Optional:
        /// <summary>
        [JsonProperty("asOfDate")]
        [XmlElement(ElementName = "asOfDate")]
        public string AsOfDate
        {
            get { return backingFields["AsOfDate"]; }
            set { backingFields.AddDate("AsOfDate", value); }
        }
        /// <summary>
        /// latestEndorsementEffectiveDate
        /// Optional:
        /// <summary>
        [JsonProperty("latestEndorsementEffectiveDate")]
        [XmlElement(ElementName = "latestEndorsementEffectiveDate")]
        public string LatestEndorsementEffectiveDate
        {
            get { return backingFields["LatestEndorsementEffectiveDate"]; }
            set { backingFields.AddDate("LatestEndorsementEffectiveDate", value); }
        }


        /// <summary>
        /// member
        /// Optional:
        /// <summary>
        [JsonProperty("member")]
        [XmlElement(ElementName = "member")]
        public EnrollmentLookupCriteriaMember Member { get; set; }

        /// <summary>
        /// subscriber
        /// Optional:
        /// <summary>
        [JsonProperty("subscriber")]
        [XmlElement(ElementName = "subscriber")]
        public EnrollmentLookupCriteriaSubscriber Subscriber { get; set; }

        /// <summary>
        /// EnrollmentLookupCriteriaSubscriber
        /// Optional:
        /// <summary>
        public class EnrollmentLookupCriteriaSubscriber
        {
            /// <summary>
            /// Backing fields for properties
            /// </summary>
            private BackfillValues backingFields = new BackfillValues();

            /// <summary>
            /// firstName
            /// Optional:
            /// <summary>
            [JsonProperty("firstName")]
            [XmlElement(ElementName = "firstName")]
            public string FirstName
            {
                get { return backingFields["FirstName"]; }
                set { backingFields.AddString("FirstName", value); }
            }

            /// <summary>
            /// lastName
            /// Optional:
            /// <summary>
            [JsonProperty("lastName")]
            [XmlElement(ElementName = "lastName")]
            public string LastName
            {
                get { return backingFields["LastName"]; }
                set { backingFields.AddString("LastName", value); }
            }

            /// <summary>
            /// fullName
            /// Optional:
            /// <summary>
            [JsonProperty("fullName")]
            [XmlElement(ElementName = "fullName")]
            public string FullName
            {
                get { return backingFields["FullName"]; }
                set { backingFields.AddString("FullName", value); }
            }

            /// <summary>
            /// gender
            /// Optional:
            /// <summary>
            [JsonProperty("gender")]
            [XmlElement(ElementName = "gender")]
            public string Gender
            {
                get { return backingFields["Gender"]; }
                set { backingFields.AddGender("Gender", value, "Subscriber.Gender"); }
            }

            /// <summary>
            /// dateOfBirth
            /// Optional:
            /// <summary>
            [JsonProperty("dateOfBirth")]
            [XmlElement(ElementName = "dateOfBirth")]
            public string DateOfBirth
            {
                get { return backingFields["DateOfBirth"]; }
                set { backingFields.AddDate("DateOfBirth", value, "Subscriber.DateOfBirth"); }
            }

            /// <summary>
            /// taxId
            /// Optional:
            /// <summary>
            [JsonProperty("taxId")]
            [XmlElement(ElementName = "taxId")]
            public string TaxID
            {
                get { return backingFields["TaxID"]; }
                set { backingFields.AddString("TaxID", value); }
            }

            /// <summary>
            /// address
            /// Optional:
            /// <summary>
            [JsonProperty("address")]
            [XmlElement(ElementName = "address")]
            public SubscriberAddress Address { get; set; }

        }

        /// <summary>
        /// SubscriberAddress
        /// Optional:
        /// <summary>
        public class SubscriberAddress
        {
            /// <summary>
            /// Backing fields for properties
            /// </summary>
            private BackfillValues backingFields = new BackfillValues();

            /// <summary>
            /// address
            /// Optional:
            /// <summary>
            [JsonProperty("address")]
            [XmlElement(ElementName = "address")]
            public string Address
            {
                get { return backingFields["Address"]; }
                set { backingFields.AddString("Address", value); }
            }

            /// <summary>
            /// cityName
            /// Optional:
            /// <summary>
            [JsonProperty("cityName")]
            [XmlElement(ElementName = "cityName")]
            public string CityName
            {
                get { return backingFields["CityName"]; }
                set { backingFields.AddString("CityName", value); }
            }

            /// <summary>
            /// stateCode
            /// Optional:
            /// <summary>
            [JsonProperty("stateCode")]
            [XmlElement(ElementName = "stateCode")]
            public string StateCode
            {
                get { return backingFields["StateCode"]; }
                set { backingFields.AddString("StateCode", value); }
            }

            /// <summary>
            /// zipCode
            /// Optional:
            /// <summary>
            [JsonProperty("zipCode")]
            [XmlElement(ElementName = "zipCode")]
            public string ZipCode
            {
                get { return backingFields["ZipCode"]; }
                set { backingFields.AddString("ZipCode", value); }
            }

            /// <summary>
            /// zipExtensionCode
            /// Optional:
            /// <summary>
            [JsonProperty("zipExtensionCode")]
            [XmlElement(ElementName = "zipExtensionCode")]
            public string ZipExtensionCode
            {
                get { return backingFields["ZipExtensionCode"]; }
                set { backingFields.AddString("ZipExtensionCode", value); }
            }

            /// <summary>
            /// countryCode
            /// Optional:
            /// <summary>
            [JsonProperty("countryCode")]
            [XmlElement(ElementName = "countryCode")]
            public string CountryCode
            {
                get { return backingFields["CountryCode"]; }
                set { backingFields.AddString("CountryCode", value); }
            }

        }

        /// <summary>
        /// EnrollmentLookupCriteriaMember
        /// Optional:
        /// <summary>
        public class EnrollmentLookupCriteriaMember
        {
            /// <summary>
            /// Backing fields for properties
            /// </summary>
            private BackfillValues backingFields = new BackfillValues();

            /// <summary>
            /// firstName
            /// Optional:
            /// <summary>
            [JsonProperty("firstName")]
            [XmlElement(ElementName = "firstName")]
            public string FirstName
            {
                get { return backingFields["FirstName"]; }
                set { backingFields.AddString("FirstName", value); }
            }

            /// <summary>
            /// lastName
            /// Optional:
            /// <summary>
            [JsonProperty("lastName")]
            [XmlElement(ElementName = "lastName")]
            public string LastName
            {
                get { return backingFields["LastName"]; }
                set { backingFields.AddString("LastName", value); }
            }

            /// <summary>
            /// fullName
            /// Optional:
            /// <summary>
            [JsonProperty("fullName")]
            [XmlElement(ElementName = "fullName")]
            public string FullName
            {
                get { return backingFields["FullName"]; }
                set { backingFields.AddString("FullName", value); }
            }

            /// <summary>
            /// gender
            /// Optional:
            /// <summary>
            [JsonProperty("gender")]
            [XmlElement(ElementName = "gender")]
            public string Gender
            {
                get { return backingFields["Gender"]; }
                set { backingFields.AddGender("Gender", value); }
            }

            /// <summary>
            /// dateOfBirth
            /// Optional:
            /// <summary>
            [JsonProperty("dateOfBirth")]
            [XmlElement(ElementName = "dateOfBirth")]
            public string DateOfBirth
            {
                get { return backingFields["DateOfBirth"]; }
                set { backingFields.AddDate("DateOfBirth", value, "Member.DateOfBirth"); }
            }

            /// <summary>
            /// taxId - Optional
            /// <summary>
            [JsonProperty("taxId")]
            [XmlElement(ElementName = "taxId")]
            public string TaxID
            {
                get { return backingFields["TaxID"]; }
                set { backingFields.AddString("TaxID", value); }
            }

            /// <summary>
            /// Address object
            /// Optional:
            /// <summary>
            [JsonProperty("address")]
            [XmlElement(ElementName = "address")]
            public MemberAddress Address { get; set; }

        }

        /// <summary>
        /// MemberAddress
        /// Optional:
        /// <summary>
        public class MemberAddress
        {
            /// <summary>
            /// Backing fields for properties
            /// </summary>
            private BackfillValues backingFields = new BackfillValues();

            /// <summary>
            /// address
            /// Optional:
            /// <summary>
            [JsonProperty("address")]
            [XmlElement(ElementName = "address")]
            public string Address
            {
                get { return backingFields["Address"]; }
                set { backingFields.AddString("Address", value); }
            }

            /// <summary>
            /// cityName
            /// Optional:
            /// <summary>
            [JsonProperty("cityName")]
            [XmlElement(ElementName = "cityName")]
            public string CityName
            {
                get { return backingFields["CityName"]; }
                set { backingFields.AddString("CityName", value); }
            }

            /// <summary>
            /// stateCode
            /// Optional:
            /// <summary>
            [JsonProperty("stateCode")]
            [XmlElement(ElementName = "stateCode")]
            public string StateCode
            {
                get { return backingFields["StateCode"]; }
                set { backingFields.AddString("StateCode", value); }
            }

            /// <summary>
            /// zipCode
            /// Optional:
            /// <summary>
            [JsonProperty("zipCode")]
            [XmlElement(ElementName = "zipCode")]
            public string ZipCode
            {
                get { return backingFields["ZipCode"]; }
                set { backingFields.AddString("ZipCode", value); }
            }

            /// <summary>
            /// zipExtensionCode
            /// Optional:
            /// <summary>
            [JsonProperty("zipExtensionCode")]
            [XmlElement(ElementName = "zipExtensionCode")]
            public string ZipExtensionCode
            {
                get { return backingFields["ZipExtensionCode"]; }
                set { backingFields.AddString("ZipExtensionCode", value); }
            }

            /// <summary>
            /// countryCode
            /// Optional:
            /// <summary>
            [JsonProperty("countryCode")]
            [XmlElement(ElementName = "countryCode")]
            public string CountryCode
            {
                get { return backingFields["CountryCode"]; }
                set { backingFields.AddString("CountryCode", value); }
            }

        }

    }

}

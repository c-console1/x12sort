using System;
using DataLayer.StaticUtilities;

namespace DataLayer.HealthRulesConnectors.Models.Requests
{
    /// <summary>
    /// Membership Lookup
    /// </summary>
    public class MembershipLookupRequest
    {
        /// <summary>
        /// Get / Generate the XML Membership Lookup Request XML
        /// </summary>
        /// <returns></returns>
        public string Get()
        {
            // Validate request
            if (string.IsNullOrWhiteSpace(this.definitionName))
                this.definitionName = "MemberMatchDefinition";

            // Build object
            string output = XMLSerialization.BuildRequestObject(this, resRequests.MembershipLookup);

            // * Return request objects
            return output;
        }

        /// <summary>
        /// Member Lookup Definition - Predefined lookup method
        /// </summary>
        public string definitionName { get; set; }

        /// <summary>
        /// HCC ID # - Recommended
        /// </summary>
        public string id { get; set; }
        public string topAccount { get; set; }
        public string customString1 { get; set; }
        public string customString2 { get; set; }
        public string customString3 { get; set; }
        public string customString4 { get; set; }
        public string customInteger1 { get; set; }
        public string customInteger2 { get; set; }
        public string customInteger3 { get; set; }
        public string customInteger4 { get; set; }
        public string customDate1 { get; set; }
        public string customDate2 { get; set; }
        public string customDate3 { get; set; }
        public string customDate4 { get; set; }
        public string customDecimal1 { get; set; }
        public string customDecimal2 { get; set; }
        public string customDecimal3 { get; set; }
        public string customDecimal4 { get; set; }

        /// <summary>
        /// Lookup by member information
        /// </summary>
        public Member member = new Member();

        /// <summary>
        /// Lookup by subscriber information
        /// </summary>
        public Subscriber subscriber = new Subscriber();

        /// <summary>
        /// As of Last Update Date - Formatted as 'YYYY-MM-DD'
        /// </summary>
        public string asOfDate { get; set; }

        /// <summary>
        /// Member lookup object
        /// </summary>
        public class Member
        {
            /// <summary>
            /// First name
            /// </summary>
            public string firstName { get; set; }

            /// <summary>
            /// Last name
            /// </summary>
            public string lastName { get; set; }

            /// <summary>
            /// Full name
            /// </summary>
            public string fullName { get; set; }

            /// <summary>
            /// 1 letter gender code
            /// </summary>
            public string gender { get; set; }

            /// <summary>
            /// Date of Birth formatted as 'YYYY-MM-DD'
            /// </summary>
            public string dateOfBirth { get; set; }

            /// <summary>
            /// Tax ID
            /// </summary>
            public string taxId { get; set; }

            /// <summary>
            /// Member address information
            /// </summary>
            public Address address = new Address();
        }

        /// <summary>
        /// Subscriber object
        /// </summary>
        public class Subscriber
        {
            /// <summary>
            /// First Name
            /// </summary>
            public string firstName { get; set; }

            /// <summary>
            /// Last Name
            /// </summary>
            public string lastName { get; set; }

            /// <summary>
            /// Full Name
            /// </summary>
            public string fullName { get; set; }

            /// <summary>
            /// 1 letter Gender Code
            /// </summary>
            public string gender { get; set; }

            /// <summary>
            /// Date of Birth formatted as 'YYYY-MM-DD'
            /// </summary>
            public string dateOfBirth { get; set; }

            /// <summary>
            /// Tax ID
            /// </summary>
            public string taxId { get; set; }

            /// <summary>
            /// Address
            /// </summary>
            public Address address = new Address();
        }

    }

}

using System;
using System.Collections.Generic;
using System.Text;
using DataLayer.StaticUtilities;

namespace DataLayer.HealthRulesConnectors.Models.Requests
{
    public class AccountPropertyRequest
    {

        /// <summary>
        /// Generate the serialized response object
        /// </summary>
        /// <returns></returns>
        public string Get()
        {
            // Serialize response and send
            return XMLSerialization.BuildRequestObject(this, resRequests.AccountProperty);
        }

        public MaintenanceInfo maintenanceInfo { get; set; }
        public string maintenanceComment { get; set; }
        public MaintenanceReasonCode maintenanceReasonCode { get; set; }
        public AccountProperty accountProperty { get; set; }

        public class MaintenanceInfo
        {
            public string transactionType { get; set; }
        }

        public class MaintenanceReasonCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class AccountProperty
        {
            public Account account { get; set; }
            public BillingHoldReasonCode billingHoldReasonCode { get; set; }
        }

        public class Account
        {
            public string accountHccIdentificationNumber { get; set; }
        }

        public class BillingHoldReasonCode
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

    }
}

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using DataLayer.StaticUtilities;
using Newtonsoft.Json;

namespace DataLayer.HealthRulesConnectors.Models.Requests
{
    public class ExternalPremiumPaymentCoreRequest
    {
        /// <summary>
        /// Generate the serialized response object
        /// </summary>
        /// <returns></returns>
        public string Get()
        {


            // Serialize response and send
            return XMLSerialization.BuildRequestObject(this, resRequests.ExternalPremiumPaymentCore);
        }

        /// <summary>
        /// isPaymentAndBill
        /// Optional:
        /// <summary>
        [JsonProperty("isPaymentAndBill")]
        [XmlElement(ElementName = "isPaymentAndBill")]
        public string IsPaymentAndBill { get; set; }

        /// <summary>
        /// transactNum
        /// Optional:
        /// <summary>
        [JsonProperty("transactNum")]
        [XmlElement(ElementName = "transactNum")]
        public string TransactNum { get; set; }

        /// <summary>
        /// paidStartDate
        /// Optional:
        /// <summary>
        [JsonProperty("paidStartDate")]
        [XmlElement(ElementName = "paidStartDate")]
        public string PaidStartDate { get; set; }

        /// <summary>
        /// paidThroughDate
        /// Optional:
        /// <summary>
        [JsonProperty("paidThroughDate")]
        [XmlElement(ElementName = "paidThroughDate")]
        public string PaidThroughDate { get; set; }

        /// <summary>
        /// accountReference
        /// Optional:
        /// <summary>
        [JsonProperty("accountReference")]
        [XmlElement(ElementName = "accountReference")]
        public ExternalPremiumPaymentAccountReference AccountReference { get; set; }

        /// <summary>
        /// accountMatchData
        /// Optional:
        /// <summary>
        [JsonProperty("accountMatchData")]
        [XmlElement(ElementName = "accountMatchData")]
        public ExternalPremiumPaymentAccountMatchData AccountMatchData { get; set; }

        /// <summary>
        /// subscriptionReference
        /// Optional:
        /// <summary>
        [JsonProperty("subscriptionReference")]
        [XmlElement(ElementName = "subscriptionReference")]
        public ExternalPremiumPaymentSubscriptionReference SubscriptionReference { get; set; }

        /// <summary>
        /// memberMatchData
        /// Optional:
        /// <summary>
        [JsonProperty("memberMatchData")]
        [XmlElement(ElementName = "memberMatchData")]
        public ExternalPremiumPaymentMemberMatchData MemberMatchData { get; set; }

        /// <summary>
        /// paymentNumber
        /// Optional:
        /// <summary>
        [JsonProperty("paymentNumber")]
        [XmlElement(ElementName = "paymentNumber")]
        public string PaymentNumber { get; set; }

        /// <summary>
        /// totalAmount
        /// Optional:
        /// <summary>
        [JsonProperty("totalAmount")]
        [XmlElement(ElementName = "totalAmount")]
        public string TotalAmount { get; set; }

        /// <summary>
        /// paymentDate
        /// Optional:
        /// <summary>
        [JsonProperty("paymentDate")]
        [XmlElement(ElementName = "paymentDate")]
        public string PaymentDate { get; set; }

        /// <summary>
        /// receiptDate
        /// Optional:
        /// <summary>
        [JsonProperty("receiptDate")]
        [XmlElement(ElementName = "receiptDate")]
        public string ReceiptDate { get; set; }

        /// <summary>
        /// paymentType
        /// Optional:
        /// <summary>
        [JsonProperty("paymentType")]
        [XmlElement(ElementName = "paymentType")]
        public ExternalPremiumPaymentPaymentType PaymentType { get; set; }

        /// <summary>
        /// externalPremiumPaymentRoster
        /// Zero or more repetitions:
        /// <summary>
        [JsonProperty("externalPremiumPaymentRoster")]
        [XmlElement(ElementName = "externalPremiumPaymentRoster")]
        public List<ExternalPremiumPaymentExternalPremiumPaymentRoster> ExternalPremiumPaymentRoster { get; set; }

        /// <summary>
        /// ExternalPremiumPaymentExternalPremiumPaymentRoster
        /// Zero or more repetitions:
        /// <summary>
        public class ExternalPremiumPaymentExternalPremiumPaymentRoster
        {
            /// <summary>
            /// startDate
            /// Optional:
            /// <summary>
            [JsonProperty("startDate")]
            [XmlElement(ElementName = "startDate")]
            public string StartDate { get; set; }

            /// <summary>
            /// throughDate
            /// Optional:
            /// <summary>
            [JsonProperty("throughDate")]
            [XmlElement(ElementName = "throughDate")]
            public string ThroughDate { get; set; }

            /// <summary>
            /// amount
            /// Optional:
            /// <summary>
            [JsonProperty("amount")]
            [XmlElement(ElementName = "amount")]
            public string Amount { get; set; }

            /// <summary>
            /// accountReference
            /// Optional:
            /// <summary>
            [JsonProperty("accountReference")]
            [XmlElement(ElementName = "accountReference")]
            public ExternalPremiumPaymentRosterAccountReference AccountReference { get; set; }

            /// <summary>
            /// accountMatchData
            /// Optional:
            /// <summary>
            [JsonProperty("accountMatchData")]
            [XmlElement(ElementName = "accountMatchData")]
            public ExternalPremiumPaymentRosterAccountMatchData AccountMatchData { get; set; }

            /// <summary>
            /// subscriptionReference
            /// Optional:
            /// <summary>
            [JsonProperty("subscriptionReference")]
            [XmlElement(ElementName = "subscriptionReference")]
            public ExternalPremiumPaymentRosterSubscriptionReference SubscriptionReference { get; set; }

            /// <summary>
            /// memberMatchData
            /// Optional:
            /// <summary>
            [JsonProperty("memberMatchData")]
            [XmlElement(ElementName = "memberMatchData")]
            public ExternalPremiumPaymentRosterMemberMatchData MemberMatchData { get; set; }

            /// <summary>
            /// membershipReference
            /// Optional:
            /// <summary>
            [JsonProperty("membershipReference")]
            [XmlElement(ElementName = "membershipReference")]
            public ExternalPremiumPaymentRosterMembershipReference MembershipReference { get; set; }

            /// <summary>
            /// benefitPlanMatchData
            /// Optional:
            /// <summary>
            [JsonProperty("benefitPlanMatchData")]
            [XmlElement(ElementName = "benefitPlanMatchData")]
            public ExternalPremiumPaymentRosterBenefitPlanMatchData BenefitPlanMatchData { get; set; }

            /// <summary>
            /// benefitPlanReference
            /// Optional:
            /// <summary>
            [JsonProperty("benefitPlanReference")]
            [XmlElement(ElementName = "benefitPlanReference")]
            public ExternalPremiumPaymentRosterBenefitPlanReference BenefitPlanReference { get; set; }

            /// <summary>
            /// benefitPlanType
            /// Optional:
            /// <summary>
            [JsonProperty("benefitPlanType")]
            [XmlElement(ElementName = "benefitPlanType")]
            public ExternalPremiumPaymentRosterBenefitPlanType BenefitPlanType { get; set; }

            /// <summary>
            /// billingCategory
            /// Optional:
            /// <summary>
            [JsonProperty("billingCategory")]
            [XmlElement(ElementName = "billingCategory")]
            public ExternalPremiumPaymentRosterBillingCategory BillingCategory { get; set; }

            /// <summary>
            /// reasonCode
            /// Optional:
            /// <summary>
            [JsonProperty("reasonCode")]
            [XmlElement(ElementName = "reasonCode")]
            public ExternalPremiumPaymentRosterReasonCode ReasonCode { get; set; }

            /// <summary>
            /// comments
            /// Optional:
            /// <summary>
            [JsonProperty("comments")]
            [XmlElement(ElementName = "comments")]
            public string Comments { get; set; }

        }

        /// <summary>
        /// ExternalPremiumPaymentRosterReasonCode
        /// Optional:
        /// <summary>
        public class ExternalPremiumPaymentRosterReasonCode
        {
            /// <summary>
            /// codeSetName
            /// Optional:
            /// <summary>
            [JsonProperty("codeSetName")]
            [XmlElement(ElementName = "codeSetName")]
            public string CodeSetName { get; set; }

            /// <summary>
            /// codeEntry
            /// Optional:
            /// <summary>
            [JsonProperty("codeEntry")]
            [XmlElement(ElementName = "codeEntry")]
            public string CodeEntry { get; set; }

            /// <summary>
            /// shortName
            /// Optional:
            /// <summary>
            [JsonProperty("shortName")]
            [XmlElement(ElementName = "shortName")]
            public string ShortName { get; set; }

        }

        /// <summary>
        /// ExternalPremiumPaymentRosterBillingCategory
        /// Optional:
        /// <summary>
        public class ExternalPremiumPaymentRosterBillingCategory
        {
            /// <summary>
            /// codeSetName
            /// Optional:
            /// <summary>
            [JsonProperty("codeSetName")]
            [XmlElement(ElementName = "codeSetName")]
            public string CodeSetName { get; set; }

            /// <summary>
            /// codeEntry
            /// Optional:
            /// <summary>
            [JsonProperty("codeEntry")]
            [XmlElement(ElementName = "codeEntry")]
            public string CodeEntry { get; set; }

            /// <summary>
            /// shortName
            /// Optional:
            /// <summary>
            [JsonProperty("shortName")]
            [XmlElement(ElementName = "shortName")]
            public string ShortName { get; set; }

        }

        /// <summary>
        /// ExternalPremiumPaymentRosterBenefitPlanType
        /// Optional:
        /// <summary>
        public class ExternalPremiumPaymentRosterBenefitPlanType
        {
            /// <summary>
            /// codeSetName
            /// Optional:
            /// <summary>
            [JsonProperty("codeSetName")]
            [XmlElement(ElementName = "codeSetName")]
            public string CodeSetName { get; set; }

            /// <summary>
            /// codeEntry
            /// Optional:
            /// <summary>
            [JsonProperty("codeEntry")]
            [XmlElement(ElementName = "codeEntry")]
            public string CodeEntry { get; set; }

            /// <summary>
            /// shortName
            /// Optional:
            /// <summary>
            [JsonProperty("shortName")]
            [XmlElement(ElementName = "shortName")]
            public string ShortName { get; set; }

        }

        /// <summary>
        /// ExternalPremiumPaymentRosterBenefitPlanReference
        /// Optional:
        /// <summary>
        public class ExternalPremiumPaymentRosterBenefitPlanReference
        {
            /// <summary>
            /// identifier
            /// <summary>
            [JsonProperty("identifier")]
            [XmlElement(ElementName = "identifier")]
            public string Identifier { get; set; }

        }

        /// <summary>
        /// ExternalPremiumPaymentRosterBenefitPlanMatchData
        /// Optional:
        /// <summary>
        public class ExternalPremiumPaymentRosterBenefitPlanMatchData
        {
            /// <summary>
            /// benefitPlanName
            /// Optional:
            /// <summary>
            [JsonProperty("benefitPlanName")]
            [XmlElement(ElementName = "benefitPlanName")]
            public string BenefitPlanName { get; set; }

            /// <summary>
            /// benefitPlanHccId
            /// Optional:
            /// <summary>
            [JsonProperty("benefitPlanHccId")]
            [XmlElement(ElementName = "benefitPlanHccId")]
            public string BenefitPlanHccId { get; set; }

            /// <summary>
            /// benefitPlanOtherIdentifier
            /// Optional:
            /// <summary>
            [JsonProperty("benefitPlanOtherIdentifier")]
            [XmlElement(ElementName = "benefitPlanOtherIdentifier")]
            public BenefitPlanMatchDataBenefitPlanOtherIdentifier BenefitPlanOtherIdentifier { get; set; }

            /// <summary>
            /// asOfDate
            /// Optional:
            /// <summary>
            [JsonProperty("asOfDate")]
            [XmlElement(ElementName = "asOfDate")]
            public string AsOfDate { get; set; }

        }

        /// <summary>
        /// BenefitPlanMatchDataBenefitPlanOtherIdentifier
        /// Optional:
        /// <summary>
        public class BenefitPlanMatchDataBenefitPlanOtherIdentifier
        {
            /// <summary>
            /// otherID
            /// Optional:
            /// <summary>
            [JsonProperty("otherID")]
            [XmlElement(ElementName = "otherID")]
            public string OtherID { get; set; }

            /// <summary>
            /// identificationType
            /// Optional:
            /// <summary>
            [JsonProperty("identificationType")]
            [XmlElement(ElementName = "identificationType")]
            public BenefitPlanOtherIdentifierIdentificationType IdentificationType { get; set; }

        }

        /// <summary>
        /// BenefitPlanOtherIdentifierIdentificationType
        /// Optional:
        /// <summary>
        public class BenefitPlanOtherIdentifierIdentificationType
        {
            /// <summary>
            /// codeSetName
            /// Optional:
            /// <summary>
            [JsonProperty("codeSetName")]
            [XmlElement(ElementName = "codeSetName")]
            public string CodeSetName { get; set; }

            /// <summary>
            /// codeEntry
            /// Optional:
            /// <summary>
            [JsonProperty("codeEntry")]
            [XmlElement(ElementName = "codeEntry")]
            public string CodeEntry { get; set; }

            /// <summary>
            /// shortName
            /// Optional:
            /// <summary>
            [JsonProperty("shortName")]
            [XmlElement(ElementName = "shortName")]
            public string ShortName { get; set; }

        }

        /// <summary>
        /// ExternalPremiumPaymentRosterMembershipReference
        /// Optional:
        /// <summary>
        public class ExternalPremiumPaymentRosterMembershipReference
        {
            /// <summary>
            /// identifier
            /// <summary>
            [JsonProperty("identifier")]
            [XmlElement(ElementName = "identifier")]
            public string Identifier { get; set; }

        }

        /// <summary>
        /// ExternalPremiumPaymentRosterMemberMatchData
        /// Optional:
        /// <summary>
        public class ExternalPremiumPaymentRosterMemberMatchData
        {
            /// <summary>
            /// definitionName
            /// <summary>
            [JsonProperty("definitionName")]
            [XmlElement(ElementName = "definitionName")]
            public string DefinitionName { get; set; }

            /// <summary>
            /// id
            /// Optional:
            /// <summary>
            [JsonProperty("id")]
            [XmlElement(ElementName = "id")]
            public string Id { get; set; }

            /// <summary>
            /// topAccount
            /// Optional:
            /// <summary>
            [JsonProperty("topAccount")]
            [XmlElement(ElementName = "topAccount")]
            public string TopAccount { get; set; }

            /// <summary>
            /// customString1
            /// Optional:
            /// <summary>
            [JsonProperty("customString1")]
            [XmlElement(ElementName = "customString1")]
            public string CustomString1 { get; set; }

            /// <summary>
            /// customString2
            /// Optional:
            /// <summary>
            [JsonProperty("customString2")]
            [XmlElement(ElementName = "customString2")]
            public string CustomString2 { get; set; }

            /// <summary>
            /// customString3
            /// Optional:
            /// <summary>
            [JsonProperty("customString3")]
            [XmlElement(ElementName = "customString3")]
            public string CustomString3 { get; set; }

            /// <summary>
            /// customString4
            /// Optional:
            /// <summary>
            [JsonProperty("customString4")]
            [XmlElement(ElementName = "customString4")]
            public string CustomString4 { get; set; }

            /// <summary>
            /// customInteger1
            /// Optional:
            /// <summary>
            [JsonProperty("customInteger1")]
            [XmlElement(ElementName = "customInteger1")]
            public string CustomInteger1 { get; set; }

            /// <summary>
            /// customInteger2
            /// Optional:
            /// <summary>
            [JsonProperty("customInteger2")]
            [XmlElement(ElementName = "customInteger2")]
            public string CustomInteger2 { get; set; }

            /// <summary>
            /// customInteger3
            /// Optional:
            /// <summary>
            [JsonProperty("customInteger3")]
            [XmlElement(ElementName = "customInteger3")]
            public string CustomInteger3 { get; set; }

            /// <summary>
            /// customInteger4
            /// Optional:
            /// <summary>
            [JsonProperty("customInteger4")]
            [XmlElement(ElementName = "customInteger4")]
            public string CustomInteger4 { get; set; }

            /// <summary>
            /// customDate1
            /// Optional:
            /// <summary>
            [JsonProperty("customDate1")]
            [XmlElement(ElementName = "customDate1")]
            public string CustomDate1 { get; set; }

            /// <summary>
            /// customDate2
            /// Optional:
            /// <summary>
            [JsonProperty("customDate2")]
            [XmlElement(ElementName = "customDate2")]
            public string CustomDate2 { get; set; }

            /// <summary>
            /// customDate3
            /// Optional:
            /// <summary>
            [JsonProperty("customDate3")]
            [XmlElement(ElementName = "customDate3")]
            public string CustomDate3 { get; set; }

            /// <summary>
            /// customDate4
            /// Optional:
            /// <summary>
            [JsonProperty("customDate4")]
            [XmlElement(ElementName = "customDate4")]
            public string CustomDate4 { get; set; }

            /// <summary>
            /// customDecimal1
            /// Optional:
            /// <summary>
            [JsonProperty("customDecimal1")]
            [XmlElement(ElementName = "customDecimal1")]
            public string CustomDecimal1 { get; set; }

            /// <summary>
            /// customDecimal2
            /// Optional:
            /// <summary>
            [JsonProperty("customDecimal2")]
            [XmlElement(ElementName = "customDecimal2")]
            public string CustomDecimal2 { get; set; }

            /// <summary>
            /// customDecimal3
            /// Optional:
            /// <summary>
            [JsonProperty("customDecimal3")]
            [XmlElement(ElementName = "customDecimal3")]
            public string CustomDecimal3 { get; set; }

            /// <summary>
            /// customDecimal4
            /// Optional:
            /// <summary>
            [JsonProperty("customDecimal4")]
            [XmlElement(ElementName = "customDecimal4")]
            public string CustomDecimal4 { get; set; }

            /// <summary>
            /// member
            /// Optional:
            /// <summary>
            [JsonProperty("member")]
            [XmlElement(ElementName = "member")]
            public MemberMatchDataMember Member { get; set; }

            /// <summary>
            /// subscriber
            /// Optional:
            /// <summary>
            [JsonProperty("subscriber")]
            [XmlElement(ElementName = "subscriber")]
            public MemberMatchDataMember Subscriber { get; set; }

            /// <summary>
            /// asOfDate
            /// Optional:
            /// <summary>
            [JsonProperty("asOfDate")]
            [XmlElement(ElementName = "asOfDate")]
            public string AsOfDate { get; set; }

        }



        /// <summary>
        /// MemberMatchDataMember
        /// Optional:
        /// <summary>
        public class MemberMatchDataMember
        {
            /// <summary>
            /// firstName
            /// Optional:
            /// <summary>
            [JsonProperty("firstName")]
            [XmlElement(ElementName = "firstName")]
            public string FirstName { get; set; }

            /// <summary>
            /// lastName
            /// Optional:
            /// <summary>
            [JsonProperty("lastName")]
            [XmlElement(ElementName = "lastName")]
            public string LastName { get; set; }

            /// <summary>
            /// fullName
            /// Optional:
            /// <summary>
            [JsonProperty("fullName")]
            [XmlElement(ElementName = "fullName")]
            public string FullName { get; set; }

            /// <summary>
            /// gender
            /// Optional:
            /// <summary>
            [JsonProperty("gender")]
            [XmlElement(ElementName = "gender")]
            public string Gender { get; set; }

            /// <summary>
            /// dateOfBirth
            /// Optional:
            /// <summary>
            [JsonProperty("dateOfBirth")]
            [XmlElement(ElementName = "dateOfBirth")]
            public string DateOfBirth { get; set; }

            /// <summary>
            /// taxId
            /// Optional:
            /// <summary>
            [JsonProperty("taxId")]
            [XmlElement(ElementName = "taxId")]
            public string TaxId { get; set; }

            /// <summary>
            /// address
            /// Optional:
            /// <summary>
            [JsonProperty("address")]
            [XmlElement(ElementName = "address")]
            public AddressShortCapitals Address { get; set; }

        }

        /// <summary>
        /// ExternalPremiumPaymentRosterSubscriptionReference
        /// Optional:
        /// <summary>
        public class ExternalPremiumPaymentRosterSubscriptionReference
        {
            /// <summary>
            /// identifier
            /// <summary>
            [JsonProperty("identifier")]
            [XmlElement(ElementName = "identifier")]
            public string Identifier { get; set; }

        }

        /// <summary>
        /// ExternalPremiumPaymentRosterAccountMatchData
        /// Optional:
        /// <summary>
        public class ExternalPremiumPaymentRosterAccountMatchData
        {
            /// <summary>
            /// accountHccIdentifier
            /// Optional:
            /// <summary>
            [JsonProperty("accountHccIdentifier")]
            [XmlElement(ElementName = "accountHccIdentifier")]
            public AccountMatchDataAccountHccIdentifier AccountHccIdentifier { get; set; }

            /// <summary>
            /// accountOtherIdentifier
            /// Zero or more repetitions:
            /// <summary>
            [JsonProperty("accountOtherIdentifier")]
            [XmlElement(ElementName = "accountOtherIdentifier")]
            public List<AccountMatchDataAccountOtherIdentifier> AccountOtherIdentifier { get; set; }

            /// <summary>
            /// asOfDate
            /// Optional:
            /// <summary>
            [JsonProperty("asOfDate")]
            [XmlElement(ElementName = "asOfDate")]
            public string AsOfDate { get; set; }

        }

        /// <summary>
        /// AccountMatchDataAccountOtherIdentifier
        /// Zero or more repetitions:
        /// <summary>
        public class AccountMatchDataAccountOtherIdentifier
        {
            /// <summary>
            /// otherIdentificationNumber
            /// Optional:
            /// <summary>
            [JsonProperty("otherIdentificationNumber")]
            [XmlElement(ElementName = "otherIdentificationNumber")]
            public string OtherIdentificationNumber { get; set; }

            /// <summary>
            /// identificationType
            /// Optional:
            /// <summary>
            [JsonProperty("identificationType")]
            [XmlElement(ElementName = "identificationType")]
            public TypeCode IdentificationType { get; set; }

        }


        /// <summary>
        /// AccountMatchDataAccountHccIdentifier
        /// Optional:
        /// <summary>
        public class AccountMatchDataAccountHccIdentifier
        {
            /// <summary>
            /// accountHccIdentificationNumber
            /// Optional:
            /// <summary>
            [JsonProperty("accountHccIdentificationNumber")]
            [XmlElement(ElementName = "accountHccIdentificationNumber")]
            public string AccountHccIdentificationNumber { get; set; }

            /// <summary>
            /// parentAccountHccIdentificationNumber
            /// Optional:
            /// <summary>
            [JsonProperty("parentAccountHccIdentificationNumber")]
            [XmlElement(ElementName = "parentAccountHccIdentificationNumber")]
            public string ParentAccountHccIdentificationNumber { get; set; }

            /// <summary>
            /// topAccountHccIdentificationNumber
            /// Optional:
            /// <summary>
            [JsonProperty("topAccountHccIdentificationNumber")]
            [XmlElement(ElementName = "topAccountHccIdentificationNumber")]
            public string TopAccountHccIdentificationNumber { get; set; }

        }

        /// <summary>
        /// ExternalPremiumPaymentRosterAccountReference
        /// Optional:
        /// <summary>
        public class ExternalPremiumPaymentRosterAccountReference
        {
            /// <summary>
            /// identifier
            /// <summary>
            [JsonProperty("identifier")]
            [XmlElement(ElementName = "identifier")]
            public string Identifier { get; set; }

        }

        /// <summary>
        /// ExternalPremiumPaymentPaymentType
        /// Optional:
        /// <summary>
        public class ExternalPremiumPaymentPaymentType
        {
            /// <summary>
            /// codeSetName
            /// Optional:
            /// <summary>
            [JsonProperty("codeSetName")]
            [XmlElement(ElementName = "codeSetName")]
            public string CodeSetName { get; set; }

            /// <summary>
            /// codeEntry
            /// Optional:
            /// <summary>
            [JsonProperty("codeEntry")]
            [XmlElement(ElementName = "codeEntry")]
            public string CodeEntry { get; set; }

            /// <summary>
            /// shortName
            /// Optional:
            /// <summary>
            [JsonProperty("shortName")]
            [XmlElement(ElementName = "shortName")]
            public string ShortName { get; set; }

        }

        /// <summary>
        /// ExternalPremiumPaymentMemberMatchData
        /// Optional:
        /// <summary>
        public class ExternalPremiumPaymentMemberMatchData
        {
            /// <summary>
            /// definitionName
            /// <summary>
            [JsonProperty("definitionName")]
            [XmlElement(ElementName = "definitionName")]
            public string DefinitionName { get; set; }

            /// <summary>
            /// id
            /// Optional:
            /// <summary>
            [JsonProperty("id")]
            [XmlElement(ElementName = "id")]
            public string Id { get; set; }

            /// <summary>
            /// topAccount
            /// Optional:
            /// <summary>
            [JsonProperty("topAccount")]
            [XmlElement(ElementName = "topAccount")]
            public string TopAccount { get; set; }

            /// <summary>
            /// customString1
            /// Optional:
            /// <summary>
            [JsonProperty("customString1")]
            [XmlElement(ElementName = "customString1")]
            public string CustomString1 { get; set; }

            /// <summary>
            /// customString2
            /// Optional:
            /// <summary>
            [JsonProperty("customString2")]
            [XmlElement(ElementName = "customString2")]
            public string CustomString2 { get; set; }

            /// <summary>
            /// customString3
            /// Optional:
            /// <summary>
            [JsonProperty("customString3")]
            [XmlElement(ElementName = "customString3")]
            public string CustomString3 { get; set; }

            /// <summary>
            /// customString4
            /// Optional:
            /// <summary>
            [JsonProperty("customString4")]
            [XmlElement(ElementName = "customString4")]
            public string CustomString4 { get; set; }

            /// <summary>
            /// customInteger1
            /// Optional:
            /// <summary>
            [JsonProperty("customInteger1")]
            [XmlElement(ElementName = "customInteger1")]
            public string CustomInteger1 { get; set; }

            /// <summary>
            /// customInteger2
            /// Optional:
            /// <summary>
            [JsonProperty("customInteger2")]
            [XmlElement(ElementName = "customInteger2")]
            public string CustomInteger2 { get; set; }

            /// <summary>
            /// customInteger3
            /// Optional:
            /// <summary>
            [JsonProperty("customInteger3")]
            [XmlElement(ElementName = "customInteger3")]
            public string CustomInteger3 { get; set; }

            /// <summary>
            /// customInteger4
            /// Optional:
            /// <summary>
            [JsonProperty("customInteger4")]
            [XmlElement(ElementName = "customInteger4")]
            public string CustomInteger4 { get; set; }

            /// <summary>
            /// customDate1
            /// Optional:
            /// <summary>
            [JsonProperty("customDate1")]
            [XmlElement(ElementName = "customDate1")]
            public string CustomDate1 { get; set; }

            /// <summary>
            /// customDate2
            /// Optional:
            /// <summary>
            [JsonProperty("customDate2")]
            [XmlElement(ElementName = "customDate2")]
            public string CustomDate2 { get; set; }

            /// <summary>
            /// customDate3
            /// Optional:
            /// <summary>
            [JsonProperty("customDate3")]
            [XmlElement(ElementName = "customDate3")]
            public string CustomDate3 { get; set; }

            /// <summary>
            /// customDate4
            /// Optional:
            /// <summary>
            [JsonProperty("customDate4")]
            [XmlElement(ElementName = "customDate4")]
            public string CustomDate4 { get; set; }

            /// <summary>
            /// customDecimal1
            /// Optional:
            /// <summary>
            [JsonProperty("customDecimal1")]
            [XmlElement(ElementName = "customDecimal1")]
            public string CustomDecimal1 { get; set; }

            /// <summary>
            /// customDecimal2
            /// Optional:
            /// <summary>
            [JsonProperty("customDecimal2")]
            [XmlElement(ElementName = "customDecimal2")]
            public string CustomDecimal2 { get; set; }

            /// <summary>
            /// customDecimal3
            /// Optional:
            /// <summary>
            [JsonProperty("customDecimal3")]
            [XmlElement(ElementName = "customDecimal3")]
            public string CustomDecimal3 { get; set; }

            /// <summary>
            /// customDecimal4
            /// Optional:
            /// <summary>
            [JsonProperty("customDecimal4")]
            [XmlElement(ElementName = "customDecimal4")]
            public string CustomDecimal4 { get; set; }

            /// <summary>
            /// member
            /// Optional:
            /// <summary>
            [JsonProperty("member")]
            [XmlElement(ElementName = "member")]
            public MemberMatchDataMember Member { get; set; }

            /// <summary>
            /// subscriber
            /// Optional:
            /// <summary>
            [JsonProperty("subscriber")]
            [XmlElement(ElementName = "subscriber")]
            public MemberMatchDataMember Subscriber { get; set; }

            /// <summary>
            /// asOfDate
            /// Optional:
            /// <summary>
            [JsonProperty("asOfDate")]
            [XmlElement(ElementName = "asOfDate")]
            public string AsOfDate { get; set; }

        }



        
        /// <summary>
        /// ExternalPremiumPaymentSubscriptionReference
        /// Optional:
        /// <summary>
        public class ExternalPremiumPaymentSubscriptionReference
        {
            /// <summary>
            /// identifier
            /// <summary>
            [JsonProperty("identifier")]
            [XmlElement(ElementName = "identifier")]
            public string Identifier { get; set; }

        }

        /// <summary>
        /// ExternalPremiumPaymentAccountMatchData
        /// Optional:
        /// <summary>
        public class ExternalPremiumPaymentAccountMatchData
        {
            /// <summary>
            /// accountHccIdentifier
            /// Optional:
            /// <summary>
            [JsonProperty("accountHccIdentifier")]
            [XmlElement(ElementName = "accountHccIdentifier")]
            public AccountMatchDataAccountHccIdentifier AccountHccIdentifier { get; set; }

            /// <summary>
            /// accountOtherIdentifier
            /// Zero or more repetitions:
            /// <summary>
            [JsonProperty("accountOtherIdentifier")]
            [XmlElement(ElementName = "accountOtherIdentifier")]
            public List<AccountMatchDataAccountOtherIdentifier> AccountOtherIdentifier { get; set; }

            /// <summary>
            /// asOfDate
            /// Optional:
            /// <summary>
            [JsonProperty("asOfDate")]
            [XmlElement(ElementName = "asOfDate")]
            public string AsOfDate { get; set; }

        }



        /// <summary>
        /// ExternalPremiumPaymentAccountReference
        /// Optional:
        /// <summary>
        public class ExternalPremiumPaymentAccountReference
        {
            /// <summary>
            /// identifier
            /// <summary>
            [JsonProperty("identifier")]
            [XmlElement(ElementName = "identifier")]
            public string Identifier { get; set; }

        }


    }
}

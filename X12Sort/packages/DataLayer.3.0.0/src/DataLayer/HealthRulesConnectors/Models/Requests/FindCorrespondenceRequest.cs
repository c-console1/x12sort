using System;
using DataLayer.StaticUtilities;

namespace DataLayer.HealthRulesConnectors.Models.Requests
{
    public class FindCorrespondenceRequest
    {

        public string Get()
        {
            // Validate at least 1 value must be entered
            if (string.IsNullOrWhiteSpace(maxRows))
                throw new Exception("'maxRows' is not set");

            // Build request
            // * Wrap the raw serialized XML with a temproary or '<junk}' tag so a multiple root XML object errot is not the
            //   thrown on XML clean-up
            // * Run the XML cleanup routine
            // * Remove the temproary or 'junk' tag
            string output = "<junk>" + XMLSerialization.Object2XML(this) + "</junk>"; // Serlize object, add temp wrapper tags
            output = XMLSerialization.RemoveEmptyTags(output); // Remove empty tag
            output = output.Replace("<junk>", "").Replace("</junk>", ""); // remove temp tags

            // * Put the XML output from above into a standard template loaded from the 'resRequests.resx' resource file
            string xmlTemplate = resRequests.FindCorrespondence;
            output = string.Format(xmlTemplate, output);

            // * Return request objects
            return output;
        }


        /// <summary>
        /// Correspondence Search Criteria
        /// </summary>
        public CorrespondenceSearchCriteria correspondenceSearchCriteria { get; set; }

        /// <summary>
        /// Maximium # of rows to return
        /// </summary>
        public string maxRows { get; set; }

        /// <summary>
        /// Correspondence Search Criteria object
        /// </summary>
        public class CorrespondenceSearchCriteria
        {
            /// <summary>
            /// Recipient Type
            /// </summary>
            public string recipientType { get; set; }

            /// <summary>
            /// Recipient Value
            /// </summary>
            public string recipientValue { get; set; }

            /// <summary>
            /// Current Status
            /// </summary>
            public string currentStatus { get; set; }

            /// <summary>
            /// Correspondence Definition
            /// </summary>
            public string correspondenceDefinition { get; set; }

            /// <summary>
            /// Start Date - formatted as 'YYYY-MM-DD'
            /// </summary>
            public string startDate { get; set; }

            /// <summary>
            /// End Date - formatted as 'YYYY-MM-DD'
            /// </summary>
            public string endDate { get; set; }
        }


    }
}

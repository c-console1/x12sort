using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;
using DataLayer.StaticUtilities;

namespace DataLayer.HealthRulesConnectors.Models.Requests
{
    public class PractitionerLookupRequest
    {

        public string Get()
        {
            // Validate fields

            // Validate dates
            AsOfDate = Validation.DateFormat(AsOfDate);
            EndorsementEffectiveDate = Validation.DateFormat(EndorsementEffectiveDate);

            // Validate bool fields
            OnlyPCP = Validation.FormatTrueFalse(OnlyPCP);
            IncludeTerminated = Validation.FormatTrueFalse(IncludeTerminated);

            // Build object
            string output = XMLSerialization.BuildRequestObject(this, resRequests.PractitionerLookup);
            return output;

        }

        /// <summary>
        /// onlyPCP
        /// Optional:
        /// <summary>
        [JsonProperty("onlyPCP")]
        [XmlElement(ElementName = "onlyPCP")]
        public string OnlyPCP { get; set; }

        /// <summary>
        /// asOfDate
        /// Optional:
        /// <summary>
        [JsonProperty("asOfDate")]
        [XmlElement(ElementName = "asOfDate")]
        public string AsOfDate { get; set; }

        /// <summary>
        /// includeTerminated
        /// Optional:
        /// <summary>
        [JsonProperty("includeTerminated")]
        [XmlElement(ElementName = "includeTerminated")]
        public string IncludeTerminated { get; set; }

        /// <summary>
        /// endorsementEffectiveDate
        /// Optional:
        /// <summary>
        [JsonProperty("endorsementEffectiveDate")]
        [XmlElement(ElementName = "endorsementEffectiveDate")]
        public string EndorsementEffectiveDate { get; set; }

        /// <summary>
        /// practitionerReferenceId
        /// Optional:
        /// <summary>
        [JsonProperty("practitionerReferenceId")]
        [XmlElement(ElementName = "practitionerReferenceId")]
        public PractitionerLookupCriteriaPractitionerReferenceId PractitionerReferenceId { get; set; }

        /// <summary>
        /// practitionerId
        /// Optional:
        /// <summary>
        [JsonProperty("practitionerId")]
        [XmlElement(ElementName = "practitionerId")]
        public string PractitionerId { get; set; }

        /// <summary>
        /// practitionerFirstName
        /// Optional:
        /// <summary>
        [JsonProperty("practitionerFirstName")]
        [XmlElement(ElementName = "practitionerFirstName")]
        public string PractitionerFirstName { get; set; }

        /// <summary>
        /// practitionerLastName
        /// Optional:
        /// <summary>
        [JsonProperty("practitionerLastName")]
        [XmlElement(ElementName = "practitionerLastName")]
        public string PractitionerLastName { get; set; }

    }

    /// <summary>
    /// PractitionerLookupCriteriaPractitionerReferenceId
    /// Optional:
    /// <summary>
    public class PractitionerLookupCriteriaPractitionerReferenceId
    {
        /// <summary>
        /// identifier
        /// <summary>
        [JsonProperty("identifier")]
        [XmlElement(ElementName = "identifier")]
        public string Identifier { get; set; }

    }


}

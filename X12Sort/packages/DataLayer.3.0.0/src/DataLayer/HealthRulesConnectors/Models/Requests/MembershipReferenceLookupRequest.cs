using System;
using DataLayer.StaticUtilities;

namespace DataLayer.HealthRulesConnectors.Models.Requests
{
    public class MembershipReferenceLookupRequest
    {
        public string Get()
        {
            // Validate request
            // If blank, set default values
            this.definitionName = string.IsNullOrWhiteSpace(this.definitionName) ?
                "MemberMatchDefinition": this.definitionName;

            // Build request
            return XMLSerialization.BuildRequestObject(this,
                resRequests.MembershipReferenceLookup);
        }

        public string definitionName { get; set; }
        public string id { get; set; }
        public string topAccount { get; set; }
        public string customString1 { get; set; }
        public string customString2 { get; set; }
        public string customString3 { get; set; }
        public string customString4 { get; set; }
        public string customInteger1 { get; set; }
        public string customInteger2 { get; set; }
        public string customInteger3 { get; set; }
        public string customInteger4 { get; set; }
        public string customDate1 { get; set; }
        public string customDate2 { get; set; }
        public string customDate3 { get; set; }
        public string customDate4 { get; set; }
        public string customDecimal1 { get; set; }
        public string customDecimal2 { get; set; }
        public string customDecimal3 { get; set; }
        public string customDecimal4 { get; set; }
        public Member member { get; set; }
        public Subscriber subscriber { get; set; }
        public string asOfDate { get; set; }

        public class Member
        {
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string fullName { get; set; }
            public string gender { get; set; }
            public string dateOfBirth { get; set; }
            public string taxId { get; set; }
            public Address address { get; set; }
        }

        public class Subscriber
        {
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string fullName { get; set; }
            public string gender { get; set; }
            public string dateOfBirth { get; set; }
            public string taxId { get; set; }
            public Address address { get; set; }
        }

        public class Address
        {
            public Address address { get; set; }
            public string cityName { get; set; }
            public string stateCode { get; set; }
            public string zipCode { get; set; }
            public string zipExtensionCode { get; set; }
            public string countryCode { get; set; }
        }

    }
}

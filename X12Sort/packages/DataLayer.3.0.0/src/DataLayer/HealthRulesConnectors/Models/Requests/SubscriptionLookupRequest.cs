using System;
using DataLayer.StaticUtilities;

namespace DataLayer.HealthRulesConnectors.Models.Requests
{
    public class SubscriptionLookupRequest
    {
        public string Get()
        {
            // Validate at least 1 value must be entered
            if ( string.IsNullOrWhiteSpace(definitionName) )
                throw new Exception("'definitionName' is not set");

            // Build request
            // * Wrap the raw serialized XML with a temproary or '<junk}' tag so a multiple root XML object errot is not the
            //   thrown on XML clean-up
            // * Run the XML cleanup routine
            // * Remove the temproary or 'junk' tag 
            string output = "<junk>" + XMLSerialization.Object2XML(this) + "</junk>"; // Serlize object, add temp wrapper tags
            output = XMLSerialization.RemoveEmptyTags(output); // Remove empty tag
            output = output.Replace("<junk>", "").Replace("</junk>", ""); // remove temp tags

            // * Put the XML output from above into a standard template loaded from the 'resRequests.resx' resource file
            string xmlTemplate = resRequests.SubscriptionLookup; // Load template
            output = string.Format(xmlTemplate, output); // Merge generated XML into template

            // * Return request objects
            return output;
        }

        /// <summary>
        /// Search definition name
        /// </summary>
        public string definitionName { get; set; }
        public string id { get; set; }
        public string topAccount { get; set; }
        public string customString1 { get; set; }
        public string customString2 { get; set; }
        public string customString3 { get; set; }
        public string customString4 { get; set; }
        public string customInteger1 { get; set; }
        public string customInteger2 { get; set; }
        public string customInteger3 { get; set; }
        public string customInteger4 { get; set; }
        public string customDate1 { get; set; }
        public string customDate2 { get; set; }
        public string customDate3 { get; set; }
        public string customDate4 { get; set; }
        public string customDecimal1 { get; set; }
        public string customDecimal2 { get; set; }
        public string customDecimal3 { get; set; }
        public string customDecimal4 { get; set; }
        public Member member { get; set; }
        public Subscriber subscriber { get; set; }
        public string asOfDate { get; set; }

    }

    /// <summary>
    /// Member object
    /// </summary>
    public class Member
    {
        /// <summary>
        /// First name
        /// </summary>
        public string firstName { get; set; }

        /// <summary>
        /// Last name
        /// </summary>
        public string lastName { get; set; }

        /// <summary>
        /// Full name
        /// </summary>
        public string fullName { get; set; }

        /// <summary>
        /// 1 leter gender code
        /// </summary>
        public string gender { get; set; }

        /// <summary>
        /// Date of Birth - format as 'YYYY-MM-DD'
        /// </summary>
        public string dateOfBirth { get; set; }

        /// <summary>
        /// Tax ID
        /// </summary>
        public string taxId { get; set; }

        /// <summary>
        /// Subscriber address
        /// </summary>
        public Address address { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class Subscriber
    {
        /// <summary>
        /// First name
        /// </summary>
        public string firstName { get; set; }

        /// <summary>
        /// Last name
        /// </summary>
        public string lastName { get; set; }

        /// <summary>
        /// Full name
        /// </summary>
        public string fullName { get; set; }

        /// <summary>
        /// 1 leter gender code
        /// </summary>
        public string gender { get; set; }

        /// <summary>
        /// Date of Birth - format as 'YYYY-MM-DD'
        /// </summary>
        public string dateOfBirth { get; set; }

        /// <summary>
        /// Tax ID
        /// </summary>
        public string taxId { get; set; }

        /// <summary>
        /// Subscriber address
        /// </summary>
        public Address address { get; set; }
    }

    /// <summary>
    /// Address object
    /// </summary>
    public class Address
    {
        /// <summary>
        /// Street address 
        /// </summary>
        public string address { get; set; }

        /// <summary>
        /// City name
        /// </summary>
        public string cityName { get; set; }

        /// <summary>
        /// 2 character state code
        /// </summary>
        public string stateCode { get; set; }

        /// <summary>
        /// 5-6 zip code
        /// </summary>
        public string zipCode { get; set; }

        /// <summary>
        /// 4 character zip extension
        /// </summary>
        public string zipExtensionCode { get; set; }

        /// <summary>
        /// Two letter country code
        /// </summary>
        public string countryCode { get; set; }
    }


}

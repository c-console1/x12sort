using System;
using DataLayer.StaticUtilities;

namespace DataLayer.HealthRulesConnectors.Models.Requests
{
    public class FindMemberRequest
    {
        /// <summary>
        /// Generate the serialized response object
        /// </summary>
        /// <returns></returns>
        public string Get()
        {
            // Validate at least 1 value must be entered
            if (
                string.IsNullOrWhiteSpace(criteria.benefitPlanName)
                && string.IsNullOrWhiteSpace(criteria.hccBenefitPlanID)
                && string.IsNullOrWhiteSpace(criteria.benefitPlanType)
                && string.IsNullOrWhiteSpace(criteria.asOfDate)
                )
                throw new Exception("At least 1 value must be entered");

            // Serialize response and send
            return XMLSerialization.BuildRequestObject(this, resRequests.FindMembers);
        }

        /// <summary>
        /// Criteria object
        /// </summary>
        public Criteria criteria = new Criteria();

        /// <summary>
        /// Criteria values
        /// </summary>
        public class Criteria
        {
            /// <summary>
            /// Benefit Plan Name
            /// </summary>
            public string benefitPlanName { get; set; }

            /// <summary>
            /// HealthRules Benefit Plan ID
            /// </summary>
            public string hccBenefitPlanID { get; set; }

            /// <summary>
            /// HealthRules Benefit Plan Type
            /// </summary>
            public string benefitPlanType { get; set; }

            /// <summary>
            /// As of Date - formatted as 'YYYY-MM-DD'
            /// </summary>
            public string asOfDate { get; set; }
        }

    }
}

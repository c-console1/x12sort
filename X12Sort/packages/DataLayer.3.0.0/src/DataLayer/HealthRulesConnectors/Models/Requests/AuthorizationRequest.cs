using System;
using System.Collections.Generic;
using System.Text;
using DataLayer.StaticUtilities;

namespace DataLayer.HealthRulesConnectors.Models.Requests
{
    /// <summary>
    /// Authorization Request
    /// </summary>
    public class AuthorizationRequest
    {

        /// <summary>
        /// Generate the serialized response object
        /// </summary>
        /// <returns></returns>
        public string Get()
        {
            // Serialize response and send
            return XMLSerialization.BuildRequestObject(this, resRequests.Authorization);
        }

        /// <summary>
        /// Authorization Internal ID
        /// </summary>
        public string authorizationInternalId { get; set; }

        /// <summary>
        /// Record Type
        /// </summary>
        public string recordType { get; set; }

        /// <summary>
        /// Authorization ID
        /// </summary>
        public string authorizationId { get; set; }

        /// <summary>
        /// External Authorization ID
        /// </summary>
        public string externalAuthorizationId { get; set; }

        /// <summary>
        /// Authorization Status
        /// </summary>
        public string authorizationStatus { get; set; }

        /// <summary>
        /// List of Trace Information items
        /// </summary>
        public List<TraceInfo> traceInfo { get; set; }

        /// <summary>
        /// Maintenance Information Details
        /// </summary>
        public MaintenanceInfo maintenanceInfo { get; set; }

        /// <summary>
        /// Input Source
        /// </summary>
        public string inputSource { get; set; }

        /// <summary>
        /// Request Date - Formatted as 'YYYY-MM-DD'
        /// </summary>
        public string requestDate { get; set; }

        /// <summary>
        /// Received Date - Formatted as 'YYYY-MM-DD'
        /// </summary>
        public string receivedDate { get; set; }

        /// <summary>
        /// RequestedAdmitStatusCode
        /// </summary>
        public string requestedAdmitStatusCode { get; set; }

        /// <summary>
        /// Internal patient tracking #
        /// </summary>
        public string patientTrackingNumber { get; set; }

        /// <summary>
        /// External authorization batch #
        /// </summary>
        public string externalAuthBatchNumber { get; set; }

        /// <summary>
        /// Performing provider
        /// </summary>
        public PerformingProvider performingProvider { get; set; }

        public RequestingProvider requestingProvider { get; set; }
        public AdmissionInformation admissionInformation { get; set; }
        public string servicesAction { get; set; }
        public List<Services> services { get; set; }
        public List<Diagnosis> diagnosis { get; set; }
        public string acuity { get; set; }
        public string authSeverity { get; set; }
        public IDCode typeOfBill { get; set; }
        public string placeOfService { get; set; }
        public string releaseOfInformation { get; set; }
        public string memberIdentifier { get; set; }
        public string memberSystemId { get; set; }
        public MemberMatchInput memberMatchInput { get; set; }
        public AuthorizationMatchInput authorizationMatchInput { get; set; }
        public string mdcCode { get; set; }
        public List<ClinicalNotes> clinicalNotes { get; set; }
        public string udtAction { get; set; }
        public IDCode userDefinedTerm { get; set; }
        public List<Udt> udt { get; set; }
        public List<AuthorizationAttachments> authorizationAttachments { get; set; }
        public string allowWorkbasketAmend { get; set; }

        public class MemberMatchInput {

            public string insurancePlanId { get; set; }

            public string c4cId { get; set; }

            public MemberMatch memberMatchInput { get; set;  }

        }

        /// <summary>
        /// Individual authorization tracking elements
        /// </summary>
        public class TraceInfo
        {
            /// <summary>
            /// Entity ID
            /// </summary>
            public string traceEntityId { get; set; }

            /// <summary>
            /// Additional ID's
            /// </summary>
            public string traceAdditionalId { get; set; }
        }

        /// <summary>
        /// Maintenance infoformation
        /// </summary>
        public class MaintenanceInfo
        {
            /// <summary>
            /// Transaction type
            /// </summary>
            public string transactionType { get; set; }

            /// <summary>
            /// Override action - override standard authorization actions
            /// </summary>
            public string overrideAction { get; set; }

            /// <summary>
            /// Results of the transaction
            /// </summary>
            public string overrideResult { get; set; }
        }

        /// <summary>
        /// Performing provider object
        /// </summary>
        public class PerformingProvider
        {
            /// <summary>
            /// Provider type
            /// </summary>
            public string providerType { get; set; }

            /// <summary>
            /// Provider system ID
            /// </summary>
            public string providerSystemId { get; set; }

            /// <summary>
            /// Provider plan ID
            /// </summary>
            public string providerPlanId { get; set; }

            /// <summary>
            /// Provider NPI ID
            /// </summary>
            public string providerNPIId { get; set; }

            /// <summary>
            /// Provider TIN ID
            /// </summary>
            public string providerTINId { get; set; }

            /// <summary>
            /// Provider identifier
            /// </summary>
            public string providerIdentifier { get; set; }

            /// <summary>
            /// Provider taxonomy code
            /// </summary>
            public string providerTaxonomyCode { get; set; }

            /// <summary>
            /// Organization name
            /// </summary>
            public string organizationName { get; set; }

            /// <summary>
            /// Individual name
            /// </summary>
            public IndividualName individualName { get; set; }

            /// <summary>
            /// Detailed address
            /// </summary>
            public AddressLong address { get; set; }

            /// <summary>
            /// Performing provider's phone numbers
            /// </summary>
            public List<TelephoneNumber> phoneNumbers { get; set; }
        }

        /// <summary>
        /// Performing provider
        /// </summary>
        public class RequestingProvider
        {
            /// <summary>
            /// Provuder type
            /// </summary>
            public string providerType { get; set; }

            /// <summary>
            /// Provider system ID
            /// </summary>
            public string providerSystemId { get; set; }

            /// <summary>
            /// Provider plan ID
            /// </summary>
            public string providerPlanId { get; set; }

            /// <summary>
            /// Provider NPI ID
            /// </summary>
            public string providerNPIId { get; set; }

            /// <summary>
            /// Provider TIN ID
            /// </summary>
            public string providerTINId { get; set; }

            /// <summary>
            /// Provider identifier
            /// </summary>
            public string providerIdentifier { get; set; }

            /// <summary>
            /// Provider taxonomy code
            /// </summary>
            public string providerTaxonomyCode { get; set; }

            /// <summary>
            /// Organization Name
            /// </summary>
            public string organizationName { get; set; }

            /// <summary>
            /// Individual name
            /// </summary>
            public IndividualName individualName { get; set; }

            /// <summary>
            /// Provider long adress
            /// </summary>
            public AddressLong address { get; set; }

            /// <summary>
            /// Provider's phone #'s
            /// </summary>
            public List<TelephoneNumber> phoneNumbers { get; set; }
        }

        public class AdmissionInformation
        {
            public string expectedAdmissionDateRangeStart { get; set; }
            public string expectedAdmissionDateRangeEnd { get; set; }
            public string actualAdmissionDate { get; set; }
            public string expectedDischargeDateRangeStart { get; set; }
            public string expectedDischargeDateRangeEnd { get; set; }
            public string actualDischargeDate { get; set; }
            public string admissionSource { get; set; }
            public string admissionType { get; set; }
            public IDCode dischargeStatus { get; set; }
        }

        public class Services
        {
            public string serviceSetIdentifier { get; set; }
            public string externalServiceSetId { get; set; }
            public string startServiceDate { get; set; }
            public string endServiceDate { get; set; }
            public string requestedStartServiceDate { get; set; }
            public string requestedEndServiceDate { get; set; }
            public string receivedDate { get; set; }
            public string clinicalReceiptDate { get; set; }
            public string serviceSetStatus { get; set; }
            public string note { get; set; }
            public string approvedQuantity { get; set; }
            public string approvedUnits { get; set; }
            public string approvedAmount { get; set; }
            public string requestedQuantity { get; set; }
            public string requestedUnits { get; set; }
            public RenderingProvider renderingProvider { get; set; }
            public ServiceType serviceType { get; set; }
            public List<Diagnosis> diagnosis { get; set; }
            public ServiceAgreementDetail serviceAgreementDetail { get; set; }
            public HicApproval hicApproval { get; set; }
            public string authorizedStatusReasonCode { get; set; }
            public string authorizationTypeCode { get; set; }
            public string authorizationPriority { get; set; }
            public TypeCode reviewType { get; set; }
            public TypeCode reviewLevel { get; set; }
            public AuthorizationServiceSetMatchInput authorizationServiceSetMatchInput { get; set; }
        }

        public class RenderingProvider
        {
            public string providerType { get; set; }
            public string providerSystemId { get; set; }
            public string providerPlanId { get; set; }
            public string providerNPIId { get; set; }
            public string providerTINId { get; set; }
            public string providerIdentifier { get; set; }
            public string providerTaxonomyCode { get; set; }
            public string organizationName { get; set; }
            public IndividualName individualName { get; set; }
            public AddressLong address { get; set; }
            public List<TelephoneNumber> phoneNumbers { get; set; }
        }

        public class ServiceType
        {
            public string serviceSystemId { get; set; }
            public string serviceCode { get; set; }
            public string serviceCodeType { get; set; }
            public List<string> modifierCode { get; set; }
            public string placeOfService { get; set; }
            public string referralCategoryType { get; set; }
            public string serviceType { get; set; }
        }

        public class Diagnosis
        {
            public string diagnosisSystemId { get; set; }
            public string diagnosisType { get; set; }
            public string diagnosisCode { get; set; }
            public string diagnosisCodeType { get; set; }
        }

        public class ServiceAgreementDetail
        {
            public string clearAgreementDetail { get; set; }
            public string overrideExclusions { get; set; }
            public string promoteToInNetwork { get; set; }
            public string payAtMaximum { get; set; }
            public string extendLimit { get; set; }
            public string coverTerminatedMember { get; set; }
            public string reimbursementMethod { get; set; }
            public string amount { get; set; }
            public string typeOfFlatFee { get; set; }
            public string percentageBilled { get; set; }
        }

        public class HicApproval
        {
            public string authorizedBySystemUser { get; set; }
            public string authorizedDate { get; set; }
            public DataLayer.HealthRulesConnectors.TypeCode authorizedByExternalUser { get; set; }
        }



        public class AuthorizationServiceSetMatchInput
        {
            public string authorizationId { get; set; }
            public string externalServiceSetId { get; set; }
            public string serviceStartDate { get; set; }
            public string serviceEndDate { get; set; }
            public string serviceCode { get; set; }
            public string serviceCodeType { get; set; }
            public string referralCategoryType { get; set; }
            public string serviceSetStatus { get; set; }
        }


        public class AuthorizationMatchInput
        {
            public string authorizationId { get; set; }
            public string externalAuthorizationId { get; set; }
        }

        public class ClinicalNotes
        {
            public string noteID { get; set; }
            public string noteContent { get; set; }
            public string reviewerFirstName { get; set; }
            public string reviewerLastName { get; set; }
            public List<NoteAttachments> noteAttachments { get; set; }
        }

        public class NoteAttachments
        {
            public string externalReferenceId { get; set; }
            public string attachmentContent { get; set; }
            public string attachmentName { get; set; }
            public string attachmentDescription { get; set; }
            public string attachmentSize { get; set; }
            public string attachmentType { get; set; }
        }

        public class Udt
        {
            public string attributeRoleName { get; set; }
            public string attrValueAsString { get; set; }
        }

        public class AuthorizationAttachments
        {
            public string externalReferenceId { get; set; }
            public string attachmentContent { get; set; }
            public string attachmentName { get; set; }
            public string attachmentDescription { get; set; }
            public string attachmentSize { get; set; }
            public string attachmentType { get; set; }
        }

    }
}

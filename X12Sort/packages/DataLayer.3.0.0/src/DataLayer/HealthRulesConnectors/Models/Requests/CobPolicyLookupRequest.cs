using System;
using DataLayer.StaticUtilities;

namespace DataLayer.HealthRulesConnectors.Models.Requests
{
    public class CobPolicyLookupRequest
    {

        public string Get()
        {
            // Validation
            if (string.IsNullOrWhiteSpace(memberHccId) && string.IsNullOrWhiteSpace(asOfDate))
                throw new Exception("You must fill in either the 'memberHccId' value or 'asOfDate' value");

            // Build request
            // * Wrap the raw serialized XML with a temproary or '<junk}' tag so a multiple root XML object errot is not the
            //   thrown on XML clean-up
            // * Run the XML cleanup routine
            // * Remove the temproary or 'junk' tag         
            string output = "<junk>" + XMLSerialization.Object2XML(this) + "</junk>"; // Serlize object, add temp wrapper tags
            output = XMLSerialization.RemoveEmptyTags(output); // Remove empty tag
            output = output.Replace("<junk>", "").Replace("</junk>", ""); // remove temp tags

            // * Put the XML output from above into a standard template loaded from the 'resRequests.resx' resource file
            output = string.Format(resRequests.CobPolicyLookup, output);

            // * Return request objects
            return output;

        }
        /// <summary>
        /// Member HealthRules Internal ID
        /// </summary>
        public string memberHccId { get; set; }

        /// <summary>
        /// As of Date - Formatted as 'YYYY-MM-DD'
        /// </summary>
        public string asOfDate { get; set; }

    }
}

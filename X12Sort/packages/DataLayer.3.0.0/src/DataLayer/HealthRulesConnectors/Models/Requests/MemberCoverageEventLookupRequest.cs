using System;
using System.Collections.Generic;
using DataLayer.StaticUtilities;

namespace DataLayer.HealthRulesConnectors.Models.Requests
{
    public class MemberCoverageEventLookupRequest
    {

        public string Get()
        {
            // Validate Start Date
            if (string.IsNullOrWhiteSpace(startDate))
                throw new Exception("Field 'Start Date' must be defined");

            // Validate Event Type
            if (eventsTypeList.Count == 0)
                throw new Exception("You must enter at least 1 Event Type");

            // Validate Reason codes
            for (int i = 0; i < reasonCodeList.Count; i++)
            {
                if (string.IsNullOrWhiteSpace(reasonCodeList[i].messageCode))
                    throw new Exception( string.Format("Reason Code #{0} is missing the Message Code", i) );

                if (string.IsNullOrWhiteSpace(reasonCodeList[i].codeDomainName))
                    throw new Exception(string.Format("Reason Code #{0} is missing the Code Domain Name", i));

                if (string.IsNullOrWhiteSpace(reasonCodeList[i].messageDescription))
                    throw new Exception(string.Format("Reason Code #{0} is missing the Message Description", i));
            }

            // Serialize response and send
            return XMLSerialization.BuildRequestObject(this, resRequests.MemberCoverageEventLookup);

        }

        /// <summary>
        /// Start Date - Formatted as 'YYYY-MM-DD'
        /// </summary>
        public string startDate { get; set; }

        /// <summary>
        /// Start Time - Formatted as 'HH:MM:SS'
        /// </summary>
        public string startTime { get; set; }


        /// <summary>
        /// End Date - Formatted as 'YYYY-MM-DD'
        /// </summary>
        public string endDate { get; set; }

        /// <summary>
        /// Start Time - Formatted as 'HH:MM:SS'
        /// </summary>
        public string endTime { get; set; }

        /// <summary>
        /// Exchange State - 2 letter code
        /// Based on: http://100.112.32.164:9191/connector/services/v4/MemberCoverageEventLookup?xsd=us-state-code.xsd
        /// </summary>
        public string exchangeState { get; set; }
        public List<string> eventsTypeList = new List<string>();
        public List<ReasonCodeList> reasonCodeList = new List<ReasonCodeList>();

        public class ReasonCodeList
        {
            public string messageCode { get; set; }
            public string codeDomainName { get; set; }
            public string messageDescription { get; set; }
            public string policyName { get; set; }
            public string messageAction { get; set; }
            public string messageResponsibility { get; set; }
        }

    }
}

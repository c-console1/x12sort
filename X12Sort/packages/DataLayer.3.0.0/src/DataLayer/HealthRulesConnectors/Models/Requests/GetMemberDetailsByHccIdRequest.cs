using System;
using DataLayer.StaticUtilities;

namespace DataLayer.HealthRulesConnectors.Models.Requests
{
    public class GetMemberDetailsByHccIdRequest
    {

        public string Get()
        {
            // Validate at least 1 value must be entered
            if (string.IsNullOrWhiteSpace(memberHccId))
                throw new Exception("Enter Member HCC ID");

            // Validate values in the 'memberDetailFlags' object are true or false
            memberDetailFlags.includeAccount = Validation.FormatTrueFalse(memberDetailFlags.includeAccount, "false");
            memberDetailFlags.includeCob = Validation.FormatTrueFalse(memberDetailFlags.includeCob, "false");
            memberDetailFlags.includeHeadOfHousehold = Validation.FormatTrueFalse(memberDetailFlags.includeHeadOfHousehold, "false");
            memberDetailFlags.includeParentAccount = Validation.FormatTrueFalse(memberDetailFlags.includeParentAccount, "false");
            memberDetailFlags.includePlan = Validation.FormatTrueFalse(memberDetailFlags.includePlan, "false");
            memberDetailFlags.includeProvider = Validation.FormatTrueFalse(memberDetailFlags.includeProvider, "false");
            memberDetailFlags.includeProviderPhone = Validation.FormatTrueFalse(memberDetailFlags.includeProviderPhone, "false");
            memberDetailFlags.includeRider = Validation.FormatTrueFalse(memberDetailFlags.includeRider, "false");
            memberDetailFlags.includeSubAccount = Validation.FormatTrueFalse(memberDetailFlags.includeSubAccount, "false");

            // If all memberDetailFlags are 'false' make them all 'true' to include everything
            if(memberDetailFlags.includeAccount == "false"
                && memberDetailFlags.includeCob == "false"
                && memberDetailFlags.includeHeadOfHousehold == "false"
                && memberDetailFlags.includeParentAccount == "false"
                && memberDetailFlags.includePlan == "false"
                && memberDetailFlags.includeProvider == "false"
                && memberDetailFlags.includeProviderPhone == "false"
                && memberDetailFlags.includeRider == "false"
                && memberDetailFlags.includeSubAccount == "false"
                )
            {
                memberDetailFlags.includeAccount = "true";
                memberDetailFlags.includeCob = "true";
                memberDetailFlags.includeHeadOfHousehold = "true";
                memberDetailFlags.includeParentAccount = "true";
                memberDetailFlags.includePlan = "true";
                memberDetailFlags.includeProvider = "true";
                memberDetailFlags.includeProviderPhone = "true";
                memberDetailFlags.includeRider = "true";
                memberDetailFlags.includeSubAccount = "true";
            }

            // Serialize response and send
            return XMLSerialization.BuildRequestObject(this, resRequests.GetMemberDetailsByHccId);

        }

        /// <summary>
        /// HealthRules Member ID
        /// </summary>
        public string memberHccId { get; set; }

        /// <summary>
        /// Member Detail Flags
        /// </summary>
        public MemberDetailFlags memberDetailFlags = new MemberDetailFlags();


        /// <summary>
        /// Member Detail Flags Values
        /// </summary>
        public class MemberDetailFlags
        {
            /// <summary>
            /// Include Account information
            /// </summary>
            public string includeAccount { get; set; }

            /// <summary>
            /// Include COB information
            /// </summary>
            public string includeCob { get; set; }

            /// <summary>
            /// Include Head of Household information
            /// </summary>
            public string includeHeadOfHousehold { get; set; }

            /// <summary>
            /// Include Parent Account information
            /// </summary>
            public string includeParentAccount { get; set; }

            /// <summary>
            /// Include Plan information
            /// </summary>
            public string includePlan { get; set; }

            /// <summary>
            /// Include Provider information
            /// </summary>
            public string includeProvider { get; set; }

            /// <summary>
            /// Include Account information
            /// </summary>
            public string includeProviderPhone { get; set; }

            /// <summary>
            /// Include Rider information
            /// </summary>
            public string includeRider { get; set; }

            /// <summary>
            /// Include Sub-Account information
            /// </summary>
            public string includeSubAccount { get; set; }
        }
    }
}

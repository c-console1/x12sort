using System;
using System.Collections.Generic;
using DataLayer.StaticUtilities;

namespace DataLayer.HealthRulesConnectors.Models.Requests
{
    public class CobPolicyListRequest
    {
        /// <summary>
        /// XML version of thw request object
        /// </summary>
        /// <param name="bypassValidation"></param>
        /// <returns>XML version of thw request object</returns>
        public string Get(bool bypassValidation = false)
        {
            if (bypassValidation)
            {

                // Validate at least 1 value must be entered
                memberHccID = Validation.IsEmptyString(memberHccID, "'memberHccID' is not set");

                // At l policy must be added
                if (cobPolicy.Count < 0)
                    throw new Exception("At least 1 'cobPolicy' object must be set");

                // Loop through each policy
                foreach (CobPolicy item in cobPolicy)
                {
                    // Validate the leadSourceForOtherInsuranceCode type code is set if instatiated
                    if (!Validation.ValidValueSet(item.leadSourceForOtherInsuranceCode, true))
                        throw new Exception("'leadSourceForOtherInsuranceCode' must have either have the 'codeEntry' or 'shortName' filled");

                    // Validate the benefitPlanTypeCode type code is set if instatiated
                    if (!Validation.ValidValueSet(item.benefitPlanTypeCode, true))
                        throw new Exception("'benefitPlanTypeCode' must have either have the 'codeEntry' or 'shortName' filled");

                    // There has to be at least 1 date range
                    if (item.dateRanges.Count == 0)
                        throw new Exception("At least 1 'dateRanges' object must be set for the 'cobPolicy'");

                    // Validate each date range
                    foreach (DateRanges dateRange in item.dateRanges)
                    {
                        // Start date validation
                        try
                        {
                            dateRange.startDate = Validation.ValidateDate(dateRange.startDate, false);
                        }
                        catch (Exception)
                        {
                            throw new Exception($"The 'startDate' value of '{dateRange.startDate}' in 'dateRanges' is invalid");
                        }

                        // End date validation
                        try
                        {
                            dateRange.endDate = Validation.ValidateDate(dateRange.endDate, false);
                        }
                        catch (Exception)
                        {
                            throw new Exception($"The 'endDate' value of '{dateRange.endDate}' in 'dateRanges' is invalid");
                        }

                        // If the OBDReasonCode has been instiated does have the required values
                        if (!Validation.ValidValueSet(dateRange.OBDReasonCode, true))
                            throw new Exception("'dateRange.OBDReasonCode' must have either have the 'codeEntry' or 'shortName' filled");

                        // responsibilitySequenceCode can only be a few a values
                        if (!string.IsNullOrWhiteSpace(dateRange.responsibilitySequenceCode))
                        {
                            string validateResponsibilitySequenceCode = Validation.MatchEnumeratedType(
                                "ResponsibilitySequenceType", dateRange.responsibilitySequenceCode);

                            if (validateResponsibilitySequenceCode == null)
                                throw new Exception("'dateRange.responsibilitySequenceCode' must "
                                    + Validation.GetEnumeratedTypes("ResponsibilitySequenceType"));

                            dateRange.responsibilitySequenceCode = validateResponsibilitySequenceCode;
                        }

                    }

                    // id the subscriberIdList has been instantiated
                    if (item.subscriberIdList != null)
                    {
                        // Look at all the Subscriber
                        foreach (SubscriberIdList Subscriber in item.subscriberIdList)
                        {
                            // if the identificationTypeCode has been instaniated validae it has the required fields
                            if (!Validation.ValidValueSet(Subscriber.identificationTypeCode, true))
                                throw new Exception("'SubscriberIdList.identificationTypeCode' must have either have the 'codeEntry' or 'shortName' filled in");
                        }

                    }

                    // If the subscriber's name has been instantiated look for required fields
                    if (item.subscriberName != null)
                    {
                        // Last name is the only required field
                        if (string.IsNullOrWhiteSpace(item.subscriberName.lastName))
                            throw new Exception("'cobPolicy.subscriberName.lastName' must be filled in");


                        // If the subscriber's name credentialSuffixList has been instantiated look for required fields
                        if (item.subscriberName.credentialSuffixList != null)
                        {
                            if (item.subscriberName.credentialSuffixList.CredentialSuffix != null)
                            {
                                foreach (TypeCode cred in item.subscriberName.credentialSuffixList.CredentialSuffix)
                                {

                                    if (!Validation.ValidValueSet(cred, true))
                                        throw new Exception("subscriberName.credentialSuffixList.CredentialSuffix' must have either have the 'codeEntry' or 'shortName' filled in");
                                }
                            }

                        }

                    }
                
                // Format the Subscriber's DOB if it has been set
                if (!string.IsNullOrWhiteSpace(item.subscriberDOB))
                    item.subscriberDOB = DateTime.Parse(item.subscriberDOB).ToString("yyyy-MM-dd");

                    // if item.correspondenceInfo has been instatiate, check for required fields
                    if (item.correspondenceInfo != null)
                    {
                        if (item.correspondenceInfo.addressInfo != null)
                        {
                            if (item.correspondenceInfo.addressInfo.addressPhoneList != null)
                            {
                                foreach (TelephoneNumber phoneList in item.correspondenceInfo.addressInfo.addressPhoneList)
                                {
                                    if (!Validation.ValidValueSet(phoneList.individualPhoneTypeCode, true))
                                        throw new Exception("correspondenceInfo.addressInfo.addressPhoneList.individualPhoneTypeCode' must have either have the 'codeEntry' or 'shortName' filled in");
                                    if (!Validation.ValidValueSet(phoneList.organizationPhoneTypeCode, true))
                                        throw new Exception("correspondenceInfo.addressInfo.addressPhoneList.individualPhoneTypeCode' must have either have the 'codeEntry' or 'shortName' filled in");
                                }
                            }
                        }

                        if (!Validation.ValidValueSet(item.correspondenceInfo.supplierCorrespondenceUsageCode, true))
                            throw new Exception("'correspondenceInfo.supplierCorrespondenceUsageCode' must have either have the 'codeEntry' or 'shortName' filled");

                        if (!Validation.ValidValueSet(item.correspondenceInfo.supplierLocationCorrespondenceUsageCode, true))
                            throw new Exception("'correspondenceInfo.supplierLocationCorrespondenceUsageCode' must have either have the 'codeEntry' or 'shortName' filled");

                        if (!Validation.ValidValueSet(item.correspondenceInfo.accountCorrespondenceUsageCode, true))
                            throw new Exception("'correspondenceInfo.accountCorrespondenceUsageCode' must have either have the 'codeEntry' or 'shortName' filled");

                        if (item.carrierIds != null)
                        {
                            foreach (CarrierIds carID in item.carrierIds)
                            {
                                if (!Validation.ValidValueSet(carID.identificationTypeCode, true))
                                    throw new Exception("'carrierIds.identificationTypeCode' must have either have the 'codeEntry' or 'shortName' filled");

                            }
                        }
                    }
                }
            }
                    
            // There is a glitch in the Connector's parser.  If certain tags render out of order,
            // even though it's valid XML, the parser will throw an error.  This is how to get around
            // this issue.

            // Step 1. Generate the output
            string output = XMLSerialization.BuildRequestObject(this, resRequests.CobPolicy);

            // Step 2. Remove the tag that will cause the issue
            string tag2Move = "<memberHccID>" + this.memberHccID + "</memberHccID>";
            output = output.Replace(tag2Move, "");

            // Step 3. Restore the tag to a location that the Connector Parser will be happy with.
            // It wants the member # tag at the top
            output = output.Replace("<cob:cobPolicyList>", "<cob:cobPolicyList>\r\n" + tag2Move);

            // Step 4. Return the clean Request object
            return output;
        }

        /// <summary>
        /// HealthrRules member ID
        /// </summary>
        public string memberHccID { get; set; }

        /// <summary>
        /// List of policies to add
        /// </summary>
        public List<CobPolicy> cobPolicy = new List<CobPolicy>();

        /// <summary>
        /// Individual COB policy
        /// </summary>
        public class CobPolicy
        {
            public string cobPolicyIdentifier { get; set; }
            public string cobPolicyId { get; set; }
            public TypeCode leadSourceForOtherInsuranceCode { get; set; }
            public IDCode relationshipToSubscriberDefinitionReference { get; set; }
            public string otherInsuranceCompanyName { get; set; }
            public string policyType { get; set; }
            public string benefitPlanName { get; set; }
            public string groupNumber { get; set; }
            public string memberId { get; set; }
            public TypeCode benefitPlanTypeCode { get; set; }
            public List<DateRanges> dateRanges { get; set; }
            public List<SubscriberIdList> subscriberIdList { get; set; }
            public IndividualName subscriberName { get; set; }
            public string subscriberDOB { get; set; }
            public string subscriberGender { get; set; }
            public RxInfo rxInfo { get; set; }
            public CorrespondenceInfo correspondenceInfo { get; set; }
            public List<CarrierIds> carrierIds { get; set; }
        }

        /// <summary>
        /// Date ranges
        /// </summary>
        public class DateRanges
        {
            /// <summary>
            /// Start Date
            /// </summary>
            public string startDate { get; set; }

            /// <summary>
            /// End Date
            /// </summary>
            public string endDate { get; set; }

            /// <summary>
            /// Responsibility Sequence Code
            /// </summary>
            public string responsibilitySequenceCode { get; set; }

            /// <summary>
            /// OBD Reason Code
            /// </summary>
            public TypeCode OBDReasonCode { get; set; }
        }

        /// <summary>
        /// Subscriber information
        /// </summary>
        public class SubscriberIdList
        {
            public string identificationNumber { get; set; }
            public string issuingState { get; set; }
            public CountryCode issuingCountry { get; set; }
            public string effectiveStartDate { get; set; }
            public string effectiveEndDate { get; set; }
            public TypeCode identificationTypeCode { get; set; }
        }

        /// <summary>
        /// Perscription information
        /// </summary>
        public class RxInfo
        {
            public string rxBin { get; set; }
            public string pcn { get; set; }
            public string rxGroup { get; set; }
            public string rxId { get; set; }
            public string rxPhoneNumber { get; set; }
        }

        public class CorrespondenceInfo
        {
            public string emailAddress { get; set; }
            public AddressInfo addressInfo { get; set; }
            public TypeCode supplierCorrespondenceUsageCode { get; set; }
            public TypeCode supplierLocationCorrespondenceUsageCode { get; set; }
            public TypeCode accountCorrespondenceUsageCode { get; set; }
        }


        /// <summary>
        /// Address Information - this is unique to this Connector
        /// </summary>
        public class AddressInfo
        {
            public AddressLong postalAddress { get; set; }

            public List<TelephoneNumber> addressPhoneList { get; set; }
        }

        public class CarrierIds
        {
            public string identificationNumber { get; set; }
            public string issuingState { get; set; }
            public CountryCode issuingCountry { get; set; }
            public string effectiveStartDate { get; set; }
            public string effectiveEndDate { get; set; }
            public TypeCode identificationTypeCode { get; set; }
        }

    }



}

using System;
using System.Collections.Generic;
using DataLayer.StaticUtilities;


namespace DataLayer.HealthRulesConnectors.Models.Requests
{
    public class UpdateMemberAddressRequest
    {
        public string Get()
        {
            // Validation

            // Memebr #
            if (string.IsNullOrEmpty(memberHccId))
                throw new Exception("'memberHccId' must be entered");

            // Apply to correspondence address
            if (string.IsNullOrEmpty(applyToCorrespondenceAddress))
                throw new Exception("'memberHccId' must be entered");
            applyToCorrespondenceAddress = applyToCorrespondenceAddress.Trim().ToLower();
            if(applyToCorrespondenceAddress != "true" && applyToCorrespondenceAddress != "false")
                throw new Exception("'applyToCorrespondenceAddress' must be 'true' or 'false'");

            // Apply To Address Type
            if (string.IsNullOrEmpty(auditLogInfo.messageCode))
                throw new Exception("'auditLogInfo.messageCode' must be entered");

            // Apply To Address Type
            if (string.IsNullOrEmpty(auditLogInfo.messageDescription))
                throw new Exception("'auditLogInfo.messageDescription' must be entered");

            // Apply to force To Workbasket
            if (string.IsNullOrEmpty(forceToWorkbasket))
                throw new Exception("'forceToWorkbasket' must be entered");
            forceToWorkbasket = forceToWorkbasket.Trim().ToLower();
            if (forceToWorkbasket != "true" && forceToWorkbasket != "false")
                throw new Exception("'forceToWorkbasket' must be 'true' or 'false'");

            // Apply to force Address Update
            if (string.IsNullOrEmpty(forceAddressUpdate))
                throw new Exception("'forceAddressUpdate' must be entered");
            forceAddressUpdate = forceAddressUpdate.Trim().ToLower();
            if (forceAddressUpdate != "true" && forceAddressUpdate != "false")
                throw new Exception("'forceAddressUpdate' must be 'true' or 'false'");


            // Build object
            string output = XMLSerialization.BuildRequestObject(this, resRequests.UpdateMemberAddress);

            // * Return request objects
            return output;

        }



        public string memberHccId { get; set; }
        public MemberAddress memberAddress = new MemberAddress();
        /// <summary>
        /// Apply To Correspondence Address - Format value as 'true' or 'false'
        /// </summary>
        public string applyToCorrespondenceAddress { get; set; }
        public List<string> applyToAddressType = new List<string>();
        public AuditLogInfo auditLogInfo = new AuditLogInfo();
        /// <summary>
        /// Force to work basket - required - must be 'true' or false'
        /// </summary>
        public string forceToWorkbasket { get; set; }

        /// <summary>
        /// Force to address update - required - must be 'true' or false'
        /// </summary>
        public string forceAddressUpdate { get; set; }

    }

    public class MemberAddress
    {
        public string address { get; set; }
        public string address2 { get; set; }
        public string address3 { get; set; }
        public string cityName { get; set; }
        public string countryCode { get; set; }
        public string countyCode { get; set; }
        public string stateCode { get; set; }
        public string zipCode { get; set; }
        public string zipExtensionCode { get; set; }
    }

    public class AuditLogInfo
    {
        /// <summary>
        /// Comment - Optional
        /// </summary>
        public string comment { get; set; }

        /// <summary>
        /// Message code - required
        /// </summary>
        public string messageCode { get; set; }

        /// <summary>
        /// Mesage description - required
        /// </summary>
        public string messageDescription { get; set; }

        /// <summary>
        /// User ID - optional
        /// </summary>
        public string user { get; set; }
    }

}

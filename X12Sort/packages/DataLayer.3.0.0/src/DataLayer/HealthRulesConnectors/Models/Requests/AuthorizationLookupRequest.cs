using System;
using System.Collections.Generic;
using System.Text;
using DataLayer.StaticUtilities;

namespace DataLayer.HealthRulesConnectors.Models.Requests
{
    public class AuthorizationLookupRequest
    {
        /// <summary>
        /// Generate the serialized response object
        /// </summary>
        /// <returns></returns>
        public string Get()
        {
            // Serialize response and send
            return XMLSerialization.BuildRequestObject(this, resRequests.AuthorizationLookup);
        }

        /// <summary>
        /// Authorization ID
        /// </summary>
        public string authorizationId { get; set; }

        /// <summary>
        /// External Authorization ID 
        /// </summary>
        public string externalAuthorizationId { get; set; }

        /// <summary>
        /// As of Date - Formatted as 'YYYY-MM-DD'
        /// </summary>
        public string asOfDate { get; set; }
    }
}

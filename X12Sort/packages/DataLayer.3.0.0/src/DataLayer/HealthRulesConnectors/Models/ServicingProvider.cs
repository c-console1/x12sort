using DataLayer.Base.Models;

namespace DataLayer.HealthRulesConnectors.Models
{
    public class ServicingProvider : IAuthorizationProvider
    {
        /// <summary>
        /// Provider ID
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// Provider identifier
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// External provider identifier
        /// </summary>
        public string ExternalNumber { get; set; }

        /// <summary>
        /// Provider name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Provider NPI
        /// </summary>
        public string NPI { get; set; }

        /// <summary>
        /// Provider TIN
        /// </summary>
        public string TaxID { get; set; }
        /// <summary>
        /// Name of contact at provider location
        /// </summary>
        public string ContactName { get; set; }

        /// <summary>
        /// Provider phone
        /// </summary>
        public PhoneNumber Phone { get; set; }

        /// <summary>
        /// Provider contact phone
        /// </summary>
        public PhoneNumber ContactPhone { get; set; }
    }
}

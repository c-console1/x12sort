using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.HealthRulesConnectors.Models.Responses
{
    public class AuthorizationResponse
    {

        public class Response
        {
            public InnerEnvelope Envelope { get; set; }
        }

        public class InnerEnvelope
        {
            public string Header { get; set; }
            public Body Body { get; set; }
        }

        public class Body
        {
            public ServiceAuthorizationResponse serviceAuthorizationResponse { get; set; }
        }

        public class ServiceAuthorizationResponse
        {
            public string authorizationInternalId { get; set; }
            public string authorizationId { get; set; }
            public string externalAuthorizationId { get; set; }
            public string memberId { get; set; }
            public string status { get; set; }
            public string authorizationStatus { get; set; }
            public string recordType { get; set; }
            public string originator { get; set; }
            public string referringProviderId { get; set; }
            public string attachmentSetId { get; set; }
            public List<Services> services { get; set; }
            public TransactionInformation transactionInformation { get; set; }
            public Errors errors { get; set; }
        }

        public class Services
        {
            public string serviceSetIdentifier { get; set; }
            public string externalServiceSetId { get; set; }
            public string serviceSetStatus { get; set; }
            public string startDate { get; set; }
            public string endDate { get; set; }
            public string approvedCount { get; set; }
            public string approvedUnit { get; set; }
            public string approvedAmount { get; set; }
            public string referredToProviderId { get; set; }
        }

        public class TransactionInformation
        {
            public string alreadyInWorkbasket { get; set; }
            public string blockedByLock { get; set; }
            public string cvcId { get; set; }
            public List<Exceptions> exceptions { get; set; }
            public string lockingUser { get; set; }
            public string needsRepair { get; set; }
            public string needsReview { get; set; }
            public string rejected { get; set; }
            public string transactionSubmitted { get; set; }
            public string versionEffectiveDate { get; set; }
        }

        public class Exceptions
        {
            public string exceptionType { get; set; }
            public string messageCode { get; set; }
            public string messageDescription { get; set; }
            public string policyName { get; set; }
        }

        public class Errors
        {
            public List<Error> error { get; set; }
        }

        public class Error
        {
            public string errorType { get; set; }
            public string message { get; set; }
            public string dateTime { get; set; }
            public string authenticatedUser { get; set; }
            public string traceInfo { get; set; }
        }


    }
}

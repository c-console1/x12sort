namespace DataLayer.HealthRulesConnectors.Models.Responses
{
    public class GetMemberDetailsByHccIdResponse
    {


        public class Response
        {
            public EnvelopeInner Envelope { get; set; }
        }


        public class EnvelopeInner
        {
            public string Header { get; set; }
            public Body Body { get; set; }
        }

        public class Body
        {
            public GetMemberDetailsByHccIdResponse2 getMemberDetailsByHccIdResponse { get; set; }
        }

        public class GetMemberDetailsByHccIdResponse2
        {
            public string memberDetail { get; set; }
        }

    }
}

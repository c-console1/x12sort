using System.Collections.Generic;

namespace DataLayer.HealthRulesConnectors.Models.Responses
{
    public class MemberCoverageEventLookupResponse
    {

        public class Response
        {
            public EnvelopeInner Envelope { get; set; }
        }

        public class EnvelopeInner
        {
            public string Header { get; set; }
            public Body Body { get; set; }
        }

        public class Body
        {
            public MemberCoverageEventLookupResponse2 memberCoverageEventLookupResponse { get; set; }
        }

        public class MemberCoverageEventLookupResponse2
        {
            public string status { get; set; }
            public string startDate { get; set; }
            public string startTime { get; set; }
            public string endDate { get; set; }
            public string endTime { get; set; }
            public string exchangeState { get; set; }
            public List<string> eventsTypeList { get; set; }
            public List<ReasonCodeList> reasonCodeList { get; set; }
            public List<SubscriptionList> subscriptionList { get; set; }
            public string totalSubscriptions { get; set; }
            public string totalEvents { get; set; }
            public List<EventTotal> eventTotal { get; set; }
            public Errors errors { get; set; }
        }

        public class ReasonCodeList
        {
            public string messageCode { get; set; }
            public string codeDomainName { get; set; }
            public string messageDescription { get; set; }
            public string policyName { get; set; }
            public string messageAction { get; set; }
            public string messageResponsibility { get; set; }
        }

        public class SubscriptionList
        {
            public string hccIdentifier { get; set; }
            public List<MemberList> memberList { get; set; }
        }

        public class MemberList
        {
            public string hccIdentifier { get; set; }
            public string memberIsSubscriber { get; set; }
            public string lastName { get; set; }
            public string firstName { get; set; }
            public string middleName { get; set; }
            public List<string> nameSuffix { get; set; }
            public List<CredentialSuffixList> credentialSuffixList { get; set; }
            public string genderCode { get; set; }
            public string birthDate { get; set; }
            public OldPlan oldPlan { get; set; }
            public NewPlan newPlan { get; set; }
            public string eventTimestamp { get; set; }
            public string eventsType { get; set; }
            public string isNewSubscription { get; set; }
            public string isNewMember { get; set; }
            public string eventDate { get; set; }
            public string isPlanRenewalDate { get; set; }
            public string oldPlanStartDate { get; set; }
            public string oldPlanEndDate { get; set; }
            public ReasonCode reasonCode { get; set; }
        }

        public class CredentialSuffixList
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class OldPlan
        {
            public string benefitPlanName { get; set; }
            public string hccBenefitPlanID { get; set; }
            public List<BenefitPlanIdentifiers> benefitPlanIdentifiers { get; set; }
        }

        public class BenefitPlanIdentifiers
        {
            public string identificationNumber { get; set; }
            public string issuingState { get; set; }
            public string effectiveStartDate { get; set; }
            public string effectiveEndDate { get; set; }
            public IdentificationType identificationType { get; set; }
        }

        public class IdentificationType
        {
            public string codeSetName { get; set; }
            public string codeEntry { get; set; }
            public string shortName { get; set; }
        }

        public class NewPlan
        {
            public string benefitPlanName { get; set; }
            public string hccBenefitPlanID { get; set; }
            public List<BenefitPlanIdentifiers> benefitPlanIdentifiers { get; set; }
        }

        public class ReasonCode
        {
            public string messageCode { get; set; }
            public string codeDomainName { get; set; }
            public string messageDescription { get; set; }
            public string policyName { get; set; }
            public string messageAction { get; set; }
            public string messageResponsibility { get; set; }
        }

        public class EventTotal
        {
            public string eventType { get; set; }
            public string total { get; set; }
        }

        public class Errors
        {
            public List<Error> error { get; set; }
        }

        public class Error
        {
            public string errorType { get; set; }
            public string message { get; set; }
            public string dateTime { get; set; }
            public string authenticatedUser { get; set; }
            public string traceInfo { get; set; }
        }

    }
}

using System;
using System.Collections.Generic;
using System.Linq;

namespace DataLayer.HealthRulesConnectors.Models.Responses
{
    public class MembershipLookupResponse
    {
        /// <summary>
        /// Primary response object
        /// </summary>
        public class Response
        {
            public EnvelopeInner Envelope { get; set; }

            #region Easier access values


            public string AttachmentSetReferenceet
            {
                get
                {
                    try
                    {
                        return this.Envelope.Body.membershipLookupResponse.membership.attachmentSetReference.ID;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }

            }

            public List<ConfidentialCommunicationDirective> ConfidentialCommunicationDirectives
            {
                get
                {
                    try
                    {
                        return this.Envelope.Body.membershipLookupResponse.membership.ccDirectiveList.confidentialCommunicationDirective;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            public CorrespondenceAddress CorrespondenceAddress
            {
                get
                {
                    try
                    {
                        return this.Envelope.Body.membershipLookupResponse.membership.correspondenceAddress;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            public DentalInfo DentalInfo
            {
                get
                {
                    try
                    {
                        return this.Envelope.Body.membershipLookupResponse.membership.dentalInfo;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            public DependentVerificationParent DependentVerification
            {
                get
                {
                    try
                    {
                        return this.Envelope.Body.membershipLookupResponse.membership.dependentVerification.FirstOrDefault();
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            public DisabilityInfoForEligibility DisabilityInfoForEligibility
            {
                get
                {
                    try
                    {
                        return this.Envelope.Body.membershipLookupResponse.membership.disabilityInfoForEligibility;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            public EmploymentInfo EmploymentInfo
            {
                get
                {
                    try
                    {
                        return this.Envelope.Body.membershipLookupResponse.membership.employmentInfo;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// Member HCC Identifier
            /// </summary>
            public string HCCIdentifier
            {
                get
                {
                    try
                    {
                        return this.Envelope.Body.membershipLookupResponse.membership.hccIdentifier;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// Health Status Reason Type Code
            /// </summary>
            public TypeCode HealthStatusReason
            {
                get
                {
                    try
                    {
                        return this.Envelope.Body.membershipLookupResponse.membership.healthStatusReasonCode;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// Individual Memeber Informations
            /// </summary>
            public Individual individual
            {
                get
                {
                    try
                    {
                        return this.Envelope.Body.membershipLookupResponse.membership.individual;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// Individual Reference code
            /// </summary>
            public IDCode IndividualReference
            {
                get
                {
                    try
                    {
                        return this.Envelope.Body.membershipLookupResponse.membership.individualReference;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// People who have access to a member's informations
            /// </summary>
            public InformationAccessPermissionList InformationAccessPermissionList
            {
                get
                {
                    try
                    {
                        return this.Envelope.Body.membershipLookupResponse.membership.informationAccessPermissionList;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// People who have limited or no access to a member's informations
            /// </summary>
            public InformationAccessRestrictionList InformationAccessRestrictionList
            {
                get
                {
                    try
                    {
                        return this.Envelope.Body.membershipLookupResponse.membership.informationAccessRestrictionList;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// Source of the information
            /// </summary>
            public TypeCode InformationSourceCode
            {
                get
                {
                    try
                    {
                        return this.Envelope.Body.membershipLookupResponse.membership.informationSourceCode;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// Is the member in Hospice?
            /// </summary>
            public YesNo IsMemberInHospiceYesNo
            {
                get {

                    if (IsMemberInHospice == null)
                        return YesNo.Unknown;


                    switch (IsMemberInHospice.Trim().ToLower()) {
                        case "true":
                        case "yes":
                            return YesNo.Yes;
                        case "false":
                        case "no":
                            return YesNo.No;
                        default:
                            return YesNo.Unknown;
                    }

                }
            }

            /// <summary>
            /// Is the member in Hospice?
            /// </summary>
            public string IsMemberInHospice
            {
                get
                {
                    try
                    {
                        return this.Envelope.Body.membershipLookupResponse.membership.isMemberInHospice;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// Is this member the subscriber?
            /// </summary>
            public string MemberIsSubscriber
            {
                get
                {
                    try
                    {
                        return this.Envelope.Body.membershipLookupResponse.membership.memberIsSubscriber;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// Is this member the subscriber?
            /// </summary>
            public YesNo MemberIsSubscriberYesNo
            {
                get
                {

                    if (MemberIsSubscriber == null)
                        return YesNo.Unknown;


                    switch (MemberIsSubscriber.Trim().ToLower())
                    {
                        case "true":
                        case "yes":
                            return YesNo.Yes;
                        case "false":
                        case "no":
                            return YesNo.No;
                        default:
                            return YesNo.Unknown;
                    }

                }
            }

            /// <summary>
            /// Does this member require representation, if so give the representive's information
            /// </summary>
            public MemberRepresentative MemberRepresentative
            {
                get
                {
                    try
                    {
                        return this.Envelope.Body.membershipLookupResponse.membership.memberRepresentative;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// Membership Identifier Code
            /// </summary>
            public string MembershipIdentifier
            {
                get
                {
                    try
                    {
                        return this.Envelope.Body.membershipLookupResponse.membership.membershipIdentifier;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }


            /// <summary>
            /// List of Membership User Defined Terms
            /// </summary>
            public List<MembershipUDT> MembershipUDTs
            {
                get
                {
                    try
                    {
                        return this.Envelope.Body.membershipLookupResponse.membership.membershipUDTList.membershipUDT;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// List of Other ID #'s
            /// </summary>
            public List<IdentificationNumber> OtherIdNumbers
            {
                get
                {
                    try
                    {
                        return this.Envelope.Body.membershipLookupResponse.membership.otherIdNumberList.identificationNumber;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// List of Other Responsible Person
            /// </summary>
            public OtherResponsiblePersonInfoParent OtherResponsiblePerson
            {
                get
                {
                    try
                    {
                        return this.Envelope.Body.membershipLookupResponse.membership.otherResponsiblePersonInfo.FirstOrDefault();
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// Is this member out of the service area?
            /// </summary>
            public string OutOfServiceArea
            {
                get
                {
                    try
                    {
                        return this.Envelope.Body.membershipLookupResponse.membership.outOfServiceArea;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// Is this member out of the service area?
            /// </summary>
            public YesNo OutOfServiceAreaYesNo
            {
                get
                {
                    if (OutOfServiceArea == null)
                        return YesNo.Unknown;


                    switch (OutOfServiceArea.Trim().ToLower())
                    {
                        case "true":
                        case "yes":
                            return YesNo.Yes;
                        case "false":
                        case "no":
                            return YesNo.No;
                        default:
                            return YesNo.Unknown;
                    }

                }
            }

            /// <summary>
            /// Payee HCC ID
            /// </summary>
            public string PayeeHCCID
            {
                get
                {
                    try
                    {
                        return this.Envelope.Body.membershipLookupResponse.membership.payeeHccId;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// People who have access to this member's PHI
            /// </summary>
            public List<HIPAAAuthorization> PHIAuthorizationList
            {
                get
                {
                    try
                    {
                        return this.Envelope.Body.membershipLookupResponse.membership.phiAuthorizationList.HIPAAAuthorization;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// List of the member's physical addresses
            /// </summary>
            public List<MemberPhysicalAddress> MemberPhysicalAddresses
            {
                get
                {
                    try
                    {
                        return this.Envelope.Body.membershipLookupResponse.membership.physicalAddress.memberPhysicalAddress;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// Physical characteristics
            /// </summary>
            public PhysicalCharacteristics PhysicalCharacteristics
            {
                get
                {
                    try
                    {
                        return this.Envelope.Body.membershipLookupResponse.membership.physicalCharacteristics;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// Most current plan select
            /// </summary>
            public PlanSelection PlanSelection
            {
                get
                {
                    try
                    {
                        return this.Envelope.Body.membershipLookupResponse.membership.planSelection;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }


            /// <summary>
            /// List of pre-existing condition
            /// </summary>
            public preExistingConditionInfoParent PreExistingConditions
            {
                get
                {
                    try
                    {
                        
                        return this.Envelope.Body.membershipLookupResponse.membership.preExistingConditionInfo.FirstOrDefault();
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// Pre Existsing Reduction
            /// </summary>
            public PreExReduction PreExReduction
            {
                get
                {
                    try
                    {

                        return this.Envelope.Body.membershipLookupResponse.membership.preExReduction;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// Prior coverage information
            /// </summary>
            public PriorCoverageInfo PriorCoverage
            {
                get
                {
                    try
                    {

                        return this.Envelope.Body.membershipLookupResponse.membership.priorCoverageInfo;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// Provider selection
            /// </summary>
            public ProviderSelection ProviderSelection
            {
                get
                {
                    try
                    {
                        return this.Envelope.Body.membershipLookupResponse.membership.providerSelection;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// List of race and ethnicity codes
            /// </summary>
            public List<TypeCode> RaceOrEthnicity
            {
                get
                {
                    try
                    {
                        return this.Envelope.Body.membershipLookupResponse.membership.raceOrEthnicityCodes;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }


            /// <summary>
            /// Receipt date
            /// </summary>
            public string ReceiptDate
            {
                get
                {
                    try
                    {
                        return this.Envelope.Body.membershipLookupResponse.membership.receiptDate;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// Receipt date formatted as a date/time object
            /// </summary>
            public DateTime ReceiptDateFormatted
            {
                get
                {
                    try
                    {
                        return DateTime.Parse(ReceiptDate);
                    }
                    catch (Exception)
                    {
                        return DateTime.Parse("1/1/1800");
                    }
                }
            }

            /// <summary>
            /// Receipt date
            /// </summary>
            public string RelationshipToSubscriberDefinitionReference
            {
                get
                {
                    try
                    {
                        return this.Envelope.Body.membershipLookupResponse.membership.relationshipToSubscriberDefinitionReference.ID;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }


            /// <summary>
            /// Smoking Status
            /// </summary>
            public string SmokingStatus
            {
                get
                {
                    try
                    {
                        return this.Envelope.Body.membershipLookupResponse.membership.smokingStatus;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// Smoking Status - Yes / No enumorator
            /// </summary>
            public YesNo SmokingStatusYesNo
            {
                get
                {
                    if (SmokingStatus == null)
                        return YesNo.Unknown;


                    switch (SmokingStatus.Trim().ToLower())
                    {
                        case "true":
                        case "yes":
                            return YesNo.Yes;
                        case "false":
                        case "no":
                            return YesNo.No;
                        default:
                            return YesNo.Unknown;
                    }

                }
            }


            /// <summary>
            /// Subscription Reference
            /// </summary>
            public string SubscriptionReference
            {
                get
                {
                    try
                    {
                        return this.Envelope.Body.membershipLookupResponse.membership.subscriptionReference.ID;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// VIP reason code
            /// </summary>
            public TypeCode VIPReason
            {
                get
                {
                    try
                    {
                        return this.Envelope.Body.membershipLookupResponse.membership.vipReasonCode;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// Waive PHI claim view restriction
            /// </summary>
            public string WaivePHIClaimViewRestriction
            {
                get
                {
                    try
                    {
                        return this.Envelope.Body.membershipLookupResponse.membership.waivePHIClaimViewRestriction;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// Smoking Status - Yes / No enumorator
            /// </summary>
            public YesNo WaivePHIClaimViewRestrictionYesNo
            {
                get
                {
                    if (WaivePHIClaimViewRestriction == null)
                        return YesNo.Unknown;


                    switch (WaivePHIClaimViewRestriction.Trim().ToLower())
                    {
                        case "true":
                        case "yes":
                            return YesNo.Yes;
                        case "false":
                        case "no":
                            return YesNo.No;
                        default:
                            return YesNo.Unknown;
                    }

                }
            }

            #endregion

        }


        public class OtherName
        {
            public IndividualName otherNameInfo { get; set; }
            public TypeCode otherNameTypeCode { get; set; }
        }

        public class OtherNames
        {
            public List<OtherName> otherName { get; set; }
        }

        public class AddressInfo
        {
            public AddressLong postalAddress { get; set; }
            public AddressPhoneList addressPhoneList { get; set; }
        }

        public class MemberPhysicalAddress
        {
            public AddressInfo addressInfo { get; set; }
            public TypeCode addressTypeCode { get; set; }
        }

        public class PhysicalAddress
        {
            public List<MemberPhysicalAddress> memberPhysicalAddress { get; set; }
        }


        public class CorrespondenceAddress
        {
            public AddressLong postalAddress { get; set; }
        }


        public class IdentificationNumber
        {
            /// <summary>
            /// List of identity numbers
            /// </summary>
            public List<string> identificationNumber { get; set; }

            /// <summary>
            /// Issuing state
            /// </summary>
            public string issuingState { get; set; }

            /// <summary>
            /// Issing Country
            /// </summary>
            public CountryCode issuingCountry { get; set; }

            /// <summary>
            /// Efective Start Date
            /// </summary>
            public string effectiveStartDate { get; set; }

            /// <summary>
            /// Effective end date
            /// </summary>
            public string effectiveEndDate { get; set; }

            /// <summary>
            /// Identification Type Code
            /// </summary>
            public TypeCode identificationTypeCode { get; set; }
        }

        public class OtherIdNumberList
        {
            public List<IdentificationNumber> identificationNumber { get; set; }
        }


        public class MembershipUDT
        {
            public List<UdtListValueSet> udtListValueSet { get; set; }
            public IDCode userDefinedTermReference { get; set; }
        }

        public class MembershipUDTList
        {
            public List<MembershipUDT> membershipUDT { get; set; }
        }

        public class RelationshipToSubscriberDefinitionReference
        {
            public string ID { get; set; }
        }


        public class BenefitPlanReference
        {
            public string benefitPlanHccId { get; set; }
            public string benefitPlanName { get; set; }
        }

        public class PlanSelection
        {
            public string startDate { get; set; }
            public string endDate { get; set; }
            public EnrollmentInfo enrollmentInfo { get; set; }

            public TypeCode memberTerminateReasonCode { get; set; }

            public BenefitPlanReference benefitPlanReference { get; set; }
        }

        public class Membership
        {
            public string membershipIdentifier { get; set; }
            public string memberIsSubscriber { get; set; }
            public string outOfServiceArea { get; set; }
            public string hccIdentifier { get; set; }
            public string receiptDate { get; set; }
            public string smokingStatus { get; set; }
            public string waivePHIClaimViewRestriction { get; set; }
            public string payeeHccId { get; set; }
            public string isMemberInHospice { get; set; }
            public Individual individual { get; set; }
            public PhysicalAddress physicalAddress { get; set; }
            public CorrespondenceAddress correspondenceAddress { get; set; }
            public OtherIdNumberList otherIdNumberList { get; set; }
            public DisabilityInfoForEligibility disabilityInfoForEligibility  { get; set; }
            public List<DependentVerificationParent> dependentVerification { get; set; }
            public PhysicalCharacteristics physicalCharacteristics { get; set; }
            public EmploymentInfo employmentInfo { get; set; }
            public DentalInfo dentalInfo { get; set; }
            public PriorCoverageInfo priorCoverageInfo { get; set; }
            public List<preExistingConditionInfoParent> preExistingConditionInfo { get; set; }
            public PreExReduction preExReduction { get; set; }
            public List<OtherResponsiblePersonInfoParent> otherResponsiblePersonInfo { get; set; }
            public MemberRepresentative memberRepresentative { get; set; }
            public MembershipUDTList membershipUDTList { get; set; }
            public InformationAccessPermissionList informationAccessPermissionList { get; set; }
            
            // This may need to be converted to a <List> object
            public InformationAccessRestrictionList informationAccessRestrictionList { get; set; }
            
            // This may need to be converted to a <List> object
            public PhiAuthorizationList phiAuthorizationList { get; set; }
            
            // This may need to be converted to a <List> object
            public CcDirectiveList ccDirectiveList { get; set; }
            
            public IDCode subscriptionReference { get; set; }
            
            public IDCode individualReference { get; set; }

            public IDCode relationshipToSubscriberDefinitionReference { get; set; }

            public List<TypeCode> raceOrEthnicityCodes { get; set; }
            
            public TypeCode vipReasonCode { get; set; }
            
            public TypeCode healthStatusReasonCode { get; set; }
            
            public TypeCode informationSourceCode { get; set; }

            public IDCode attachmentSetReference { get; set; }
            public PlanSelection planSelection { get; set; }
            public ProviderSelection providerSelection { get; set; }
        }

        public class EnrollmentInfo
        {
            public string applicationSignatureDomain { get; set; }

            public string applicationSignatureDate { get; set; }

            public string applicationReceiptDate { get; set; }

            public string completedDate { get; set; }

            public string submitDate { get; set; }

            public string disenrollDate { get; set; }

            public string disenrollReason { get; set; }

            public TypeCode medicareEnrollmentPeriodCode { get; set; }

        }

        
        public class ProviderSelection
        {
            
            public string providerRoleType { get; set; }
            
            public string startDate { get; set; }
            
            public string endDate { get; set; }
            
            public PractitionerReference practitionerReference { get; set; }
            
            public PractitionerRoleReference practitionerRoleReference { get; set; }
            
            public SupplierReference supplierReference { get; set; }
            
            public AffiliatedSupplierNetworkReference affiliatedSupplierNetworkReference { get; set; }
        }


        /// <summary>
        /// Supplier Reference
        /// </summary>
        public class SupplierReference
        {
            
            public string hccIdentificationNumber { get; set; }
            
            public string supplierOtherIdentificationNumber { get; set; }
            
            public IndividualName primaryName { get; set; }
            
            public string asOfDate { get; set; }
        }

        
        public class AffiliatedSupplierNetworkReference
        {
            
            public string hccIdentificationNumber { get; set; }
        }


        
        public class PractitionerReference
        {
            
            public string hccIdentificationNumber { get; set; }
            
            public string practitionerOtherIdentificationNumber { get; set; }
            
            public string firstName { get; set; }
            
            public string lastName { get; set; }
            
            public string asOfDate { get; set; }
        }

        
        public class PractitionerRoleReference
        {
            
            public string hccIdentificationNumber { get; set; }
            
            public string practitionerRoleOtherIdentificationNumber { get; set; }
            
            public TypeCode practitionerRoleOtherIdTypeCode { get; set; }
            
            public string asOfDate { get; set; }
            
            public string roleName { get; set; }
        }



        
        public class DentalInfo
        {
            
            public string remarks { get; set; }
            
            public MissingTeeth missingTeeth { get; set; }
        }

        
        public class MissingTeeth
        {
            
            // This may need to be converted to a <List> object
            public string listMode { get; set; }
            // The object below will most likely need to put into a <List>
            
            public List<string> toothName { get; set; }
        }

        
        public class PriorCoverageInfo
        {
            
            public string priorCoverageMonthCount { get; set; }
        }

        public class preExistingConditionInfoParent
        {
            List<PreExistingConditionInfo> preExistingConditionInfo { get; set; }
        }

        
        public class PreExistingConditionInfo
        {
            /// <summary>
            /// Onset Of Condition Date - Formatted as 'YYYY-MM-DD'
            /// </summary>
            public string onsetOfConditionDate { get; set; }

            /// <summary>
            /// Pre-existing Period End Date - Formatted as 'YYYY-MM-DD'
            /// </summary>
            public string preexistingPeriodEndDate { get; set; }

            /// <summary>
            /// Pre Existing Condition Description
            /// </summary>
            public string preexDescription { get; set; }

            
            public PreExDiagnosisCodeRangeConditions preExDiagnosisCodeRangeConditions { get; set; }
            
            public PreExProcedureCodeRangeConditions preExProcedureCodeRangeConditions { get; set; }
            
            public PreexistingConditions preexistingConditions { get; set; }
            
            public PreexistingConditionProcedures preexistingConditionProcedures { get; set; }
        }


        
        public class MemberRepresentative
        {
            
            public List<AuthorizedIndividual> authorizedIndividual { get; set; }
        }

        
        public class AuthorizedIndividual
        {
            
            public string effectiveStartDate { get; set; }
            
            public string effectiveEndDate { get; set; }
            
            public string scope { get; set; }
            
            public string hccReviewer { get; set; }
            
            public string documentsReceivedDate { get; set; }
            
            public AuthorizedPerson authorizedPerson { get; set; }
            
            // This may need to be converted to a <List> object
            public AuthorizedIndividualGroupPhoneList authorizedIndividualGroupPhoneList { get; set; }
            
            public TypeCode representativeTypeCode { get; set; }
            
            public TypeCode relationshipToMemberCode { get; set; }
        }

        
        public class AuthorizedPerson
        {
            
            public string lastName { get; set; }
            
            public string firstName { get; set; }
            
            public string middleName { get; set; }
            
            public string namePrefix { get; set; }
            
            public string nameSuffix { get; set; }
            
            // This may need to be converted to a <List> object
            public NameSuffixList nameSuffixList { get; set; }
            
            // This may need to be converted to a <List> object
            public CredentialSuffixList credentialSuffixList { get; set; }
        }

        
        public class AuthorizedIndividualGroupPhoneList
        {
            public string listMode { get; set; }
            // The object below will most likely need to put into a <List>
            
            public List<TelephoneNumber> telephoneNumber { get; set; }
        }

        public class OtherResponsiblePersonInfoParent {
            List<OtherResponsiblePersonInfo> otherResponsiblePersonInfo { get; set;  }
        }

        
        public class OtherResponsiblePersonInfo
        {
            
            public string sourceOfAuthorization { get; set; }
            
            public string responsiblePersonEmailAddress { get; set; }
            
            public ResponsiblePersonName responsiblePersonName { get; set; }
            
            public ResponsibleAddressInfo responsibleAddressInfo { get; set; }
            
            public TypeCode responsibilityTypeCodeCode { get; set; }
            
            public TypeCode relationshipToMemberCode { get; set; }
        }

        
        public class ResponsiblePersonName
        {
            
            public string lastName { get; set; }
            
            public string firstName { get; set; }
            
            public string middleName { get; set; }
            
            public string namePrefix { get; set; }
            
            public string nameSuffix { get; set; }
            
            // This may need to be converted to a <List> object
            public NameSuffixList nameSuffixList { get; set; }
            
            // This may need to be converted to a <List> object
            public CredentialSuffixList credentialSuffixList { get; set; }
        }

        
        public class ResponsibleAddressInfo
        {
            /// <summary>
            /// Postal address
            /// </summary>
            public AddressLong postalAddress { get; set; }

            /// <summary>
            /// Object that contains a list of address phone list
            /// </summary>
            public AddressPhoneList addressPhoneList { get; set; }
        }

    

        
        public class PreExProcedureCodeRangeConditions
        {
            
            public List<PreExProcedureCodeRangeCondition> preExProcedureCodeRangeCondition { get; set; }
        }

        
        public class PreExProcedureCodeRangeCondition
        {
            
            public RangeProcedureCode rangeStartProcedureCode { get; set; }
            
            public RangeProcedureCode rangeEndProcedureCode { get; set; }
        }


        
        public class RangeProcedureCode
        {
            
            public List<IDCode> serviceReference { get; set; }
        }

        
        public class RangeEndProcedureCode
        {
            
            public List<IDCode> serviceReference { get; set; }
        }

        
        public class PreexistingConditions
        {
            
            public List<IDCode> diagnosisReference { get; set; }
        }

        
        public class PreexistingConditionProcedures
        {
            
            public List<IDCode> serviceReference { get; set; }
        }

        
        public class PreExReduction
        {
            /// <summary>
            /// Pre-existing Reduction Start Date - Formatted as 'YYYY-MM-DD'
            /// </summary>
            public string preexStartDate { get; set; }

            /// <summary>
            /// Pre-existing Reduction End Date - Formatted as 'YYYY-MM-DD'
            /// </summary>
            public string reductionEndDate { get; set; }
            /// <summary>
            /// Reduction Reason Receipt Date- Formatted as 'YYYY-MM-DD'
            /// </summary>
            public string reductionReasonReceiptDate { get; set; }

            /// <summary>
            /// Reduction Reason Code
            /// </summary>
            public TypeCode reductionReasonCode { get; set; }
        }


        
        public class PreExDiagnosisCodeRangeConditions
        {
            
            public List<PreExDiagnosisCodeRangeCondition> preExDiagnosisCodeRangeCondition { get; set; }
        }

        
        public class PreExDiagnosisCodeRangeCondition
        {
            
            public RangeDiagnosisCode rangeStartDiagnosisCode { get; set; }
            
            public RangeDiagnosisCode rangeEndDiagnosisCode { get; set; }
        }

        
        public class RangeDiagnosisCode
        {
            
            public List<IDCode> diagnosisReference { get; set; }
        }


        
        public class EmploymentInfo
        {
            /// <summary>
            /// Hire date - Formatted as 'YYYY-MM-DD'
            /// </summary>
            public string hireDate { get; set; }

            
            public string salaryInterval { get; set; }
            
            public string salaryPerInterval { get; set; }
            
            public string unionStatus { get; set; }
            
            public string positionOrTitle { get; set; }
            
            public string numberOfHoursWorked { get; set; }

            /// <summary>
            /// Retirement date - Formatted as 'YYYY-MM-DD'
            /// </summary>
            public string retirementDate { get; set; }


            /// <summary>
            /// Employment status code
            /// </summary>
            public TypeCode employmentStatusCodeCode { get; set; }

            /// <summary>
            /// Salary Grade Code
            /// </summary>
            public TypeCode salaryGradeCodeCode { get; set; }

            /// <summary>
            /// Units domain
            /// </summary>
            public TypeCode unitsDomainCode { get; set; }

            /// <summary>
            /// Employee Type Code
            /// </summary>
            public TypeCode employeeTypeDomainCode { get; set; }

            /// <summary>
            /// Department name
            /// </summary>
            public string department { get; set; }
        }



        
        public class InformationAccessPermissionList
        {
            
            public List<HIPAAPermission> HIPAAPermission { get; set; }
        }

        
        public class HIPAAPermission
        {
            
            public string permissionScope { get; set; }
            
            public string effectiveStartDate { get; set; }
            
            public string effectiveEndDate { get; set; }
            
            public PermittedPerson permittedPerson { get; set; }
            
            // This may need to be converted to a <List> object
            public HIPAAPermissionGroupPhoneList hIPAAPermissionGroupPhoneList { get; set; }
            
            public TypeCode relationshipToMemberCode { get; set; }
        }

        
        public class PermittedPerson
        {
            
            public string lastName { get; set; }
            
            public string firstName { get; set; }
            
            public string middleName { get; set; }
            
            public string namePrefix { get; set; }
            
            public string nameSuffix { get; set; }
            
            // This may need to be converted to a <List> object
            public NameSuffixList nameSuffixList { get; set; }
            
            // This may need to be converted to a <List> object
            public CredentialSuffixList credentialSuffixList { get; set; }
        }

        
        public class HIPAAPermissionGroupPhoneList
        {

            public string listMode { get; set; }

            public List<TelephoneNumber> telephoneNumber { get; set; }
        }

        
        public class InformationAccessRestrictionList
        {
            
            public List<HIPAARestriction> HIPAARestriction { get; set; }
        }

        
        public class HIPAARestriction
        {
            
            public string restrictionScope { get; set; }
            
            public string effectiveStartDate { get; set; }
            
            public string effectiveEndDate { get; set; }
            
            public RestrictedPerson restrictedPerson { get; set; }
            
            public TypeCode relationshipToMemberCode { get; set; }
        }

        
        public class RestrictedPerson
        {
            
            public string lastName { get; set; }
            
            public string firstName { get; set; }
            
            public string middleName { get; set; }
            
            public string namePrefix { get; set; }
            
            public string nameSuffix { get; set; }
            
            // This may need to be converted to a <List> object
            public NameSuffixList nameSuffixList { get; set; }
            
            // This may need to be converted to a <List> object
            public CredentialSuffixList credentialSuffixList { get; set; }
        }

        
        public class PhiAuthorizationList
        {
            
            public List<HIPAAAuthorization> HIPAAAuthorization { get; set; }
        }

        
        public class HIPAAAuthorization
        {
            
            public string authorizationType { get; set; }
            
            public string authorizationScope { get; set; }
            
            public string effectiveStartDate { get; set; }
            
            public string effectiveEndDate { get; set; }
            
            public string whoCanBeDisclosedTo { get; set; }
            
            public string signature { get; set; }
            
            public string signatureDate { get; set; }
            
            public TypeCode authorizationPurposeCode { get; set; }
        }



        
        public class CcDirectiveList
        {
            
            public List<ConfidentialCommunicationDirective> confidentialCommunicationDirective { get; set; }
        }


        
        public class PhysicalCharacteristics
        {
            
            public string heightInchesCount { get; set; }
            
            public string weightPoundsCount { get; set; }
        }

        public class DependentVerificationParent
        {
            List<DependentVerification> dependentVerification { get; set; }
        }

        
        public class DependentVerification
        {
            
            public string verificationType { get; set; }
            
            public string certLastInfoRequestDate { get; set; }
            
            public string certLastInfoReceiptDate { get; set; }
            
            public string certInformationDueDate { get; set; }
            
            public string lastVerificationDate { get; set; }
            
            public StudentSchoolInfo studentSchoolInfo { get; set; }
        }

        
        public class StudentSchoolInfo
        {
            
            public string studentStatusCode { get; set; }

            /// <summary>
            /// Graduation date - Formatted as 'YYYY-MM-DD'
            /// </summary>
            public string graduationDate { get; set; }

            /// <summary>
            /// School Information
            /// </summary>
            public SchoolInformation schoolInformation { get; set; }

            /// <summary>
            /// School Tyoe Code
            /// </summary>
            public TypeCode schoolTypeCode { get; set; }
        }

        /// <summary>
        /// School Information
        /// </summary>
        public class SchoolInformation
        {
            /// <summary>
            /// School / Institution Name
            /// </summary>
            public string institutionName { get; set; }

            /// <summary>
            /// Contact information
            /// </summary>
            public ContactInfo contactInfo { get; set; }
        }


        /// <summary>
        /// Contact information
        /// </summary>
        public class ContactInfo
        {
            /// <summary>
            /// Contact description
            /// </summary>
            public string contactDescription { get; set; }
            
            public string contactEmailAddress { get; set; }

            /// <summary>
            /// Contact Name object
            /// </summary>
            public ContactName contactName { get; set; }
            
            public LocationAddress locationAddress { get; set; }
            
            public TypeCode contactTypeCode { get; set; }
        }

        
        public class ContactName
        {
            
            public string lastName { get; set; }
            
            public string firstName { get; set; }
            
            public string middleName { get; set; }
            
            public string namePrefix { get; set; }
            
            public string nameSuffix { get; set; }
            /// <summary>
            /// Name suffix object with sub-objects list
            /// </summary>
            public NameSuffixList nameSuffixList { get; set; }

            /// <summary>
            /// Credential suffix object with sub-objects list
            /// </summary>
            public CredentialSuffixList credentialSuffixList { get; set; }
        }

        
        public class LocationAddress
        {
            /// <summary>
            /// Postal address
            /// </summary>
            public AddressLong postalAddress { get; set; }

            /// <summary>
            /// Address phone # list
            /// </summary>
            public AddressPhoneList addressPhoneList { get; set; }
        }

               
        public class ConfidentialCommunicationDirective
        {
            
            public string confidentialCommunicationScope { get; set; }
            
            public string effectiveStartDate { get; set; }
            
            public string effectiveEndDate { get; set; }
            
            public TypeCode confidentialCommunicationTypeCode { get; set; }
        }

        public class DisabilityInfoForEligibility
        {
            
            public string disabilityTypeCode { get; set; }
            
            public string disabilityReportedDate { get; set; }
            
            public string verificationReceiptDate { get; set; }
            
            public TypeCode disabilityDenialReasonCode { get; set; }
            
            public List<IDCode> diagnosisReference { get; set; }
        }

        
        public class Individual
        {
            
            public string taxIdentificationNumber { get; set; }
            
            public string genderCode { get; set; }
            
            public string maritalStatusCode { get; set; }
            
            public string birthDate { get; set; }
            
            public string birthPlaceStateCode { get; set; }
            
            public string birthPlaceCountyCode { get; set; }
            
            public CountryCode birthPlaceCountryCode { get; set; }

            public string countryCode2
            {
                get {
                    if (birthPlaceCountryCode == null)
                        return null;
                    return birthPlaceCountryCode.countryCode;
                }
            }

            /// <summary>
            /// Date of Death
            /// </summary>
            public string dateOfDeath { get; set; }
            
            public IndividualName primaryName = new IndividualName();
            
            public OtherNames otherNames = new OtherNames();

            public Languages languages = new Languages();

            public TypeCode immigrationStatusCode { get; set; }
        }


        public class Languages
        {

            // This may need to be converted to a <List> object
            public string listMode { get; set; }
            
            public List<Language> language { get; set; }
        }


        public class MembershipLookupResponse2
        {
            /// <summary>
            /// Primary Membership object
            /// </summary>
            public Membership membership { get; set; }

            /// <summary>
            /// Call status string
            /// </summary>
            public string status { get; set; }

            /// <summary>
            /// Error(s)
            /// </summary>
            public Errors errors { get; set; }

        }

        
        public class Errors
        {
            
            public List<Error> error { get; set; }
        }

        
        public class Error
        {
            /// <summary>
            /// Error Type Code
            /// </summary>
            public string errorType { get; set; }

            /// <summary>
            /// Human readable message
            /// </summary>
            public string message { get; set; }

            /// <summary>
            /// Date / Time errors experienced
            /// </summary>
            public string dateTime { get; set; }

            /// <summary>
            /// Username that made the Connector call
            /// </summary>
            public string authenticatedUser { get; set; }

            /// <summary>
            /// Trace information
            /// </summary>
            public string traceInfo { get; set; }
        }

        public class Body
        {
            public MembershipLookupResponse2 membershipLookupResponse { get; set; }
        }

        public class Header
        {
            public string holder { get; set; }
        }

        /// <summary>
        /// Inner  Envelope
        /// </summary>
        public class EnvelopeInner
        {
            /// <summary>
            /// Body object
            /// </summary>
            public Body Body { get; set; }

            /// <summary>
            /// Header object
            /// </summary>
            public Header Header { get; set; }
        }




    }
}

using System;
using System.Collections.Generic;

namespace DataLayer.HealthRulesConnectors.Models.Responses
{
    public class MembershipSparseLookupResponse
    {

        // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://schemas.xmlsoap.org/soap/envelope/", IsNullable = false)]
        public partial class Envelope
        {

            private string headerField;

            private EnvelopeBody bodyField;

            /// <remarks/>
            public string Header
            {
                get
                {
                    return this.headerField;
                }
                set
                {
                    this.headerField = value;
                }
            }

            /// <remarks/>
            public EnvelopeBody Body
            {
                get
                {
                    return this.bodyField;
                }
                set
                {
                    this.bodyField = value;
                }
            }


            #region Error trapping

            /// <summary>
            /// Call status
            /// </summary>
            public string Status
            {
                get
                {
                    try
                    {
                        return Body.membershipSparseLookupResponse.status;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            public string StatusFull
            {
                get {
                    string output = "";
                    if (Status == null)
                        return "Error - Undefined"; ;

                    output = "Status: " + Status;

                    if(Errors != null)
                    {
                        for (int i = 0; i < Errors.Length; i++){
                            output += "\r\n";

                            if (!string.IsNullOrWhiteSpace(Errors[i].dateTime))
                                output += $"Date of Error     : {Errors[i].dateTime}\r\n";
                            if (!string.IsNullOrWhiteSpace(Errors[i].authenticatedUser))
                                output += $"Authenticated User: {Errors[i].authenticatedUser}\r\n";
                            if (!string.IsNullOrWhiteSpace(Errors[i].errorType))
                                output += $"Error Type        : {Errors[i].errorType}\r\n";
                            if (!string.IsNullOrWhiteSpace(Errors[i].message))
                                output += $"Message           : {Errors[i].message}\r\n";
                            if (!string.IsNullOrWhiteSpace(Errors[i].traceInfo))
                                output += $"Trace Info        : {Errors[i].traceInfo}\r\n";
                        }
                    }

                    return output;

                }



            }

            /// <summary>
            /// Errors
            /// </summary>
            public errorsError[] Errors
            {
                get
                {
                    try
                    {
                        return Body.membershipSparseLookupResponse.errors;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            #endregion

            #region Shortcut to Body > membershipSparseLookupResponse > enrollment objects

            /// <summary>
            /// Where to find documenation
            /// </summary>
            public static string Information
            {
                get
                {
                    return "Go to page #683 at this link: https://sps.promedica.org/it/pcsi/Shared%20Documents/Project%20Planning/HealthEdge%20Documentation/Health%20Rules%2020.2/Connector/20.2_HealthRules_Connector_Guide_Vol5_Extracts_and_Services.pdf" +
                        "\r\nand look for documentation in the DataLayer doc's folder at: http://gitlab01.promedica.org/phc/datalayer/tree/master/docs/datalayer/API/HealthRulesConnectors";
                }
            }

            /// <summary>
            /// Array of COB policies
            /// </summary>
            public cobPolicy[] COBPolicies
            {
                get
                {
                    try
                    {
                        return Body.membershipSparseLookupResponse.membership.cobPolicy;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// Members associated with this
            /// </summary>
            public membership Membership
            {
                get
                {
                    try
                    {
                        return Body.membershipSparseLookupResponse.membership;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            public string ResourceURI
            {
                get
                {
                    try
                    {
                        return Body.membershipSparseLookupResponse.membership.resourceURI;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            public string Version
            {
                get
                {
                    try
                    {
                        return Body.membershipSparseLookupResponse.membership.version;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }


            #endregion Shortcut to Body > enrollmentSparseLookupResponse > enrollment objects


        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public partial class EnvelopeBody
        {

            private membershipSparseLookupResponse membershipSparseLookupResponseField;

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.healthedge.com/connector/schema/enrollmentsparselookup")]
            public membershipSparseLookupResponse membershipSparseLookupResponse
            {
                get
                {
                    return this.membershipSparseLookupResponseField;
                }
                set
                {
                    this.membershipSparseLookupResponseField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.healthedge.com/connector/schema/enrollmentsparselookup")]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.healthedge.com/connector/schema/enrollmentsparselookup", IsNullable = false)]
        public partial class membershipSparseLookupResponse
        {

            private membership membershipField;

            private string statusField;

            private errorsError[] errorsField;

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.healthedge.com/connector/schema/membershipsparse")]
            public membership membership
            {
                get
                {
                    return this.membershipField;
                }
                set
                {
                    this.membershipField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public string status
            {
                get
                {
                    return this.statusField;
                }
                set
                {
                    this.statusField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlArrayAttribute(Namespace = "")]
            [System.Xml.Serialization.XmlArrayItemAttribute("error", IsNullable = false)]
            public errorsError[] errors
            {
                get
                {
                    return this.errorsField;
                }
                set
                {
                    this.errorsField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.healthedge.com/connector/schema/membershipsparse")]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.healthedge.com/connector/schema/membershipsparse", IsNullable = false)]
        public partial class membership
        {

            private string maintenanceTypeCodeField;

            private maintenanceReasonCode maintenanceReasonCodeField;

            private string maintenanceCommentField;

            private bool maintenanceOverrideField;

            private string maintenanceOverrideResultField;

            private string benefitStatusCodeField;

            private string membershipIdentifierField;

            private bool memberIsSubscriberField;

            private string hccIdentifierField;

            private string memberInputIDField;

            private string receiptDateField;

            private bool outOfServiceAreaField;

            private string smokingStatusField;

            private string isHandicappedField;

            private string waivePHIClaimViewRestrictionField;

            private string payeeHccIdField;

            private bool isMemberInHospiceField;

            private memberMatchData memberMatchDataField;

            private oldMemberData oldMemberDataField;

            private individual individualField;

            private physicalAddress physicalAddressField;

            private correspondenceAddress correspondenceAddressField;

            private communicationPreferences communicationPreferencesField;

            private specificCommunicationPreferences[] specificCommunicationPreferencesField;

            private otherIdNumberList otherIdNumberListField;

            private disabilityInfoForEligibility disabilityInfoForEligibilityField;

            private dependentVerification dependentVerificationField;

            private physicalCharacteristics physicalCharacteristicsField;

            private employmentInfo employmentInfoField;

            private dentalInfo dentalInfoField;

            private priorCoverageInfo priorCoverageInfoField;

            private preExistingConditionInfo preExistingConditionInfoField;

            private preExReduction preExReductionField;

            private otherResponsiblePersonInfo otherResponsiblePersonInfoField;

            private memberRepresentative memberRepresentativeField;

            private membershipUDTList membershipUDTListField;

            private informationAccessPermissionList informationAccessPermissionListField;

            private informationAccessRestrictionList informationAccessRestrictionListField;

            private phiAuthorizationList phiAuthorizationListField;

            private ccDirectiveList ccDirectiveListField;

            private subscriptionReference subscriptionReferenceField;

            private individualReference individualReferenceField;

            private relationshipToSubscriberDefinitionReference relationshipToSubscriberDefinitionReferenceField;

            private raceOrEthnicity raceOrEthnicityField;

            private vipReasonCode vipReasonCodeField;

            private healthStatusReasonCode healthStatusReasonCodeField;

            private informationSourceCode informationSourceCodeField;

            private attachmentSetReference attachmentSetReferenceField;

            private string planSelectionListModeField;

            private planSelection[] planSelectionField;

            private providerSelections providerSelectionsField;

            private reportingCategory[] reportingCategoryField;

            private string medicareHICNlistModeField;

            private medicareHICN[] medicareHICNField;

            private cobPolicy[] cobPolicyField;

            private headOfHousehold headOfHouseholdField;

            private memberComplianceProgram memberComplianceProgramField;

            private enrollmentMemberFactors enrollmentMemberFactorsField;

            private alternatePaymentContact alternatePaymentContactField;

            private string resourceURIField;

            private string versionField;

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public string maintenanceTypeCode
            {
                get
                {
                    return this.maintenanceTypeCodeField;
                }
                set
                {
                    this.maintenanceTypeCodeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public maintenanceReasonCode maintenanceReasonCode
            {
                get
                {
                    return this.maintenanceReasonCodeField;
                }
                set
                {
                    this.maintenanceReasonCodeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public string maintenanceComment
            {
                get
                {
                    return this.maintenanceCommentField;
                }
                set
                {
                    this.maintenanceCommentField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public bool maintenanceOverride
            {
                get
                {
                    return this.maintenanceOverrideField;
                }
                set
                {
                    this.maintenanceOverrideField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public string maintenanceOverrideResult
            {
                get
                {
                    return this.maintenanceOverrideResultField;
                }
                set
                {
                    this.maintenanceOverrideResultField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public string benefitStatusCode
            {
                get
                {
                    return this.benefitStatusCodeField;
                }
                set
                {
                    this.benefitStatusCodeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public string membershipIdentifier
            {
                get
                {
                    return this.membershipIdentifierField;
                }
                set
                {
                    this.membershipIdentifierField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public bool memberIsSubscriber
            {
                get
                {
                    return this.memberIsSubscriberField;
                }
                set
                {
                    this.memberIsSubscriberField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public string hccIdentifier
            {
                get
                {
                    return this.hccIdentifierField;
                }
                set
                {
                    this.hccIdentifierField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public string memberInputID
            {
                get
                {
                    return this.memberInputIDField;
                }
                set
                {
                    this.memberInputIDField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public string receiptDate
            {
                get
                {
                    return this.receiptDateField;
                }
                set
                {
                    this.receiptDateField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public bool outOfServiceArea
            {
                get
                {
                    return this.outOfServiceAreaField;
                }
                set
                {
                    this.outOfServiceAreaField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public string smokingStatus
            {
                get
                {
                    return this.smokingStatusField;
                }
                set
                {
                    this.smokingStatusField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public string isHandicapped
            {
                get
                {
                    return this.isHandicappedField;
                }
                set
                {
                    this.isHandicappedField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public string waivePHIClaimViewRestriction
            {
                get
                {
                    return this.waivePHIClaimViewRestrictionField;
                }
                set
                {
                    this.waivePHIClaimViewRestrictionField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public string payeeHccId
            {
                get
                {
                    return this.payeeHccIdField;
                }
                set
                {
                    this.payeeHccIdField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public bool isMemberInHospice
            {
                get
                {
                    return this.isMemberInHospiceField;
                }
                set
                {
                    this.isMemberInHospiceField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public memberMatchData memberMatchData
            {
                get
                {
                    return this.memberMatchDataField;
                }
                set
                {
                    this.memberMatchDataField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public oldMemberData oldMemberData
            {
                get
                {
                    return this.oldMemberDataField;
                }
                set
                {
                    this.oldMemberDataField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public individual individual
            {
                get
                {
                    return this.individualField;
                }
                set
                {
                    this.individualField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public physicalAddress physicalAddress
            {
                get
                {
                    return this.physicalAddressField;
                }
                set
                {
                    this.physicalAddressField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public correspondenceAddress correspondenceAddress
            {
                get
                {
                    return this.correspondenceAddressField;
                }
                set
                {
                    this.correspondenceAddressField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public communicationPreferences communicationPreferences
            {
                get
                {
                    return this.communicationPreferencesField;
                }
                set
                {
                    this.communicationPreferencesField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("specificCommunicationPreferences", Namespace = "")]
            public specificCommunicationPreferences[] specificCommunicationPreferences
            {
                get
                {
                    return this.specificCommunicationPreferencesField;
                }
                set
                {
                    this.specificCommunicationPreferencesField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public otherIdNumberList otherIdNumberList
            {
                get
                {
                    return this.otherIdNumberListField;
                }
                set
                {
                    this.otherIdNumberListField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public disabilityInfoForEligibility disabilityInfoForEligibility
            {
                get
                {
                    return this.disabilityInfoForEligibilityField;
                }
                set
                {
                    this.disabilityInfoForEligibilityField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public dependentVerification dependentVerification
            {
                get
                {
                    return this.dependentVerificationField;
                }
                set
                {
                    this.dependentVerificationField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public physicalCharacteristics physicalCharacteristics
            {
                get
                {
                    return this.physicalCharacteristicsField;
                }
                set
                {
                    this.physicalCharacteristicsField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public employmentInfo employmentInfo
            {
                get
                {
                    return this.employmentInfoField;
                }
                set
                {
                    this.employmentInfoField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public dentalInfo dentalInfo
            {
                get
                {
                    return this.dentalInfoField;
                }
                set
                {
                    this.dentalInfoField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public priorCoverageInfo priorCoverageInfo
            {
                get
                {
                    return this.priorCoverageInfoField;
                }
                set
                {
                    this.priorCoverageInfoField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public preExistingConditionInfo preExistingConditionInfo
            {
                get
                {
                    return this.preExistingConditionInfoField;
                }
                set
                {
                    this.preExistingConditionInfoField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public preExReduction preExReduction
            {
                get
                {
                    return this.preExReductionField;
                }
                set
                {
                    this.preExReductionField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public otherResponsiblePersonInfo otherResponsiblePersonInfo
            {
                get
                {
                    return this.otherResponsiblePersonInfoField;
                }
                set
                {
                    this.otherResponsiblePersonInfoField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public memberRepresentative memberRepresentative
            {
                get
                {
                    return this.memberRepresentativeField;
                }
                set
                {
                    this.memberRepresentativeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public membershipUDTList membershipUDTList
            {
                get
                {
                    return this.membershipUDTListField;
                }
                set
                {
                    this.membershipUDTListField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public informationAccessPermissionList informationAccessPermissionList
            {
                get
                {
                    return this.informationAccessPermissionListField;
                }
                set
                {
                    this.informationAccessPermissionListField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public informationAccessRestrictionList informationAccessRestrictionList
            {
                get
                {
                    return this.informationAccessRestrictionListField;
                }
                set
                {
                    this.informationAccessRestrictionListField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public phiAuthorizationList phiAuthorizationList
            {
                get
                {
                    return this.phiAuthorizationListField;
                }
                set
                {
                    this.phiAuthorizationListField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public ccDirectiveList ccDirectiveList
            {
                get
                {
                    return this.ccDirectiveListField;
                }
                set
                {
                    this.ccDirectiveListField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public subscriptionReference subscriptionReference
            {
                get
                {
                    return this.subscriptionReferenceField;
                }
                set
                {
                    this.subscriptionReferenceField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public individualReference individualReference
            {
                get
                {
                    return this.individualReferenceField;
                }
                set
                {
                    this.individualReferenceField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public relationshipToSubscriberDefinitionReference relationshipToSubscriberDefinitionReference
            {
                get
                {
                    return this.relationshipToSubscriberDefinitionReferenceField;
                }
                set
                {
                    this.relationshipToSubscriberDefinitionReferenceField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public raceOrEthnicity raceOrEthnicity
            {
                get
                {
                    return this.raceOrEthnicityField;
                }
                set
                {
                    this.raceOrEthnicityField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public vipReasonCode vipReasonCode
            {
                get
                {
                    return this.vipReasonCodeField;
                }
                set
                {
                    this.vipReasonCodeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public healthStatusReasonCode healthStatusReasonCode
            {
                get
                {
                    return this.healthStatusReasonCodeField;
                }
                set
                {
                    this.healthStatusReasonCodeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public informationSourceCode informationSourceCode
            {
                get
                {
                    return this.informationSourceCodeField;
                }
                set
                {
                    this.informationSourceCodeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public attachmentSetReference attachmentSetReference
            {
                get
                {
                    return this.attachmentSetReferenceField;
                }
                set
                {
                    this.attachmentSetReferenceField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public string planSelectionListMode
            {
                get
                {
                    return this.planSelectionListModeField;
                }
                set
                {
                    this.planSelectionListModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("planSelection", Namespace = "")]
            public planSelection[] planSelection
            {
                get
                {
                    return this.planSelectionField;
                }
                set
                {
                    this.planSelectionField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public providerSelections providerSelections
            {
                get
                {
                    return this.providerSelectionsField;
                }
                set
                {
                    this.providerSelectionsField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("reportingCategory", Namespace = "")]
            public reportingCategory[] reportingCategory
            {
                get
                {
                    return this.reportingCategoryField;
                }
                set
                {
                    this.reportingCategoryField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public string medicareHICNlistMode
            {
                get
                {
                    return this.medicareHICNlistModeField;
                }
                set
                {
                    this.medicareHICNlistModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("medicareHICN", Namespace = "")]
            public medicareHICN[] medicareHICN
            {
                get
                {
                    return this.medicareHICNField;
                }
                set
                {
                    this.medicareHICNField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("cobPolicy", Namespace = "")]
            public cobPolicy[] cobPolicy
            {
                get
                {
                    return this.cobPolicyField;
                }
                set
                {
                    this.cobPolicyField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public headOfHousehold headOfHousehold
            {
                get
                {
                    return this.headOfHouseholdField;
                }
                set
                {
                    this.headOfHouseholdField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public memberComplianceProgram memberComplianceProgram
            {
                get
                {
                    return this.memberComplianceProgramField;
                }
                set
                {
                    this.memberComplianceProgramField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public enrollmentMemberFactors enrollmentMemberFactors
            {
                get
                {
                    return this.enrollmentMemberFactorsField;
                }
                set
                {
                    this.enrollmentMemberFactorsField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public alternatePaymentContact alternatePaymentContact
            {
                get
                {
                    return this.alternatePaymentContactField;
                }
                set
                {
                    this.alternatePaymentContactField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string resourceURI
            {
                get
                {
                    return this.resourceURIField;
                }
                set
                {
                    this.resourceURIField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string version
            {
                get
                {
                    return this.versionField;
                }
                set
                {
                    this.versionField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class maintenanceReasonCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class memberMatchData
        {

            private string definitionNameField;

            private string idField;

            private string topAccountField;

            private string customString1Field;

            private string customString2Field;

            private string customString3Field;

            private string customString4Field;

            private string customInteger1Field;

            private string customInteger2Field;

            private string customInteger3Field;

            private string customInteger4Field;

            private string customDate1Field;

            private string customDate2Field;

            private string customDate3Field;

            private string customDate4Field;

            private string customDecimal1Field;

            private string customDecimal2Field;

            private string customDecimal3Field;

            private string customDecimal4Field;

            private memberMatchDataMember memberField;

            private memberMatchDataSubscriber subscriberField;

            private string asOfDateField;

            /// <remarks/>
            public string definitionName
            {
                get
                {
                    return this.definitionNameField;
                }
                set
                {
                    this.definitionNameField = value;
                }
            }

            /// <remarks/>
            public string id
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }

            /// <remarks/>
            public string topAccount
            {
                get
                {
                    return this.topAccountField;
                }
                set
                {
                    this.topAccountField = value;
                }
            }

            /// <remarks/>
            public string customString1
            {
                get
                {
                    return this.customString1Field;
                }
                set
                {
                    this.customString1Field = value;
                }
            }

            /// <remarks/>
            public string customString2
            {
                get
                {
                    return this.customString2Field;
                }
                set
                {
                    this.customString2Field = value;
                }
            }

            /// <remarks/>
            public string customString3
            {
                get
                {
                    return this.customString3Field;
                }
                set
                {
                    this.customString3Field = value;
                }
            }

            /// <remarks/>
            public string customString4
            {
                get
                {
                    return this.customString4Field;
                }
                set
                {
                    this.customString4Field = value;
                }
            }

            /// <remarks/>
            public string customInteger1
            {
                get
                {
                    return this.customInteger1Field;
                }
                set
                {
                    this.customInteger1Field = value;
                }
            }

            /// <remarks/>
            public string customInteger2
            {
                get
                {
                    return this.customInteger2Field;
                }
                set
                {
                    this.customInteger2Field = value;
                }
            }

            /// <remarks/>
            public string customInteger3
            {
                get
                {
                    return this.customInteger3Field;
                }
                set
                {
                    this.customInteger3Field = value;
                }
            }

            /// <remarks/>
            public string customInteger4
            {
                get
                {
                    return this.customInteger4Field;
                }
                set
                {
                    this.customInteger4Field = value;
                }
            }

            /// <remarks/>
            public string customDate1
            {
                get
                {
                    return this.customDate1Field;
                }
                set
                {
                    this.customDate1Field = value;
                }
            }

            /// <remarks/>
            public string customDate2
            {
                get
                {
                    return this.customDate2Field;
                }
                set
                {
                    this.customDate2Field = value;
                }
            }

            /// <remarks/>
            public string customDate3
            {
                get
                {
                    return this.customDate3Field;
                }
                set
                {
                    this.customDate3Field = value;
                }
            }

            /// <remarks/>
            public string customDate4
            {
                get
                {
                    return this.customDate4Field;
                }
                set
                {
                    this.customDate4Field = value;
                }
            }

            /// <remarks/>
            public string customDecimal1
            {
                get
                {
                    return this.customDecimal1Field;
                }
                set
                {
                    this.customDecimal1Field = value;
                }
            }

            /// <remarks/>
            public string customDecimal2
            {
                get
                {
                    return this.customDecimal2Field;
                }
                set
                {
                    this.customDecimal2Field = value;
                }
            }

            /// <remarks/>
            public string customDecimal3
            {
                get
                {
                    return this.customDecimal3Field;
                }
                set
                {
                    this.customDecimal3Field = value;
                }
            }

            /// <remarks/>
            public string customDecimal4
            {
                get
                {
                    return this.customDecimal4Field;
                }
                set
                {
                    this.customDecimal4Field = value;
                }
            }

            /// <remarks/>
            public memberMatchDataMember member
            {
                get
                {
                    return this.memberField;
                }
                set
                {
                    this.memberField = value;
                }
            }

            /// <remarks/>
            public memberMatchDataSubscriber subscriber
            {
                get
                {
                    return this.subscriberField;
                }
                set
                {
                    this.subscriberField = value;
                }
            }

            /// <remarks/>
            public string asOfDate
            {
                get
                {
                    return this.asOfDateField;
                }
                set
                {
                    this.asOfDateField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMatchDataMember
        {

            private string firstNameField;

            private string lastNameField;

            private string fullNameField;

            private string genderField;

            private string dateOfBirthField;

            private string taxIdField;

            private memberMatchDataMemberAddress addressField;

            /// <remarks/>
            public string firstName
            {
                get
                {
                    return this.firstNameField;
                }
                set
                {
                    this.firstNameField = value;
                }
            }

            /// <remarks/>
            public string lastName
            {
                get
                {
                    return this.lastNameField;
                }
                set
                {
                    this.lastNameField = value;
                }
            }

            /// <remarks/>
            public string fullName
            {
                get
                {
                    return this.fullNameField;
                }
                set
                {
                    this.fullNameField = value;
                }
            }

            /// <remarks/>
            public string gender
            {
                get
                {
                    return this.genderField;
                }
                set
                {
                    this.genderField = value;
                }
            }

            /// <remarks/>
            public string dateOfBirth
            {
                get
                {
                    return this.dateOfBirthField;
                }
                set
                {
                    this.dateOfBirthField = value;
                }
            }

            /// <remarks/>
            public string taxId
            {
                get
                {
                    return this.taxIdField;
                }
                set
                {
                    this.taxIdField = value;
                }
            }

            /// <remarks/>
            public memberMatchDataMemberAddress address
            {
                get
                {
                    return this.addressField;
                }
                set
                {
                    this.addressField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMatchDataMemberAddress
        {

            private string addressField;

            private string cityNameField;

            private string stateCodeField;

            private string zipCodeField;

            private string zipExtensionCodeField;

            private string countryCodeField;

            /// <remarks/>
            public string address
            {
                get
                {
                    return this.addressField;
                }
                set
                {
                    this.addressField = value;
                }
            }

            /// <remarks/>
            public string cityName
            {
                get
                {
                    return this.cityNameField;
                }
                set
                {
                    this.cityNameField = value;
                }
            }

            /// <remarks/>
            public string stateCode
            {
                get
                {
                    return this.stateCodeField;
                }
                set
                {
                    this.stateCodeField = value;
                }
            }

            /// <remarks/>
            public string zipCode
            {
                get
                {
                    return this.zipCodeField;
                }
                set
                {
                    this.zipCodeField = value;
                }
            }

            /// <remarks/>
            public string zipExtensionCode
            {
                get
                {
                    return this.zipExtensionCodeField;
                }
                set
                {
                    this.zipExtensionCodeField = value;
                }
            }

            /// <remarks/>
            public string countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMatchDataSubscriber
        {

            private string firstNameField;

            private string lastNameField;

            private string fullNameField;

            private string genderField;

            private string dateOfBirthField;

            private string taxIdField;

            private memberMatchDataSubscriberAddress addressField;

            /// <remarks/>
            public string firstName
            {
                get
                {
                    return this.firstNameField;
                }
                set
                {
                    this.firstNameField = value;
                }
            }

            /// <remarks/>
            public string lastName
            {
                get
                {
                    return this.lastNameField;
                }
                set
                {
                    this.lastNameField = value;
                }
            }

            /// <remarks/>
            public string fullName
            {
                get
                {
                    return this.fullNameField;
                }
                set
                {
                    this.fullNameField = value;
                }
            }

            /// <remarks/>
            public string gender
            {
                get
                {
                    return this.genderField;
                }
                set
                {
                    this.genderField = value;
                }
            }

            /// <remarks/>
            public string dateOfBirth
            {
                get
                {
                    return this.dateOfBirthField;
                }
                set
                {
                    this.dateOfBirthField = value;
                }
            }

            /// <remarks/>
            public string taxId
            {
                get
                {
                    return this.taxIdField;
                }
                set
                {
                    this.taxIdField = value;
                }
            }

            /// <remarks/>
            public memberMatchDataSubscriberAddress address
            {
                get
                {
                    return this.addressField;
                }
                set
                {
                    this.addressField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMatchDataSubscriberAddress
        {

            private string addressField;

            private string cityNameField;

            private string stateCodeField;

            private string zipCodeField;

            private string zipExtensionCodeField;

            private string countryCodeField;

            /// <remarks/>
            public string address
            {
                get
                {
                    return this.addressField;
                }
                set
                {
                    this.addressField = value;
                }
            }

            /// <remarks/>
            public string cityName
            {
                get
                {
                    return this.cityNameField;
                }
                set
                {
                    this.cityNameField = value;
                }
            }

            /// <remarks/>
            public string stateCode
            {
                get
                {
                    return this.stateCodeField;
                }
                set
                {
                    this.stateCodeField = value;
                }
            }

            /// <remarks/>
            public string zipCode
            {
                get
                {
                    return this.zipCodeField;
                }
                set
                {
                    this.zipCodeField = value;
                }
            }

            /// <remarks/>
            public string zipExtensionCode
            {
                get
                {
                    return this.zipExtensionCodeField;
                }
                set
                {
                    this.zipExtensionCodeField = value;
                }
            }

            /// <remarks/>
            public string countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class oldMemberData
        {

            private oldMemberDataPrimaryName primaryNameField;

            private string hccIdentifierField;

            private string taxIdentificationNumberField;

            private string genderCodeField;

            private string maritalStatusCodeField;

            private string birthDateField;

            private oldMemberDataImmigrationStatusCode immigrationStatusCodeField;

            private oldMemberDataAddress addressField;

            /// <remarks/>
            public oldMemberDataPrimaryName primaryName
            {
                get
                {
                    return this.primaryNameField;
                }
                set
                {
                    this.primaryNameField = value;
                }
            }

            /// <remarks/>
            public string hccIdentifier
            {
                get
                {
                    return this.hccIdentifierField;
                }
                set
                {
                    this.hccIdentifierField = value;
                }
            }

            /// <remarks/>
            public string taxIdentificationNumber
            {
                get
                {
                    return this.taxIdentificationNumberField;
                }
                set
                {
                    this.taxIdentificationNumberField = value;
                }
            }

            /// <remarks/>
            public string genderCode
            {
                get
                {
                    return this.genderCodeField;
                }
                set
                {
                    this.genderCodeField = value;
                }
            }

            /// <remarks/>
            public string maritalStatusCode
            {
                get
                {
                    return this.maritalStatusCodeField;
                }
                set
                {
                    this.maritalStatusCodeField = value;
                }
            }

            /// <remarks/>
            public string birthDate
            {
                get
                {
                    return this.birthDateField;
                }
                set
                {
                    this.birthDateField = value;
                }
            }

            /// <remarks/>
            public oldMemberDataImmigrationStatusCode immigrationStatusCode
            {
                get
                {
                    return this.immigrationStatusCodeField;
                }
                set
                {
                    this.immigrationStatusCodeField = value;
                }
            }

            /// <remarks/>
            public oldMemberDataAddress address
            {
                get
                {
                    return this.addressField;
                }
                set
                {
                    this.addressField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class oldMemberDataPrimaryName
        {

            private string lastNameField;

            private string firstNameField;

            private string middleNameField;

            private string namePrefixField;

            private string nameSuffixField;

            private oldMemberDataPrimaryNameNameSuffixList nameSuffixListField;

            private oldMemberDataPrimaryNameCredentialSuffixList credentialSuffixListField;

            /// <remarks/>
            public string lastName
            {
                get
                {
                    return this.lastNameField;
                }
                set
                {
                    this.lastNameField = value;
                }
            }

            /// <remarks/>
            public string firstName
            {
                get
                {
                    return this.firstNameField;
                }
                set
                {
                    this.firstNameField = value;
                }
            }

            /// <remarks/>
            public string middleName
            {
                get
                {
                    return this.middleNameField;
                }
                set
                {
                    this.middleNameField = value;
                }
            }

            /// <remarks/>
            public string namePrefix
            {
                get
                {
                    return this.namePrefixField;
                }
                set
                {
                    this.namePrefixField = value;
                }
            }

            /// <remarks/>
            public string nameSuffix
            {
                get
                {
                    return this.nameSuffixField;
                }
                set
                {
                    this.nameSuffixField = value;
                }
            }

            /// <remarks/>
            public oldMemberDataPrimaryNameNameSuffixList nameSuffixList
            {
                get
                {
                    return this.nameSuffixListField;
                }
                set
                {
                    this.nameSuffixListField = value;
                }
            }

            /// <remarks/>
            public oldMemberDataPrimaryNameCredentialSuffixList credentialSuffixList
            {
                get
                {
                    return this.credentialSuffixListField;
                }
                set
                {
                    this.credentialSuffixListField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class oldMemberDataPrimaryNameNameSuffixList
        {

            private string listModeField;

            private string[] suffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("suffix")]
            public string[] suffix
            {
                get
                {
                    return this.suffixField;
                }
                set
                {
                    this.suffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class oldMemberDataPrimaryNameCredentialSuffixList
        {

            private string listModeField;

            private oldMemberDataPrimaryNameCredentialSuffixListCredentialSuffix[] credentialSuffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("CredentialSuffix")]
            public oldMemberDataPrimaryNameCredentialSuffixListCredentialSuffix[] CredentialSuffix
            {
                get
                {
                    return this.credentialSuffixField;
                }
                set
                {
                    this.credentialSuffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class oldMemberDataPrimaryNameCredentialSuffixListCredentialSuffix
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class oldMemberDataImmigrationStatusCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class oldMemberDataAddress
        {

            private string addressField;

            private string address2Field;

            private string address3Field;

            private string stateCodeField;

            private string zipCodeField;

            private string zipExtensionCodeField;

            private string cityNameField;

            private string countyCodeField;

            private oldMemberDataAddressCountryCode countryCodeField;

            /// <remarks/>
            public string address
            {
                get
                {
                    return this.addressField;
                }
                set
                {
                    this.addressField = value;
                }
            }

            /// <remarks/>
            public string address2
            {
                get
                {
                    return this.address2Field;
                }
                set
                {
                    this.address2Field = value;
                }
            }

            /// <remarks/>
            public string address3
            {
                get
                {
                    return this.address3Field;
                }
                set
                {
                    this.address3Field = value;
                }
            }

            /// <remarks/>
            public string stateCode
            {
                get
                {
                    return this.stateCodeField;
                }
                set
                {
                    this.stateCodeField = value;
                }
            }

            /// <remarks/>
            public string zipCode
            {
                get
                {
                    return this.zipCodeField;
                }
                set
                {
                    this.zipCodeField = value;
                }
            }

            /// <remarks/>
            public string zipExtensionCode
            {
                get
                {
                    return this.zipExtensionCodeField;
                }
                set
                {
                    this.zipExtensionCodeField = value;
                }
            }

            /// <remarks/>
            public string cityName
            {
                get
                {
                    return this.cityNameField;
                }
                set
                {
                    this.cityNameField = value;
                }
            }

            /// <remarks/>
            public string countyCode
            {
                get
                {
                    return this.countyCodeField;
                }
                set
                {
                    this.countyCodeField = value;
                }
            }

            /// <remarks/>
            public oldMemberDataAddressCountryCode countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class oldMemberDataAddressCountryCode
        {

            private string countryCodeField;

            /// <remarks/>
            public string countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class individual
        {

            private string taxIdentificationNumberField;

            private string genderCodeField;

            private string maritalStatusCodeField;

            private string birthDateField;

            private string birthPlaceStateCodeField;

            private string birthPlaceCountyCodeField;

            private individualBirthPlaceCountryCode birthPlaceCountryCodeField;

            private string dateOfDeathField;

            private individualPrimaryName primaryNameField;

            private individualOtherNames otherNamesField;

            private individualLanguages languagesField;

            private individualImmigrationStatusCode immigrationStatusCodeField;

            /// <remarks/>
            public string taxIdentificationNumber
            {
                get
                {
                    return this.taxIdentificationNumberField;
                }
                set
                {
                    this.taxIdentificationNumberField = value;
                }
            }

            /// <remarks/>
            public string genderCode
            {
                get
                {
                    return this.genderCodeField;
                }
                set
                {
                    this.genderCodeField = value;
                }
            }

            /// <remarks/>
            public string maritalStatusCode
            {
                get
                {
                    return this.maritalStatusCodeField;
                }
                set
                {
                    this.maritalStatusCodeField = value;
                }
            }

            /// <remarks/>
            public string birthDate
            {
                get
                {
                    return this.birthDateField;
                }
                set
                {
                    this.birthDateField = value;
                }
            }

            /// <remarks/>
            public string birthPlaceStateCode
            {
                get
                {
                    return this.birthPlaceStateCodeField;
                }
                set
                {
                    this.birthPlaceStateCodeField = value;
                }
            }

            /// <remarks/>
            public string birthPlaceCountyCode
            {
                get
                {
                    return this.birthPlaceCountyCodeField;
                }
                set
                {
                    this.birthPlaceCountyCodeField = value;
                }
            }

            /// <remarks/>
            public individualBirthPlaceCountryCode birthPlaceCountryCode
            {
                get
                {
                    return this.birthPlaceCountryCodeField;
                }
                set
                {
                    this.birthPlaceCountryCodeField = value;
                }
            }

            /// <remarks/>
            public string dateOfDeath
            {
                get
                {
                    return this.dateOfDeathField;
                }
                set
                {
                    this.dateOfDeathField = value;
                }
            }

            /// <remarks/>
            public individualPrimaryName primaryName
            {
                get
                {
                    return this.primaryNameField;
                }
                set
                {
                    this.primaryNameField = value;
                }
            }

            /// <remarks/>
            public individualOtherNames otherNames
            {
                get
                {
                    return this.otherNamesField;
                }
                set
                {
                    this.otherNamesField = value;
                }
            }

            /// <remarks/>
            public individualLanguages languages
            {
                get
                {
                    return this.languagesField;
                }
                set
                {
                    this.languagesField = value;
                }
            }

            /// <remarks/>
            public individualImmigrationStatusCode immigrationStatusCode
            {
                get
                {
                    return this.immigrationStatusCodeField;
                }
                set
                {
                    this.immigrationStatusCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class individualBirthPlaceCountryCode
        {

            private string countryCodeField;

            /// <remarks/>
            public string countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class individualPrimaryName
        {

            private string lastNameField;

            private string firstNameField;

            private string middleNameField;

            private string namePrefixField;

            private string nameSuffixField;

            private individualPrimaryNameNameSuffixList nameSuffixListField;

            private individualPrimaryNameCredentialSuffixList credentialSuffixListField;

            /// <remarks/>
            public string lastName
            {
                get
                {
                    return this.lastNameField;
                }
                set
                {
                    this.lastNameField = value;
                }
            }

            /// <remarks/>
            public string firstName
            {
                get
                {
                    return this.firstNameField;
                }
                set
                {
                    this.firstNameField = value;
                }
            }

            /// <remarks/>
            public string middleName
            {
                get
                {
                    return this.middleNameField;
                }
                set
                {
                    this.middleNameField = value;
                }
            }

            /// <remarks/>
            public string namePrefix
            {
                get
                {
                    return this.namePrefixField;
                }
                set
                {
                    this.namePrefixField = value;
                }
            }

            /// <remarks/>
            public string nameSuffix
            {
                get
                {
                    return this.nameSuffixField;
                }
                set
                {
                    this.nameSuffixField = value;
                }
            }

            /// <remarks/>
            public individualPrimaryNameNameSuffixList nameSuffixList
            {
                get
                {
                    return this.nameSuffixListField;
                }
                set
                {
                    this.nameSuffixListField = value;
                }
            }

            /// <remarks/>
            public individualPrimaryNameCredentialSuffixList credentialSuffixList
            {
                get
                {
                    return this.credentialSuffixListField;
                }
                set
                {
                    this.credentialSuffixListField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class individualPrimaryNameNameSuffixList
        {

            private string listModeField;

            private string[] suffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("suffix")]
            public string[] suffix
            {
                get
                {
                    return this.suffixField;
                }
                set
                {
                    this.suffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class individualPrimaryNameCredentialSuffixList
        {

            private string listModeField;

            private individualPrimaryNameCredentialSuffixListCredentialSuffix[] credentialSuffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("CredentialSuffix")]
            public individualPrimaryNameCredentialSuffixListCredentialSuffix[] CredentialSuffix
            {
                get
                {
                    return this.credentialSuffixField;
                }
                set
                {
                    this.credentialSuffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class individualPrimaryNameCredentialSuffixListCredentialSuffix
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class individualOtherNames
        {

            private string listModeField;

            private individualOtherNamesOtherName[] otherNameField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("otherName")]
            public individualOtherNamesOtherName[] otherName
            {
                get
                {
                    return this.otherNameField;
                }
                set
                {
                    this.otherNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class individualOtherNamesOtherName
        {

            private individualOtherNamesOtherNameOtherNameInfo otherNameInfoField;

            private individualOtherNamesOtherNameOtherNameTypeCode otherNameTypeCodeField;

            /// <remarks/>
            public individualOtherNamesOtherNameOtherNameInfo otherNameInfo
            {
                get
                {
                    return this.otherNameInfoField;
                }
                set
                {
                    this.otherNameInfoField = value;
                }
            }

            /// <remarks/>
            public individualOtherNamesOtherNameOtherNameTypeCode otherNameTypeCode
            {
                get
                {
                    return this.otherNameTypeCodeField;
                }
                set
                {
                    this.otherNameTypeCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class individualOtherNamesOtherNameOtherNameInfo
        {

            private string lastNameField;

            private string firstNameField;

            private string middleNameField;

            private string namePrefixField;

            private string nameSuffixField;

            private individualOtherNamesOtherNameOtherNameInfoNameSuffixList nameSuffixListField;

            private individualOtherNamesOtherNameOtherNameInfoCredentialSuffixList credentialSuffixListField;

            /// <remarks/>
            public string lastName
            {
                get
                {
                    return this.lastNameField;
                }
                set
                {
                    this.lastNameField = value;
                }
            }

            /// <remarks/>
            public string firstName
            {
                get
                {
                    return this.firstNameField;
                }
                set
                {
                    this.firstNameField = value;
                }
            }

            /// <remarks/>
            public string middleName
            {
                get
                {
                    return this.middleNameField;
                }
                set
                {
                    this.middleNameField = value;
                }
            }

            /// <remarks/>
            public string namePrefix
            {
                get
                {
                    return this.namePrefixField;
                }
                set
                {
                    this.namePrefixField = value;
                }
            }

            /// <remarks/>
            public string nameSuffix
            {
                get
                {
                    return this.nameSuffixField;
                }
                set
                {
                    this.nameSuffixField = value;
                }
            }

            /// <remarks/>
            public individualOtherNamesOtherNameOtherNameInfoNameSuffixList nameSuffixList
            {
                get
                {
                    return this.nameSuffixListField;
                }
                set
                {
                    this.nameSuffixListField = value;
                }
            }

            /// <remarks/>
            public individualOtherNamesOtherNameOtherNameInfoCredentialSuffixList credentialSuffixList
            {
                get
                {
                    return this.credentialSuffixListField;
                }
                set
                {
                    this.credentialSuffixListField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class individualOtherNamesOtherNameOtherNameInfoNameSuffixList
        {

            private string listModeField;

            private string[] suffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("suffix")]
            public string[] suffix
            {
                get
                {
                    return this.suffixField;
                }
                set
                {
                    this.suffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class individualOtherNamesOtherNameOtherNameInfoCredentialSuffixList
        {

            private string listModeField;

            private individualOtherNamesOtherNameOtherNameInfoCredentialSuffixListCredentialSuffix[] credentialSuffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("CredentialSuffix")]
            public individualOtherNamesOtherNameOtherNameInfoCredentialSuffixListCredentialSuffix[] CredentialSuffix
            {
                get
                {
                    return this.credentialSuffixField;
                }
                set
                {
                    this.credentialSuffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class individualOtherNamesOtherNameOtherNameInfoCredentialSuffixListCredentialSuffix
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class individualOtherNamesOtherNameOtherNameTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class individualLanguages
        {

            private string listModeField;

            private individualLanguagesLanguage[] languageField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("language")]
            public individualLanguagesLanguage[] language
            {
                get
                {
                    return this.languageField;
                }
                set
                {
                    this.languageField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class individualLanguagesLanguage
        {

            private string primaryLanguageField;

            private string nativeLanguageField;

            private individualLanguagesLanguageLanguageDomainCode languageDomainCodeField;

            /// <remarks/>
            public string primaryLanguage
            {
                get
                {
                    return this.primaryLanguageField;
                }
                set
                {
                    this.primaryLanguageField = value;
                }
            }

            /// <remarks/>
            public string nativeLanguage
            {
                get
                {
                    return this.nativeLanguageField;
                }
                set
                {
                    this.nativeLanguageField = value;
                }
            }

            /// <remarks/>
            public individualLanguagesLanguageLanguageDomainCode languageDomainCode
            {
                get
                {
                    return this.languageDomainCodeField;
                }
                set
                {
                    this.languageDomainCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class individualLanguagesLanguageLanguageDomainCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class individualImmigrationStatusCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class physicalAddress
        {

            private string listModeField;

            private physicalAddressMemberPhysicalAddress[] memberPhysicalAddressField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("memberPhysicalAddress")]
            public physicalAddressMemberPhysicalAddress[] memberPhysicalAddress
            {
                get
                {
                    return this.memberPhysicalAddressField;
                }
                set
                {
                    this.memberPhysicalAddressField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class physicalAddressMemberPhysicalAddress
        {

            private string emailAddressField;

            private physicalAddressMemberPhysicalAddressAddressInfo addressInfoField;

            private physicalAddressMemberPhysicalAddressAddressTypeCode addressTypeCodeField;

            /// <remarks/>
            public string emailAddress
            {
                get
                {
                    return this.emailAddressField;
                }
                set
                {
                    this.emailAddressField = value;
                }
            }

            /// <remarks/>
            public physicalAddressMemberPhysicalAddressAddressInfo addressInfo
            {
                get
                {
                    return this.addressInfoField;
                }
                set
                {
                    this.addressInfoField = value;
                }
            }

            /// <remarks/>
            public physicalAddressMemberPhysicalAddressAddressTypeCode addressTypeCode
            {
                get
                {
                    return this.addressTypeCodeField;
                }
                set
                {
                    this.addressTypeCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class physicalAddressMemberPhysicalAddressAddressInfo
        {

            private physicalAddressMemberPhysicalAddressAddressInfoPostalAddress postalAddressField;

            private physicalAddressMemberPhysicalAddressAddressInfoAddressPhoneList addressPhoneListField;

            /// <remarks/>
            public physicalAddressMemberPhysicalAddressAddressInfoPostalAddress postalAddress
            {
                get
                {
                    return this.postalAddressField;
                }
                set
                {
                    this.postalAddressField = value;
                }
            }

            /// <remarks/>
            public physicalAddressMemberPhysicalAddressAddressInfoAddressPhoneList addressPhoneList
            {
                get
                {
                    return this.addressPhoneListField;
                }
                set
                {
                    this.addressPhoneListField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class physicalAddressMemberPhysicalAddressAddressInfoPostalAddress
        {

            private string addressField;

            private string address2Field;

            private string address3Field;

            private string stateCodeField;

            private string zipCodeField;

            private string zipExtensionCodeField;

            private string cityNameField;

            private string countyCodeField;

            private physicalAddressMemberPhysicalAddressAddressInfoPostalAddressCountryCode countryCodeField;

            private string longitudeField;

            private string latitudeField;

            private string ignoreAddressCheckField;

            private string undeliverableAddressField;

            /// <remarks/>
            public string address
            {
                get
                {
                    return this.addressField;
                }
                set
                {
                    this.addressField = value;
                }
            }

            /// <remarks/>
            public string address2
            {
                get
                {
                    return this.address2Field;
                }
                set
                {
                    this.address2Field = value;
                }
            }

            /// <remarks/>
            public string address3
            {
                get
                {
                    return this.address3Field;
                }
                set
                {
                    this.address3Field = value;
                }
            }

            /// <remarks/>
            public string stateCode
            {
                get
                {
                    return this.stateCodeField;
                }
                set
                {
                    this.stateCodeField = value;
                }
            }

            /// <remarks/>
            public string zipCode
            {
                get
                {
                    return this.zipCodeField;
                }
                set
                {
                    this.zipCodeField = value;
                }
            }

            /// <remarks/>
            public string zipExtensionCode
            {
                get
                {
                    return this.zipExtensionCodeField;
                }
                set
                {
                    this.zipExtensionCodeField = value;
                }
            }

            /// <remarks/>
            public string cityName
            {
                get
                {
                    return this.cityNameField;
                }
                set
                {
                    this.cityNameField = value;
                }
            }

            /// <remarks/>
            public string countyCode
            {
                get
                {
                    return this.countyCodeField;
                }
                set
                {
                    this.countyCodeField = value;
                }
            }

            /// <remarks/>
            public physicalAddressMemberPhysicalAddressAddressInfoPostalAddressCountryCode countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }

            /// <remarks/>
            public string longitude
            {
                get
                {
                    return this.longitudeField;
                }
                set
                {
                    this.longitudeField = value;
                }
            }

            /// <remarks/>
            public string latitude
            {
                get
                {
                    return this.latitudeField;
                }
                set
                {
                    this.latitudeField = value;
                }
            }

            /// <remarks/>
            public string ignoreAddressCheck
            {
                get
                {
                    return this.ignoreAddressCheckField;
                }
                set
                {
                    this.ignoreAddressCheckField = value;
                }
            }

            /// <remarks/>
            public string undeliverableAddress
            {
                get
                {
                    return this.undeliverableAddressField;
                }
                set
                {
                    this.undeliverableAddressField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class physicalAddressMemberPhysicalAddressAddressInfoPostalAddressCountryCode
        {

            private string countryCodeField;

            /// <remarks/>
            public string countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class physicalAddressMemberPhysicalAddressAddressInfoAddressPhoneList
        {

            private string listModeField;

            private physicalAddressMemberPhysicalAddressAddressInfoAddressPhoneListTelephoneNumber[] telephoneNumberField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("telephoneNumber")]
            public physicalAddressMemberPhysicalAddressAddressInfoAddressPhoneListTelephoneNumber[] telephoneNumber
            {
                get
                {
                    return this.telephoneNumberField;
                }
                set
                {
                    this.telephoneNumberField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class physicalAddressMemberPhysicalAddressAddressInfoAddressPhoneListTelephoneNumber
        {

            private string phoneCountryCodeField;

            private string phoneAreaCodeField;

            private string phoneNumberField;

            private string phoneExtensionNumberField;

            private physicalAddressMemberPhysicalAddressAddressInfoAddressPhoneListTelephoneNumberIndividualPhoneTypeCode individualPhoneTypeCodeField;

            private physicalAddressMemberPhysicalAddressAddressInfoAddressPhoneListTelephoneNumberOrganizationPhoneTypeCode organizationPhoneTypeCodeField;

            /// <remarks/>
            public string phoneCountryCode
            {
                get
                {
                    return this.phoneCountryCodeField;
                }
                set
                {
                    this.phoneCountryCodeField = value;
                }
            }

            /// <remarks/>
            public string phoneAreaCode
            {
                get
                {
                    return this.phoneAreaCodeField;
                }
                set
                {
                    this.phoneAreaCodeField = value;
                }
            }

            /// <remarks/>
            public string phoneNumber
            {
                get
                {
                    return this.phoneNumberField;
                }
                set
                {
                    this.phoneNumberField = value;
                }
            }

            /// <remarks/>
            public string phoneExtensionNumber
            {
                get
                {
                    return this.phoneExtensionNumberField;
                }
                set
                {
                    this.phoneExtensionNumberField = value;
                }
            }

            /// <remarks/>
            public physicalAddressMemberPhysicalAddressAddressInfoAddressPhoneListTelephoneNumberIndividualPhoneTypeCode individualPhoneTypeCode
            {
                get
                {
                    return this.individualPhoneTypeCodeField;
                }
                set
                {
                    this.individualPhoneTypeCodeField = value;
                }
            }

            /// <remarks/>
            public physicalAddressMemberPhysicalAddressAddressInfoAddressPhoneListTelephoneNumberOrganizationPhoneTypeCode organizationPhoneTypeCode
            {
                get
                {
                    return this.organizationPhoneTypeCodeField;
                }
                set
                {
                    this.organizationPhoneTypeCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class physicalAddressMemberPhysicalAddressAddressInfoAddressPhoneListTelephoneNumberIndividualPhoneTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class physicalAddressMemberPhysicalAddressAddressInfoAddressPhoneListTelephoneNumberOrganizationPhoneTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class physicalAddressMemberPhysicalAddressAddressTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class correspondenceAddress
        {

            private string emailAddressField;

            private correspondenceAddressPostalAddress postalAddressField;

            /// <remarks/>
            public string emailAddress
            {
                get
                {
                    return this.emailAddressField;
                }
                set
                {
                    this.emailAddressField = value;
                }
            }

            /// <remarks/>
            public correspondenceAddressPostalAddress postalAddress
            {
                get
                {
                    return this.postalAddressField;
                }
                set
                {
                    this.postalAddressField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class correspondenceAddressPostalAddress
        {

            private string addressField;

            private string address2Field;

            private string address3Field;

            private string stateCodeField;

            private string zipCodeField;

            private string zipExtensionCodeField;

            private string cityNameField;

            private string countyCodeField;

            private correspondenceAddressPostalAddressCountryCode countryCodeField;

            private string longitudeField;

            private string latitudeField;

            private string ignoreAddressCheckField;

            private string undeliverableAddressField;

            /// <remarks/>
            public string address
            {
                get
                {
                    return this.addressField;
                }
                set
                {
                    this.addressField = value;
                }
            }

            /// <remarks/>
            public string address2
            {
                get
                {
                    return this.address2Field;
                }
                set
                {
                    this.address2Field = value;
                }
            }

            /// <remarks/>
            public string address3
            {
                get
                {
                    return this.address3Field;
                }
                set
                {
                    this.address3Field = value;
                }
            }

            /// <remarks/>
            public string stateCode
            {
                get
                {
                    return this.stateCodeField;
                }
                set
                {
                    this.stateCodeField = value;
                }
            }

            /// <remarks/>
            public string zipCode
            {
                get
                {
                    return this.zipCodeField;
                }
                set
                {
                    this.zipCodeField = value;
                }
            }

            /// <remarks/>
            public string zipExtensionCode
            {
                get
                {
                    return this.zipExtensionCodeField;
                }
                set
                {
                    this.zipExtensionCodeField = value;
                }
            }

            /// <remarks/>
            public string cityName
            {
                get
                {
                    return this.cityNameField;
                }
                set
                {
                    this.cityNameField = value;
                }
            }

            /// <remarks/>
            public string countyCode
            {
                get
                {
                    return this.countyCodeField;
                }
                set
                {
                    this.countyCodeField = value;
                }
            }

            /// <remarks/>
            public correspondenceAddressPostalAddressCountryCode countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }

            /// <remarks/>
            public string longitude
            {
                get
                {
                    return this.longitudeField;
                }
                set
                {
                    this.longitudeField = value;
                }
            }

            /// <remarks/>
            public string latitude
            {
                get
                {
                    return this.latitudeField;
                }
                set
                {
                    this.latitudeField = value;
                }
            }

            /// <remarks/>
            public string ignoreAddressCheck
            {
                get
                {
                    return this.ignoreAddressCheckField;
                }
                set
                {
                    this.ignoreAddressCheckField = value;
                }
            }

            /// <remarks/>
            public string undeliverableAddress
            {
                get
                {
                    return this.undeliverableAddressField;
                }
                set
                {
                    this.undeliverableAddressField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class correspondenceAddressPostalAddressCountryCode
        {

            private string countryCodeField;

            /// <remarks/>
            public string countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class communicationPreferences
        {

            private string contactMethodField;

            private communicationPreferencesDocumentDeliveryMethodCode documentDeliveryMethodCodeField;

            private communicationPreferencesEmailFormatCode emailFormatCodeField;

            /// <remarks/>
            public string contactMethod
            {
                get
                {
                    return this.contactMethodField;
                }
                set
                {
                    this.contactMethodField = value;
                }
            }

            /// <remarks/>
            public communicationPreferencesDocumentDeliveryMethodCode documentDeliveryMethodCode
            {
                get
                {
                    return this.documentDeliveryMethodCodeField;
                }
                set
                {
                    this.documentDeliveryMethodCodeField = value;
                }
            }

            /// <remarks/>
            public communicationPreferencesEmailFormatCode emailFormatCode
            {
                get
                {
                    return this.emailFormatCodeField;
                }
                set
                {
                    this.emailFormatCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class communicationPreferencesDocumentDeliveryMethodCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class communicationPreferencesEmailFormatCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class specificCommunicationPreferences
        {

            private string contactMethodField;

            private specificCommunicationPreferencesDocumentDeliveryMethodCode documentDeliveryMethodCodeField;

            private string optOutField;

            private string correspondenceNameField;

            private string recipientField;

            /// <remarks/>
            public string contactMethod
            {
                get
                {
                    return this.contactMethodField;
                }
                set
                {
                    this.contactMethodField = value;
                }
            }

            /// <remarks/>
            public specificCommunicationPreferencesDocumentDeliveryMethodCode documentDeliveryMethodCode
            {
                get
                {
                    return this.documentDeliveryMethodCodeField;
                }
                set
                {
                    this.documentDeliveryMethodCodeField = value;
                }
            }

            /// <remarks/>
            public string optOut
            {
                get
                {
                    return this.optOutField;
                }
                set
                {
                    this.optOutField = value;
                }
            }

            /// <remarks/>
            public string correspondenceName
            {
                get
                {
                    return this.correspondenceNameField;
                }
                set
                {
                    this.correspondenceNameField = value;
                }
            }

            /// <remarks/>
            public string recipient
            {
                get
                {
                    return this.recipientField;
                }
                set
                {
                    this.recipientField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class specificCommunicationPreferencesDocumentDeliveryMethodCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class otherIdNumberList
        {

            private string listModeField;

            private otherIdNumberListIdentificationNumber[] identificationNumberField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("identificationNumber")]
            public otherIdNumberListIdentificationNumber[] identificationNumber
            {
                get
                {
                    return this.identificationNumberField;
                }
                set
                {
                    this.identificationNumberField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class otherIdNumberListIdentificationNumber
        {

            private string identificationNumberField;

            private string issuingStateField;

            private otherIdNumberListIdentificationNumberIssuingCountry issuingCountryField;

            private string effectiveStartDateField;

            private string effectiveEndDateField;

            private otherIdNumberListIdentificationNumberIdentificationTypeCode identificationTypeCodeField;

            /// <remarks/>
            public string identificationNumber
            {
                get
                {
                    return this.identificationNumberField;
                }
                set
                {
                    this.identificationNumberField = value;
                }
            }

            /// <remarks/>
            public string issuingState
            {
                get
                {
                    return this.issuingStateField;
                }
                set
                {
                    this.issuingStateField = value;
                }
            }

            /// <remarks/>
            public otherIdNumberListIdentificationNumberIssuingCountry issuingCountry
            {
                get
                {
                    return this.issuingCountryField;
                }
                set
                {
                    this.issuingCountryField = value;
                }
            }

            /// <remarks/>
            public string effectiveStartDate
            {
                get
                {
                    return this.effectiveStartDateField;
                }
                set
                {
                    this.effectiveStartDateField = value;
                }
            }

            /// <remarks/>
            public string effectiveEndDate
            {
                get
                {
                    return this.effectiveEndDateField;
                }
                set
                {
                    this.effectiveEndDateField = value;
                }
            }

            /// <remarks/>
            public otherIdNumberListIdentificationNumberIdentificationTypeCode identificationTypeCode
            {
                get
                {
                    return this.identificationTypeCodeField;
                }
                set
                {
                    this.identificationTypeCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class otherIdNumberListIdentificationNumberIssuingCountry
        {

            private string countryCodeField;

            /// <remarks/>
            public string countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class otherIdNumberListIdentificationNumberIdentificationTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class disabilityInfoForEligibility
        {

            private string disabilityTypeCodeField;

            private string disabilityReportedDateField;

            private string verificationReceiptDateField;

            private disabilityInfoForEligibilityDisabilityDenialReasonCode disabilityDenialReasonCodeField;

            private disabilityInfoForEligibilityDiagnosisReference diagnosisReferenceField;

            /// <remarks/>
            public string disabilityTypeCode
            {
                get
                {
                    return this.disabilityTypeCodeField;
                }
                set
                {
                    this.disabilityTypeCodeField = value;
                }
            }

            /// <remarks/>
            public string disabilityReportedDate
            {
                get
                {
                    return this.disabilityReportedDateField;
                }
                set
                {
                    this.disabilityReportedDateField = value;
                }
            }

            /// <remarks/>
            public string verificationReceiptDate
            {
                get
                {
                    return this.verificationReceiptDateField;
                }
                set
                {
                    this.verificationReceiptDateField = value;
                }
            }

            /// <remarks/>
            public disabilityInfoForEligibilityDisabilityDenialReasonCode disabilityDenialReasonCode
            {
                get
                {
                    return this.disabilityDenialReasonCodeField;
                }
                set
                {
                    this.disabilityDenialReasonCodeField = value;
                }
            }

            /// <remarks/>
            public disabilityInfoForEligibilityDiagnosisReference diagnosisReference
            {
                get
                {
                    return this.diagnosisReferenceField;
                }
                set
                {
                    this.diagnosisReferenceField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class disabilityInfoForEligibilityDisabilityDenialReasonCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class disabilityInfoForEligibilityDiagnosisReference
        {

            private string idField;

            /// <remarks/>
            public string ID
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class dependentVerification
        {

            private string listModeField;

            private dependentVerificationDependentVerification[] dependentVerification1Field;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("dependentVerification")]
            public dependentVerificationDependentVerification[] dependentVerification1
            {
                get
                {
                    return this.dependentVerification1Field;
                }
                set
                {
                    this.dependentVerification1Field = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class dependentVerificationDependentVerification
        {

            private string verificationTypeField;

            private string certLastInfoRequestDateField;

            private string certLastInfoReceiptDateField;

            private string certInformationDueDateField;

            private string lastVerificationDateField;

            private dependentVerificationDependentVerificationStudentSchoolInfo studentSchoolInfoField;

            /// <remarks/>
            public string verificationType
            {
                get
                {
                    return this.verificationTypeField;
                }
                set
                {
                    this.verificationTypeField = value;
                }
            }

            /// <remarks/>
            public string certLastInfoRequestDate
            {
                get
                {
                    return this.certLastInfoRequestDateField;
                }
                set
                {
                    this.certLastInfoRequestDateField = value;
                }
            }

            /// <remarks/>
            public string certLastInfoReceiptDate
            {
                get
                {
                    return this.certLastInfoReceiptDateField;
                }
                set
                {
                    this.certLastInfoReceiptDateField = value;
                }
            }

            /// <remarks/>
            public string certInformationDueDate
            {
                get
                {
                    return this.certInformationDueDateField;
                }
                set
                {
                    this.certInformationDueDateField = value;
                }
            }

            /// <remarks/>
            public string lastVerificationDate
            {
                get
                {
                    return this.lastVerificationDateField;
                }
                set
                {
                    this.lastVerificationDateField = value;
                }
            }

            /// <remarks/>
            public dependentVerificationDependentVerificationStudentSchoolInfo studentSchoolInfo
            {
                get
                {
                    return this.studentSchoolInfoField;
                }
                set
                {
                    this.studentSchoolInfoField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class dependentVerificationDependentVerificationStudentSchoolInfo
        {

            private string studentStatusCodeField;

            private string graduationDateField;

            private dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformation schoolInformationField;

            private dependentVerificationDependentVerificationStudentSchoolInfoSchoolTypeCode schoolTypeCodeField;

            /// <remarks/>
            public string studentStatusCode
            {
                get
                {
                    return this.studentStatusCodeField;
                }
                set
                {
                    this.studentStatusCodeField = value;
                }
            }

            /// <remarks/>
            public string graduationDate
            {
                get
                {
                    return this.graduationDateField;
                }
                set
                {
                    this.graduationDateField = value;
                }
            }

            /// <remarks/>
            public dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformation schoolInformation
            {
                get
                {
                    return this.schoolInformationField;
                }
                set
                {
                    this.schoolInformationField = value;
                }
            }

            /// <remarks/>
            public dependentVerificationDependentVerificationStudentSchoolInfoSchoolTypeCode schoolTypeCode
            {
                get
                {
                    return this.schoolTypeCodeField;
                }
                set
                {
                    this.schoolTypeCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformation
        {

            private string institutionNameField;

            private dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfo contactInfoField;

            /// <remarks/>
            public string institutionName
            {
                get
                {
                    return this.institutionNameField;
                }
                set
                {
                    this.institutionNameField = value;
                }
            }

            /// <remarks/>
            public dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfo contactInfo
            {
                get
                {
                    return this.contactInfoField;
                }
                set
                {
                    this.contactInfoField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfo
        {

            private string contactDescriptionField;

            private string contactEmailAddressField;

            private dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoContactName contactNameField;

            private dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddress locationAddressField;

            private dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoContactTypeCode contactTypeCodeField;

            /// <remarks/>
            public string contactDescription
            {
                get
                {
                    return this.contactDescriptionField;
                }
                set
                {
                    this.contactDescriptionField = value;
                }
            }

            /// <remarks/>
            public string contactEmailAddress
            {
                get
                {
                    return this.contactEmailAddressField;
                }
                set
                {
                    this.contactEmailAddressField = value;
                }
            }

            /// <remarks/>
            public dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoContactName contactName
            {
                get
                {
                    return this.contactNameField;
                }
                set
                {
                    this.contactNameField = value;
                }
            }

            /// <remarks/>
            public dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddress locationAddress
            {
                get
                {
                    return this.locationAddressField;
                }
                set
                {
                    this.locationAddressField = value;
                }
            }

            /// <remarks/>
            public dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoContactTypeCode contactTypeCode
            {
                get
                {
                    return this.contactTypeCodeField;
                }
                set
                {
                    this.contactTypeCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoContactName
        {

            private string lastNameField;

            private string firstNameField;

            private string middleNameField;

            private string namePrefixField;

            private string nameSuffixField;

            private dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoContactNameNameSuffixList nameSuffixListField;

            private dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoContactNameCredentialSuffixList credentialSuffixListField;

            /// <remarks/>
            public string lastName
            {
                get
                {
                    return this.lastNameField;
                }
                set
                {
                    this.lastNameField = value;
                }
            }

            /// <remarks/>
            public string firstName
            {
                get
                {
                    return this.firstNameField;
                }
                set
                {
                    this.firstNameField = value;
                }
            }

            /// <remarks/>
            public string middleName
            {
                get
                {
                    return this.middleNameField;
                }
                set
                {
                    this.middleNameField = value;
                }
            }

            /// <remarks/>
            public string namePrefix
            {
                get
                {
                    return this.namePrefixField;
                }
                set
                {
                    this.namePrefixField = value;
                }
            }

            /// <remarks/>
            public string nameSuffix
            {
                get
                {
                    return this.nameSuffixField;
                }
                set
                {
                    this.nameSuffixField = value;
                }
            }

            /// <remarks/>
            public dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoContactNameNameSuffixList nameSuffixList
            {
                get
                {
                    return this.nameSuffixListField;
                }
                set
                {
                    this.nameSuffixListField = value;
                }
            }

            /// <remarks/>
            public dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoContactNameCredentialSuffixList credentialSuffixList
            {
                get
                {
                    return this.credentialSuffixListField;
                }
                set
                {
                    this.credentialSuffixListField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoContactNameNameSuffixList
        {

            private string listModeField;

            private string[] suffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("suffix")]
            public string[] suffix
            {
                get
                {
                    return this.suffixField;
                }
                set
                {
                    this.suffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoContactNameCredentialSuffixList
        {

            private string listModeField;

            private dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoContactNameCredentialSuffixListCredentialSuffix[] credentialSuffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("CredentialSuffix")]
            public dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoContactNameCredentialSuffixListCredentialSuffix[] CredentialSuffix
            {
                get
                {
                    return this.credentialSuffixField;
                }
                set
                {
                    this.credentialSuffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoContactNameCredentialSuffixListCredentialSuffix
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddress
        {

            private dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddressPostalAddress postalAddressField;

            private dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddressAddressPhoneList addressPhoneListField;

            /// <remarks/>
            public dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddressPostalAddress postalAddress
            {
                get
                {
                    return this.postalAddressField;
                }
                set
                {
                    this.postalAddressField = value;
                }
            }

            /// <remarks/>
            public dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddressAddressPhoneList addressPhoneList
            {
                get
                {
                    return this.addressPhoneListField;
                }
                set
                {
                    this.addressPhoneListField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddressPostalAddress
        {

            private string addressField;

            private string address2Field;

            private string address3Field;

            private string stateCodeField;

            private string zipCodeField;

            private string zipExtensionCodeField;

            private string cityNameField;

            private string countyCodeField;

            private dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddressPostalAddressCountryCode countryCodeField;

            private string longitudeField;

            private string latitudeField;

            private string ignoreAddressCheckField;

            private string undeliverableAddressField;

            /// <remarks/>
            public string address
            {
                get
                {
                    return this.addressField;
                }
                set
                {
                    this.addressField = value;
                }
            }

            /// <remarks/>
            public string address2
            {
                get
                {
                    return this.address2Field;
                }
                set
                {
                    this.address2Field = value;
                }
            }

            /// <remarks/>
            public string address3
            {
                get
                {
                    return this.address3Field;
                }
                set
                {
                    this.address3Field = value;
                }
            }

            /// <remarks/>
            public string stateCode
            {
                get
                {
                    return this.stateCodeField;
                }
                set
                {
                    this.stateCodeField = value;
                }
            }

            /// <remarks/>
            public string zipCode
            {
                get
                {
                    return this.zipCodeField;
                }
                set
                {
                    this.zipCodeField = value;
                }
            }

            /// <remarks/>
            public string zipExtensionCode
            {
                get
                {
                    return this.zipExtensionCodeField;
                }
                set
                {
                    this.zipExtensionCodeField = value;
                }
            }

            /// <remarks/>
            public string cityName
            {
                get
                {
                    return this.cityNameField;
                }
                set
                {
                    this.cityNameField = value;
                }
            }

            /// <remarks/>
            public string countyCode
            {
                get
                {
                    return this.countyCodeField;
                }
                set
                {
                    this.countyCodeField = value;
                }
            }

            /// <remarks/>
            public dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddressPostalAddressCountryCode countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }

            /// <remarks/>
            public string longitude
            {
                get
                {
                    return this.longitudeField;
                }
                set
                {
                    this.longitudeField = value;
                }
            }

            /// <remarks/>
            public string latitude
            {
                get
                {
                    return this.latitudeField;
                }
                set
                {
                    this.latitudeField = value;
                }
            }

            /// <remarks/>
            public string ignoreAddressCheck
            {
                get
                {
                    return this.ignoreAddressCheckField;
                }
                set
                {
                    this.ignoreAddressCheckField = value;
                }
            }

            /// <remarks/>
            public string undeliverableAddress
            {
                get
                {
                    return this.undeliverableAddressField;
                }
                set
                {
                    this.undeliverableAddressField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddressPostalAddressCountryCode
        {

            private string countryCodeField;

            /// <remarks/>
            public string countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddressAddressPhoneList
        {

            private string listModeField;

            private dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddressAddressPhoneListTelephoneNumber[] telephoneNumberField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("telephoneNumber")]
            public dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddressAddressPhoneListTelephoneNumber[] telephoneNumber
            {
                get
                {
                    return this.telephoneNumberField;
                }
                set
                {
                    this.telephoneNumberField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddressAddressPhoneListTelephoneNumber
        {

            private string phoneCountryCodeField;

            private string phoneAreaCodeField;

            private string phoneNumberField;

            private string phoneExtensionNumberField;

            private dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddressAddressPhoneListTelephoneNumberIndividualPhoneTypeCode individualPhoneTypeCodeField;

            private dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddressAddressPhoneListTelephoneNumberOrganizationPhoneTypeCode organizationPhoneTypeCodeField;

            /// <remarks/>
            public string phoneCountryCode
            {
                get
                {
                    return this.phoneCountryCodeField;
                }
                set
                {
                    this.phoneCountryCodeField = value;
                }
            }

            /// <remarks/>
            public string phoneAreaCode
            {
                get
                {
                    return this.phoneAreaCodeField;
                }
                set
                {
                    this.phoneAreaCodeField = value;
                }
            }

            /// <remarks/>
            public string phoneNumber
            {
                get
                {
                    return this.phoneNumberField;
                }
                set
                {
                    this.phoneNumberField = value;
                }
            }

            /// <remarks/>
            public string phoneExtensionNumber
            {
                get
                {
                    return this.phoneExtensionNumberField;
                }
                set
                {
                    this.phoneExtensionNumberField = value;
                }
            }

            /// <remarks/>
            public dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddressAddressPhoneListTelephoneNumberIndividualPhoneTypeCode individualPhoneTypeCode
            {
                get
                {
                    return this.individualPhoneTypeCodeField;
                }
                set
                {
                    this.individualPhoneTypeCodeField = value;
                }
            }

            /// <remarks/>
            public dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddressAddressPhoneListTelephoneNumberOrganizationPhoneTypeCode organizationPhoneTypeCode
            {
                get
                {
                    return this.organizationPhoneTypeCodeField;
                }
                set
                {
                    this.organizationPhoneTypeCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddressAddressPhoneListTelephoneNumberIndividualPhoneTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddressAddressPhoneListTelephoneNumberOrganizationPhoneTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class dependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoContactTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class dependentVerificationDependentVerificationStudentSchoolInfoSchoolTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class physicalCharacteristics
        {

            private string heightInchesCountField;

            private string weightPoundsCountField;

            /// <remarks/>
            public string heightInchesCount
            {
                get
                {
                    return this.heightInchesCountField;
                }
                set
                {
                    this.heightInchesCountField = value;
                }
            }

            /// <remarks/>
            public string weightPoundsCount
            {
                get
                {
                    return this.weightPoundsCountField;
                }
                set
                {
                    this.weightPoundsCountField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class employmentInfo
        {

            private string hireDateField;

            private string salaryIntervalField;

            private string salaryPerIntervalField;

            private string unionStatusField;

            private string positionOrTitleField;

            private string numberOfHoursWorkedField;

            private string retirementDateField;

            private employmentInfoEmploymentStatusCodeCode employmentStatusCodeCodeField;

            private employmentInfoSalaryGradeCodeCode salaryGradeCodeCodeField;

            private employmentInfoUnitsDomainCode unitsDomainCodeField;

            private employmentInfoEmployeeTypeDomainCode employeeTypeDomainCodeField;

            private string departmentField;

            /// <remarks/>
            public string hireDate
            {
                get
                {
                    return this.hireDateField;
                }
                set
                {
                    this.hireDateField = value;
                }
            }

            /// <remarks/>
            public string salaryInterval
            {
                get
                {
                    return this.salaryIntervalField;
                }
                set
                {
                    this.salaryIntervalField = value;
                }
            }

            /// <remarks/>
            public string salaryPerInterval
            {
                get
                {
                    return this.salaryPerIntervalField;
                }
                set
                {
                    this.salaryPerIntervalField = value;
                }
            }

            /// <remarks/>
            public string unionStatus
            {
                get
                {
                    return this.unionStatusField;
                }
                set
                {
                    this.unionStatusField = value;
                }
            }

            /// <remarks/>
            public string positionOrTitle
            {
                get
                {
                    return this.positionOrTitleField;
                }
                set
                {
                    this.positionOrTitleField = value;
                }
            }

            /// <remarks/>
            public string numberOfHoursWorked
            {
                get
                {
                    return this.numberOfHoursWorkedField;
                }
                set
                {
                    this.numberOfHoursWorkedField = value;
                }
            }

            /// <remarks/>
            public string retirementDate
            {
                get
                {
                    return this.retirementDateField;
                }
                set
                {
                    this.retirementDateField = value;
                }
            }

            /// <remarks/>
            public employmentInfoEmploymentStatusCodeCode employmentStatusCodeCode
            {
                get
                {
                    return this.employmentStatusCodeCodeField;
                }
                set
                {
                    this.employmentStatusCodeCodeField = value;
                }
            }

            /// <remarks/>
            public employmentInfoSalaryGradeCodeCode salaryGradeCodeCode
            {
                get
                {
                    return this.salaryGradeCodeCodeField;
                }
                set
                {
                    this.salaryGradeCodeCodeField = value;
                }
            }

            /// <remarks/>
            public employmentInfoUnitsDomainCode unitsDomainCode
            {
                get
                {
                    return this.unitsDomainCodeField;
                }
                set
                {
                    this.unitsDomainCodeField = value;
                }
            }

            /// <remarks/>
            public employmentInfoEmployeeTypeDomainCode employeeTypeDomainCode
            {
                get
                {
                    return this.employeeTypeDomainCodeField;
                }
                set
                {
                    this.employeeTypeDomainCodeField = value;
                }
            }

            /// <remarks/>
            public string department
            {
                get
                {
                    return this.departmentField;
                }
                set
                {
                    this.departmentField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class employmentInfoEmploymentStatusCodeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class employmentInfoSalaryGradeCodeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class employmentInfoUnitsDomainCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class employmentInfoEmployeeTypeDomainCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class dentalInfo
        {

            private string remarksField;

            private dentalInfoMissingTeeth missingTeethField;

            /// <remarks/>
            public string remarks
            {
                get
                {
                    return this.remarksField;
                }
                set
                {
                    this.remarksField = value;
                }
            }

            /// <remarks/>
            public dentalInfoMissingTeeth missingTeeth
            {
                get
                {
                    return this.missingTeethField;
                }
                set
                {
                    this.missingTeethField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class dentalInfoMissingTeeth
        {

            private string listModeField;

            private string[] toothNameField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("toothName")]
            public string[] toothName
            {
                get
                {
                    return this.toothNameField;
                }
                set
                {
                    this.toothNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class priorCoverageInfo
        {

            private string priorCoverageMonthCountField;

            /// <remarks/>
            public string priorCoverageMonthCount
            {
                get
                {
                    return this.priorCoverageMonthCountField;
                }
                set
                {
                    this.priorCoverageMonthCountField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class preExistingConditionInfo
        {

            private string listModeField;

            private preExistingConditionInfoPreExistingConditionInfo[] preExistingConditionInfo1Field;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("preExistingConditionInfo")]
            public preExistingConditionInfoPreExistingConditionInfo[] preExistingConditionInfo1
            {
                get
                {
                    return this.preExistingConditionInfo1Field;
                }
                set
                {
                    this.preExistingConditionInfo1Field = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class preExistingConditionInfoPreExistingConditionInfo
        {

            private string onsetOfConditionDateField;

            private string preexistingPeriodEndDateField;

            private string preexDescriptionField;

            private preExistingConditionInfoPreExistingConditionInfoPreExDiagnosisCodeRangeCondition[] preExDiagnosisCodeRangeConditionsField;

            private preExistingConditionInfoPreExistingConditionInfoPreExProcedureCodeRangeCondition[] preExProcedureCodeRangeConditionsField;

            private preExistingConditionInfoPreExistingConditionInfoDiagnosisReference[] preexistingConditionsField;

            private preExistingConditionInfoPreExistingConditionInfoServiceReference[] preexistingConditionProceduresField;

            /// <remarks/>
            public string onsetOfConditionDate
            {
                get
                {
                    return this.onsetOfConditionDateField;
                }
                set
                {
                    this.onsetOfConditionDateField = value;
                }
            }

            /// <remarks/>
            public string preexistingPeriodEndDate
            {
                get
                {
                    return this.preexistingPeriodEndDateField;
                }
                set
                {
                    this.preexistingPeriodEndDateField = value;
                }
            }

            /// <remarks/>
            public string preexDescription
            {
                get
                {
                    return this.preexDescriptionField;
                }
                set
                {
                    this.preexDescriptionField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlArrayItemAttribute("preExDiagnosisCodeRangeCondition", IsNullable = false)]
            public preExistingConditionInfoPreExistingConditionInfoPreExDiagnosisCodeRangeCondition[] preExDiagnosisCodeRangeConditions
            {
                get
                {
                    return this.preExDiagnosisCodeRangeConditionsField;
                }
                set
                {
                    this.preExDiagnosisCodeRangeConditionsField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlArrayItemAttribute("preExProcedureCodeRangeCondition", IsNullable = false)]
            public preExistingConditionInfoPreExistingConditionInfoPreExProcedureCodeRangeCondition[] preExProcedureCodeRangeConditions
            {
                get
                {
                    return this.preExProcedureCodeRangeConditionsField;
                }
                set
                {
                    this.preExProcedureCodeRangeConditionsField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlArrayItemAttribute("diagnosisReference", IsNullable = false)]
            public preExistingConditionInfoPreExistingConditionInfoDiagnosisReference[] preexistingConditions
            {
                get
                {
                    return this.preexistingConditionsField;
                }
                set
                {
                    this.preexistingConditionsField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlArrayItemAttribute("serviceReference", IsNullable = false)]
            public preExistingConditionInfoPreExistingConditionInfoServiceReference[] preexistingConditionProcedures
            {
                get
                {
                    return this.preexistingConditionProceduresField;
                }
                set
                {
                    this.preexistingConditionProceduresField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class preExistingConditionInfoPreExistingConditionInfoPreExDiagnosisCodeRangeCondition
        {

            private preExistingConditionInfoPreExistingConditionInfoPreExDiagnosisCodeRangeConditionRangeStartDiagnosisCode rangeStartDiagnosisCodeField;

            private preExistingConditionInfoPreExistingConditionInfoPreExDiagnosisCodeRangeConditionRangeEndDiagnosisCode rangeEndDiagnosisCodeField;

            /// <remarks/>
            public preExistingConditionInfoPreExistingConditionInfoPreExDiagnosisCodeRangeConditionRangeStartDiagnosisCode rangeStartDiagnosisCode
            {
                get
                {
                    return this.rangeStartDiagnosisCodeField;
                }
                set
                {
                    this.rangeStartDiagnosisCodeField = value;
                }
            }

            /// <remarks/>
            public preExistingConditionInfoPreExistingConditionInfoPreExDiagnosisCodeRangeConditionRangeEndDiagnosisCode rangeEndDiagnosisCode
            {
                get
                {
                    return this.rangeEndDiagnosisCodeField;
                }
                set
                {
                    this.rangeEndDiagnosisCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class preExistingConditionInfoPreExistingConditionInfoPreExDiagnosisCodeRangeConditionRangeStartDiagnosisCode
        {

            private preExistingConditionInfoPreExistingConditionInfoPreExDiagnosisCodeRangeConditionRangeStartDiagnosisCodeDiagnosisReference diagnosisReferenceField;

            /// <remarks/>
            public preExistingConditionInfoPreExistingConditionInfoPreExDiagnosisCodeRangeConditionRangeStartDiagnosisCodeDiagnosisReference diagnosisReference
            {
                get
                {
                    return this.diagnosisReferenceField;
                }
                set
                {
                    this.diagnosisReferenceField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class preExistingConditionInfoPreExistingConditionInfoPreExDiagnosisCodeRangeConditionRangeStartDiagnosisCodeDiagnosisReference
        {

            private string idField;

            /// <remarks/>
            public string ID
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class preExistingConditionInfoPreExistingConditionInfoPreExDiagnosisCodeRangeConditionRangeEndDiagnosisCode
        {

            private preExistingConditionInfoPreExistingConditionInfoPreExDiagnosisCodeRangeConditionRangeEndDiagnosisCodeDiagnosisReference diagnosisReferenceField;

            /// <remarks/>
            public preExistingConditionInfoPreExistingConditionInfoPreExDiagnosisCodeRangeConditionRangeEndDiagnosisCodeDiagnosisReference diagnosisReference
            {
                get
                {
                    return this.diagnosisReferenceField;
                }
                set
                {
                    this.diagnosisReferenceField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class preExistingConditionInfoPreExistingConditionInfoPreExDiagnosisCodeRangeConditionRangeEndDiagnosisCodeDiagnosisReference
        {

            private string idField;

            /// <remarks/>
            public string ID
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class preExistingConditionInfoPreExistingConditionInfoPreExProcedureCodeRangeCondition
        {

            private preExistingConditionInfoPreExistingConditionInfoPreExProcedureCodeRangeConditionRangeStartProcedureCode rangeStartProcedureCodeField;

            private preExistingConditionInfoPreExistingConditionInfoPreExProcedureCodeRangeConditionRangeEndProcedureCode rangeEndProcedureCodeField;

            /// <remarks/>
            public preExistingConditionInfoPreExistingConditionInfoPreExProcedureCodeRangeConditionRangeStartProcedureCode rangeStartProcedureCode
            {
                get
                {
                    return this.rangeStartProcedureCodeField;
                }
                set
                {
                    this.rangeStartProcedureCodeField = value;
                }
            }

            /// <remarks/>
            public preExistingConditionInfoPreExistingConditionInfoPreExProcedureCodeRangeConditionRangeEndProcedureCode rangeEndProcedureCode
            {
                get
                {
                    return this.rangeEndProcedureCodeField;
                }
                set
                {
                    this.rangeEndProcedureCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class preExistingConditionInfoPreExistingConditionInfoPreExProcedureCodeRangeConditionRangeStartProcedureCode
        {

            private preExistingConditionInfoPreExistingConditionInfoPreExProcedureCodeRangeConditionRangeStartProcedureCodeServiceReference serviceReferenceField;

            /// <remarks/>
            public preExistingConditionInfoPreExistingConditionInfoPreExProcedureCodeRangeConditionRangeStartProcedureCodeServiceReference serviceReference
            {
                get
                {
                    return this.serviceReferenceField;
                }
                set
                {
                    this.serviceReferenceField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class preExistingConditionInfoPreExistingConditionInfoPreExProcedureCodeRangeConditionRangeStartProcedureCodeServiceReference
        {

            private string idField;

            /// <remarks/>
            public string ID
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class preExistingConditionInfoPreExistingConditionInfoPreExProcedureCodeRangeConditionRangeEndProcedureCode
        {

            private preExistingConditionInfoPreExistingConditionInfoPreExProcedureCodeRangeConditionRangeEndProcedureCodeServiceReference serviceReferenceField;

            /// <remarks/>
            public preExistingConditionInfoPreExistingConditionInfoPreExProcedureCodeRangeConditionRangeEndProcedureCodeServiceReference serviceReference
            {
                get
                {
                    return this.serviceReferenceField;
                }
                set
                {
                    this.serviceReferenceField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class preExistingConditionInfoPreExistingConditionInfoPreExProcedureCodeRangeConditionRangeEndProcedureCodeServiceReference
        {

            private string idField;

            /// <remarks/>
            public string ID
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class preExistingConditionInfoPreExistingConditionInfoDiagnosisReference
        {

            private string idField;

            /// <remarks/>
            public string ID
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class preExistingConditionInfoPreExistingConditionInfoServiceReference
        {

            private string idField;

            /// <remarks/>
            public string ID
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class preExReduction
        {

            private string preexStartDateField;

            private string reductionEndDateField;

            private string reductionReasonReceiptDateField;

            private preExReductionReductionReasonCode reductionReasonCodeField;

            /// <remarks/>
            public string preexStartDate
            {
                get
                {
                    return this.preexStartDateField;
                }
                set
                {
                    this.preexStartDateField = value;
                }
            }

            /// <remarks/>
            public string reductionEndDate
            {
                get
                {
                    return this.reductionEndDateField;
                }
                set
                {
                    this.reductionEndDateField = value;
                }
            }

            /// <remarks/>
            public string reductionReasonReceiptDate
            {
                get
                {
                    return this.reductionReasonReceiptDateField;
                }
                set
                {
                    this.reductionReasonReceiptDateField = value;
                }
            }

            /// <remarks/>
            public preExReductionReductionReasonCode reductionReasonCode
            {
                get
                {
                    return this.reductionReasonCodeField;
                }
                set
                {
                    this.reductionReasonCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class preExReductionReductionReasonCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class otherResponsiblePersonInfo
        {

            private string listModeField;

            private otherResponsiblePersonInfoOtherResponsiblePersonInfo[] otherResponsiblePersonInfo1Field;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("otherResponsiblePersonInfo")]
            public otherResponsiblePersonInfoOtherResponsiblePersonInfo[] otherResponsiblePersonInfo1
            {
                get
                {
                    return this.otherResponsiblePersonInfo1Field;
                }
                set
                {
                    this.otherResponsiblePersonInfo1Field = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class otherResponsiblePersonInfoOtherResponsiblePersonInfo
        {

            private string sourceOfAuthorizationField;

            private string responsiblePersonEmailAddressField;

            private otherResponsiblePersonInfoOtherResponsiblePersonInfoResponsiblePersonName responsiblePersonNameField;

            private otherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfo responsibleAddressInfoField;

            private otherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibilityTypeCodeCode responsibilityTypeCodeCodeField;

            private otherResponsiblePersonInfoOtherResponsiblePersonInfoRelationshipToMemberCode relationshipToMemberCodeField;

            /// <remarks/>
            public string sourceOfAuthorization
            {
                get
                {
                    return this.sourceOfAuthorizationField;
                }
                set
                {
                    this.sourceOfAuthorizationField = value;
                }
            }

            /// <remarks/>
            public string responsiblePersonEmailAddress
            {
                get
                {
                    return this.responsiblePersonEmailAddressField;
                }
                set
                {
                    this.responsiblePersonEmailAddressField = value;
                }
            }

            /// <remarks/>
            public otherResponsiblePersonInfoOtherResponsiblePersonInfoResponsiblePersonName responsiblePersonName
            {
                get
                {
                    return this.responsiblePersonNameField;
                }
                set
                {
                    this.responsiblePersonNameField = value;
                }
            }

            /// <remarks/>
            public otherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfo responsibleAddressInfo
            {
                get
                {
                    return this.responsibleAddressInfoField;
                }
                set
                {
                    this.responsibleAddressInfoField = value;
                }
            }

            /// <remarks/>
            public otherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibilityTypeCodeCode responsibilityTypeCodeCode
            {
                get
                {
                    return this.responsibilityTypeCodeCodeField;
                }
                set
                {
                    this.responsibilityTypeCodeCodeField = value;
                }
            }

            /// <remarks/>
            public otherResponsiblePersonInfoOtherResponsiblePersonInfoRelationshipToMemberCode relationshipToMemberCode
            {
                get
                {
                    return this.relationshipToMemberCodeField;
                }
                set
                {
                    this.relationshipToMemberCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class otherResponsiblePersonInfoOtherResponsiblePersonInfoResponsiblePersonName
        {

            private string lastNameField;

            private string firstNameField;

            private string middleNameField;

            private string namePrefixField;

            private string nameSuffixField;

            private otherResponsiblePersonInfoOtherResponsiblePersonInfoResponsiblePersonNameNameSuffixList nameSuffixListField;

            private otherResponsiblePersonInfoOtherResponsiblePersonInfoResponsiblePersonNameCredentialSuffixList credentialSuffixListField;

            /// <remarks/>
            public string lastName
            {
                get
                {
                    return this.lastNameField;
                }
                set
                {
                    this.lastNameField = value;
                }
            }

            /// <remarks/>
            public string firstName
            {
                get
                {
                    return this.firstNameField;
                }
                set
                {
                    this.firstNameField = value;
                }
            }

            /// <remarks/>
            public string middleName
            {
                get
                {
                    return this.middleNameField;
                }
                set
                {
                    this.middleNameField = value;
                }
            }

            /// <remarks/>
            public string namePrefix
            {
                get
                {
                    return this.namePrefixField;
                }
                set
                {
                    this.namePrefixField = value;
                }
            }

            /// <remarks/>
            public string nameSuffix
            {
                get
                {
                    return this.nameSuffixField;
                }
                set
                {
                    this.nameSuffixField = value;
                }
            }

            /// <remarks/>
            public otherResponsiblePersonInfoOtherResponsiblePersonInfoResponsiblePersonNameNameSuffixList nameSuffixList
            {
                get
                {
                    return this.nameSuffixListField;
                }
                set
                {
                    this.nameSuffixListField = value;
                }
            }

            /// <remarks/>
            public otherResponsiblePersonInfoOtherResponsiblePersonInfoResponsiblePersonNameCredentialSuffixList credentialSuffixList
            {
                get
                {
                    return this.credentialSuffixListField;
                }
                set
                {
                    this.credentialSuffixListField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class otherResponsiblePersonInfoOtherResponsiblePersonInfoResponsiblePersonNameNameSuffixList
        {

            private string listModeField;

            private string[] suffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("suffix")]
            public string[] suffix
            {
                get
                {
                    return this.suffixField;
                }
                set
                {
                    this.suffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class otherResponsiblePersonInfoOtherResponsiblePersonInfoResponsiblePersonNameCredentialSuffixList
        {

            private string listModeField;

            private otherResponsiblePersonInfoOtherResponsiblePersonInfoResponsiblePersonNameCredentialSuffixListCredentialSuffix[] credentialSuffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("CredentialSuffix")]
            public otherResponsiblePersonInfoOtherResponsiblePersonInfoResponsiblePersonNameCredentialSuffixListCredentialSuffix[] CredentialSuffix
            {
                get
                {
                    return this.credentialSuffixField;
                }
                set
                {
                    this.credentialSuffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class otherResponsiblePersonInfoOtherResponsiblePersonInfoResponsiblePersonNameCredentialSuffixListCredentialSuffix
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class otherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfo
        {

            private otherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfoPostalAddress postalAddressField;

            private otherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfoAddressPhoneList addressPhoneListField;

            /// <remarks/>
            public otherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfoPostalAddress postalAddress
            {
                get
                {
                    return this.postalAddressField;
                }
                set
                {
                    this.postalAddressField = value;
                }
            }

            /// <remarks/>
            public otherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfoAddressPhoneList addressPhoneList
            {
                get
                {
                    return this.addressPhoneListField;
                }
                set
                {
                    this.addressPhoneListField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class otherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfoPostalAddress
        {

            private string addressField;

            private string address2Field;

            private string address3Field;

            private string stateCodeField;

            private string zipCodeField;

            private string zipExtensionCodeField;

            private string cityNameField;

            private string countyCodeField;

            private otherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfoPostalAddressCountryCode countryCodeField;

            private string longitudeField;

            private string latitudeField;

            private string ignoreAddressCheckField;

            private string undeliverableAddressField;

            /// <remarks/>
            public string address
            {
                get
                {
                    return this.addressField;
                }
                set
                {
                    this.addressField = value;
                }
            }

            /// <remarks/>
            public string address2
            {
                get
                {
                    return this.address2Field;
                }
                set
                {
                    this.address2Field = value;
                }
            }

            /// <remarks/>
            public string address3
            {
                get
                {
                    return this.address3Field;
                }
                set
                {
                    this.address3Field = value;
                }
            }

            /// <remarks/>
            public string stateCode
            {
                get
                {
                    return this.stateCodeField;
                }
                set
                {
                    this.stateCodeField = value;
                }
            }

            /// <remarks/>
            public string zipCode
            {
                get
                {
                    return this.zipCodeField;
                }
                set
                {
                    this.zipCodeField = value;
                }
            }

            /// <remarks/>
            public string zipExtensionCode
            {
                get
                {
                    return this.zipExtensionCodeField;
                }
                set
                {
                    this.zipExtensionCodeField = value;
                }
            }

            /// <remarks/>
            public string cityName
            {
                get
                {
                    return this.cityNameField;
                }
                set
                {
                    this.cityNameField = value;
                }
            }

            /// <remarks/>
            public string countyCode
            {
                get
                {
                    return this.countyCodeField;
                }
                set
                {
                    this.countyCodeField = value;
                }
            }

            /// <remarks/>
            public otherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfoPostalAddressCountryCode countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }

            /// <remarks/>
            public string longitude
            {
                get
                {
                    return this.longitudeField;
                }
                set
                {
                    this.longitudeField = value;
                }
            }

            /// <remarks/>
            public string latitude
            {
                get
                {
                    return this.latitudeField;
                }
                set
                {
                    this.latitudeField = value;
                }
            }

            /// <remarks/>
            public string ignoreAddressCheck
            {
                get
                {
                    return this.ignoreAddressCheckField;
                }
                set
                {
                    this.ignoreAddressCheckField = value;
                }
            }

            /// <remarks/>
            public string undeliverableAddress
            {
                get
                {
                    return this.undeliverableAddressField;
                }
                set
                {
                    this.undeliverableAddressField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class otherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfoPostalAddressCountryCode
        {

            private string countryCodeField;

            /// <remarks/>
            public string countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class otherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfoAddressPhoneList
        {

            private string listModeField;

            private otherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfoAddressPhoneListTelephoneNumber[] telephoneNumberField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("telephoneNumber")]
            public otherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfoAddressPhoneListTelephoneNumber[] telephoneNumber
            {
                get
                {
                    return this.telephoneNumberField;
                }
                set
                {
                    this.telephoneNumberField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class otherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfoAddressPhoneListTelephoneNumber
        {

            private string phoneCountryCodeField;

            private string phoneAreaCodeField;

            private string phoneNumberField;

            private string phoneExtensionNumberField;

            private otherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfoAddressPhoneListTelephoneNumberIndividualPhoneTypeCode individualPhoneTypeCodeField;

            private otherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfoAddressPhoneListTelephoneNumberOrganizationPhoneTypeCode organizationPhoneTypeCodeField;

            /// <remarks/>
            public string phoneCountryCode
            {
                get
                {
                    return this.phoneCountryCodeField;
                }
                set
                {
                    this.phoneCountryCodeField = value;
                }
            }

            /// <remarks/>
            public string phoneAreaCode
            {
                get
                {
                    return this.phoneAreaCodeField;
                }
                set
                {
                    this.phoneAreaCodeField = value;
                }
            }

            /// <remarks/>
            public string phoneNumber
            {
                get
                {
                    return this.phoneNumberField;
                }
                set
                {
                    this.phoneNumberField = value;
                }
            }

            /// <remarks/>
            public string phoneExtensionNumber
            {
                get
                {
                    return this.phoneExtensionNumberField;
                }
                set
                {
                    this.phoneExtensionNumberField = value;
                }
            }

            /// <remarks/>
            public otherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfoAddressPhoneListTelephoneNumberIndividualPhoneTypeCode individualPhoneTypeCode
            {
                get
                {
                    return this.individualPhoneTypeCodeField;
                }
                set
                {
                    this.individualPhoneTypeCodeField = value;
                }
            }

            /// <remarks/>
            public otherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfoAddressPhoneListTelephoneNumberOrganizationPhoneTypeCode organizationPhoneTypeCode
            {
                get
                {
                    return this.organizationPhoneTypeCodeField;
                }
                set
                {
                    this.organizationPhoneTypeCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class otherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfoAddressPhoneListTelephoneNumberIndividualPhoneTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class otherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfoAddressPhoneListTelephoneNumberOrganizationPhoneTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class otherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibilityTypeCodeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class otherResponsiblePersonInfoOtherResponsiblePersonInfoRelationshipToMemberCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class memberRepresentative
        {

            private string listModeField;

            private memberRepresentativeAuthorizedIndividual[] authorizedIndividualField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("authorizedIndividual")]
            public memberRepresentativeAuthorizedIndividual[] authorizedIndividual
            {
                get
                {
                    return this.authorizedIndividualField;
                }
                set
                {
                    this.authorizedIndividualField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberRepresentativeAuthorizedIndividual
        {

            private string effectiveStartDateField;

            private string effectiveEndDateField;

            private string scopeField;

            private string hccReviewerField;

            private string documentsReceivedDateField;

            private memberRepresentativeAuthorizedIndividualAuthorizedPerson authorizedPersonField;

            private memberRepresentativeAuthorizedIndividualAuthorizedIndividualGroupPhoneList authorizedIndividualGroupPhoneListField;

            private memberRepresentativeAuthorizedIndividualRepresentativeTypeCode representativeTypeCodeField;

            private memberRepresentativeAuthorizedIndividualRelationshipToMemberCode relationshipToMemberCodeField;

            /// <remarks/>
            public string effectiveStartDate
            {
                get
                {
                    return this.effectiveStartDateField;
                }
                set
                {
                    this.effectiveStartDateField = value;
                }
            }

            /// <remarks/>
            public string effectiveEndDate
            {
                get
                {
                    return this.effectiveEndDateField;
                }
                set
                {
                    this.effectiveEndDateField = value;
                }
            }

            /// <remarks/>
            public string scope
            {
                get
                {
                    return this.scopeField;
                }
                set
                {
                    this.scopeField = value;
                }
            }

            /// <remarks/>
            public string hccReviewer
            {
                get
                {
                    return this.hccReviewerField;
                }
                set
                {
                    this.hccReviewerField = value;
                }
            }

            /// <remarks/>
            public string documentsReceivedDate
            {
                get
                {
                    return this.documentsReceivedDateField;
                }
                set
                {
                    this.documentsReceivedDateField = value;
                }
            }

            /// <remarks/>
            public memberRepresentativeAuthorizedIndividualAuthorizedPerson authorizedPerson
            {
                get
                {
                    return this.authorizedPersonField;
                }
                set
                {
                    this.authorizedPersonField = value;
                }
            }

            /// <remarks/>
            public memberRepresentativeAuthorizedIndividualAuthorizedIndividualGroupPhoneList authorizedIndividualGroupPhoneList
            {
                get
                {
                    return this.authorizedIndividualGroupPhoneListField;
                }
                set
                {
                    this.authorizedIndividualGroupPhoneListField = value;
                }
            }

            /// <remarks/>
            public memberRepresentativeAuthorizedIndividualRepresentativeTypeCode representativeTypeCode
            {
                get
                {
                    return this.representativeTypeCodeField;
                }
                set
                {
                    this.representativeTypeCodeField = value;
                }
            }

            /// <remarks/>
            public memberRepresentativeAuthorizedIndividualRelationshipToMemberCode relationshipToMemberCode
            {
                get
                {
                    return this.relationshipToMemberCodeField;
                }
                set
                {
                    this.relationshipToMemberCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberRepresentativeAuthorizedIndividualAuthorizedPerson
        {

            private string lastNameField;

            private string firstNameField;

            private string middleNameField;

            private string namePrefixField;

            private string nameSuffixField;

            private memberRepresentativeAuthorizedIndividualAuthorizedPersonNameSuffixList nameSuffixListField;

            private memberRepresentativeAuthorizedIndividualAuthorizedPersonCredentialSuffixList credentialSuffixListField;

            /// <remarks/>
            public string lastName
            {
                get
                {
                    return this.lastNameField;
                }
                set
                {
                    this.lastNameField = value;
                }
            }

            /// <remarks/>
            public string firstName
            {
                get
                {
                    return this.firstNameField;
                }
                set
                {
                    this.firstNameField = value;
                }
            }

            /// <remarks/>
            public string middleName
            {
                get
                {
                    return this.middleNameField;
                }
                set
                {
                    this.middleNameField = value;
                }
            }

            /// <remarks/>
            public string namePrefix
            {
                get
                {
                    return this.namePrefixField;
                }
                set
                {
                    this.namePrefixField = value;
                }
            }

            /// <remarks/>
            public string nameSuffix
            {
                get
                {
                    return this.nameSuffixField;
                }
                set
                {
                    this.nameSuffixField = value;
                }
            }

            /// <remarks/>
            public memberRepresentativeAuthorizedIndividualAuthorizedPersonNameSuffixList nameSuffixList
            {
                get
                {
                    return this.nameSuffixListField;
                }
                set
                {
                    this.nameSuffixListField = value;
                }
            }

            /// <remarks/>
            public memberRepresentativeAuthorizedIndividualAuthorizedPersonCredentialSuffixList credentialSuffixList
            {
                get
                {
                    return this.credentialSuffixListField;
                }
                set
                {
                    this.credentialSuffixListField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberRepresentativeAuthorizedIndividualAuthorizedPersonNameSuffixList
        {

            private string listModeField;

            private string[] suffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("suffix")]
            public string[] suffix
            {
                get
                {
                    return this.suffixField;
                }
                set
                {
                    this.suffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberRepresentativeAuthorizedIndividualAuthorizedPersonCredentialSuffixList
        {

            private string listModeField;

            private memberRepresentativeAuthorizedIndividualAuthorizedPersonCredentialSuffixListCredentialSuffix[] credentialSuffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("CredentialSuffix")]
            public memberRepresentativeAuthorizedIndividualAuthorizedPersonCredentialSuffixListCredentialSuffix[] CredentialSuffix
            {
                get
                {
                    return this.credentialSuffixField;
                }
                set
                {
                    this.credentialSuffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberRepresentativeAuthorizedIndividualAuthorizedPersonCredentialSuffixListCredentialSuffix
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberRepresentativeAuthorizedIndividualAuthorizedIndividualGroupPhoneList
        {

            private string listModeField;

            private memberRepresentativeAuthorizedIndividualAuthorizedIndividualGroupPhoneListTelephoneNumber[] telephoneNumberField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("telephoneNumber")]
            public memberRepresentativeAuthorizedIndividualAuthorizedIndividualGroupPhoneListTelephoneNumber[] telephoneNumber
            {
                get
                {
                    return this.telephoneNumberField;
                }
                set
                {
                    this.telephoneNumberField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberRepresentativeAuthorizedIndividualAuthorizedIndividualGroupPhoneListTelephoneNumber
        {

            private string phoneCountryCodeField;

            private string phoneAreaCodeField;

            private string phoneNumberField;

            private string phoneExtensionNumberField;

            private memberRepresentativeAuthorizedIndividualAuthorizedIndividualGroupPhoneListTelephoneNumberIndividualPhoneTypeCode individualPhoneTypeCodeField;

            private memberRepresentativeAuthorizedIndividualAuthorizedIndividualGroupPhoneListTelephoneNumberOrganizationPhoneTypeCode organizationPhoneTypeCodeField;

            /// <remarks/>
            public string phoneCountryCode
            {
                get
                {
                    return this.phoneCountryCodeField;
                }
                set
                {
                    this.phoneCountryCodeField = value;
                }
            }

            /// <remarks/>
            public string phoneAreaCode
            {
                get
                {
                    return this.phoneAreaCodeField;
                }
                set
                {
                    this.phoneAreaCodeField = value;
                }
            }

            /// <remarks/>
            public string phoneNumber
            {
                get
                {
                    return this.phoneNumberField;
                }
                set
                {
                    this.phoneNumberField = value;
                }
            }

            /// <remarks/>
            public string phoneExtensionNumber
            {
                get
                {
                    return this.phoneExtensionNumberField;
                }
                set
                {
                    this.phoneExtensionNumberField = value;
                }
            }

            /// <remarks/>
            public memberRepresentativeAuthorizedIndividualAuthorizedIndividualGroupPhoneListTelephoneNumberIndividualPhoneTypeCode individualPhoneTypeCode
            {
                get
                {
                    return this.individualPhoneTypeCodeField;
                }
                set
                {
                    this.individualPhoneTypeCodeField = value;
                }
            }

            /// <remarks/>
            public memberRepresentativeAuthorizedIndividualAuthorizedIndividualGroupPhoneListTelephoneNumberOrganizationPhoneTypeCode organizationPhoneTypeCode
            {
                get
                {
                    return this.organizationPhoneTypeCodeField;
                }
                set
                {
                    this.organizationPhoneTypeCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberRepresentativeAuthorizedIndividualAuthorizedIndividualGroupPhoneListTelephoneNumberIndividualPhoneTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberRepresentativeAuthorizedIndividualAuthorizedIndividualGroupPhoneListTelephoneNumberOrganizationPhoneTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberRepresentativeAuthorizedIndividualRepresentativeTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberRepresentativeAuthorizedIndividualRelationshipToMemberCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class membershipUDTList
        {

            private string listModeField;

            private membershipUDTListMembershipUDT[] membershipUDTField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("membershipUDT")]
            public membershipUDTListMembershipUDT[] membershipUDT
            {
                get
                {
                    return this.membershipUDTField;
                }
                set
                {
                    this.membershipUDTField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class membershipUDTListMembershipUDT
        {

            private membershipUDTListMembershipUDTUdtListValueSet[] udtListValueSetField;

            private membershipUDTListMembershipUDTUserDefinedTermReference userDefinedTermReferenceField;

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("udtListValueSet")]
            public membershipUDTListMembershipUDTUdtListValueSet[] udtListValueSet
            {
                get
                {
                    return this.udtListValueSetField;
                }
                set
                {
                    this.udtListValueSetField = value;
                }
            }

            /// <remarks/>
            public membershipUDTListMembershipUDTUserDefinedTermReference userDefinedTermReference
            {
                get
                {
                    return this.userDefinedTermReferenceField;
                }
                set
                {
                    this.userDefinedTermReferenceField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class membershipUDTListMembershipUDTUdtListValueSet
        {

            private string attributeRoleNameField;

            private string attrValueAsStringField;

            /// <remarks/>
            public string attributeRoleName
            {
                get
                {
                    return this.attributeRoleNameField;
                }
                set
                {
                    this.attributeRoleNameField = value;
                }
            }

            /// <remarks/>
            public string attrValueAsString
            {
                get
                {
                    return this.attrValueAsStringField;
                }
                set
                {
                    this.attrValueAsStringField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class membershipUDTListMembershipUDTUserDefinedTermReference
        {

            private string idField;

            /// <remarks/>
            public string ID
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class informationAccessPermissionList
        {

            private string listModeField;

            private informationAccessPermissionListHIPAAPermission[] hIPAAPermissionField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("HIPAAPermission")]
            public informationAccessPermissionListHIPAAPermission[] HIPAAPermission
            {
                get
                {
                    return this.hIPAAPermissionField;
                }
                set
                {
                    this.hIPAAPermissionField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class informationAccessPermissionListHIPAAPermission
        {

            private string permissionScopeField;

            private string effectiveStartDateField;

            private string effectiveEndDateField;

            private informationAccessPermissionListHIPAAPermissionPermittedPerson permittedPersonField;

            private informationAccessPermissionListHIPAAPermissionHIPAAPermissionGroupPhoneList hIPAAPermissionGroupPhoneListField;

            private informationAccessPermissionListHIPAAPermissionRelationshipToMemberCode relationshipToMemberCodeField;

            /// <remarks/>
            public string permissionScope
            {
                get
                {
                    return this.permissionScopeField;
                }
                set
                {
                    this.permissionScopeField = value;
                }
            }

            /// <remarks/>
            public string effectiveStartDate
            {
                get
                {
                    return this.effectiveStartDateField;
                }
                set
                {
                    this.effectiveStartDateField = value;
                }
            }

            /// <remarks/>
            public string effectiveEndDate
            {
                get
                {
                    return this.effectiveEndDateField;
                }
                set
                {
                    this.effectiveEndDateField = value;
                }
            }

            /// <remarks/>
            public informationAccessPermissionListHIPAAPermissionPermittedPerson permittedPerson
            {
                get
                {
                    return this.permittedPersonField;
                }
                set
                {
                    this.permittedPersonField = value;
                }
            }

            /// <remarks/>
            public informationAccessPermissionListHIPAAPermissionHIPAAPermissionGroupPhoneList hIPAAPermissionGroupPhoneList
            {
                get
                {
                    return this.hIPAAPermissionGroupPhoneListField;
                }
                set
                {
                    this.hIPAAPermissionGroupPhoneListField = value;
                }
            }

            /// <remarks/>
            public informationAccessPermissionListHIPAAPermissionRelationshipToMemberCode relationshipToMemberCode
            {
                get
                {
                    return this.relationshipToMemberCodeField;
                }
                set
                {
                    this.relationshipToMemberCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class informationAccessPermissionListHIPAAPermissionPermittedPerson
        {

            private string lastNameField;

            private string firstNameField;

            private string middleNameField;

            private string namePrefixField;

            private string nameSuffixField;

            private informationAccessPermissionListHIPAAPermissionPermittedPersonNameSuffixList nameSuffixListField;

            private informationAccessPermissionListHIPAAPermissionPermittedPersonCredentialSuffixList credentialSuffixListField;

            /// <remarks/>
            public string lastName
            {
                get
                {
                    return this.lastNameField;
                }
                set
                {
                    this.lastNameField = value;
                }
            }

            /// <remarks/>
            public string firstName
            {
                get
                {
                    return this.firstNameField;
                }
                set
                {
                    this.firstNameField = value;
                }
            }

            /// <remarks/>
            public string middleName
            {
                get
                {
                    return this.middleNameField;
                }
                set
                {
                    this.middleNameField = value;
                }
            }

            /// <remarks/>
            public string namePrefix
            {
                get
                {
                    return this.namePrefixField;
                }
                set
                {
                    this.namePrefixField = value;
                }
            }

            /// <remarks/>
            public string nameSuffix
            {
                get
                {
                    return this.nameSuffixField;
                }
                set
                {
                    this.nameSuffixField = value;
                }
            }

            /// <remarks/>
            public informationAccessPermissionListHIPAAPermissionPermittedPersonNameSuffixList nameSuffixList
            {
                get
                {
                    return this.nameSuffixListField;
                }
                set
                {
                    this.nameSuffixListField = value;
                }
            }

            /// <remarks/>
            public informationAccessPermissionListHIPAAPermissionPermittedPersonCredentialSuffixList credentialSuffixList
            {
                get
                {
                    return this.credentialSuffixListField;
                }
                set
                {
                    this.credentialSuffixListField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class informationAccessPermissionListHIPAAPermissionPermittedPersonNameSuffixList
        {

            private string listModeField;

            private string[] suffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("suffix")]
            public string[] suffix
            {
                get
                {
                    return this.suffixField;
                }
                set
                {
                    this.suffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class informationAccessPermissionListHIPAAPermissionPermittedPersonCredentialSuffixList
        {

            private string listModeField;

            private informationAccessPermissionListHIPAAPermissionPermittedPersonCredentialSuffixListCredentialSuffix[] credentialSuffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("CredentialSuffix")]
            public informationAccessPermissionListHIPAAPermissionPermittedPersonCredentialSuffixListCredentialSuffix[] CredentialSuffix
            {
                get
                {
                    return this.credentialSuffixField;
                }
                set
                {
                    this.credentialSuffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class informationAccessPermissionListHIPAAPermissionPermittedPersonCredentialSuffixListCredentialSuffix
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class informationAccessPermissionListHIPAAPermissionHIPAAPermissionGroupPhoneList
        {

            private string listModeField;

            private informationAccessPermissionListHIPAAPermissionHIPAAPermissionGroupPhoneListTelephoneNumber[] telephoneNumberField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("telephoneNumber")]
            public informationAccessPermissionListHIPAAPermissionHIPAAPermissionGroupPhoneListTelephoneNumber[] telephoneNumber
            {
                get
                {
                    return this.telephoneNumberField;
                }
                set
                {
                    this.telephoneNumberField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class informationAccessPermissionListHIPAAPermissionHIPAAPermissionGroupPhoneListTelephoneNumber
        {

            private string phoneCountryCodeField;

            private string phoneAreaCodeField;

            private string phoneNumberField;

            private string phoneExtensionNumberField;

            private informationAccessPermissionListHIPAAPermissionHIPAAPermissionGroupPhoneListTelephoneNumberIndividualPhoneTypeCode individualPhoneTypeCodeField;

            private informationAccessPermissionListHIPAAPermissionHIPAAPermissionGroupPhoneListTelephoneNumberOrganizationPhoneTypeCode organizationPhoneTypeCodeField;

            /// <remarks/>
            public string phoneCountryCode
            {
                get
                {
                    return this.phoneCountryCodeField;
                }
                set
                {
                    this.phoneCountryCodeField = value;
                }
            }

            /// <remarks/>
            public string phoneAreaCode
            {
                get
                {
                    return this.phoneAreaCodeField;
                }
                set
                {
                    this.phoneAreaCodeField = value;
                }
            }

            /// <remarks/>
            public string phoneNumber
            {
                get
                {
                    return this.phoneNumberField;
                }
                set
                {
                    this.phoneNumberField = value;
                }
            }

            /// <remarks/>
            public string phoneExtensionNumber
            {
                get
                {
                    return this.phoneExtensionNumberField;
                }
                set
                {
                    this.phoneExtensionNumberField = value;
                }
            }

            /// <remarks/>
            public informationAccessPermissionListHIPAAPermissionHIPAAPermissionGroupPhoneListTelephoneNumberIndividualPhoneTypeCode individualPhoneTypeCode
            {
                get
                {
                    return this.individualPhoneTypeCodeField;
                }
                set
                {
                    this.individualPhoneTypeCodeField = value;
                }
            }

            /// <remarks/>
            public informationAccessPermissionListHIPAAPermissionHIPAAPermissionGroupPhoneListTelephoneNumberOrganizationPhoneTypeCode organizationPhoneTypeCode
            {
                get
                {
                    return this.organizationPhoneTypeCodeField;
                }
                set
                {
                    this.organizationPhoneTypeCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class informationAccessPermissionListHIPAAPermissionHIPAAPermissionGroupPhoneListTelephoneNumberIndividualPhoneTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class informationAccessPermissionListHIPAAPermissionHIPAAPermissionGroupPhoneListTelephoneNumberOrganizationPhoneTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class informationAccessPermissionListHIPAAPermissionRelationshipToMemberCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class informationAccessRestrictionList
        {

            private string listModeField;

            private informationAccessRestrictionListHIPAARestriction[] hIPAARestrictionField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("HIPAARestriction")]
            public informationAccessRestrictionListHIPAARestriction[] HIPAARestriction
            {
                get
                {
                    return this.hIPAARestrictionField;
                }
                set
                {
                    this.hIPAARestrictionField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class informationAccessRestrictionListHIPAARestriction
        {

            private string restrictionScopeField;

            private string effectiveStartDateField;

            private string effectiveEndDateField;

            private informationAccessRestrictionListHIPAARestrictionRestrictedPerson restrictedPersonField;

            private informationAccessRestrictionListHIPAARestrictionRelationshipToMemberCode relationshipToMemberCodeField;

            /// <remarks/>
            public string restrictionScope
            {
                get
                {
                    return this.restrictionScopeField;
                }
                set
                {
                    this.restrictionScopeField = value;
                }
            }

            /// <remarks/>
            public string effectiveStartDate
            {
                get
                {
                    return this.effectiveStartDateField;
                }
                set
                {
                    this.effectiveStartDateField = value;
                }
            }

            /// <remarks/>
            public string effectiveEndDate
            {
                get
                {
                    return this.effectiveEndDateField;
                }
                set
                {
                    this.effectiveEndDateField = value;
                }
            }

            /// <remarks/>
            public informationAccessRestrictionListHIPAARestrictionRestrictedPerson restrictedPerson
            {
                get
                {
                    return this.restrictedPersonField;
                }
                set
                {
                    this.restrictedPersonField = value;
                }
            }

            /// <remarks/>
            public informationAccessRestrictionListHIPAARestrictionRelationshipToMemberCode relationshipToMemberCode
            {
                get
                {
                    return this.relationshipToMemberCodeField;
                }
                set
                {
                    this.relationshipToMemberCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class informationAccessRestrictionListHIPAARestrictionRestrictedPerson
        {

            private string lastNameField;

            private string firstNameField;

            private string middleNameField;

            private string namePrefixField;

            private string nameSuffixField;

            private informationAccessRestrictionListHIPAARestrictionRestrictedPersonNameSuffixList nameSuffixListField;

            private informationAccessRestrictionListHIPAARestrictionRestrictedPersonCredentialSuffixList credentialSuffixListField;

            /// <remarks/>
            public string lastName
            {
                get
                {
                    return this.lastNameField;
                }
                set
                {
                    this.lastNameField = value;
                }
            }

            /// <remarks/>
            public string firstName
            {
                get
                {
                    return this.firstNameField;
                }
                set
                {
                    this.firstNameField = value;
                }
            }

            /// <remarks/>
            public string middleName
            {
                get
                {
                    return this.middleNameField;
                }
                set
                {
                    this.middleNameField = value;
                }
            }

            /// <remarks/>
            public string namePrefix
            {
                get
                {
                    return this.namePrefixField;
                }
                set
                {
                    this.namePrefixField = value;
                }
            }

            /// <remarks/>
            public string nameSuffix
            {
                get
                {
                    return this.nameSuffixField;
                }
                set
                {
                    this.nameSuffixField = value;
                }
            }

            /// <remarks/>
            public informationAccessRestrictionListHIPAARestrictionRestrictedPersonNameSuffixList nameSuffixList
            {
                get
                {
                    return this.nameSuffixListField;
                }
                set
                {
                    this.nameSuffixListField = value;
                }
            }

            /// <remarks/>
            public informationAccessRestrictionListHIPAARestrictionRestrictedPersonCredentialSuffixList credentialSuffixList
            {
                get
                {
                    return this.credentialSuffixListField;
                }
                set
                {
                    this.credentialSuffixListField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class informationAccessRestrictionListHIPAARestrictionRestrictedPersonNameSuffixList
        {

            private string listModeField;

            private string[] suffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("suffix")]
            public string[] suffix
            {
                get
                {
                    return this.suffixField;
                }
                set
                {
                    this.suffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class informationAccessRestrictionListHIPAARestrictionRestrictedPersonCredentialSuffixList
        {

            private string listModeField;

            private informationAccessRestrictionListHIPAARestrictionRestrictedPersonCredentialSuffixListCredentialSuffix[] credentialSuffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("CredentialSuffix")]
            public informationAccessRestrictionListHIPAARestrictionRestrictedPersonCredentialSuffixListCredentialSuffix[] CredentialSuffix
            {
                get
                {
                    return this.credentialSuffixField;
                }
                set
                {
                    this.credentialSuffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class informationAccessRestrictionListHIPAARestrictionRestrictedPersonCredentialSuffixListCredentialSuffix
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class informationAccessRestrictionListHIPAARestrictionRelationshipToMemberCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class phiAuthorizationList
        {

            private string listModeField;

            private phiAuthorizationListHIPAAAuthorization[] hIPAAAuthorizationField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("HIPAAAuthorization")]
            public phiAuthorizationListHIPAAAuthorization[] HIPAAAuthorization
            {
                get
                {
                    return this.hIPAAAuthorizationField;
                }
                set
                {
                    this.hIPAAAuthorizationField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class phiAuthorizationListHIPAAAuthorization
        {

            private string authorizationTypeField;

            private string authorizationScopeField;

            private string effectiveStartDateField;

            private string effectiveEndDateField;

            private string whoCanBeDisclosedToField;

            private string signatureField;

            private string signatureDateField;

            private phiAuthorizationListHIPAAAuthorizationAuthorizationPurposeCode authorizationPurposeCodeField;

            /// <remarks/>
            public string authorizationType
            {
                get
                {
                    return this.authorizationTypeField;
                }
                set
                {
                    this.authorizationTypeField = value;
                }
            }

            /// <remarks/>
            public string authorizationScope
            {
                get
                {
                    return this.authorizationScopeField;
                }
                set
                {
                    this.authorizationScopeField = value;
                }
            }

            /// <remarks/>
            public string effectiveStartDate
            {
                get
                {
                    return this.effectiveStartDateField;
                }
                set
                {
                    this.effectiveStartDateField = value;
                }
            }

            /// <remarks/>
            public string effectiveEndDate
            {
                get
                {
                    return this.effectiveEndDateField;
                }
                set
                {
                    this.effectiveEndDateField = value;
                }
            }

            /// <remarks/>
            public string whoCanBeDisclosedTo
            {
                get
                {
                    return this.whoCanBeDisclosedToField;
                }
                set
                {
                    this.whoCanBeDisclosedToField = value;
                }
            }

            /// <remarks/>
            public string signature
            {
                get
                {
                    return this.signatureField;
                }
                set
                {
                    this.signatureField = value;
                }
            }

            /// <remarks/>
            public string signatureDate
            {
                get
                {
                    return this.signatureDateField;
                }
                set
                {
                    this.signatureDateField = value;
                }
            }

            /// <remarks/>
            public phiAuthorizationListHIPAAAuthorizationAuthorizationPurposeCode authorizationPurposeCode
            {
                get
                {
                    return this.authorizationPurposeCodeField;
                }
                set
                {
                    this.authorizationPurposeCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class phiAuthorizationListHIPAAAuthorizationAuthorizationPurposeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class ccDirectiveList
        {

            private string listModeField;

            private ccDirectiveListConfidentialCommunicationDirective[] confidentialCommunicationDirectiveField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("confidentialCommunicationDirective")]
            public ccDirectiveListConfidentialCommunicationDirective[] confidentialCommunicationDirective
            {
                get
                {
                    return this.confidentialCommunicationDirectiveField;
                }
                set
                {
                    this.confidentialCommunicationDirectiveField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class ccDirectiveListConfidentialCommunicationDirective
        {

            private string confidentialCommunicationScopeField;

            private string effectiveStartDateField;

            private string effectiveEndDateField;

            private ccDirectiveListConfidentialCommunicationDirectiveConfidentialCommunicationTypeCode confidentialCommunicationTypeCodeField;

            /// <remarks/>
            public string confidentialCommunicationScope
            {
                get
                {
                    return this.confidentialCommunicationScopeField;
                }
                set
                {
                    this.confidentialCommunicationScopeField = value;
                }
            }

            /// <remarks/>
            public string effectiveStartDate
            {
                get
                {
                    return this.effectiveStartDateField;
                }
                set
                {
                    this.effectiveStartDateField = value;
                }
            }

            /// <remarks/>
            public string effectiveEndDate
            {
                get
                {
                    return this.effectiveEndDateField;
                }
                set
                {
                    this.effectiveEndDateField = value;
                }
            }

            /// <remarks/>
            public ccDirectiveListConfidentialCommunicationDirectiveConfidentialCommunicationTypeCode confidentialCommunicationTypeCode
            {
                get
                {
                    return this.confidentialCommunicationTypeCodeField;
                }
                set
                {
                    this.confidentialCommunicationTypeCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class ccDirectiveListConfidentialCommunicationDirectiveConfidentialCommunicationTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class subscriptionReference
        {

            private string idField;

            /// <remarks/>
            public string ID
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class individualReference
        {

            private string idField;

            /// <remarks/>
            public string ID
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class relationshipToSubscriberDefinitionReference
        {

            private relationshipToSubscriberDefinitionReferenceReference referenceField;

            private string relationshipNameField;

            private string domainNameField;

            /// <remarks/>
            public relationshipToSubscriberDefinitionReferenceReference reference
            {
                get
                {
                    return this.referenceField;
                }
                set
                {
                    this.referenceField = value;
                }
            }

            /// <remarks/>
            public string relationshipName
            {
                get
                {
                    return this.relationshipNameField;
                }
                set
                {
                    this.relationshipNameField = value;
                }
            }

            /// <remarks/>
            public string domainName
            {
                get
                {
                    return this.domainNameField;
                }
                set
                {
                    this.domainNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class relationshipToSubscriberDefinitionReferenceReference
        {

            private string idField;

            /// <remarks/>
            public string ID
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class raceOrEthnicity
        {

            private string listModeField;

            private raceOrEthnicityRaceOrEthnicityCodes[] raceOrEthnicityCodesField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("raceOrEthnicityCodes")]
            public raceOrEthnicityRaceOrEthnicityCodes[] raceOrEthnicityCodes
            {
                get
                {
                    return this.raceOrEthnicityCodesField;
                }
                set
                {
                    this.raceOrEthnicityCodesField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class raceOrEthnicityRaceOrEthnicityCodes
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class vipReasonCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class healthStatusReasonCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class informationSourceCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class attachmentSetReference
        {

            private string idField;

            /// <remarks/>
            public string ID
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class planSelection
        {

            private string startDateField;

            private string endDateField;

            private string claimHistoryFromOtherPlansField;

            private planSelectionEnrollmentInfo enrollmentInfoField;

            private planSelectionMemberTerminateReasonCode memberTerminateReasonCodeField;

            private planSelectionSpecialEnrollmentPeriodReasonCode specialEnrollmentPeriodReasonCodeField;

            private bool waiveBenefitWaitingPeriodField;

            private string planDescriptionField;

            private string planCodeField;

            private string classOfContractNumberField;

            private string programCodeField;

            private planSelectionBenefitPlanMatchData benefitPlanMatchDataField;

            private planSelectionBenefitPlanReference benefitPlanReferenceField;

            private string healthCoverageMaintenanceCodeField;

            private string insuranceLineCodeField;

            private string[] benefitNetworksField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }

            /// <remarks/>
            public string claimHistoryFromOtherPlans
            {
                get
                {
                    return this.claimHistoryFromOtherPlansField;
                }
                set
                {
                    this.claimHistoryFromOtherPlansField = value;
                }
            }

            /// <remarks/>
            public planSelectionEnrollmentInfo enrollmentInfo
            {
                get
                {
                    return this.enrollmentInfoField;
                }
                set
                {
                    this.enrollmentInfoField = value;
                }
            }

            /// <remarks/>
            public planSelectionMemberTerminateReasonCode memberTerminateReasonCode
            {
                get
                {
                    return this.memberTerminateReasonCodeField;
                }
                set
                {
                    this.memberTerminateReasonCodeField = value;
                }
            }

            /// <remarks/>
            public planSelectionSpecialEnrollmentPeriodReasonCode specialEnrollmentPeriodReasonCode
            {
                get
                {
                    return this.specialEnrollmentPeriodReasonCodeField;
                }
                set
                {
                    this.specialEnrollmentPeriodReasonCodeField = value;
                }
            }

            /// <remarks/>
            public bool waiveBenefitWaitingPeriod
            {
                get
                {
                    return this.waiveBenefitWaitingPeriodField;
                }
                set
                {
                    this.waiveBenefitWaitingPeriodField = value;
                }
            }

            /// <remarks/>
            public string planDescription
            {
                get
                {
                    return this.planDescriptionField;
                }
                set
                {
                    this.planDescriptionField = value;
                }
            }

            /// <remarks/>
            public string planCode
            {
                get
                {
                    return this.planCodeField;
                }
                set
                {
                    this.planCodeField = value;
                }
            }

            /// <remarks/>
            public string classOfContractNumber
            {
                get
                {
                    return this.classOfContractNumberField;
                }
                set
                {
                    this.classOfContractNumberField = value;
                }
            }

            /// <remarks/>
            public string programCode
            {
                get
                {
                    return this.programCodeField;
                }
                set
                {
                    this.programCodeField = value;
                }
            }

            /// <remarks/>
            public planSelectionBenefitPlanMatchData benefitPlanMatchData
            {
                get
                {
                    return this.benefitPlanMatchDataField;
                }
                set
                {
                    this.benefitPlanMatchDataField = value;
                }
            }

            /// <remarks/>
            public planSelectionBenefitPlanReference benefitPlanReference
            {
                get
                {
                    return this.benefitPlanReferenceField;
                }
                set
                {
                    this.benefitPlanReferenceField = value;
                }
            }

            /// <remarks/>
            public string healthCoverageMaintenanceCode
            {
                get
                {
                    return this.healthCoverageMaintenanceCodeField;
                }
                set
                {
                    this.healthCoverageMaintenanceCodeField = value;
                }
            }

            /// <remarks/>
            public string insuranceLineCode
            {
                get
                {
                    return this.insuranceLineCodeField;
                }
                set
                {
                    this.insuranceLineCodeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlArrayItemAttribute("benefitNetworkName", IsNullable = false)]
            public string[] benefitNetworks
            {
                get
                {
                    return this.benefitNetworksField;
                }
                set
                {
                    this.benefitNetworksField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class planSelectionEnrollmentInfo
        {

            private string applicationSignatureDomainField;

            private string applicationSignatureDateField;

            private string applicationReceiptDateField;

            private string completedDateField;

            private string submitDateField;

            private string disenrollDateField;

            private string disenrollReasonField;

            private planSelectionEnrollmentInfoMedicareEnrollmentPeriodCode medicareEnrollmentPeriodCodeField;

            /// <remarks/>
            public string applicationSignatureDomain
            {
                get
                {
                    return this.applicationSignatureDomainField;
                }
                set
                {
                    this.applicationSignatureDomainField = value;
                }
            }

            /// <remarks/>
            public string applicationSignatureDate
            {
                get
                {
                    return this.applicationSignatureDateField;
                }
                set
                {
                    this.applicationSignatureDateField = value;
                }
            }

            /// <remarks/>
            public string applicationReceiptDate
            {
                get
                {
                    return this.applicationReceiptDateField;
                }
                set
                {
                    this.applicationReceiptDateField = value;
                }
            }

            /// <remarks/>
            public string completedDate
            {
                get
                {
                    return this.completedDateField;
                }
                set
                {
                    this.completedDateField = value;
                }
            }

            /// <remarks/>
            public string submitDate
            {
                get
                {
                    return this.submitDateField;
                }
                set
                {
                    this.submitDateField = value;
                }
            }

            /// <remarks/>
            public string disenrollDate
            {
                get
                {
                    return this.disenrollDateField;
                }
                set
                {
                    this.disenrollDateField = value;
                }
            }

            /// <remarks/>
            public string disenrollReason
            {
                get
                {
                    return this.disenrollReasonField;
                }
                set
                {
                    this.disenrollReasonField = value;
                }
            }

            /// <remarks/>
            public planSelectionEnrollmentInfoMedicareEnrollmentPeriodCode medicareEnrollmentPeriodCode
            {
                get
                {
                    return this.medicareEnrollmentPeriodCodeField;
                }
                set
                {
                    this.medicareEnrollmentPeriodCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class planSelectionEnrollmentInfoMedicareEnrollmentPeriodCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class planSelectionMemberTerminateReasonCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class planSelectionSpecialEnrollmentPeriodReasonCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class planSelectionBenefitPlanMatchData
        {

            private string benefitPlanNameField;

            private string benefitPlanHccIdField;

            private planSelectionBenefitPlanMatchDataBenefitPlanOtherIdentifier benefitPlanOtherIdentifierField;

            private string asOfDateField;

            /// <remarks/>
            public string benefitPlanName
            {
                get
                {
                    return this.benefitPlanNameField;
                }
                set
                {
                    this.benefitPlanNameField = value;
                }
            }

            /// <remarks/>
            public string benefitPlanHccId
            {
                get
                {
                    return this.benefitPlanHccIdField;
                }
                set
                {
                    this.benefitPlanHccIdField = value;
                }
            }

            /// <remarks/>
            public planSelectionBenefitPlanMatchDataBenefitPlanOtherIdentifier benefitPlanOtherIdentifier
            {
                get
                {
                    return this.benefitPlanOtherIdentifierField;
                }
                set
                {
                    this.benefitPlanOtherIdentifierField = value;
                }
            }

            /// <remarks/>
            public string asOfDate
            {
                get
                {
                    return this.asOfDateField;
                }
                set
                {
                    this.asOfDateField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class planSelectionBenefitPlanMatchDataBenefitPlanOtherIdentifier
        {

            private string otherIDField;

            private planSelectionBenefitPlanMatchDataBenefitPlanOtherIdentifierIdentificationType identificationTypeField;

            /// <remarks/>
            public string otherID
            {
                get
                {
                    return this.otherIDField;
                }
                set
                {
                    this.otherIDField = value;
                }
            }

            /// <remarks/>
            public planSelectionBenefitPlanMatchDataBenefitPlanOtherIdentifierIdentificationType identificationType
            {
                get
                {
                    return this.identificationTypeField;
                }
                set
                {
                    this.identificationTypeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class planSelectionBenefitPlanMatchDataBenefitPlanOtherIdentifierIdentificationType
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class planSelectionBenefitPlanReference
        {

            private string identifierField;

            /// <remarks/>
            public string identifier
            {
                get
                {
                    return this.identifierField;
                }
                set
                {
                    this.identifierField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class providerSelections
        {

            private string listModeField;

            private providerSelectionsProviderSelection[] providerSelectionField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("providerSelection")]
            public providerSelectionsProviderSelection[] providerSelection
            {
                get
                {
                    return this.providerSelectionField;
                }
                set
                {
                    this.providerSelectionField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class providerSelectionsProviderSelection
        {

            private string providerRoleTypeField;

            private providerSelectionsProviderSelectionProviderDateRanges[] providerDateRangesField;

            /// <remarks/>
            public string providerRoleType
            {
                get
                {
                    return this.providerRoleTypeField;
                }
                set
                {
                    this.providerRoleTypeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("providerDateRanges")]
            public providerSelectionsProviderSelectionProviderDateRanges[] providerDateRanges
            {
                get
                {
                    return this.providerDateRangesField;
                }
                set
                {
                    this.providerDateRangesField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class providerSelectionsProviderSelectionProviderDateRanges
        {

            private string startDateField;

            private string endDateField;

            private providerSelectionsProviderSelectionProviderDateRangesProviderMatch providerMatchField;

            private providerSelectionsProviderSelectionProviderDateRangesPractitionerReference practitionerReferenceField;

            private providerSelectionsProviderSelectionProviderDateRangesPractitionerRoleReference practitionerRoleReferenceField;

            private providerSelectionsProviderSelectionProviderDateRangesSupplierReference supplierReferenceField;

            private providerSelectionsProviderSelectionProviderDateRangesAffiliatedSupplierNetworkReference affiliatedSupplierNetworkReferenceField;

            private providerSelectionsProviderSelectionProviderDateRangesSupplierLocationReference supplierLocationReferenceField;

            private providerSelectionsProviderSelectionProviderDateRangesSupplierNetworkReference supplierNetworkReferenceField;

            private bool pcpAutoAssignedField;

            private providerSelectionsProviderSelectionProviderDateRangesProviderSelectionReason providerSelectionReasonField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }

            /// <remarks/>
            public providerSelectionsProviderSelectionProviderDateRangesProviderMatch providerMatch
            {
                get
                {
                    return this.providerMatchField;
                }
                set
                {
                    this.providerMatchField = value;
                }
            }

            /// <remarks/>
            public providerSelectionsProviderSelectionProviderDateRangesPractitionerReference practitionerReference
            {
                get
                {
                    return this.practitionerReferenceField;
                }
                set
                {
                    this.practitionerReferenceField = value;
                }
            }

            /// <remarks/>
            public providerSelectionsProviderSelectionProviderDateRangesPractitionerRoleReference practitionerRoleReference
            {
                get
                {
                    return this.practitionerRoleReferenceField;
                }
                set
                {
                    this.practitionerRoleReferenceField = value;
                }
            }

            /// <remarks/>
            public providerSelectionsProviderSelectionProviderDateRangesSupplierReference supplierReference
            {
                get
                {
                    return this.supplierReferenceField;
                }
                set
                {
                    this.supplierReferenceField = value;
                }
            }

            /// <remarks/>
            public providerSelectionsProviderSelectionProviderDateRangesAffiliatedSupplierNetworkReference affiliatedSupplierNetworkReference
            {
                get
                {
                    return this.affiliatedSupplierNetworkReferenceField;
                }
                set
                {
                    this.affiliatedSupplierNetworkReferenceField = value;
                }
            }

            /// <remarks/>
            public providerSelectionsProviderSelectionProviderDateRangesSupplierLocationReference supplierLocationReference
            {
                get
                {
                    return this.supplierLocationReferenceField;
                }
                set
                {
                    this.supplierLocationReferenceField = value;
                }
            }

            /// <remarks/>
            public providerSelectionsProviderSelectionProviderDateRangesSupplierNetworkReference supplierNetworkReference
            {
                get
                {
                    return this.supplierNetworkReferenceField;
                }
                set
                {
                    this.supplierNetworkReferenceField = value;
                }
            }

            /// <remarks/>
            public bool pcpAutoAssigned
            {
                get
                {
                    return this.pcpAutoAssignedField;
                }
                set
                {
                    this.pcpAutoAssignedField = value;
                }
            }

            /// <remarks/>
            public providerSelectionsProviderSelectionProviderDateRangesProviderSelectionReason providerSelectionReason
            {
                get
                {
                    return this.providerSelectionReasonField;
                }
                set
                {
                    this.providerSelectionReasonField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class providerSelectionsProviderSelectionProviderDateRangesProviderMatch
        {

            private string asOfDateField;

            private providerSelectionsProviderSelectionProviderDateRangesProviderMatchPractitioner practitionerField;

            private providerSelectionsProviderSelectionProviderDateRangesProviderMatchPractitionerRole practitionerRoleField;

            private providerSelectionsProviderSelectionProviderDateRangesProviderMatchSupplier supplierField;

            private providerSelectionsProviderSelectionProviderDateRangesProviderMatchAffiliatedSupplierNetwork affiliatedSupplierNetworkField;

            private providerSelectionsProviderSelectionProviderDateRangesProviderMatchSupplierLocation supplierLocationField;

            private providerSelectionsProviderSelectionProviderDateRangesProviderMatchSupplierNetwork supplierNetworkField;

            /// <remarks/>
            public string asOfDate
            {
                get
                {
                    return this.asOfDateField;
                }
                set
                {
                    this.asOfDateField = value;
                }
            }

            /// <remarks/>
            public providerSelectionsProviderSelectionProviderDateRangesProviderMatchPractitioner practitioner
            {
                get
                {
                    return this.practitionerField;
                }
                set
                {
                    this.practitionerField = value;
                }
            }

            /// <remarks/>
            public providerSelectionsProviderSelectionProviderDateRangesProviderMatchPractitionerRole practitionerRole
            {
                get
                {
                    return this.practitionerRoleField;
                }
                set
                {
                    this.practitionerRoleField = value;
                }
            }

            /// <remarks/>
            public providerSelectionsProviderSelectionProviderDateRangesProviderMatchSupplier supplier
            {
                get
                {
                    return this.supplierField;
                }
                set
                {
                    this.supplierField = value;
                }
            }

            /// <remarks/>
            public providerSelectionsProviderSelectionProviderDateRangesProviderMatchAffiliatedSupplierNetwork affiliatedSupplierNetwork
            {
                get
                {
                    return this.affiliatedSupplierNetworkField;
                }
                set
                {
                    this.affiliatedSupplierNetworkField = value;
                }
            }

            /// <remarks/>
            public providerSelectionsProviderSelectionProviderDateRangesProviderMatchSupplierLocation supplierLocation
            {
                get
                {
                    return this.supplierLocationField;
                }
                set
                {
                    this.supplierLocationField = value;
                }
            }

            /// <remarks/>
            public providerSelectionsProviderSelectionProviderDateRangesProviderMatchSupplierNetwork supplierNetwork
            {
                get
                {
                    return this.supplierNetworkField;
                }
                set
                {
                    this.supplierNetworkField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class providerSelectionsProviderSelectionProviderDateRangesProviderMatchPractitioner
        {

            private string practitionerOtherIdentificationNumberField;

            private string firstNameField;

            private string lastNameField;

            private string hccIdentificationNumberField;

            /// <remarks/>
            public string practitionerOtherIdentificationNumber
            {
                get
                {
                    return this.practitionerOtherIdentificationNumberField;
                }
                set
                {
                    this.practitionerOtherIdentificationNumberField = value;
                }
            }

            /// <remarks/>
            public string firstName
            {
                get
                {
                    return this.firstNameField;
                }
                set
                {
                    this.firstNameField = value;
                }
            }

            /// <remarks/>
            public string lastName
            {
                get
                {
                    return this.lastNameField;
                }
                set
                {
                    this.lastNameField = value;
                }
            }

            /// <remarks/>
            public string hccIdentificationNumber
            {
                get
                {
                    return this.hccIdentificationNumberField;
                }
                set
                {
                    this.hccIdentificationNumberField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class providerSelectionsProviderSelectionProviderDateRangesProviderMatchPractitionerRole
        {

            private string hccIdentificationNumberField;

            private string practitionerRoleIdentificationNumberField;

            private string roleNameField;

            private string practitionerRoleOtherIdentificationNumberField;

            private providerSelectionsProviderSelectionProviderDateRangesProviderMatchPractitionerRolePractitionerRoleOtherIdTypeCode practitionerRoleOtherIdTypeCodeField;

            /// <remarks/>
            public string hccIdentificationNumber
            {
                get
                {
                    return this.hccIdentificationNumberField;
                }
                set
                {
                    this.hccIdentificationNumberField = value;
                }
            }

            /// <remarks/>
            public string practitionerRoleIdentificationNumber
            {
                get
                {
                    return this.practitionerRoleIdentificationNumberField;
                }
                set
                {
                    this.practitionerRoleIdentificationNumberField = value;
                }
            }

            /// <remarks/>
            public string roleName
            {
                get
                {
                    return this.roleNameField;
                }
                set
                {
                    this.roleNameField = value;
                }
            }

            /// <remarks/>
            public string practitionerRoleOtherIdentificationNumber
            {
                get
                {
                    return this.practitionerRoleOtherIdentificationNumberField;
                }
                set
                {
                    this.practitionerRoleOtherIdentificationNumberField = value;
                }
            }

            /// <remarks/>
            public providerSelectionsProviderSelectionProviderDateRangesProviderMatchPractitionerRolePractitionerRoleOtherIdTypeCode practitionerRoleOtherIdTypeCode
            {
                get
                {
                    return this.practitionerRoleOtherIdTypeCodeField;
                }
                set
                {
                    this.practitionerRoleOtherIdTypeCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class providerSelectionsProviderSelectionProviderDateRangesProviderMatchPractitionerRolePractitionerRoleOtherIdTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class providerSelectionsProviderSelectionProviderDateRangesProviderMatchSupplier
        {

            private string supplierOtherIdentificationNumberField;

            private string primaryNameField;

            private string hccIdentificationNumberField;

            /// <remarks/>
            public string supplierOtherIdentificationNumber
            {
                get
                {
                    return this.supplierOtherIdentificationNumberField;
                }
                set
                {
                    this.supplierOtherIdentificationNumberField = value;
                }
            }

            /// <remarks/>
            public string primaryName
            {
                get
                {
                    return this.primaryNameField;
                }
                set
                {
                    this.primaryNameField = value;
                }
            }

            /// <remarks/>
            public string hccIdentificationNumber
            {
                get
                {
                    return this.hccIdentificationNumberField;
                }
                set
                {
                    this.hccIdentificationNumberField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class providerSelectionsProviderSelectionProviderDateRangesProviderMatchAffiliatedSupplierNetwork
        {

            private string hccIdentificationNumberField;

            /// <remarks/>
            public string hccIdentificationNumber
            {
                get
                {
                    return this.hccIdentificationNumberField;
                }
                set
                {
                    this.hccIdentificationNumberField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class providerSelectionsProviderSelectionProviderDateRangesProviderMatchSupplierLocation
        {

            private string hccIdentificationNumberField;

            /// <remarks/>
            public string hccIdentificationNumber
            {
                get
                {
                    return this.hccIdentificationNumberField;
                }
                set
                {
                    this.hccIdentificationNumberField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class providerSelectionsProviderSelectionProviderDateRangesProviderMatchSupplierNetwork
        {

            private string hccIdentificationNumberField;

            /// <remarks/>
            public string hccIdentificationNumber
            {
                get
                {
                    return this.hccIdentificationNumberField;
                }
                set
                {
                    this.hccIdentificationNumberField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class providerSelectionsProviderSelectionProviderDateRangesPractitionerReference
        {

            private string identifierField;

            /// <remarks/>
            public string identifier
            {
                get
                {
                    return this.identifierField;
                }
                set
                {
                    this.identifierField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class providerSelectionsProviderSelectionProviderDateRangesPractitionerRoleReference
        {

            private string identifierField;

            /// <remarks/>
            public string identifier
            {
                get
                {
                    return this.identifierField;
                }
                set
                {
                    this.identifierField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class providerSelectionsProviderSelectionProviderDateRangesSupplierReference
        {

            private string identifierField;

            /// <remarks/>
            public string identifier
            {
                get
                {
                    return this.identifierField;
                }
                set
                {
                    this.identifierField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class providerSelectionsProviderSelectionProviderDateRangesAffiliatedSupplierNetworkReference
        {

            private string identifierField;

            /// <remarks/>
            public string identifier
            {
                get
                {
                    return this.identifierField;
                }
                set
                {
                    this.identifierField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class providerSelectionsProviderSelectionProviderDateRangesSupplierLocationReference
        {

            private string identifierField;

            /// <remarks/>
            public string identifier
            {
                get
                {
                    return this.identifierField;
                }
                set
                {
                    this.identifierField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class providerSelectionsProviderSelectionProviderDateRangesSupplierNetworkReference
        {

            private string identifierField;

            /// <remarks/>
            public string identifier
            {
                get
                {
                    return this.identifierField;
                }
                set
                {
                    this.identifierField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class providerSelectionsProviderSelectionProviderDateRangesProviderSelectionReason
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class reportingCategory
        {

            private string entityIdentifierCodeField;

            private string reportingCategoryNameField;

            private string referenceQualifierIdentifierField;

            private string referenceIdField;

            private string amountField;

            private string startDateField;

            private string endDateField;

            private string counterField;

            /// <remarks/>
            public string entityIdentifierCode
            {
                get
                {
                    return this.entityIdentifierCodeField;
                }
                set
                {
                    this.entityIdentifierCodeField = value;
                }
            }

            /// <remarks/>
            public string reportingCategoryName
            {
                get
                {
                    return this.reportingCategoryNameField;
                }
                set
                {
                    this.reportingCategoryNameField = value;
                }
            }

            /// <remarks/>
            public string referenceQualifierIdentifier
            {
                get
                {
                    return this.referenceQualifierIdentifierField;
                }
                set
                {
                    this.referenceQualifierIdentifierField = value;
                }
            }

            /// <remarks/>
            public string referenceId
            {
                get
                {
                    return this.referenceIdField;
                }
                set
                {
                    this.referenceIdField = value;
                }
            }

            /// <remarks/>
            public string amount
            {
                get
                {
                    return this.amountField;
                }
                set
                {
                    this.amountField = value;
                }
            }

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string counter
            {
                get
                {
                    return this.counterField;
                }
                set
                {
                    this.counterField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class medicareHICN
        {

            private string hicnField;

            private string mbiField;

            private string effectiveDateField;

            private string expirationDateField;

            private string entryDateField;

            private string waitingPeriodStartDateField;

            private string waitingPeriodEndDateField;

            private string nursingHomeCompareIndicatorField;

            private string stateMedicaidEnrollmentField;

            private medicareHICNStateMedicaidEnrollments stateMedicaidEnrollmentsField;

            private string workingSpouseField;

            private medicareHICNWorkingSpouses workingSpousesField;

            private string longTermCareResidentField;

            private medicareHICNDialysisDateRanges dialysisDateRangesField;

            private medicareHICNTransplantDateRanges transplantDateRangesField;

            private string esrdIndicatorField;

            private medicareHICNEsrdDateRanges esrdDateRangesField;

            private string hospiceIndicatorField;

            private medicareHICNHospiceDateRanges hospiceDateRangesField;

            private string institutionalIndicatorField;

            private medicareHICNInstitutionalDateRanges institutionalDateRangesField;

            private medicareHICNMedicaidDateRanges medicaidDateRangesField;

            private medicareHICNNhcDateRanges nhcDateRangesField;

            private medicareHICNPartADates partADatesField;

            private medicareHICNPartBDates partBDatesField;

            private medicareHICNPremiumWithholdDateRanges premiumWithholdDateRangesField;

            private medicareHICNSupplementalDateRanges supplementalDateRangesField;

            private medicareHICNPartDDateRanges partDDateRangesField;

            private medicareHICNMedicareEligibilityReasonCode medicareEligibilityReasonCodeField;

            private medicareHICNMedicareHICNFields medicareHICNFieldsField;

            private medicareHICNMedicaidDualStatusRanges medicaidDualStatusRangesField;

            private string resourceURIField;

            private string versionField;

            /// <remarks/>
            public string hicn
            {
                get
                {
                    return this.hicnField;
                }
                set
                {
                    this.hicnField = value;
                }
            }

            /// <remarks/>
            public string mbi
            {
                get
                {
                    return this.mbiField;
                }
                set
                {
                    this.mbiField = value;
                }
            }

            /// <remarks/>
            public string effectiveDate
            {
                get
                {
                    return this.effectiveDateField;
                }
                set
                {
                    this.effectiveDateField = value;
                }
            }

            /// <remarks/>
            public string expirationDate
            {
                get
                {
                    return this.expirationDateField;
                }
                set
                {
                    this.expirationDateField = value;
                }
            }

            /// <remarks/>
            public string entryDate
            {
                get
                {
                    return this.entryDateField;
                }
                set
                {
                    this.entryDateField = value;
                }
            }

            /// <remarks/>
            public string waitingPeriodStartDate
            {
                get
                {
                    return this.waitingPeriodStartDateField;
                }
                set
                {
                    this.waitingPeriodStartDateField = value;
                }
            }

            /// <remarks/>
            public string waitingPeriodEndDate
            {
                get
                {
                    return this.waitingPeriodEndDateField;
                }
                set
                {
                    this.waitingPeriodEndDateField = value;
                }
            }

            /// <remarks/>
            public string nursingHomeCompareIndicator
            {
                get
                {
                    return this.nursingHomeCompareIndicatorField;
                }
                set
                {
                    this.nursingHomeCompareIndicatorField = value;
                }
            }

            /// <remarks/>
            public string stateMedicaidEnrollment
            {
                get
                {
                    return this.stateMedicaidEnrollmentField;
                }
                set
                {
                    this.stateMedicaidEnrollmentField = value;
                }
            }

            /// <remarks/>
            public medicareHICNStateMedicaidEnrollments stateMedicaidEnrollments
            {
                get
                {
                    return this.stateMedicaidEnrollmentsField;
                }
                set
                {
                    this.stateMedicaidEnrollmentsField = value;
                }
            }

            /// <remarks/>
            public string workingSpouse
            {
                get
                {
                    return this.workingSpouseField;
                }
                set
                {
                    this.workingSpouseField = value;
                }
            }

            /// <remarks/>
            public medicareHICNWorkingSpouses workingSpouses
            {
                get
                {
                    return this.workingSpousesField;
                }
                set
                {
                    this.workingSpousesField = value;
                }
            }

            /// <remarks/>
            public string longTermCareResident
            {
                get
                {
                    return this.longTermCareResidentField;
                }
                set
                {
                    this.longTermCareResidentField = value;
                }
            }

            /// <remarks/>
            public medicareHICNDialysisDateRanges dialysisDateRanges
            {
                get
                {
                    return this.dialysisDateRangesField;
                }
                set
                {
                    this.dialysisDateRangesField = value;
                }
            }

            /// <remarks/>
            public medicareHICNTransplantDateRanges transplantDateRanges
            {
                get
                {
                    return this.transplantDateRangesField;
                }
                set
                {
                    this.transplantDateRangesField = value;
                }
            }

            /// <remarks/>
            public string esrdIndicator
            {
                get
                {
                    return this.esrdIndicatorField;
                }
                set
                {
                    this.esrdIndicatorField = value;
                }
            }

            /// <remarks/>
            public medicareHICNEsrdDateRanges esrdDateRanges
            {
                get
                {
                    return this.esrdDateRangesField;
                }
                set
                {
                    this.esrdDateRangesField = value;
                }
            }

            /// <remarks/>
            public string hospiceIndicator
            {
                get
                {
                    return this.hospiceIndicatorField;
                }
                set
                {
                    this.hospiceIndicatorField = value;
                }
            }

            /// <remarks/>
            public medicareHICNHospiceDateRanges hospiceDateRanges
            {
                get
                {
                    return this.hospiceDateRangesField;
                }
                set
                {
                    this.hospiceDateRangesField = value;
                }
            }

            /// <remarks/>
            public string institutionalIndicator
            {
                get
                {
                    return this.institutionalIndicatorField;
                }
                set
                {
                    this.institutionalIndicatorField = value;
                }
            }

            /// <remarks/>
            public medicareHICNInstitutionalDateRanges institutionalDateRanges
            {
                get
                {
                    return this.institutionalDateRangesField;
                }
                set
                {
                    this.institutionalDateRangesField = value;
                }
            }

            /// <remarks/>
            public medicareHICNMedicaidDateRanges medicaidDateRanges
            {
                get
                {
                    return this.medicaidDateRangesField;
                }
                set
                {
                    this.medicaidDateRangesField = value;
                }
            }

            /// <remarks/>
            public medicareHICNNhcDateRanges nhcDateRanges
            {
                get
                {
                    return this.nhcDateRangesField;
                }
                set
                {
                    this.nhcDateRangesField = value;
                }
            }

            /// <remarks/>
            public medicareHICNPartADates partADates
            {
                get
                {
                    return this.partADatesField;
                }
                set
                {
                    this.partADatesField = value;
                }
            }

            /// <remarks/>
            public medicareHICNPartBDates partBDates
            {
                get
                {
                    return this.partBDatesField;
                }
                set
                {
                    this.partBDatesField = value;
                }
            }

            /// <remarks/>
            public medicareHICNPremiumWithholdDateRanges premiumWithholdDateRanges
            {
                get
                {
                    return this.premiumWithholdDateRangesField;
                }
                set
                {
                    this.premiumWithholdDateRangesField = value;
                }
            }

            /// <remarks/>
            public medicareHICNSupplementalDateRanges supplementalDateRanges
            {
                get
                {
                    return this.supplementalDateRangesField;
                }
                set
                {
                    this.supplementalDateRangesField = value;
                }
            }

            /// <remarks/>
            public medicareHICNPartDDateRanges partDDateRanges
            {
                get
                {
                    return this.partDDateRangesField;
                }
                set
                {
                    this.partDDateRangesField = value;
                }
            }

            /// <remarks/>
            public medicareHICNMedicareEligibilityReasonCode medicareEligibilityReasonCode
            {
                get
                {
                    return this.medicareEligibilityReasonCodeField;
                }
                set
                {
                    this.medicareEligibilityReasonCodeField = value;
                }
            }

            /// <remarks/>
            public medicareHICNMedicareHICNFields medicareHICNFields
            {
                get
                {
                    return this.medicareHICNFieldsField;
                }
                set
                {
                    this.medicareHICNFieldsField = value;
                }
            }

            /// <remarks/>
            public medicareHICNMedicaidDualStatusRanges medicaidDualStatusRanges
            {
                get
                {
                    return this.medicaidDualStatusRangesField;
                }
                set
                {
                    this.medicaidDualStatusRangesField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string resourceURI
            {
                get
                {
                    return this.resourceURIField;
                }
                set
                {
                    this.resourceURIField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string version
            {
                get
                {
                    return this.versionField;
                }
                set
                {
                    this.versionField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNStateMedicaidEnrollments
        {

            private string updateModeField;

            private medicareHICNStateMedicaidEnrollmentsStateMedicaidEnrollment[] stateMedicaidEnrollmentField;

            /// <remarks/>
            public string updateMode
            {
                get
                {
                    return this.updateModeField;
                }
                set
                {
                    this.updateModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("stateMedicaidEnrollment")]
            public medicareHICNStateMedicaidEnrollmentsStateMedicaidEnrollment[] stateMedicaidEnrollment
            {
                get
                {
                    return this.stateMedicaidEnrollmentField;
                }
                set
                {
                    this.stateMedicaidEnrollmentField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNStateMedicaidEnrollmentsStateMedicaidEnrollment
        {

            private string startDateField;

            private string endDateField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNWorkingSpouses
        {

            private string updateModeField;

            private medicareHICNWorkingSpousesWorkingSpouse[] workingSpouseField;

            /// <remarks/>
            public string updateMode
            {
                get
                {
                    return this.updateModeField;
                }
                set
                {
                    this.updateModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("workingSpouse")]
            public medicareHICNWorkingSpousesWorkingSpouse[] workingSpouse
            {
                get
                {
                    return this.workingSpouseField;
                }
                set
                {
                    this.workingSpouseField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNWorkingSpousesWorkingSpouse
        {

            private string startDateField;

            private string endDateField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNDialysisDateRanges
        {

            private string updateModeField;

            private medicareHICNDialysisDateRangesDialysisDateRange[] dialysisDateRangeField;

            /// <remarks/>
            public string updateMode
            {
                get
                {
                    return this.updateModeField;
                }
                set
                {
                    this.updateModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("dialysisDateRange")]
            public medicareHICNDialysisDateRangesDialysisDateRange[] dialysisDateRange
            {
                get
                {
                    return this.dialysisDateRangeField;
                }
                set
                {
                    this.dialysisDateRangeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNDialysisDateRangesDialysisDateRange
        {

            private string startDateField;

            private string endDateField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNTransplantDateRanges
        {

            private string updateModeField;

            private medicareHICNTransplantDateRangesTransplantDateRange[] transplantDateRangeField;

            /// <remarks/>
            public string updateMode
            {
                get
                {
                    return this.updateModeField;
                }
                set
                {
                    this.updateModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("transplantDateRange")]
            public medicareHICNTransplantDateRangesTransplantDateRange[] transplantDateRange
            {
                get
                {
                    return this.transplantDateRangeField;
                }
                set
                {
                    this.transplantDateRangeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNTransplantDateRangesTransplantDateRange
        {

            private string startDateField;

            private string endDateField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNEsrdDateRanges
        {

            private string updateModeField;

            private medicareHICNEsrdDateRangesEsrdDateRange[] esrdDateRangeField;

            /// <remarks/>
            public string updateMode
            {
                get
                {
                    return this.updateModeField;
                }
                set
                {
                    this.updateModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("esrdDateRange")]
            public medicareHICNEsrdDateRangesEsrdDateRange[] esrdDateRange
            {
                get
                {
                    return this.esrdDateRangeField;
                }
                set
                {
                    this.esrdDateRangeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNEsrdDateRangesEsrdDateRange
        {

            private string startDateField;

            private string endDateField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNHospiceDateRanges
        {

            private string updateModeField;

            private medicareHICNHospiceDateRangesHospiceDateRange[] hospiceDateRangeField;

            /// <remarks/>
            public string updateMode
            {
                get
                {
                    return this.updateModeField;
                }
                set
                {
                    this.updateModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("hospiceDateRange")]
            public medicareHICNHospiceDateRangesHospiceDateRange[] hospiceDateRange
            {
                get
                {
                    return this.hospiceDateRangeField;
                }
                set
                {
                    this.hospiceDateRangeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNHospiceDateRangesHospiceDateRange
        {

            private string startDateField;

            private string endDateField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNInstitutionalDateRanges
        {

            private string updateModeField;

            private medicareHICNInstitutionalDateRangesInstitutionalDateRange[] institutionalDateRangeField;

            /// <remarks/>
            public string updateMode
            {
                get
                {
                    return this.updateModeField;
                }
                set
                {
                    this.updateModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("institutionalDateRange")]
            public medicareHICNInstitutionalDateRangesInstitutionalDateRange[] institutionalDateRange
            {
                get
                {
                    return this.institutionalDateRangeField;
                }
                set
                {
                    this.institutionalDateRangeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNInstitutionalDateRangesInstitutionalDateRange
        {

            private string startDateField;

            private string endDateField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNMedicaidDateRanges
        {

            private string updateModeField;

            private medicareHICNMedicaidDateRangesMedicaidDateRange[] medicaidDateRangeField;

            /// <remarks/>
            public string updateMode
            {
                get
                {
                    return this.updateModeField;
                }
                set
                {
                    this.updateModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("medicaidDateRange")]
            public medicareHICNMedicaidDateRangesMedicaidDateRange[] medicaidDateRange
            {
                get
                {
                    return this.medicaidDateRangeField;
                }
                set
                {
                    this.medicaidDateRangeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNMedicaidDateRangesMedicaidDateRange
        {

            private string startDateField;

            private string endDateField;

            private string medicaidRecertificationDateField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }

            /// <remarks/>
            public string medicaidRecertificationDate
            {
                get
                {
                    return this.medicaidRecertificationDateField;
                }
                set
                {
                    this.medicaidRecertificationDateField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNNhcDateRanges
        {

            private string updateModeField;

            private medicareHICNNhcDateRangesNhcDateRange[] nhcDateRangeField;

            /// <remarks/>
            public string updateMode
            {
                get
                {
                    return this.updateModeField;
                }
                set
                {
                    this.updateModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("nhcDateRange")]
            public medicareHICNNhcDateRangesNhcDateRange[] nhcDateRange
            {
                get
                {
                    return this.nhcDateRangeField;
                }
                set
                {
                    this.nhcDateRangeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNNhcDateRangesNhcDateRange
        {

            private string startDateField;

            private string endDateField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNPartADates
        {

            private string updateModeField;

            private medicareHICNPartADatesPartADate[] partADateField;

            /// <remarks/>
            public string updateMode
            {
                get
                {
                    return this.updateModeField;
                }
                set
                {
                    this.updateModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("partADate")]
            public medicareHICNPartADatesPartADate[] partADate
            {
                get
                {
                    return this.partADateField;
                }
                set
                {
                    this.partADateField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNPartADatesPartADate
        {

            private string effectiveDateField;

            private string terminationDateField;

            /// <remarks/>
            public string effectiveDate
            {
                get
                {
                    return this.effectiveDateField;
                }
                set
                {
                    this.effectiveDateField = value;
                }
            }

            /// <remarks/>
            public string terminationDate
            {
                get
                {
                    return this.terminationDateField;
                }
                set
                {
                    this.terminationDateField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNPartBDates
        {

            private string updateModeField;

            private medicareHICNPartBDatesPartBDate[] partBDateField;

            /// <remarks/>
            public string updateMode
            {
                get
                {
                    return this.updateModeField;
                }
                set
                {
                    this.updateModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("partBDate")]
            public medicareHICNPartBDatesPartBDate[] partBDate
            {
                get
                {
                    return this.partBDateField;
                }
                set
                {
                    this.partBDateField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNPartBDatesPartBDate
        {

            private string effectiveDateField;

            private string terminationDateField;

            /// <remarks/>
            public string effectiveDate
            {
                get
                {
                    return this.effectiveDateField;
                }
                set
                {
                    this.effectiveDateField = value;
                }
            }

            /// <remarks/>
            public string terminationDate
            {
                get
                {
                    return this.terminationDateField;
                }
                set
                {
                    this.terminationDateField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNPremiumWithholdDateRanges
        {

            private string updateModeField;

            private medicareHICNPremiumWithholdDateRangesPremiumWithholdDateRange[] premiumWithholdDateRangeField;

            /// <remarks/>
            public string updateMode
            {
                get
                {
                    return this.updateModeField;
                }
                set
                {
                    this.updateModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("premiumWithholdDateRange")]
            public medicareHICNPremiumWithholdDateRangesPremiumWithholdDateRange[] premiumWithholdDateRange
            {
                get
                {
                    return this.premiumWithholdDateRangeField;
                }
                set
                {
                    this.premiumWithholdDateRangeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNPremiumWithholdDateRangesPremiumWithholdDateRange
        {

            private string startDateField;

            private string endDateField;

            private string premiumWithholdFlagField;

            private string premiumWithholdPaymentMethodTypeField;

            private string premiumWithholdOptionField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }

            /// <remarks/>
            public string premiumWithholdFlag
            {
                get
                {
                    return this.premiumWithholdFlagField;
                }
                set
                {
                    this.premiumWithholdFlagField = value;
                }
            }

            /// <remarks/>
            public string premiumWithholdPaymentMethodType
            {
                get
                {
                    return this.premiumWithholdPaymentMethodTypeField;
                }
                set
                {
                    this.premiumWithholdPaymentMethodTypeField = value;
                }
            }

            /// <remarks/>
            public string premiumWithholdOption
            {
                get
                {
                    return this.premiumWithholdOptionField;
                }
                set
                {
                    this.premiumWithholdOptionField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNSupplementalDateRanges
        {

            private string updateModeField;

            private medicareHICNSupplementalDateRangesSupplementalDateRange[] supplementalDateRangeField;

            /// <remarks/>
            public string updateMode
            {
                get
                {
                    return this.updateModeField;
                }
                set
                {
                    this.updateModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("supplementalDateRange")]
            public medicareHICNSupplementalDateRangesSupplementalDateRange[] supplementalDateRange
            {
                get
                {
                    return this.supplementalDateRangeField;
                }
                set
                {
                    this.supplementalDateRangeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNSupplementalDateRangesSupplementalDateRange
        {

            private string startDateField;

            private string endDateField;

            private string personCodeField;

            private string supplementalTypeCodeField;

            private string supplementalRxCoverageField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }

            /// <remarks/>
            public string personCode
            {
                get
                {
                    return this.personCodeField;
                }
                set
                {
                    this.personCodeField = value;
                }
            }

            /// <remarks/>
            public string supplementalTypeCode
            {
                get
                {
                    return this.supplementalTypeCodeField;
                }
                set
                {
                    this.supplementalTypeCodeField = value;
                }
            }

            /// <remarks/>
            public string supplementalRxCoverage
            {
                get
                {
                    return this.supplementalRxCoverageField;
                }
                set
                {
                    this.supplementalRxCoverageField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNPartDDateRanges
        {

            private string updateModeField;

            private medicareHICNPartDDateRangesPartDDateRanges[] partDDateRangesField;

            /// <remarks/>
            public string updateMode
            {
                get
                {
                    return this.updateModeField;
                }
                set
                {
                    this.updateModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("partDDateRanges")]
            public medicareHICNPartDDateRangesPartDDateRanges[] partDDateRanges
            {
                get
                {
                    return this.partDDateRangesField;
                }
                set
                {
                    this.partDDateRangesField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNPartDDateRangesPartDDateRanges
        {

            private string startDateField;

            private string endDateField;

            private string partDBestAvailableEvidenceIndicatorField;

            private string partDCreditableCoverageIndicatorField;

            private medicareHICNPartDDateRangesPartDDateRangesPartDLEPDateRanges[] partDLEPDateRangesField;

            private medicareHICNPartDDateRangesPartDDateRangesPartDLISCopayCategoryDates[] partDLISCopayCategoryDatesField;

            private medicareHICNPartDDateRangesPartDDateRangesPartDLISSubsidyLevelDates[] partDLISSubsidyLevelDatesField;

            private medicareHICNPartDDateRangesPartDDateRangesPartDCurrLISPremSubsidyDates[] partDCurrLISPremSubsidyDatesField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }

            /// <remarks/>
            public string partDBestAvailableEvidenceIndicator
            {
                get
                {
                    return this.partDBestAvailableEvidenceIndicatorField;
                }
                set
                {
                    this.partDBestAvailableEvidenceIndicatorField = value;
                }
            }

            /// <remarks/>
            public string partDCreditableCoverageIndicator
            {
                get
                {
                    return this.partDCreditableCoverageIndicatorField;
                }
                set
                {
                    this.partDCreditableCoverageIndicatorField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("partDLEPDateRanges")]
            public medicareHICNPartDDateRangesPartDDateRangesPartDLEPDateRanges[] partDLEPDateRanges
            {
                get
                {
                    return this.partDLEPDateRangesField;
                }
                set
                {
                    this.partDLEPDateRangesField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("partDLISCopayCategoryDates")]
            public medicareHICNPartDDateRangesPartDDateRangesPartDLISCopayCategoryDates[] partDLISCopayCategoryDates
            {
                get
                {
                    return this.partDLISCopayCategoryDatesField;
                }
                set
                {
                    this.partDLISCopayCategoryDatesField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("partDLISSubsidyLevelDates")]
            public medicareHICNPartDDateRangesPartDDateRangesPartDLISSubsidyLevelDates[] partDLISSubsidyLevelDates
            {
                get
                {
                    return this.partDLISSubsidyLevelDatesField;
                }
                set
                {
                    this.partDLISSubsidyLevelDatesField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("partDCurrLISPremSubsidyDates")]
            public medicareHICNPartDDateRangesPartDDateRangesPartDCurrLISPremSubsidyDates[] partDCurrLISPremSubsidyDates
            {
                get
                {
                    return this.partDCurrLISPremSubsidyDatesField;
                }
                set
                {
                    this.partDCurrLISPremSubsidyDatesField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNPartDDateRangesPartDDateRangesPartDLEPDateRanges
        {

            private string startDateField;

            private string endDateField;

            private string partDLEPAmountField;

            private string partDLEPWaivedAmountField;

            private string partDLEPSubsidyAmountField;

            private string partDLEPCurrentNumberofUncoveredMonthsField;

            private string partDLEPTotalNumberofUncoveredMonthsField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }

            /// <remarks/>
            public string partDLEPAmount
            {
                get
                {
                    return this.partDLEPAmountField;
                }
                set
                {
                    this.partDLEPAmountField = value;
                }
            }

            /// <remarks/>
            public string partDLEPWaivedAmount
            {
                get
                {
                    return this.partDLEPWaivedAmountField;
                }
                set
                {
                    this.partDLEPWaivedAmountField = value;
                }
            }

            /// <remarks/>
            public string partDLEPSubsidyAmount
            {
                get
                {
                    return this.partDLEPSubsidyAmountField;
                }
                set
                {
                    this.partDLEPSubsidyAmountField = value;
                }
            }

            /// <remarks/>
            public string partDLEPCurrentNumberofUncoveredMonths
            {
                get
                {
                    return this.partDLEPCurrentNumberofUncoveredMonthsField;
                }
                set
                {
                    this.partDLEPCurrentNumberofUncoveredMonthsField = value;
                }
            }

            /// <remarks/>
            public string partDLEPTotalNumberofUncoveredMonths
            {
                get
                {
                    return this.partDLEPTotalNumberofUncoveredMonthsField;
                }
                set
                {
                    this.partDLEPTotalNumberofUncoveredMonthsField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNPartDDateRangesPartDDateRangesPartDLISCopayCategoryDates
        {

            private string startDateField;

            private string endDateField;

            private medicareHICNPartDDateRangesPartDDateRangesPartDLISCopayCategoryDatesPartDLISCopayCategoryCode partDLISCopayCategoryCodeField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }

            /// <remarks/>
            public medicareHICNPartDDateRangesPartDDateRangesPartDLISCopayCategoryDatesPartDLISCopayCategoryCode partDLISCopayCategoryCode
            {
                get
                {
                    return this.partDLISCopayCategoryCodeField;
                }
                set
                {
                    this.partDLISCopayCategoryCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNPartDDateRangesPartDDateRangesPartDLISCopayCategoryDatesPartDLISCopayCategoryCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNPartDDateRangesPartDDateRangesPartDLISSubsidyLevelDates
        {

            private string startDateField;

            private string endDateField;

            private medicareHICNPartDDateRangesPartDDateRangesPartDLISSubsidyLevelDatesPartDLISSubsidyLevel partDLISSubsidyLevelField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }

            /// <remarks/>
            public medicareHICNPartDDateRangesPartDDateRangesPartDLISSubsidyLevelDatesPartDLISSubsidyLevel partDLISSubsidyLevel
            {
                get
                {
                    return this.partDLISSubsidyLevelField;
                }
                set
                {
                    this.partDLISSubsidyLevelField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNPartDDateRangesPartDDateRangesPartDLISSubsidyLevelDatesPartDLISSubsidyLevel
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNPartDDateRangesPartDDateRangesPartDCurrLISPremSubsidyDates
        {

            private string startDateField;

            private string endDateField;

            private string partDCurrentLISPremiumSubsidyAmountField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }

            /// <remarks/>
            public string partDCurrentLISPremiumSubsidyAmount
            {
                get
                {
                    return this.partDCurrentLISPremiumSubsidyAmountField;
                }
                set
                {
                    this.partDCurrentLISPremiumSubsidyAmountField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNMedicareEligibilityReasonCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNMedicareHICNFields
        {

            private string updateModeField;

            private medicareHICNMedicareHICNFieldsMedicareHICNField[] medicareHICNFieldField;

            /// <remarks/>
            public string updateMode
            {
                get
                {
                    return this.updateModeField;
                }
                set
                {
                    this.updateModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("medicareHICNField")]
            public medicareHICNMedicareHICNFieldsMedicareHICNField[] medicareHICNField
            {
                get
                {
                    return this.medicareHICNFieldField;
                }
                set
                {
                    this.medicareHICNFieldField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNMedicareHICNFieldsMedicareHICNField
        {

            private string fieldNameField;

            private string fieldValueField;

            /// <remarks/>
            public string fieldName
            {
                get
                {
                    return this.fieldNameField;
                }
                set
                {
                    this.fieldNameField = value;
                }
            }

            /// <remarks/>
            public string fieldValue
            {
                get
                {
                    return this.fieldValueField;
                }
                set
                {
                    this.fieldValueField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNMedicaidDualStatusRanges
        {

            private string updateModeField;

            private medicareHICNMedicaidDualStatusRangesMedicaidDualStatus[] medicaidDualStatusField;

            /// <remarks/>
            public string updateMode
            {
                get
                {
                    return this.updateModeField;
                }
                set
                {
                    this.updateModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("medicaidDualStatus")]
            public medicareHICNMedicaidDualStatusRangesMedicaidDualStatus[] medicaidDualStatus
            {
                get
                {
                    return this.medicaidDualStatusField;
                }
                set
                {
                    this.medicaidDualStatusField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNMedicaidDualStatusRangesMedicaidDualStatus
        {

            private string effectiveDateField;

            private string terminationDateField;

            private string gracePeriodStartDateField;

            private string medicaidDualField;

            private medicareHICNMedicaidDualStatusRangesMedicaidDualStatusMedicaidDualStatusSourceCode medicaidDualStatusSourceCodeField;

            private medicareHICNMedicaidDualStatusRangesMedicaidDualStatusMedicaidDualStatusCode medicaidDualStatusCodeField;

            /// <remarks/>
            public string effectiveDate
            {
                get
                {
                    return this.effectiveDateField;
                }
                set
                {
                    this.effectiveDateField = value;
                }
            }

            /// <remarks/>
            public string terminationDate
            {
                get
                {
                    return this.terminationDateField;
                }
                set
                {
                    this.terminationDateField = value;
                }
            }

            /// <remarks/>
            public string gracePeriodStartDate
            {
                get
                {
                    return this.gracePeriodStartDateField;
                }
                set
                {
                    this.gracePeriodStartDateField = value;
                }
            }

            /// <remarks/>
            public string medicaidDual
            {
                get
                {
                    return this.medicaidDualField;
                }
                set
                {
                    this.medicaidDualField = value;
                }
            }

            /// <remarks/>
            public medicareHICNMedicaidDualStatusRangesMedicaidDualStatusMedicaidDualStatusSourceCode medicaidDualStatusSourceCode
            {
                get
                {
                    return this.medicaidDualStatusSourceCodeField;
                }
                set
                {
                    this.medicaidDualStatusSourceCodeField = value;
                }
            }

            /// <remarks/>
            public medicareHICNMedicaidDualStatusRangesMedicaidDualStatusMedicaidDualStatusCode medicaidDualStatusCode
            {
                get
                {
                    return this.medicaidDualStatusCodeField;
                }
                set
                {
                    this.medicaidDualStatusCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNMedicaidDualStatusRangesMedicaidDualStatusMedicaidDualStatusSourceCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class medicareHICNMedicaidDualStatusRangesMedicaidDualStatusMedicaidDualStatusCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class cobPolicy
        {

            private string cobPolicyIdentifierField;

            private string cobPolicyIdField;

            private cobPolicyLeadSourceForOtherInsuranceCode leadSourceForOtherInsuranceCodeField;

            private cobPolicyRelationshipToSubscriberDefinitionReference relationshipToSubscriberDefinitionReferenceField;

            private string otherInsuranceCompanyNameField;

            private string policyTypeField;

            private string policyHolderNameField;

            private string benefitPlanNameField;

            private string groupNumberField;

            private string memberIdField;

            private cobPolicyBenefitPlanTypeCode benefitPlanTypeCodeField;

            private cobPolicyDateRanges[] dateRangesField;

            private cobPolicySubscriberIdList[] subscriberIdListField;

            private cobPolicySubscriberName subscriberNameField;

            private string subscriberDOBField;

            private string subscriberGenderField;

            private cobPolicyRxInfo rxInfoField;

            private cobPolicyCorrespondenceInfo correspondenceInfoField;

            private cobPolicyCarrierIds[] carrierIdsField;

            private string resourceURIField;

            private string versionField;

            /// <remarks/>
            public string cobPolicyIdentifier
            {
                get
                {
                    return this.cobPolicyIdentifierField;
                }
                set
                {
                    this.cobPolicyIdentifierField = value;
                }
            }

            /// <remarks/>
            public string cobPolicyId
            {
                get
                {
                    return this.cobPolicyIdField;
                }
                set
                {
                    this.cobPolicyIdField = value;
                }
            }

            /// <remarks/>
            public cobPolicyLeadSourceForOtherInsuranceCode leadSourceForOtherInsuranceCode
            {
                get
                {
                    return this.leadSourceForOtherInsuranceCodeField;
                }
                set
                {
                    this.leadSourceForOtherInsuranceCodeField = value;
                }
            }

            /// <remarks/>
            public cobPolicyRelationshipToSubscriberDefinitionReference relationshipToSubscriberDefinitionReference
            {
                get
                {
                    return this.relationshipToSubscriberDefinitionReferenceField;
                }
                set
                {
                    this.relationshipToSubscriberDefinitionReferenceField = value;
                }
            }

            /// <remarks/>
            public string otherInsuranceCompanyName
            {
                get
                {
                    return this.otherInsuranceCompanyNameField;
                }
                set
                {
                    this.otherInsuranceCompanyNameField = value;
                }
            }

            /// <remarks/>
            public string policyType
            {
                get
                {
                    return this.policyTypeField;
                }
                set
                {
                    this.policyTypeField = value;
                }
            }

            /// <remarks/>
            public string policyHolderName
            {
                get
                {
                    return this.policyHolderNameField;
                }
                set
                {
                    this.policyHolderNameField = value;
                }
            }

            /// <remarks/>
            public string benefitPlanName
            {
                get
                {
                    return this.benefitPlanNameField;
                }
                set
                {
                    this.benefitPlanNameField = value;
                }
            }

            /// <remarks/>
            public string groupNumber
            {
                get
                {
                    return this.groupNumberField;
                }
                set
                {
                    this.groupNumberField = value;
                }
            }

            /// <remarks/>
            public string memberId
            {
                get
                {
                    return this.memberIdField;
                }
                set
                {
                    this.memberIdField = value;
                }
            }

            /// <remarks/>
            public cobPolicyBenefitPlanTypeCode benefitPlanTypeCode
            {
                get
                {
                    return this.benefitPlanTypeCodeField;
                }
                set
                {
                    this.benefitPlanTypeCodeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("dateRanges")]
            public cobPolicyDateRanges[] dateRanges
            {
                get
                {
                    return this.dateRangesField;
                }
                set
                {
                    this.dateRangesField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("subscriberIdList")]
            public cobPolicySubscriberIdList[] subscriberIdList
            {
                get
                {
                    return this.subscriberIdListField;
                }
                set
                {
                    this.subscriberIdListField = value;
                }
            }

            /// <remarks/>
            public cobPolicySubscriberName subscriberName
            {
                get
                {
                    return this.subscriberNameField;
                }
                set
                {
                    this.subscriberNameField = value;
                }
            }

            /// <remarks/>
            public string subscriberDOB
            {
                get
                {
                    return this.subscriberDOBField;
                }
                set
                {
                    this.subscriberDOBField = value;
                }
            }

            /// <remarks/>
            public string subscriberGender
            {
                get
                {
                    return this.subscriberGenderField;
                }
                set
                {
                    this.subscriberGenderField = value;
                }
            }

            /// <remarks/>
            public cobPolicyRxInfo rxInfo
            {
                get
                {
                    return this.rxInfoField;
                }
                set
                {
                    this.rxInfoField = value;
                }
            }

            /// <remarks/>
            public cobPolicyCorrespondenceInfo correspondenceInfo
            {
                get
                {
                    return this.correspondenceInfoField;
                }
                set
                {
                    this.correspondenceInfoField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("carrierIds")]
            public cobPolicyCarrierIds[] carrierIds
            {
                get
                {
                    return this.carrierIdsField;
                }
                set
                {
                    this.carrierIdsField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string resourceURI
            {
                get
                {
                    return this.resourceURIField;
                }
                set
                {
                    this.resourceURIField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string version
            {
                get
                {
                    return this.versionField;
                }
                set
                {
                    this.versionField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class cobPolicyLeadSourceForOtherInsuranceCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class cobPolicyRelationshipToSubscriberDefinitionReference
        {

            private string idField;

            /// <remarks/>
            public string ID
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class cobPolicyBenefitPlanTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class cobPolicyDateRanges
        {

            private string startDateField;

            private string endDateField;

            private string responsibilitySequenceCodeField;

            private cobPolicyDateRangesOBDReasonCode oBDReasonCodeField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }

            /// <remarks/>
            public string responsibilitySequenceCode
            {
                get
                {
                    return this.responsibilitySequenceCodeField;
                }
                set
                {
                    this.responsibilitySequenceCodeField = value;
                }
            }

            /// <remarks/>
            public cobPolicyDateRangesOBDReasonCode OBDReasonCode
            {
                get
                {
                    return this.oBDReasonCodeField;
                }
                set
                {
                    this.oBDReasonCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class cobPolicyDateRangesOBDReasonCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class cobPolicySubscriberIdList
        {

            private string identificationNumberField;

            private string issuingStateField;

            private cobPolicySubscriberIdListIssuingCountry issuingCountryField;

            private string effectiveStartDateField;

            private string effectiveEndDateField;

            private cobPolicySubscriberIdListIdentificationTypeCode identificationTypeCodeField;

            /// <remarks/>
            public string identificationNumber
            {
                get
                {
                    return this.identificationNumberField;
                }
                set
                {
                    this.identificationNumberField = value;
                }
            }

            /// <remarks/>
            public string issuingState
            {
                get
                {
                    return this.issuingStateField;
                }
                set
                {
                    this.issuingStateField = value;
                }
            }

            /// <remarks/>
            public cobPolicySubscriberIdListIssuingCountry issuingCountry
            {
                get
                {
                    return this.issuingCountryField;
                }
                set
                {
                    this.issuingCountryField = value;
                }
            }

            /// <remarks/>
            public string effectiveStartDate
            {
                get
                {
                    return this.effectiveStartDateField;
                }
                set
                {
                    this.effectiveStartDateField = value;
                }
            }

            /// <remarks/>
            public string effectiveEndDate
            {
                get
                {
                    return this.effectiveEndDateField;
                }
                set
                {
                    this.effectiveEndDateField = value;
                }
            }

            /// <remarks/>
            public cobPolicySubscriberIdListIdentificationTypeCode identificationTypeCode
            {
                get
                {
                    return this.identificationTypeCodeField;
                }
                set
                {
                    this.identificationTypeCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class cobPolicySubscriberIdListIssuingCountry
        {

            private string countryCodeField;

            /// <remarks/>
            public string countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class cobPolicySubscriberIdListIdentificationTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class cobPolicySubscriberName
        {

            private string lastNameField;

            private string firstNameField;

            private string middleNameField;

            private string namePrefixField;

            private string nameSuffixField;

            private cobPolicySubscriberNameNameSuffixList nameSuffixListField;

            private cobPolicySubscriberNameCredentialSuffixList credentialSuffixListField;

            /// <remarks/>
            public string lastName
            {
                get
                {
                    return this.lastNameField;
                }
                set
                {
                    this.lastNameField = value;
                }
            }

            /// <remarks/>
            public string firstName
            {
                get
                {
                    return this.firstNameField;
                }
                set
                {
                    this.firstNameField = value;
                }
            }

            /// <remarks/>
            public string middleName
            {
                get
                {
                    return this.middleNameField;
                }
                set
                {
                    this.middleNameField = value;
                }
            }

            /// <remarks/>
            public string namePrefix
            {
                get
                {
                    return this.namePrefixField;
                }
                set
                {
                    this.namePrefixField = value;
                }
            }

            /// <remarks/>
            public string nameSuffix
            {
                get
                {
                    return this.nameSuffixField;
                }
                set
                {
                    this.nameSuffixField = value;
                }
            }

            /// <remarks/>
            public cobPolicySubscriberNameNameSuffixList nameSuffixList
            {
                get
                {
                    return this.nameSuffixListField;
                }
                set
                {
                    this.nameSuffixListField = value;
                }
            }

            /// <remarks/>
            public cobPolicySubscriberNameCredentialSuffixList credentialSuffixList
            {
                get
                {
                    return this.credentialSuffixListField;
                }
                set
                {
                    this.credentialSuffixListField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class cobPolicySubscriberNameNameSuffixList
        {

            private string listModeField;

            private string[] suffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("suffix")]
            public string[] suffix
            {
                get
                {
                    return this.suffixField;
                }
                set
                {
                    this.suffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class cobPolicySubscriberNameCredentialSuffixList
        {

            private string listModeField;

            private cobPolicySubscriberNameCredentialSuffixListCredentialSuffix[] credentialSuffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("CredentialSuffix")]
            public cobPolicySubscriberNameCredentialSuffixListCredentialSuffix[] CredentialSuffix
            {
                get
                {
                    return this.credentialSuffixField;
                }
                set
                {
                    this.credentialSuffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class cobPolicySubscriberNameCredentialSuffixListCredentialSuffix
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class cobPolicyRxInfo
        {

            private string rxBinField;

            private string pcnField;

            private string rxGroupField;

            private string rxIdField;

            private string rxPhoneNumberField;

            /// <remarks/>
            public string rxBin
            {
                get
                {
                    return this.rxBinField;
                }
                set
                {
                    this.rxBinField = value;
                }
            }

            /// <remarks/>
            public string pcn
            {
                get
                {
                    return this.pcnField;
                }
                set
                {
                    this.pcnField = value;
                }
            }

            /// <remarks/>
            public string rxGroup
            {
                get
                {
                    return this.rxGroupField;
                }
                set
                {
                    this.rxGroupField = value;
                }
            }

            /// <remarks/>
            public string rxId
            {
                get
                {
                    return this.rxIdField;
                }
                set
                {
                    this.rxIdField = value;
                }
            }

            /// <remarks/>
            public string rxPhoneNumber
            {
                get
                {
                    return this.rxPhoneNumberField;
                }
                set
                {
                    this.rxPhoneNumberField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class cobPolicyCorrespondenceInfo
        {

            private string emailAddressField;

            private cobPolicyCorrespondenceInfoAddressInfo addressInfoField;

            private cobPolicyCorrespondenceInfoSupplierCorrespondenceUsageCode supplierCorrespondenceUsageCodeField;

            private cobPolicyCorrespondenceInfoSupplierLocationCorrespondenceUsageCode supplierLocationCorrespondenceUsageCodeField;

            private cobPolicyCorrespondenceInfoAccountCorrespondenceUsageCode accountCorrespondenceUsageCodeField;

            /// <remarks/>
            public string emailAddress
            {
                get
                {
                    return this.emailAddressField;
                }
                set
                {
                    this.emailAddressField = value;
                }
            }

            /// <remarks/>
            public cobPolicyCorrespondenceInfoAddressInfo addressInfo
            {
                get
                {
                    return this.addressInfoField;
                }
                set
                {
                    this.addressInfoField = value;
                }
            }

            /// <remarks/>
            public cobPolicyCorrespondenceInfoSupplierCorrespondenceUsageCode supplierCorrespondenceUsageCode
            {
                get
                {
                    return this.supplierCorrespondenceUsageCodeField;
                }
                set
                {
                    this.supplierCorrespondenceUsageCodeField = value;
                }
            }

            /// <remarks/>
            public cobPolicyCorrespondenceInfoSupplierLocationCorrespondenceUsageCode supplierLocationCorrespondenceUsageCode
            {
                get
                {
                    return this.supplierLocationCorrespondenceUsageCodeField;
                }
                set
                {
                    this.supplierLocationCorrespondenceUsageCodeField = value;
                }
            }

            /// <remarks/>
            public cobPolicyCorrespondenceInfoAccountCorrespondenceUsageCode accountCorrespondenceUsageCode
            {
                get
                {
                    return this.accountCorrespondenceUsageCodeField;
                }
                set
                {
                    this.accountCorrespondenceUsageCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class cobPolicyCorrespondenceInfoAddressInfo
        {

            private cobPolicyCorrespondenceInfoAddressInfoPostalAddress postalAddressField;

            private cobPolicyCorrespondenceInfoAddressInfoAddressPhoneList[] addressPhoneListField;

            /// <remarks/>
            public cobPolicyCorrespondenceInfoAddressInfoPostalAddress postalAddress
            {
                get
                {
                    return this.postalAddressField;
                }
                set
                {
                    this.postalAddressField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("addressPhoneList")]
            public cobPolicyCorrespondenceInfoAddressInfoAddressPhoneList[] addressPhoneList
            {
                get
                {
                    return this.addressPhoneListField;
                }
                set
                {
                    this.addressPhoneListField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class cobPolicyCorrespondenceInfoAddressInfoPostalAddress
        {

            private string addressField;

            private string address2Field;

            private string address3Field;

            private string stateCodeField;

            private string zipCodeField;

            private string zipExtensionCodeField;

            private string cityNameField;

            private string countyCodeField;

            private cobPolicyCorrespondenceInfoAddressInfoPostalAddressCountryCode countryCodeField;

            private string longitudeField;

            private string latitudeField;

            private string ignoreAddressCheckField;

            private string undeliverableAddressField;

            /// <remarks/>
            public string address
            {
                get
                {
                    return this.addressField;
                }
                set
                {
                    this.addressField = value;
                }
            }

            /// <remarks/>
            public string address2
            {
                get
                {
                    return this.address2Field;
                }
                set
                {
                    this.address2Field = value;
                }
            }

            /// <remarks/>
            public string address3
            {
                get
                {
                    return this.address3Field;
                }
                set
                {
                    this.address3Field = value;
                }
            }

            /// <remarks/>
            public string stateCode
            {
                get
                {
                    return this.stateCodeField;
                }
                set
                {
                    this.stateCodeField = value;
                }
            }

            /// <remarks/>
            public string zipCode
            {
                get
                {
                    return this.zipCodeField;
                }
                set
                {
                    this.zipCodeField = value;
                }
            }

            /// <remarks/>
            public string zipExtensionCode
            {
                get
                {
                    return this.zipExtensionCodeField;
                }
                set
                {
                    this.zipExtensionCodeField = value;
                }
            }

            /// <remarks/>
            public string cityName
            {
                get
                {
                    return this.cityNameField;
                }
                set
                {
                    this.cityNameField = value;
                }
            }

            /// <remarks/>
            public string countyCode
            {
                get
                {
                    return this.countyCodeField;
                }
                set
                {
                    this.countyCodeField = value;
                }
            }

            /// <remarks/>
            public cobPolicyCorrespondenceInfoAddressInfoPostalAddressCountryCode countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }

            /// <remarks/>
            public string longitude
            {
                get
                {
                    return this.longitudeField;
                }
                set
                {
                    this.longitudeField = value;
                }
            }

            /// <remarks/>
            public string latitude
            {
                get
                {
                    return this.latitudeField;
                }
                set
                {
                    this.latitudeField = value;
                }
            }

            /// <remarks/>
            public string ignoreAddressCheck
            {
                get
                {
                    return this.ignoreAddressCheckField;
                }
                set
                {
                    this.ignoreAddressCheckField = value;
                }
            }

            /// <remarks/>
            public string undeliverableAddress
            {
                get
                {
                    return this.undeliverableAddressField;
                }
                set
                {
                    this.undeliverableAddressField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class cobPolicyCorrespondenceInfoAddressInfoPostalAddressCountryCode
        {

            private string countryCodeField;

            /// <remarks/>
            public string countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class cobPolicyCorrespondenceInfoAddressInfoAddressPhoneList
        {

            private string phoneCountryCodeField;

            private string phoneAreaCodeField;

            private string phoneNumberField;

            private string phoneExtensionNumberField;

            private cobPolicyCorrespondenceInfoAddressInfoAddressPhoneListIndividualPhoneTypeCode individualPhoneTypeCodeField;

            private cobPolicyCorrespondenceInfoAddressInfoAddressPhoneListOrganizationPhoneTypeCode organizationPhoneTypeCodeField;

            /// <remarks/>
            public string phoneCountryCode
            {
                get
                {
                    return this.phoneCountryCodeField;
                }
                set
                {
                    this.phoneCountryCodeField = value;
                }
            }

            /// <remarks/>
            public string phoneAreaCode
            {
                get
                {
                    return this.phoneAreaCodeField;
                }
                set
                {
                    this.phoneAreaCodeField = value;
                }
            }

            /// <remarks/>
            public string phoneNumber
            {
                get
                {
                    return this.phoneNumberField;
                }
                set
                {
                    this.phoneNumberField = value;
                }
            }

            /// <remarks/>
            public string phoneExtensionNumber
            {
                get
                {
                    return this.phoneExtensionNumberField;
                }
                set
                {
                    this.phoneExtensionNumberField = value;
                }
            }

            /// <remarks/>
            public cobPolicyCorrespondenceInfoAddressInfoAddressPhoneListIndividualPhoneTypeCode individualPhoneTypeCode
            {
                get
                {
                    return this.individualPhoneTypeCodeField;
                }
                set
                {
                    this.individualPhoneTypeCodeField = value;
                }
            }

            /// <remarks/>
            public cobPolicyCorrespondenceInfoAddressInfoAddressPhoneListOrganizationPhoneTypeCode organizationPhoneTypeCode
            {
                get
                {
                    return this.organizationPhoneTypeCodeField;
                }
                set
                {
                    this.organizationPhoneTypeCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class cobPolicyCorrespondenceInfoAddressInfoAddressPhoneListIndividualPhoneTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class cobPolicyCorrespondenceInfoAddressInfoAddressPhoneListOrganizationPhoneTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class cobPolicyCorrespondenceInfoSupplierCorrespondenceUsageCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class cobPolicyCorrespondenceInfoSupplierLocationCorrespondenceUsageCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class cobPolicyCorrespondenceInfoAccountCorrespondenceUsageCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class cobPolicyCarrierIds
        {

            private string identificationNumberField;

            private string issuingStateField;

            private cobPolicyCarrierIdsIssuingCountry issuingCountryField;

            private string effectiveStartDateField;

            private string effectiveEndDateField;

            private cobPolicyCarrierIdsIdentificationTypeCode identificationTypeCodeField;

            /// <remarks/>
            public string identificationNumber
            {
                get
                {
                    return this.identificationNumberField;
                }
                set
                {
                    this.identificationNumberField = value;
                }
            }

            /// <remarks/>
            public string issuingState
            {
                get
                {
                    return this.issuingStateField;
                }
                set
                {
                    this.issuingStateField = value;
                }
            }

            /// <remarks/>
            public cobPolicyCarrierIdsIssuingCountry issuingCountry
            {
                get
                {
                    return this.issuingCountryField;
                }
                set
                {
                    this.issuingCountryField = value;
                }
            }

            /// <remarks/>
            public string effectiveStartDate
            {
                get
                {
                    return this.effectiveStartDateField;
                }
                set
                {
                    this.effectiveStartDateField = value;
                }
            }

            /// <remarks/>
            public string effectiveEndDate
            {
                get
                {
                    return this.effectiveEndDateField;
                }
                set
                {
                    this.effectiveEndDateField = value;
                }
            }

            /// <remarks/>
            public cobPolicyCarrierIdsIdentificationTypeCode identificationTypeCode
            {
                get
                {
                    return this.identificationTypeCodeField;
                }
                set
                {
                    this.identificationTypeCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class cobPolicyCarrierIdsIssuingCountry
        {

            private string countryCodeField;

            /// <remarks/>
            public string countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class cobPolicyCarrierIdsIdentificationTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class headOfHousehold
        {

            private headOfHouseholdRelatedMember relatedMemberField;

            private headOfHouseholdExternalMemberInformation externalMemberInformationField;

            /// <remarks/>
            public headOfHouseholdRelatedMember relatedMember
            {
                get
                {
                    return this.relatedMemberField;
                }
                set
                {
                    this.relatedMemberField = value;
                }
            }

            /// <remarks/>
            public headOfHouseholdExternalMemberInformation externalMemberInformation
            {
                get
                {
                    return this.externalMemberInformationField;
                }
                set
                {
                    this.externalMemberInformationField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class headOfHouseholdRelatedMember
        {

            private string definitionNameField;

            private string idField;

            private string topAccountField;

            private string customString1Field;

            private string customString2Field;

            private string customString3Field;

            private string customString4Field;

            private string customInteger1Field;

            private string customInteger2Field;

            private string customInteger3Field;

            private string customInteger4Field;

            private string customDate1Field;

            private string customDate2Field;

            private string customDate3Field;

            private string customDate4Field;

            private string customDecimal1Field;

            private string customDecimal2Field;

            private string customDecimal3Field;

            private string customDecimal4Field;

            private headOfHouseholdRelatedMemberMember memberField;

            private headOfHouseholdRelatedMemberSubscriber subscriberField;

            private string asOfDateField;

            /// <remarks/>
            public string definitionName
            {
                get
                {
                    return this.definitionNameField;
                }
                set
                {
                    this.definitionNameField = value;
                }
            }

            /// <remarks/>
            public string id
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }

            /// <remarks/>
            public string topAccount
            {
                get
                {
                    return this.topAccountField;
                }
                set
                {
                    this.topAccountField = value;
                }
            }

            /// <remarks/>
            public string customString1
            {
                get
                {
                    return this.customString1Field;
                }
                set
                {
                    this.customString1Field = value;
                }
            }

            /// <remarks/>
            public string customString2
            {
                get
                {
                    return this.customString2Field;
                }
                set
                {
                    this.customString2Field = value;
                }
            }

            /// <remarks/>
            public string customString3
            {
                get
                {
                    return this.customString3Field;
                }
                set
                {
                    this.customString3Field = value;
                }
            }

            /// <remarks/>
            public string customString4
            {
                get
                {
                    return this.customString4Field;
                }
                set
                {
                    this.customString4Field = value;
                }
            }

            /// <remarks/>
            public string customInteger1
            {
                get
                {
                    return this.customInteger1Field;
                }
                set
                {
                    this.customInteger1Field = value;
                }
            }

            /// <remarks/>
            public string customInteger2
            {
                get
                {
                    return this.customInteger2Field;
                }
                set
                {
                    this.customInteger2Field = value;
                }
            }

            /// <remarks/>
            public string customInteger3
            {
                get
                {
                    return this.customInteger3Field;
                }
                set
                {
                    this.customInteger3Field = value;
                }
            }

            /// <remarks/>
            public string customInteger4
            {
                get
                {
                    return this.customInteger4Field;
                }
                set
                {
                    this.customInteger4Field = value;
                }
            }

            /// <remarks/>
            public string customDate1
            {
                get
                {
                    return this.customDate1Field;
                }
                set
                {
                    this.customDate1Field = value;
                }
            }

            /// <remarks/>
            public string customDate2
            {
                get
                {
                    return this.customDate2Field;
                }
                set
                {
                    this.customDate2Field = value;
                }
            }

            /// <remarks/>
            public string customDate3
            {
                get
                {
                    return this.customDate3Field;
                }
                set
                {
                    this.customDate3Field = value;
                }
            }

            /// <remarks/>
            public string customDate4
            {
                get
                {
                    return this.customDate4Field;
                }
                set
                {
                    this.customDate4Field = value;
                }
            }

            /// <remarks/>
            public string customDecimal1
            {
                get
                {
                    return this.customDecimal1Field;
                }
                set
                {
                    this.customDecimal1Field = value;
                }
            }

            /// <remarks/>
            public string customDecimal2
            {
                get
                {
                    return this.customDecimal2Field;
                }
                set
                {
                    this.customDecimal2Field = value;
                }
            }

            /// <remarks/>
            public string customDecimal3
            {
                get
                {
                    return this.customDecimal3Field;
                }
                set
                {
                    this.customDecimal3Field = value;
                }
            }

            /// <remarks/>
            public string customDecimal4
            {
                get
                {
                    return this.customDecimal4Field;
                }
                set
                {
                    this.customDecimal4Field = value;
                }
            }

            /// <remarks/>
            public headOfHouseholdRelatedMemberMember member
            {
                get
                {
                    return this.memberField;
                }
                set
                {
                    this.memberField = value;
                }
            }

            /// <remarks/>
            public headOfHouseholdRelatedMemberSubscriber subscriber
            {
                get
                {
                    return this.subscriberField;
                }
                set
                {
                    this.subscriberField = value;
                }
            }

            /// <remarks/>
            public string asOfDate
            {
                get
                {
                    return this.asOfDateField;
                }
                set
                {
                    this.asOfDateField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class headOfHouseholdRelatedMemberMember
        {

            private string firstNameField;

            private string lastNameField;

            private string fullNameField;

            private string genderField;

            private string dateOfBirthField;

            private string taxIdField;

            private headOfHouseholdRelatedMemberMemberAddress addressField;

            /// <remarks/>
            public string firstName
            {
                get
                {
                    return this.firstNameField;
                }
                set
                {
                    this.firstNameField = value;
                }
            }

            /// <remarks/>
            public string lastName
            {
                get
                {
                    return this.lastNameField;
                }
                set
                {
                    this.lastNameField = value;
                }
            }

            /// <remarks/>
            public string fullName
            {
                get
                {
                    return this.fullNameField;
                }
                set
                {
                    this.fullNameField = value;
                }
            }

            /// <remarks/>
            public string gender
            {
                get
                {
                    return this.genderField;
                }
                set
                {
                    this.genderField = value;
                }
            }

            /// <remarks/>
            public string dateOfBirth
            {
                get
                {
                    return this.dateOfBirthField;
                }
                set
                {
                    this.dateOfBirthField = value;
                }
            }

            /// <remarks/>
            public string taxId
            {
                get
                {
                    return this.taxIdField;
                }
                set
                {
                    this.taxIdField = value;
                }
            }

            /// <remarks/>
            public headOfHouseholdRelatedMemberMemberAddress address
            {
                get
                {
                    return this.addressField;
                }
                set
                {
                    this.addressField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class headOfHouseholdRelatedMemberMemberAddress
        {

            private string addressField;

            private string cityNameField;

            private string stateCodeField;

            private string zipCodeField;

            private string zipExtensionCodeField;

            private string countryCodeField;

            /// <remarks/>
            public string address
            {
                get
                {
                    return this.addressField;
                }
                set
                {
                    this.addressField = value;
                }
            }

            /// <remarks/>
            public string cityName
            {
                get
                {
                    return this.cityNameField;
                }
                set
                {
                    this.cityNameField = value;
                }
            }

            /// <remarks/>
            public string stateCode
            {
                get
                {
                    return this.stateCodeField;
                }
                set
                {
                    this.stateCodeField = value;
                }
            }

            /// <remarks/>
            public string zipCode
            {
                get
                {
                    return this.zipCodeField;
                }
                set
                {
                    this.zipCodeField = value;
                }
            }

            /// <remarks/>
            public string zipExtensionCode
            {
                get
                {
                    return this.zipExtensionCodeField;
                }
                set
                {
                    this.zipExtensionCodeField = value;
                }
            }

            /// <remarks/>
            public string countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class headOfHouseholdRelatedMemberSubscriber
        {

            private string firstNameField;

            private string lastNameField;

            private string fullNameField;

            private string genderField;

            private string dateOfBirthField;

            private string taxIdField;

            private headOfHouseholdRelatedMemberSubscriberAddress addressField;

            /// <remarks/>
            public string firstName
            {
                get
                {
                    return this.firstNameField;
                }
                set
                {
                    this.firstNameField = value;
                }
            }

            /// <remarks/>
            public string lastName
            {
                get
                {
                    return this.lastNameField;
                }
                set
                {
                    this.lastNameField = value;
                }
            }

            /// <remarks/>
            public string fullName
            {
                get
                {
                    return this.fullNameField;
                }
                set
                {
                    this.fullNameField = value;
                }
            }

            /// <remarks/>
            public string gender
            {
                get
                {
                    return this.genderField;
                }
                set
                {
                    this.genderField = value;
                }
            }

            /// <remarks/>
            public string dateOfBirth
            {
                get
                {
                    return this.dateOfBirthField;
                }
                set
                {
                    this.dateOfBirthField = value;
                }
            }

            /// <remarks/>
            public string taxId
            {
                get
                {
                    return this.taxIdField;
                }
                set
                {
                    this.taxIdField = value;
                }
            }

            /// <remarks/>
            public headOfHouseholdRelatedMemberSubscriberAddress address
            {
                get
                {
                    return this.addressField;
                }
                set
                {
                    this.addressField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class headOfHouseholdRelatedMemberSubscriberAddress
        {

            private string addressField;

            private string cityNameField;

            private string stateCodeField;

            private string zipCodeField;

            private string zipExtensionCodeField;

            private string countryCodeField;

            /// <remarks/>
            public string address
            {
                get
                {
                    return this.addressField;
                }
                set
                {
                    this.addressField = value;
                }
            }

            /// <remarks/>
            public string cityName
            {
                get
                {
                    return this.cityNameField;
                }
                set
                {
                    this.cityNameField = value;
                }
            }

            /// <remarks/>
            public string stateCode
            {
                get
                {
                    return this.stateCodeField;
                }
                set
                {
                    this.stateCodeField = value;
                }
            }

            /// <remarks/>
            public string zipCode
            {
                get
                {
                    return this.zipCodeField;
                }
                set
                {
                    this.zipCodeField = value;
                }
            }

            /// <remarks/>
            public string zipExtensionCode
            {
                get
                {
                    return this.zipExtensionCodeField;
                }
                set
                {
                    this.zipExtensionCodeField = value;
                }
            }

            /// <remarks/>
            public string countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class headOfHouseholdExternalMemberInformation
        {

            private string matchDefinitionNameField;

            private string taxIdentificationNumberField;

            private headOfHouseholdExternalMemberInformationPrimaryName primaryNameField;

            private headOfHouseholdExternalMemberInformationAddressInfo addressInfoField;

            /// <remarks/>
            public string matchDefinitionName
            {
                get
                {
                    return this.matchDefinitionNameField;
                }
                set
                {
                    this.matchDefinitionNameField = value;
                }
            }

            /// <remarks/>
            public string taxIdentificationNumber
            {
                get
                {
                    return this.taxIdentificationNumberField;
                }
                set
                {
                    this.taxIdentificationNumberField = value;
                }
            }

            /// <remarks/>
            public headOfHouseholdExternalMemberInformationPrimaryName primaryName
            {
                get
                {
                    return this.primaryNameField;
                }
                set
                {
                    this.primaryNameField = value;
                }
            }

            /// <remarks/>
            public headOfHouseholdExternalMemberInformationAddressInfo addressInfo
            {
                get
                {
                    return this.addressInfoField;
                }
                set
                {
                    this.addressInfoField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class headOfHouseholdExternalMemberInformationPrimaryName
        {

            private string lastNameField;

            private string firstNameField;

            private string middleNameField;

            private string namePrefixField;

            private string nameSuffixField;

            private headOfHouseholdExternalMemberInformationPrimaryNameNameSuffixList nameSuffixListField;

            private headOfHouseholdExternalMemberInformationPrimaryNameCredentialSuffixList credentialSuffixListField;

            /// <remarks/>
            public string lastName
            {
                get
                {
                    return this.lastNameField;
                }
                set
                {
                    this.lastNameField = value;
                }
            }

            /// <remarks/>
            public string firstName
            {
                get
                {
                    return this.firstNameField;
                }
                set
                {
                    this.firstNameField = value;
                }
            }

            /// <remarks/>
            public string middleName
            {
                get
                {
                    return this.middleNameField;
                }
                set
                {
                    this.middleNameField = value;
                }
            }

            /// <remarks/>
            public string namePrefix
            {
                get
                {
                    return this.namePrefixField;
                }
                set
                {
                    this.namePrefixField = value;
                }
            }

            /// <remarks/>
            public string nameSuffix
            {
                get
                {
                    return this.nameSuffixField;
                }
                set
                {
                    this.nameSuffixField = value;
                }
            }

            /// <remarks/>
            public headOfHouseholdExternalMemberInformationPrimaryNameNameSuffixList nameSuffixList
            {
                get
                {
                    return this.nameSuffixListField;
                }
                set
                {
                    this.nameSuffixListField = value;
                }
            }

            /// <remarks/>
            public headOfHouseholdExternalMemberInformationPrimaryNameCredentialSuffixList credentialSuffixList
            {
                get
                {
                    return this.credentialSuffixListField;
                }
                set
                {
                    this.credentialSuffixListField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class headOfHouseholdExternalMemberInformationPrimaryNameNameSuffixList
        {

            private string listModeField;

            private string[] suffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("suffix")]
            public string[] suffix
            {
                get
                {
                    return this.suffixField;
                }
                set
                {
                    this.suffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class headOfHouseholdExternalMemberInformationPrimaryNameCredentialSuffixList
        {

            private string listModeField;

            private headOfHouseholdExternalMemberInformationPrimaryNameCredentialSuffixListCredentialSuffix[] credentialSuffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("CredentialSuffix")]
            public headOfHouseholdExternalMemberInformationPrimaryNameCredentialSuffixListCredentialSuffix[] CredentialSuffix
            {
                get
                {
                    return this.credentialSuffixField;
                }
                set
                {
                    this.credentialSuffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class headOfHouseholdExternalMemberInformationPrimaryNameCredentialSuffixListCredentialSuffix
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class headOfHouseholdExternalMemberInformationAddressInfo
        {

            private headOfHouseholdExternalMemberInformationAddressInfoPostalAddress postalAddressField;

            private headOfHouseholdExternalMemberInformationAddressInfoAddressPhoneList addressPhoneListField;

            /// <remarks/>
            public headOfHouseholdExternalMemberInformationAddressInfoPostalAddress postalAddress
            {
                get
                {
                    return this.postalAddressField;
                }
                set
                {
                    this.postalAddressField = value;
                }
            }

            /// <remarks/>
            public headOfHouseholdExternalMemberInformationAddressInfoAddressPhoneList addressPhoneList
            {
                get
                {
                    return this.addressPhoneListField;
                }
                set
                {
                    this.addressPhoneListField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class headOfHouseholdExternalMemberInformationAddressInfoPostalAddress
        {

            private string addressField;

            private string address2Field;

            private string address3Field;

            private string stateCodeField;

            private string zipCodeField;

            private string zipExtensionCodeField;

            private string cityNameField;

            private string countyCodeField;

            private headOfHouseholdExternalMemberInformationAddressInfoPostalAddressCountryCode countryCodeField;

            private string longitudeField;

            private string latitudeField;

            private string ignoreAddressCheckField;

            private string undeliverableAddressField;

            /// <remarks/>
            public string address
            {
                get
                {
                    return this.addressField;
                }
                set
                {
                    this.addressField = value;
                }
            }

            /// <remarks/>
            public string address2
            {
                get
                {
                    return this.address2Field;
                }
                set
                {
                    this.address2Field = value;
                }
            }

            /// <remarks/>
            public string address3
            {
                get
                {
                    return this.address3Field;
                }
                set
                {
                    this.address3Field = value;
                }
            }

            /// <remarks/>
            public string stateCode
            {
                get
                {
                    return this.stateCodeField;
                }
                set
                {
                    this.stateCodeField = value;
                }
            }

            /// <remarks/>
            public string zipCode
            {
                get
                {
                    return this.zipCodeField;
                }
                set
                {
                    this.zipCodeField = value;
                }
            }

            /// <remarks/>
            public string zipExtensionCode
            {
                get
                {
                    return this.zipExtensionCodeField;
                }
                set
                {
                    this.zipExtensionCodeField = value;
                }
            }

            /// <remarks/>
            public string cityName
            {
                get
                {
                    return this.cityNameField;
                }
                set
                {
                    this.cityNameField = value;
                }
            }

            /// <remarks/>
            public string countyCode
            {
                get
                {
                    return this.countyCodeField;
                }
                set
                {
                    this.countyCodeField = value;
                }
            }

            /// <remarks/>
            public headOfHouseholdExternalMemberInformationAddressInfoPostalAddressCountryCode countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }

            /// <remarks/>
            public string longitude
            {
                get
                {
                    return this.longitudeField;
                }
                set
                {
                    this.longitudeField = value;
                }
            }

            /// <remarks/>
            public string latitude
            {
                get
                {
                    return this.latitudeField;
                }
                set
                {
                    this.latitudeField = value;
                }
            }

            /// <remarks/>
            public string ignoreAddressCheck
            {
                get
                {
                    return this.ignoreAddressCheckField;
                }
                set
                {
                    this.ignoreAddressCheckField = value;
                }
            }

            /// <remarks/>
            public string undeliverableAddress
            {
                get
                {
                    return this.undeliverableAddressField;
                }
                set
                {
                    this.undeliverableAddressField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class headOfHouseholdExternalMemberInformationAddressInfoPostalAddressCountryCode
        {

            private string countryCodeField;

            /// <remarks/>
            public string countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class headOfHouseholdExternalMemberInformationAddressInfoAddressPhoneList
        {

            private string listModeField;

            private headOfHouseholdExternalMemberInformationAddressInfoAddressPhoneListTelephoneNumber[] telephoneNumberField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("telephoneNumber")]
            public headOfHouseholdExternalMemberInformationAddressInfoAddressPhoneListTelephoneNumber[] telephoneNumber
            {
                get
                {
                    return this.telephoneNumberField;
                }
                set
                {
                    this.telephoneNumberField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class headOfHouseholdExternalMemberInformationAddressInfoAddressPhoneListTelephoneNumber
        {

            private string phoneCountryCodeField;

            private string phoneAreaCodeField;

            private string phoneNumberField;

            private string phoneExtensionNumberField;

            private headOfHouseholdExternalMemberInformationAddressInfoAddressPhoneListTelephoneNumberIndividualPhoneTypeCode individualPhoneTypeCodeField;

            private headOfHouseholdExternalMemberInformationAddressInfoAddressPhoneListTelephoneNumberOrganizationPhoneTypeCode organizationPhoneTypeCodeField;

            /// <remarks/>
            public string phoneCountryCode
            {
                get
                {
                    return this.phoneCountryCodeField;
                }
                set
                {
                    this.phoneCountryCodeField = value;
                }
            }

            /// <remarks/>
            public string phoneAreaCode
            {
                get
                {
                    return this.phoneAreaCodeField;
                }
                set
                {
                    this.phoneAreaCodeField = value;
                }
            }

            /// <remarks/>
            public string phoneNumber
            {
                get
                {
                    return this.phoneNumberField;
                }
                set
                {
                    this.phoneNumberField = value;
                }
            }

            /// <remarks/>
            public string phoneExtensionNumber
            {
                get
                {
                    return this.phoneExtensionNumberField;
                }
                set
                {
                    this.phoneExtensionNumberField = value;
                }
            }

            /// <remarks/>
            public headOfHouseholdExternalMemberInformationAddressInfoAddressPhoneListTelephoneNumberIndividualPhoneTypeCode individualPhoneTypeCode
            {
                get
                {
                    return this.individualPhoneTypeCodeField;
                }
                set
                {
                    this.individualPhoneTypeCodeField = value;
                }
            }

            /// <remarks/>
            public headOfHouseholdExternalMemberInformationAddressInfoAddressPhoneListTelephoneNumberOrganizationPhoneTypeCode organizationPhoneTypeCode
            {
                get
                {
                    return this.organizationPhoneTypeCodeField;
                }
                set
                {
                    this.organizationPhoneTypeCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class headOfHouseholdExternalMemberInformationAddressInfoAddressPhoneListTelephoneNumberIndividualPhoneTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class headOfHouseholdExternalMemberInformationAddressInfoAddressPhoneListTelephoneNumberOrganizationPhoneTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class memberComplianceProgram
        {

            private memberComplianceProgramComplianceProgramSelections complianceProgramSelectionsField;

            private string resourceURIField;

            private string versionField;

            /// <remarks/>
            public memberComplianceProgramComplianceProgramSelections complianceProgramSelections
            {
                get
                {
                    return this.complianceProgramSelectionsField;
                }
                set
                {
                    this.complianceProgramSelectionsField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string resourceURI
            {
                get
                {
                    return this.resourceURIField;
                }
                set
                {
                    this.resourceURIField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string version
            {
                get
                {
                    return this.versionField;
                }
                set
                {
                    this.versionField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberComplianceProgramComplianceProgramSelections
        {

            private string updateModeField;

            private memberComplianceProgramComplianceProgramSelectionsComplianceProgramSelection[] complianceProgramSelectionField;

            /// <remarks/>
            public string updateMode
            {
                get
                {
                    return this.updateModeField;
                }
                set
                {
                    this.updateModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("complianceProgramSelection")]
            public memberComplianceProgramComplianceProgramSelectionsComplianceProgramSelection[] complianceProgramSelection
            {
                get
                {
                    return this.complianceProgramSelectionField;
                }
                set
                {
                    this.complianceProgramSelectionField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberComplianceProgramComplianceProgramSelectionsComplianceProgramSelection
        {

            private memberComplianceProgramComplianceProgramSelectionsComplianceProgramSelectionComplianceProgramDateRanges complianceProgramDateRangesField;

            private memberComplianceProgramComplianceProgramSelectionsComplianceProgramSelectionComplianceProgramReference complianceProgramReferenceField;

            /// <remarks/>
            public memberComplianceProgramComplianceProgramSelectionsComplianceProgramSelectionComplianceProgramDateRanges complianceProgramDateRanges
            {
                get
                {
                    return this.complianceProgramDateRangesField;
                }
                set
                {
                    this.complianceProgramDateRangesField = value;
                }
            }

            /// <remarks/>
            public memberComplianceProgramComplianceProgramSelectionsComplianceProgramSelectionComplianceProgramReference complianceProgramReference
            {
                get
                {
                    return this.complianceProgramReferenceField;
                }
                set
                {
                    this.complianceProgramReferenceField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberComplianceProgramComplianceProgramSelectionsComplianceProgramSelectionComplianceProgramDateRanges
        {

            private string startDateField;

            private string endDateField;

            private string complianceCodeField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }

            /// <remarks/>
            public string complianceCode
            {
                get
                {
                    return this.complianceCodeField;
                }
                set
                {
                    this.complianceCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberComplianceProgramComplianceProgramSelectionsComplianceProgramSelectionComplianceProgramReference
        {

            private string idField;

            /// <remarks/>
            public string ID
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class enrollmentMemberFactors
        {

            private string updateModeField;

            private enrollmentMemberFactorsMemberFactor[] memberFactorsField;

            /// <remarks/>
            public string updateMode
            {
                get
                {
                    return this.updateModeField;
                }
                set
                {
                    this.updateModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlArrayItemAttribute("memberFactor", IsNullable = false)]
            public enrollmentMemberFactorsMemberFactor[] memberFactors
            {
                get
                {
                    return this.memberFactorsField;
                }
                set
                {
                    this.memberFactorsField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class enrollmentMemberFactorsMemberFactor
        {

            private enrollmentMemberFactorsMemberFactorFactorType factorTypeField;

            private enrollmentMemberFactorsMemberFactorFactorDateRanges[] factorDateRangesField;

            /// <remarks/>
            public enrollmentMemberFactorsMemberFactorFactorType factorType
            {
                get
                {
                    return this.factorTypeField;
                }
                set
                {
                    this.factorTypeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("factorDateRanges")]
            public enrollmentMemberFactorsMemberFactorFactorDateRanges[] factorDateRanges
            {
                get
                {
                    return this.factorDateRangesField;
                }
                set
                {
                    this.factorDateRangesField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class enrollmentMemberFactorsMemberFactorFactorType
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class enrollmentMemberFactorsMemberFactorFactorDateRanges
        {

            private string factorStartDateField;

            private string factorEndDateField;

            private enrollmentMemberFactorsMemberFactorFactorDateRangesFactorQualifier factorQualifierField;

            private string factorValueField;

            /// <remarks/>
            public string factorStartDate
            {
                get
                {
                    return this.factorStartDateField;
                }
                set
                {
                    this.factorStartDateField = value;
                }
            }

            /// <remarks/>
            public string factorEndDate
            {
                get
                {
                    return this.factorEndDateField;
                }
                set
                {
                    this.factorEndDateField = value;
                }
            }

            /// <remarks/>
            public enrollmentMemberFactorsMemberFactorFactorDateRangesFactorQualifier factorQualifier
            {
                get
                {
                    return this.factorQualifierField;
                }
                set
                {
                    this.factorQualifierField = value;
                }
            }

            /// <remarks/>
            public string factorValue
            {
                get
                {
                    return this.factorValueField;
                }
                set
                {
                    this.factorValueField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class enrollmentMemberFactorsMemberFactorFactorDateRangesFactorQualifier
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class alternatePaymentContact
        {

            private alternatePaymentContactRecipientName recipientNameField;

            private alternatePaymentContactRecipientAddress recipientAddressField;

            /// <remarks/>
            public alternatePaymentContactRecipientName recipientName
            {
                get
                {
                    return this.recipientNameField;
                }
                set
                {
                    this.recipientNameField = value;
                }
            }

            /// <remarks/>
            public alternatePaymentContactRecipientAddress recipientAddress
            {
                get
                {
                    return this.recipientAddressField;
                }
                set
                {
                    this.recipientAddressField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class alternatePaymentContactRecipientName
        {

            private string lastNameField;

            private string firstNameField;

            private string middleNameField;

            private string namePrefixField;

            private string nameSuffixField;

            private alternatePaymentContactRecipientNameNameSuffixList nameSuffixListField;

            private alternatePaymentContactRecipientNameCredentialSuffixList credentialSuffixListField;

            /// <remarks/>
            public string lastName
            {
                get
                {
                    return this.lastNameField;
                }
                set
                {
                    this.lastNameField = value;
                }
            }

            /// <remarks/>
            public string firstName
            {
                get
                {
                    return this.firstNameField;
                }
                set
                {
                    this.firstNameField = value;
                }
            }

            /// <remarks/>
            public string middleName
            {
                get
                {
                    return this.middleNameField;
                }
                set
                {
                    this.middleNameField = value;
                }
            }

            /// <remarks/>
            public string namePrefix
            {
                get
                {
                    return this.namePrefixField;
                }
                set
                {
                    this.namePrefixField = value;
                }
            }

            /// <remarks/>
            public string nameSuffix
            {
                get
                {
                    return this.nameSuffixField;
                }
                set
                {
                    this.nameSuffixField = value;
                }
            }

            /// <remarks/>
            public alternatePaymentContactRecipientNameNameSuffixList nameSuffixList
            {
                get
                {
                    return this.nameSuffixListField;
                }
                set
                {
                    this.nameSuffixListField = value;
                }
            }

            /// <remarks/>
            public alternatePaymentContactRecipientNameCredentialSuffixList credentialSuffixList
            {
                get
                {
                    return this.credentialSuffixListField;
                }
                set
                {
                    this.credentialSuffixListField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class alternatePaymentContactRecipientNameNameSuffixList
        {

            private string listModeField;

            private string[] suffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("suffix")]
            public string[] suffix
            {
                get
                {
                    return this.suffixField;
                }
                set
                {
                    this.suffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class alternatePaymentContactRecipientNameCredentialSuffixList
        {

            private string listModeField;

            private alternatePaymentContactRecipientNameCredentialSuffixListCredentialSuffix[] credentialSuffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("CredentialSuffix")]
            public alternatePaymentContactRecipientNameCredentialSuffixListCredentialSuffix[] CredentialSuffix
            {
                get
                {
                    return this.credentialSuffixField;
                }
                set
                {
                    this.credentialSuffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class alternatePaymentContactRecipientNameCredentialSuffixListCredentialSuffix
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class alternatePaymentContactRecipientAddress
        {

            private string addressField;

            private string address2Field;

            private string address3Field;

            private string stateCodeField;

            private string zipCodeField;

            private string zipExtensionCodeField;

            private string cityNameField;

            private string countyCodeField;

            private alternatePaymentContactRecipientAddressCountryCode countryCodeField;

            private string longitudeField;

            private string latitudeField;

            private string ignoreAddressCheckField;

            private string undeliverableAddressField;

            /// <remarks/>
            public string address
            {
                get
                {
                    return this.addressField;
                }
                set
                {
                    this.addressField = value;
                }
            }

            /// <remarks/>
            public string address2
            {
                get
                {
                    return this.address2Field;
                }
                set
                {
                    this.address2Field = value;
                }
            }

            /// <remarks/>
            public string address3
            {
                get
                {
                    return this.address3Field;
                }
                set
                {
                    this.address3Field = value;
                }
            }

            /// <remarks/>
            public string stateCode
            {
                get
                {
                    return this.stateCodeField;
                }
                set
                {
                    this.stateCodeField = value;
                }
            }

            /// <remarks/>
            public string zipCode
            {
                get
                {
                    return this.zipCodeField;
                }
                set
                {
                    this.zipCodeField = value;
                }
            }

            /// <remarks/>
            public string zipExtensionCode
            {
                get
                {
                    return this.zipExtensionCodeField;
                }
                set
                {
                    this.zipExtensionCodeField = value;
                }
            }

            /// <remarks/>
            public string cityName
            {
                get
                {
                    return this.cityNameField;
                }
                set
                {
                    this.cityNameField = value;
                }
            }

            /// <remarks/>
            public string countyCode
            {
                get
                {
                    return this.countyCodeField;
                }
                set
                {
                    this.countyCodeField = value;
                }
            }

            /// <remarks/>
            public alternatePaymentContactRecipientAddressCountryCode countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }

            /// <remarks/>
            public string longitude
            {
                get
                {
                    return this.longitudeField;
                }
                set
                {
                    this.longitudeField = value;
                }
            }

            /// <remarks/>
            public string latitude
            {
                get
                {
                    return this.latitudeField;
                }
                set
                {
                    this.latitudeField = value;
                }
            }

            /// <remarks/>
            public string ignoreAddressCheck
            {
                get
                {
                    return this.ignoreAddressCheckField;
                }
                set
                {
                    this.ignoreAddressCheckField = value;
                }
            }

            /// <remarks/>
            public string undeliverableAddress
            {
                get
                {
                    return this.undeliverableAddressField;
                }
                set
                {
                    this.undeliverableAddressField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class alternatePaymentContactRecipientAddressCountryCode
        {

            private string countryCodeField;

            /// <remarks/>
            public string countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class errorsError
        {

            private string errorTypeField;

            private string messageField;

            private string dateTimeField;

            private string authenticatedUserField;

            private string traceInfoField;

            /// <remarks/>
            public string errorType
            {
                get
                {
                    return this.errorTypeField;
                }
                set
                {
                    this.errorTypeField = value;
                }
            }

            /// <remarks/>
            public string message
            {
                get
                {
                    return this.messageField;
                }
                set
                {
                    this.messageField = value;
                }
            }

            /// <remarks/>
            public string dateTime
            {
                get
                {
                    return this.dateTimeField;
                }
                set
                {
                    this.dateTimeField = value;
                }
            }

            /// <remarks/>
            public string authenticatedUser
            {
                get
                {
                    return this.authenticatedUserField;
                }
                set
                {
                    this.authenticatedUserField = value;
                }
            }

            /// <remarks/>
            public string traceInfo
            {
                get
                {
                    return this.traceInfoField;
                }
                set
                {
                    this.traceInfoField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class errors
        {

            private errorsError[] errorField;

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("error")]
            public errorsError[] error
            {
                get
                {
                    return this.errorField;
                }
                set
                {
                    this.errorField = value;
                }
            }
        }



    }
}

using System;
using System.Linq;
using System.Collections.Generic;

namespace DataLayer.HealthRulesConnectors.Models.Responses
{
    /// <summary>
    /// Wraps all response objects fot the MembershipReferenceLookup
    /// </summary>
    public class MembershipReferenceLookupResponse
    {

        public class Response
        {
            public EnvelopeInner Envelope { get; set; }

            /// <summary>
            /// Call Status - Shortcut to: Envelope.Body.membershipReferenceLookupResponse.status
            /// </summary>
            public string Status
            {
                get
                {
                    try
                    {
                        return Envelope.Body.membershipReferenceLookupResponse.status;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// List of Errors - Shortcut to: Envelope.Body.membershipReferenceLookupResponse.errors.error
            /// </summary>
            public List<Error> Errors
            {
                get
                {
                    try
                    {
                        return Envelope.Body.membershipReferenceLookupResponse.errors.error;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            public string StatusFull
            {
                get
                {
                    string output = "";
                    if (Status == null)
                        return "Error - Undefined"; ;

                    output = "Status: " + Status;

                    if (Errors != null)
                    {
                        for (int i = 0; i < Errors.Count; i++)
                        {
                            output += "\r\n";

                            if (!string.IsNullOrWhiteSpace(Errors[i].dateTime))
                                output += $"Date of Error     : {Errors[i].dateTime}\r\n";
                            if (!string.IsNullOrWhiteSpace(Errors[i].authenticatedUser))
                                output += $"Authenticated User: {Errors[i].authenticatedUser}\r\n";
                            if (!string.IsNullOrWhiteSpace(Errors[i].errorType))
                                output += $"Error Type        : {Errors[i].errorType}\r\n";
                            if (!string.IsNullOrWhiteSpace(Errors[i].message))
                                output += $"Message           : {Errors[i].message}\r\n";
                            if (!string.IsNullOrWhiteSpace(Errors[i].traceInfo))
                                output += $"Trace Info        : {Errors[i].traceInfo}\r\n";
                        }
                    }

                    return output;
                }

            }

            /// <summary>
            /// Member ID that was found -
            /// Shortcut to: Envelope.Body.membershipReferenceLookupResponse.lookupReference.ID
            /// </summary>
            public string LookupReferenceID
            {
                get
                {
                    try
                    {
                        return Envelope.Body.membershipReferenceLookupResponse.lookupReference.ID;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

        }

        public class EnvelopeInner
        {
            public string Header { get; set; }
            public Body Body { get; set; }
        }

        public class Body
        {
            public MembershipReferenceLookupResponse2 membershipReferenceLookupResponse { get; set; }
        }

        public class MembershipReferenceLookupResponse2
        {
            public LookupReference lookupReference { get; set; }
            public string status { get; set; }
            public Errors errors { get; set; }
        }

        public class LookupReference
        {
            public string ID { get; set; }
        }

        public class Errors
        {
            public List<Error> error { get; set; }
        }

        public class Error
        {
            public string errorType { get; set; }
            public string message { get; set; }
            public string dateTime { get; set; }
            public string authenticatedUser { get; set; }
            public string traceInfo { get; set; }
        }


    }
}

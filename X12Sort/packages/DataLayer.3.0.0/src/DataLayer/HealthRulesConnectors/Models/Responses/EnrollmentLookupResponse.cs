using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.HealthRulesConnectors.Models.Responses
{
    public partial class EnrollmentLookupResponse
    {
        // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
        /// <summary>
        /// Envelope - Parent of the Response tree
        /// </summary>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://schemas.xmlsoap.org/soap/envelope/", IsNullable = false)]
        public partial class Envelope
        {
            /// <summary>
            /// Header - This should either be null or an empty string
            /// </summary>
            private string headerField = null;

            /// <summary>
            /// Holds the content of the core Body object
            /// </summary>
            private EnvelopeBody bodyField;

            /// <summary>
            /// Header - This should either be null or an empty string
            /// </summary>
            public string Header
            {
                get
                {
                    return this.headerField;
                }
                set
                {
                    this.headerField = value;
                }
            }

            /// <summary>
            /// Core Body object
            /// </summary>
            public EnvelopeBody Body
            {
                get
                {
                    return this.bodyField;
                }
                set
                {
                    this.bodyField = value;
                }
            }

            #region Error trapping

            /// <summary>
            /// Call status
            /// </summary>
            public string Status {
                get
                {
                    try
                    {
                        return Body.enrollmentSparseLookupResponse.status;
                    }catch(Exception)
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// Errors
            /// </summary>
            public errorsError[] Errors
            {
                get
                {
                    try
                    {
                        return Body.enrollmentSparseLookupResponse.errors;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            #endregion

            #region Shortcut to Body > enrollmentSparseLookupResponse > enrollment objects

            /// <summary>
            /// Where to find documenation
            /// </summary>
            public static string Information
            {
                get
                {
                    return "Go to page #683 at this link: https://sps.promedica.org/it/pcsi/Shared%20Documents/Project%20Planning/HealthEdge%20Documentation/Health%20Rules%2020.2/Connector/20.2_HealthRules_Connector_Guide_Vol5_Extracts_and_Services.pdf" +
                        "\r\nand look for documentation in the DataLayer doc's folder at: http://gitlab01.promedica.org/phc/datalayer/tree/master/docs/datalayer/API/HealthRulesConnectors";
                }
            }

            /// <summary>
            /// Transaction identifier, used for tracking.
            /// </summary>
            public string TransactionID
            {
                get
                {
                    try
                    {
                        return Body.enrollmentSparseLookupResponse.enrollment.transactionId;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// Action Mode - default "SPARSE"
            /// An enumerated value that can be one of the following:
            /// FULL - Reserved for future use.
            /// SPARSE - Perform a sparse enrollment or update.
            /// AUDIT - Check the submitted enrollment file for processing errors.
            /// REPLACE - Used by the HMEM module.
            /// </summary>
            public ActionMode ActionMode
            {
                get
                {
                    try
                    {
                        string tempValue = Body.enrollmentSparseLookupResponse.enrollment.actionMode;
                        return Validation.ActionModeValidation(tempValue);
                    }
                    catch (Exception)
                    {
                        return ActionMode.Unknown;
                    }
                }
            }

            /// <summary>
            /// As of date to use for data committed to Payor.
            /// </summary>
            public DateTime? AsOfDate
            {
                get
                {
                    try
                    {
                        return Validation.ParseDateTime(
                            Body.enrollmentSparseLookupResponse.enrollment.asOfDate);
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// Active Plan
            /// </summary>
            public YesNo Activate
            {
                get
                {
                    try
                    {
                        return Validation.YesNoValidation(
                            Body.enrollmentSparseLookupResponse.enrollment.activate);
                    }
                    catch (Exception)
                    {
                        return YesNo.Unknown;
                    }
                }
            }

            /// <summary>
            /// The subscriber ID, if an update
            /// </summary>
            public string SubscriberId
            {
                get
                {
                    try
                    {
                        return Body.enrollmentSparseLookupResponse.enrollment.subscriberId;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// Terminations are cascaded
            /// </summary>
            public bool ? CascadeTerms
            {
                get
                {
                    try
                    {
                        return Body.enrollmentSparseLookupResponse.enrollment.cascadeTerms;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// Cancellations are cascaded
            /// </summary>
            public bool ? CascadeCancels
            {
                get
                {
                    try
                    {
                        return Body.enrollmentSparseLookupResponse.enrollment.cascadeCancels;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// Reinstate are cascaded
            /// </summary>
            public bool ? CascadeReinstate
            {
                get
                {
                    try
                    {
                        return Body.enrollmentSparseLookupResponse.enrollment.cascadeReinstate;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// If yes, any errors encountered during enrollment processing
            /// result in sending the member to the workbasket.
            /// </summary>
            public bool ? SendToWorkBasketIfExceptionsPresent
            {
                get
                {
                    try
                    {
                        return Body.enrollmentSparseLookupResponse.enrollment.sendToWorkBasketIfExceptionsPresent;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            public subscription Subscription
            {
                get
                {
                    try
                    {
                        return Body.enrollmentSparseLookupResponse.enrollment.subscription;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// Members associated with this
            /// </summary>
            public member[] Members
            {
                get
                {
                    try
                    {
                        return Body.enrollmentSparseLookupResponse.enrollment.member;
                    }catch(Exception) {
                        return null;
                    }
                }
            }

            public string ResourceURI
            {
                get
                {
                    try
                    {
                        return Body.enrollmentSparseLookupResponse.enrollment.resourceURI;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            public string Version
            {
                get
                {
                    try
                    {
                        return Body.enrollmentSparseLookupResponse.enrollment.version;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }


            #endregion Shortcut to Body > enrollmentSparseLookupResponse > enrollment objects

        }

        /// <summary>
        /// Envelope Body - Secondary branch from parent Envelope object
        /// </summary>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public partial class EnvelopeBody
        {
            /// <summary>
            /// Backing field for the enrollmentSparseLookupResponse
            /// </summary>
            private enrollmentSparseLookupResponse enrollmentSparseLookupResponseField;

            /// <summary>
            /// Enrollment Sparse class structure
            /// </summary>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.healthedge.com/connector/schema/enrollmentsparselookup")]
            public enrollmentSparseLookupResponse enrollmentSparseLookupResponse
            {
                get
                {
                    return this.enrollmentSparseLookupResponseField;
                }
                set
                {
                    this.enrollmentSparseLookupResponseField = value;
                }
            }
        }

        /// <summary>
        /// Enrollment Sparse - tertary branch from Body
        /// Main enrollement and Error track will be held in here
        /// </summary>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.healthedge.com/connector/schema/enrollmentsparselookup")]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.healthedge.com/connector/schema/enrollmentsparselookup", IsNullable = false)]
        public partial class enrollmentSparseLookupResponse
        {
            /// <summary>
            /// Backing field the hold level of useable for the 
            /// </summary>
            private enrollment enrollmentField;

            [NonSerialized]
            private string statusField;

            [NonSerialized]
            private errorsError[] errorsField;

            /// <summary>
            /// Primary object that holds enrollment data
            /// </summary>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.healthedge.com/connector/schema/enrollmentsparse")]
            public enrollment enrollment
            {
                get
                {
                    return this.enrollmentField;
                }
                set
                {
                    this.enrollmentField = value;
                }
            }

            /// <summary>
            /// SOAP call status
            /// </summary>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public string status
            {
                get
                {
                    return this.statusField;
                }
                set
                {
                    this.statusField = value;
                }
            }

            /// <summary>
            /// List of Errors
            /// </summary>
            [System.Xml.Serialization.XmlArrayAttribute(Namespace = "")]
            [System.Xml.Serialization.XmlArrayItemAttribute("error", IsNullable = false)]
            public errorsError[] errors
            {
                get
                {
                    return this.errorsField;
                }
                set
                {
                    this.errorsField = value;
                }
            }
        }

        /// <summary>
        /// Primary object that holds enrollment data
        /// </summary>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.healthedge.com/connector/schema/enrollmentsparse")]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.healthedge.com/connector/schema/enrollmentsparse", IsNullable = false)]
        public partial class enrollment
        {

            private string transactionIdField;

            private string actionModeField;

            private string asOfDateField;

            private string activateField;

            private string subscriberIdField;

            private bool cascadeTermsField;

            private bool cascadeCancelsField;

            private bool cascadeReinstateField;

            private bool sendToWorkBasketIfExceptionsPresentField;

            private subscription subscriptionField;

            private member[] memberField;

            private string resourceURIField;

            private string versionField;

            /// <summary>
            /// Transaction identifier, used for tracking.
            /// <example>12345</example>
            /// </summary>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public string transactionId
            {
                get
                {
                    return this.transactionIdField;
                }
                set
                {
                    this.transactionIdField = value;
                }
            }

            /// <summary>
            /// Action mode
            /// <example>
            /// An enumerated value that can be one of the following
            /// FULL - Reserved for future use.
            /// SPARSE - Perform a sparse enrollment or update.
            /// AUDIT - Check the submitted enrollment file for processing errors.
            /// REPLACE - Used by the HMEM module.
            /// </example>
            /// </summary>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public string actionMode
            {
                get
                {
                    return this.actionModeField;
                }
                set
                {
                    this.actionModeField = value;
                }
            }

            /// <summary>
            /// As of date to use for data committed to Payor
            /// <example>Format: 2020-01-01</example>
            /// </summary>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public string asOfDate
            {
                get
                {
                    return this.asOfDateField;
                }
                set
                {
                    this.asOfDateField = value;
                }
            }

            /// <summary>
            /// If the benefit plan requires a binder payment in order to be
            /// activated, setting this value to True activates the plan.If the
            /// plan is active, then setting this to False deactivates the plan.
            /// If no binder payment is required by the benefit plan, this value is
            /// ignored.
            /// <example>true / false</example>
            /// </summary>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public string activate
            {
                get
                {
                    return this.activateField;
                }
                set
                {
                    this.activateField = value;
                }
            }

            /// <summary>
            /// Subscriber member #
            /// </summary>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public string subscriberId
            {
                get
                {
                    return this.subscriberIdField;
                }
                set
                {
                    this.subscriberIdField = value;
                }
            }

            /// <summary>
            /// Cascade COB terms? true or false
            /// </summary>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public bool cascadeTerms
            {
                get
                {
                    return this.cascadeTermsField;
                }
                set
                {
                    this.cascadeTermsField = value;
                }
            }

            /// <summary>
            /// Cascade cancel all member's policy?
            /// </summary>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public bool cascadeCancels
            {
                get
                {
                    return this.cascadeCancelsField;
                }
                set
                {
                    this.cascadeCancelsField = value;
                }
            }

            /// <summary>
            /// Casacade re-enstate all member policies
            /// </summary>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public bool cascadeReinstate
            {
                get
                {
                    return this.cascadeReinstateField;
                }
                set
                {
                    this.cascadeReinstateField = value;
                }
            }

            /// <summary>
            /// Send subscriber to workbaseket for review
            /// </summary>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public bool sendToWorkBasketIfExceptionsPresent
            {
                get
                {
                    return this.sendToWorkBasketIfExceptionsPresentField;
                }
                set
                {
                    this.sendToWorkBasketIfExceptionsPresentField = value;
                }
            }

            /// <summary>
            /// Member's subscription
            /// </summary>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public subscription subscription
            {
                get
                {
                    return this.subscriptionField;
                }
                set
                {
                    this.subscriptionField = value;
                }
            }

            /// <summary>
            /// Individual members in a subscription
            /// </summary>
            [System.Xml.Serialization.XmlElementAttribute("member", Namespace = "")]
            public member[] member
            {
                get
                {
                    return this.memberField;
                }
                set
                {
                    this.memberField = value;
                }
            }

            /// <summary>
            /// Connector resouces URI
            /// </summary>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string resourceURI
            {
                get
                {
                    return this.resourceURIField;
                }
                set
                {
                    this.resourceURIField = value;
                }
            }

            /// <summary>
            /// Version of the Connector
            /// </summary>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string version
            {
                get
                {
                    return this.versionField;
                }
                set
                {
                    this.versionField = value;
                }
            }
        }

        /// <summary>
        /// Subscription object
        /// </summary>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class subscription
        {

            private subscriptionMaintenanceReasonCode maintenanceReasonCodeField;

            private string maintenanceCommentField;

            private string subscriptionIdentifierField;

            private string payeeHccIdField;

            private string hccIdentifierField;

            private string receiptDateField;

            private bool autoWithdrawalEnabledField;

            private string originalEffectiveDateField;

            private string applicationSignatureField;

            private string applicationDateField;

            private string lastPremiumPaidDateField;

            private string lastPremiumPaymentDateField;

            private subscriptionPaymentHold paymentHoldField;

            private subscriptionSubscriptionUDTList subscriptionUDTListField;

            private subscriptionAccountMatchData accountMatchDataField;

            private subscriptionAccountReference accountReferenceField;

            private string divisionField;

            private string memberGroupField;

            private subscriptionVipReasonCode vipReasonCodeField;

            private subscriptionInformationSourceCode informationSourceCodeField;

            private subscriptionCobraSelection cobraSelectionField;

            private subscriptionClaimReviewList claimReviewListField;

            private subscriptionSubsidy[] subsidyField;

            private subscriptionApplicationInfo applicationInfoField;

            private subscriptionBrokerSelections brokerSelectionsField;

            private subscriptionPaymentType paymentTypeField;

            private subscriptionPayeeBankAccount payeeBankAccountField;

            private string resourceURIField;

            private string versionField;

            /// <summary>
            /// Latest Maintenance Reason Code
            /// </summary>
            public subscriptionMaintenanceReasonCode maintenanceReasonCode
            {
                get
                {
                    return this.maintenanceReasonCodeField;
                }
                set
                {
                    this.maintenanceReasonCodeField = value;
                }
            }

            /// <summary>
            /// Latest Maintenance Reason Comment
            /// </summary>
            public string maintenanceComment
            {
                get
                {
                    return this.maintenanceCommentField;
                }
                set
                {
                    this.maintenanceCommentField = value;
                }
            }

            /// <remarks/>
            public string subscriptionIdentifier
            {
                get
                {
                    return this.subscriptionIdentifierField;
                }
                set
                {
                    this.subscriptionIdentifierField = value;
                }
            }

            /// <remarks/>
            public string payeeHccId
            {
                get
                {
                    return this.payeeHccIdField;
                }
                set
                {
                    this.payeeHccIdField = value;
                }
            }

            /// <remarks/>
            public string hccIdentifier
            {
                get
                {
                    return this.hccIdentifierField;
                }
                set
                {
                    this.hccIdentifierField = value;
                }
            }

            /// <remarks/>
            public string receiptDate
            {
                get
                {
                    return this.receiptDateField;
                }
                set
                {
                    this.receiptDateField = value;
                }
            }

            /// <remarks/>
            public bool autoWithdrawalEnabled
            {
                get
                {
                    return this.autoWithdrawalEnabledField;
                }
                set
                {
                    this.autoWithdrawalEnabledField = value;
                }
            }

            /// <remarks/>
            public string originalEffectiveDate
            {
                get
                {
                    return this.originalEffectiveDateField;
                }
                set
                {
                    this.originalEffectiveDateField = value;
                }
            }

            /// <remarks/>
            public string applicationSignature
            {
                get
                {
                    return this.applicationSignatureField;
                }
                set
                {
                    this.applicationSignatureField = value;
                }
            }

            /// <remarks/>
            public string applicationDate
            {
                get
                {
                    return this.applicationDateField;
                }
                set
                {
                    this.applicationDateField = value;
                }
            }

            /// <remarks/>
            public string lastPremiumPaidDate
            {
                get
                {
                    return this.lastPremiumPaidDateField;
                }
                set
                {
                    this.lastPremiumPaidDateField = value;
                }
            }

            /// <remarks/>
            public string lastPremiumPaymentDate
            {
                get
                {
                    return this.lastPremiumPaymentDateField;
                }
                set
                {
                    this.lastPremiumPaymentDateField = value;
                }
            }

            /// <remarks/>
            public subscriptionPaymentHold paymentHold
            {
                get
                {
                    return this.paymentHoldField;
                }
                set
                {
                    this.paymentHoldField = value;
                }
            }

            /// <remarks/>
            public subscriptionSubscriptionUDTList subscriptionUDTList
            {
                get
                {
                    return this.subscriptionUDTListField;
                }
                set
                {
                    this.subscriptionUDTListField = value;
                }
            }

            /// <remarks/>
            public subscriptionAccountMatchData accountMatchData
            {
                get
                {
                    return this.accountMatchDataField;
                }
                set
                {
                    this.accountMatchDataField = value;
                }
            }

            /// <remarks/>
            public subscriptionAccountReference accountReference
            {
                get
                {
                    return this.accountReferenceField;
                }
                set
                {
                    this.accountReferenceField = value;
                }
            }

            /// <remarks/>
            public string division
            {
                get
                {
                    return this.divisionField;
                }
                set
                {
                    this.divisionField = value;
                }
            }

            /// <remarks/>
            public string memberGroup
            {
                get
                {
                    return this.memberGroupField;
                }
                set
                {
                    this.memberGroupField = value;
                }
            }

            /// <remarks/>
            public subscriptionVipReasonCode vipReasonCode
            {
                get
                {
                    return this.vipReasonCodeField;
                }
                set
                {
                    this.vipReasonCodeField = value;
                }
            }

            /// <remarks/>
            public subscriptionInformationSourceCode informationSourceCode
            {
                get
                {
                    return this.informationSourceCodeField;
                }
                set
                {
                    this.informationSourceCodeField = value;
                }
            }

            /// <remarks/>
            public subscriptionCobraSelection cobraSelection
            {
                get
                {
                    return this.cobraSelectionField;
                }
                set
                {
                    this.cobraSelectionField = value;
                }
            }

            /// <remarks/>
            public subscriptionClaimReviewList claimReviewList
            {
                get
                {
                    return this.claimReviewListField;
                }
                set
                {
                    this.claimReviewListField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("subsidy")]
            public subscriptionSubsidy[] subsidy
            {
                get
                {
                    return this.subsidyField;
                }
                set
                {
                    this.subsidyField = value;
                }
            }

            /// <remarks/>
            public subscriptionApplicationInfo applicationInfo
            {
                get
                {
                    return this.applicationInfoField;
                }
                set
                {
                    this.applicationInfoField = value;
                }
            }

            /// <remarks/>
            public subscriptionBrokerSelections brokerSelections
            {
                get
                {
                    return this.brokerSelectionsField;
                }
                set
                {
                    this.brokerSelectionsField = value;
                }
            }

            /// <remarks/>
            public subscriptionPaymentType paymentType
            {
                get
                {
                    return this.paymentTypeField;
                }
                set
                {
                    this.paymentTypeField = value;
                }
            }

            /// <remarks/>
            public subscriptionPayeeBankAccount payeeBankAccount
            {
                get
                {
                    return this.payeeBankAccountField;
                }
                set
                {
                    this.payeeBankAccountField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string resourceURI
            {
                get
                {
                    return this.resourceURIField;
                }
                set
                {
                    this.resourceURIField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string version
            {
                get
                {
                    return this.versionField;
                }
                set
                {
                    this.versionField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionMaintenanceReasonCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionPaymentHold
        {

            private string enablePaymentHoldField;

            private string requestingUserField;

            private string paymentHoldReasonDescriptionField;

            private subscriptionPaymentHoldPaymentHoldReasonCode paymentHoldReasonCodeField;

            /// <remarks/>
            public string enablePaymentHold
            {
                get
                {
                    return this.enablePaymentHoldField;
                }
                set
                {
                    this.enablePaymentHoldField = value;
                }
            }

            /// <remarks/>
            public string requestingUser
            {
                get
                {
                    return this.requestingUserField;
                }
                set
                {
                    this.requestingUserField = value;
                }
            }

            /// <remarks/>
            public string paymentHoldReasonDescription
            {
                get
                {
                    return this.paymentHoldReasonDescriptionField;
                }
                set
                {
                    this.paymentHoldReasonDescriptionField = value;
                }
            }

            /// <remarks/>
            public subscriptionPaymentHoldPaymentHoldReasonCode paymentHoldReasonCode
            {
                get
                {
                    return this.paymentHoldReasonCodeField;
                }
                set
                {
                    this.paymentHoldReasonCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionPaymentHoldPaymentHoldReasonCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionSubscriptionUDTList
        {

            private string listModeField;

            private subscriptionSubscriptionUDTListSubscriptionUDT[] subscriptionUDTField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("subscriptionUDT")]
            public subscriptionSubscriptionUDTListSubscriptionUDT[] subscriptionUDT
            {
                get
                {
                    return this.subscriptionUDTField;
                }
                set
                {
                    this.subscriptionUDTField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionSubscriptionUDTListSubscriptionUDT
        {

            private subscriptionSubscriptionUDTListSubscriptionUDTUdtListValueSet[] udtListValueSetField;

            private subscriptionSubscriptionUDTListSubscriptionUDTUserDefinedTermReference userDefinedTermReferenceField;

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("udtListValueSet")]
            public subscriptionSubscriptionUDTListSubscriptionUDTUdtListValueSet[] udtListValueSet
            {
                get
                {
                    return this.udtListValueSetField;
                }
                set
                {
                    this.udtListValueSetField = value;
                }
            }

            /// <remarks/>
            public subscriptionSubscriptionUDTListSubscriptionUDTUserDefinedTermReference userDefinedTermReference
            {
                get
                {
                    return this.userDefinedTermReferenceField;
                }
                set
                {
                    this.userDefinedTermReferenceField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionSubscriptionUDTListSubscriptionUDTUdtListValueSet
        {

            private string attributeRoleNameField;

            private string attrValueAsStringField;

            /// <remarks/>
            public string attributeRoleName
            {
                get
                {
                    return this.attributeRoleNameField;
                }
                set
                {
                    this.attributeRoleNameField = value;
                }
            }

            /// <remarks/>
            public string attrValueAsString
            {
                get
                {
                    return this.attrValueAsStringField;
                }
                set
                {
                    this.attrValueAsStringField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionSubscriptionUDTListSubscriptionUDTUserDefinedTermReference
        {

            private string idField;

            /// <remarks/>
            public string ID
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionAccountMatchData
        {

            private subscriptionAccountMatchDataAccountHccIdentifier accountHccIdentifierField;

            private subscriptionAccountMatchDataAccountOtherIdentifier[] accountOtherIdentifierField;

            private string asOfDateField;

            /// <remarks/>
            public subscriptionAccountMatchDataAccountHccIdentifier accountHccIdentifier
            {
                get
                {
                    return this.accountHccIdentifierField;
                }
                set
                {
                    this.accountHccIdentifierField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("accountOtherIdentifier")]
            public subscriptionAccountMatchDataAccountOtherIdentifier[] accountOtherIdentifier
            {
                get
                {
                    return this.accountOtherIdentifierField;
                }
                set
                {
                    this.accountOtherIdentifierField = value;
                }
            }

            /// <remarks/>
            public string asOfDate
            {
                get
                {
                    return this.asOfDateField;
                }
                set
                {
                    this.asOfDateField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionAccountMatchDataAccountHccIdentifier
        {

            private string accountHccIdentificationNumberField;

            private string parentAccountHccIdentificationNumberField;

            private string topAccountHccIdentificationNumberField;

            /// <remarks/>
            public string accountHccIdentificationNumber
            {
                get
                {
                    return this.accountHccIdentificationNumberField;
                }
                set
                {
                    this.accountHccIdentificationNumberField = value;
                }
            }

            /// <remarks/>
            public string parentAccountHccIdentificationNumber
            {
                get
                {
                    return this.parentAccountHccIdentificationNumberField;
                }
                set
                {
                    this.parentAccountHccIdentificationNumberField = value;
                }
            }

            /// <remarks/>
            public string topAccountHccIdentificationNumber
            {
                get
                {
                    return this.topAccountHccIdentificationNumberField;
                }
                set
                {
                    this.topAccountHccIdentificationNumberField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionAccountMatchDataAccountOtherIdentifier
        {

            private string otherIdentificationNumberField;

            private subscriptionAccountMatchDataAccountOtherIdentifierIdentificationType identificationTypeField;

            /// <remarks/>
            public string otherIdentificationNumber
            {
                get
                {
                    return this.otherIdentificationNumberField;
                }
                set
                {
                    this.otherIdentificationNumberField = value;
                }
            }

            /// <remarks/>
            public subscriptionAccountMatchDataAccountOtherIdentifierIdentificationType identificationType
            {
                get
                {
                    return this.identificationTypeField;
                }
                set
                {
                    this.identificationTypeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionAccountMatchDataAccountOtherIdentifierIdentificationType
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionAccountReference
        {

            private string identifierField;

            /// <remarks/>
            public string identifier
            {
                get
                {
                    return this.identifierField;
                }
                set
                {
                    this.identifierField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionVipReasonCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionInformationSourceCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionCobraSelection
        {

            private string cobraStartField;

            private string cobraEndField;

            private string cobraReceiptDateField;

            private string cobraPaidThroughDateField;

            private string commentField;

            private subscriptionCobraSelectionCobraQualifyingEventCode cobraQualifyingEventCodeField;

            /// <remarks/>
            public string cobraStart
            {
                get
                {
                    return this.cobraStartField;
                }
                set
                {
                    this.cobraStartField = value;
                }
            }

            /// <remarks/>
            public string cobraEnd
            {
                get
                {
                    return this.cobraEndField;
                }
                set
                {
                    this.cobraEndField = value;
                }
            }

            /// <remarks/>
            public string cobraReceiptDate
            {
                get
                {
                    return this.cobraReceiptDateField;
                }
                set
                {
                    this.cobraReceiptDateField = value;
                }
            }

            /// <remarks/>
            public string cobraPaidThroughDate
            {
                get
                {
                    return this.cobraPaidThroughDateField;
                }
                set
                {
                    this.cobraPaidThroughDateField = value;
                }
            }

            /// <remarks/>
            public string comment
            {
                get
                {
                    return this.commentField;
                }
                set
                {
                    this.commentField = value;
                }
            }

            /// <remarks/>
            public subscriptionCobraSelectionCobraQualifyingEventCode cobraQualifyingEventCode
            {
                get
                {
                    return this.cobraQualifyingEventCodeField;
                }
                set
                {
                    this.cobraQualifyingEventCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionCobraSelectionCobraQualifyingEventCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionClaimReviewList
        {

            private string listModeField;

            private subscriptionClaimReviewListClaimReview[] claimReviewField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("claimReview")]
            public subscriptionClaimReviewListClaimReview[] claimReview
            {
                get
                {
                    return this.claimReviewField;
                }
                set
                {
                    this.claimReviewField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionClaimReviewListClaimReview
        {

            private string enableClaimReviewField;

            private string claimTypeField;

            private subscriptionClaimReviewListClaimReviewClaimReviewReasonCode claimReviewReasonCodeField;

            private string reviewReasonDescriptionField;

            private string requestingUserField;

            private string reviewServiceStartDateField;

            private string reviewServiceEndDateField;

            private string reviewReceiptStartDateField;

            private string reviewReceiptEndDateField;

            private subscriptionClaimReviewListClaimReviewReviewPractitionerList reviewPractitionerListField;

            private subscriptionClaimReviewListClaimReviewReviewSupplierList reviewSupplierListField;

            /// <remarks/>
            public string enableClaimReview
            {
                get
                {
                    return this.enableClaimReviewField;
                }
                set
                {
                    this.enableClaimReviewField = value;
                }
            }

            /// <remarks/>
            public string claimType
            {
                get
                {
                    return this.claimTypeField;
                }
                set
                {
                    this.claimTypeField = value;
                }
            }

            /// <remarks/>
            public subscriptionClaimReviewListClaimReviewClaimReviewReasonCode claimReviewReasonCode
            {
                get
                {
                    return this.claimReviewReasonCodeField;
                }
                set
                {
                    this.claimReviewReasonCodeField = value;
                }
            }

            /// <remarks/>
            public string reviewReasonDescription
            {
                get
                {
                    return this.reviewReasonDescriptionField;
                }
                set
                {
                    this.reviewReasonDescriptionField = value;
                }
            }

            /// <remarks/>
            public string requestingUser
            {
                get
                {
                    return this.requestingUserField;
                }
                set
                {
                    this.requestingUserField = value;
                }
            }

            /// <remarks/>
            public string reviewServiceStartDate
            {
                get
                {
                    return this.reviewServiceStartDateField;
                }
                set
                {
                    this.reviewServiceStartDateField = value;
                }
            }

            /// <remarks/>
            public string reviewServiceEndDate
            {
                get
                {
                    return this.reviewServiceEndDateField;
                }
                set
                {
                    this.reviewServiceEndDateField = value;
                }
            }

            /// <remarks/>
            public string reviewReceiptStartDate
            {
                get
                {
                    return this.reviewReceiptStartDateField;
                }
                set
                {
                    this.reviewReceiptStartDateField = value;
                }
            }

            /// <remarks/>
            public string reviewReceiptEndDate
            {
                get
                {
                    return this.reviewReceiptEndDateField;
                }
                set
                {
                    this.reviewReceiptEndDateField = value;
                }
            }

            /// <remarks/>
            public subscriptionClaimReviewListClaimReviewReviewPractitionerList reviewPractitionerList
            {
                get
                {
                    return this.reviewPractitionerListField;
                }
                set
                {
                    this.reviewPractitionerListField = value;
                }
            }

            /// <remarks/>
            public subscriptionClaimReviewListClaimReviewReviewSupplierList reviewSupplierList
            {
                get
                {
                    return this.reviewSupplierListField;
                }
                set
                {
                    this.reviewSupplierListField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionClaimReviewListClaimReviewClaimReviewReasonCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionClaimReviewListClaimReviewReviewPractitionerList
        {

            private string otherThanField;

            private subscriptionClaimReviewListClaimReviewReviewPractitionerListPractitionerReference[] practitionerReferenceField;

            /// <remarks/>
            public string otherThan
            {
                get
                {
                    return this.otherThanField;
                }
                set
                {
                    this.otherThanField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("practitionerReference")]
            public subscriptionClaimReviewListClaimReviewReviewPractitionerListPractitionerReference[] practitionerReference
            {
                get
                {
                    return this.practitionerReferenceField;
                }
                set
                {
                    this.practitionerReferenceField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionClaimReviewListClaimReviewReviewPractitionerListPractitionerReference
        {

            private string practitionerIDField;

            private subscriptionClaimReviewListClaimReviewReviewPractitionerListPractitionerReferencePractitionerMatch practitionerMatchField;

            /// <remarks/>
            public string practitionerID
            {
                get
                {
                    return this.practitionerIDField;
                }
                set
                {
                    this.practitionerIDField = value;
                }
            }

            /// <remarks/>
            public subscriptionClaimReviewListClaimReviewReviewPractitionerListPractitionerReferencePractitionerMatch practitionerMatch
            {
                get
                {
                    return this.practitionerMatchField;
                }
                set
                {
                    this.practitionerMatchField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionClaimReviewListClaimReviewReviewPractitionerListPractitionerReferencePractitionerMatch
        {

            private string hccIdentificationNumberField;

            private string practitionerOtherIdentificationNumberField;

            private string firstNameField;

            private string lastNameField;

            private string asOfDateField;

            /// <remarks/>
            public string hccIdentificationNumber
            {
                get
                {
                    return this.hccIdentificationNumberField;
                }
                set
                {
                    this.hccIdentificationNumberField = value;
                }
            }

            /// <remarks/>
            public string practitionerOtherIdentificationNumber
            {
                get
                {
                    return this.practitionerOtherIdentificationNumberField;
                }
                set
                {
                    this.practitionerOtherIdentificationNumberField = value;
                }
            }

            /// <remarks/>
            public string firstName
            {
                get
                {
                    return this.firstNameField;
                }
                set
                {
                    this.firstNameField = value;
                }
            }

            /// <remarks/>
            public string lastName
            {
                get
                {
                    return this.lastNameField;
                }
                set
                {
                    this.lastNameField = value;
                }
            }

            /// <remarks/>
            public string asOfDate
            {
                get
                {
                    return this.asOfDateField;
                }
                set
                {
                    this.asOfDateField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionClaimReviewListClaimReviewReviewSupplierList
        {

            private string otherThanField;

            private subscriptionClaimReviewListClaimReviewReviewSupplierListSupplierReference[] supplierReferenceField;

            /// <remarks/>
            public string otherThan
            {
                get
                {
                    return this.otherThanField;
                }
                set
                {
                    this.otherThanField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("supplierReference")]
            public subscriptionClaimReviewListClaimReviewReviewSupplierListSupplierReference[] supplierReference
            {
                get
                {
                    return this.supplierReferenceField;
                }
                set
                {
                    this.supplierReferenceField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionClaimReviewListClaimReviewReviewSupplierListSupplierReference
        {

            private string supplierIDField;

            private subscriptionClaimReviewListClaimReviewReviewSupplierListSupplierReferenceSupplierMatch supplierMatchField;

            /// <remarks/>
            public string supplierID
            {
                get
                {
                    return this.supplierIDField;
                }
                set
                {
                    this.supplierIDField = value;
                }
            }

            /// <remarks/>
            public subscriptionClaimReviewListClaimReviewReviewSupplierListSupplierReferenceSupplierMatch supplierMatch
            {
                get
                {
                    return this.supplierMatchField;
                }
                set
                {
                    this.supplierMatchField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionClaimReviewListClaimReviewReviewSupplierListSupplierReferenceSupplierMatch
        {

            private string hccIdentificationNumberField;

            private string supplierOtherIdentificationNumberField;

            private string primaryNameField;

            private string asOfDateField;

            /// <remarks/>
            public string hccIdentificationNumber
            {
                get
                {
                    return this.hccIdentificationNumberField;
                }
                set
                {
                    this.hccIdentificationNumberField = value;
                }
            }

            /// <remarks/>
            public string supplierOtherIdentificationNumber
            {
                get
                {
                    return this.supplierOtherIdentificationNumberField;
                }
                set
                {
                    this.supplierOtherIdentificationNumberField = value;
                }
            }

            /// <remarks/>
            public string primaryName
            {
                get
                {
                    return this.primaryNameField;
                }
                set
                {
                    this.primaryNameField = value;
                }
            }

            /// <remarks/>
            public string asOfDate
            {
                get
                {
                    return this.asOfDateField;
                }
                set
                {
                    this.asOfDateField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionSubsidy
        {

            private string subscriptionHccIdField;

            private string otherBillRecipientHccIdField;

            private subscriptionSubsidyBenefitPlanTypeCode benefitPlanTypeCodeField;

            private subscriptionSubsidyBillingCategoryCode billingCategoryCodeField;

            private subscriptionSubsidyDateRanges dateRangesField;

            private string resourceURIField;

            private string versionField;

            /// <remarks/>
            public string subscriptionHccId
            {
                get
                {
                    return this.subscriptionHccIdField;
                }
                set
                {
                    this.subscriptionHccIdField = value;
                }
            }

            /// <remarks/>
            public string otherBillRecipientHccId
            {
                get
                {
                    return this.otherBillRecipientHccIdField;
                }
                set
                {
                    this.otherBillRecipientHccIdField = value;
                }
            }

            /// <remarks/>
            public subscriptionSubsidyBenefitPlanTypeCode benefitPlanTypeCode
            {
                get
                {
                    return this.benefitPlanTypeCodeField;
                }
                set
                {
                    this.benefitPlanTypeCodeField = value;
                }
            }

            /// <remarks/>
            public subscriptionSubsidyBillingCategoryCode billingCategoryCode
            {
                get
                {
                    return this.billingCategoryCodeField;
                }
                set
                {
                    this.billingCategoryCodeField = value;
                }
            }

            /// <remarks/>
            public subscriptionSubsidyDateRanges dateRanges
            {
                get
                {
                    return this.dateRangesField;
                }
                set
                {
                    this.dateRangesField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string resourceURI
            {
                get
                {
                    return this.resourceURIField;
                }
                set
                {
                    this.resourceURIField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string version
            {
                get
                {
                    return this.versionField;
                }
                set
                {
                    this.versionField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionSubsidyBenefitPlanTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionSubsidyBillingCategoryCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionSubsidyDateRanges
        {

            private string updateModeField;

            private subscriptionSubsidyDateRangesDateRange[] dateRangeField;

            /// <remarks/>
            public string updateMode
            {
                get
                {
                    return this.updateModeField;
                }
                set
                {
                    this.updateModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("dateRange")]
            public subscriptionSubsidyDateRangesDateRange[] dateRange
            {
                get
                {
                    return this.dateRangeField;
                }
                set
                {
                    this.dateRangeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionSubsidyDateRangesDateRange
        {

            private string startDateField;

            private string throughDateField;

            private string amountField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string throughDate
            {
                get
                {
                    return this.throughDateField;
                }
                set
                {
                    this.throughDateField = value;
                }
            }

            /// <remarks/>
            public string amount
            {
                get
                {
                    return this.amountField;
                }
                set
                {
                    this.amountField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionApplicationInfo
        {

            private string applicationIdField;

            private string ratingAreaField;

            private string paymentTransactionIdField;

            private string paymentDateField;

            /// <remarks/>
            public string applicationId
            {
                get
                {
                    return this.applicationIdField;
                }
                set
                {
                    this.applicationIdField = value;
                }
            }

            /// <remarks/>
            public string ratingArea
            {
                get
                {
                    return this.ratingAreaField;
                }
                set
                {
                    this.ratingAreaField = value;
                }
            }

            /// <remarks/>
            public string paymentTransactionId
            {
                get
                {
                    return this.paymentTransactionIdField;
                }
                set
                {
                    this.paymentTransactionIdField = value;
                }
            }

            /// <remarks/>
            public string paymentDate
            {
                get
                {
                    return this.paymentDateField;
                }
                set
                {
                    this.paymentDateField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionBrokerSelections
        {

            private string listModeField;

            private subscriptionBrokerSelectionsBrokerSelection[] brokerSelectionField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("brokerSelection")]
            public subscriptionBrokerSelectionsBrokerSelection[] brokerSelection
            {
                get
                {
                    return this.brokerSelectionField;
                }
                set
                {
                    this.brokerSelectionField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionBrokerSelectionsBrokerSelection
        {

            private subscriptionBrokerSelectionsBrokerSelectionBroker brokerField;

            private subscriptionBrokerSelectionsBrokerSelectionDateRanges[] dateRangesField;

            /// <remarks/>
            public subscriptionBrokerSelectionsBrokerSelectionBroker broker
            {
                get
                {
                    return this.brokerField;
                }
                set
                {
                    this.brokerField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("dateRanges")]
            public subscriptionBrokerSelectionsBrokerSelectionDateRanges[] dateRanges
            {
                get
                {
                    return this.dateRangesField;
                }
                set
                {
                    this.dateRangesField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionBrokerSelectionsBrokerSelectionBroker
        {

            private string brokerIdentifierField;

            private string brokerHccIdField;

            /// <remarks/>
            public string brokerIdentifier
            {
                get
                {
                    return this.brokerIdentifierField;
                }
                set
                {
                    this.brokerIdentifierField = value;
                }
            }

            /// <remarks/>
            public string brokerHccId
            {
                get
                {
                    return this.brokerHccIdField;
                }
                set
                {
                    this.brokerHccIdField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionBrokerSelectionsBrokerSelectionDateRanges
        {

            private string startDateField;

            private string endDateField;

            private subscriptionBrokerSelectionsBrokerSelectionDateRangesBrokerAgents[] brokerAgentsField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("brokerAgents")]
            public subscriptionBrokerSelectionsBrokerSelectionDateRangesBrokerAgents[] brokerAgents
            {
                get
                {
                    return this.brokerAgentsField;
                }
                set
                {
                    this.brokerAgentsField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionBrokerSelectionsBrokerSelectionDateRangesBrokerAgents
        {

            private string brokerAgentIdentifierField;

            private string brokerAgentHccIdField;

            /// <remarks/>
            public string brokerAgentIdentifier
            {
                get
                {
                    return this.brokerAgentIdentifierField;
                }
                set
                {
                    this.brokerAgentIdentifierField = value;
                }
            }

            /// <remarks/>
            public string brokerAgentHccId
            {
                get
                {
                    return this.brokerAgentHccIdField;
                }
                set
                {
                    this.brokerAgentHccIdField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionPaymentType
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionPayeeBankAccount
        {

            private string instanceActiveCodeField;

            private string bankAccountIdField;

            private string oldBankAccountIdField;

            private string endorsementEffectiveDateField;

            private string endorsementExpirationDateField;

            private subscriptionPayeeBankAccountPayee payeeField;

            private subscriptionPayeeBankAccountPayeeBankAccountDateRange payeeBankAccountDateRangeField;

            private subscriptionPayeeBankAccountPayeeBankAccountDateRange1[] payeeBankAccountDateRangesField;

            private string maintenanceCommentField;

            private subscriptionPayeeBankAccountMaintenanceReasonCode maintenanceReasonCodeField;

            private string resourceURIField;

            private string versionField;

            /// <remarks/>
            public string instanceActiveCode
            {
                get
                {
                    return this.instanceActiveCodeField;
                }
                set
                {
                    this.instanceActiveCodeField = value;
                }
            }

            /// <remarks/>
            public string bankAccountId
            {
                get
                {
                    return this.bankAccountIdField;
                }
                set
                {
                    this.bankAccountIdField = value;
                }
            }

            /// <remarks/>
            public string oldBankAccountId
            {
                get
                {
                    return this.oldBankAccountIdField;
                }
                set
                {
                    this.oldBankAccountIdField = value;
                }
            }

            /// <remarks/>
            public string endorsementEffectiveDate
            {
                get
                {
                    return this.endorsementEffectiveDateField;
                }
                set
                {
                    this.endorsementEffectiveDateField = value;
                }
            }

            /// <remarks/>
            public string endorsementExpirationDate
            {
                get
                {
                    return this.endorsementExpirationDateField;
                }
                set
                {
                    this.endorsementExpirationDateField = value;
                }
            }

            /// <remarks/>
            public subscriptionPayeeBankAccountPayee payee
            {
                get
                {
                    return this.payeeField;
                }
                set
                {
                    this.payeeField = value;
                }
            }

            /// <remarks/>
            public subscriptionPayeeBankAccountPayeeBankAccountDateRange payeeBankAccountDateRange
            {
                get
                {
                    return this.payeeBankAccountDateRangeField;
                }
                set
                {
                    this.payeeBankAccountDateRangeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlArrayItemAttribute("payeeBankAccountDateRange", IsNullable = false)]
            public subscriptionPayeeBankAccountPayeeBankAccountDateRange1[] payeeBankAccountDateRanges
            {
                get
                {
                    return this.payeeBankAccountDateRangesField;
                }
                set
                {
                    this.payeeBankAccountDateRangesField = value;
                }
            }

            /// <remarks/>
            public string maintenanceComment
            {
                get
                {
                    return this.maintenanceCommentField;
                }
                set
                {
                    this.maintenanceCommentField = value;
                }
            }

            /// <remarks/>
            public subscriptionPayeeBankAccountMaintenanceReasonCode maintenanceReasonCode
            {
                get
                {
                    return this.maintenanceReasonCodeField;
                }
                set
                {
                    this.maintenanceReasonCodeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string resourceURI
            {
                get
                {
                    return this.resourceURIField;
                }
                set
                {
                    this.resourceURIField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string version
            {
                get
                {
                    return this.versionField;
                }
                set
                {
                    this.versionField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionPayeeBankAccountPayee
        {

            private string payeeSupplierHccIdField;

            private string payeeSupplierLocationHccIdField;

            private string payeeBrokerHccIdField;

            private string payeeSubscriptionHccIdField;

            private string resourceURIField;

            private string versionField;

            /// <remarks/>
            public string payeeSupplierHccId
            {
                get
                {
                    return this.payeeSupplierHccIdField;
                }
                set
                {
                    this.payeeSupplierHccIdField = value;
                }
            }

            /// <remarks/>
            public string payeeSupplierLocationHccId
            {
                get
                {
                    return this.payeeSupplierLocationHccIdField;
                }
                set
                {
                    this.payeeSupplierLocationHccIdField = value;
                }
            }

            /// <remarks/>
            public string payeeBrokerHccId
            {
                get
                {
                    return this.payeeBrokerHccIdField;
                }
                set
                {
                    this.payeeBrokerHccIdField = value;
                }
            }

            /// <remarks/>
            public string payeeSubscriptionHccId
            {
                get
                {
                    return this.payeeSubscriptionHccIdField;
                }
                set
                {
                    this.payeeSubscriptionHccIdField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string resourceURI
            {
                get
                {
                    return this.resourceURIField;
                }
                set
                {
                    this.resourceURIField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string version
            {
                get
                {
                    return this.versionField;
                }
                set
                {
                    this.versionField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionPayeeBankAccountPayeeBankAccountDateRange
        {

            private string startDateField;

            private string endDateField;

            private string routingNumberField;

            private string accountNumberField;

            private string bankNameField;

            private subscriptionPayeeBankAccountPayeeBankAccountDateRangeCountryCode countryCodeField;

            private string accountOwnerField;

            private subscriptionPayeeBankAccountPayeeBankAccountDateRangeBankAccountType bankAccountTypeField;

            private subscriptionPayeeBankAccountPayeeBankAccountDateRangeExternalUsageType externalUsageTypeField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }

            /// <remarks/>
            public string routingNumber
            {
                get
                {
                    return this.routingNumberField;
                }
                set
                {
                    this.routingNumberField = value;
                }
            }

            /// <remarks/>
            public string accountNumber
            {
                get
                {
                    return this.accountNumberField;
                }
                set
                {
                    this.accountNumberField = value;
                }
            }

            /// <remarks/>
            public string bankName
            {
                get
                {
                    return this.bankNameField;
                }
                set
                {
                    this.bankNameField = value;
                }
            }

            /// <remarks/>
            public subscriptionPayeeBankAccountPayeeBankAccountDateRangeCountryCode countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }

            /// <remarks/>
            public string accountOwner
            {
                get
                {
                    return this.accountOwnerField;
                }
                set
                {
                    this.accountOwnerField = value;
                }
            }

            /// <remarks/>
            public subscriptionPayeeBankAccountPayeeBankAccountDateRangeBankAccountType bankAccountType
            {
                get
                {
                    return this.bankAccountTypeField;
                }
                set
                {
                    this.bankAccountTypeField = value;
                }
            }

            /// <remarks/>
            public subscriptionPayeeBankAccountPayeeBankAccountDateRangeExternalUsageType externalUsageType
            {
                get
                {
                    return this.externalUsageTypeField;
                }
                set
                {
                    this.externalUsageTypeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionPayeeBankAccountPayeeBankAccountDateRangeCountryCode
        {

            private string countryCodeField;

            /// <remarks/>
            public string countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionPayeeBankAccountPayeeBankAccountDateRangeBankAccountType
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionPayeeBankAccountPayeeBankAccountDateRangeExternalUsageType
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionPayeeBankAccountPayeeBankAccountDateRange1
        {

            private string startDateField;

            private string endDateField;

            private string routingNumberField;

            private string accountNumberField;

            private string bankNameField;

            private subscriptionPayeeBankAccountPayeeBankAccountDateRangeCountryCode1 countryCodeField;

            private string accountOwnerField;

            private subscriptionPayeeBankAccountPayeeBankAccountDateRangeBankAccountType1 bankAccountTypeField;

            private subscriptionPayeeBankAccountPayeeBankAccountDateRangeExternalUsageType1 externalUsageTypeField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }

            /// <remarks/>
            public string routingNumber
            {
                get
                {
                    return this.routingNumberField;
                }
                set
                {
                    this.routingNumberField = value;
                }
            }

            /// <remarks/>
            public string accountNumber
            {
                get
                {
                    return this.accountNumberField;
                }
                set
                {
                    this.accountNumberField = value;
                }
            }

            /// <remarks/>
            public string bankName
            {
                get
                {
                    return this.bankNameField;
                }
                set
                {
                    this.bankNameField = value;
                }
            }

            /// <remarks/>
            public subscriptionPayeeBankAccountPayeeBankAccountDateRangeCountryCode1 countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }

            /// <remarks/>
            public string accountOwner
            {
                get
                {
                    return this.accountOwnerField;
                }
                set
                {
                    this.accountOwnerField = value;
                }
            }

            /// <remarks/>
            public subscriptionPayeeBankAccountPayeeBankAccountDateRangeBankAccountType1 bankAccountType
            {
                get
                {
                    return this.bankAccountTypeField;
                }
                set
                {
                    this.bankAccountTypeField = value;
                }
            }

            /// <remarks/>
            public subscriptionPayeeBankAccountPayeeBankAccountDateRangeExternalUsageType1 externalUsageType
            {
                get
                {
                    return this.externalUsageTypeField;
                }
                set
                {
                    this.externalUsageTypeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionPayeeBankAccountPayeeBankAccountDateRangeCountryCode1
        {

            private string countryCodeField;

            /// <remarks/>
            public string countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionPayeeBankAccountPayeeBankAccountDateRangeBankAccountType1
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionPayeeBankAccountPayeeBankAccountDateRangeExternalUsageType1
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class subscriptionPayeeBankAccountMaintenanceReasonCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class member
        {

            private string maintenanceTypeCodeField;

            private memberMaintenanceReasonCode maintenanceReasonCodeField;

            private string maintenanceCommentField;

            private bool maintenanceOverrideField;

            private string maintenanceOverrideResultField;

            private string benefitStatusCodeField;

            private string membershipIdentifierField;

            private bool memberIsSubscriberField;

            private string hccIdentifierField;

            private string memberInputIDField;

            private string receiptDateField;

            private bool outOfServiceAreaField;

            private string smokingStatusField;

            private string isHandicappedField;

            private string waivePHIClaimViewRestrictionField;

            private string payeeHccIdField;

            private bool isMemberInHospiceField;

            private memberMemberMatchData memberMatchDataField;

            private memberOldMemberData oldMemberDataField;

            private memberIndividual individualField;

            private memberPhysicalAddress physicalAddressField;

            private memberCorrespondenceAddress correspondenceAddressField;

            private memberCommunicationPreferences communicationPreferencesField;

            private memberSpecificCommunicationPreferences[] specificCommunicationPreferencesField;

            private memberOtherIdNumberList otherIdNumberListField;

            private memberDisabilityInfoForEligibility disabilityInfoForEligibilityField;

            private memberDependentVerification dependentVerificationField;

            private memberPhysicalCharacteristics physicalCharacteristicsField;

            private memberEmploymentInfo employmentInfoField;

            private memberDentalInfo dentalInfoField;

            private memberPriorCoverageInfo priorCoverageInfoField;

            private memberPreExistingConditionInfo preExistingConditionInfoField;

            private memberPreExReduction preExReductionField;

            private memberOtherResponsiblePersonInfo otherResponsiblePersonInfoField;

            private memberMemberRepresentative memberRepresentativeField;

            private memberMembershipUDTList membershipUDTListField;

            private memberInformationAccessPermissionList informationAccessPermissionListField;

            private memberInformationAccessRestrictionList informationAccessRestrictionListField;

            private memberPhiAuthorizationList phiAuthorizationListField;

            private memberCcDirectiveList ccDirectiveListField;

            private memberSubscriptionReference subscriptionReferenceField;

            private memberIndividualReference individualReferenceField;

            private memberRelationshipToSubscriberDefinitionReference relationshipToSubscriberDefinitionReferenceField;

            private memberRaceOrEthnicity raceOrEthnicityField;

            private memberVipReasonCode vipReasonCodeField;

            private memberHealthStatusReasonCode healthStatusReasonCodeField;

            private memberInformationSourceCode informationSourceCodeField;

            private memberAttachmentSetReference attachmentSetReferenceField;

            private string planSelectionListModeField;

            private memberPlanSelection[] planSelectionField;

            private memberProviderSelections providerSelectionsField;

            private memberReportingCategory[] reportingCategoryField;

            private string medicareHICNlistModeField;

            private memberMedicareHICN[] medicareHICNField;

            private memberCobPolicy[] cobPolicyField;

            private memberHeadOfHousehold headOfHouseholdField;

            private memberMemberComplianceProgram memberComplianceProgramField;

            private memberEnrollmentMemberFactors enrollmentMemberFactorsField;

            private memberAlternatePaymentContact alternatePaymentContactField;

            private string resourceURIField;

            private string versionField;

            /// <remarks/>
            public string maintenanceTypeCode
            {
                get
                {
                    return this.maintenanceTypeCodeField;
                }
                set
                {
                    this.maintenanceTypeCodeField = value;
                }
            }

            /// <remarks/>
            public memberMaintenanceReasonCode maintenanceReasonCode
            {
                get
                {
                    return this.maintenanceReasonCodeField;
                }
                set
                {
                    this.maintenanceReasonCodeField = value;
                }
            }

            /// <remarks/>
            public string maintenanceComment
            {
                get
                {
                    return this.maintenanceCommentField;
                }
                set
                {
                    this.maintenanceCommentField = value;
                }
            }

            /// <remarks/>
            public bool maintenanceOverride
            {
                get
                {
                    return this.maintenanceOverrideField;
                }
                set
                {
                    this.maintenanceOverrideField = value;
                }
            }

            /// <remarks/>
            public string maintenanceOverrideResult
            {
                get
                {
                    return this.maintenanceOverrideResultField;
                }
                set
                {
                    this.maintenanceOverrideResultField = value;
                }
            }

            /// <remarks/>
            public string benefitStatusCode
            {
                get
                {
                    return this.benefitStatusCodeField;
                }
                set
                {
                    this.benefitStatusCodeField = value;
                }
            }

            /// <remarks/>
            public string membershipIdentifier
            {
                get
                {
                    return this.membershipIdentifierField;
                }
                set
                {
                    this.membershipIdentifierField = value;
                }
            }

            /// <remarks/>
            public bool memberIsSubscriber
            {
                get
                {
                    return this.memberIsSubscriberField;
                }
                set
                {
                    this.memberIsSubscriberField = value;
                }
            }

            /// <remarks/>
            public string hccIdentifier
            {
                get
                {
                    return this.hccIdentifierField;
                }
                set
                {
                    this.hccIdentifierField = value;
                }
            }

            /// <remarks/>
            public string memberInputID
            {
                get
                {
                    return this.memberInputIDField;
                }
                set
                {
                    this.memberInputIDField = value;
                }
            }

            /// <remarks/>
            public string receiptDate
            {
                get
                {
                    return this.receiptDateField;
                }
                set
                {
                    this.receiptDateField = value;
                }
            }

            /// <remarks/>
            public bool outOfServiceArea
            {
                get
                {
                    return this.outOfServiceAreaField;
                }
                set
                {
                    this.outOfServiceAreaField = value;
                }
            }

            /// <remarks/>
            public string smokingStatus
            {
                get
                {
                    return this.smokingStatusField;
                }
                set
                {
                    this.smokingStatusField = value;
                }
            }

            /// <remarks/>
            public string isHandicapped
            {
                get
                {
                    return this.isHandicappedField;
                }
                set
                {
                    this.isHandicappedField = value;
                }
            }

            /// <remarks/>
            public string waivePHIClaimViewRestriction
            {
                get
                {
                    return this.waivePHIClaimViewRestrictionField;
                }
                set
                {
                    this.waivePHIClaimViewRestrictionField = value;
                }
            }

            /// <remarks/>
            public string payeeHccId
            {
                get
                {
                    return this.payeeHccIdField;
                }
                set
                {
                    this.payeeHccIdField = value;
                }
            }

            /// <remarks/>
            public bool isMemberInHospice
            {
                get
                {
                    return this.isMemberInHospiceField;
                }
                set
                {
                    this.isMemberInHospiceField = value;
                }
            }

            /// <remarks/>
            public memberMemberMatchData memberMatchData
            {
                get
                {
                    return this.memberMatchDataField;
                }
                set
                {
                    this.memberMatchDataField = value;
                }
            }

            /// <remarks/>
            public memberOldMemberData oldMemberData
            {
                get
                {
                    return this.oldMemberDataField;
                }
                set
                {
                    this.oldMemberDataField = value;
                }
            }

            /// <remarks/>
            public memberIndividual individual
            {
                get
                {
                    return this.individualField;
                }
                set
                {
                    this.individualField = value;
                }
            }

            /// <remarks/>
            public memberPhysicalAddress physicalAddress
            {
                get
                {
                    return this.physicalAddressField;
                }
                set
                {
                    this.physicalAddressField = value;
                }
            }

            /// <remarks/>
            public memberCorrespondenceAddress correspondenceAddress
            {
                get
                {
                    return this.correspondenceAddressField;
                }
                set
                {
                    this.correspondenceAddressField = value;
                }
            }

            /// <remarks/>
            public memberCommunicationPreferences communicationPreferences
            {
                get
                {
                    return this.communicationPreferencesField;
                }
                set
                {
                    this.communicationPreferencesField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("specificCommunicationPreferences")]
            public memberSpecificCommunicationPreferences[] specificCommunicationPreferences
            {
                get
                {
                    return this.specificCommunicationPreferencesField;
                }
                set
                {
                    this.specificCommunicationPreferencesField = value;
                }
            }

            /// <remarks/>
            public memberOtherIdNumberList otherIdNumberList
            {
                get
                {
                    return this.otherIdNumberListField;
                }
                set
                {
                    this.otherIdNumberListField = value;
                }
            }

            /// <remarks/>
            public memberDisabilityInfoForEligibility disabilityInfoForEligibility
            {
                get
                {
                    return this.disabilityInfoForEligibilityField;
                }
                set
                {
                    this.disabilityInfoForEligibilityField = value;
                }
            }

            /// <remarks/>
            public memberDependentVerification dependentVerification
            {
                get
                {
                    return this.dependentVerificationField;
                }
                set
                {
                    this.dependentVerificationField = value;
                }
            }

            /// <remarks/>
            public memberPhysicalCharacteristics physicalCharacteristics
            {
                get
                {
                    return this.physicalCharacteristicsField;
                }
                set
                {
                    this.physicalCharacteristicsField = value;
                }
            }

            /// <remarks/>
            public memberEmploymentInfo employmentInfo
            {
                get
                {
                    return this.employmentInfoField;
                }
                set
                {
                    this.employmentInfoField = value;
                }
            }

            /// <remarks/>
            public memberDentalInfo dentalInfo
            {
                get
                {
                    return this.dentalInfoField;
                }
                set
                {
                    this.dentalInfoField = value;
                }
            }

            /// <remarks/>
            public memberPriorCoverageInfo priorCoverageInfo
            {
                get
                {
                    return this.priorCoverageInfoField;
                }
                set
                {
                    this.priorCoverageInfoField = value;
                }
            }

            /// <remarks/>
            public memberPreExistingConditionInfo preExistingConditionInfo
            {
                get
                {
                    return this.preExistingConditionInfoField;
                }
                set
                {
                    this.preExistingConditionInfoField = value;
                }
            }

            /// <remarks/>
            public memberPreExReduction preExReduction
            {
                get
                {
                    return this.preExReductionField;
                }
                set
                {
                    this.preExReductionField = value;
                }
            }

            /// <remarks/>
            public memberOtherResponsiblePersonInfo otherResponsiblePersonInfo
            {
                get
                {
                    return this.otherResponsiblePersonInfoField;
                }
                set
                {
                    this.otherResponsiblePersonInfoField = value;
                }
            }

            /// <remarks/>
            public memberMemberRepresentative memberRepresentative
            {
                get
                {
                    return this.memberRepresentativeField;
                }
                set
                {
                    this.memberRepresentativeField = value;
                }
            }

            /// <remarks/>
            public memberMembershipUDTList membershipUDTList
            {
                get
                {
                    return this.membershipUDTListField;
                }
                set
                {
                    this.membershipUDTListField = value;
                }
            }

            /// <remarks/>
            public memberInformationAccessPermissionList informationAccessPermissionList
            {
                get
                {
                    return this.informationAccessPermissionListField;
                }
                set
                {
                    this.informationAccessPermissionListField = value;
                }
            }

            /// <remarks/>
            public memberInformationAccessRestrictionList informationAccessRestrictionList
            {
                get
                {
                    return this.informationAccessRestrictionListField;
                }
                set
                {
                    this.informationAccessRestrictionListField = value;
                }
            }

            /// <remarks/>
            public memberPhiAuthorizationList phiAuthorizationList
            {
                get
                {
                    return this.phiAuthorizationListField;
                }
                set
                {
                    this.phiAuthorizationListField = value;
                }
            }

            /// <remarks/>
            public memberCcDirectiveList ccDirectiveList
            {
                get
                {
                    return this.ccDirectiveListField;
                }
                set
                {
                    this.ccDirectiveListField = value;
                }
            }

            /// <remarks/>
            public memberSubscriptionReference subscriptionReference
            {
                get
                {
                    return this.subscriptionReferenceField;
                }
                set
                {
                    this.subscriptionReferenceField = value;
                }
            }

            /// <remarks/>
            public memberIndividualReference individualReference
            {
                get
                {
                    return this.individualReferenceField;
                }
                set
                {
                    this.individualReferenceField = value;
                }
            }

            /// <remarks/>
            public memberRelationshipToSubscriberDefinitionReference relationshipToSubscriberDefinitionReference
            {
                get
                {
                    return this.relationshipToSubscriberDefinitionReferenceField;
                }
                set
                {
                    this.relationshipToSubscriberDefinitionReferenceField = value;
                }
            }

            /// <remarks/>
            public memberRaceOrEthnicity raceOrEthnicity
            {
                get
                {
                    return this.raceOrEthnicityField;
                }
                set
                {
                    this.raceOrEthnicityField = value;
                }
            }

            /// <remarks/>
            public memberVipReasonCode vipReasonCode
            {
                get
                {
                    return this.vipReasonCodeField;
                }
                set
                {
                    this.vipReasonCodeField = value;
                }
            }

            /// <remarks/>
            public memberHealthStatusReasonCode healthStatusReasonCode
            {
                get
                {
                    return this.healthStatusReasonCodeField;
                }
                set
                {
                    this.healthStatusReasonCodeField = value;
                }
            }

            /// <remarks/>
            public memberInformationSourceCode informationSourceCode
            {
                get
                {
                    return this.informationSourceCodeField;
                }
                set
                {
                    this.informationSourceCodeField = value;
                }
            }

            /// <remarks/>
            public memberAttachmentSetReference attachmentSetReference
            {
                get
                {
                    return this.attachmentSetReferenceField;
                }
                set
                {
                    this.attachmentSetReferenceField = value;
                }
            }

            /// <remarks/>
            public string planSelectionListMode
            {
                get
                {
                    return this.planSelectionListModeField;
                }
                set
                {
                    this.planSelectionListModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("planSelection")]
            public memberPlanSelection[] planSelection
            {
                get
                {
                    return this.planSelectionField;
                }
                set
                {
                    this.planSelectionField = value;
                }
            }

            /// <remarks/>
            public memberProviderSelections providerSelections
            {
                get
                {
                    return this.providerSelectionsField;
                }
                set
                {
                    this.providerSelectionsField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("reportingCategory")]
            public memberReportingCategory[] reportingCategory
            {
                get
                {
                    return this.reportingCategoryField;
                }
                set
                {
                    this.reportingCategoryField = value;
                }
            }

            /// <remarks/>
            public string medicareHICNlistMode
            {
                get
                {
                    return this.medicareHICNlistModeField;
                }
                set
                {
                    this.medicareHICNlistModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("medicareHICN")]
            public memberMedicareHICN[] medicareHICN
            {
                get
                {
                    return this.medicareHICNField;
                }
                set
                {
                    this.medicareHICNField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("cobPolicy")]
            public memberCobPolicy[] cobPolicy
            {
                get
                {
                    return this.cobPolicyField;
                }
                set
                {
                    this.cobPolicyField = value;
                }
            }

            /// <remarks/>
            public memberHeadOfHousehold headOfHousehold
            {
                get
                {
                    return this.headOfHouseholdField;
                }
                set
                {
                    this.headOfHouseholdField = value;
                }
            }

            /// <remarks/>
            public memberMemberComplianceProgram memberComplianceProgram
            {
                get
                {
                    return this.memberComplianceProgramField;
                }
                set
                {
                    this.memberComplianceProgramField = value;
                }
            }

            /// <remarks/>
            public memberEnrollmentMemberFactors enrollmentMemberFactors
            {
                get
                {
                    return this.enrollmentMemberFactorsField;
                }
                set
                {
                    this.enrollmentMemberFactorsField = value;
                }
            }

            /// <remarks/>
            public memberAlternatePaymentContact alternatePaymentContact
            {
                get
                {
                    return this.alternatePaymentContactField;
                }
                set
                {
                    this.alternatePaymentContactField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string resourceURI
            {
                get
                {
                    return this.resourceURIField;
                }
                set
                {
                    this.resourceURIField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string version
            {
                get
                {
                    return this.versionField;
                }
                set
                {
                    this.versionField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMaintenanceReasonCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMemberMatchData
        {

            private string definitionNameField;

            private string idField;

            private string topAccountField;

            private string customString1Field;

            private string customString2Field;

            private string customString3Field;

            private string customString4Field;

            private string customInteger1Field;

            private string customInteger2Field;

            private string customInteger3Field;

            private string customInteger4Field;

            private string customDate1Field;

            private string customDate2Field;

            private string customDate3Field;

            private string customDate4Field;

            private string customDecimal1Field;

            private string customDecimal2Field;

            private string customDecimal3Field;

            private string customDecimal4Field;

            private memberMemberMatchDataMember memberField;

            private memberMemberMatchDataSubscriber subscriberField;

            private string asOfDateField;

            /// <remarks/>
            public string definitionName
            {
                get
                {
                    return this.definitionNameField;
                }
                set
                {
                    this.definitionNameField = value;
                }
            }

            /// <remarks/>
            public string id
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }

            /// <remarks/>
            public string topAccount
            {
                get
                {
                    return this.topAccountField;
                }
                set
                {
                    this.topAccountField = value;
                }
            }

            /// <remarks/>
            public string customString1
            {
                get
                {
                    return this.customString1Field;
                }
                set
                {
                    this.customString1Field = value;
                }
            }

            /// <remarks/>
            public string customString2
            {
                get
                {
                    return this.customString2Field;
                }
                set
                {
                    this.customString2Field = value;
                }
            }

            /// <remarks/>
            public string customString3
            {
                get
                {
                    return this.customString3Field;
                }
                set
                {
                    this.customString3Field = value;
                }
            }

            /// <remarks/>
            public string customString4
            {
                get
                {
                    return this.customString4Field;
                }
                set
                {
                    this.customString4Field = value;
                }
            }

            /// <remarks/>
            public string customInteger1
            {
                get
                {
                    return this.customInteger1Field;
                }
                set
                {
                    this.customInteger1Field = value;
                }
            }

            /// <remarks/>
            public string customInteger2
            {
                get
                {
                    return this.customInteger2Field;
                }
                set
                {
                    this.customInteger2Field = value;
                }
            }

            /// <remarks/>
            public string customInteger3
            {
                get
                {
                    return this.customInteger3Field;
                }
                set
                {
                    this.customInteger3Field = value;
                }
            }

            /// <remarks/>
            public string customInteger4
            {
                get
                {
                    return this.customInteger4Field;
                }
                set
                {
                    this.customInteger4Field = value;
                }
            }

            /// <remarks/>
            public string customDate1
            {
                get
                {
                    return this.customDate1Field;
                }
                set
                {
                    this.customDate1Field = value;
                }
            }

            /// <remarks/>
            public string customDate2
            {
                get
                {
                    return this.customDate2Field;
                }
                set
                {
                    this.customDate2Field = value;
                }
            }

            /// <remarks/>
            public string customDate3
            {
                get
                {
                    return this.customDate3Field;
                }
                set
                {
                    this.customDate3Field = value;
                }
            }

            /// <remarks/>
            public string customDate4
            {
                get
                {
                    return this.customDate4Field;
                }
                set
                {
                    this.customDate4Field = value;
                }
            }

            /// <remarks/>
            public string customDecimal1
            {
                get
                {
                    return this.customDecimal1Field;
                }
                set
                {
                    this.customDecimal1Field = value;
                }
            }

            /// <remarks/>
            public string customDecimal2
            {
                get
                {
                    return this.customDecimal2Field;
                }
                set
                {
                    this.customDecimal2Field = value;
                }
            }

            /// <remarks/>
            public string customDecimal3
            {
                get
                {
                    return this.customDecimal3Field;
                }
                set
                {
                    this.customDecimal3Field = value;
                }
            }

            /// <remarks/>
            public string customDecimal4
            {
                get
                {
                    return this.customDecimal4Field;
                }
                set
                {
                    this.customDecimal4Field = value;
                }
            }

            /// <remarks/>
            public memberMemberMatchDataMember member
            {
                get
                {
                    return this.memberField;
                }
                set
                {
                    this.memberField = value;
                }
            }

            /// <remarks/>
            public memberMemberMatchDataSubscriber subscriber
            {
                get
                {
                    return this.subscriberField;
                }
                set
                {
                    this.subscriberField = value;
                }
            }

            /// <remarks/>
            public string asOfDate
            {
                get
                {
                    return this.asOfDateField;
                }
                set
                {
                    this.asOfDateField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMemberMatchDataMember
        {

            private string firstNameField;

            private string lastNameField;

            private string fullNameField;

            private string genderField;

            private string dateOfBirthField;

            private string taxIdField;

            private memberMemberMatchDataMemberAddress addressField;

            /// <remarks/>
            public string firstName
            {
                get
                {
                    return this.firstNameField;
                }
                set
                {
                    this.firstNameField = value;
                }
            }

            /// <remarks/>
            public string lastName
            {
                get
                {
                    return this.lastNameField;
                }
                set
                {
                    this.lastNameField = value;
                }
            }

            /// <remarks/>
            public string fullName
            {
                get
                {
                    return this.fullNameField;
                }
                set
                {
                    this.fullNameField = value;
                }
            }

            /// <remarks/>
            public string gender
            {
                get
                {
                    return this.genderField;
                }
                set
                {
                    this.genderField = value;
                }
            }

            /// <remarks/>
            public string dateOfBirth
            {
                get
                {
                    return this.dateOfBirthField;
                }
                set
                {
                    this.dateOfBirthField = value;
                }
            }

            /// <remarks/>
            public string taxId
            {
                get
                {
                    return this.taxIdField;
                }
                set
                {
                    this.taxIdField = value;
                }
            }

            /// <remarks/>
            public memberMemberMatchDataMemberAddress address
            {
                get
                {
                    return this.addressField;
                }
                set
                {
                    this.addressField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMemberMatchDataMemberAddress
        {

            private string addressField;

            private string cityNameField;

            private string stateCodeField;

            private string zipCodeField;

            private string zipExtensionCodeField;

            private string countryCodeField;

            /// <remarks/>
            public string address
            {
                get
                {
                    return this.addressField;
                }
                set
                {
                    this.addressField = value;
                }
            }

            /// <remarks/>
            public string cityName
            {
                get
                {
                    return this.cityNameField;
                }
                set
                {
                    this.cityNameField = value;
                }
            }

            /// <remarks/>
            public string stateCode
            {
                get
                {
                    return this.stateCodeField;
                }
                set
                {
                    this.stateCodeField = value;
                }
            }

            /// <remarks/>
            public string zipCode
            {
                get
                {
                    return this.zipCodeField;
                }
                set
                {
                    this.zipCodeField = value;
                }
            }

            /// <remarks/>
            public string zipExtensionCode
            {
                get
                {
                    return this.zipExtensionCodeField;
                }
                set
                {
                    this.zipExtensionCodeField = value;
                }
            }

            /// <remarks/>
            public string countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMemberMatchDataSubscriber
        {

            private string firstNameField;

            private string lastNameField;

            private string fullNameField;

            private string genderField;

            private string dateOfBirthField;

            private string taxIdField;

            private memberMemberMatchDataSubscriberAddress addressField;

            /// <remarks/>
            public string firstName
            {
                get
                {
                    return this.firstNameField;
                }
                set
                {
                    this.firstNameField = value;
                }
            }

            /// <remarks/>
            public string lastName
            {
                get
                {
                    return this.lastNameField;
                }
                set
                {
                    this.lastNameField = value;
                }
            }

            /// <remarks/>
            public string fullName
            {
                get
                {
                    return this.fullNameField;
                }
                set
                {
                    this.fullNameField = value;
                }
            }

            /// <remarks/>
            public string gender
            {
                get
                {
                    return this.genderField;
                }
                set
                {
                    this.genderField = value;
                }
            }

            /// <remarks/>
            public string dateOfBirth
            {
                get
                {
                    return this.dateOfBirthField;
                }
                set
                {
                    this.dateOfBirthField = value;
                }
            }

            /// <remarks/>
            public string taxId
            {
                get
                {
                    return this.taxIdField;
                }
                set
                {
                    this.taxIdField = value;
                }
            }

            /// <remarks/>
            public memberMemberMatchDataSubscriberAddress address
            {
                get
                {
                    return this.addressField;
                }
                set
                {
                    this.addressField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMemberMatchDataSubscriberAddress
        {

            private string addressField;

            private string cityNameField;

            private string stateCodeField;

            private string zipCodeField;

            private string zipExtensionCodeField;

            private string countryCodeField;

            /// <remarks/>
            public string address
            {
                get
                {
                    return this.addressField;
                }
                set
                {
                    this.addressField = value;
                }
            }

            /// <remarks/>
            public string cityName
            {
                get
                {
                    return this.cityNameField;
                }
                set
                {
                    this.cityNameField = value;
                }
            }

            /// <remarks/>
            public string stateCode
            {
                get
                {
                    return this.stateCodeField;
                }
                set
                {
                    this.stateCodeField = value;
                }
            }

            /// <remarks/>
            public string zipCode
            {
                get
                {
                    return this.zipCodeField;
                }
                set
                {
                    this.zipCodeField = value;
                }
            }

            /// <remarks/>
            public string zipExtensionCode
            {
                get
                {
                    return this.zipExtensionCodeField;
                }
                set
                {
                    this.zipExtensionCodeField = value;
                }
            }

            /// <remarks/>
            public string countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberOldMemberData
        {

            private memberOldMemberDataPrimaryName primaryNameField;

            private string hccIdentifierField;

            private string taxIdentificationNumberField;

            private string genderCodeField;

            private string maritalStatusCodeField;

            private string birthDateField;

            private memberOldMemberDataImmigrationStatusCode immigrationStatusCodeField;

            private memberOldMemberDataAddress addressField;

            /// <remarks/>
            public memberOldMemberDataPrimaryName primaryName
            {
                get
                {
                    return this.primaryNameField;
                }
                set
                {
                    this.primaryNameField = value;
                }
            }

            /// <remarks/>
            public string hccIdentifier
            {
                get
                {
                    return this.hccIdentifierField;
                }
                set
                {
                    this.hccIdentifierField = value;
                }
            }

            /// <remarks/>
            public string taxIdentificationNumber
            {
                get
                {
                    return this.taxIdentificationNumberField;
                }
                set
                {
                    this.taxIdentificationNumberField = value;
                }
            }

            /// <remarks/>
            public string genderCode
            {
                get
                {
                    return this.genderCodeField;
                }
                set
                {
                    this.genderCodeField = value;
                }
            }

            /// <remarks/>
            public string maritalStatusCode
            {
                get
                {
                    return this.maritalStatusCodeField;
                }
                set
                {
                    this.maritalStatusCodeField = value;
                }
            }

            /// <remarks/>
            public string birthDate
            {
                get
                {
                    return this.birthDateField;
                }
                set
                {
                    this.birthDateField = value;
                }
            }

            /// <remarks/>
            public memberOldMemberDataImmigrationStatusCode immigrationStatusCode
            {
                get
                {
                    return this.immigrationStatusCodeField;
                }
                set
                {
                    this.immigrationStatusCodeField = value;
                }
            }

            /// <remarks/>
            public memberOldMemberDataAddress address
            {
                get
                {
                    return this.addressField;
                }
                set
                {
                    this.addressField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberOldMemberDataPrimaryName
        {

            private string lastNameField;

            private string firstNameField;

            private string middleNameField;

            private string namePrefixField;

            private string nameSuffixField;

            private memberOldMemberDataPrimaryNameNameSuffixList nameSuffixListField;

            private memberOldMemberDataPrimaryNameCredentialSuffixList credentialSuffixListField;

            /// <remarks/>
            public string lastName
            {
                get
                {
                    return this.lastNameField;
                }
                set
                {
                    this.lastNameField = value;
                }
            }

            /// <remarks/>
            public string firstName
            {
                get
                {
                    return this.firstNameField;
                }
                set
                {
                    this.firstNameField = value;
                }
            }

            /// <remarks/>
            public string middleName
            {
                get
                {
                    return this.middleNameField;
                }
                set
                {
                    this.middleNameField = value;
                }
            }

            /// <remarks/>
            public string namePrefix
            {
                get
                {
                    return this.namePrefixField;
                }
                set
                {
                    this.namePrefixField = value;
                }
            }

            /// <remarks/>
            public string nameSuffix
            {
                get
                {
                    return this.nameSuffixField;
                }
                set
                {
                    this.nameSuffixField = value;
                }
            }

            /// <remarks/>
            public memberOldMemberDataPrimaryNameNameSuffixList nameSuffixList
            {
                get
                {
                    return this.nameSuffixListField;
                }
                set
                {
                    this.nameSuffixListField = value;
                }
            }

            /// <remarks/>
            public memberOldMemberDataPrimaryNameCredentialSuffixList credentialSuffixList
            {
                get
                {
                    return this.credentialSuffixListField;
                }
                set
                {
                    this.credentialSuffixListField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberOldMemberDataPrimaryNameNameSuffixList
        {

            private string listModeField;

            private string[] suffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("suffix")]
            public string[] suffix
            {
                get
                {
                    return this.suffixField;
                }
                set
                {
                    this.suffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberOldMemberDataPrimaryNameCredentialSuffixList
        {

            private string listModeField;

            private memberOldMemberDataPrimaryNameCredentialSuffixListCredentialSuffix[] credentialSuffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("CredentialSuffix")]
            public memberOldMemberDataPrimaryNameCredentialSuffixListCredentialSuffix[] CredentialSuffix
            {
                get
                {
                    return this.credentialSuffixField;
                }
                set
                {
                    this.credentialSuffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberOldMemberDataPrimaryNameCredentialSuffixListCredentialSuffix
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberOldMemberDataImmigrationStatusCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberOldMemberDataAddress
        {

            private string addressField;

            private string address2Field;

            private string address3Field;

            private string stateCodeField;

            private string zipCodeField;

            private string zipExtensionCodeField;

            private string cityNameField;

            private string countyCodeField;

            private memberOldMemberDataAddressCountryCode countryCodeField;

            /// <remarks/>
            public string address
            {
                get
                {
                    return this.addressField;
                }
                set
                {
                    this.addressField = value;
                }
            }

            /// <remarks/>
            public string address2
            {
                get
                {
                    return this.address2Field;
                }
                set
                {
                    this.address2Field = value;
                }
            }

            /// <remarks/>
            public string address3
            {
                get
                {
                    return this.address3Field;
                }
                set
                {
                    this.address3Field = value;
                }
            }

            /// <remarks/>
            public string stateCode
            {
                get
                {
                    return this.stateCodeField;
                }
                set
                {
                    this.stateCodeField = value;
                }
            }

            /// <remarks/>
            public string zipCode
            {
                get
                {
                    return this.zipCodeField;
                }
                set
                {
                    this.zipCodeField = value;
                }
            }

            /// <remarks/>
            public string zipExtensionCode
            {
                get
                {
                    return this.zipExtensionCodeField;
                }
                set
                {
                    this.zipExtensionCodeField = value;
                }
            }

            /// <remarks/>
            public string cityName
            {
                get
                {
                    return this.cityNameField;
                }
                set
                {
                    this.cityNameField = value;
                }
            }

            /// <remarks/>
            public string countyCode
            {
                get
                {
                    return this.countyCodeField;
                }
                set
                {
                    this.countyCodeField = value;
                }
            }

            /// <remarks/>
            public memberOldMemberDataAddressCountryCode countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberOldMemberDataAddressCountryCode
        {

            private string countryCodeField;

            /// <remarks/>
            public string countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberIndividual
        {

            private string taxIdentificationNumberField;

            private string genderCodeField;

            private string maritalStatusCodeField;

            private string birthDateField;

            private string birthPlaceStateCodeField;

            private string birthPlaceCountyCodeField;

            private memberIndividualBirthPlaceCountryCode birthPlaceCountryCodeField;

            private string dateOfDeathField;

            private memberIndividualPrimaryName primaryNameField;

            private memberIndividualOtherNames otherNamesField;

            private memberIndividualLanguages languagesField;

            private memberIndividualImmigrationStatusCode immigrationStatusCodeField;

            /// <remarks/>
            public string taxIdentificationNumber
            {
                get
                {
                    return this.taxIdentificationNumberField;
                }
                set
                {
                    this.taxIdentificationNumberField = value;
                }
            }

            /// <remarks/>
            public string genderCode
            {
                get
                {
                    return this.genderCodeField;
                }
                set
                {
                    this.genderCodeField = value;
                }
            }

            /// <remarks/>
            public string maritalStatusCode
            {
                get
                {
                    return this.maritalStatusCodeField;
                }
                set
                {
                    this.maritalStatusCodeField = value;
                }
            }

            /// <remarks/>
            public string birthDate
            {
                get
                {
                    return this.birthDateField;
                }
                set
                {
                    this.birthDateField = value;
                }
            }

            /// <remarks/>
            public string birthPlaceStateCode
            {
                get
                {
                    return this.birthPlaceStateCodeField;
                }
                set
                {
                    this.birthPlaceStateCodeField = value;
                }
            }

            /// <remarks/>
            public string birthPlaceCountyCode
            {
                get
                {
                    return this.birthPlaceCountyCodeField;
                }
                set
                {
                    this.birthPlaceCountyCodeField = value;
                }
            }

            /// <remarks/>
            public memberIndividualBirthPlaceCountryCode birthPlaceCountryCode
            {
                get
                {
                    return this.birthPlaceCountryCodeField;
                }
                set
                {
                    this.birthPlaceCountryCodeField = value;
                }
            }

            /// <remarks/>
            public string dateOfDeath
            {
                get
                {
                    return this.dateOfDeathField;
                }
                set
                {
                    this.dateOfDeathField = value;
                }
            }

            /// <remarks/>
            public memberIndividualPrimaryName primaryName
            {
                get
                {
                    return this.primaryNameField;
                }
                set
                {
                    this.primaryNameField = value;
                }
            }

            /// <remarks/>
            public memberIndividualOtherNames otherNames
            {
                get
                {
                    return this.otherNamesField;
                }
                set
                {
                    this.otherNamesField = value;
                }
            }

            /// <remarks/>
            public memberIndividualLanguages languages
            {
                get
                {
                    return this.languagesField;
                }
                set
                {
                    this.languagesField = value;
                }
            }

            /// <remarks/>
            public memberIndividualImmigrationStatusCode immigrationStatusCode
            {
                get
                {
                    return this.immigrationStatusCodeField;
                }
                set
                {
                    this.immigrationStatusCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberIndividualBirthPlaceCountryCode
        {

            private string countryCodeField;

            /// <remarks/>
            public string countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberIndividualPrimaryName
        {

            private string lastNameField;

            private string firstNameField;

            private string middleNameField;

            private string namePrefixField;

            private string nameSuffixField;

            private memberIndividualPrimaryNameNameSuffixList nameSuffixListField;

            private memberIndividualPrimaryNameCredentialSuffixList credentialSuffixListField;

            /// <remarks/>
            public string lastName
            {
                get
                {
                    return this.lastNameField;
                }
                set
                {
                    this.lastNameField = value;
                }
            }

            /// <remarks/>
            public string firstName
            {
                get
                {
                    return this.firstNameField;
                }
                set
                {
                    this.firstNameField = value;
                }
            }

            /// <remarks/>
            public string middleName
            {
                get
                {
                    return this.middleNameField;
                }
                set
                {
                    this.middleNameField = value;
                }
            }

            /// <remarks/>
            public string namePrefix
            {
                get
                {
                    return this.namePrefixField;
                }
                set
                {
                    this.namePrefixField = value;
                }
            }

            /// <remarks/>
            public string nameSuffix
            {
                get
                {
                    return this.nameSuffixField;
                }
                set
                {
                    this.nameSuffixField = value;
                }
            }

            /// <remarks/>
            public memberIndividualPrimaryNameNameSuffixList nameSuffixList
            {
                get
                {
                    return this.nameSuffixListField;
                }
                set
                {
                    this.nameSuffixListField = value;
                }
            }

            /// <remarks/>
            public memberIndividualPrimaryNameCredentialSuffixList credentialSuffixList
            {
                get
                {
                    return this.credentialSuffixListField;
                }
                set
                {
                    this.credentialSuffixListField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberIndividualPrimaryNameNameSuffixList
        {

            private string listModeField;

            private string[] suffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("suffix")]
            public string[] suffix
            {
                get
                {
                    return this.suffixField;
                }
                set
                {
                    this.suffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberIndividualPrimaryNameCredentialSuffixList
        {

            private string listModeField;

            private memberIndividualPrimaryNameCredentialSuffixListCredentialSuffix[] credentialSuffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("CredentialSuffix")]
            public memberIndividualPrimaryNameCredentialSuffixListCredentialSuffix[] CredentialSuffix
            {
                get
                {
                    return this.credentialSuffixField;
                }
                set
                {
                    this.credentialSuffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberIndividualPrimaryNameCredentialSuffixListCredentialSuffix
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberIndividualOtherNames
        {

            private string listModeField;

            private memberIndividualOtherNamesOtherName[] otherNameField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("otherName")]
            public memberIndividualOtherNamesOtherName[] otherName
            {
                get
                {
                    return this.otherNameField;
                }
                set
                {
                    this.otherNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberIndividualOtherNamesOtherName
        {

            private memberIndividualOtherNamesOtherNameOtherNameInfo otherNameInfoField;

            private memberIndividualOtherNamesOtherNameOtherNameTypeCode otherNameTypeCodeField;

            /// <remarks/>
            public memberIndividualOtherNamesOtherNameOtherNameInfo otherNameInfo
            {
                get
                {
                    return this.otherNameInfoField;
                }
                set
                {
                    this.otherNameInfoField = value;
                }
            }

            /// <remarks/>
            public memberIndividualOtherNamesOtherNameOtherNameTypeCode otherNameTypeCode
            {
                get
                {
                    return this.otherNameTypeCodeField;
                }
                set
                {
                    this.otherNameTypeCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberIndividualOtherNamesOtherNameOtherNameInfo
        {

            private string lastNameField;

            private string firstNameField;

            private string middleNameField;

            private string namePrefixField;

            private string nameSuffixField;

            private memberIndividualOtherNamesOtherNameOtherNameInfoNameSuffixList nameSuffixListField;

            private memberIndividualOtherNamesOtherNameOtherNameInfoCredentialSuffixList credentialSuffixListField;

            /// <remarks/>
            public string lastName
            {
                get
                {
                    return this.lastNameField;
                }
                set
                {
                    this.lastNameField = value;
                }
            }

            /// <remarks/>
            public string firstName
            {
                get
                {
                    return this.firstNameField;
                }
                set
                {
                    this.firstNameField = value;
                }
            }

            /// <remarks/>
            public string middleName
            {
                get
                {
                    return this.middleNameField;
                }
                set
                {
                    this.middleNameField = value;
                }
            }

            /// <remarks/>
            public string namePrefix
            {
                get
                {
                    return this.namePrefixField;
                }
                set
                {
                    this.namePrefixField = value;
                }
            }

            /// <remarks/>
            public string nameSuffix
            {
                get
                {
                    return this.nameSuffixField;
                }
                set
                {
                    this.nameSuffixField = value;
                }
            }

            /// <remarks/>
            public memberIndividualOtherNamesOtherNameOtherNameInfoNameSuffixList nameSuffixList
            {
                get
                {
                    return this.nameSuffixListField;
                }
                set
                {
                    this.nameSuffixListField = value;
                }
            }

            /// <remarks/>
            public memberIndividualOtherNamesOtherNameOtherNameInfoCredentialSuffixList credentialSuffixList
            {
                get
                {
                    return this.credentialSuffixListField;
                }
                set
                {
                    this.credentialSuffixListField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberIndividualOtherNamesOtherNameOtherNameInfoNameSuffixList
        {

            private string listModeField;

            private string[] suffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("suffix")]
            public string[] suffix
            {
                get
                {
                    return this.suffixField;
                }
                set
                {
                    this.suffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberIndividualOtherNamesOtherNameOtherNameInfoCredentialSuffixList
        {

            private string listModeField;

            private memberIndividualOtherNamesOtherNameOtherNameInfoCredentialSuffixListCredentialSuffix[] credentialSuffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("CredentialSuffix")]
            public memberIndividualOtherNamesOtherNameOtherNameInfoCredentialSuffixListCredentialSuffix[] CredentialSuffix
            {
                get
                {
                    return this.credentialSuffixField;
                }
                set
                {
                    this.credentialSuffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberIndividualOtherNamesOtherNameOtherNameInfoCredentialSuffixListCredentialSuffix
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberIndividualOtherNamesOtherNameOtherNameTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberIndividualLanguages
        {

            private string listModeField;

            private memberIndividualLanguagesLanguage[] languageField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("language")]
            public memberIndividualLanguagesLanguage[] language
            {
                get
                {
                    return this.languageField;
                }
                set
                {
                    this.languageField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberIndividualLanguagesLanguage
        {

            private string primaryLanguageField;

            private string nativeLanguageField;

            private memberIndividualLanguagesLanguageLanguageDomainCode languageDomainCodeField;

            /// <remarks/>
            public string primaryLanguage
            {
                get
                {
                    return this.primaryLanguageField;
                }
                set
                {
                    this.primaryLanguageField = value;
                }
            }

            /// <remarks/>
            public string nativeLanguage
            {
                get
                {
                    return this.nativeLanguageField;
                }
                set
                {
                    this.nativeLanguageField = value;
                }
            }

            /// <remarks/>
            public memberIndividualLanguagesLanguageLanguageDomainCode languageDomainCode
            {
                get
                {
                    return this.languageDomainCodeField;
                }
                set
                {
                    this.languageDomainCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberIndividualLanguagesLanguageLanguageDomainCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberIndividualImmigrationStatusCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPhysicalAddress
        {

            private string listModeField;

            private memberPhysicalAddressMemberPhysicalAddress[] memberPhysicalAddress1Field;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("memberPhysicalAddress")]
            public memberPhysicalAddressMemberPhysicalAddress[] memberPhysicalAddress1
            {
                get
                {
                    return this.memberPhysicalAddress1Field;
                }
                set
                {
                    this.memberPhysicalAddress1Field = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPhysicalAddressMemberPhysicalAddress
        {

            private string emailAddressField;

            private memberPhysicalAddressMemberPhysicalAddressAddressInfo addressInfoField;

            private memberPhysicalAddressMemberPhysicalAddressAddressTypeCode addressTypeCodeField;

            /// <remarks/>
            public string emailAddress
            {
                get
                {
                    return this.emailAddressField;
                }
                set
                {
                    this.emailAddressField = value;
                }
            }

            /// <remarks/>
            public memberPhysicalAddressMemberPhysicalAddressAddressInfo addressInfo
            {
                get
                {
                    return this.addressInfoField;
                }
                set
                {
                    this.addressInfoField = value;
                }
            }

            /// <remarks/>
            public memberPhysicalAddressMemberPhysicalAddressAddressTypeCode addressTypeCode
            {
                get
                {
                    return this.addressTypeCodeField;
                }
                set
                {
                    this.addressTypeCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPhysicalAddressMemberPhysicalAddressAddressInfo
        {

            private memberPhysicalAddressMemberPhysicalAddressAddressInfoPostalAddress postalAddressField;

            private memberPhysicalAddressMemberPhysicalAddressAddressInfoAddressPhoneList addressPhoneListField;

            /// <remarks/>
            public memberPhysicalAddressMemberPhysicalAddressAddressInfoPostalAddress postalAddress
            {
                get
                {
                    return this.postalAddressField;
                }
                set
                {
                    this.postalAddressField = value;
                }
            }

            /// <remarks/>
            public memberPhysicalAddressMemberPhysicalAddressAddressInfoAddressPhoneList addressPhoneList
            {
                get
                {
                    return this.addressPhoneListField;
                }
                set
                {
                    this.addressPhoneListField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPhysicalAddressMemberPhysicalAddressAddressInfoPostalAddress
        {

            private string addressField;

            private string address2Field;

            private string address3Field;

            private string stateCodeField;

            private string zipCodeField;

            private string zipExtensionCodeField;

            private string cityNameField;

            private string countyCodeField;

            private memberPhysicalAddressMemberPhysicalAddressAddressInfoPostalAddressCountryCode countryCodeField;

            private string longitudeField;

            private string latitudeField;

            private string ignoreAddressCheckField;

            private string undeliverableAddressField;

            /// <remarks/>
            public string address
            {
                get
                {
                    return this.addressField;
                }
                set
                {
                    this.addressField = value;
                }
            }

            /// <remarks/>
            public string address2
            {
                get
                {
                    return this.address2Field;
                }
                set
                {
                    this.address2Field = value;
                }
            }

            /// <remarks/>
            public string address3
            {
                get
                {
                    return this.address3Field;
                }
                set
                {
                    this.address3Field = value;
                }
            }

            /// <remarks/>
            public string stateCode
            {
                get
                {
                    return this.stateCodeField;
                }
                set
                {
                    this.stateCodeField = value;
                }
            }

            /// <remarks/>
            public string zipCode
            {
                get
                {
                    return this.zipCodeField;
                }
                set
                {
                    this.zipCodeField = value;
                }
            }

            /// <remarks/>
            public string zipExtensionCode
            {
                get
                {
                    return this.zipExtensionCodeField;
                }
                set
                {
                    this.zipExtensionCodeField = value;
                }
            }

            /// <remarks/>
            public string cityName
            {
                get
                {
                    return this.cityNameField;
                }
                set
                {
                    this.cityNameField = value;
                }
            }

            /// <remarks/>
            public string countyCode
            {
                get
                {
                    return this.countyCodeField;
                }
                set
                {
                    this.countyCodeField = value;
                }
            }

            /// <remarks/>
            public memberPhysicalAddressMemberPhysicalAddressAddressInfoPostalAddressCountryCode countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }

            /// <remarks/>
            public string longitude
            {
                get
                {
                    return this.longitudeField;
                }
                set
                {
                    this.longitudeField = value;
                }
            }

            /// <remarks/>
            public string latitude
            {
                get
                {
                    return this.latitudeField;
                }
                set
                {
                    this.latitudeField = value;
                }
            }

            /// <remarks/>
            public string ignoreAddressCheck
            {
                get
                {
                    return this.ignoreAddressCheckField;
                }
                set
                {
                    this.ignoreAddressCheckField = value;
                }
            }

            /// <remarks/>
            public string undeliverableAddress
            {
                get
                {
                    return this.undeliverableAddressField;
                }
                set
                {
                    this.undeliverableAddressField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPhysicalAddressMemberPhysicalAddressAddressInfoPostalAddressCountryCode
        {

            private string countryCodeField;

            /// <remarks/>
            public string countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPhysicalAddressMemberPhysicalAddressAddressInfoAddressPhoneList
        {

            private string listModeField;

            private memberPhysicalAddressMemberPhysicalAddressAddressInfoAddressPhoneListTelephoneNumber[] telephoneNumberField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("telephoneNumber")]
            public memberPhysicalAddressMemberPhysicalAddressAddressInfoAddressPhoneListTelephoneNumber[] telephoneNumber
            {
                get
                {
                    return this.telephoneNumberField;
                }
                set
                {
                    this.telephoneNumberField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPhysicalAddressMemberPhysicalAddressAddressInfoAddressPhoneListTelephoneNumber
        {

            private string phoneCountryCodeField;

            private string phoneAreaCodeField;

            private string phoneNumberField;

            private string phoneExtensionNumberField;

            private memberPhysicalAddressMemberPhysicalAddressAddressInfoAddressPhoneListTelephoneNumberIndividualPhoneTypeCode individualPhoneTypeCodeField;

            private memberPhysicalAddressMemberPhysicalAddressAddressInfoAddressPhoneListTelephoneNumberOrganizationPhoneTypeCode organizationPhoneTypeCodeField;

            /// <remarks/>
            public string phoneCountryCode
            {
                get
                {
                    return this.phoneCountryCodeField;
                }
                set
                {
                    this.phoneCountryCodeField = value;
                }
            }

            /// <remarks/>
            public string phoneAreaCode
            {
                get
                {
                    return this.phoneAreaCodeField;
                }
                set
                {
                    this.phoneAreaCodeField = value;
                }
            }

            /// <remarks/>
            public string phoneNumber
            {
                get
                {
                    return this.phoneNumberField;
                }
                set
                {
                    this.phoneNumberField = value;
                }
            }

            /// <remarks/>
            public string phoneExtensionNumber
            {
                get
                {
                    return this.phoneExtensionNumberField;
                }
                set
                {
                    this.phoneExtensionNumberField = value;
                }
            }

            /// <remarks/>
            public memberPhysicalAddressMemberPhysicalAddressAddressInfoAddressPhoneListTelephoneNumberIndividualPhoneTypeCode individualPhoneTypeCode
            {
                get
                {
                    return this.individualPhoneTypeCodeField;
                }
                set
                {
                    this.individualPhoneTypeCodeField = value;
                }
            }

            /// <remarks/>
            public memberPhysicalAddressMemberPhysicalAddressAddressInfoAddressPhoneListTelephoneNumberOrganizationPhoneTypeCode organizationPhoneTypeCode
            {
                get
                {
                    return this.organizationPhoneTypeCodeField;
                }
                set
                {
                    this.organizationPhoneTypeCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPhysicalAddressMemberPhysicalAddressAddressInfoAddressPhoneListTelephoneNumberIndividualPhoneTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPhysicalAddressMemberPhysicalAddressAddressInfoAddressPhoneListTelephoneNumberOrganizationPhoneTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPhysicalAddressMemberPhysicalAddressAddressTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberCorrespondenceAddress
        {

            private string emailAddressField;

            private memberCorrespondenceAddressPostalAddress postalAddressField;

            /// <remarks/>
            public string emailAddress
            {
                get
                {
                    return this.emailAddressField;
                }
                set
                {
                    this.emailAddressField = value;
                }
            }

            /// <remarks/>
            public memberCorrespondenceAddressPostalAddress postalAddress
            {
                get
                {
                    return this.postalAddressField;
                }
                set
                {
                    this.postalAddressField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberCorrespondenceAddressPostalAddress
        {

            private string addressField;

            private string address2Field;

            private string address3Field;

            private string stateCodeField;

            private string zipCodeField;

            private string zipExtensionCodeField;

            private string cityNameField;

            private string countyCodeField;

            private memberCorrespondenceAddressPostalAddressCountryCode countryCodeField;

            private string longitudeField;

            private string latitudeField;

            private string ignoreAddressCheckField;

            private string undeliverableAddressField;

            /// <remarks/>
            public string address
            {
                get
                {
                    return this.addressField;
                }
                set
                {
                    this.addressField = value;
                }
            }

            /// <remarks/>
            public string address2
            {
                get
                {
                    return this.address2Field;
                }
                set
                {
                    this.address2Field = value;
                }
            }

            /// <remarks/>
            public string address3
            {
                get
                {
                    return this.address3Field;
                }
                set
                {
                    this.address3Field = value;
                }
            }

            /// <remarks/>
            public string stateCode
            {
                get
                {
                    return this.stateCodeField;
                }
                set
                {
                    this.stateCodeField = value;
                }
            }

            /// <remarks/>
            public string zipCode
            {
                get
                {
                    return this.zipCodeField;
                }
                set
                {
                    this.zipCodeField = value;
                }
            }

            /// <remarks/>
            public string zipExtensionCode
            {
                get
                {
                    return this.zipExtensionCodeField;
                }
                set
                {
                    this.zipExtensionCodeField = value;
                }
            }

            /// <remarks/>
            public string cityName
            {
                get
                {
                    return this.cityNameField;
                }
                set
                {
                    this.cityNameField = value;
                }
            }

            /// <remarks/>
            public string countyCode
            {
                get
                {
                    return this.countyCodeField;
                }
                set
                {
                    this.countyCodeField = value;
                }
            }

            /// <remarks/>
            public memberCorrespondenceAddressPostalAddressCountryCode countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }

            /// <remarks/>
            public string longitude
            {
                get
                {
                    return this.longitudeField;
                }
                set
                {
                    this.longitudeField = value;
                }
            }

            /// <remarks/>
            public string latitude
            {
                get
                {
                    return this.latitudeField;
                }
                set
                {
                    this.latitudeField = value;
                }
            }

            /// <remarks/>
            public string ignoreAddressCheck
            {
                get
                {
                    return this.ignoreAddressCheckField;
                }
                set
                {
                    this.ignoreAddressCheckField = value;
                }
            }

            /// <remarks/>
            public string undeliverableAddress
            {
                get
                {
                    return this.undeliverableAddressField;
                }
                set
                {
                    this.undeliverableAddressField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberCorrespondenceAddressPostalAddressCountryCode
        {

            private string countryCodeField;

            /// <remarks/>
            public string countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberCommunicationPreferences
        {

            private string contactMethodField;

            private memberCommunicationPreferencesDocumentDeliveryMethodCode documentDeliveryMethodCodeField;

            private memberCommunicationPreferencesEmailFormatCode emailFormatCodeField;

            /// <remarks/>
            public string contactMethod
            {
                get
                {
                    return this.contactMethodField;
                }
                set
                {
                    this.contactMethodField = value;
                }
            }

            /// <remarks/>
            public memberCommunicationPreferencesDocumentDeliveryMethodCode documentDeliveryMethodCode
            {
                get
                {
                    return this.documentDeliveryMethodCodeField;
                }
                set
                {
                    this.documentDeliveryMethodCodeField = value;
                }
            }

            /// <remarks/>
            public memberCommunicationPreferencesEmailFormatCode emailFormatCode
            {
                get
                {
                    return this.emailFormatCodeField;
                }
                set
                {
                    this.emailFormatCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberCommunicationPreferencesDocumentDeliveryMethodCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberCommunicationPreferencesEmailFormatCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberSpecificCommunicationPreferences
        {

            private string contactMethodField;

            private memberSpecificCommunicationPreferencesDocumentDeliveryMethodCode documentDeliveryMethodCodeField;

            private string optOutField;

            private string correspondenceNameField;

            private string recipientField;

            /// <remarks/>
            public string contactMethod
            {
                get
                {
                    return this.contactMethodField;
                }
                set
                {
                    this.contactMethodField = value;
                }
            }

            /// <remarks/>
            public memberSpecificCommunicationPreferencesDocumentDeliveryMethodCode documentDeliveryMethodCode
            {
                get
                {
                    return this.documentDeliveryMethodCodeField;
                }
                set
                {
                    this.documentDeliveryMethodCodeField = value;
                }
            }

            /// <remarks/>
            public string optOut
            {
                get
                {
                    return this.optOutField;
                }
                set
                {
                    this.optOutField = value;
                }
            }

            /// <remarks/>
            public string correspondenceName
            {
                get
                {
                    return this.correspondenceNameField;
                }
                set
                {
                    this.correspondenceNameField = value;
                }
            }

            /// <remarks/>
            public string recipient
            {
                get
                {
                    return this.recipientField;
                }
                set
                {
                    this.recipientField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberSpecificCommunicationPreferencesDocumentDeliveryMethodCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberOtherIdNumberList
        {

            private string listModeField;

            private memberOtherIdNumberListIdentificationNumber[] identificationNumberField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("identificationNumber")]
            public memberOtherIdNumberListIdentificationNumber[] identificationNumber
            {
                get
                {
                    return this.identificationNumberField;
                }
                set
                {
                    this.identificationNumberField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberOtherIdNumberListIdentificationNumber
        {

            private string identificationNumberField;

            private string issuingStateField;

            private memberOtherIdNumberListIdentificationNumberIssuingCountry issuingCountryField;

            private string effectiveStartDateField;

            private string effectiveEndDateField;

            private memberOtherIdNumberListIdentificationNumberIdentificationTypeCode identificationTypeCodeField;

            /// <remarks/>
            public string identificationNumber
            {
                get
                {
                    return this.identificationNumberField;
                }
                set
                {
                    this.identificationNumberField = value;
                }
            }

            /// <remarks/>
            public string issuingState
            {
                get
                {
                    return this.issuingStateField;
                }
                set
                {
                    this.issuingStateField = value;
                }
            }

            /// <remarks/>
            public memberOtherIdNumberListIdentificationNumberIssuingCountry issuingCountry
            {
                get
                {
                    return this.issuingCountryField;
                }
                set
                {
                    this.issuingCountryField = value;
                }
            }

            /// <remarks/>
            public string effectiveStartDate
            {
                get
                {
                    return this.effectiveStartDateField;
                }
                set
                {
                    this.effectiveStartDateField = value;
                }
            }

            /// <remarks/>
            public string effectiveEndDate
            {
                get
                {
                    return this.effectiveEndDateField;
                }
                set
                {
                    this.effectiveEndDateField = value;
                }
            }

            /// <remarks/>
            public memberOtherIdNumberListIdentificationNumberIdentificationTypeCode identificationTypeCode
            {
                get
                {
                    return this.identificationTypeCodeField;
                }
                set
                {
                    this.identificationTypeCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberOtherIdNumberListIdentificationNumberIssuingCountry
        {

            private string countryCodeField;

            /// <remarks/>
            public string countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberOtherIdNumberListIdentificationNumberIdentificationTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberDisabilityInfoForEligibility
        {

            private string disabilityTypeCodeField;

            private string disabilityReportedDateField;

            private string verificationReceiptDateField;

            private memberDisabilityInfoForEligibilityDisabilityDenialReasonCode disabilityDenialReasonCodeField;

            private memberDisabilityInfoForEligibilityDiagnosisReference diagnosisReferenceField;

            /// <remarks/>
            public string disabilityTypeCode
            {
                get
                {
                    return this.disabilityTypeCodeField;
                }
                set
                {
                    this.disabilityTypeCodeField = value;
                }
            }

            /// <remarks/>
            public string disabilityReportedDate
            {
                get
                {
                    return this.disabilityReportedDateField;
                }
                set
                {
                    this.disabilityReportedDateField = value;
                }
            }

            /// <remarks/>
            public string verificationReceiptDate
            {
                get
                {
                    return this.verificationReceiptDateField;
                }
                set
                {
                    this.verificationReceiptDateField = value;
                }
            }

            /// <remarks/>
            public memberDisabilityInfoForEligibilityDisabilityDenialReasonCode disabilityDenialReasonCode
            {
                get
                {
                    return this.disabilityDenialReasonCodeField;
                }
                set
                {
                    this.disabilityDenialReasonCodeField = value;
                }
            }

            /// <remarks/>
            public memberDisabilityInfoForEligibilityDiagnosisReference diagnosisReference
            {
                get
                {
                    return this.diagnosisReferenceField;
                }
                set
                {
                    this.diagnosisReferenceField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberDisabilityInfoForEligibilityDisabilityDenialReasonCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberDisabilityInfoForEligibilityDiagnosisReference
        {

            private string idField;

            /// <remarks/>
            public string ID
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberDependentVerification
        {

            private string listModeField;

            private memberDependentVerificationDependentVerification[] dependentVerificationField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("dependentVerification")]
            public memberDependentVerificationDependentVerification[] dependentVerification
            {
                get
                {
                    return this.dependentVerificationField;
                }
                set
                {
                    this.dependentVerificationField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberDependentVerificationDependentVerification
        {

            private string verificationTypeField;

            private string certLastInfoRequestDateField;

            private string certLastInfoReceiptDateField;

            private string certInformationDueDateField;

            private string lastVerificationDateField;

            private memberDependentVerificationDependentVerificationStudentSchoolInfo studentSchoolInfoField;

            /// <remarks/>
            public string verificationType
            {
                get
                {
                    return this.verificationTypeField;
                }
                set
                {
                    this.verificationTypeField = value;
                }
            }

            /// <remarks/>
            public string certLastInfoRequestDate
            {
                get
                {
                    return this.certLastInfoRequestDateField;
                }
                set
                {
                    this.certLastInfoRequestDateField = value;
                }
            }

            /// <remarks/>
            public string certLastInfoReceiptDate
            {
                get
                {
                    return this.certLastInfoReceiptDateField;
                }
                set
                {
                    this.certLastInfoReceiptDateField = value;
                }
            }

            /// <remarks/>
            public string certInformationDueDate
            {
                get
                {
                    return this.certInformationDueDateField;
                }
                set
                {
                    this.certInformationDueDateField = value;
                }
            }

            /// <remarks/>
            public string lastVerificationDate
            {
                get
                {
                    return this.lastVerificationDateField;
                }
                set
                {
                    this.lastVerificationDateField = value;
                }
            }

            /// <remarks/>
            public memberDependentVerificationDependentVerificationStudentSchoolInfo studentSchoolInfo
            {
                get
                {
                    return this.studentSchoolInfoField;
                }
                set
                {
                    this.studentSchoolInfoField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberDependentVerificationDependentVerificationStudentSchoolInfo
        {

            private string studentStatusCodeField;

            private string graduationDateField;

            private memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformation schoolInformationField;

            private memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolTypeCode schoolTypeCodeField;

            /// <remarks/>
            public string studentStatusCode
            {
                get
                {
                    return this.studentStatusCodeField;
                }
                set
                {
                    this.studentStatusCodeField = value;
                }
            }

            /// <remarks/>
            public string graduationDate
            {
                get
                {
                    return this.graduationDateField;
                }
                set
                {
                    this.graduationDateField = value;
                }
            }

            /// <remarks/>
            public memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformation schoolInformation
            {
                get
                {
                    return this.schoolInformationField;
                }
                set
                {
                    this.schoolInformationField = value;
                }
            }

            /// <remarks/>
            public memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolTypeCode schoolTypeCode
            {
                get
                {
                    return this.schoolTypeCodeField;
                }
                set
                {
                    this.schoolTypeCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformation
        {

            private string institutionNameField;

            private memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfo contactInfoField;

            /// <remarks/>
            public string institutionName
            {
                get
                {
                    return this.institutionNameField;
                }
                set
                {
                    this.institutionNameField = value;
                }
            }

            /// <remarks/>
            public memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfo contactInfo
            {
                get
                {
                    return this.contactInfoField;
                }
                set
                {
                    this.contactInfoField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfo
        {

            private string contactDescriptionField;

            private string contactEmailAddressField;

            private memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoContactName contactNameField;

            private memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddress locationAddressField;

            private memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoContactTypeCode contactTypeCodeField;

            /// <remarks/>
            public string contactDescription
            {
                get
                {
                    return this.contactDescriptionField;
                }
                set
                {
                    this.contactDescriptionField = value;
                }
            }

            /// <remarks/>
            public string contactEmailAddress
            {
                get
                {
                    return this.contactEmailAddressField;
                }
                set
                {
                    this.contactEmailAddressField = value;
                }
            }

            /// <remarks/>
            public memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoContactName contactName
            {
                get
                {
                    return this.contactNameField;
                }
                set
                {
                    this.contactNameField = value;
                }
            }

            /// <remarks/>
            public memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddress locationAddress
            {
                get
                {
                    return this.locationAddressField;
                }
                set
                {
                    this.locationAddressField = value;
                }
            }

            /// <remarks/>
            public memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoContactTypeCode contactTypeCode
            {
                get
                {
                    return this.contactTypeCodeField;
                }
                set
                {
                    this.contactTypeCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoContactName
        {

            private string lastNameField;

            private string firstNameField;

            private string middleNameField;

            private string namePrefixField;

            private string nameSuffixField;

            private memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoContactNameNameSuffixList nameSuffixListField;

            private memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoContactNameCredentialSuffixList credentialSuffixListField;

            /// <remarks/>
            public string lastName
            {
                get
                {
                    return this.lastNameField;
                }
                set
                {
                    this.lastNameField = value;
                }
            }

            /// <remarks/>
            public string firstName
            {
                get
                {
                    return this.firstNameField;
                }
                set
                {
                    this.firstNameField = value;
                }
            }

            /// <remarks/>
            public string middleName
            {
                get
                {
                    return this.middleNameField;
                }
                set
                {
                    this.middleNameField = value;
                }
            }

            /// <remarks/>
            public string namePrefix
            {
                get
                {
                    return this.namePrefixField;
                }
                set
                {
                    this.namePrefixField = value;
                }
            }

            /// <remarks/>
            public string nameSuffix
            {
                get
                {
                    return this.nameSuffixField;
                }
                set
                {
                    this.nameSuffixField = value;
                }
            }

            /// <remarks/>
            public memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoContactNameNameSuffixList nameSuffixList
            {
                get
                {
                    return this.nameSuffixListField;
                }
                set
                {
                    this.nameSuffixListField = value;
                }
            }

            /// <remarks/>
            public memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoContactNameCredentialSuffixList credentialSuffixList
            {
                get
                {
                    return this.credentialSuffixListField;
                }
                set
                {
                    this.credentialSuffixListField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoContactNameNameSuffixList
        {

            private string listModeField;

            private string[] suffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("suffix")]
            public string[] suffix
            {
                get
                {
                    return this.suffixField;
                }
                set
                {
                    this.suffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoContactNameCredentialSuffixList
        {

            private string listModeField;

            private memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoContactNameCredentialSuffixListCredentialSuffix[] credentialSuffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("CredentialSuffix")]
            public memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoContactNameCredentialSuffixListCredentialSuffix[] CredentialSuffix
            {
                get
                {
                    return this.credentialSuffixField;
                }
                set
                {
                    this.credentialSuffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoContactNameCredentialSuffixListCredentialSuffix
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddress
        {

            private memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddressPostalAddress postalAddressField;

            private memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddressAddressPhoneList addressPhoneListField;

            /// <remarks/>
            public memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddressPostalAddress postalAddress
            {
                get
                {
                    return this.postalAddressField;
                }
                set
                {
                    this.postalAddressField = value;
                }
            }

            /// <remarks/>
            public memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddressAddressPhoneList addressPhoneList
            {
                get
                {
                    return this.addressPhoneListField;
                }
                set
                {
                    this.addressPhoneListField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddressPostalAddress
        {

            private string addressField;

            private string address2Field;

            private string address3Field;

            private string stateCodeField;

            private string zipCodeField;

            private string zipExtensionCodeField;

            private string cityNameField;

            private string countyCodeField;

            private memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddressPostalAddressCountryCode countryCodeField;

            private string longitudeField;

            private string latitudeField;

            private string ignoreAddressCheckField;

            private string undeliverableAddressField;

            /// <remarks/>
            public string address
            {
                get
                {
                    return this.addressField;
                }
                set
                {
                    this.addressField = value;
                }
            }

            /// <remarks/>
            public string address2
            {
                get
                {
                    return this.address2Field;
                }
                set
                {
                    this.address2Field = value;
                }
            }

            /// <remarks/>
            public string address3
            {
                get
                {
                    return this.address3Field;
                }
                set
                {
                    this.address3Field = value;
                }
            }

            /// <remarks/>
            public string stateCode
            {
                get
                {
                    return this.stateCodeField;
                }
                set
                {
                    this.stateCodeField = value;
                }
            }

            /// <remarks/>
            public string zipCode
            {
                get
                {
                    return this.zipCodeField;
                }
                set
                {
                    this.zipCodeField = value;
                }
            }

            /// <remarks/>
            public string zipExtensionCode
            {
                get
                {
                    return this.zipExtensionCodeField;
                }
                set
                {
                    this.zipExtensionCodeField = value;
                }
            }

            /// <remarks/>
            public string cityName
            {
                get
                {
                    return this.cityNameField;
                }
                set
                {
                    this.cityNameField = value;
                }
            }

            /// <remarks/>
            public string countyCode
            {
                get
                {
                    return this.countyCodeField;
                }
                set
                {
                    this.countyCodeField = value;
                }
            }

            /// <remarks/>
            public memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddressPostalAddressCountryCode countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }

            /// <remarks/>
            public string longitude
            {
                get
                {
                    return this.longitudeField;
                }
                set
                {
                    this.longitudeField = value;
                }
            }

            /// <remarks/>
            public string latitude
            {
                get
                {
                    return this.latitudeField;
                }
                set
                {
                    this.latitudeField = value;
                }
            }

            /// <remarks/>
            public string ignoreAddressCheck
            {
                get
                {
                    return this.ignoreAddressCheckField;
                }
                set
                {
                    this.ignoreAddressCheckField = value;
                }
            }

            /// <remarks/>
            public string undeliverableAddress
            {
                get
                {
                    return this.undeliverableAddressField;
                }
                set
                {
                    this.undeliverableAddressField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddressPostalAddressCountryCode
        {

            private string countryCodeField;

            /// <remarks/>
            public string countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddressAddressPhoneList
        {

            private string listModeField;

            private memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddressAddressPhoneListTelephoneNumber[] telephoneNumberField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("telephoneNumber")]
            public memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddressAddressPhoneListTelephoneNumber[] telephoneNumber
            {
                get
                {
                    return this.telephoneNumberField;
                }
                set
                {
                    this.telephoneNumberField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddressAddressPhoneListTelephoneNumber
        {

            private string phoneCountryCodeField;

            private string phoneAreaCodeField;

            private string phoneNumberField;

            private string phoneExtensionNumberField;

            private memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddressAddressPhoneListTelephoneNumberIndividualPhoneTypeCode individualPhoneTypeCodeField;

            private memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddressAddressPhoneListTelephoneNumberOrganizationPhoneTypeCode organizationPhoneTypeCodeField;

            /// <remarks/>
            public string phoneCountryCode
            {
                get
                {
                    return this.phoneCountryCodeField;
                }
                set
                {
                    this.phoneCountryCodeField = value;
                }
            }

            /// <remarks/>
            public string phoneAreaCode
            {
                get
                {
                    return this.phoneAreaCodeField;
                }
                set
                {
                    this.phoneAreaCodeField = value;
                }
            }

            /// <remarks/>
            public string phoneNumber
            {
                get
                {
                    return this.phoneNumberField;
                }
                set
                {
                    this.phoneNumberField = value;
                }
            }

            /// <remarks/>
            public string phoneExtensionNumber
            {
                get
                {
                    return this.phoneExtensionNumberField;
                }
                set
                {
                    this.phoneExtensionNumberField = value;
                }
            }

            /// <remarks/>
            public memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddressAddressPhoneListTelephoneNumberIndividualPhoneTypeCode individualPhoneTypeCode
            {
                get
                {
                    return this.individualPhoneTypeCodeField;
                }
                set
                {
                    this.individualPhoneTypeCodeField = value;
                }
            }

            /// <remarks/>
            public memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddressAddressPhoneListTelephoneNumberOrganizationPhoneTypeCode organizationPhoneTypeCode
            {
                get
                {
                    return this.organizationPhoneTypeCodeField;
                }
                set
                {
                    this.organizationPhoneTypeCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddressAddressPhoneListTelephoneNumberIndividualPhoneTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoLocationAddressAddressPhoneListTelephoneNumberOrganizationPhoneTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolInformationContactInfoContactTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberDependentVerificationDependentVerificationStudentSchoolInfoSchoolTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPhysicalCharacteristics
        {

            private string heightInchesCountField;

            private string weightPoundsCountField;

            /// <remarks/>
            public string heightInchesCount
            {
                get
                {
                    return this.heightInchesCountField;
                }
                set
                {
                    this.heightInchesCountField = value;
                }
            }

            /// <remarks/>
            public string weightPoundsCount
            {
                get
                {
                    return this.weightPoundsCountField;
                }
                set
                {
                    this.weightPoundsCountField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberEmploymentInfo
        {

            private string hireDateField;

            private string salaryIntervalField;

            private string salaryPerIntervalField;

            private string unionStatusField;

            private string positionOrTitleField;

            private string numberOfHoursWorkedField;

            private string retirementDateField;

            private memberEmploymentInfoEmploymentStatusCodeCode employmentStatusCodeCodeField;

            private memberEmploymentInfoSalaryGradeCodeCode salaryGradeCodeCodeField;

            private memberEmploymentInfoUnitsDomainCode unitsDomainCodeField;

            private memberEmploymentInfoEmployeeTypeDomainCode employeeTypeDomainCodeField;

            private string departmentField;

            /// <remarks/>
            public string hireDate
            {
                get
                {
                    return this.hireDateField;
                }
                set
                {
                    this.hireDateField = value;
                }
            }

            /// <remarks/>
            public string salaryInterval
            {
                get
                {
                    return this.salaryIntervalField;
                }
                set
                {
                    this.salaryIntervalField = value;
                }
            }

            /// <remarks/>
            public string salaryPerInterval
            {
                get
                {
                    return this.salaryPerIntervalField;
                }
                set
                {
                    this.salaryPerIntervalField = value;
                }
            }

            /// <remarks/>
            public string unionStatus
            {
                get
                {
                    return this.unionStatusField;
                }
                set
                {
                    this.unionStatusField = value;
                }
            }

            /// <remarks/>
            public string positionOrTitle
            {
                get
                {
                    return this.positionOrTitleField;
                }
                set
                {
                    this.positionOrTitleField = value;
                }
            }

            /// <remarks/>
            public string numberOfHoursWorked
            {
                get
                {
                    return this.numberOfHoursWorkedField;
                }
                set
                {
                    this.numberOfHoursWorkedField = value;
                }
            }

            /// <remarks/>
            public string retirementDate
            {
                get
                {
                    return this.retirementDateField;
                }
                set
                {
                    this.retirementDateField = value;
                }
            }

            /// <remarks/>
            public memberEmploymentInfoEmploymentStatusCodeCode employmentStatusCodeCode
            {
                get
                {
                    return this.employmentStatusCodeCodeField;
                }
                set
                {
                    this.employmentStatusCodeCodeField = value;
                }
            }

            /// <remarks/>
            public memberEmploymentInfoSalaryGradeCodeCode salaryGradeCodeCode
            {
                get
                {
                    return this.salaryGradeCodeCodeField;
                }
                set
                {
                    this.salaryGradeCodeCodeField = value;
                }
            }

            /// <remarks/>
            public memberEmploymentInfoUnitsDomainCode unitsDomainCode
            {
                get
                {
                    return this.unitsDomainCodeField;
                }
                set
                {
                    this.unitsDomainCodeField = value;
                }
            }

            /// <remarks/>
            public memberEmploymentInfoEmployeeTypeDomainCode employeeTypeDomainCode
            {
                get
                {
                    return this.employeeTypeDomainCodeField;
                }
                set
                {
                    this.employeeTypeDomainCodeField = value;
                }
            }

            /// <remarks/>
            public string department
            {
                get
                {
                    return this.departmentField;
                }
                set
                {
                    this.departmentField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberEmploymentInfoEmploymentStatusCodeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberEmploymentInfoSalaryGradeCodeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberEmploymentInfoUnitsDomainCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberEmploymentInfoEmployeeTypeDomainCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberDentalInfo
        {

            private string remarksField;

            private memberDentalInfoMissingTeeth missingTeethField;

            /// <remarks/>
            public string remarks
            {
                get
                {
                    return this.remarksField;
                }
                set
                {
                    this.remarksField = value;
                }
            }

            /// <remarks/>
            public memberDentalInfoMissingTeeth missingTeeth
            {
                get
                {
                    return this.missingTeethField;
                }
                set
                {
                    this.missingTeethField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberDentalInfoMissingTeeth
        {

            private string listModeField;

            private string toothNameField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            public string toothName
            {
                get
                {
                    return this.toothNameField;
                }
                set
                {
                    this.toothNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPriorCoverageInfo
        {

            private string priorCoverageMonthCountField;

            /// <remarks/>
            public string priorCoverageMonthCount
            {
                get
                {
                    return this.priorCoverageMonthCountField;
                }
                set
                {
                    this.priorCoverageMonthCountField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPreExistingConditionInfo
        {

            private string listModeField;

            private memberPreExistingConditionInfoPreExistingConditionInfo preExistingConditionInfoField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            public memberPreExistingConditionInfoPreExistingConditionInfo preExistingConditionInfo
            {
                get
                {
                    return this.preExistingConditionInfoField;
                }
                set
                {
                    this.preExistingConditionInfoField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPreExistingConditionInfoPreExistingConditionInfo
        {

            private string onsetOfConditionDateField;

            private string preexistingPeriodEndDateField;

            private string preexDescriptionField;

            private memberPreExistingConditionInfoPreExistingConditionInfoPreExDiagnosisCodeRangeConditions preExDiagnosisCodeRangeConditionsField;

            private memberPreExistingConditionInfoPreExistingConditionInfoPreExProcedureCodeRangeConditions preExProcedureCodeRangeConditionsField;

            private memberPreExistingConditionInfoPreExistingConditionInfoPreexistingConditions preexistingConditionsField;

            private memberPreExistingConditionInfoPreExistingConditionInfoPreexistingConditionProcedures preexistingConditionProceduresField;

            /// <remarks/>
            public string onsetOfConditionDate
            {
                get
                {
                    return this.onsetOfConditionDateField;
                }
                set
                {
                    this.onsetOfConditionDateField = value;
                }
            }

            /// <remarks/>
            public string preexistingPeriodEndDate
            {
                get
                {
                    return this.preexistingPeriodEndDateField;
                }
                set
                {
                    this.preexistingPeriodEndDateField = value;
                }
            }

            /// <remarks/>
            public string preexDescription
            {
                get
                {
                    return this.preexDescriptionField;
                }
                set
                {
                    this.preexDescriptionField = value;
                }
            }

            /// <remarks/>
            public memberPreExistingConditionInfoPreExistingConditionInfoPreExDiagnosisCodeRangeConditions preExDiagnosisCodeRangeConditions
            {
                get
                {
                    return this.preExDiagnosisCodeRangeConditionsField;
                }
                set
                {
                    this.preExDiagnosisCodeRangeConditionsField = value;
                }
            }

            /// <remarks/>
            public memberPreExistingConditionInfoPreExistingConditionInfoPreExProcedureCodeRangeConditions preExProcedureCodeRangeConditions
            {
                get
                {
                    return this.preExProcedureCodeRangeConditionsField;
                }
                set
                {
                    this.preExProcedureCodeRangeConditionsField = value;
                }
            }

            /// <remarks/>
            public memberPreExistingConditionInfoPreExistingConditionInfoPreexistingConditions preexistingConditions
            {
                get
                {
                    return this.preexistingConditionsField;
                }
                set
                {
                    this.preexistingConditionsField = value;
                }
            }

            /// <remarks/>
            public memberPreExistingConditionInfoPreExistingConditionInfoPreexistingConditionProcedures preexistingConditionProcedures
            {
                get
                {
                    return this.preexistingConditionProceduresField;
                }
                set
                {
                    this.preexistingConditionProceduresField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPreExistingConditionInfoPreExistingConditionInfoPreExDiagnosisCodeRangeConditions
        {

            private memberPreExistingConditionInfoPreExistingConditionInfoPreExDiagnosisCodeRangeConditionsPreExDiagnosisCodeRangeCondition preExDiagnosisCodeRangeConditionField;

            /// <remarks/>
            public memberPreExistingConditionInfoPreExistingConditionInfoPreExDiagnosisCodeRangeConditionsPreExDiagnosisCodeRangeCondition preExDiagnosisCodeRangeCondition
            {
                get
                {
                    return this.preExDiagnosisCodeRangeConditionField;
                }
                set
                {
                    this.preExDiagnosisCodeRangeConditionField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPreExistingConditionInfoPreExistingConditionInfoPreExDiagnosisCodeRangeConditionsPreExDiagnosisCodeRangeCondition
        {

            private memberPreExistingConditionInfoPreExistingConditionInfoPreExDiagnosisCodeRangeConditionsPreExDiagnosisCodeRangeConditionRangeStartDiagnosisCode rangeStartDiagnosisCodeField;

            private memberPreExistingConditionInfoPreExistingConditionInfoPreExDiagnosisCodeRangeConditionsPreExDiagnosisCodeRangeConditionRangeEndDiagnosisCode rangeEndDiagnosisCodeField;

            /// <remarks/>
            public memberPreExistingConditionInfoPreExistingConditionInfoPreExDiagnosisCodeRangeConditionsPreExDiagnosisCodeRangeConditionRangeStartDiagnosisCode rangeStartDiagnosisCode
            {
                get
                {
                    return this.rangeStartDiagnosisCodeField;
                }
                set
                {
                    this.rangeStartDiagnosisCodeField = value;
                }
            }

            /// <remarks/>
            public memberPreExistingConditionInfoPreExistingConditionInfoPreExDiagnosisCodeRangeConditionsPreExDiagnosisCodeRangeConditionRangeEndDiagnosisCode rangeEndDiagnosisCode
            {
                get
                {
                    return this.rangeEndDiagnosisCodeField;
                }
                set
                {
                    this.rangeEndDiagnosisCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPreExistingConditionInfoPreExistingConditionInfoPreExDiagnosisCodeRangeConditionsPreExDiagnosisCodeRangeConditionRangeStartDiagnosisCode
        {

            private memberPreExistingConditionInfoPreExistingConditionInfoPreExDiagnosisCodeRangeConditionsPreExDiagnosisCodeRangeConditionRangeStartDiagnosisCodeDiagnosisReference diagnosisReferenceField;

            /// <remarks/>
            public memberPreExistingConditionInfoPreExistingConditionInfoPreExDiagnosisCodeRangeConditionsPreExDiagnosisCodeRangeConditionRangeStartDiagnosisCodeDiagnosisReference diagnosisReference
            {
                get
                {
                    return this.diagnosisReferenceField;
                }
                set
                {
                    this.diagnosisReferenceField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPreExistingConditionInfoPreExistingConditionInfoPreExDiagnosisCodeRangeConditionsPreExDiagnosisCodeRangeConditionRangeStartDiagnosisCodeDiagnosisReference
        {

            private string idField;

            /// <remarks/>
            public string ID
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPreExistingConditionInfoPreExistingConditionInfoPreExDiagnosisCodeRangeConditionsPreExDiagnosisCodeRangeConditionRangeEndDiagnosisCode
        {

            private memberPreExistingConditionInfoPreExistingConditionInfoPreExDiagnosisCodeRangeConditionsPreExDiagnosisCodeRangeConditionRangeEndDiagnosisCodeDiagnosisReference diagnosisReferenceField;

            /// <remarks/>
            public memberPreExistingConditionInfoPreExistingConditionInfoPreExDiagnosisCodeRangeConditionsPreExDiagnosisCodeRangeConditionRangeEndDiagnosisCodeDiagnosisReference diagnosisReference
            {
                get
                {
                    return this.diagnosisReferenceField;
                }
                set
                {
                    this.diagnosisReferenceField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPreExistingConditionInfoPreExistingConditionInfoPreExDiagnosisCodeRangeConditionsPreExDiagnosisCodeRangeConditionRangeEndDiagnosisCodeDiagnosisReference
        {

            private string idField;

            /// <remarks/>
            public string ID
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPreExistingConditionInfoPreExistingConditionInfoPreExProcedureCodeRangeConditions
        {

            private memberPreExistingConditionInfoPreExistingConditionInfoPreExProcedureCodeRangeConditionsPreExProcedureCodeRangeCondition preExProcedureCodeRangeConditionField;

            /// <remarks/>
            public memberPreExistingConditionInfoPreExistingConditionInfoPreExProcedureCodeRangeConditionsPreExProcedureCodeRangeCondition preExProcedureCodeRangeCondition
            {
                get
                {
                    return this.preExProcedureCodeRangeConditionField;
                }
                set
                {
                    this.preExProcedureCodeRangeConditionField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPreExistingConditionInfoPreExistingConditionInfoPreExProcedureCodeRangeConditionsPreExProcedureCodeRangeCondition
        {

            private memberPreExistingConditionInfoPreExistingConditionInfoPreExProcedureCodeRangeConditionsPreExProcedureCodeRangeConditionRangeStartProcedureCode rangeStartProcedureCodeField;

            private memberPreExistingConditionInfoPreExistingConditionInfoPreExProcedureCodeRangeConditionsPreExProcedureCodeRangeConditionRangeEndProcedureCode rangeEndProcedureCodeField;

            /// <remarks/>
            public memberPreExistingConditionInfoPreExistingConditionInfoPreExProcedureCodeRangeConditionsPreExProcedureCodeRangeConditionRangeStartProcedureCode rangeStartProcedureCode
            {
                get
                {
                    return this.rangeStartProcedureCodeField;
                }
                set
                {
                    this.rangeStartProcedureCodeField = value;
                }
            }

            /// <remarks/>
            public memberPreExistingConditionInfoPreExistingConditionInfoPreExProcedureCodeRangeConditionsPreExProcedureCodeRangeConditionRangeEndProcedureCode rangeEndProcedureCode
            {
                get
                {
                    return this.rangeEndProcedureCodeField;
                }
                set
                {
                    this.rangeEndProcedureCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPreExistingConditionInfoPreExistingConditionInfoPreExProcedureCodeRangeConditionsPreExProcedureCodeRangeConditionRangeStartProcedureCode
        {

            private memberPreExistingConditionInfoPreExistingConditionInfoPreExProcedureCodeRangeConditionsPreExProcedureCodeRangeConditionRangeStartProcedureCodeServiceReference serviceReferenceField;

            /// <remarks/>
            public memberPreExistingConditionInfoPreExistingConditionInfoPreExProcedureCodeRangeConditionsPreExProcedureCodeRangeConditionRangeStartProcedureCodeServiceReference serviceReference
            {
                get
                {
                    return this.serviceReferenceField;
                }
                set
                {
                    this.serviceReferenceField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPreExistingConditionInfoPreExistingConditionInfoPreExProcedureCodeRangeConditionsPreExProcedureCodeRangeConditionRangeStartProcedureCodeServiceReference
        {

            private string idField;

            /// <remarks/>
            public string ID
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPreExistingConditionInfoPreExistingConditionInfoPreExProcedureCodeRangeConditionsPreExProcedureCodeRangeConditionRangeEndProcedureCode
        {

            private memberPreExistingConditionInfoPreExistingConditionInfoPreExProcedureCodeRangeConditionsPreExProcedureCodeRangeConditionRangeEndProcedureCodeServiceReference serviceReferenceField;

            /// <remarks/>
            public memberPreExistingConditionInfoPreExistingConditionInfoPreExProcedureCodeRangeConditionsPreExProcedureCodeRangeConditionRangeEndProcedureCodeServiceReference serviceReference
            {
                get
                {
                    return this.serviceReferenceField;
                }
                set
                {
                    this.serviceReferenceField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPreExistingConditionInfoPreExistingConditionInfoPreExProcedureCodeRangeConditionsPreExProcedureCodeRangeConditionRangeEndProcedureCodeServiceReference
        {

            private string idField;

            /// <remarks/>
            public string ID
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPreExistingConditionInfoPreExistingConditionInfoPreexistingConditions
        {

            private memberPreExistingConditionInfoPreExistingConditionInfoPreexistingConditionsDiagnosisReference diagnosisReferenceField;

            /// <remarks/>
            public memberPreExistingConditionInfoPreExistingConditionInfoPreexistingConditionsDiagnosisReference diagnosisReference
            {
                get
                {
                    return this.diagnosisReferenceField;
                }
                set
                {
                    this.diagnosisReferenceField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPreExistingConditionInfoPreExistingConditionInfoPreexistingConditionsDiagnosisReference
        {

            private string idField;

            /// <remarks/>
            public string ID
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPreExistingConditionInfoPreExistingConditionInfoPreexistingConditionProcedures
        {

            private memberPreExistingConditionInfoPreExistingConditionInfoPreexistingConditionProceduresServiceReference serviceReferenceField;

            /// <remarks/>
            public memberPreExistingConditionInfoPreExistingConditionInfoPreexistingConditionProceduresServiceReference serviceReference
            {
                get
                {
                    return this.serviceReferenceField;
                }
                set
                {
                    this.serviceReferenceField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPreExistingConditionInfoPreExistingConditionInfoPreexistingConditionProceduresServiceReference
        {

            private string idField;

            /// <remarks/>
            public string ID
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPreExReduction
        {

            private string preexStartDateField;

            private string reductionEndDateField;

            private string reductionReasonReceiptDateField;

            private memberPreExReductionReductionReasonCode reductionReasonCodeField;

            /// <remarks/>
            public string preexStartDate
            {
                get
                {
                    return this.preexStartDateField;
                }
                set
                {
                    this.preexStartDateField = value;
                }
            }

            /// <remarks/>
            public string reductionEndDate
            {
                get
                {
                    return this.reductionEndDateField;
                }
                set
                {
                    this.reductionEndDateField = value;
                }
            }

            /// <remarks/>
            public string reductionReasonReceiptDate
            {
                get
                {
                    return this.reductionReasonReceiptDateField;
                }
                set
                {
                    this.reductionReasonReceiptDateField = value;
                }
            }

            /// <remarks/>
            public memberPreExReductionReductionReasonCode reductionReasonCode
            {
                get
                {
                    return this.reductionReasonCodeField;
                }
                set
                {
                    this.reductionReasonCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPreExReductionReductionReasonCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberOtherResponsiblePersonInfo
        {

            private string listModeField;

            private memberOtherResponsiblePersonInfoOtherResponsiblePersonInfo otherResponsiblePersonInfoField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            public memberOtherResponsiblePersonInfoOtherResponsiblePersonInfo otherResponsiblePersonInfo
            {
                get
                {
                    return this.otherResponsiblePersonInfoField;
                }
                set
                {
                    this.otherResponsiblePersonInfoField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberOtherResponsiblePersonInfoOtherResponsiblePersonInfo
        {

            private string sourceOfAuthorizationField;

            private string responsiblePersonEmailAddressField;

            private memberOtherResponsiblePersonInfoOtherResponsiblePersonInfoResponsiblePersonName responsiblePersonNameField;

            private memberOtherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfo responsibleAddressInfoField;

            private memberOtherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibilityTypeCodeCode responsibilityTypeCodeCodeField;

            private memberOtherResponsiblePersonInfoOtherResponsiblePersonInfoRelationshipToMemberCode relationshipToMemberCodeField;

            /// <remarks/>
            public string sourceOfAuthorization
            {
                get
                {
                    return this.sourceOfAuthorizationField;
                }
                set
                {
                    this.sourceOfAuthorizationField = value;
                }
            }

            /// <remarks/>
            public string responsiblePersonEmailAddress
            {
                get
                {
                    return this.responsiblePersonEmailAddressField;
                }
                set
                {
                    this.responsiblePersonEmailAddressField = value;
                }
            }

            /// <remarks/>
            public memberOtherResponsiblePersonInfoOtherResponsiblePersonInfoResponsiblePersonName responsiblePersonName
            {
                get
                {
                    return this.responsiblePersonNameField;
                }
                set
                {
                    this.responsiblePersonNameField = value;
                }
            }

            /// <remarks/>
            public memberOtherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfo responsibleAddressInfo
            {
                get
                {
                    return this.responsibleAddressInfoField;
                }
                set
                {
                    this.responsibleAddressInfoField = value;
                }
            }

            /// <remarks/>
            public memberOtherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibilityTypeCodeCode responsibilityTypeCodeCode
            {
                get
                {
                    return this.responsibilityTypeCodeCodeField;
                }
                set
                {
                    this.responsibilityTypeCodeCodeField = value;
                }
            }

            /// <remarks/>
            public memberOtherResponsiblePersonInfoOtherResponsiblePersonInfoRelationshipToMemberCode relationshipToMemberCode
            {
                get
                {
                    return this.relationshipToMemberCodeField;
                }
                set
                {
                    this.relationshipToMemberCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberOtherResponsiblePersonInfoOtherResponsiblePersonInfoResponsiblePersonName
        {

            private string lastNameField;

            private string firstNameField;

            private string middleNameField;

            private string namePrefixField;

            private string nameSuffixField;

            private memberOtherResponsiblePersonInfoOtherResponsiblePersonInfoResponsiblePersonNameNameSuffixList nameSuffixListField;

            private memberOtherResponsiblePersonInfoOtherResponsiblePersonInfoResponsiblePersonNameCredentialSuffixList credentialSuffixListField;

            /// <remarks/>
            public string lastName
            {
                get
                {
                    return this.lastNameField;
                }
                set
                {
                    this.lastNameField = value;
                }
            }

            /// <remarks/>
            public string firstName
            {
                get
                {
                    return this.firstNameField;
                }
                set
                {
                    this.firstNameField = value;
                }
            }

            /// <remarks/>
            public string middleName
            {
                get
                {
                    return this.middleNameField;
                }
                set
                {
                    this.middleNameField = value;
                }
            }

            /// <remarks/>
            public string namePrefix
            {
                get
                {
                    return this.namePrefixField;
                }
                set
                {
                    this.namePrefixField = value;
                }
            }

            /// <remarks/>
            public string nameSuffix
            {
                get
                {
                    return this.nameSuffixField;
                }
                set
                {
                    this.nameSuffixField = value;
                }
            }

            /// <remarks/>
            public memberOtherResponsiblePersonInfoOtherResponsiblePersonInfoResponsiblePersonNameNameSuffixList nameSuffixList
            {
                get
                {
                    return this.nameSuffixListField;
                }
                set
                {
                    this.nameSuffixListField = value;
                }
            }

            /// <remarks/>
            public memberOtherResponsiblePersonInfoOtherResponsiblePersonInfoResponsiblePersonNameCredentialSuffixList credentialSuffixList
            {
                get
                {
                    return this.credentialSuffixListField;
                }
                set
                {
                    this.credentialSuffixListField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberOtherResponsiblePersonInfoOtherResponsiblePersonInfoResponsiblePersonNameNameSuffixList
        {

            private string listModeField;

            private string suffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            public string suffix
            {
                get
                {
                    return this.suffixField;
                }
                set
                {
                    this.suffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberOtherResponsiblePersonInfoOtherResponsiblePersonInfoResponsiblePersonNameCredentialSuffixList
        {

            private string listModeField;

            private memberOtherResponsiblePersonInfoOtherResponsiblePersonInfoResponsiblePersonNameCredentialSuffixListCredentialSuffix credentialSuffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            public memberOtherResponsiblePersonInfoOtherResponsiblePersonInfoResponsiblePersonNameCredentialSuffixListCredentialSuffix CredentialSuffix
            {
                get
                {
                    return this.credentialSuffixField;
                }
                set
                {
                    this.credentialSuffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberOtherResponsiblePersonInfoOtherResponsiblePersonInfoResponsiblePersonNameCredentialSuffixListCredentialSuffix
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberOtherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfo
        {

            private memberOtherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfoPostalAddress postalAddressField;

            private memberOtherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfoAddressPhoneList addressPhoneListField;

            /// <remarks/>
            public memberOtherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfoPostalAddress postalAddress
            {
                get
                {
                    return this.postalAddressField;
                }
                set
                {
                    this.postalAddressField = value;
                }
            }

            /// <remarks/>
            public memberOtherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfoAddressPhoneList addressPhoneList
            {
                get
                {
                    return this.addressPhoneListField;
                }
                set
                {
                    this.addressPhoneListField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberOtherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfoPostalAddress
        {

            private string addressField;

            private string address2Field;

            private string address3Field;

            private string stateCodeField;

            private string zipCodeField;

            private string zipExtensionCodeField;

            private string cityNameField;

            private string countyCodeField;

            private memberOtherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfoPostalAddressCountryCode countryCodeField;

            private string longitudeField;

            private string latitudeField;

            private string ignoreAddressCheckField;

            private string undeliverableAddressField;

            /// <remarks/>
            public string address
            {
                get
                {
                    return this.addressField;
                }
                set
                {
                    this.addressField = value;
                }
            }

            /// <remarks/>
            public string address2
            {
                get
                {
                    return this.address2Field;
                }
                set
                {
                    this.address2Field = value;
                }
            }

            /// <remarks/>
            public string address3
            {
                get
                {
                    return this.address3Field;
                }
                set
                {
                    this.address3Field = value;
                }
            }

            /// <remarks/>
            public string stateCode
            {
                get
                {
                    return this.stateCodeField;
                }
                set
                {
                    this.stateCodeField = value;
                }
            }

            /// <remarks/>
            public string zipCode
            {
                get
                {
                    return this.zipCodeField;
                }
                set
                {
                    this.zipCodeField = value;
                }
            }

            /// <remarks/>
            public string zipExtensionCode
            {
                get
                {
                    return this.zipExtensionCodeField;
                }
                set
                {
                    this.zipExtensionCodeField = value;
                }
            }

            /// <remarks/>
            public string cityName
            {
                get
                {
                    return this.cityNameField;
                }
                set
                {
                    this.cityNameField = value;
                }
            }

            /// <remarks/>
            public string countyCode
            {
                get
                {
                    return this.countyCodeField;
                }
                set
                {
                    this.countyCodeField = value;
                }
            }

            /// <remarks/>
            public memberOtherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfoPostalAddressCountryCode countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }

            /// <remarks/>
            public string longitude
            {
                get
                {
                    return this.longitudeField;
                }
                set
                {
                    this.longitudeField = value;
                }
            }

            /// <remarks/>
            public string latitude
            {
                get
                {
                    return this.latitudeField;
                }
                set
                {
                    this.latitudeField = value;
                }
            }

            /// <remarks/>
            public string ignoreAddressCheck
            {
                get
                {
                    return this.ignoreAddressCheckField;
                }
                set
                {
                    this.ignoreAddressCheckField = value;
                }
            }

            /// <remarks/>
            public string undeliverableAddress
            {
                get
                {
                    return this.undeliverableAddressField;
                }
                set
                {
                    this.undeliverableAddressField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberOtherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfoPostalAddressCountryCode
        {

            private string countryCodeField;

            /// <remarks/>
            public string countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberOtherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfoAddressPhoneList
        {

            private string listModeField;

            private memberOtherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfoAddressPhoneListTelephoneNumber telephoneNumberField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            public memberOtherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfoAddressPhoneListTelephoneNumber telephoneNumber
            {
                get
                {
                    return this.telephoneNumberField;
                }
                set
                {
                    this.telephoneNumberField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberOtherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfoAddressPhoneListTelephoneNumber
        {

            private string phoneCountryCodeField;

            private string phoneAreaCodeField;

            private string phoneNumberField;

            private string phoneExtensionNumberField;

            private memberOtherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfoAddressPhoneListTelephoneNumberIndividualPhoneTypeCode individualPhoneTypeCodeField;

            private memberOtherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfoAddressPhoneListTelephoneNumberOrganizationPhoneTypeCode organizationPhoneTypeCodeField;

            /// <remarks/>
            public string phoneCountryCode
            {
                get
                {
                    return this.phoneCountryCodeField;
                }
                set
                {
                    this.phoneCountryCodeField = value;
                }
            }

            /// <remarks/>
            public string phoneAreaCode
            {
                get
                {
                    return this.phoneAreaCodeField;
                }
                set
                {
                    this.phoneAreaCodeField = value;
                }
            }

            /// <remarks/>
            public string phoneNumber
            {
                get
                {
                    return this.phoneNumberField;
                }
                set
                {
                    this.phoneNumberField = value;
                }
            }

            /// <remarks/>
            public string phoneExtensionNumber
            {
                get
                {
                    return this.phoneExtensionNumberField;
                }
                set
                {
                    this.phoneExtensionNumberField = value;
                }
            }

            /// <remarks/>
            public memberOtherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfoAddressPhoneListTelephoneNumberIndividualPhoneTypeCode individualPhoneTypeCode
            {
                get
                {
                    return this.individualPhoneTypeCodeField;
                }
                set
                {
                    this.individualPhoneTypeCodeField = value;
                }
            }

            /// <remarks/>
            public memberOtherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfoAddressPhoneListTelephoneNumberOrganizationPhoneTypeCode organizationPhoneTypeCode
            {
                get
                {
                    return this.organizationPhoneTypeCodeField;
                }
                set
                {
                    this.organizationPhoneTypeCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberOtherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfoAddressPhoneListTelephoneNumberIndividualPhoneTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberOtherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibleAddressInfoAddressPhoneListTelephoneNumberOrganizationPhoneTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberOtherResponsiblePersonInfoOtherResponsiblePersonInfoResponsibilityTypeCodeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberOtherResponsiblePersonInfoOtherResponsiblePersonInfoRelationshipToMemberCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMemberRepresentative
        {

            private string listModeField;

            private memberMemberRepresentativeAuthorizedIndividual authorizedIndividualField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            public memberMemberRepresentativeAuthorizedIndividual authorizedIndividual
            {
                get
                {
                    return this.authorizedIndividualField;
                }
                set
                {
                    this.authorizedIndividualField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMemberRepresentativeAuthorizedIndividual
        {

            private string effectiveStartDateField;

            private string effectiveEndDateField;

            private string scopeField;

            private string hccReviewerField;

            private string documentsReceivedDateField;

            private memberMemberRepresentativeAuthorizedIndividualAuthorizedPerson authorizedPersonField;

            private memberMemberRepresentativeAuthorizedIndividualAuthorizedIndividualGroupPhoneList authorizedIndividualGroupPhoneListField;

            private memberMemberRepresentativeAuthorizedIndividualRepresentativeTypeCode representativeTypeCodeField;

            private memberMemberRepresentativeAuthorizedIndividualRelationshipToMemberCode relationshipToMemberCodeField;

            /// <remarks/>
            public string effectiveStartDate
            {
                get
                {
                    return this.effectiveStartDateField;
                }
                set
                {
                    this.effectiveStartDateField = value;
                }
            }

            /// <remarks/>
            public string effectiveEndDate
            {
                get
                {
                    return this.effectiveEndDateField;
                }
                set
                {
                    this.effectiveEndDateField = value;
                }
            }

            /// <remarks/>
            public string scope
            {
                get
                {
                    return this.scopeField;
                }
                set
                {
                    this.scopeField = value;
                }
            }

            /// <remarks/>
            public string hccReviewer
            {
                get
                {
                    return this.hccReviewerField;
                }
                set
                {
                    this.hccReviewerField = value;
                }
            }

            /// <remarks/>
            public string documentsReceivedDate
            {
                get
                {
                    return this.documentsReceivedDateField;
                }
                set
                {
                    this.documentsReceivedDateField = value;
                }
            }

            /// <remarks/>
            public memberMemberRepresentativeAuthorizedIndividualAuthorizedPerson authorizedPerson
            {
                get
                {
                    return this.authorizedPersonField;
                }
                set
                {
                    this.authorizedPersonField = value;
                }
            }

            /// <remarks/>
            public memberMemberRepresentativeAuthorizedIndividualAuthorizedIndividualGroupPhoneList authorizedIndividualGroupPhoneList
            {
                get
                {
                    return this.authorizedIndividualGroupPhoneListField;
                }
                set
                {
                    this.authorizedIndividualGroupPhoneListField = value;
                }
            }

            /// <remarks/>
            public memberMemberRepresentativeAuthorizedIndividualRepresentativeTypeCode representativeTypeCode
            {
                get
                {
                    return this.representativeTypeCodeField;
                }
                set
                {
                    this.representativeTypeCodeField = value;
                }
            }

            /// <remarks/>
            public memberMemberRepresentativeAuthorizedIndividualRelationshipToMemberCode relationshipToMemberCode
            {
                get
                {
                    return this.relationshipToMemberCodeField;
                }
                set
                {
                    this.relationshipToMemberCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMemberRepresentativeAuthorizedIndividualAuthorizedPerson
        {

            private string lastNameField;

            private string firstNameField;

            private string middleNameField;

            private string namePrefixField;

            private string nameSuffixField;

            private memberMemberRepresentativeAuthorizedIndividualAuthorizedPersonNameSuffixList nameSuffixListField;

            private memberMemberRepresentativeAuthorizedIndividualAuthorizedPersonCredentialSuffixList credentialSuffixListField;

            /// <remarks/>
            public string lastName
            {
                get
                {
                    return this.lastNameField;
                }
                set
                {
                    this.lastNameField = value;
                }
            }

            /// <remarks/>
            public string firstName
            {
                get
                {
                    return this.firstNameField;
                }
                set
                {
                    this.firstNameField = value;
                }
            }

            /// <remarks/>
            public string middleName
            {
                get
                {
                    return this.middleNameField;
                }
                set
                {
                    this.middleNameField = value;
                }
            }

            /// <remarks/>
            public string namePrefix
            {
                get
                {
                    return this.namePrefixField;
                }
                set
                {
                    this.namePrefixField = value;
                }
            }

            /// <remarks/>
            public string nameSuffix
            {
                get
                {
                    return this.nameSuffixField;
                }
                set
                {
                    this.nameSuffixField = value;
                }
            }

            /// <remarks/>
            public memberMemberRepresentativeAuthorizedIndividualAuthorizedPersonNameSuffixList nameSuffixList
            {
                get
                {
                    return this.nameSuffixListField;
                }
                set
                {
                    this.nameSuffixListField = value;
                }
            }

            /// <remarks/>
            public memberMemberRepresentativeAuthorizedIndividualAuthorizedPersonCredentialSuffixList credentialSuffixList
            {
                get
                {
                    return this.credentialSuffixListField;
                }
                set
                {
                    this.credentialSuffixListField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMemberRepresentativeAuthorizedIndividualAuthorizedPersonNameSuffixList
        {

            private string listModeField;

            private string suffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            public string suffix
            {
                get
                {
                    return this.suffixField;
                }
                set
                {
                    this.suffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMemberRepresentativeAuthorizedIndividualAuthorizedPersonCredentialSuffixList
        {

            private string listModeField;

            private memberMemberRepresentativeAuthorizedIndividualAuthorizedPersonCredentialSuffixListCredentialSuffix credentialSuffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            public memberMemberRepresentativeAuthorizedIndividualAuthorizedPersonCredentialSuffixListCredentialSuffix CredentialSuffix
            {
                get
                {
                    return this.credentialSuffixField;
                }
                set
                {
                    this.credentialSuffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMemberRepresentativeAuthorizedIndividualAuthorizedPersonCredentialSuffixListCredentialSuffix
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMemberRepresentativeAuthorizedIndividualAuthorizedIndividualGroupPhoneList
        {

            private string listModeField;

            private memberMemberRepresentativeAuthorizedIndividualAuthorizedIndividualGroupPhoneListTelephoneNumber telephoneNumberField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            public memberMemberRepresentativeAuthorizedIndividualAuthorizedIndividualGroupPhoneListTelephoneNumber telephoneNumber
            {
                get
                {
                    return this.telephoneNumberField;
                }
                set
                {
                    this.telephoneNumberField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMemberRepresentativeAuthorizedIndividualAuthorizedIndividualGroupPhoneListTelephoneNumber
        {

            private string phoneCountryCodeField;

            private string phoneAreaCodeField;

            private string phoneNumberField;

            private string phoneExtensionNumberField;

            private memberMemberRepresentativeAuthorizedIndividualAuthorizedIndividualGroupPhoneListTelephoneNumberIndividualPhoneTypeCode individualPhoneTypeCodeField;

            private memberMemberRepresentativeAuthorizedIndividualAuthorizedIndividualGroupPhoneListTelephoneNumberOrganizationPhoneTypeCode organizationPhoneTypeCodeField;

            /// <remarks/>
            public string phoneCountryCode
            {
                get
                {
                    return this.phoneCountryCodeField;
                }
                set
                {
                    this.phoneCountryCodeField = value;
                }
            }

            /// <remarks/>
            public string phoneAreaCode
            {
                get
                {
                    return this.phoneAreaCodeField;
                }
                set
                {
                    this.phoneAreaCodeField = value;
                }
            }

            /// <remarks/>
            public string phoneNumber
            {
                get
                {
                    return this.phoneNumberField;
                }
                set
                {
                    this.phoneNumberField = value;
                }
            }

            /// <remarks/>
            public string phoneExtensionNumber
            {
                get
                {
                    return this.phoneExtensionNumberField;
                }
                set
                {
                    this.phoneExtensionNumberField = value;
                }
            }

            /// <remarks/>
            public memberMemberRepresentativeAuthorizedIndividualAuthorizedIndividualGroupPhoneListTelephoneNumberIndividualPhoneTypeCode individualPhoneTypeCode
            {
                get
                {
                    return this.individualPhoneTypeCodeField;
                }
                set
                {
                    this.individualPhoneTypeCodeField = value;
                }
            }

            /// <remarks/>
            public memberMemberRepresentativeAuthorizedIndividualAuthorizedIndividualGroupPhoneListTelephoneNumberOrganizationPhoneTypeCode organizationPhoneTypeCode
            {
                get
                {
                    return this.organizationPhoneTypeCodeField;
                }
                set
                {
                    this.organizationPhoneTypeCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMemberRepresentativeAuthorizedIndividualAuthorizedIndividualGroupPhoneListTelephoneNumberIndividualPhoneTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMemberRepresentativeAuthorizedIndividualAuthorizedIndividualGroupPhoneListTelephoneNumberOrganizationPhoneTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMemberRepresentativeAuthorizedIndividualRepresentativeTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMemberRepresentativeAuthorizedIndividualRelationshipToMemberCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMembershipUDTList
        {

            private string listModeField;

            private memberMembershipUDTListMembershipUDT[] membershipUDTField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("membershipUDT")]
            public memberMembershipUDTListMembershipUDT[] membershipUDT
            {
                get
                {
                    return this.membershipUDTField;
                }
                set
                {
                    this.membershipUDTField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMembershipUDTListMembershipUDT
        {

            private memberMembershipUDTListMembershipUDTUdtListValueSet[] udtListValueSetField;

            private memberMembershipUDTListMembershipUDTUserDefinedTermReference userDefinedTermReferenceField;

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("udtListValueSet")]
            public memberMembershipUDTListMembershipUDTUdtListValueSet[] udtListValueSet
            {
                get
                {
                    return this.udtListValueSetField;
                }
                set
                {
                    this.udtListValueSetField = value;
                }
            }

            /// <remarks/>
            public memberMembershipUDTListMembershipUDTUserDefinedTermReference userDefinedTermReference
            {
                get
                {
                    return this.userDefinedTermReferenceField;
                }
                set
                {
                    this.userDefinedTermReferenceField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMembershipUDTListMembershipUDTUdtListValueSet
        {

            private string attributeRoleNameField;

            private string attrValueAsStringField;

            /// <remarks/>
            public string attributeRoleName
            {
                get
                {
                    return this.attributeRoleNameField;
                }
                set
                {
                    this.attributeRoleNameField = value;
                }
            }

            /// <remarks/>
            public string attrValueAsString
            {
                get
                {
                    return this.attrValueAsStringField;
                }
                set
                {
                    this.attrValueAsStringField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMembershipUDTListMembershipUDTUserDefinedTermReference
        {

            private string idField;

            /// <remarks/>
            public string ID
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberInformationAccessPermissionList
        {

            private string listModeField;

            private memberInformationAccessPermissionListHIPAAPermission[] hIPAAPermissionField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("HIPAAPermission")]
            public memberInformationAccessPermissionListHIPAAPermission[] HIPAAPermission
            {
                get
                {
                    return this.hIPAAPermissionField;
                }
                set
                {
                    this.hIPAAPermissionField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberInformationAccessPermissionListHIPAAPermission
        {

            private string permissionScopeField;

            private string effectiveStartDateField;

            private string effectiveEndDateField;

            private memberInformationAccessPermissionListHIPAAPermissionPermittedPerson permittedPersonField;

            private memberInformationAccessPermissionListHIPAAPermissionHIPAAPermissionGroupPhoneList hIPAAPermissionGroupPhoneListField;

            private memberInformationAccessPermissionListHIPAAPermissionRelationshipToMemberCode relationshipToMemberCodeField;

            /// <remarks/>
            public string permissionScope
            {
                get
                {
                    return this.permissionScopeField;
                }
                set
                {
                    this.permissionScopeField = value;
                }
            }

            /// <remarks/>
            public string effectiveStartDate
            {
                get
                {
                    return this.effectiveStartDateField;
                }
                set
                {
                    this.effectiveStartDateField = value;
                }
            }

            /// <remarks/>
            public string effectiveEndDate
            {
                get
                {
                    return this.effectiveEndDateField;
                }
                set
                {
                    this.effectiveEndDateField = value;
                }
            }

            /// <remarks/>
            public memberInformationAccessPermissionListHIPAAPermissionPermittedPerson permittedPerson
            {
                get
                {
                    return this.permittedPersonField;
                }
                set
                {
                    this.permittedPersonField = value;
                }
            }

            /// <remarks/>
            public memberInformationAccessPermissionListHIPAAPermissionHIPAAPermissionGroupPhoneList hIPAAPermissionGroupPhoneList
            {
                get
                {
                    return this.hIPAAPermissionGroupPhoneListField;
                }
                set
                {
                    this.hIPAAPermissionGroupPhoneListField = value;
                }
            }

            /// <remarks/>
            public memberInformationAccessPermissionListHIPAAPermissionRelationshipToMemberCode relationshipToMemberCode
            {
                get
                {
                    return this.relationshipToMemberCodeField;
                }
                set
                {
                    this.relationshipToMemberCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberInformationAccessPermissionListHIPAAPermissionPermittedPerson
        {

            private string lastNameField;

            private string firstNameField;

            private string middleNameField;

            private string namePrefixField;

            private string nameSuffixField;

            private memberInformationAccessPermissionListHIPAAPermissionPermittedPersonNameSuffixList nameSuffixListField;

            private memberInformationAccessPermissionListHIPAAPermissionPermittedPersonCredentialSuffixList credentialSuffixListField;

            /// <remarks/>
            public string lastName
            {
                get
                {
                    return this.lastNameField;
                }
                set
                {
                    this.lastNameField = value;
                }
            }

            /// <remarks/>
            public string firstName
            {
                get
                {
                    return this.firstNameField;
                }
                set
                {
                    this.firstNameField = value;
                }
            }

            /// <remarks/>
            public string middleName
            {
                get
                {
                    return this.middleNameField;
                }
                set
                {
                    this.middleNameField = value;
                }
            }

            /// <remarks/>
            public string namePrefix
            {
                get
                {
                    return this.namePrefixField;
                }
                set
                {
                    this.namePrefixField = value;
                }
            }

            /// <remarks/>
            public string nameSuffix
            {
                get
                {
                    return this.nameSuffixField;
                }
                set
                {
                    this.nameSuffixField = value;
                }
            }

            /// <remarks/>
            public memberInformationAccessPermissionListHIPAAPermissionPermittedPersonNameSuffixList nameSuffixList
            {
                get
                {
                    return this.nameSuffixListField;
                }
                set
                {
                    this.nameSuffixListField = value;
                }
            }

            /// <remarks/>
            public memberInformationAccessPermissionListHIPAAPermissionPermittedPersonCredentialSuffixList credentialSuffixList
            {
                get
                {
                    return this.credentialSuffixListField;
                }
                set
                {
                    this.credentialSuffixListField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberInformationAccessPermissionListHIPAAPermissionPermittedPersonNameSuffixList
        {

            private string listModeField;

            private string[] suffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("suffix")]
            public string[] suffix
            {
                get
                {
                    return this.suffixField;
                }
                set
                {
                    this.suffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberInformationAccessPermissionListHIPAAPermissionPermittedPersonCredentialSuffixList
        {

            private string listModeField;

            private memberInformationAccessPermissionListHIPAAPermissionPermittedPersonCredentialSuffixListCredentialSuffix[] credentialSuffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("CredentialSuffix")]
            public memberInformationAccessPermissionListHIPAAPermissionPermittedPersonCredentialSuffixListCredentialSuffix[] CredentialSuffix
            {
                get
                {
                    return this.credentialSuffixField;
                }
                set
                {
                    this.credentialSuffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberInformationAccessPermissionListHIPAAPermissionPermittedPersonCredentialSuffixListCredentialSuffix
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberInformationAccessPermissionListHIPAAPermissionHIPAAPermissionGroupPhoneList
        {

            private string listModeField;

            private memberInformationAccessPermissionListHIPAAPermissionHIPAAPermissionGroupPhoneListTelephoneNumber[] telephoneNumberField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("telephoneNumber")]
            public memberInformationAccessPermissionListHIPAAPermissionHIPAAPermissionGroupPhoneListTelephoneNumber[] telephoneNumber
            {
                get
                {
                    return this.telephoneNumberField;
                }
                set
                {
                    this.telephoneNumberField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberInformationAccessPermissionListHIPAAPermissionHIPAAPermissionGroupPhoneListTelephoneNumber
        {

            private string phoneCountryCodeField;

            private string phoneAreaCodeField;

            private string phoneNumberField;

            private string phoneExtensionNumberField;

            private memberInformationAccessPermissionListHIPAAPermissionHIPAAPermissionGroupPhoneListTelephoneNumberIndividualPhoneTypeCode individualPhoneTypeCodeField;

            private memberInformationAccessPermissionListHIPAAPermissionHIPAAPermissionGroupPhoneListTelephoneNumberOrganizationPhoneTypeCode organizationPhoneTypeCodeField;

            /// <remarks/>
            public string phoneCountryCode
            {
                get
                {
                    return this.phoneCountryCodeField;
                }
                set
                {
                    this.phoneCountryCodeField = value;
                }
            }

            /// <remarks/>
            public string phoneAreaCode
            {
                get
                {
                    return this.phoneAreaCodeField;
                }
                set
                {
                    this.phoneAreaCodeField = value;
                }
            }

            /// <remarks/>
            public string phoneNumber
            {
                get
                {
                    return this.phoneNumberField;
                }
                set
                {
                    this.phoneNumberField = value;
                }
            }

            /// <remarks/>
            public string phoneExtensionNumber
            {
                get
                {
                    return this.phoneExtensionNumberField;
                }
                set
                {
                    this.phoneExtensionNumberField = value;
                }
            }

            /// <remarks/>
            public memberInformationAccessPermissionListHIPAAPermissionHIPAAPermissionGroupPhoneListTelephoneNumberIndividualPhoneTypeCode individualPhoneTypeCode
            {
                get
                {
                    return this.individualPhoneTypeCodeField;
                }
                set
                {
                    this.individualPhoneTypeCodeField = value;
                }
            }

            /// <remarks/>
            public memberInformationAccessPermissionListHIPAAPermissionHIPAAPermissionGroupPhoneListTelephoneNumberOrganizationPhoneTypeCode organizationPhoneTypeCode
            {
                get
                {
                    return this.organizationPhoneTypeCodeField;
                }
                set
                {
                    this.organizationPhoneTypeCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberInformationAccessPermissionListHIPAAPermissionHIPAAPermissionGroupPhoneListTelephoneNumberIndividualPhoneTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberInformationAccessPermissionListHIPAAPermissionHIPAAPermissionGroupPhoneListTelephoneNumberOrganizationPhoneTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberInformationAccessPermissionListHIPAAPermissionRelationshipToMemberCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberInformationAccessRestrictionList
        {

            private string listModeField;

            private memberInformationAccessRestrictionListHIPAARestriction[] hIPAARestrictionField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("HIPAARestriction")]
            public memberInformationAccessRestrictionListHIPAARestriction[] HIPAARestriction
            {
                get
                {
                    return this.hIPAARestrictionField;
                }
                set
                {
                    this.hIPAARestrictionField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberInformationAccessRestrictionListHIPAARestriction
        {

            private string restrictionScopeField;

            private string effectiveStartDateField;

            private string effectiveEndDateField;

            private memberInformationAccessRestrictionListHIPAARestrictionRestrictedPerson restrictedPersonField;

            private memberInformationAccessRestrictionListHIPAARestrictionRelationshipToMemberCode relationshipToMemberCodeField;

            /// <remarks/>
            public string restrictionScope
            {
                get
                {
                    return this.restrictionScopeField;
                }
                set
                {
                    this.restrictionScopeField = value;
                }
            }

            /// <remarks/>
            public string effectiveStartDate
            {
                get
                {
                    return this.effectiveStartDateField;
                }
                set
                {
                    this.effectiveStartDateField = value;
                }
            }

            /// <remarks/>
            public string effectiveEndDate
            {
                get
                {
                    return this.effectiveEndDateField;
                }
                set
                {
                    this.effectiveEndDateField = value;
                }
            }

            /// <remarks/>
            public memberInformationAccessRestrictionListHIPAARestrictionRestrictedPerson restrictedPerson
            {
                get
                {
                    return this.restrictedPersonField;
                }
                set
                {
                    this.restrictedPersonField = value;
                }
            }

            /// <remarks/>
            public memberInformationAccessRestrictionListHIPAARestrictionRelationshipToMemberCode relationshipToMemberCode
            {
                get
                {
                    return this.relationshipToMemberCodeField;
                }
                set
                {
                    this.relationshipToMemberCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberInformationAccessRestrictionListHIPAARestrictionRestrictedPerson
        {

            private string lastNameField;

            private string firstNameField;

            private string middleNameField;

            private string namePrefixField;

            private string nameSuffixField;

            private memberInformationAccessRestrictionListHIPAARestrictionRestrictedPersonNameSuffixList nameSuffixListField;

            private memberInformationAccessRestrictionListHIPAARestrictionRestrictedPersonCredentialSuffixList credentialSuffixListField;

            /// <remarks/>
            public string lastName
            {
                get
                {
                    return this.lastNameField;
                }
                set
                {
                    this.lastNameField = value;
                }
            }

            /// <remarks/>
            public string firstName
            {
                get
                {
                    return this.firstNameField;
                }
                set
                {
                    this.firstNameField = value;
                }
            }

            /// <remarks/>
            public string middleName
            {
                get
                {
                    return this.middleNameField;
                }
                set
                {
                    this.middleNameField = value;
                }
            }

            /// <remarks/>
            public string namePrefix
            {
                get
                {
                    return this.namePrefixField;
                }
                set
                {
                    this.namePrefixField = value;
                }
            }

            /// <remarks/>
            public string nameSuffix
            {
                get
                {
                    return this.nameSuffixField;
                }
                set
                {
                    this.nameSuffixField = value;
                }
            }

            /// <remarks/>
            public memberInformationAccessRestrictionListHIPAARestrictionRestrictedPersonNameSuffixList nameSuffixList
            {
                get
                {
                    return this.nameSuffixListField;
                }
                set
                {
                    this.nameSuffixListField = value;
                }
            }

            /// <remarks/>
            public memberInformationAccessRestrictionListHIPAARestrictionRestrictedPersonCredentialSuffixList credentialSuffixList
            {
                get
                {
                    return this.credentialSuffixListField;
                }
                set
                {
                    this.credentialSuffixListField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberInformationAccessRestrictionListHIPAARestrictionRestrictedPersonNameSuffixList
        {

            private string listModeField;

            private string[] suffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("suffix")]
            public string[] suffix
            {
                get
                {
                    return this.suffixField;
                }
                set
                {
                    this.suffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberInformationAccessRestrictionListHIPAARestrictionRestrictedPersonCredentialSuffixList
        {

            private string listModeField;

            private memberInformationAccessRestrictionListHIPAARestrictionRestrictedPersonCredentialSuffixListCredentialSuffix[] credentialSuffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("CredentialSuffix")]
            public memberInformationAccessRestrictionListHIPAARestrictionRestrictedPersonCredentialSuffixListCredentialSuffix[] CredentialSuffix
            {
                get
                {
                    return this.credentialSuffixField;
                }
                set
                {
                    this.credentialSuffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberInformationAccessRestrictionListHIPAARestrictionRestrictedPersonCredentialSuffixListCredentialSuffix
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberInformationAccessRestrictionListHIPAARestrictionRelationshipToMemberCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPhiAuthorizationList
        {

            private string listModeField;

            private memberPhiAuthorizationListHIPAAAuthorization[] hIPAAAuthorizationField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("HIPAAAuthorization")]
            public memberPhiAuthorizationListHIPAAAuthorization[] HIPAAAuthorization
            {
                get
                {
                    return this.hIPAAAuthorizationField;
                }
                set
                {
                    this.hIPAAAuthorizationField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPhiAuthorizationListHIPAAAuthorization
        {

            private string authorizationTypeField;

            private string authorizationScopeField;

            private string effectiveStartDateField;

            private string effectiveEndDateField;

            private string whoCanBeDisclosedToField;

            private string signatureField;

            private string signatureDateField;

            private memberPhiAuthorizationListHIPAAAuthorizationAuthorizationPurposeCode authorizationPurposeCodeField;

            /// <remarks/>
            public string authorizationType
            {
                get
                {
                    return this.authorizationTypeField;
                }
                set
                {
                    this.authorizationTypeField = value;
                }
            }

            /// <remarks/>
            public string authorizationScope
            {
                get
                {
                    return this.authorizationScopeField;
                }
                set
                {
                    this.authorizationScopeField = value;
                }
            }

            /// <remarks/>
            public string effectiveStartDate
            {
                get
                {
                    return this.effectiveStartDateField;
                }
                set
                {
                    this.effectiveStartDateField = value;
                }
            }

            /// <remarks/>
            public string effectiveEndDate
            {
                get
                {
                    return this.effectiveEndDateField;
                }
                set
                {
                    this.effectiveEndDateField = value;
                }
            }

            /// <remarks/>
            public string whoCanBeDisclosedTo
            {
                get
                {
                    return this.whoCanBeDisclosedToField;
                }
                set
                {
                    this.whoCanBeDisclosedToField = value;
                }
            }

            /// <remarks/>
            public string signature
            {
                get
                {
                    return this.signatureField;
                }
                set
                {
                    this.signatureField = value;
                }
            }

            /// <remarks/>
            public string signatureDate
            {
                get
                {
                    return this.signatureDateField;
                }
                set
                {
                    this.signatureDateField = value;
                }
            }

            /// <remarks/>
            public memberPhiAuthorizationListHIPAAAuthorizationAuthorizationPurposeCode authorizationPurposeCode
            {
                get
                {
                    return this.authorizationPurposeCodeField;
                }
                set
                {
                    this.authorizationPurposeCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPhiAuthorizationListHIPAAAuthorizationAuthorizationPurposeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberCcDirectiveList
        {

            private string listModeField;

            private memberCcDirectiveListConfidentialCommunicationDirective[] confidentialCommunicationDirectiveField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("confidentialCommunicationDirective")]
            public memberCcDirectiveListConfidentialCommunicationDirective[] confidentialCommunicationDirective
            {
                get
                {
                    return this.confidentialCommunicationDirectiveField;
                }
                set
                {
                    this.confidentialCommunicationDirectiveField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberCcDirectiveListConfidentialCommunicationDirective
        {

            private string confidentialCommunicationScopeField;

            private string effectiveStartDateField;

            private string effectiveEndDateField;

            private memberCcDirectiveListConfidentialCommunicationDirectiveConfidentialCommunicationTypeCode confidentialCommunicationTypeCodeField;

            /// <remarks/>
            public string confidentialCommunicationScope
            {
                get
                {
                    return this.confidentialCommunicationScopeField;
                }
                set
                {
                    this.confidentialCommunicationScopeField = value;
                }
            }

            /// <remarks/>
            public string effectiveStartDate
            {
                get
                {
                    return this.effectiveStartDateField;
                }
                set
                {
                    this.effectiveStartDateField = value;
                }
            }

            /// <remarks/>
            public string effectiveEndDate
            {
                get
                {
                    return this.effectiveEndDateField;
                }
                set
                {
                    this.effectiveEndDateField = value;
                }
            }

            /// <remarks/>
            public memberCcDirectiveListConfidentialCommunicationDirectiveConfidentialCommunicationTypeCode confidentialCommunicationTypeCode
            {
                get
                {
                    return this.confidentialCommunicationTypeCodeField;
                }
                set
                {
                    this.confidentialCommunicationTypeCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberCcDirectiveListConfidentialCommunicationDirectiveConfidentialCommunicationTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberSubscriptionReference
        {

            private string idField;

            /// <remarks/>
            public string ID
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberIndividualReference
        {

            private string idField;

            /// <remarks/>
            public string ID
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberRelationshipToSubscriberDefinitionReference
        {

            private memberRelationshipToSubscriberDefinitionReferenceReference referenceField;

            private string relationshipNameField;

            private string domainNameField;

            /// <remarks/>
            public memberRelationshipToSubscriberDefinitionReferenceReference reference
            {
                get
                {
                    return this.referenceField;
                }
                set
                {
                    this.referenceField = value;
                }
            }

            /// <remarks/>
            public string relationshipName
            {
                get
                {
                    return this.relationshipNameField;
                }
                set
                {
                    this.relationshipNameField = value;
                }
            }

            /// <remarks/>
            public string domainName
            {
                get
                {
                    return this.domainNameField;
                }
                set
                {
                    this.domainNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberRelationshipToSubscriberDefinitionReferenceReference
        {

            private string idField;

            /// <remarks/>
            public string ID
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberRaceOrEthnicity
        {

            private string listModeField;

            private memberRaceOrEthnicityRaceOrEthnicityCodes[] raceOrEthnicityCodesField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("raceOrEthnicityCodes")]
            public memberRaceOrEthnicityRaceOrEthnicityCodes[] raceOrEthnicityCodes
            {
                get
                {
                    return this.raceOrEthnicityCodesField;
                }
                set
                {
                    this.raceOrEthnicityCodesField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberRaceOrEthnicityRaceOrEthnicityCodes
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberVipReasonCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberHealthStatusReasonCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberInformationSourceCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberAttachmentSetReference
        {

            private string idField;

            /// <remarks/>
            public string ID
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPlanSelection
        {

            private string startDateField;

            private string endDateField;

            private string claimHistoryFromOtherPlansField;

            private memberPlanSelectionEnrollmentInfo enrollmentInfoField;

            private memberPlanSelectionMemberTerminateReasonCode memberTerminateReasonCodeField;

            private memberPlanSelectionSpecialEnrollmentPeriodReasonCode specialEnrollmentPeriodReasonCodeField;

            private bool waiveBenefitWaitingPeriodField;

            private string planDescriptionField;

            private string planCodeField;

            private string classOfContractNumberField;

            private string programCodeField;

            private memberPlanSelectionBenefitPlanMatchData benefitPlanMatchDataField;

            private memberPlanSelectionBenefitPlanReference benefitPlanReferenceField;

            private string healthCoverageMaintenanceCodeField;

            private string insuranceLineCodeField;

            private string[] benefitNetworksField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }

            /// <remarks/>
            public string claimHistoryFromOtherPlans
            {
                get
                {
                    return this.claimHistoryFromOtherPlansField;
                }
                set
                {
                    this.claimHistoryFromOtherPlansField = value;
                }
            }

            /// <remarks/>
            public memberPlanSelectionEnrollmentInfo enrollmentInfo
            {
                get
                {
                    return this.enrollmentInfoField;
                }
                set
                {
                    this.enrollmentInfoField = value;
                }
            }

            /// <remarks/>
            public memberPlanSelectionMemberTerminateReasonCode memberTerminateReasonCode
            {
                get
                {
                    return this.memberTerminateReasonCodeField;
                }
                set
                {
                    this.memberTerminateReasonCodeField = value;
                }
            }

            /// <remarks/>
            public memberPlanSelectionSpecialEnrollmentPeriodReasonCode specialEnrollmentPeriodReasonCode
            {
                get
                {
                    return this.specialEnrollmentPeriodReasonCodeField;
                }
                set
                {
                    this.specialEnrollmentPeriodReasonCodeField = value;
                }
            }

            /// <remarks/>
            public bool waiveBenefitWaitingPeriod
            {
                get
                {
                    return this.waiveBenefitWaitingPeriodField;
                }
                set
                {
                    this.waiveBenefitWaitingPeriodField = value;
                }
            }

            /// <remarks/>
            public string planDescription
            {
                get
                {
                    return this.planDescriptionField;
                }
                set
                {
                    this.planDescriptionField = value;
                }
            }

            /// <remarks/>
            public string planCode
            {
                get
                {
                    return this.planCodeField;
                }
                set
                {
                    this.planCodeField = value;
                }
            }

            /// <remarks/>
            public string classOfContractNumber
            {
                get
                {
                    return this.classOfContractNumberField;
                }
                set
                {
                    this.classOfContractNumberField = value;
                }
            }

            /// <remarks/>
            public string programCode
            {
                get
                {
                    return this.programCodeField;
                }
                set
                {
                    this.programCodeField = value;
                }
            }

            /// <remarks/>
            public memberPlanSelectionBenefitPlanMatchData benefitPlanMatchData
            {
                get
                {
                    return this.benefitPlanMatchDataField;
                }
                set
                {
                    this.benefitPlanMatchDataField = value;
                }
            }

            /// <remarks/>
            public memberPlanSelectionBenefitPlanReference benefitPlanReference
            {
                get
                {
                    return this.benefitPlanReferenceField;
                }
                set
                {
                    this.benefitPlanReferenceField = value;
                }
            }

            /// <remarks/>
            public string healthCoverageMaintenanceCode
            {
                get
                {
                    return this.healthCoverageMaintenanceCodeField;
                }
                set
                {
                    this.healthCoverageMaintenanceCodeField = value;
                }
            }

            /// <remarks/>
            public string insuranceLineCode
            {
                get
                {
                    return this.insuranceLineCodeField;
                }
                set
                {
                    this.insuranceLineCodeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlArrayItemAttribute("benefitNetworkName", IsNullable = false)]
            public string[] benefitNetworks
            {
                get
                {
                    return this.benefitNetworksField;
                }
                set
                {
                    this.benefitNetworksField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPlanSelectionEnrollmentInfo
        {

            private string applicationSignatureDomainField;

            private string applicationSignatureDateField;

            private string applicationReceiptDateField;

            private string completedDateField;

            private string submitDateField;

            private string disenrollDateField;

            private string disenrollReasonField;

            private memberPlanSelectionEnrollmentInfoMedicareEnrollmentPeriodCode medicareEnrollmentPeriodCodeField;

            /// <remarks/>
            public string applicationSignatureDomain
            {
                get
                {
                    return this.applicationSignatureDomainField;
                }
                set
                {
                    this.applicationSignatureDomainField = value;
                }
            }

            /// <remarks/>
            public string applicationSignatureDate
            {
                get
                {
                    return this.applicationSignatureDateField;
                }
                set
                {
                    this.applicationSignatureDateField = value;
                }
            }

            /// <remarks/>
            public string applicationReceiptDate
            {
                get
                {
                    return this.applicationReceiptDateField;
                }
                set
                {
                    this.applicationReceiptDateField = value;
                }
            }

            /// <remarks/>
            public string completedDate
            {
                get
                {
                    return this.completedDateField;
                }
                set
                {
                    this.completedDateField = value;
                }
            }

            /// <remarks/>
            public string submitDate
            {
                get
                {
                    return this.submitDateField;
                }
                set
                {
                    this.submitDateField = value;
                }
            }

            /// <remarks/>
            public string disenrollDate
            {
                get
                {
                    return this.disenrollDateField;
                }
                set
                {
                    this.disenrollDateField = value;
                }
            }

            /// <remarks/>
            public string disenrollReason
            {
                get
                {
                    return this.disenrollReasonField;
                }
                set
                {
                    this.disenrollReasonField = value;
                }
            }

            /// <remarks/>
            public memberPlanSelectionEnrollmentInfoMedicareEnrollmentPeriodCode medicareEnrollmentPeriodCode
            {
                get
                {
                    return this.medicareEnrollmentPeriodCodeField;
                }
                set
                {
                    this.medicareEnrollmentPeriodCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPlanSelectionEnrollmentInfoMedicareEnrollmentPeriodCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPlanSelectionMemberTerminateReasonCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPlanSelectionSpecialEnrollmentPeriodReasonCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPlanSelectionBenefitPlanMatchData
        {

            private string benefitPlanNameField;

            private string benefitPlanHccIdField;

            private memberPlanSelectionBenefitPlanMatchDataBenefitPlanOtherIdentifier benefitPlanOtherIdentifierField;

            private string asOfDateField;

            /// <remarks/>
            public string benefitPlanName
            {
                get
                {
                    return this.benefitPlanNameField;
                }
                set
                {
                    this.benefitPlanNameField = value;
                }
            }

            /// <remarks/>
            public string benefitPlanHccId
            {
                get
                {
                    return this.benefitPlanHccIdField;
                }
                set
                {
                    this.benefitPlanHccIdField = value;
                }
            }

            /// <remarks/>
            public memberPlanSelectionBenefitPlanMatchDataBenefitPlanOtherIdentifier benefitPlanOtherIdentifier
            {
                get
                {
                    return this.benefitPlanOtherIdentifierField;
                }
                set
                {
                    this.benefitPlanOtherIdentifierField = value;
                }
            }

            /// <remarks/>
            public string asOfDate
            {
                get
                {
                    return this.asOfDateField;
                }
                set
                {
                    this.asOfDateField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPlanSelectionBenefitPlanMatchDataBenefitPlanOtherIdentifier
        {

            private string otherIDField;

            private memberPlanSelectionBenefitPlanMatchDataBenefitPlanOtherIdentifierIdentificationType identificationTypeField;

            /// <remarks/>
            public string otherID
            {
                get
                {
                    return this.otherIDField;
                }
                set
                {
                    this.otherIDField = value;
                }
            }

            /// <remarks/>
            public memberPlanSelectionBenefitPlanMatchDataBenefitPlanOtherIdentifierIdentificationType identificationType
            {
                get
                {
                    return this.identificationTypeField;
                }
                set
                {
                    this.identificationTypeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPlanSelectionBenefitPlanMatchDataBenefitPlanOtherIdentifierIdentificationType
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberPlanSelectionBenefitPlanReference
        {

            private string identifierField;

            /// <remarks/>
            public string identifier
            {
                get
                {
                    return this.identifierField;
                }
                set
                {
                    this.identifierField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberProviderSelections
        {

            private string listModeField;

            private memberProviderSelectionsProviderSelection[] providerSelectionField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("providerSelection")]
            public memberProviderSelectionsProviderSelection[] providerSelection
            {
                get
                {
                    return this.providerSelectionField;
                }
                set
                {
                    this.providerSelectionField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberProviderSelectionsProviderSelection
        {

            private string providerRoleTypeField;

            private memberProviderSelectionsProviderSelectionProviderDateRanges[] providerDateRangesField;

            /// <remarks/>
            public string providerRoleType
            {
                get
                {
                    return this.providerRoleTypeField;
                }
                set
                {
                    this.providerRoleTypeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("providerDateRanges")]
            public memberProviderSelectionsProviderSelectionProviderDateRanges[] providerDateRanges
            {
                get
                {
                    return this.providerDateRangesField;
                }
                set
                {
                    this.providerDateRangesField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberProviderSelectionsProviderSelectionProviderDateRanges
        {

            private string startDateField;

            private string endDateField;

            private memberProviderSelectionsProviderSelectionProviderDateRangesProviderMatch providerMatchField;

            private memberProviderSelectionsProviderSelectionProviderDateRangesPractitionerReference practitionerReferenceField;

            private memberProviderSelectionsProviderSelectionProviderDateRangesPractitionerRoleReference practitionerRoleReferenceField;

            private memberProviderSelectionsProviderSelectionProviderDateRangesSupplierReference supplierReferenceField;

            private memberProviderSelectionsProviderSelectionProviderDateRangesAffiliatedSupplierNetworkReference affiliatedSupplierNetworkReferenceField;

            private memberProviderSelectionsProviderSelectionProviderDateRangesSupplierLocationReference supplierLocationReferenceField;

            private memberProviderSelectionsProviderSelectionProviderDateRangesSupplierNetworkReference supplierNetworkReferenceField;

            private bool pcpAutoAssignedField;

            private memberProviderSelectionsProviderSelectionProviderDateRangesProviderSelectionReason providerSelectionReasonField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }

            /// <remarks/>
            public memberProviderSelectionsProviderSelectionProviderDateRangesProviderMatch providerMatch
            {
                get
                {
                    return this.providerMatchField;
                }
                set
                {
                    this.providerMatchField = value;
                }
            }

            /// <remarks/>
            public memberProviderSelectionsProviderSelectionProviderDateRangesPractitionerReference practitionerReference
            {
                get
                {
                    return this.practitionerReferenceField;
                }
                set
                {
                    this.practitionerReferenceField = value;
                }
            }

            /// <remarks/>
            public memberProviderSelectionsProviderSelectionProviderDateRangesPractitionerRoleReference practitionerRoleReference
            {
                get
                {
                    return this.practitionerRoleReferenceField;
                }
                set
                {
                    this.practitionerRoleReferenceField = value;
                }
            }

            /// <remarks/>
            public memberProviderSelectionsProviderSelectionProviderDateRangesSupplierReference supplierReference
            {
                get
                {
                    return this.supplierReferenceField;
                }
                set
                {
                    this.supplierReferenceField = value;
                }
            }

            /// <remarks/>
            public memberProviderSelectionsProviderSelectionProviderDateRangesAffiliatedSupplierNetworkReference affiliatedSupplierNetworkReference
            {
                get
                {
                    return this.affiliatedSupplierNetworkReferenceField;
                }
                set
                {
                    this.affiliatedSupplierNetworkReferenceField = value;
                }
            }

            /// <remarks/>
            public memberProviderSelectionsProviderSelectionProviderDateRangesSupplierLocationReference supplierLocationReference
            {
                get
                {
                    return this.supplierLocationReferenceField;
                }
                set
                {
                    this.supplierLocationReferenceField = value;
                }
            }

            /// <remarks/>
            public memberProviderSelectionsProviderSelectionProviderDateRangesSupplierNetworkReference supplierNetworkReference
            {
                get
                {
                    return this.supplierNetworkReferenceField;
                }
                set
                {
                    this.supplierNetworkReferenceField = value;
                }
            }

            /// <remarks/>
            public bool pcpAutoAssigned
            {
                get
                {
                    return this.pcpAutoAssignedField;
                }
                set
                {
                    this.pcpAutoAssignedField = value;
                }
            }

            /// <remarks/>
            public memberProviderSelectionsProviderSelectionProviderDateRangesProviderSelectionReason providerSelectionReason
            {
                get
                {
                    return this.providerSelectionReasonField;
                }
                set
                {
                    this.providerSelectionReasonField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberProviderSelectionsProviderSelectionProviderDateRangesProviderMatch
        {

            private string asOfDateField;

            private memberProviderSelectionsProviderSelectionProviderDateRangesProviderMatchPractitioner practitionerField;

            private memberProviderSelectionsProviderSelectionProviderDateRangesProviderMatchPractitionerRole practitionerRoleField;

            private memberProviderSelectionsProviderSelectionProviderDateRangesProviderMatchSupplier supplierField;

            private memberProviderSelectionsProviderSelectionProviderDateRangesProviderMatchAffiliatedSupplierNetwork affiliatedSupplierNetworkField;

            private memberProviderSelectionsProviderSelectionProviderDateRangesProviderMatchSupplierLocation supplierLocationField;

            private memberProviderSelectionsProviderSelectionProviderDateRangesProviderMatchSupplierNetwork supplierNetworkField;

            /// <remarks/>
            public string asOfDate
            {
                get
                {
                    return this.asOfDateField;
                }
                set
                {
                    this.asOfDateField = value;
                }
            }

            /// <remarks/>
            public memberProviderSelectionsProviderSelectionProviderDateRangesProviderMatchPractitioner practitioner
            {
                get
                {
                    return this.practitionerField;
                }
                set
                {
                    this.practitionerField = value;
                }
            }

            /// <remarks/>
            public memberProviderSelectionsProviderSelectionProviderDateRangesProviderMatchPractitionerRole practitionerRole
            {
                get
                {
                    return this.practitionerRoleField;
                }
                set
                {
                    this.practitionerRoleField = value;
                }
            }

            /// <remarks/>
            public memberProviderSelectionsProviderSelectionProviderDateRangesProviderMatchSupplier supplier
            {
                get
                {
                    return this.supplierField;
                }
                set
                {
                    this.supplierField = value;
                }
            }

            /// <remarks/>
            public memberProviderSelectionsProviderSelectionProviderDateRangesProviderMatchAffiliatedSupplierNetwork affiliatedSupplierNetwork
            {
                get
                {
                    return this.affiliatedSupplierNetworkField;
                }
                set
                {
                    this.affiliatedSupplierNetworkField = value;
                }
            }

            /// <remarks/>
            public memberProviderSelectionsProviderSelectionProviderDateRangesProviderMatchSupplierLocation supplierLocation
            {
                get
                {
                    return this.supplierLocationField;
                }
                set
                {
                    this.supplierLocationField = value;
                }
            }

            /// <remarks/>
            public memberProviderSelectionsProviderSelectionProviderDateRangesProviderMatchSupplierNetwork supplierNetwork
            {
                get
                {
                    return this.supplierNetworkField;
                }
                set
                {
                    this.supplierNetworkField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberProviderSelectionsProviderSelectionProviderDateRangesProviderMatchPractitioner
        {

            private string practitionerOtherIdentificationNumberField;

            private string firstNameField;

            private string lastNameField;

            private string hccIdentificationNumberField;

            /// <remarks/>
            public string practitionerOtherIdentificationNumber
            {
                get
                {
                    return this.practitionerOtherIdentificationNumberField;
                }
                set
                {
                    this.practitionerOtherIdentificationNumberField = value;
                }
            }

            /// <remarks/>
            public string firstName
            {
                get
                {
                    return this.firstNameField;
                }
                set
                {
                    this.firstNameField = value;
                }
            }

            /// <remarks/>
            public string lastName
            {
                get
                {
                    return this.lastNameField;
                }
                set
                {
                    this.lastNameField = value;
                }
            }

            /// <remarks/>
            public string hccIdentificationNumber
            {
                get
                {
                    return this.hccIdentificationNumberField;
                }
                set
                {
                    this.hccIdentificationNumberField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberProviderSelectionsProviderSelectionProviderDateRangesProviderMatchPractitionerRole
        {

            private string hccIdentificationNumberField;

            private string practitionerRoleIdentificationNumberField;

            private string roleNameField;

            private string practitionerRoleOtherIdentificationNumberField;

            private memberProviderSelectionsProviderSelectionProviderDateRangesProviderMatchPractitionerRolePractitionerRoleOtherIdTypeCode practitionerRoleOtherIdTypeCodeField;

            /// <remarks/>
            public string hccIdentificationNumber
            {
                get
                {
                    return this.hccIdentificationNumberField;
                }
                set
                {
                    this.hccIdentificationNumberField = value;
                }
            }

            /// <remarks/>
            public string practitionerRoleIdentificationNumber
            {
                get
                {
                    return this.practitionerRoleIdentificationNumberField;
                }
                set
                {
                    this.practitionerRoleIdentificationNumberField = value;
                }
            }

            /// <remarks/>
            public string roleName
            {
                get
                {
                    return this.roleNameField;
                }
                set
                {
                    this.roleNameField = value;
                }
            }

            /// <remarks/>
            public string practitionerRoleOtherIdentificationNumber
            {
                get
                {
                    return this.practitionerRoleOtherIdentificationNumberField;
                }
                set
                {
                    this.practitionerRoleOtherIdentificationNumberField = value;
                }
            }

            /// <remarks/>
            public memberProviderSelectionsProviderSelectionProviderDateRangesProviderMatchPractitionerRolePractitionerRoleOtherIdTypeCode practitionerRoleOtherIdTypeCode
            {
                get
                {
                    return this.practitionerRoleOtherIdTypeCodeField;
                }
                set
                {
                    this.practitionerRoleOtherIdTypeCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberProviderSelectionsProviderSelectionProviderDateRangesProviderMatchPractitionerRolePractitionerRoleOtherIdTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberProviderSelectionsProviderSelectionProviderDateRangesProviderMatchSupplier
        {

            private string supplierOtherIdentificationNumberField;

            private string primaryNameField;

            private string hccIdentificationNumberField;

            /// <remarks/>
            public string supplierOtherIdentificationNumber
            {
                get
                {
                    return this.supplierOtherIdentificationNumberField;
                }
                set
                {
                    this.supplierOtherIdentificationNumberField = value;
                }
            }

            /// <remarks/>
            public string primaryName
            {
                get
                {
                    return this.primaryNameField;
                }
                set
                {
                    this.primaryNameField = value;
                }
            }

            /// <remarks/>
            public string hccIdentificationNumber
            {
                get
                {
                    return this.hccIdentificationNumberField;
                }
                set
                {
                    this.hccIdentificationNumberField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberProviderSelectionsProviderSelectionProviderDateRangesProviderMatchAffiliatedSupplierNetwork
        {

            private string hccIdentificationNumberField;

            /// <remarks/>
            public string hccIdentificationNumber
            {
                get
                {
                    return this.hccIdentificationNumberField;
                }
                set
                {
                    this.hccIdentificationNumberField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberProviderSelectionsProviderSelectionProviderDateRangesProviderMatchSupplierLocation
        {

            private string hccIdentificationNumberField;

            /// <remarks/>
            public string hccIdentificationNumber
            {
                get
                {
                    return this.hccIdentificationNumberField;
                }
                set
                {
                    this.hccIdentificationNumberField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberProviderSelectionsProviderSelectionProviderDateRangesProviderMatchSupplierNetwork
        {

            private string hccIdentificationNumberField;

            /// <remarks/>
            public string hccIdentificationNumber
            {
                get
                {
                    return this.hccIdentificationNumberField;
                }
                set
                {
                    this.hccIdentificationNumberField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberProviderSelectionsProviderSelectionProviderDateRangesPractitionerReference
        {

            private string identifierField;

            /// <remarks/>
            public string identifier
            {
                get
                {
                    return this.identifierField;
                }
                set
                {
                    this.identifierField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberProviderSelectionsProviderSelectionProviderDateRangesPractitionerRoleReference
        {

            private string identifierField;

            /// <remarks/>
            public string identifier
            {
                get
                {
                    return this.identifierField;
                }
                set
                {
                    this.identifierField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberProviderSelectionsProviderSelectionProviderDateRangesSupplierReference
        {

            private string identifierField;

            /// <remarks/>
            public string identifier
            {
                get
                {
                    return this.identifierField;
                }
                set
                {
                    this.identifierField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberProviderSelectionsProviderSelectionProviderDateRangesAffiliatedSupplierNetworkReference
        {

            private string identifierField;

            /// <remarks/>
            public string identifier
            {
                get
                {
                    return this.identifierField;
                }
                set
                {
                    this.identifierField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberProviderSelectionsProviderSelectionProviderDateRangesSupplierLocationReference
        {

            private string identifierField;

            /// <remarks/>
            public string identifier
            {
                get
                {
                    return this.identifierField;
                }
                set
                {
                    this.identifierField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberProviderSelectionsProviderSelectionProviderDateRangesSupplierNetworkReference
        {

            private string identifierField;

            /// <remarks/>
            public string identifier
            {
                get
                {
                    return this.identifierField;
                }
                set
                {
                    this.identifierField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberProviderSelectionsProviderSelectionProviderDateRangesProviderSelectionReason
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberReportingCategory
        {

            private string entityIdentifierCodeField;

            private string reportingCategoryNameField;

            private string referenceQualifierIdentifierField;

            private string referenceIdField;

            private string amountField;

            private string startDateField;

            private string endDateField;

            private string counterField;

            /// <remarks/>
            public string entityIdentifierCode
            {
                get
                {
                    return this.entityIdentifierCodeField;
                }
                set
                {
                    this.entityIdentifierCodeField = value;
                }
            }

            /// <remarks/>
            public string reportingCategoryName
            {
                get
                {
                    return this.reportingCategoryNameField;
                }
                set
                {
                    this.reportingCategoryNameField = value;
                }
            }

            /// <remarks/>
            public string referenceQualifierIdentifier
            {
                get
                {
                    return this.referenceQualifierIdentifierField;
                }
                set
                {
                    this.referenceQualifierIdentifierField = value;
                }
            }

            /// <remarks/>
            public string referenceId
            {
                get
                {
                    return this.referenceIdField;
                }
                set
                {
                    this.referenceIdField = value;
                }
            }

            /// <remarks/>
            public string amount
            {
                get
                {
                    return this.amountField;
                }
                set
                {
                    this.amountField = value;
                }
            }

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string counter
            {
                get
                {
                    return this.counterField;
                }
                set
                {
                    this.counterField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICN
        {

            private string hicnField;

            private string mbiField;

            private string effectiveDateField;

            private string expirationDateField;

            private string entryDateField;

            private string waitingPeriodStartDateField;

            private string waitingPeriodEndDateField;

            private string nursingHomeCompareIndicatorField;

            private string stateMedicaidEnrollmentField;

            private memberMedicareHICNStateMedicaidEnrollments stateMedicaidEnrollmentsField;

            private string workingSpouseField;

            private memberMedicareHICNWorkingSpouses workingSpousesField;

            private string longTermCareResidentField;

            private memberMedicareHICNDialysisDateRanges dialysisDateRangesField;

            private memberMedicareHICNTransplantDateRanges transplantDateRangesField;

            private string esrdIndicatorField;

            private memberMedicareHICNEsrdDateRanges esrdDateRangesField;

            private string hospiceIndicatorField;

            private memberMedicareHICNHospiceDateRanges hospiceDateRangesField;

            private string institutionalIndicatorField;

            private memberMedicareHICNInstitutionalDateRanges institutionalDateRangesField;

            private memberMedicareHICNMedicaidDateRanges medicaidDateRangesField;

            private memberMedicareHICNNhcDateRanges nhcDateRangesField;

            private memberMedicareHICNPartADates partADatesField;

            private memberMedicareHICNPartBDates partBDatesField;

            private memberMedicareHICNPremiumWithholdDateRanges premiumWithholdDateRangesField;

            private memberMedicareHICNSupplementalDateRanges supplementalDateRangesField;

            private memberMedicareHICNPartDDateRanges partDDateRangesField;

            private memberMedicareHICNMedicareEligibilityReasonCode medicareEligibilityReasonCodeField;

            private memberMedicareHICNMedicareHICNFields medicareHICNFieldsField;

            private memberMedicareHICNMedicaidDualStatusRanges medicaidDualStatusRangesField;

            private string resourceURIField;

            private string versionField;

            /// <remarks/>
            public string hicn
            {
                get
                {
                    return this.hicnField;
                }
                set
                {
                    this.hicnField = value;
                }
            }

            /// <remarks/>
            public string mbi
            {
                get
                {
                    return this.mbiField;
                }
                set
                {
                    this.mbiField = value;
                }
            }

            /// <remarks/>
            public string effectiveDate
            {
                get
                {
                    return this.effectiveDateField;
                }
                set
                {
                    this.effectiveDateField = value;
                }
            }

            /// <remarks/>
            public string expirationDate
            {
                get
                {
                    return this.expirationDateField;
                }
                set
                {
                    this.expirationDateField = value;
                }
            }

            /// <remarks/>
            public string entryDate
            {
                get
                {
                    return this.entryDateField;
                }
                set
                {
                    this.entryDateField = value;
                }
            }

            /// <remarks/>
            public string waitingPeriodStartDate
            {
                get
                {
                    return this.waitingPeriodStartDateField;
                }
                set
                {
                    this.waitingPeriodStartDateField = value;
                }
            }

            /// <remarks/>
            public string waitingPeriodEndDate
            {
                get
                {
                    return this.waitingPeriodEndDateField;
                }
                set
                {
                    this.waitingPeriodEndDateField = value;
                }
            }

            /// <remarks/>
            public string nursingHomeCompareIndicator
            {
                get
                {
                    return this.nursingHomeCompareIndicatorField;
                }
                set
                {
                    this.nursingHomeCompareIndicatorField = value;
                }
            }

            /// <remarks/>
            public string stateMedicaidEnrollment
            {
                get
                {
                    return this.stateMedicaidEnrollmentField;
                }
                set
                {
                    this.stateMedicaidEnrollmentField = value;
                }
            }

            /// <remarks/>
            public memberMedicareHICNStateMedicaidEnrollments stateMedicaidEnrollments
            {
                get
                {
                    return this.stateMedicaidEnrollmentsField;
                }
                set
                {
                    this.stateMedicaidEnrollmentsField = value;
                }
            }

            /// <remarks/>
            public string workingSpouse
            {
                get
                {
                    return this.workingSpouseField;
                }
                set
                {
                    this.workingSpouseField = value;
                }
            }

            /// <remarks/>
            public memberMedicareHICNWorkingSpouses workingSpouses
            {
                get
                {
                    return this.workingSpousesField;
                }
                set
                {
                    this.workingSpousesField = value;
                }
            }

            /// <remarks/>
            public string longTermCareResident
            {
                get
                {
                    return this.longTermCareResidentField;
                }
                set
                {
                    this.longTermCareResidentField = value;
                }
            }

            /// <remarks/>
            public memberMedicareHICNDialysisDateRanges dialysisDateRanges
            {
                get
                {
                    return this.dialysisDateRangesField;
                }
                set
                {
                    this.dialysisDateRangesField = value;
                }
            }

            /// <remarks/>
            public memberMedicareHICNTransplantDateRanges transplantDateRanges
            {
                get
                {
                    return this.transplantDateRangesField;
                }
                set
                {
                    this.transplantDateRangesField = value;
                }
            }

            /// <remarks/>
            public string esrdIndicator
            {
                get
                {
                    return this.esrdIndicatorField;
                }
                set
                {
                    this.esrdIndicatorField = value;
                }
            }

            /// <remarks/>
            public memberMedicareHICNEsrdDateRanges esrdDateRanges
            {
                get
                {
                    return this.esrdDateRangesField;
                }
                set
                {
                    this.esrdDateRangesField = value;
                }
            }

            /// <remarks/>
            public string hospiceIndicator
            {
                get
                {
                    return this.hospiceIndicatorField;
                }
                set
                {
                    this.hospiceIndicatorField = value;
                }
            }

            /// <remarks/>
            public memberMedicareHICNHospiceDateRanges hospiceDateRanges
            {
                get
                {
                    return this.hospiceDateRangesField;
                }
                set
                {
                    this.hospiceDateRangesField = value;
                }
            }

            /// <remarks/>
            public string institutionalIndicator
            {
                get
                {
                    return this.institutionalIndicatorField;
                }
                set
                {
                    this.institutionalIndicatorField = value;
                }
            }

            /// <remarks/>
            public memberMedicareHICNInstitutionalDateRanges institutionalDateRanges
            {
                get
                {
                    return this.institutionalDateRangesField;
                }
                set
                {
                    this.institutionalDateRangesField = value;
                }
            }

            /// <remarks/>
            public memberMedicareHICNMedicaidDateRanges medicaidDateRanges
            {
                get
                {
                    return this.medicaidDateRangesField;
                }
                set
                {
                    this.medicaidDateRangesField = value;
                }
            }

            /// <remarks/>
            public memberMedicareHICNNhcDateRanges nhcDateRanges
            {
                get
                {
                    return this.nhcDateRangesField;
                }
                set
                {
                    this.nhcDateRangesField = value;
                }
            }

            /// <remarks/>
            public memberMedicareHICNPartADates partADates
            {
                get
                {
                    return this.partADatesField;
                }
                set
                {
                    this.partADatesField = value;
                }
            }

            /// <remarks/>
            public memberMedicareHICNPartBDates partBDates
            {
                get
                {
                    return this.partBDatesField;
                }
                set
                {
                    this.partBDatesField = value;
                }
            }

            /// <remarks/>
            public memberMedicareHICNPremiumWithholdDateRanges premiumWithholdDateRanges
            {
                get
                {
                    return this.premiumWithholdDateRangesField;
                }
                set
                {
                    this.premiumWithholdDateRangesField = value;
                }
            }

            /// <remarks/>
            public memberMedicareHICNSupplementalDateRanges supplementalDateRanges
            {
                get
                {
                    return this.supplementalDateRangesField;
                }
                set
                {
                    this.supplementalDateRangesField = value;
                }
            }

            /// <remarks/>
            public memberMedicareHICNPartDDateRanges partDDateRanges
            {
                get
                {
                    return this.partDDateRangesField;
                }
                set
                {
                    this.partDDateRangesField = value;
                }
            }

            /// <remarks/>
            public memberMedicareHICNMedicareEligibilityReasonCode medicareEligibilityReasonCode
            {
                get
                {
                    return this.medicareEligibilityReasonCodeField;
                }
                set
                {
                    this.medicareEligibilityReasonCodeField = value;
                }
            }

            /// <remarks/>
            public memberMedicareHICNMedicareHICNFields medicareHICNFields
            {
                get
                {
                    return this.medicareHICNFieldsField;
                }
                set
                {
                    this.medicareHICNFieldsField = value;
                }
            }

            /// <remarks/>
            public memberMedicareHICNMedicaidDualStatusRanges medicaidDualStatusRanges
            {
                get
                {
                    return this.medicaidDualStatusRangesField;
                }
                set
                {
                    this.medicaidDualStatusRangesField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string resourceURI
            {
                get
                {
                    return this.resourceURIField;
                }
                set
                {
                    this.resourceURIField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string version
            {
                get
                {
                    return this.versionField;
                }
                set
                {
                    this.versionField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNStateMedicaidEnrollments
        {

            private string updateModeField;

            private memberMedicareHICNStateMedicaidEnrollmentsStateMedicaidEnrollment[] stateMedicaidEnrollmentField;

            /// <remarks/>
            public string updateMode
            {
                get
                {
                    return this.updateModeField;
                }
                set
                {
                    this.updateModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("stateMedicaidEnrollment")]
            public memberMedicareHICNStateMedicaidEnrollmentsStateMedicaidEnrollment[] stateMedicaidEnrollment
            {
                get
                {
                    return this.stateMedicaidEnrollmentField;
                }
                set
                {
                    this.stateMedicaidEnrollmentField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNStateMedicaidEnrollmentsStateMedicaidEnrollment
        {

            private string startDateField;

            private string endDateField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNWorkingSpouses
        {

            private string updateModeField;

            private memberMedicareHICNWorkingSpousesWorkingSpouse[] workingSpouseField;

            /// <remarks/>
            public string updateMode
            {
                get
                {
                    return this.updateModeField;
                }
                set
                {
                    this.updateModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("workingSpouse")]
            public memberMedicareHICNWorkingSpousesWorkingSpouse[] workingSpouse
            {
                get
                {
                    return this.workingSpouseField;
                }
                set
                {
                    this.workingSpouseField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNWorkingSpousesWorkingSpouse
        {

            private string startDateField;

            private string endDateField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNDialysisDateRanges
        {

            private string updateModeField;

            private memberMedicareHICNDialysisDateRangesDialysisDateRange[] dialysisDateRangeField;

            /// <remarks/>
            public string updateMode
            {
                get
                {
                    return this.updateModeField;
                }
                set
                {
                    this.updateModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("dialysisDateRange")]
            public memberMedicareHICNDialysisDateRangesDialysisDateRange[] dialysisDateRange
            {
                get
                {
                    return this.dialysisDateRangeField;
                }
                set
                {
                    this.dialysisDateRangeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNDialysisDateRangesDialysisDateRange
        {

            private string startDateField;

            private string endDateField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNTransplantDateRanges
        {

            private string updateModeField;

            private memberMedicareHICNTransplantDateRangesTransplantDateRange[] transplantDateRangeField;

            /// <remarks/>
            public string updateMode
            {
                get
                {
                    return this.updateModeField;
                }
                set
                {
                    this.updateModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("transplantDateRange")]
            public memberMedicareHICNTransplantDateRangesTransplantDateRange[] transplantDateRange
            {
                get
                {
                    return this.transplantDateRangeField;
                }
                set
                {
                    this.transplantDateRangeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNTransplantDateRangesTransplantDateRange
        {

            private string startDateField;

            private string endDateField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNEsrdDateRanges
        {

            private string updateModeField;

            private memberMedicareHICNEsrdDateRangesEsrdDateRange[] esrdDateRangeField;

            /// <remarks/>
            public string updateMode
            {
                get
                {
                    return this.updateModeField;
                }
                set
                {
                    this.updateModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("esrdDateRange")]
            public memberMedicareHICNEsrdDateRangesEsrdDateRange[] esrdDateRange
            {
                get
                {
                    return this.esrdDateRangeField;
                }
                set
                {
                    this.esrdDateRangeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNEsrdDateRangesEsrdDateRange
        {

            private string startDateField;

            private string endDateField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNHospiceDateRanges
        {

            private string updateModeField;

            private memberMedicareHICNHospiceDateRangesHospiceDateRange[] hospiceDateRangeField;

            /// <remarks/>
            public string updateMode
            {
                get
                {
                    return this.updateModeField;
                }
                set
                {
                    this.updateModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("hospiceDateRange")]
            public memberMedicareHICNHospiceDateRangesHospiceDateRange[] hospiceDateRange
            {
                get
                {
                    return this.hospiceDateRangeField;
                }
                set
                {
                    this.hospiceDateRangeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNHospiceDateRangesHospiceDateRange
        {

            private string startDateField;

            private string endDateField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNInstitutionalDateRanges
        {

            private string updateModeField;

            private memberMedicareHICNInstitutionalDateRangesInstitutionalDateRange[] institutionalDateRangeField;

            /// <remarks/>
            public string updateMode
            {
                get
                {
                    return this.updateModeField;
                }
                set
                {
                    this.updateModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("institutionalDateRange")]
            public memberMedicareHICNInstitutionalDateRangesInstitutionalDateRange[] institutionalDateRange
            {
                get
                {
                    return this.institutionalDateRangeField;
                }
                set
                {
                    this.institutionalDateRangeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNInstitutionalDateRangesInstitutionalDateRange
        {

            private string startDateField;

            private string endDateField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNMedicaidDateRanges
        {

            private string updateModeField;

            private memberMedicareHICNMedicaidDateRangesMedicaidDateRange[] medicaidDateRangeField;

            /// <remarks/>
            public string updateMode
            {
                get
                {
                    return this.updateModeField;
                }
                set
                {
                    this.updateModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("medicaidDateRange")]
            public memberMedicareHICNMedicaidDateRangesMedicaidDateRange[] medicaidDateRange
            {
                get
                {
                    return this.medicaidDateRangeField;
                }
                set
                {
                    this.medicaidDateRangeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNMedicaidDateRangesMedicaidDateRange
        {

            private string startDateField;

            private string endDateField;

            private string medicaidRecertificationDateField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }

            /// <remarks/>
            public string medicaidRecertificationDate
            {
                get
                {
                    return this.medicaidRecertificationDateField;
                }
                set
                {
                    this.medicaidRecertificationDateField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNNhcDateRanges
        {

            private string updateModeField;

            private memberMedicareHICNNhcDateRangesNhcDateRange[] nhcDateRangeField;

            /// <remarks/>
            public string updateMode
            {
                get
                {
                    return this.updateModeField;
                }
                set
                {
                    this.updateModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("nhcDateRange")]
            public memberMedicareHICNNhcDateRangesNhcDateRange[] nhcDateRange
            {
                get
                {
                    return this.nhcDateRangeField;
                }
                set
                {
                    this.nhcDateRangeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNNhcDateRangesNhcDateRange
        {

            private string startDateField;

            private string endDateField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNPartADates
        {

            private string updateModeField;

            private memberMedicareHICNPartADatesPartADate[] partADateField;

            /// <remarks/>
            public string updateMode
            {
                get
                {
                    return this.updateModeField;
                }
                set
                {
                    this.updateModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("partADate")]
            public memberMedicareHICNPartADatesPartADate[] partADate
            {
                get
                {
                    return this.partADateField;
                }
                set
                {
                    this.partADateField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNPartADatesPartADate
        {

            private string effectiveDateField;

            private string terminationDateField;

            /// <remarks/>
            public string effectiveDate
            {
                get
                {
                    return this.effectiveDateField;
                }
                set
                {
                    this.effectiveDateField = value;
                }
            }

            /// <remarks/>
            public string terminationDate
            {
                get
                {
                    return this.terminationDateField;
                }
                set
                {
                    this.terminationDateField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNPartBDates
        {

            private string updateModeField;

            private memberMedicareHICNPartBDatesPartBDate[] partBDateField;

            /// <remarks/>
            public string updateMode
            {
                get
                {
                    return this.updateModeField;
                }
                set
                {
                    this.updateModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("partBDate")]
            public memberMedicareHICNPartBDatesPartBDate[] partBDate
            {
                get
                {
                    return this.partBDateField;
                }
                set
                {
                    this.partBDateField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNPartBDatesPartBDate
        {

            private string effectiveDateField;

            private string terminationDateField;

            /// <remarks/>
            public string effectiveDate
            {
                get
                {
                    return this.effectiveDateField;
                }
                set
                {
                    this.effectiveDateField = value;
                }
            }

            /// <remarks/>
            public string terminationDate
            {
                get
                {
                    return this.terminationDateField;
                }
                set
                {
                    this.terminationDateField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNPremiumWithholdDateRanges
        {

            private string updateModeField;

            private memberMedicareHICNPremiumWithholdDateRangesPremiumWithholdDateRange[] premiumWithholdDateRangeField;

            /// <remarks/>
            public string updateMode
            {
                get
                {
                    return this.updateModeField;
                }
                set
                {
                    this.updateModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("premiumWithholdDateRange")]
            public memberMedicareHICNPremiumWithholdDateRangesPremiumWithholdDateRange[] premiumWithholdDateRange
            {
                get
                {
                    return this.premiumWithholdDateRangeField;
                }
                set
                {
                    this.premiumWithholdDateRangeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNPremiumWithholdDateRangesPremiumWithholdDateRange
        {

            private string startDateField;

            private string endDateField;

            private string premiumWithholdFlagField;

            private string premiumWithholdPaymentMethodTypeField;

            private string premiumWithholdOptionField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }

            /// <remarks/>
            public string premiumWithholdFlag
            {
                get
                {
                    return this.premiumWithholdFlagField;
                }
                set
                {
                    this.premiumWithholdFlagField = value;
                }
            }

            /// <remarks/>
            public string premiumWithholdPaymentMethodType
            {
                get
                {
                    return this.premiumWithholdPaymentMethodTypeField;
                }
                set
                {
                    this.premiumWithholdPaymentMethodTypeField = value;
                }
            }

            /// <remarks/>
            public string premiumWithholdOption
            {
                get
                {
                    return this.premiumWithholdOptionField;
                }
                set
                {
                    this.premiumWithholdOptionField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNSupplementalDateRanges
        {

            private string updateModeField;

            private memberMedicareHICNSupplementalDateRangesSupplementalDateRange[] supplementalDateRangeField;

            /// <remarks/>
            public string updateMode
            {
                get
                {
                    return this.updateModeField;
                }
                set
                {
                    this.updateModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("supplementalDateRange")]
            public memberMedicareHICNSupplementalDateRangesSupplementalDateRange[] supplementalDateRange
            {
                get
                {
                    return this.supplementalDateRangeField;
                }
                set
                {
                    this.supplementalDateRangeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNSupplementalDateRangesSupplementalDateRange
        {

            private string startDateField;

            private string endDateField;

            private string personCodeField;

            private string supplementalTypeCodeField;

            private string supplementalRxCoverageField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }

            /// <remarks/>
            public string personCode
            {
                get
                {
                    return this.personCodeField;
                }
                set
                {
                    this.personCodeField = value;
                }
            }

            /// <remarks/>
            public string supplementalTypeCode
            {
                get
                {
                    return this.supplementalTypeCodeField;
                }
                set
                {
                    this.supplementalTypeCodeField = value;
                }
            }

            /// <remarks/>
            public string supplementalRxCoverage
            {
                get
                {
                    return this.supplementalRxCoverageField;
                }
                set
                {
                    this.supplementalRxCoverageField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNPartDDateRanges
        {

            private string updateModeField;

            private memberMedicareHICNPartDDateRangesPartDDateRanges[] partDDateRangesField;

            /// <remarks/>
            public string updateMode
            {
                get
                {
                    return this.updateModeField;
                }
                set
                {
                    this.updateModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("partDDateRanges")]
            public memberMedicareHICNPartDDateRangesPartDDateRanges[] partDDateRanges
            {
                get
                {
                    return this.partDDateRangesField;
                }
                set
                {
                    this.partDDateRangesField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNPartDDateRangesPartDDateRanges
        {

            private string startDateField;

            private string endDateField;

            private string partDBestAvailableEvidenceIndicatorField;

            private string partDCreditableCoverageIndicatorField;

            private memberMedicareHICNPartDDateRangesPartDDateRangesPartDLEPDateRanges[] partDLEPDateRangesField;

            private memberMedicareHICNPartDDateRangesPartDDateRangesPartDLISCopayCategoryDates[] partDLISCopayCategoryDatesField;

            private memberMedicareHICNPartDDateRangesPartDDateRangesPartDLISSubsidyLevelDates[] partDLISSubsidyLevelDatesField;

            private memberMedicareHICNPartDDateRangesPartDDateRangesPartDCurrLISPremSubsidyDates[] partDCurrLISPremSubsidyDatesField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }

            /// <remarks/>
            public string partDBestAvailableEvidenceIndicator
            {
                get
                {
                    return this.partDBestAvailableEvidenceIndicatorField;
                }
                set
                {
                    this.partDBestAvailableEvidenceIndicatorField = value;
                }
            }

            /// <remarks/>
            public string partDCreditableCoverageIndicator
            {
                get
                {
                    return this.partDCreditableCoverageIndicatorField;
                }
                set
                {
                    this.partDCreditableCoverageIndicatorField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("partDLEPDateRanges")]
            public memberMedicareHICNPartDDateRangesPartDDateRangesPartDLEPDateRanges[] partDLEPDateRanges
            {
                get
                {
                    return this.partDLEPDateRangesField;
                }
                set
                {
                    this.partDLEPDateRangesField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("partDLISCopayCategoryDates")]
            public memberMedicareHICNPartDDateRangesPartDDateRangesPartDLISCopayCategoryDates[] partDLISCopayCategoryDates
            {
                get
                {
                    return this.partDLISCopayCategoryDatesField;
                }
                set
                {
                    this.partDLISCopayCategoryDatesField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("partDLISSubsidyLevelDates")]
            public memberMedicareHICNPartDDateRangesPartDDateRangesPartDLISSubsidyLevelDates[] partDLISSubsidyLevelDates
            {
                get
                {
                    return this.partDLISSubsidyLevelDatesField;
                }
                set
                {
                    this.partDLISSubsidyLevelDatesField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("partDCurrLISPremSubsidyDates")]
            public memberMedicareHICNPartDDateRangesPartDDateRangesPartDCurrLISPremSubsidyDates[] partDCurrLISPremSubsidyDates
            {
                get
                {
                    return this.partDCurrLISPremSubsidyDatesField;
                }
                set
                {
                    this.partDCurrLISPremSubsidyDatesField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNPartDDateRangesPartDDateRangesPartDLEPDateRanges
        {

            private string startDateField;

            private string endDateField;

            private string partDLEPAmountField;

            private string partDLEPWaivedAmountField;

            private string partDLEPSubsidyAmountField;

            private string partDLEPCurrentNumberofUncoveredMonthsField;

            private string partDLEPTotalNumberofUncoveredMonthsField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }

            /// <remarks/>
            public string partDLEPAmount
            {
                get
                {
                    return this.partDLEPAmountField;
                }
                set
                {
                    this.partDLEPAmountField = value;
                }
            }

            /// <remarks/>
            public string partDLEPWaivedAmount
            {
                get
                {
                    return this.partDLEPWaivedAmountField;
                }
                set
                {
                    this.partDLEPWaivedAmountField = value;
                }
            }

            /// <remarks/>
            public string partDLEPSubsidyAmount
            {
                get
                {
                    return this.partDLEPSubsidyAmountField;
                }
                set
                {
                    this.partDLEPSubsidyAmountField = value;
                }
            }

            /// <remarks/>
            public string partDLEPCurrentNumberofUncoveredMonths
            {
                get
                {
                    return this.partDLEPCurrentNumberofUncoveredMonthsField;
                }
                set
                {
                    this.partDLEPCurrentNumberofUncoveredMonthsField = value;
                }
            }

            /// <remarks/>
            public string partDLEPTotalNumberofUncoveredMonths
            {
                get
                {
                    return this.partDLEPTotalNumberofUncoveredMonthsField;
                }
                set
                {
                    this.partDLEPTotalNumberofUncoveredMonthsField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNPartDDateRangesPartDDateRangesPartDLISCopayCategoryDates
        {

            private string startDateField;

            private string endDateField;

            private memberMedicareHICNPartDDateRangesPartDDateRangesPartDLISCopayCategoryDatesPartDLISCopayCategoryCode partDLISCopayCategoryCodeField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }

            /// <remarks/>
            public memberMedicareHICNPartDDateRangesPartDDateRangesPartDLISCopayCategoryDatesPartDLISCopayCategoryCode partDLISCopayCategoryCode
            {
                get
                {
                    return this.partDLISCopayCategoryCodeField;
                }
                set
                {
                    this.partDLISCopayCategoryCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNPartDDateRangesPartDDateRangesPartDLISCopayCategoryDatesPartDLISCopayCategoryCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNPartDDateRangesPartDDateRangesPartDLISSubsidyLevelDates
        {

            private string startDateField;

            private string endDateField;

            private memberMedicareHICNPartDDateRangesPartDDateRangesPartDLISSubsidyLevelDatesPartDLISSubsidyLevel partDLISSubsidyLevelField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }

            /// <remarks/>
            public memberMedicareHICNPartDDateRangesPartDDateRangesPartDLISSubsidyLevelDatesPartDLISSubsidyLevel partDLISSubsidyLevel
            {
                get
                {
                    return this.partDLISSubsidyLevelField;
                }
                set
                {
                    this.partDLISSubsidyLevelField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNPartDDateRangesPartDDateRangesPartDLISSubsidyLevelDatesPartDLISSubsidyLevel
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNPartDDateRangesPartDDateRangesPartDCurrLISPremSubsidyDates
        {

            private string startDateField;

            private string endDateField;

            private string partDCurrentLISPremiumSubsidyAmountField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }

            /// <remarks/>
            public string partDCurrentLISPremiumSubsidyAmount
            {
                get
                {
                    return this.partDCurrentLISPremiumSubsidyAmountField;
                }
                set
                {
                    this.partDCurrentLISPremiumSubsidyAmountField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNMedicareEligibilityReasonCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNMedicareHICNFields
        {

            private string updateModeField;

            private memberMedicareHICNMedicareHICNFieldsMedicareHICNField[] medicareHICNFieldField;

            /// <remarks/>
            public string updateMode
            {
                get
                {
                    return this.updateModeField;
                }
                set
                {
                    this.updateModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("medicareHICNField")]
            public memberMedicareHICNMedicareHICNFieldsMedicareHICNField[] medicareHICNField
            {
                get
                {
                    return this.medicareHICNFieldField;
                }
                set
                {
                    this.medicareHICNFieldField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNMedicareHICNFieldsMedicareHICNField
        {

            private string fieldNameField;

            private string fieldValueField;

            /// <remarks/>
            public string fieldName
            {
                get
                {
                    return this.fieldNameField;
                }
                set
                {
                    this.fieldNameField = value;
                }
            }

            /// <remarks/>
            public string fieldValue
            {
                get
                {
                    return this.fieldValueField;
                }
                set
                {
                    this.fieldValueField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNMedicaidDualStatusRanges
        {

            private string updateModeField;

            private memberMedicareHICNMedicaidDualStatusRangesMedicaidDualStatus[] medicaidDualStatusField;

            /// <remarks/>
            public string updateMode
            {
                get
                {
                    return this.updateModeField;
                }
                set
                {
                    this.updateModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("medicaidDualStatus")]
            public memberMedicareHICNMedicaidDualStatusRangesMedicaidDualStatus[] medicaidDualStatus
            {
                get
                {
                    return this.medicaidDualStatusField;
                }
                set
                {
                    this.medicaidDualStatusField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNMedicaidDualStatusRangesMedicaidDualStatus
        {

            private string effectiveDateField;

            private string terminationDateField;

            private string gracePeriodStartDateField;

            private string medicaidDualField;

            private memberMedicareHICNMedicaidDualStatusRangesMedicaidDualStatusMedicaidDualStatusSourceCode medicaidDualStatusSourceCodeField;

            private memberMedicareHICNMedicaidDualStatusRangesMedicaidDualStatusMedicaidDualStatusCode medicaidDualStatusCodeField;

            /// <remarks/>
            public string effectiveDate
            {
                get
                {
                    return this.effectiveDateField;
                }
                set
                {
                    this.effectiveDateField = value;
                }
            }

            /// <remarks/>
            public string terminationDate
            {
                get
                {
                    return this.terminationDateField;
                }
                set
                {
                    this.terminationDateField = value;
                }
            }

            /// <remarks/>
            public string gracePeriodStartDate
            {
                get
                {
                    return this.gracePeriodStartDateField;
                }
                set
                {
                    this.gracePeriodStartDateField = value;
                }
            }

            /// <remarks/>
            public string medicaidDual
            {
                get
                {
                    return this.medicaidDualField;
                }
                set
                {
                    this.medicaidDualField = value;
                }
            }

            /// <remarks/>
            public memberMedicareHICNMedicaidDualStatusRangesMedicaidDualStatusMedicaidDualStatusSourceCode medicaidDualStatusSourceCode
            {
                get
                {
                    return this.medicaidDualStatusSourceCodeField;
                }
                set
                {
                    this.medicaidDualStatusSourceCodeField = value;
                }
            }

            /// <remarks/>
            public memberMedicareHICNMedicaidDualStatusRangesMedicaidDualStatusMedicaidDualStatusCode medicaidDualStatusCode
            {
                get
                {
                    return this.medicaidDualStatusCodeField;
                }
                set
                {
                    this.medicaidDualStatusCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNMedicaidDualStatusRangesMedicaidDualStatusMedicaidDualStatusSourceCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMedicareHICNMedicaidDualStatusRangesMedicaidDualStatusMedicaidDualStatusCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberCobPolicy
        {

            private string cobPolicyIdentifierField;

            private string cobPolicyIdField;

            private memberCobPolicyLeadSourceForOtherInsuranceCode leadSourceForOtherInsuranceCodeField;

            private memberCobPolicyRelationshipToSubscriberDefinitionReference relationshipToSubscriberDefinitionReferenceField;

            private string otherInsuranceCompanyNameField;

            private string policyTypeField;

            private string policyHolderNameField;

            private string benefitPlanNameField;

            private string groupNumberField;

            private string memberIdField;

            private memberCobPolicyBenefitPlanTypeCode benefitPlanTypeCodeField;

            private memberCobPolicyDateRanges[] dateRangesField;

            private memberCobPolicySubscriberIdList[] subscriberIdListField;

            private memberCobPolicySubscriberName subscriberNameField;

            private string subscriberDOBField;

            private string subscriberGenderField;

            private memberCobPolicyRxInfo rxInfoField;

            private memberCobPolicyCorrespondenceInfo correspondenceInfoField;

            private memberCobPolicyCarrierIds[] carrierIdsField;

            private string resourceURIField;

            private string versionField;

            /// <remarks/>
            public string cobPolicyIdentifier
            {
                get
                {
                    return this.cobPolicyIdentifierField;
                }
                set
                {
                    this.cobPolicyIdentifierField = value;
                }
            }

            /// <remarks/>
            public string cobPolicyId
            {
                get
                {
                    return this.cobPolicyIdField;
                }
                set
                {
                    this.cobPolicyIdField = value;
                }
            }

            /// <remarks/>
            public memberCobPolicyLeadSourceForOtherInsuranceCode leadSourceForOtherInsuranceCode
            {
                get
                {
                    return this.leadSourceForOtherInsuranceCodeField;
                }
                set
                {
                    this.leadSourceForOtherInsuranceCodeField = value;
                }
            }

            /// <remarks/>
            public memberCobPolicyRelationshipToSubscriberDefinitionReference relationshipToSubscriberDefinitionReference
            {
                get
                {
                    return this.relationshipToSubscriberDefinitionReferenceField;
                }
                set
                {
                    this.relationshipToSubscriberDefinitionReferenceField = value;
                }
            }

            /// <remarks/>
            public string otherInsuranceCompanyName
            {
                get
                {
                    return this.otherInsuranceCompanyNameField;
                }
                set
                {
                    this.otherInsuranceCompanyNameField = value;
                }
            }

            /// <remarks/>
            public string policyType
            {
                get
                {
                    return this.policyTypeField;
                }
                set
                {
                    this.policyTypeField = value;
                }
            }

            /// <remarks/>
            public string policyHolderName
            {
                get
                {
                    return this.policyHolderNameField;
                }
                set
                {
                    this.policyHolderNameField = value;
                }
            }

            /// <remarks/>
            public string benefitPlanName
            {
                get
                {
                    return this.benefitPlanNameField;
                }
                set
                {
                    this.benefitPlanNameField = value;
                }
            }

            /// <remarks/>
            public string groupNumber
            {
                get
                {
                    return this.groupNumberField;
                }
                set
                {
                    this.groupNumberField = value;
                }
            }

            /// <remarks/>
            public string memberId
            {
                get
                {
                    return this.memberIdField;
                }
                set
                {
                    this.memberIdField = value;
                }
            }

            /// <remarks/>
            public memberCobPolicyBenefitPlanTypeCode benefitPlanTypeCode
            {
                get
                {
                    return this.benefitPlanTypeCodeField;
                }
                set
                {
                    this.benefitPlanTypeCodeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("dateRanges")]
            public memberCobPolicyDateRanges[] dateRanges
            {
                get
                {
                    return this.dateRangesField;
                }
                set
                {
                    this.dateRangesField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("subscriberIdList")]
            public memberCobPolicySubscriberIdList[] subscriberIdList
            {
                get
                {
                    return this.subscriberIdListField;
                }
                set
                {
                    this.subscriberIdListField = value;
                }
            }

            /// <remarks/>
            public memberCobPolicySubscriberName subscriberName
            {
                get
                {
                    return this.subscriberNameField;
                }
                set
                {
                    this.subscriberNameField = value;
                }
            }

            /// <remarks/>
            public string subscriberDOB
            {
                get
                {
                    return this.subscriberDOBField;
                }
                set
                {
                    this.subscriberDOBField = value;
                }
            }

            /// <remarks/>
            public string subscriberGender
            {
                get
                {
                    return this.subscriberGenderField;
                }
                set
                {
                    this.subscriberGenderField = value;
                }
            }

            /// <remarks/>
            public memberCobPolicyRxInfo rxInfo
            {
                get
                {
                    return this.rxInfoField;
                }
                set
                {
                    this.rxInfoField = value;
                }
            }

            /// <remarks/>
            public memberCobPolicyCorrespondenceInfo correspondenceInfo
            {
                get
                {
                    return this.correspondenceInfoField;
                }
                set
                {
                    this.correspondenceInfoField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("carrierIds")]
            public memberCobPolicyCarrierIds[] carrierIds
            {
                get
                {
                    return this.carrierIdsField;
                }
                set
                {
                    this.carrierIdsField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string resourceURI
            {
                get
                {
                    return this.resourceURIField;
                }
                set
                {
                    this.resourceURIField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string version
            {
                get
                {
                    return this.versionField;
                }
                set
                {
                    this.versionField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberCobPolicyLeadSourceForOtherInsuranceCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberCobPolicyRelationshipToSubscriberDefinitionReference
        {

            private string idField;

            /// <remarks/>
            public string ID
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberCobPolicyBenefitPlanTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberCobPolicyDateRanges
        {

            private string startDateField;

            private string endDateField;

            private string responsibilitySequenceCodeField;

            private memberCobPolicyDateRangesOBDReasonCode oBDReasonCodeField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }

            /// <remarks/>
            public string responsibilitySequenceCode
            {
                get
                {
                    return this.responsibilitySequenceCodeField;
                }
                set
                {
                    this.responsibilitySequenceCodeField = value;
                }
            }

            /// <remarks/>
            public memberCobPolicyDateRangesOBDReasonCode OBDReasonCode
            {
                get
                {
                    return this.oBDReasonCodeField;
                }
                set
                {
                    this.oBDReasonCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberCobPolicyDateRangesOBDReasonCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberCobPolicySubscriberIdList
        {

            private string identificationNumberField;

            private string issuingStateField;

            private memberCobPolicySubscriberIdListIssuingCountry issuingCountryField;

            private string effectiveStartDateField;

            private string effectiveEndDateField;

            private memberCobPolicySubscriberIdListIdentificationTypeCode identificationTypeCodeField;

            /// <remarks/>
            public string identificationNumber
            {
                get
                {
                    return this.identificationNumberField;
                }
                set
                {
                    this.identificationNumberField = value;
                }
            }

            /// <remarks/>
            public string issuingState
            {
                get
                {
                    return this.issuingStateField;
                }
                set
                {
                    this.issuingStateField = value;
                }
            }

            /// <remarks/>
            public memberCobPolicySubscriberIdListIssuingCountry issuingCountry
            {
                get
                {
                    return this.issuingCountryField;
                }
                set
                {
                    this.issuingCountryField = value;
                }
            }

            /// <remarks/>
            public string effectiveStartDate
            {
                get
                {
                    return this.effectiveStartDateField;
                }
                set
                {
                    this.effectiveStartDateField = value;
                }
            }

            /// <remarks/>
            public string effectiveEndDate
            {
                get
                {
                    return this.effectiveEndDateField;
                }
                set
                {
                    this.effectiveEndDateField = value;
                }
            }

            /// <remarks/>
            public memberCobPolicySubscriberIdListIdentificationTypeCode identificationTypeCode
            {
                get
                {
                    return this.identificationTypeCodeField;
                }
                set
                {
                    this.identificationTypeCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberCobPolicySubscriberIdListIssuingCountry
        {

            private string countryCodeField;

            /// <remarks/>
            public string countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberCobPolicySubscriberIdListIdentificationTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberCobPolicySubscriberName
        {

            private string lastNameField;

            private string firstNameField;

            private string middleNameField;

            private string namePrefixField;

            private string nameSuffixField;

            private memberCobPolicySubscriberNameNameSuffixList nameSuffixListField;

            private memberCobPolicySubscriberNameCredentialSuffixList credentialSuffixListField;

            /// <remarks/>
            public string lastName
            {
                get
                {
                    return this.lastNameField;
                }
                set
                {
                    this.lastNameField = value;
                }
            }

            /// <remarks/>
            public string firstName
            {
                get
                {
                    return this.firstNameField;
                }
                set
                {
                    this.firstNameField = value;
                }
            }

            /// <remarks/>
            public string middleName
            {
                get
                {
                    return this.middleNameField;
                }
                set
                {
                    this.middleNameField = value;
                }
            }

            /// <remarks/>
            public string namePrefix
            {
                get
                {
                    return this.namePrefixField;
                }
                set
                {
                    this.namePrefixField = value;
                }
            }

            /// <remarks/>
            public string nameSuffix
            {
                get
                {
                    return this.nameSuffixField;
                }
                set
                {
                    this.nameSuffixField = value;
                }
            }

            /// <remarks/>
            public memberCobPolicySubscriberNameNameSuffixList nameSuffixList
            {
                get
                {
                    return this.nameSuffixListField;
                }
                set
                {
                    this.nameSuffixListField = value;
                }
            }

            /// <remarks/>
            public memberCobPolicySubscriberNameCredentialSuffixList credentialSuffixList
            {
                get
                {
                    return this.credentialSuffixListField;
                }
                set
                {
                    this.credentialSuffixListField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberCobPolicySubscriberNameNameSuffixList
        {

            private string listModeField;

            private string[] suffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("suffix")]
            public string[] suffix
            {
                get
                {
                    return this.suffixField;
                }
                set
                {
                    this.suffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberCobPolicySubscriberNameCredentialSuffixList
        {

            private string listModeField;

            private memberCobPolicySubscriberNameCredentialSuffixListCredentialSuffix[] credentialSuffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("CredentialSuffix")]
            public memberCobPolicySubscriberNameCredentialSuffixListCredentialSuffix[] CredentialSuffix
            {
                get
                {
                    return this.credentialSuffixField;
                }
                set
                {
                    this.credentialSuffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberCobPolicySubscriberNameCredentialSuffixListCredentialSuffix
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberCobPolicyRxInfo
        {

            private string rxBinField;

            private string pcnField;

            private string rxGroupField;

            private string rxIdField;

            private string rxPhoneNumberField;

            /// <remarks/>
            public string rxBin
            {
                get
                {
                    return this.rxBinField;
                }
                set
                {
                    this.rxBinField = value;
                }
            }

            /// <remarks/>
            public string pcn
            {
                get
                {
                    return this.pcnField;
                }
                set
                {
                    this.pcnField = value;
                }
            }

            /// <remarks/>
            public string rxGroup
            {
                get
                {
                    return this.rxGroupField;
                }
                set
                {
                    this.rxGroupField = value;
                }
            }

            /// <remarks/>
            public string rxId
            {
                get
                {
                    return this.rxIdField;
                }
                set
                {
                    this.rxIdField = value;
                }
            }

            /// <remarks/>
            public string rxPhoneNumber
            {
                get
                {
                    return this.rxPhoneNumberField;
                }
                set
                {
                    this.rxPhoneNumberField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberCobPolicyCorrespondenceInfo
        {

            private string emailAddressField;

            private memberCobPolicyCorrespondenceInfoAddressInfo addressInfoField;

            private memberCobPolicyCorrespondenceInfoSupplierCorrespondenceUsageCode supplierCorrespondenceUsageCodeField;

            private memberCobPolicyCorrespondenceInfoSupplierLocationCorrespondenceUsageCode supplierLocationCorrespondenceUsageCodeField;

            private memberCobPolicyCorrespondenceInfoAccountCorrespondenceUsageCode accountCorrespondenceUsageCodeField;

            /// <remarks/>
            public string emailAddress
            {
                get
                {
                    return this.emailAddressField;
                }
                set
                {
                    this.emailAddressField = value;
                }
            }

            /// <remarks/>
            public memberCobPolicyCorrespondenceInfoAddressInfo addressInfo
            {
                get
                {
                    return this.addressInfoField;
                }
                set
                {
                    this.addressInfoField = value;
                }
            }

            /// <remarks/>
            public memberCobPolicyCorrespondenceInfoSupplierCorrespondenceUsageCode supplierCorrespondenceUsageCode
            {
                get
                {
                    return this.supplierCorrespondenceUsageCodeField;
                }
                set
                {
                    this.supplierCorrespondenceUsageCodeField = value;
                }
            }

            /// <remarks/>
            public memberCobPolicyCorrespondenceInfoSupplierLocationCorrespondenceUsageCode supplierLocationCorrespondenceUsageCode
            {
                get
                {
                    return this.supplierLocationCorrespondenceUsageCodeField;
                }
                set
                {
                    this.supplierLocationCorrespondenceUsageCodeField = value;
                }
            }

            /// <remarks/>
            public memberCobPolicyCorrespondenceInfoAccountCorrespondenceUsageCode accountCorrespondenceUsageCode
            {
                get
                {
                    return this.accountCorrespondenceUsageCodeField;
                }
                set
                {
                    this.accountCorrespondenceUsageCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberCobPolicyCorrespondenceInfoAddressInfo
        {

            private memberCobPolicyCorrespondenceInfoAddressInfoPostalAddress postalAddressField;

            private memberCobPolicyCorrespondenceInfoAddressInfoAddressPhoneList[] addressPhoneListField;

            /// <remarks/>
            public memberCobPolicyCorrespondenceInfoAddressInfoPostalAddress postalAddress
            {
                get
                {
                    return this.postalAddressField;
                }
                set
                {
                    this.postalAddressField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("addressPhoneList")]
            public memberCobPolicyCorrespondenceInfoAddressInfoAddressPhoneList[] addressPhoneList
            {
                get
                {
                    return this.addressPhoneListField;
                }
                set
                {
                    this.addressPhoneListField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberCobPolicyCorrespondenceInfoAddressInfoPostalAddress
        {

            private string addressField;

            private string address2Field;

            private string address3Field;

            private string stateCodeField;

            private string zipCodeField;

            private string zipExtensionCodeField;

            private string cityNameField;

            private string countyCodeField;

            private memberCobPolicyCorrespondenceInfoAddressInfoPostalAddressCountryCode countryCodeField;

            private string longitudeField;

            private string latitudeField;

            private string ignoreAddressCheckField;

            private string undeliverableAddressField;

            /// <remarks/>
            public string address
            {
                get
                {
                    return this.addressField;
                }
                set
                {
                    this.addressField = value;
                }
            }

            /// <remarks/>
            public string address2
            {
                get
                {
                    return this.address2Field;
                }
                set
                {
                    this.address2Field = value;
                }
            }

            /// <remarks/>
            public string address3
            {
                get
                {
                    return this.address3Field;
                }
                set
                {
                    this.address3Field = value;
                }
            }

            /// <remarks/>
            public string stateCode
            {
                get
                {
                    return this.stateCodeField;
                }
                set
                {
                    this.stateCodeField = value;
                }
            }

            /// <remarks/>
            public string zipCode
            {
                get
                {
                    return this.zipCodeField;
                }
                set
                {
                    this.zipCodeField = value;
                }
            }

            /// <remarks/>
            public string zipExtensionCode
            {
                get
                {
                    return this.zipExtensionCodeField;
                }
                set
                {
                    this.zipExtensionCodeField = value;
                }
            }

            /// <remarks/>
            public string cityName
            {
                get
                {
                    return this.cityNameField;
                }
                set
                {
                    this.cityNameField = value;
                }
            }

            /// <remarks/>
            public string countyCode
            {
                get
                {
                    return this.countyCodeField;
                }
                set
                {
                    this.countyCodeField = value;
                }
            }

            /// <remarks/>
            public memberCobPolicyCorrespondenceInfoAddressInfoPostalAddressCountryCode countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }

            /// <remarks/>
            public string longitude
            {
                get
                {
                    return this.longitudeField;
                }
                set
                {
                    this.longitudeField = value;
                }
            }

            /// <remarks/>
            public string latitude
            {
                get
                {
                    return this.latitudeField;
                }
                set
                {
                    this.latitudeField = value;
                }
            }

            /// <remarks/>
            public string ignoreAddressCheck
            {
                get
                {
                    return this.ignoreAddressCheckField;
                }
                set
                {
                    this.ignoreAddressCheckField = value;
                }
            }

            /// <remarks/>
            public string undeliverableAddress
            {
                get
                {
                    return this.undeliverableAddressField;
                }
                set
                {
                    this.undeliverableAddressField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberCobPolicyCorrespondenceInfoAddressInfoPostalAddressCountryCode
        {

            private string countryCodeField;

            /// <remarks/>
            public string countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberCobPolicyCorrespondenceInfoAddressInfoAddressPhoneList
        {

            private string phoneCountryCodeField;

            private string phoneAreaCodeField;

            private string phoneNumberField;

            private string phoneExtensionNumberField;

            private memberCobPolicyCorrespondenceInfoAddressInfoAddressPhoneListIndividualPhoneTypeCode individualPhoneTypeCodeField;

            private memberCobPolicyCorrespondenceInfoAddressInfoAddressPhoneListOrganizationPhoneTypeCode organizationPhoneTypeCodeField;

            /// <remarks/>
            public string phoneCountryCode
            {
                get
                {
                    return this.phoneCountryCodeField;
                }
                set
                {
                    this.phoneCountryCodeField = value;
                }
            }

            /// <remarks/>
            public string phoneAreaCode
            {
                get
                {
                    return this.phoneAreaCodeField;
                }
                set
                {
                    this.phoneAreaCodeField = value;
                }
            }

            /// <remarks/>
            public string phoneNumber
            {
                get
                {
                    return this.phoneNumberField;
                }
                set
                {
                    this.phoneNumberField = value;
                }
            }

            /// <remarks/>
            public string phoneExtensionNumber
            {
                get
                {
                    return this.phoneExtensionNumberField;
                }
                set
                {
                    this.phoneExtensionNumberField = value;
                }
            }

            /// <remarks/>
            public memberCobPolicyCorrespondenceInfoAddressInfoAddressPhoneListIndividualPhoneTypeCode individualPhoneTypeCode
            {
                get
                {
                    return this.individualPhoneTypeCodeField;
                }
                set
                {
                    this.individualPhoneTypeCodeField = value;
                }
            }

            /// <remarks/>
            public memberCobPolicyCorrespondenceInfoAddressInfoAddressPhoneListOrganizationPhoneTypeCode organizationPhoneTypeCode
            {
                get
                {
                    return this.organizationPhoneTypeCodeField;
                }
                set
                {
                    this.organizationPhoneTypeCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberCobPolicyCorrespondenceInfoAddressInfoAddressPhoneListIndividualPhoneTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberCobPolicyCorrespondenceInfoAddressInfoAddressPhoneListOrganizationPhoneTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberCobPolicyCorrespondenceInfoSupplierCorrespondenceUsageCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberCobPolicyCorrespondenceInfoSupplierLocationCorrespondenceUsageCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberCobPolicyCorrespondenceInfoAccountCorrespondenceUsageCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberCobPolicyCarrierIds
        {

            private string identificationNumberField;

            private string issuingStateField;

            private memberCobPolicyCarrierIdsIssuingCountry issuingCountryField;

            private string effectiveStartDateField;

            private string effectiveEndDateField;

            private memberCobPolicyCarrierIdsIdentificationTypeCode identificationTypeCodeField;

            /// <remarks/>
            public string identificationNumber
            {
                get
                {
                    return this.identificationNumberField;
                }
                set
                {
                    this.identificationNumberField = value;
                }
            }

            /// <remarks/>
            public string issuingState
            {
                get
                {
                    return this.issuingStateField;
                }
                set
                {
                    this.issuingStateField = value;
                }
            }

            /// <remarks/>
            public memberCobPolicyCarrierIdsIssuingCountry issuingCountry
            {
                get
                {
                    return this.issuingCountryField;
                }
                set
                {
                    this.issuingCountryField = value;
                }
            }

            /// <remarks/>
            public string effectiveStartDate
            {
                get
                {
                    return this.effectiveStartDateField;
                }
                set
                {
                    this.effectiveStartDateField = value;
                }
            }

            /// <remarks/>
            public string effectiveEndDate
            {
                get
                {
                    return this.effectiveEndDateField;
                }
                set
                {
                    this.effectiveEndDateField = value;
                }
            }

            /// <remarks/>
            public memberCobPolicyCarrierIdsIdentificationTypeCode identificationTypeCode
            {
                get
                {
                    return this.identificationTypeCodeField;
                }
                set
                {
                    this.identificationTypeCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberCobPolicyCarrierIdsIssuingCountry
        {

            private string countryCodeField;

            /// <remarks/>
            public string countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberCobPolicyCarrierIdsIdentificationTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberHeadOfHousehold
        {

            private memberHeadOfHouseholdRelatedMember relatedMemberField;

            private memberHeadOfHouseholdExternalMemberInformation externalMemberInformationField;

            /// <remarks/>
            public memberHeadOfHouseholdRelatedMember relatedMember
            {
                get
                {
                    return this.relatedMemberField;
                }
                set
                {
                    this.relatedMemberField = value;
                }
            }

            /// <remarks/>
            public memberHeadOfHouseholdExternalMemberInformation externalMemberInformation
            {
                get
                {
                    return this.externalMemberInformationField;
                }
                set
                {
                    this.externalMemberInformationField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberHeadOfHouseholdRelatedMember
        {

            private string definitionNameField;

            private string idField;

            private string topAccountField;

            private string customString1Field;

            private string customString2Field;

            private string customString3Field;

            private string customString4Field;

            private string customInteger1Field;

            private string customInteger2Field;

            private string customInteger3Field;

            private string customInteger4Field;

            private string customDate1Field;

            private string customDate2Field;

            private string customDate3Field;

            private string customDate4Field;

            private string customDecimal1Field;

            private string customDecimal2Field;

            private string customDecimal3Field;

            private string customDecimal4Field;

            private memberHeadOfHouseholdRelatedMemberMember memberField;

            private memberHeadOfHouseholdRelatedMemberSubscriber subscriberField;

            private string asOfDateField;

            /// <remarks/>
            public string definitionName
            {
                get
                {
                    return this.definitionNameField;
                }
                set
                {
                    this.definitionNameField = value;
                }
            }

            /// <remarks/>
            public string id
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }

            /// <remarks/>
            public string topAccount
            {
                get
                {
                    return this.topAccountField;
                }
                set
                {
                    this.topAccountField = value;
                }
            }

            /// <remarks/>
            public string customString1
            {
                get
                {
                    return this.customString1Field;
                }
                set
                {
                    this.customString1Field = value;
                }
            }

            /// <remarks/>
            public string customString2
            {
                get
                {
                    return this.customString2Field;
                }
                set
                {
                    this.customString2Field = value;
                }
            }

            /// <remarks/>
            public string customString3
            {
                get
                {
                    return this.customString3Field;
                }
                set
                {
                    this.customString3Field = value;
                }
            }

            /// <remarks/>
            public string customString4
            {
                get
                {
                    return this.customString4Field;
                }
                set
                {
                    this.customString4Field = value;
                }
            }

            /// <remarks/>
            public string customInteger1
            {
                get
                {
                    return this.customInteger1Field;
                }
                set
                {
                    this.customInteger1Field = value;
                }
            }

            /// <remarks/>
            public string customInteger2
            {
                get
                {
                    return this.customInteger2Field;
                }
                set
                {
                    this.customInteger2Field = value;
                }
            }

            /// <remarks/>
            public string customInteger3
            {
                get
                {
                    return this.customInteger3Field;
                }
                set
                {
                    this.customInteger3Field = value;
                }
            }

            /// <remarks/>
            public string customInteger4
            {
                get
                {
                    return this.customInteger4Field;
                }
                set
                {
                    this.customInteger4Field = value;
                }
            }

            /// <remarks/>
            public string customDate1
            {
                get
                {
                    return this.customDate1Field;
                }
                set
                {
                    this.customDate1Field = value;
                }
            }

            /// <remarks/>
            public string customDate2
            {
                get
                {
                    return this.customDate2Field;
                }
                set
                {
                    this.customDate2Field = value;
                }
            }

            /// <remarks/>
            public string customDate3
            {
                get
                {
                    return this.customDate3Field;
                }
                set
                {
                    this.customDate3Field = value;
                }
            }

            /// <remarks/>
            public string customDate4
            {
                get
                {
                    return this.customDate4Field;
                }
                set
                {
                    this.customDate4Field = value;
                }
            }

            /// <remarks/>
            public string customDecimal1
            {
                get
                {
                    return this.customDecimal1Field;
                }
                set
                {
                    this.customDecimal1Field = value;
                }
            }

            /// <remarks/>
            public string customDecimal2
            {
                get
                {
                    return this.customDecimal2Field;
                }
                set
                {
                    this.customDecimal2Field = value;
                }
            }

            /// <remarks/>
            public string customDecimal3
            {
                get
                {
                    return this.customDecimal3Field;
                }
                set
                {
                    this.customDecimal3Field = value;
                }
            }

            /// <remarks/>
            public string customDecimal4
            {
                get
                {
                    return this.customDecimal4Field;
                }
                set
                {
                    this.customDecimal4Field = value;
                }
            }

            /// <remarks/>
            public memberHeadOfHouseholdRelatedMemberMember member
            {
                get
                {
                    return this.memberField;
                }
                set
                {
                    this.memberField = value;
                }
            }

            /// <remarks/>
            public memberHeadOfHouseholdRelatedMemberSubscriber subscriber
            {
                get
                {
                    return this.subscriberField;
                }
                set
                {
                    this.subscriberField = value;
                }
            }

            /// <remarks/>
            public string asOfDate
            {
                get
                {
                    return this.asOfDateField;
                }
                set
                {
                    this.asOfDateField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberHeadOfHouseholdRelatedMemberMember
        {

            private string firstNameField;

            private string lastNameField;

            private string fullNameField;

            private string genderField;

            private string dateOfBirthField;

            private string taxIdField;

            private memberHeadOfHouseholdRelatedMemberMemberAddress addressField;

            /// <remarks/>
            public string firstName
            {
                get
                {
                    return this.firstNameField;
                }
                set
                {
                    this.firstNameField = value;
                }
            }

            /// <remarks/>
            public string lastName
            {
                get
                {
                    return this.lastNameField;
                }
                set
                {
                    this.lastNameField = value;
                }
            }

            /// <remarks/>
            public string fullName
            {
                get
                {
                    return this.fullNameField;
                }
                set
                {
                    this.fullNameField = value;
                }
            }

            /// <remarks/>
            public string gender
            {
                get
                {
                    return this.genderField;
                }
                set
                {
                    this.genderField = value;
                }
            }

            /// <remarks/>
            public string dateOfBirth
            {
                get
                {
                    return this.dateOfBirthField;
                }
                set
                {
                    this.dateOfBirthField = value;
                }
            }

            /// <remarks/>
            public string taxId
            {
                get
                {
                    return this.taxIdField;
                }
                set
                {
                    this.taxIdField = value;
                }
            }

            /// <remarks/>
            public memberHeadOfHouseholdRelatedMemberMemberAddress address
            {
                get
                {
                    return this.addressField;
                }
                set
                {
                    this.addressField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberHeadOfHouseholdRelatedMemberMemberAddress
        {

            private string addressField;

            private string cityNameField;

            private string stateCodeField;

            private string zipCodeField;

            private string zipExtensionCodeField;

            private string countryCodeField;

            /// <remarks/>
            public string address
            {
                get
                {
                    return this.addressField;
                }
                set
                {
                    this.addressField = value;
                }
            }

            /// <remarks/>
            public string cityName
            {
                get
                {
                    return this.cityNameField;
                }
                set
                {
                    this.cityNameField = value;
                }
            }

            /// <remarks/>
            public string stateCode
            {
                get
                {
                    return this.stateCodeField;
                }
                set
                {
                    this.stateCodeField = value;
                }
            }

            /// <remarks/>
            public string zipCode
            {
                get
                {
                    return this.zipCodeField;
                }
                set
                {
                    this.zipCodeField = value;
                }
            }

            /// <remarks/>
            public string zipExtensionCode
            {
                get
                {
                    return this.zipExtensionCodeField;
                }
                set
                {
                    this.zipExtensionCodeField = value;
                }
            }

            /// <remarks/>
            public string countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberHeadOfHouseholdRelatedMemberSubscriber
        {

            private string firstNameField;

            private string lastNameField;

            private string fullNameField;

            private string genderField;

            private string dateOfBirthField;

            private string taxIdField;

            private memberHeadOfHouseholdRelatedMemberSubscriberAddress addressField;

            /// <remarks/>
            public string firstName
            {
                get
                {
                    return this.firstNameField;
                }
                set
                {
                    this.firstNameField = value;
                }
            }

            /// <remarks/>
            public string lastName
            {
                get
                {
                    return this.lastNameField;
                }
                set
                {
                    this.lastNameField = value;
                }
            }

            /// <remarks/>
            public string fullName
            {
                get
                {
                    return this.fullNameField;
                }
                set
                {
                    this.fullNameField = value;
                }
            }

            /// <remarks/>
            public string gender
            {
                get
                {
                    return this.genderField;
                }
                set
                {
                    this.genderField = value;
                }
            }

            /// <remarks/>
            public string dateOfBirth
            {
                get
                {
                    return this.dateOfBirthField;
                }
                set
                {
                    this.dateOfBirthField = value;
                }
            }

            /// <remarks/>
            public string taxId
            {
                get
                {
                    return this.taxIdField;
                }
                set
                {
                    this.taxIdField = value;
                }
            }

            /// <remarks/>
            public memberHeadOfHouseholdRelatedMemberSubscriberAddress address
            {
                get
                {
                    return this.addressField;
                }
                set
                {
                    this.addressField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberHeadOfHouseholdRelatedMemberSubscriberAddress
        {

            private string addressField;

            private string cityNameField;

            private string stateCodeField;

            private string zipCodeField;

            private string zipExtensionCodeField;

            private string countryCodeField;

            /// <remarks/>
            public string address
            {
                get
                {
                    return this.addressField;
                }
                set
                {
                    this.addressField = value;
                }
            }

            /// <remarks/>
            public string cityName
            {
                get
                {
                    return this.cityNameField;
                }
                set
                {
                    this.cityNameField = value;
                }
            }

            /// <remarks/>
            public string stateCode
            {
                get
                {
                    return this.stateCodeField;
                }
                set
                {
                    this.stateCodeField = value;
                }
            }

            /// <remarks/>
            public string zipCode
            {
                get
                {
                    return this.zipCodeField;
                }
                set
                {
                    this.zipCodeField = value;
                }
            }

            /// <remarks/>
            public string zipExtensionCode
            {
                get
                {
                    return this.zipExtensionCodeField;
                }
                set
                {
                    this.zipExtensionCodeField = value;
                }
            }

            /// <remarks/>
            public string countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberHeadOfHouseholdExternalMemberInformation
        {

            private string matchDefinitionNameField;

            private string taxIdentificationNumberField;

            private memberHeadOfHouseholdExternalMemberInformationPrimaryName primaryNameField;

            private memberHeadOfHouseholdExternalMemberInformationAddressInfo addressInfoField;

            /// <remarks/>
            public string matchDefinitionName
            {
                get
                {
                    return this.matchDefinitionNameField;
                }
                set
                {
                    this.matchDefinitionNameField = value;
                }
            }

            /// <remarks/>
            public string taxIdentificationNumber
            {
                get
                {
                    return this.taxIdentificationNumberField;
                }
                set
                {
                    this.taxIdentificationNumberField = value;
                }
            }

            /// <remarks/>
            public memberHeadOfHouseholdExternalMemberInformationPrimaryName primaryName
            {
                get
                {
                    return this.primaryNameField;
                }
                set
                {
                    this.primaryNameField = value;
                }
            }

            /// <remarks/>
            public memberHeadOfHouseholdExternalMemberInformationAddressInfo addressInfo
            {
                get
                {
                    return this.addressInfoField;
                }
                set
                {
                    this.addressInfoField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberHeadOfHouseholdExternalMemberInformationPrimaryName
        {

            private string lastNameField;

            private string firstNameField;

            private string middleNameField;

            private string namePrefixField;

            private string nameSuffixField;

            private memberHeadOfHouseholdExternalMemberInformationPrimaryNameNameSuffixList nameSuffixListField;

            private memberHeadOfHouseholdExternalMemberInformationPrimaryNameCredentialSuffixList credentialSuffixListField;

            /// <remarks/>
            public string lastName
            {
                get
                {
                    return this.lastNameField;
                }
                set
                {
                    this.lastNameField = value;
                }
            }

            /// <remarks/>
            public string firstName
            {
                get
                {
                    return this.firstNameField;
                }
                set
                {
                    this.firstNameField = value;
                }
            }

            /// <remarks/>
            public string middleName
            {
                get
                {
                    return this.middleNameField;
                }
                set
                {
                    this.middleNameField = value;
                }
            }

            /// <remarks/>
            public string namePrefix
            {
                get
                {
                    return this.namePrefixField;
                }
                set
                {
                    this.namePrefixField = value;
                }
            }

            /// <remarks/>
            public string nameSuffix
            {
                get
                {
                    return this.nameSuffixField;
                }
                set
                {
                    this.nameSuffixField = value;
                }
            }

            /// <remarks/>
            public memberHeadOfHouseholdExternalMemberInformationPrimaryNameNameSuffixList nameSuffixList
            {
                get
                {
                    return this.nameSuffixListField;
                }
                set
                {
                    this.nameSuffixListField = value;
                }
            }

            /// <remarks/>
            public memberHeadOfHouseholdExternalMemberInformationPrimaryNameCredentialSuffixList credentialSuffixList
            {
                get
                {
                    return this.credentialSuffixListField;
                }
                set
                {
                    this.credentialSuffixListField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberHeadOfHouseholdExternalMemberInformationPrimaryNameNameSuffixList
        {

            private string listModeField;

            private string[] suffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("suffix")]
            public string[] suffix
            {
                get
                {
                    return this.suffixField;
                }
                set
                {
                    this.suffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberHeadOfHouseholdExternalMemberInformationPrimaryNameCredentialSuffixList
        {

            private string listModeField;

            private memberHeadOfHouseholdExternalMemberInformationPrimaryNameCredentialSuffixListCredentialSuffix[] credentialSuffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("CredentialSuffix")]
            public memberHeadOfHouseholdExternalMemberInformationPrimaryNameCredentialSuffixListCredentialSuffix[] CredentialSuffix
            {
                get
                {
                    return this.credentialSuffixField;
                }
                set
                {
                    this.credentialSuffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberHeadOfHouseholdExternalMemberInformationPrimaryNameCredentialSuffixListCredentialSuffix
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberHeadOfHouseholdExternalMemberInformationAddressInfo
        {

            private memberHeadOfHouseholdExternalMemberInformationAddressInfoPostalAddress postalAddressField;

            private memberHeadOfHouseholdExternalMemberInformationAddressInfoAddressPhoneList addressPhoneListField;

            /// <remarks/>
            public memberHeadOfHouseholdExternalMemberInformationAddressInfoPostalAddress postalAddress
            {
                get
                {
                    return this.postalAddressField;
                }
                set
                {
                    this.postalAddressField = value;
                }
            }

            /// <remarks/>
            public memberHeadOfHouseholdExternalMemberInformationAddressInfoAddressPhoneList addressPhoneList
            {
                get
                {
                    return this.addressPhoneListField;
                }
                set
                {
                    this.addressPhoneListField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberHeadOfHouseholdExternalMemberInformationAddressInfoPostalAddress
        {

            private string addressField;

            private string address2Field;

            private string address3Field;

            private string stateCodeField;

            private string zipCodeField;

            private string zipExtensionCodeField;

            private string cityNameField;

            private string countyCodeField;

            private memberHeadOfHouseholdExternalMemberInformationAddressInfoPostalAddressCountryCode countryCodeField;

            private string longitudeField;

            private string latitudeField;

            private string ignoreAddressCheckField;

            private string undeliverableAddressField;

            /// <remarks/>
            public string address
            {
                get
                {
                    return this.addressField;
                }
                set
                {
                    this.addressField = value;
                }
            }

            /// <remarks/>
            public string address2
            {
                get
                {
                    return this.address2Field;
                }
                set
                {
                    this.address2Field = value;
                }
            }

            /// <remarks/>
            public string address3
            {
                get
                {
                    return this.address3Field;
                }
                set
                {
                    this.address3Field = value;
                }
            }

            /// <remarks/>
            public string stateCode
            {
                get
                {
                    return this.stateCodeField;
                }
                set
                {
                    this.stateCodeField = value;
                }
            }

            /// <remarks/>
            public string zipCode
            {
                get
                {
                    return this.zipCodeField;
                }
                set
                {
                    this.zipCodeField = value;
                }
            }

            /// <remarks/>
            public string zipExtensionCode
            {
                get
                {
                    return this.zipExtensionCodeField;
                }
                set
                {
                    this.zipExtensionCodeField = value;
                }
            }

            /// <remarks/>
            public string cityName
            {
                get
                {
                    return this.cityNameField;
                }
                set
                {
                    this.cityNameField = value;
                }
            }

            /// <remarks/>
            public string countyCode
            {
                get
                {
                    return this.countyCodeField;
                }
                set
                {
                    this.countyCodeField = value;
                }
            }

            /// <remarks/>
            public memberHeadOfHouseholdExternalMemberInformationAddressInfoPostalAddressCountryCode countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }

            /// <remarks/>
            public string longitude
            {
                get
                {
                    return this.longitudeField;
                }
                set
                {
                    this.longitudeField = value;
                }
            }

            /// <remarks/>
            public string latitude
            {
                get
                {
                    return this.latitudeField;
                }
                set
                {
                    this.latitudeField = value;
                }
            }

            /// <remarks/>
            public string ignoreAddressCheck
            {
                get
                {
                    return this.ignoreAddressCheckField;
                }
                set
                {
                    this.ignoreAddressCheckField = value;
                }
            }

            /// <remarks/>
            public string undeliverableAddress
            {
                get
                {
                    return this.undeliverableAddressField;
                }
                set
                {
                    this.undeliverableAddressField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberHeadOfHouseholdExternalMemberInformationAddressInfoPostalAddressCountryCode
        {

            private string countryCodeField;

            /// <remarks/>
            public string countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberHeadOfHouseholdExternalMemberInformationAddressInfoAddressPhoneList
        {

            private string listModeField;

            private memberHeadOfHouseholdExternalMemberInformationAddressInfoAddressPhoneListTelephoneNumber[] telephoneNumberField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("telephoneNumber")]
            public memberHeadOfHouseholdExternalMemberInformationAddressInfoAddressPhoneListTelephoneNumber[] telephoneNumber
            {
                get
                {
                    return this.telephoneNumberField;
                }
                set
                {
                    this.telephoneNumberField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberHeadOfHouseholdExternalMemberInformationAddressInfoAddressPhoneListTelephoneNumber
        {

            private string phoneCountryCodeField;

            private string phoneAreaCodeField;

            private string phoneNumberField;

            private string phoneExtensionNumberField;

            private memberHeadOfHouseholdExternalMemberInformationAddressInfoAddressPhoneListTelephoneNumberIndividualPhoneTypeCode individualPhoneTypeCodeField;

            private memberHeadOfHouseholdExternalMemberInformationAddressInfoAddressPhoneListTelephoneNumberOrganizationPhoneTypeCode organizationPhoneTypeCodeField;

            /// <remarks/>
            public string phoneCountryCode
            {
                get
                {
                    return this.phoneCountryCodeField;
                }
                set
                {
                    this.phoneCountryCodeField = value;
                }
            }

            /// <remarks/>
            public string phoneAreaCode
            {
                get
                {
                    return this.phoneAreaCodeField;
                }
                set
                {
                    this.phoneAreaCodeField = value;
                }
            }

            /// <remarks/>
            public string phoneNumber
            {
                get
                {
                    return this.phoneNumberField;
                }
                set
                {
                    this.phoneNumberField = value;
                }
            }

            /// <remarks/>
            public string phoneExtensionNumber
            {
                get
                {
                    return this.phoneExtensionNumberField;
                }
                set
                {
                    this.phoneExtensionNumberField = value;
                }
            }

            /// <remarks/>
            public memberHeadOfHouseholdExternalMemberInformationAddressInfoAddressPhoneListTelephoneNumberIndividualPhoneTypeCode individualPhoneTypeCode
            {
                get
                {
                    return this.individualPhoneTypeCodeField;
                }
                set
                {
                    this.individualPhoneTypeCodeField = value;
                }
            }

            /// <remarks/>
            public memberHeadOfHouseholdExternalMemberInformationAddressInfoAddressPhoneListTelephoneNumberOrganizationPhoneTypeCode organizationPhoneTypeCode
            {
                get
                {
                    return this.organizationPhoneTypeCodeField;
                }
                set
                {
                    this.organizationPhoneTypeCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberHeadOfHouseholdExternalMemberInformationAddressInfoAddressPhoneListTelephoneNumberIndividualPhoneTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberHeadOfHouseholdExternalMemberInformationAddressInfoAddressPhoneListTelephoneNumberOrganizationPhoneTypeCode
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMemberComplianceProgram
        {

            private memberMemberComplianceProgramComplianceProgramSelections complianceProgramSelectionsField;

            private string resourceURIField;

            private string versionField;

            /// <remarks/>
            public memberMemberComplianceProgramComplianceProgramSelections complianceProgramSelections
            {
                get
                {
                    return this.complianceProgramSelectionsField;
                }
                set
                {
                    this.complianceProgramSelectionsField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string resourceURI
            {
                get
                {
                    return this.resourceURIField;
                }
                set
                {
                    this.resourceURIField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string version
            {
                get
                {
                    return this.versionField;
                }
                set
                {
                    this.versionField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMemberComplianceProgramComplianceProgramSelections
        {

            private string updateModeField;

            private memberMemberComplianceProgramComplianceProgramSelectionsComplianceProgramSelection[] complianceProgramSelectionField;

            /// <remarks/>
            public string updateMode
            {
                get
                {
                    return this.updateModeField;
                }
                set
                {
                    this.updateModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("complianceProgramSelection")]
            public memberMemberComplianceProgramComplianceProgramSelectionsComplianceProgramSelection[] complianceProgramSelection
            {
                get
                {
                    return this.complianceProgramSelectionField;
                }
                set
                {
                    this.complianceProgramSelectionField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMemberComplianceProgramComplianceProgramSelectionsComplianceProgramSelection
        {

            private memberMemberComplianceProgramComplianceProgramSelectionsComplianceProgramSelectionComplianceProgramDateRanges complianceProgramDateRangesField;

            private memberMemberComplianceProgramComplianceProgramSelectionsComplianceProgramSelectionComplianceProgramReference complianceProgramReferenceField;

            /// <remarks/>
            public memberMemberComplianceProgramComplianceProgramSelectionsComplianceProgramSelectionComplianceProgramDateRanges complianceProgramDateRanges
            {
                get
                {
                    return this.complianceProgramDateRangesField;
                }
                set
                {
                    this.complianceProgramDateRangesField = value;
                }
            }

            /// <remarks/>
            public memberMemberComplianceProgramComplianceProgramSelectionsComplianceProgramSelectionComplianceProgramReference complianceProgramReference
            {
                get
                {
                    return this.complianceProgramReferenceField;
                }
                set
                {
                    this.complianceProgramReferenceField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMemberComplianceProgramComplianceProgramSelectionsComplianceProgramSelectionComplianceProgramDateRanges
        {

            private string startDateField;

            private string endDateField;

            private string complianceCodeField;

            /// <remarks/>
            public string startDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            public string endDate
            {
                get
                {
                    return this.endDateField;
                }
                set
                {
                    this.endDateField = value;
                }
            }

            /// <remarks/>
            public string complianceCode
            {
                get
                {
                    return this.complianceCodeField;
                }
                set
                {
                    this.complianceCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberMemberComplianceProgramComplianceProgramSelectionsComplianceProgramSelectionComplianceProgramReference
        {

            private string idField;

            /// <remarks/>
            public string ID
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberEnrollmentMemberFactors
        {

            private string updateModeField;

            private memberEnrollmentMemberFactorsMemberFactor[] memberFactorsField;

            /// <remarks/>
            public string updateMode
            {
                get
                {
                    return this.updateModeField;
                }
                set
                {
                    this.updateModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlArrayItemAttribute("memberFactor", IsNullable = false)]
            public memberEnrollmentMemberFactorsMemberFactor[] memberFactors
            {
                get
                {
                    return this.memberFactorsField;
                }
                set
                {
                    this.memberFactorsField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberEnrollmentMemberFactorsMemberFactor
        {

            private memberEnrollmentMemberFactorsMemberFactorFactorType factorTypeField;

            private memberEnrollmentMemberFactorsMemberFactorFactorDateRanges[] factorDateRangesField;

            /// <remarks/>
            public memberEnrollmentMemberFactorsMemberFactorFactorType factorType
            {
                get
                {
                    return this.factorTypeField;
                }
                set
                {
                    this.factorTypeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("factorDateRanges")]
            public memberEnrollmentMemberFactorsMemberFactorFactorDateRanges[] factorDateRanges
            {
                get
                {
                    return this.factorDateRangesField;
                }
                set
                {
                    this.factorDateRangesField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberEnrollmentMemberFactorsMemberFactorFactorType
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberEnrollmentMemberFactorsMemberFactorFactorDateRanges
        {

            private string factorStartDateField;

            private string factorEndDateField;

            private memberEnrollmentMemberFactorsMemberFactorFactorDateRangesFactorQualifier factorQualifierField;

            private string factorValueField;

            /// <remarks/>
            public string factorStartDate
            {
                get
                {
                    return this.factorStartDateField;
                }
                set
                {
                    this.factorStartDateField = value;
                }
            }

            /// <remarks/>
            public string factorEndDate
            {
                get
                {
                    return this.factorEndDateField;
                }
                set
                {
                    this.factorEndDateField = value;
                }
            }

            /// <remarks/>
            public memberEnrollmentMemberFactorsMemberFactorFactorDateRangesFactorQualifier factorQualifier
            {
                get
                {
                    return this.factorQualifierField;
                }
                set
                {
                    this.factorQualifierField = value;
                }
            }

            /// <remarks/>
            public string factorValue
            {
                get
                {
                    return this.factorValueField;
                }
                set
                {
                    this.factorValueField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberEnrollmentMemberFactorsMemberFactorFactorDateRangesFactorQualifier
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberAlternatePaymentContact
        {

            private memberAlternatePaymentContactRecipientName recipientNameField;

            private memberAlternatePaymentContactRecipientAddress recipientAddressField;

            /// <remarks/>
            public memberAlternatePaymentContactRecipientName recipientName
            {
                get
                {
                    return this.recipientNameField;
                }
                set
                {
                    this.recipientNameField = value;
                }
            }

            /// <remarks/>
            public memberAlternatePaymentContactRecipientAddress recipientAddress
            {
                get
                {
                    return this.recipientAddressField;
                }
                set
                {
                    this.recipientAddressField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberAlternatePaymentContactRecipientName
        {

            private string lastNameField;

            private string firstNameField;

            private string middleNameField;

            private string namePrefixField;

            private string nameSuffixField;

            private memberAlternatePaymentContactRecipientNameNameSuffixList nameSuffixListField;

            private memberAlternatePaymentContactRecipientNameCredentialSuffixList credentialSuffixListField;

            /// <remarks/>
            public string lastName
            {
                get
                {
                    return this.lastNameField;
                }
                set
                {
                    this.lastNameField = value;
                }
            }

            /// <remarks/>
            public string firstName
            {
                get
                {
                    return this.firstNameField;
                }
                set
                {
                    this.firstNameField = value;
                }
            }

            /// <remarks/>
            public string middleName
            {
                get
                {
                    return this.middleNameField;
                }
                set
                {
                    this.middleNameField = value;
                }
            }

            /// <remarks/>
            public string namePrefix
            {
                get
                {
                    return this.namePrefixField;
                }
                set
                {
                    this.namePrefixField = value;
                }
            }

            /// <remarks/>
            public string nameSuffix
            {
                get
                {
                    return this.nameSuffixField;
                }
                set
                {
                    this.nameSuffixField = value;
                }
            }

            /// <remarks/>
            public memberAlternatePaymentContactRecipientNameNameSuffixList nameSuffixList
            {
                get
                {
                    return this.nameSuffixListField;
                }
                set
                {
                    this.nameSuffixListField = value;
                }
            }

            /// <remarks/>
            public memberAlternatePaymentContactRecipientNameCredentialSuffixList credentialSuffixList
            {
                get
                {
                    return this.credentialSuffixListField;
                }
                set
                {
                    this.credentialSuffixListField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberAlternatePaymentContactRecipientNameNameSuffixList
        {

            private string listModeField;

            private string[] suffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("suffix")]
            public string[] suffix
            {
                get
                {
                    return this.suffixField;
                }
                set
                {
                    this.suffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberAlternatePaymentContactRecipientNameCredentialSuffixList
        {

            private string listModeField;

            private memberAlternatePaymentContactRecipientNameCredentialSuffixListCredentialSuffix[] credentialSuffixField;

            /// <remarks/>
            public string listMode
            {
                get
                {
                    return this.listModeField;
                }
                set
                {
                    this.listModeField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("CredentialSuffix")]
            public memberAlternatePaymentContactRecipientNameCredentialSuffixListCredentialSuffix[] CredentialSuffix
            {
                get
                {
                    return this.credentialSuffixField;
                }
                set
                {
                    this.credentialSuffixField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberAlternatePaymentContactRecipientNameCredentialSuffixListCredentialSuffix
        {

            private string codeSetNameField;

            private string codeEntryField;

            private string shortNameField;

            /// <remarks/>
            public string codeSetName
            {
                get
                {
                    return this.codeSetNameField;
                }
                set
                {
                    this.codeSetNameField = value;
                }
            }

            /// <remarks/>
            public string codeEntry
            {
                get
                {
                    return this.codeEntryField;
                }
                set
                {
                    this.codeEntryField = value;
                }
            }

            /// <remarks/>
            public string shortName
            {
                get
                {
                    return this.shortNameField;
                }
                set
                {
                    this.shortNameField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberAlternatePaymentContactRecipientAddress
        {

            private string addressField;

            private string address2Field;

            private string address3Field;

            private string stateCodeField;

            private string zipCodeField;

            private string zipExtensionCodeField;

            private string cityNameField;

            private string countyCodeField;

            private memberAlternatePaymentContactRecipientAddressCountryCode countryCodeField;

            private string longitudeField;

            private string latitudeField;

            private string ignoreAddressCheckField;

            private string undeliverableAddressField;

            /// <remarks/>
            public string address
            {
                get
                {
                    return this.addressField;
                }
                set
                {
                    this.addressField = value;
                }
            }

            /// <remarks/>
            public string address2
            {
                get
                {
                    return this.address2Field;
                }
                set
                {
                    this.address2Field = value;
                }
            }

            /// <remarks/>
            public string address3
            {
                get
                {
                    return this.address3Field;
                }
                set
                {
                    this.address3Field = value;
                }
            }

            /// <remarks/>
            public string stateCode
            {
                get
                {
                    return this.stateCodeField;
                }
                set
                {
                    this.stateCodeField = value;
                }
            }

            /// <remarks/>
            public string zipCode
            {
                get
                {
                    return this.zipCodeField;
                }
                set
                {
                    this.zipCodeField = value;
                }
            }

            /// <remarks/>
            public string zipExtensionCode
            {
                get
                {
                    return this.zipExtensionCodeField;
                }
                set
                {
                    this.zipExtensionCodeField = value;
                }
            }

            /// <remarks/>
            public string cityName
            {
                get
                {
                    return this.cityNameField;
                }
                set
                {
                    this.cityNameField = value;
                }
            }

            /// <remarks/>
            public string countyCode
            {
                get
                {
                    return this.countyCodeField;
                }
                set
                {
                    this.countyCodeField = value;
                }
            }

            /// <remarks/>
            public memberAlternatePaymentContactRecipientAddressCountryCode countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }

            /// <remarks/>
            public string longitude
            {
                get
                {
                    return this.longitudeField;
                }
                set
                {
                    this.longitudeField = value;
                }
            }

            /// <remarks/>
            public string latitude
            {
                get
                {
                    return this.latitudeField;
                }
                set
                {
                    this.latitudeField = value;
                }
            }

            /// <remarks/>
            public string ignoreAddressCheck
            {
                get
                {
                    return this.ignoreAddressCheckField;
                }
                set
                {
                    this.ignoreAddressCheckField = value;
                }
            }

            /// <remarks/>
            public string undeliverableAddress
            {
                get
                {
                    return this.undeliverableAddressField;
                }
                set
                {
                    this.undeliverableAddressField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class memberAlternatePaymentContactRecipientAddressCountryCode
        {

            private string countryCodeField;

            /// <remarks/>
            public string countryCode
            {
                get
                {
                    return this.countryCodeField;
                }
                set
                {
                    this.countryCodeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class errorsError
        {

            private string errorTypeField;

            private string messageField;

            private string dateTimeField;

            private string authenticatedUserField;

            private string traceInfoField;

            /// <remarks/>
            public string errorType
            {
                get
                {
                    return this.errorTypeField;
                }
                set
                {
                    this.errorTypeField = value;
                }
            }

            /// <remarks/>
            public string message
            {
                get
                {
                    return this.messageField;
                }
                set
                {
                    this.messageField = value;
                }
            }

            /// <remarks/>
            public string dateTime
            {
                get
                {
                    return this.dateTimeField;
                }
                set
                {
                    this.dateTimeField = value;
                }
            }

            /// <remarks/>
            public string authenticatedUser
            {
                get
                {
                    return this.authenticatedUserField;
                }
                set
                {
                    this.authenticatedUserField = value;
                }
            }

            /// <remarks/>
            public string traceInfo
            {
                get
                {
                    return this.traceInfoField;
                }
                set
                {
                    this.traceInfoField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class errors
        {

            private errorsError[] errorField;

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("error")]
            public errorsError[] error
            {
                get
                {
                    return this.errorField;
                }
                set
                {
                    this.errorField = value;
                }
            }
        }



    }
}

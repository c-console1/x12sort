using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.HealthRulesConnectors.Models.Responses
{
    interface IResponse
    {
        /// <summary>
        /// Request object
        /// </summary>
        string XMLRequest { get; set; }

        /// <summary>
        /// Response object
        /// </summary>
        string XMLResponse { get; set; }

    }
}

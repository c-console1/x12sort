using System;
using System.Collections.Generic;

namespace DataLayer.HealthRulesConnectors.Models.Responses
{
    public class FindMemberResponse
    {

        public class Response
        {
            public EnvelopeInner Envelope { get; set; }

            /// <summary>
            /// Member ID list
            /// </summary>
            public List<string> memberIds
            {
                get
                {

                    try
                    {
                        return Envelope.Body.findMembersResponse.memberId;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

        }

        public class EnvelopeInner
        {
            public string Header { get; set; }
            public Body Body { get; set; }
        }

        public class Body
        {
            public FindMembersResponse findMembersResponse { get; set; }
        }

        public class FindMembersResponse
        {
            public List<string> memberId { get; set; }
        }

    }
}

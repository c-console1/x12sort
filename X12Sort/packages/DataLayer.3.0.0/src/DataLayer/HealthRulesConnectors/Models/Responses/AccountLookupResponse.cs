using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.HealthRulesConnectors.Models.Responses
{
    public class AccountLookupResponse
    {

        public class Response
        {
            public InnerEnvelope Envelope { get; set; }
        }

        public class InnerEnvelope
        {
            public string Header { get; set; }
            public Body Body { get; set; }
        }

        public class Body
        {
            public AccountLookupListResponse accountLookupListResponse { get; set; }
        }

        public class AccountLookupListResponse
        {
            public List<Account> account { get; set; }
            public List<string> status { get; set; }
            public Errors errors { get; set; }
        }

        public class Account
        {
            public string accountHccIdentifier { get; set; }
            public string accountName { get; set; }
            public string accountReferenceIdentifier { get; set; }
            public string parentAccountHccIdentifier { get; set; }
            public string parentAccountName { get; set; }
            public string parentAccountReferenceIdentifier { get; set; }
            public string topAccountHccIdentifier { get; set; }
            public string topAccountName { get; set; }
            public string topAccountReferenceIdentifier { get; set; }
            public List<OtherNames> otherNames { get; set; }
            public List<OtherIDs> otherIDs { get; set; }
            public string taxId { get; set; }
        }

        public class OtherNames
        {
            public string otherName { get; set; }
            public OtherNameTypeCode otherNameTypeCode { get; set; }
        }

        public class OtherNameTypeCode
        {
            public string codeEntry { get; set; }
            public string shortName { get; set; }
            public string shortDescription { get; set; }
        }

        public class OtherIDs
        {
            public string otherID { get; set; }
            public OtherIDTypeCode otherIDTypeCode { get; set; }
        }

        public class OtherIDTypeCode
        {
            public string codeEntry { get; set; }
            public string shortName { get; set; }
            public string shortDescription { get; set; }
        }

        public class Errors
        {
            public List<Error> error { get; set; }
        }

        public class Error
        {
            public string errorType { get; set; }
            public string message { get; set; }
            public string dateTime { get; set; }
            public string authenticatedUser { get; set; }
            public string traceInfo { get; set; }
        }

    }
}

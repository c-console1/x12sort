using System.Collections.Generic;

namespace DataLayer.HealthRulesConnectors.Models.Responses
{
    /// <summary>
    /// Primary response object
    /// </summary>
    public class UpdateProviderSelectionResponse
    {
        /// <summary>
        /// Primary Response
        /// </summary>
        public class Response
        {
            /// <summary>
            /// Inner Envelope
            /// </summary>
            public EnvelopeInner Envelope { get; set; }
        }

        /// <summary>
        /// Inner Envelope
        /// </summary>
        public class EnvelopeInner
        {
            /// <summary>
            /// Header response - This should allows be blanks
            /// </summary>
            public string Header { get; set; }

            /// <summary>
            /// Primary Body
            /// </summary>
            public Body Body { get; set; }
        }

        /// <summary>
        /// Primary body object
        /// </summary>
        public class Body
        {
            public UpdateProviderSelectionResponseInner updateProviderSelectionResponse { get; set; }
        }

        public class UpdateProviderSelectionResponseInner
        {
            public TransactionInformation transactionInformation { get; set; }
        }

        public class TransactionInformation
        {
            public string alreadyInWorkbasket { get; set; }
            public string blockedByLock { get; set; }
            public string cvcId { get; set; }
            public List<Exceptions> exceptions { get; set; }
            public string lockingUser { get; set; }
            public string needsRepair { get; set; }
            public string needsReview { get; set; }
            public string rejected { get; set; }
            public string transactionSubmitted { get; set; }
            public string versionEffectiveDate { get; set; }
        }

        public class Exceptions
        {
            public string exceptionType { get; set; }
            public string messageCode { get; set; }
            public string messageDescription { get; set; }
            public string policyName { get; set; }
        }

    }
}

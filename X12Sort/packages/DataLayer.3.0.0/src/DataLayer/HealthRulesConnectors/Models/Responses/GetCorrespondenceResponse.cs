using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.HealthRulesConnectors.Models.Responses
{
    /// <summary>
    /// Find a Correspondence with a member
    /// </summary>
    public class GetCorrespondenceResponse
    {
        /// <summary>
        /// Reponse object that holds all the values for the GetCorrespondence Connector Call
        /// This is the root of the response object
        /// </summary>

        public class Response
        {
            /// <summary>
            /// Envelope - This where all the objects branch from
            /// </summary>
            public InnerEnvelope Envelope { get; set; }

            /// <summary>
            /// Primary correspondence results
            /// Alias to: Envelope.Body.getCorrespondenceResponse.correspondence;
            /// </summary>
            public Correspondence correspondence
            {
                get
                {

                    try
                    {
                        return Envelope.Body.getCorrespondenceResponse.correspondence;
                    }
                    catch (Exception)
                    {
                        return null;
                    }

                }
            }
        }

        /// <summary>
        /// Primary Envelope Object
        /// </summary>
        public class InnerEnvelope : Fault
        {
            /// <summary>
            /// Text header - This should always be NULL or Blank
            /// </summary>
            public string Header { get; set; }

            /// <summary>
            /// Main Body, most values should be in here
            /// </summary>
            public Body Body { get; set; }
        }
        /// <summary>
        /// Main Body, most values should be in here
        /// </summary>
        public class Body
        {
            /// <summary>
            /// GetCorrespondence Response Content
            /// </summary>
            public GetCorrespondenceChildResponse getCorrespondenceResponse { get; set; }
        }

        /// <summary>
        /// Inner child to the getCorrespondenceResponse object
        /// </summary>
        public class GetCorrespondenceChildResponse
        {
            /// <summary>
            /// Primary correspondence results
            /// </summary>
            public Correspondence correspondence { get; set; }
        }

        /// <summary>
        /// Correspondence values
        /// </summary>
        public class Correspondence
        {
            /// <summary>
            /// HHC ID
            /// </summary>
            public string id { get; set; }
            public List<StatusTimeList> statusTimeList { get; set; }
            public string url { get; set; }
            public string description { get; set; }
            public string definition { get; set; }
            public Recipient recipient { get; set; }
            public Subject subject { get; set; }
            public List<ManuallyEnteredFields> manuallyEnteredFields { get; set; }
        }

        public class StatusTimeList
        {
            public string correspondenceStatus { get; set; }
            public string setOnTime { get; set; }
        }

        public class Recipient
        {
            public string hccId { get; set; }
            public string recipientType { get; set; }
        }

        public class Subject
        {
            public string hccId { get; set; }
            public string subjectType { get; set; }
        }

        public class ManuallyEnteredFields
        {
            public string name { get; set; }
            public string value { get; set; }
        }

    }
}

using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace DataLayer.HealthRulesConnectors.Models.Responses
{

    public class PractitionerLookup
    {

        public class Response : IResponse
        {
            /// <summary>
            /// Response Envelope
            /// <summary>
            [JsonProperty("Envelope")]
            [XmlElement(ElementName = "Envelope")]
            public DocumentEnvelope Envelope { get; set; }


            public string XMLRequest { get; set; }

            public string XMLResponse { get; set; }

            /// <summary>
            /// Details on 0 to many practitioners
            /// </summary>
            public List<PractitionerLookupListResponsePractitionerDetail> PractitionerDetail
            {
                get {

                    try
                    {
                        return this.Envelope.Body.PractitionerLookupListResponse.PractitionerDetail;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// Number of practitioners found
            /// </summary>
            public int Count
            {
                get
                {
                    try
                    {
                        return this.Envelope.Body.PractitionerLookupListResponse.PractitionerDetail.Count;
                    }
                    catch (Exception)
                    {
                        return 0;
                    }
                }
            }

            /// <summary>
            /// First practitioner found
            /// </summary>
            public PractitionerLookupListResponsePractitionerDetail First
            {
                get
                {
                    try
                    {
                        return this.Envelope.Body.PractitionerLookupListResponse.PractitionerDetail[0];
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

        }

        /// <summary>
        /// DocumentEnvelope
        /// <summary>
        public class DocumentEnvelope
        {
            /// <summary>
            /// Header
            /// <summary>
            [JsonProperty("Header")]
            [XmlElement(ElementName = "Header")]
            public string Header { get; set; }

            /// <summary>
            /// Body
            /// <summary>
            [JsonProperty("Body")]
            [XmlElement(ElementName = "Body")]
            public EnvelopeBody Body { get; set; }

        }

        /// <summary>
        /// EnvelopeBody
        /// <summary>
        public class EnvelopeBody
        {
            /// <summary>
            /// practitionerLookupListResponse
            /// <summary>
            [JsonProperty("practitionerLookupListResponse")]
            [XmlElement(ElementName = "practitionerLookupListResponse")]
            public BodyPractitionerLookupListResponse PractitionerLookupListResponse { get; set; }

        }

        /// <summary>
        /// BodyPractitionerLookupListResponse
        /// <summary>
        public class BodyPractitionerLookupListResponse
        {
            /// <summary>
            /// practitionerDetail
            /// Zero or more repetitions:
            /// <summary>
            [JsonProperty("practitionerDetail")]
            [XmlElement(ElementName = "practitionerDetail")]
            public List<PractitionerLookupListResponsePractitionerDetail> PractitionerDetail { get; set; }

            /// <summary>
            /// status
            /// <summary>
            [JsonProperty("status")]
            [XmlElement(ElementName = "status")]
            public string Status { get; set; }

            /// <summary>
            /// errors
            /// Optional:
            /// <summary>
            [JsonProperty("errors")]
            [XmlElement(ElementName = "errors")]
            public PractitionerLookupListResponseErrors Errors { get; set; }

        }

        /// <summary>
        /// PractitionerLookupListResponseErrors
        /// Optional:
        /// <summary>
        public class PractitionerLookupListResponseErrors
        {
            /// <summary>
            /// error
            /// Zero or more repetitions:
            /// <summary>
            [JsonProperty("error")]
            [XmlElement(ElementName = "error")]
            public List<ErrorsError> Error { get; set; }

        }

        /// <summary>
        /// ErrorsError
        /// Zero or more repetitions:
        /// <summary>
        public class ErrorsError
        {
            /// <summary>
            /// errorType
            /// Optional:
            /// <summary>
            [JsonProperty("errorType")]
            [XmlElement(ElementName = "errorType")]
            public string ErrorType { get; set; }

            /// <summary>
            /// message
            /// <summary>
            [JsonProperty("message")]
            [XmlElement(ElementName = "message")]
            public string Message { get; set; }

            /// <summary>
            /// dateTime
            /// Optional:
            /// <summary>
            [JsonProperty("dateTime")]
            [XmlElement(ElementName = "dateTime")]
            public string DateTime { get; set; }

            /// <summary>
            /// authenticatedUser
            /// Optional:
            /// <summary>
            [JsonProperty("authenticatedUser")]
            [XmlElement(ElementName = "authenticatedUser")]
            public string AuthenticatedUser { get; set; }

            /// <summary>
            /// traceInfo
            /// Optional:
            /// <summary>
            [JsonProperty("traceInfo")]
            [XmlElement(ElementName = "traceInfo")]
            public string TraceInfo { get; set; }

        }

        /// <summary>
        /// PractitionerLookupListResponsePractitionerDetail
        /// Zero or more repetitions:
        /// <summary>
        public class PractitionerLookupListResponsePractitionerDetail
        {
            /// <summary>
            /// hccID
            /// Optional:
            /// <summary>
            [JsonProperty("hccID")]
            [XmlElement(ElementName = "hccID")]
            public string HccID { get; set; }

            /// <summary>
            /// practitionerIdentifier
            /// Optional:
            /// <summary>
            [JsonProperty("practitionerIdentifier")]
            [XmlElement(ElementName = "practitionerIdentifier")]
            public string PractitionerIdentifier { get; set; }

            /// <summary>
            /// nationalProviderId
            /// Optional:
            /// <summary>
            [JsonProperty("nationalProviderId")]
            [XmlElement(ElementName = "nationalProviderId")]
            public string NationalProviderId { get; set; }

            /// <summary>
            /// pcp
            /// Optional:
            /// <summary>
            [JsonProperty("pcp")]
            [XmlElement(ElementName = "pcp")]
            public string Pcp { get; set; }

            /// <summary>
            /// isAutoAssignedAsPCP
            /// Optional:
            /// <summary>
            [JsonProperty("isAutoAssignedAsPCP")]
            [XmlElement(ElementName = "isAutoAssignedAsPCP")]
            public string IsAutoAssignedAsPCP { get; set; }

            /// <summary>
            /// excludeFromProviderDir
            /// Optional:
            /// <summary>
            [JsonProperty("excludeFromProviderDir")]
            [XmlElement(ElementName = "excludeFromProviderDir")]
            public string ExcludeFromProviderDir { get; set; }

            /// <summary>
            /// effectiveStartDate
            /// Optional:
            /// <summary>
            [JsonProperty("effectiveStartDate")]
            [XmlElement(ElementName = "effectiveStartDate")]
            public string EffectiveStartDate { get; set; }

            /// <summary>
            /// endorsementEffectiveDate
            /// Optional:
            /// <summary>
            [JsonProperty("endorsementEffectiveDate")]
            [XmlElement(ElementName = "endorsementEffectiveDate")]
            public string EndorsementEffectiveDate { get; set; }

            /// <summary>
            /// endorsementExpirationDate
            /// Optional:
            /// <summary>
            [JsonProperty("endorsementExpirationDate")]
            [XmlElement(ElementName = "endorsementExpirationDate")]
            public string EndorsementExpirationDate { get; set; }

            /// <summary>
            /// lastName
            /// <summary>
            [JsonProperty("lastName")]
            [XmlElement(ElementName = "lastName")]
            public string LastName { get; set; }

            /// <summary>
            /// firstName
            /// Optional:
            /// <summary>
            [JsonProperty("firstName")]
            [XmlElement(ElementName = "firstName")]
            public string FirstName { get; set; }

            /// <summary>
            /// middleName
            /// Optional:
            /// <summary>
            [JsonProperty("middleName")]
            [XmlElement(ElementName = "middleName")]
            public string MiddleName { get; set; }

            /// <summary>
            /// namePrefix
            /// Optional:
            /// <summary>
            [JsonProperty("namePrefix")]
            [XmlElement(ElementName = "namePrefix")]
            public string NamePrefix { get; set; }

            /// <summary>
            /// nameSuffix
            /// Optional:
            /// <summary>
            [JsonProperty("nameSuffix")]
            [XmlElement(ElementName = "nameSuffix")]
            public string NameSuffix { get; set; }

            /// <summary>
            /// nameSuffixList
            /// Optional:
            /// <summary>
            [JsonProperty("nameSuffixList")]
            [XmlElement(ElementName = "nameSuffixList")]
            public PractitionerDetailNameSuffixList NameSuffixList { get; set; }

            /// <summary>
            /// credentialSuffixList
            /// Optional:
            /// <summary>
            [JsonProperty("credentialSuffixList")]
            [XmlElement(ElementName = "credentialSuffixList")]
            public PractitionerDetailCredentialSuffixList CredentialSuffixList { get; set; }

            /// <summary>
            /// genderCode
            /// Optional:
            /// <summary>
            [JsonProperty("genderCode")]
            [XmlElement(ElementName = "genderCode")]
            public string GenderCode { get; set; }

            /// <summary>
            /// ethnicityCode
            /// Optional:
            /// <summary>
            [JsonProperty("ethnicityCode")]
            [XmlElement(ElementName = "ethnicityCode")]
            public string EthnicityCode { get; set; }

            /// <summary>
            /// primaryLanguage
            /// Optional:
            /// <summary>
            [JsonProperty("primaryLanguage")]
            [XmlElement(ElementName = "primaryLanguage")]
            public string PrimaryLanguage { get; set; }

            /// <summary>
            /// dateOfBirth
            /// Optional:
            /// <summary>
            [JsonProperty("dateOfBirth")]
            [XmlElement(ElementName = "dateOfBirth")]
            public string DateOfBirth { get; set; }

            /// <summary>
            /// activeCode
            /// Optional:
            /// <summary>
            [JsonProperty("activeCode")]
            [XmlElement(ElementName = "activeCode")]
            public string ActiveCode { get; set; }

            /// <summary>
            /// firstActiveDate
            /// Optional:
            /// <summary>
            [JsonProperty("firstActiveDate")]
            [XmlElement(ElementName = "firstActiveDate")]
            public string FirstActiveDate { get; set; }

            /// <summary>
            /// practitionerCredential
            /// Zero or more repetitions:
            /// <summary>
            [JsonProperty("practitionerCredential")]
            [XmlElement(ElementName = "practitionerCredential")]
            public List<PractitionerDetailPractitionerCredential> PractitionerCredential { get; set; }

            /// <summary>
            /// practitionerLanguage
            /// Zero or more repetitions:
            /// <summary>
            [JsonProperty("practitionerLanguage")]
            [XmlElement(ElementName = "practitionerLanguage")]
            public List<PractitionerDetailPractitionerLanguage> PractitionerLanguage { get; set; }

            /// <summary>
            /// practitionerIdDetail
            /// Zero or more repetitions:
            /// <summary>
            [JsonProperty("practitionerIdDetail")]
            [XmlElement(ElementName = "practitionerIdDetail")]
            public List<PractitionerDetailPractitionerIdDetail> PractitionerIdDetail { get; set; }

            /// <summary>
            /// correspondenceAddress
            /// Optional:
            /// <summary>
            [JsonProperty("correspondenceAddress")]
            [XmlElement(ElementName = "correspondenceAddress")]
            public PractitionerDetailCorrespondenceAddress CorrespondenceAddress { get; set; }

            /// <summary>
            /// practitionerLicense
            /// Zero or more repetitions:
            /// <summary>
            [JsonProperty("practitionerLicense")]
            [XmlElement(ElementName = "practitionerLicense")]
            public List<PractitionerDetailPractitionerLicense> PractitionerLicense { get; set; }

            /// <summary>
            /// primarySpecialty
            /// Optional:
            /// <summary>
            [JsonProperty("primarySpecialty")]
            [XmlElement(ElementName = "primarySpecialty")]
            public PractitionerDetailPrimarySpecialty PrimarySpecialty { get; set; }

            /// <summary>
            /// secondarySpecialty
            /// Optional:
            /// <summary>
            [JsonProperty("secondarySpecialty")]
            [XmlElement(ElementName = "secondarySpecialty")]
            public PractitionerDetailSecondarySpecialty SecondarySpecialty { get; set; }

            /// <summary>
            /// specialtyList
            /// Optional:
            /// <summary>
            [JsonProperty("specialtyList")]
            [XmlElement(ElementName = "specialtyList")]
            public PractitionerDetailSpecialtyList SpecialtyList { get; set; }

            /// <summary>
            /// practitionerCredentialing
            /// Optional:
            /// <summary>
            [JsonProperty("practitionerCredentialing")]
            [XmlElement(ElementName = "practitionerCredentialing")]
            public PractitionerDetailPractitionerCredentialing PractitionerCredentialing { get; set; }

            /// <summary>
            /// practitionerUDTList
            /// Optional:
            /// <summary>
            [JsonProperty("practitionerUDTList")]
            [XmlElement(ElementName = "practitionerUDTList")]
            public PractitionerDetailPractitionerUDTList PractitionerUDTList { get; set; }

        }

        /// <summary>
        /// PractitionerDetailPractitionerUDTList
        /// Optional:
        /// <summary>
        public class PractitionerDetailPractitionerUDTList
        {
            /// <summary>
            /// udtList
            /// Zero or more repetitions:
            /// <summary>
            [JsonProperty("udtList")]
            [XmlElement(ElementName = "udtList")]
            public List<PractitionerUDTListUdtList> UdtList { get; set; }

        }

        /// <summary>
        /// PractitionerUDTListUdtList
        /// Zero or more repetitions:
        /// <summary>
        public class PractitionerUDTListUdtList
        {
            /// <summary>
            /// udtListValueSet
            /// Zero or more repetitions:
            /// <summary>
            [JsonProperty("udtListValueSet")]
            [XmlElement(ElementName = "udtListValueSet")]
            public List<UdtListUdtListValueSet> UdtListValueSet { get; set; }

            /// <summary>
            /// userDefinedTermReference
            /// Optional:
            /// <summary>
            [JsonProperty("userDefinedTermReference")]
            [XmlElement(ElementName = "userDefinedTermReference")]
            public UdtListUserDefinedTermReference UserDefinedTermReference { get; set; }

        }

        /// <summary>
        /// UdtListUserDefinedTermReference
        /// Optional:
        /// <summary>
        public class UdtListUserDefinedTermReference
        {
            /// <summary>
            /// ID
            /// Optional:
            /// <summary>
            [JsonProperty("ID")]
            [XmlElement(ElementName = "ID")]
            public string ID { get; set; }

        }

        /// <summary>
        /// UdtListUdtListValueSet
        /// Zero or more repetitions:
        /// <summary>
        public class UdtListUdtListValueSet
        {
            /// <summary>
            /// attributeRoleName
            /// Optional:
            /// <summary>
            [JsonProperty("attributeRoleName")]
            [XmlElement(ElementName = "attributeRoleName")]
            public string AttributeRoleName { get; set; }

            /// <summary>
            /// attrValueAsString
            /// Optional:
            /// <summary>
            [JsonProperty("attrValueAsString")]
            [XmlElement(ElementName = "attrValueAsString")]
            public string AttrValueAsString { get; set; }

        }

        /// <summary>
        /// PractitionerDetailPractitionerCredentialing
        /// Optional:
        /// <summary>
        public class PractitionerDetailPractitionerCredentialing
        {
            /// <summary>
            /// medicalSchoolInfo
            /// Zero or more repetitions:
            /// <summary>
            [JsonProperty("medicalSchoolInfo")]
            [XmlElement(ElementName = "medicalSchoolInfo")]
            public List<PractitionerCredentialingMedicalSchoolInfo> MedicalSchoolInfo { get; set; }

            /// <summary>
            /// residencyInfo
            /// Zero or more repetitions:
            /// <summary>
            [JsonProperty("residencyInfo")]
            [XmlElement(ElementName = "residencyInfo")]
            public List<PractitionerCredentialingResidencyInfo> ResidencyInfo { get; set; }

        }

        /// <summary>
        /// PractitionerCredentialingResidencyInfo
        /// Zero or more repetitions:
        /// <summary>
        public class PractitionerCredentialingResidencyInfo
        {
            /// <summary>
            /// residencyProgramName
            /// Optional:
            /// <summary>
            [JsonProperty("residencyProgramName")]
            [XmlElement(ElementName = "residencyProgramName")]
            public string ResidencyProgramName { get; set; }

            /// <summary>
            /// residencySpecialty
            /// Optional:
            /// <summary>
            [JsonProperty("residencySpecialty")]
            [XmlElement(ElementName = "residencySpecialty")]
            public ResidencyInfoResidencySpecialty ResidencySpecialty { get; set; }

        }

        /// <summary>
        /// ResidencyInfoResidencySpecialty
        /// Optional:
        /// <summary>
        public class ResidencyInfoResidencySpecialty
        {
            /// <summary>
            /// specialtyCode
            /// Optional:
            /// <summary>
            [JsonProperty("specialtyCode")]
            [XmlElement(ElementName = "specialtyCode")]
            public string SpecialtyCode { get; set; }

            /// <summary>
            /// specialtyName
            /// Optional:
            /// <summary>
            [JsonProperty("specialtyName")]
            [XmlElement(ElementName = "specialtyName")]
            public string SpecialtyName { get; set; }

        }

        /// <summary>
        /// PractitionerCredentialingMedicalSchoolInfo
        /// Zero or more repetitions:
        /// <summary>
        public class PractitionerCredentialingMedicalSchoolInfo
        {
            /// <summary>
            /// schoolName
            /// Optional:
            /// <summary>
            [JsonProperty("schoolName")]
            [XmlElement(ElementName = "schoolName")]
            public string SchoolName { get; set; }

        }

        /// <summary>
        /// PractitionerDetailSpecialtyList
        /// Optional:
        /// <summary>
        public class PractitionerDetailSpecialtyList
        {
            /// <summary>
            /// specialty
            /// Zero or more repetitions:
            /// <summary>
            [JsonProperty("specialty")]
            [XmlElement(ElementName = "specialty")]
            public List<SpecialtyListSpecialty> Specialty { get; set; }

        }

        /// <summary>
        /// SpecialtyListSpecialty
        /// Zero or more repetitions:
        /// <summary>
        public class SpecialtyListSpecialty
        {
            /// <summary>
            /// specialtyCode
            /// Optional:
            /// <summary>
            [JsonProperty("specialtyCode")]
            [XmlElement(ElementName = "specialtyCode")]
            public string SpecialtyCode { get; set; }

            /// <summary>
            /// specialtyName
            /// Optional:
            /// <summary>
            [JsonProperty("specialtyName")]
            [XmlElement(ElementName = "specialtyName")]
            public string SpecialtyName { get; set; }

        }

        /// <summary>
        /// PractitionerDetailSecondarySpecialty
        /// Optional:
        /// <summary>
        public class PractitionerDetailSecondarySpecialty
        {
            /// <summary>
            /// specialtyCode
            /// Optional:
            /// <summary>
            [JsonProperty("specialtyCode")]
            [XmlElement(ElementName = "specialtyCode")]
            public string SpecialtyCode { get; set; }

            /// <summary>
            /// specialtyName
            /// Optional:
            /// <summary>
            [JsonProperty("specialtyName")]
            [XmlElement(ElementName = "specialtyName")]
            public string SpecialtyName { get; set; }

        }

        /// <summary>
        /// PractitionerDetailPrimarySpecialty
        /// Optional:
        /// <summary>
        public class PractitionerDetailPrimarySpecialty
        {
            /// <summary>
            /// specialtyCode
            /// Optional:
            /// <summary>
            [JsonProperty("specialtyCode")]
            [XmlElement(ElementName = "specialtyCode")]
            public string SpecialtyCode { get; set; }

            /// <summary>
            /// specialtyName
            /// Optional:
            /// <summary>
            [JsonProperty("specialtyName")]
            [XmlElement(ElementName = "specialtyName")]
            public string SpecialtyName { get; set; }

        }

        /// <summary>
        /// PractitionerDetailPractitionerLicense
        /// Zero or more repetitions:
        /// <summary>
        public class PractitionerDetailPractitionerLicense
        {
            /// <summary>
            /// licenseNumber
            /// Optional:
            /// <summary>
            [JsonProperty("licenseNumber")]
            [XmlElement(ElementName = "licenseNumber")]
            public string LicenseNumber { get; set; }

            /// <summary>
            /// licenseStateCode
            /// Optional:
            /// <summary>
            [JsonProperty("licenseStateCode")]
            [XmlElement(ElementName = "licenseStateCode")]
            public string LicenseStateCode { get; set; }

            /// <summary>
            /// effectiveStartDate
            /// Optional:
            /// <summary>
            [JsonProperty("effectiveStartDate")]
            [XmlElement(ElementName = "effectiveStartDate")]
            public string EffectiveStartDate { get; set; }

            /// <summary>
            /// effectiveEndDate
            /// Optional:
            /// <summary>
            [JsonProperty("effectiveEndDate")]
            [XmlElement(ElementName = "effectiveEndDate")]
            public string EffectiveEndDate { get; set; }

            /// <summary>
            /// typeOfLicense
            /// Optional:
            /// <summary>
            [JsonProperty("typeOfLicense")]
            [XmlElement(ElementName = "typeOfLicense")]
            public string TypeOfLicense { get; set; }

            /// <summary>
            /// licenseTerminateReason
            /// Optional:
            /// <summary>
            [JsonProperty("licenseTerminateReason")]
            [XmlElement(ElementName = "licenseTerminateReason")]
            public string LicenseTerminateReason { get; set; }

        }

        /// <summary>
        /// PractitionerDetailCorrespondenceAddress
        /// Optional:
        /// <summary>
        public class PractitionerDetailCorrespondenceAddress
        {
            /// <summary>
            /// emailAddress
            /// Optional:
            /// <summary>
            [JsonProperty("emailAddress")]
            [XmlElement(ElementName = "emailAddress")]
            public string EmailAddress { get; set; }

            /// <summary>
            /// correspondenceAddress
            /// Optional:
            /// <summary>
            [JsonProperty("correspondenceAddress")]
            [XmlElement(ElementName = "correspondenceAddress")]
            public CorrespondenceAddressCorrespondenceAddress CorrespondenceAddress { get; set; }

            /// <summary>
            /// phoneNumber
            /// Zero or more repetitions:
            /// <summary>
            [JsonProperty("phoneNumber")]
            [XmlElement(ElementName = "phoneNumber")]
            public List<CorrespondenceAddressPhoneNumber> PhoneNumber { get; set; }

        }

        /// <summary>
        /// CorrespondenceAddressPhoneNumber
        /// Zero or more repetitions:
        /// <summary>
        public class CorrespondenceAddressPhoneNumber
        {
            /// <summary>
            /// phoneCountryCode
            /// Optional:
            /// <summary>
            [JsonProperty("phoneCountryCode")]
            [XmlElement(ElementName = "phoneCountryCode")]
            public string PhoneCountryCode { get; set; }

            /// <summary>
            /// phoneAreaCode
            /// Optional:
            /// <summary>
            [JsonProperty("phoneAreaCode")]
            [XmlElement(ElementName = "phoneAreaCode")]
            public string PhoneAreaCode { get; set; }

            /// <summary>
            /// phoneNumber
            /// Optional:
            /// <summary>
            [JsonProperty("phoneNumber")]
            [XmlElement(ElementName = "phoneNumber")]
            public List<string> PhoneNumber { get; set; }

            /// <summary>
            /// phoneExtensionNumber
            /// Optional:
            /// <summary>
            [JsonProperty("phoneExtensionNumber")]
            [XmlElement(ElementName = "phoneExtensionNumber")]
            public string PhoneExtensionNumber { get; set; }

            /// <summary>
            /// individualPhoneTypeCode
            /// Optional:
            /// <summary>
            [JsonProperty("individualPhoneTypeCode")]
            [XmlElement(ElementName = "individualPhoneTypeCode")]
            public PhoneNumberIndividualPhoneTypeCode IndividualPhoneTypeCode { get; set; }

            /// <summary>
            /// organizationPhoneTypeCode
            /// Optional:
            /// <summary>
            [JsonProperty("organizationPhoneTypeCode")]
            [XmlElement(ElementName = "organizationPhoneTypeCode")]
            public PhoneNumberOrganizationPhoneTypeCode OrganizationPhoneTypeCode { get; set; }

        }

        /// <summary>
        /// PhoneNumberOrganizationPhoneTypeCode
        /// Optional:
        /// <summary>
        public class PhoneNumberOrganizationPhoneTypeCode
        {
            /// <summary>
            /// codeSetName
            /// Optional:
            /// <summary>
            [JsonProperty("codeSetName")]
            [XmlElement(ElementName = "codeSetName")]
            public string CodeSetName { get; set; }

            /// <summary>
            /// codeEntry
            /// Optional:
            /// <summary>
            [JsonProperty("codeEntry")]
            [XmlElement(ElementName = "codeEntry")]
            public string CodeEntry { get; set; }

            /// <summary>
            /// shortName
            /// Optional:
            /// <summary>
            [JsonProperty("shortName")]
            [XmlElement(ElementName = "shortName")]
            public string ShortName { get; set; }

        }

        /// <summary>
        /// PhoneNumberIndividualPhoneTypeCode
        /// Optional:
        /// <summary>
        public class PhoneNumberIndividualPhoneTypeCode
        {
            /// <summary>
            /// codeSetName
            /// Optional:
            /// <summary>
            [JsonProperty("codeSetName")]
            [XmlElement(ElementName = "codeSetName")]
            public string CodeSetName { get; set; }

            /// <summary>
            /// codeEntry
            /// Optional:
            /// <summary>
            [JsonProperty("codeEntry")]
            [XmlElement(ElementName = "codeEntry")]
            public string CodeEntry { get; set; }

            /// <summary>
            /// shortName
            /// Optional:
            /// <summary>
            [JsonProperty("shortName")]
            [XmlElement(ElementName = "shortName")]
            public string ShortName { get; set; }

        }

        /// <summary>
        /// CorrespondenceAddressCorrespondenceAddress
        /// Optional:
        /// <summary>
        public class CorrespondenceAddressCorrespondenceAddress
        {
            /// <summary>
            /// address
            /// Optional:
            /// <summary>
            [JsonProperty("address")]
            [XmlElement(ElementName = "address")]
            public string Address { get; set; }

            /// <summary>
            /// address2
            /// Optional:
            /// <summary>
            [JsonProperty("address2")]
            [XmlElement(ElementName = "address2")]
            public string Address2 { get; set; }

            /// <summary>
            /// address3
            /// Optional:
            /// <summary>
            [JsonProperty("address3")]
            [XmlElement(ElementName = "address3")]
            public string Address3 { get; set; }

            /// <summary>
            /// stateCode
            /// Optional:
            /// <summary>
            [JsonProperty("stateCode")]
            [XmlElement(ElementName = "stateCode")]
            public string StateCode { get; set; }

            /// <summary>
            /// zipCode
            /// Optional:
            /// <summary>
            [JsonProperty("zipCode")]
            [XmlElement(ElementName = "zipCode")]
            public string ZipCode { get; set; }

            /// <summary>
            /// zipExtensionCode
            /// Optional:
            /// <summary>
            [JsonProperty("zipExtensionCode")]
            [XmlElement(ElementName = "zipExtensionCode")]
            public string ZipExtensionCode { get; set; }

            /// <summary>
            /// cityName
            /// Optional:
            /// <summary>
            [JsonProperty("cityName")]
            [XmlElement(ElementName = "cityName")]
            public string CityName { get; set; }

            /// <summary>
            /// countyCode
            /// Optional:
            /// <summary>
            [JsonProperty("countyCode")]
            [XmlElement(ElementName = "countyCode")]
            public string CountyCode { get; set; }

            /// <summary>
            /// countryCode
            /// Optional:
            /// <summary>
            [JsonProperty("countryCode")]
            [XmlElement(ElementName = "countryCode")]
            public CorrespondenceAddressCountryCode CountryCode { get; set; }

            /// <summary>
            /// longitude
            /// Optional:
            /// <summary>
            [JsonProperty("longitude")]
            [XmlElement(ElementName = "longitude")]
            public string Longitude { get; set; }

            /// <summary>
            /// latitude
            /// Optional:
            /// <summary>
            [JsonProperty("latitude")]
            [XmlElement(ElementName = "latitude")]
            public string Latitude { get; set; }

            /// <summary>
            /// ignoreAddressCheck
            /// Optional:
            /// <summary>
            [JsonProperty("ignoreAddressCheck")]
            [XmlElement(ElementName = "ignoreAddressCheck")]
            public string IgnoreAddressCheck { get; set; }

            /// <summary>
            /// undeliverableAddress
            /// Optional:
            /// <summary>
            [JsonProperty("undeliverableAddress")]
            [XmlElement(ElementName = "undeliverableAddress")]
            public string UndeliverableAddress { get; set; }

        }

        /// <summary>
        /// CorrespondenceAddressCountryCode
        /// Optional:
        /// <summary>
        public class CorrespondenceAddressCountryCode
        {
            /// <summary>
            /// countryCode
            /// Optional:
            /// <summary>
            [JsonProperty("countryCode")]
            [XmlElement(ElementName = "countryCode")]
            public string CountryCode { get; set; }

        }

        /// <summary>
        /// PractitionerDetailPractitionerIdDetail
        /// Zero or more repetitions:
        /// <summary>
        public class PractitionerDetailPractitionerIdDetail
        {
            /// <summary>
            /// identificationNumber
            /// Optional:
            /// <summary>
            [JsonProperty("identificationNumber")]
            [XmlElement(ElementName = "identificationNumber")]
            public string IdentificationNumber { get; set; }

            /// <summary>
            /// issuingState
            /// Optional:
            /// <summary>
            [JsonProperty("issuingState")]
            [XmlElement(ElementName = "issuingState")]
            public string IssuingState { get; set; }

            /// <summary>
            /// issuingCountry
            /// Optional:
            /// <summary>
            [JsonProperty("issuingCountry")]
            [XmlElement(ElementName = "issuingCountry")]
            public PractitionerIdDetailIssuingCountry IssuingCountry { get; set; }

            /// <summary>
            /// effectiveStartDate
            /// Optional:
            /// <summary>
            [JsonProperty("effectiveStartDate")]
            [XmlElement(ElementName = "effectiveStartDate")]
            public string EffectiveStartDate { get; set; }

            /// <summary>
            /// effectiveEndDate
            /// Optional:
            /// <summary>
            [JsonProperty("effectiveEndDate")]
            [XmlElement(ElementName = "effectiveEndDate")]
            public string EffectiveEndDate { get; set; }

            /// <summary>
            /// identificationTypeCode
            /// Optional:
            /// <summary>
            [JsonProperty("identificationTypeCode")]
            [XmlElement(ElementName = "identificationTypeCode")]
            public PractitionerIdDetailIdentificationTypeCode IdentificationTypeCode { get; set; }

        }

        /// <summary>
        /// PractitionerIdDetailIdentificationTypeCode
        /// Optional:
        /// <summary>
        public class PractitionerIdDetailIdentificationTypeCode
        {
            /// <summary>
            /// codeSetName
            /// Optional:
            /// <summary>
            [JsonProperty("codeSetName")]
            [XmlElement(ElementName = "codeSetName")]
            public string CodeSetName { get; set; }

            /// <summary>
            /// codeEntry
            /// Optional:
            /// <summary>
            [JsonProperty("codeEntry")]
            [XmlElement(ElementName = "codeEntry")]
            public string CodeEntry { get; set; }

            /// <summary>
            /// shortName
            /// Optional:
            /// <summary>
            [JsonProperty("shortName")]
            [XmlElement(ElementName = "shortName")]
            public string ShortName { get; set; }

        }

        /// <summary>
        /// PractitionerIdDetailIssuingCountry
        /// Optional:
        /// <summary>
        public class PractitionerIdDetailIssuingCountry
        {
            /// <summary>
            /// countryCode
            /// Optional:
            /// <summary>
            [JsonProperty("countryCode")]
            [XmlElement(ElementName = "countryCode")]
            public string CountryCode { get; set; }

        }

        /// <summary>
        /// PractitionerDetailPractitionerLanguage
        /// Zero or more repetitions:
        /// <summary>
        public class PractitionerDetailPractitionerLanguage
        {
            /// <summary>
            /// primaryLanguage
            /// Optional:
            /// <summary>
            [JsonProperty("primaryLanguage")]
            [XmlElement(ElementName = "primaryLanguage")]
            public string PrimaryLanguage { get; set; }

            /// <summary>
            /// nativeLanguage
            /// Optional:
            /// <summary>
            [JsonProperty("nativeLanguage")]
            [XmlElement(ElementName = "nativeLanguage")]
            public string NativeLanguage { get; set; }

            /// <summary>
            /// languageDomainCode
            /// Optional:
            /// <summary>
            [JsonProperty("languageDomainCode")]
            [XmlElement(ElementName = "languageDomainCode")]
            public PractitionerLanguageLanguageDomainCode LanguageDomainCode { get; set; }

        }

        /// <summary>
        /// PractitionerLanguageLanguageDomainCode
        /// Optional:
        /// <summary>
        public class PractitionerLanguageLanguageDomainCode
        {
            /// <summary>
            /// codeSetName
            /// Optional:
            /// <summary>
            [JsonProperty("codeSetName")]
            [XmlElement(ElementName = "codeSetName")]
            public string CodeSetName { get; set; }

            /// <summary>
            /// codeEntry
            /// Optional:
            /// <summary>
            [JsonProperty("codeEntry")]
            [XmlElement(ElementName = "codeEntry")]
            public string CodeEntry { get; set; }

            /// <summary>
            /// shortName
            /// Optional:
            /// <summary>
            [JsonProperty("shortName")]
            [XmlElement(ElementName = "shortName")]
            public string ShortName { get; set; }

        }

        /// <summary>
        /// PractitionerDetailPractitionerCredential
        /// Zero or more repetitions:
        /// <summary>
        public class PractitionerDetailPractitionerCredential
        {
            /// <summary>
            /// codeSetName
            /// Optional:
            /// <summary>
            [JsonProperty("codeSetName")]
            [XmlElement(ElementName = "codeSetName")]
            public string CodeSetName { get; set; }

            /// <summary>
            /// codeEntry
            /// Optional:
            /// <summary>
            [JsonProperty("codeEntry")]
            [XmlElement(ElementName = "codeEntry")]
            public string CodeEntry { get; set; }

            /// <summary>
            /// shortName
            /// Optional:
            /// <summary>
            [JsonProperty("shortName")]
            [XmlElement(ElementName = "shortName")]
            public string ShortName { get; set; }

        }

        /// <summary>
        /// PractitionerDetailCredentialSuffixList
        /// Optional:
        /// <summary>
        public class PractitionerDetailCredentialSuffixList
        {
            /// <summary>
            /// listMode
            /// Optional:
            /// <summary>
            [JsonProperty("listMode")]
            [XmlElement(ElementName = "listMode")]
            public string ListMode { get; set; }

            /// <summary>
            /// CredentialSuffix
            /// Zero or more repetitions:
            /// <summary>
            [JsonProperty("CredentialSuffix")]
            [XmlElement(ElementName = "CredentialSuffix")]
            public List<CredentialSuffixListCredentialSuffix> CredentialSuffix { get; set; }

        }

        /// <summary>
        /// CredentialSuffixListCredentialSuffix
        /// Zero or more repetitions:
        /// <summary>
        public class CredentialSuffixListCredentialSuffix
        {
            /// <summary>
            /// codeSetName
            /// Optional:
            /// <summary>
            [JsonProperty("codeSetName")]
            [XmlElement(ElementName = "codeSetName")]
            public string CodeSetName { get; set; }

            /// <summary>
            /// codeEntry
            /// Optional:
            /// <summary>
            [JsonProperty("codeEntry")]
            [XmlElement(ElementName = "codeEntry")]
            public string CodeEntry { get; set; }

            /// <summary>
            /// shortName
            /// Optional:
            /// <summary>
            [JsonProperty("shortName")]
            [XmlElement(ElementName = "shortName")]
            public string ShortName { get; set; }

        }

        /// <summary>
        /// PractitionerDetailNameSuffixList
        /// Optional:
        /// <summary>
        public class PractitionerDetailNameSuffixList
        {
            /// <summary>
            /// listMode
            /// Optional:
            /// <summary>
            [JsonProperty("listMode")]
            [XmlElement(ElementName = "listMode")]
            public string ListMode { get; set; }

            /// <summary>
            /// suffix
            /// Zero or more repetitions:
            /// <summary>
            [JsonProperty("suffix")]
            [XmlElement(ElementName = "suffix")]
            public List<string> Suffix { get; set; }

        }



    }

}

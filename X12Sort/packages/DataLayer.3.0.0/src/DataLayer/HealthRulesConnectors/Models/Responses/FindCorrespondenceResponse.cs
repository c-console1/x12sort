using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.HealthRulesConnectors.Models.Responses
{
    /// <summary>
    /// Wrapper for Find Correspondence response object
    /// </summary>
    public class FindCorrespondenceResponse
    {

        /// <summary>
        /// Reponse object that holds all the values for the FindCorrespondence Connector Call
        /// This is the root of the response object
        /// </summary>
        public class Response
        {
            /// <summary>
            /// Envelope - This where all the objects branch from
            /// </summary>
            public InnerEnvelope Envelope { get; set; }

            /// <summary>
            /// List of Correspondence ID's
            /// Alias to: Envelope.Body.findCorrespondenceResponse.correspondenceId
            /// </summary>
            public List<string> correspondenceId
            {
                get
                {
                    try
                    {
                        return Envelope.Body.findCorrespondenceResponse.correspondenceId;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }

        }
        /// <summary>
        /// Envelope - This where all the objects branch from
        /// </summary>
        public class InnerEnvelope : Fault
        {
            /// <summary>
            /// Text header - This should always be NULL or Blank
            /// </summary>
            public string Header { get; set; }

            /// <summary>
            /// Main Body, most values should be in here
            /// </summary>
            public Body Body { get; set; }
        }

        /// <summary>
        /// Main Body, most values should be in here
        /// </summary>
        public class Body
        {
            /// <summary>
            /// Values of the Correspondence Response
            /// </summary>
            public FindCorrespondenceChildResponse findCorrespondenceResponse { get; set; }
        }

        /// <summary>
        /// Correspondence Response Results
        /// </summary>
        public class FindCorrespondenceChildResponse
        {
            /// <summary>
            /// List of Correspondence ID's 
            /// </summary>
            public List<string> correspondenceId { get; set; }
        }

    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace DataLayer.HealthRulesConnectors.Models.Responses
{
    /// <summary>
    /// Custom response file for the GetMemberDetailsByHcc response object
    /// Please Note: this format can change at any time
    /// </summary>
    public class GetMemberDetailsByHccResponse
    {
        #region Parse raw XML

        /// <summary>
        /// Modules / XML nodes that have been evaluted
        /// This stops duplate evaluations
        /// </summary>
        private List<string> moduleList = new List<string>();

        /// <summary>
        /// Parse XML text, fillin this object, return this object
        /// </summary>
        /// <param name="xml">XML text to parse</param>
        /// <returns>This object filled in</returns>
        public GetMemberDetailsByHccResponse Parse(string xml)
        {
            Xml = xml;

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            // Parse Children
            ParseChildNodes(doc.ChildNodes);

            // Error was experience
            if (ParseException != null)
            {
                IsValid = false;
            }

            // Parse objects indidividually
            ParsePhoneNumbers();

            return this;

        }

        /// <summary>
        /// Loop through all child nodes from the parent document
        /// </summary>
        /// <param name="nodes">Child nodes to parse</param>
        public void ParseChildNodes(XmlNodeList nodes)
        {
            if (nodes == null)
            {
                IsValid = false;
                return;
            }

            foreach (XmlNode indNode in nodes)
            {
                // Capture all base attributes because this document's structure is not guarenteed
                if (indNode.ParentNode.Name == "member" && indNode.ChildNodes.Count == 1)
                {
                    RawValues[indNode.Name] = indNode.InnerText;
                }

                if (indNode.ParentNode.Name == "member")
                {
                    // Capture XML Nodes that have at least 1 sub-node for XML blocks that are not known.
                    // The structure of this Connector is fluid
                    if (RawXML.ContainsKey(indNode.Name))
                        RawXML[indNode.Name] += "\r\n" + indNode.OuterXml;
                    else
                        RawXML[indNode.Name] = indNode.OuterXml;
                }

                // Server fault
                if (indNode.Name == "faultstring")
                {
                    if (RawXML == null)
                        RawXML = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);

                    if (RawXML.ContainsKey("fault"))
                        RawXML["fault"] += indNode.ParentNode.InnerXml;
                    else
                        RawXML["fault"] = indNode.ParentNode.InnerXml;

                    XmlFault = indNode.ParentNode.InnerXml;

                    ParseException = new Exception(indNode.InnerText);
                }

                // Parse member base tags, child blocks will read into seperate block
                if (moduleList.Any(x => x == "member") && indNode.Name == "member")
                {
                    moduleList.Add("member");

                    // Set the Member XML value
                    XmlMember = indNode.OuterXml;

                    // Create an XML document so that custom parsing can be more easily done
                    MemberXmlDoc = new XmlDocument();
                    MemberXmlDoc.LoadXml(indNode.OuterXml);

                    // Load and parse base values into thier public properties
                    foreach (XmlNode nodeLittle in indNode.ChildNodes)
                    {
                        VIP = Validation.SeValueByNameBool("VIP", nodeLittle.Name, nodeLittle.InnerText, VIP);
                        BirthDate = Validation.SeValueByNameDateTime("birthDate", nodeLittle.Name, nodeLittle.InnerText, BirthDate);
                        EffectiveDate = Validation.SeValueByNameDateTime("effectiveDate", nodeLittle.Name, nodeLittle.InnerText, EffectiveDate);
                        FirstName = Validation.SeValueByNameString("firstName", nodeLittle.Name, nodeLittle.InnerText, FirstName);
                        Gender = Validation.SeValueByNameString("gender", nodeLittle.Name, nodeLittle.InnerText, Gender);
                        GenderCode = Validation.SeValueByNameString("genderCode", nodeLittle.Name, nodeLittle.InnerText, GenderCode);
                        ID = Validation.SeValueByNameString("id", nodeLittle.Name, nodeLittle.InnerText, ID);
                        IsSubscriber = Validation.SeValueByNameBool("isSubscriber", nodeLittle.Name, nodeLittle.InnerText, IsSubscriber);
                        LastName = Validation.SeValueByNameString("lastName", nodeLittle.Name, nodeLittle.InnerText, LastName);
                        MiddleName = Validation.SeValueByNameString("middleName", nodeLittle.Name, nodeLittle.InnerText, MiddleName);
                        NumberOfDependents = Validation.SeValueByNameInt("numberOfDependents", nodeLittle.Name, nodeLittle.InnerText, NumberOfDependents);
                        SomkingStatus = Validation.SeValueByNameString("somkingStatus", nodeLittle.Name, nodeLittle.InnerText, SomkingStatus);
                        // Misspelled in test data, production may have this fixed
                        SmokingStatus = Validation.SeValueByNameString("smokingStatus", nodeLittle.Name, nodeLittle.InnerText, SmokingStatus);
                        SSN = Validation.SeValueByNameString("ssn", nodeLittle.Name, nodeLittle.InnerText, SSN);
                        SubscriberID = Validation.SeValueByNameString("subscriberId", nodeLittle.Name, nodeLittle.InnerText, SubscriberID);
                        SubscriberSystemID = Validation.SeValueByNameString("subscriberSystemId", nodeLittle.Name, nodeLittle.InnerText, SubscriberSystemID);
                        SubscriptionID = Validation.SeValueByNameString("subscriptionId", nodeLittle.Name, nodeLittle.InnerText, SubscriptionID);
                        SubscriptionSystemID = Validation.SeValueByNameString("subscriptionSystemId", nodeLittle.Name, nodeLittle.InnerText, SubscriptionSystemID);
                        SystemID = Validation.SeValueByNameString("systemId", nodeLittle.Name, nodeLittle.InnerText, SystemID);
                        VersionEffectiveDate = Validation.SeValueByNameDateTime("versionEffectiveDate", nodeLittle.Name, nodeLittle.InnerText, VersionEffectiveDate);
                    }

                }


                // Parse extractTime
                if (moduleList.Any(x => x == "extractTime") && indNode.Name == "extractTime")
                {
                    moduleList.Add("extractTime");
                    DateTime tempExtractTime;
                    if (DateTime.TryParse(indNode.InnerText, out tempExtractTime))
                        ExtractTime = tempExtractTime;
                }


                // Correspondence Address
                if (indNode.Name == "correspondenceAddress" && indNode.ParentNode.Name == "member")
                {
                    if (XmlCorrespondenceAddress == null)
                        XmlCorrespondenceAddress = indNode.OuterXml;
                    else
                        XmlCorrespondenceAddress += indNode.OuterXml;

                    AddressValue item = new AddressValue();

                    if (CorrespondenceAddresses == null)
                        CorrespondenceAddresses = new List<AddressValue>();

                    foreach (XmlNode nodeLittle in indNode.ChildNodes)
                    {
                        item.Address = Validation.SeValueByNameString("address", nodeLittle.Name, nodeLittle.InnerText, item.Address);
                        item.Address2 = Validation.SeValueByNameString("address2", nodeLittle.Name, nodeLittle.InnerText, item.Address2);
                        item.Address3 = Validation.SeValueByNameString("address3", nodeLittle.Name, nodeLittle.InnerText, item.Address3);
                        item.City = Validation.SeValueByNameString("city", nodeLittle.Name, nodeLittle.InnerText, item.City);
                        item.CountryCode = Validation.SeValueByNameString("countryCode", nodeLittle.Name, nodeLittle.InnerText, item.CountryCode);
                        item.CountyCode = Validation.SeValueByNameString("countyCode", nodeLittle.Name, nodeLittle.InnerText, item.CountyCode);
                        item.StateCode = Validation.SeValueByNameString("stateCode", nodeLittle.Name, nodeLittle.InnerText, item.StateCode);
                        item.ZipCode = Validation.SeValueByNameString("zipCode", nodeLittle.Name, nodeLittle.InnerText, item.ZipCode);
                        item.ZipExtension = Validation.SeValueByNameString("zipExtension", nodeLittle.Name, nodeLittle.InnerText, item.ZipExtension);
                        item.RawValues[nodeLittle.Name] = nodeLittle.InnerText;
                    }

                    CorrespondenceAddresses.Add(item);
                }


                // Residential Address
                if (indNode.Name == "residentialAddress")
                {
                    if (XmlResidentialAddress == null)
                        XmlResidentialAddress = indNode.OuterXml;
                    else
                        XmlResidentialAddress += indNode.OuterXml;

                    if (ResidentialAddresses == null)
                        ResidentialAddresses = new List<AddressValue>();

                    AddressValue item = new AddressValue();

                    foreach (XmlNode nodeLittle in indNode.ChildNodes)
                    {
                        item.Address = Validation.SeValueByNameString("address", nodeLittle.Name, nodeLittle.InnerText, item.Address);
                        item.Address2 = Validation.SeValueByNameString("address2", nodeLittle.Name, nodeLittle.InnerText, item.Address2);
                        item.Address3 = Validation.SeValueByNameString("address3", nodeLittle.Name, nodeLittle.InnerText, item.Address3);
                        item.City = Validation.SeValueByNameString("city", nodeLittle.Name, nodeLittle.InnerText, item.City);
                        item.CountryCode = Validation.SeValueByNameString("countryCode", nodeLittle.Name, nodeLittle.InnerText, item.CountryCode);
                        item.CountyCode = Validation.SeValueByNameString("countyCode", nodeLittle.Name, nodeLittle.InnerText, item.CountyCode);
                        item.StateCode = Validation.SeValueByNameString("stateCode", nodeLittle.Name, nodeLittle.InnerText, item.StateCode);
                        item.ZipCode = Validation.SeValueByNameString("zipCode", nodeLittle.Name, nodeLittle.InnerText, item.ZipCode);
                        item.ZipExtension = Validation.SeValueByNameString("zipExtension", nodeLittle.Name, nodeLittle.InnerText, item.ZipExtension);
                        item.RawValues[nodeLittle.Name] = nodeLittle.InnerText;
                    }

                    ResidentialAddresses.Add(item);
                }


                // SubscriberInformation
                if (indNode.Name == "subscriberInformation")
                {
                    if (XmlSubscriberInformation == null)
                        XmlSubscriberInformation = indNode.OuterXml;
                    else
                        XmlSubscriberInformation += indNode.OuterXml;

                    if (SubscribersInformation == null)
                        SubscribersInformation = new List<SubscriberInformationItem>();

                    SubscriberInformationItem item = new SubscriberInformationItem();

                    foreach (XmlNode nodeLittle in indNode.ChildNodes)
                    {
                        item.BirthDate = Validation.SeValueByNameDateTime("birthDate", nodeLittle.Name, nodeLittle.InnerText, item.BirthDate);
                        item.FirstName = Validation.SeValueByNameString("firstName", nodeLittle.Name, nodeLittle.InnerText, item.FirstName);
                        item.Gender = Validation.SeValueByNameString("gender", nodeLittle.Name, nodeLittle.InnerText, item.Gender);
                        item.GenderCode = Validation.SeValueByNameString("genderCode", nodeLittle.Name, nodeLittle.InnerText, item.GenderCode);
                        item.ID = Validation.SeValueByNameString("id", nodeLittle.Name, nodeLittle.InnerText, item.ID);
                        item.LastName = Validation.SeValueByNameString("lastName", nodeLittle.Name, nodeLittle.InnerText, item.LastName);
                        item.MiddleName = Validation.SeValueByNameString("middleName", nodeLittle.Name, nodeLittle.InnerText, item.MiddleName);
                        item.SSN = Validation.SeValueByNameString("ssn", nodeLittle.Name, nodeLittle.InnerText, item.SSN);
                        item.SystemID = Validation.SeValueByNameString("systemId", nodeLittle.Name, nodeLittle.InnerText, item.SystemID);
                        item.RawValues[nodeLittle.Name] = nodeLittle.InnerText;
                    }

                    SubscribersInformation.Add(item);
                }


                // Hierarchial Account Information - 1:Inifinity Levels at the 'member' node root level
                if (indNode.Name == "hierarchialAccountInformation")
                {
                    if (HierarchialAccountInformationList == null)
                        HierarchialAccountInformationList = new List<HierarchialAccountInformation>();


                    HierarchialAccountInformation item = new HierarchialAccountInformation();
                    item.ParentAccountId = Validation.SeValueByNameString("parentAccountId", indNode.Name, indNode.InnerText, item.ParentAccountId);
                    item.ParentAccountType = Validation.SeValueByNameString("parentAccountType", indNode.Name, indNode.InnerText, item.ParentAccountType);
                    item.ParentAccountTypeCode = Validation.SeValueByNameString("parentAccountTypeCode", indNode.Name, indNode.InnerText, item.ParentAccountTypeCode);
                    item.TopAccountId = Validation.SeValueByNameString("topAccountId", indNode.Name, indNode.InnerText, item.TopAccountId);
                    item.TopAccountName = Validation.SeValueByNameString("topAccountName", indNode.Name, indNode.InnerText, item.TopAccountName);
                    item.TopAccountType = Validation.SeValueByNameString("topAccountType", indNode.Name, indNode.InnerText, item.TopAccountType);
                    item.TopAccountTypeCode = Validation.SeValueByNameString("topAccountTypeCode", indNode.Name, indNode.InnerText, item.TopAccountTypeCode);
                    item.RawValues[indNode.Name] = indNode.InnerText;
                    HierarchialAccountInformationList.Add(item);
                }

                // Current plans
                if (indNode.Name == "currentPlans")
                {
                    if (CurrentPlans == null)
                        CurrentPlans = new List<PlanInformation>();

                    foreach (XmlNode planInformation in indNode.ChildNodes)
                    {

                        if (planInformation.Name == "planInformation")
                        {
                            PlanInformation plan = new PlanInformation();

                            if (plan.RawXML == null)
                                plan.RawXML = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);

                            foreach (XmlNode planInfoBase in planInformation.ChildNodes)
                            {
                                if (plan.RawXML.ContainsKey(planInfoBase.Name))
                                    plan.RawXML[planInfoBase.Name] += "\r\n" + planInfoBase.OuterXml;
                                else
                                    plan.RawXML[planInfoBase.Name] = planInfoBase.OuterXml;


                                plan.CoverageTier = Validation.SeValueByNameString("coverageTier", planInfoBase.Name, planInfoBase.InnerText, plan.CoverageTier);
                                plan.StartDate = Validation.SeValueByNameDateTime("startDate", planInfoBase.Name, planInfoBase.InnerText, plan.StartDate);
                                plan.EndDate = Validation.SeValueByNameDateTime("endDate", planInfoBase.Name, planInfoBase.InnerText, plan.EndDate);
                                plan.PlanId = Validation.SeValueByNameString("planId", planInfoBase.Name, planInfoBase.InnerText, plan.PlanId);
                                plan.PlanName = Validation.SeValueByNameString("planName", planInfoBase.Name, planInfoBase.InnerText, plan.PlanName);

                                if (planInfoBase.Name == "product")
                                {
                                    plan.Product = new Product();
                                    foreach (XmlNode product in planInfoBase.ChildNodes)
                                    {
                                        plan.Product.IsMedicare = Validation.SeValueByNameBool("isMedicare", product.Name, product.InnerText, plan.Product.IsMedicare);
                                        plan.Product.PlanType = Validation.SeValueByNameString("planType", product.Name, product.InnerText, plan.Product.PlanType);
                                        plan.Product.ProductName = Validation.SeValueByNameString("productName", product.Name, product.InnerText, plan.Product.ProductName);
                                        plan.Product.RawValues[product.Name] = product.InnerText;
                                    }
                                }

                                if (planInfoBase.Name == "benefitNetworkList")
                                {
                                    plan.BenefitNetworkList = new List<BenefitNetworkInformation>();
                                    foreach (XmlNode product in planInfoBase.ChildNodes)
                                    {
                                        BenefitNetworkInformation bni = new BenefitNetworkInformation();
                                        foreach (XmlNode nodeBNI in product.ChildNodes)
                                        {
                                            bni.BenefitNetworkName = Validation.SeValueByNameString("benefitNetworkName", nodeBNI.Name, nodeBNI.InnerText, bni.BenefitNetworkName);
                                            bni.RawValues[nodeBNI.Name] = nodeBNI.InnerText;
                                        }
                                        plan.BenefitNetworkList.Add(bni);
                                    }
                                }

                            }
                            CurrentPlans.Add(plan);
                        }

                    }


                }

                // SubscriberInformation
                if (indNode.Name == "account")
                {
                    if (Accounts == null)
                        Accounts = new List<Account>();

                    Account account = new Account();
                    foreach (XmlNode nodeAcc in indNode.ChildNodes)
                    {
                        account.AccountType = Validation.SeValueByNameString("accountType", nodeAcc.Name, nodeAcc.InnerText, account.AccountType);
                        account.AccountTypeCode = Validation.SeValueByNameString("accountTypeCode", nodeAcc.Name, nodeAcc.InnerText, account.AccountTypeCode);
                        account.ID = Validation.SeValueByNameString("id", nodeAcc.Name, nodeAcc.InnerText, account.ID);
                        account.Name = Validation.SeValueByNameString("name", nodeAcc.Name, nodeAcc.InnerText, account.Name);
                        account.StartDate = Validation.SeValueByNameDateTime("startDate", nodeAcc.Name, nodeAcc.InnerText, account.StartDate);
                        account.EndDate = Validation.SeValueByNameDateTime("endDate", nodeAcc.Name, nodeAcc.InnerText, account.EndDate);
                        account.SystemId = Validation.SeValueByNameString("systemId", nodeAcc.Name, nodeAcc.InnerText, account.SystemId);

                        // Addresses
                        if (nodeAcc.Name == "correspondenceAddress")
                        {
                            if (account.CorrespondenceAddresses == null)
                                account.CorrespondenceAddresses = new List<AddressValue>();

                            AddressValue addr = new AddressValue();
                            foreach (XmlNode nodeAddr in nodeAcc.ChildNodes)
                            {
                                addr.Address = Validation.SeValueByNameString("address", nodeAddr.Name, nodeAddr.InnerText, addr.Address);
                                addr.Address2 = Validation.SeValueByNameString("address2", nodeAddr.Name, nodeAddr.InnerText, addr.Address2);
                                addr.Address3 = Validation.SeValueByNameString("address3", nodeAddr.Name, nodeAddr.InnerText, addr.Address3);
                                addr.City = Validation.SeValueByNameString("city", nodeAddr.Name, nodeAddr.InnerText, addr.City);
                                addr.StateCode = Validation.SeValueByNameString("stateCode", nodeAddr.Name, nodeAddr.InnerText, addr.StateCode);
                                addr.ZipCode = Validation.SeValueByNameString("zipCode", nodeAddr.Name, nodeAddr.InnerText, addr.ZipCode);
                                addr.ZipExtension = Validation.SeValueByNameString("zipExtension", nodeAddr.Name, nodeAddr.InnerText, addr.ZipExtension);
                                addr.CountyCode = Validation.SeValueByNameString("countyCode", nodeAddr.Name, nodeAddr.InnerText, addr.CountyCode);
                                addr.CountryCode = Validation.SeValueByNameString("countryCode", nodeAddr.Name, nodeAddr.InnerText, addr.CountryCode);
                                addr.RawValues[nodeAddr.Name] = nodeAddr.InnerText;
                            }
                            account.CorrespondenceAddresses.Add(addr);
                        }

                        if (nodeAcc.Name != "correspondenceAddress")
                            account.RawValues[nodeAcc.Name] = nodeAcc.InnerText;
                    }

                    Accounts.Add(account);

                }


                // SubscriberInformation
                if (indNode.Name == "providerChoice")
                {
                    if (XmlProviderChoices == null)
                        XmlProviderChoices = indNode.OuterXml;
                    else
                        XmlProviderChoices += indNode.OuterXml;

                    XmlCurrentPlans = string.IsNullOrWhiteSpace(XmlCurrentPlans) ? indNode.OuterXml : "\r\n" + indNode.OuterXml;

                    if (CurrentProviderChoices == null)
                        CurrentProviderChoices = new List<ProviderChoice>();

                    ProviderChoice item = new ProviderChoice();

                    foreach (XmlNode nodeLittle in indNode.ChildNodes)
                    {
                        item.ChoiceType = Validation.SeValueByNameString("choiceType", nodeLittle.Name, nodeLittle.InnerText, item.ChoiceType);
                        item.StartDate = Validation.SeValueByNameDateTime("startDate", nodeLittle.Name, nodeLittle.InnerText, item.StartDate);
                        item.EndDate = Validation.SeValueByNameDateTime("endDate", nodeLittle.Name, nodeLittle.InnerText, item.EndDate);
                        item.FirstName = Validation.SeValueByNameString("firstName", nodeLittle.Name, nodeLittle.InnerText, item.FirstName);
                        item.LastName = Validation.SeValueByNameString("lastName", nodeLittle.Name, nodeLittle.InnerText, item.LastName);
                        item.MiddleName = Validation.SeValueByNameString("middleName", nodeLittle.Name, nodeLittle.InnerText, item.MiddleName);
                        item.ProviderChoiceReason = Validation.SeValueByNameString("providerChoiceReason", nodeLittle.Name, nodeLittle.InnerText, item.ProviderChoiceReason);
                        item.ProviderChoiceReasonCode = Validation.SeValueByNameString("providerChoiceReasonCode", nodeLittle.Name, nodeLittle.InnerText, item.ProviderChoiceReasonCode);
                        item.ProviderID = Validation.SeValueByNameString("providerId", nodeLittle.Name, nodeLittle.InnerText, item.ProviderID);
                        item.ProviderNPI = Validation.SeValueByNameString("providerNPI", nodeLittle.Name, nodeLittle.InnerText, item.ProviderNPI);
                        item.ProviderRoleName = Validation.SeValueByNameString("providerRoleName", nodeLittle.Name, nodeLittle.InnerText, item.ProviderRoleName);
                        item.RawValues[nodeLittle.Name] = nodeLittle.InnerText;
                    }

                    CurrentProviderChoices.Add(item);
                }


                // identificationNumber
                if (indNode.Name == "identificationNumber")
                {
                    if (XmlOtherIDs == null)
                        XmlOtherIDs = indNode.OuterXml;
                    else
                        XmlOtherIDs += indNode.OuterXml;

                    if (OtherIDs == null)
                        OtherIDs = new List<IdentificationNumber>();

                    IdentificationNumber item = new IdentificationNumber();

                    foreach (XmlNode nodeLittle in indNode.ChildNodes)
                    {
                        item.IDTypeCode = Validation.SeValueByNameString("idTypeCode", nodeLittle.Name, nodeLittle.InnerText, item.IDTypeCode);
                        item.IDNumber = Validation.SeValueByNameString("idNumber", nodeLittle.Name, nodeLittle.InnerText, item.IDNumber);
                        item.RawValues[nodeLittle.Name] = nodeLittle.InnerText;
                    }
                    OtherIDs.Add(item);
                }


                // UDT - use special sub method because of how deep this can go
                if (indNode.Name == "UDT")
                {
                    if (XmlUDTList == null)
                        XmlUDTList = indNode.OuterXml;
                    else
                        XmlUDTList += indNode.OuterXml;

                    ParseUDT(indNode);

                }


                ParseChildNodes(indNode.ChildNodes);

            }

            IsValid = true;

        }

        /// <summary>
        /// Parse User Defined Terms
        /// </summary>
        /// <param name="nodeSet">XML node set</param>
        private void ParseUDT(XmlNode nodeSet)
        {

            if (UDTList == null)
                UDTList = new List<UDTItem>();

            UDTItem item = new UDTItem();

            foreach (XmlNode nodeBase in nodeSet)
            {
                item.UDTName = Validation.SeValueByNameString("udtName", nodeBase.Name, nodeBase.InnerText, item.UDTName);
                if (nodeBase.Name == "attributes")
                {
                    if (item.Attributes == null)
                        item.Attributes = new List<UDTItem.AttributeItem>();

                    UDTItem.AttributeItem itemAttr = new UDTItem.AttributeItem();
                    foreach (XmlNode nodeAttrs in nodeBase.ChildNodes)
                    {
                        if (nodeAttrs.Name == "attribute")
                        {

                            foreach (XmlNode nodeIndAttrs in nodeAttrs.ChildNodes)
                            {
                                itemAttr.AttributeName = Validation.SeValueByNameString("attributeName", nodeIndAttrs.Name, nodeIndAttrs.InnerText, itemAttr.AttributeName);
                                itemAttr.LongName = Validation.SeValueByNameString("longName", nodeIndAttrs.Name, nodeIndAttrs.InnerText, itemAttr.LongName);
                                itemAttr.Value = Validation.SeValueByNameString("value", nodeIndAttrs.Name, nodeIndAttrs.InnerText, itemAttr.Value);
                                itemAttr.ValueShortName = Validation.SeValueByNameString("valueShortName", nodeIndAttrs.Name, nodeIndAttrs.InnerText, itemAttr.ValueShortName);
                                if (itemAttr.RawXML.ContainsKey(nodeIndAttrs.Name))
                                    itemAttr.RawXML[nodeIndAttrs.Name] += "\r\n" + nodeIndAttrs.OuterXml;
                                else
                                    itemAttr.RawXML[nodeIndAttrs.Name] = nodeIndAttrs.OuterXml;
                            }

                        }
                    }
                    item.Attributes.Add(itemAttr);
                }
            }

            UDTList.Add(item);

        }

        #endregion

        #region Raw XML / Parse XML

        /// <summary>
        /// Was this a valid call?
        /// </summary>
        public bool IsValid { get; set; } = false;

        /// <summary>
        /// Raw XML content
        /// </summary>
        public string Xml { get; private set; }

        /// <summary>
        /// Raw XML Member Content
        /// </summary>
        public string XmlMember { get; private set; }

        /// <summary>
        /// XML Document object built from root 'member' tag
        /// </summary>
        public XmlDocument MemberXmlDoc { get; private set; }

        /// <summary>
        /// Raw XML Correspondence Address Content
        /// </summary>
        public string XmlCorrespondenceAddress { get; private set; }

        /// <summary>
        /// List of Current Plans
        /// </summary>
        public string XmlCurrentPlans { get; private set; }

        /// <summary>
        /// Raw XML of a server fault
        /// </summary>
        public string XmlFault { get; private set; }

        /// <summary>
        /// Raw XML Residential Address Content
        /// </summary>
        public string XmlResidentialAddress { get; private set; }

        /// <summary>
        /// Raw XML Subscriber Address Content
        /// </summary>
        public string XmlSubscriberInformation { get; private set; }

        /// <summary>
        /// Raw XML Provider Choices Content
        /// </summary>
        public string XmlProviderChoices { get; private set; }

        /// <summary>
        /// Raw XML Other ID's Content
        /// </summary>
        public string XmlOtherIDs { get; private set; }

        /// <summary>
        /// Raw XML Other ID's Content
        /// </summary>
        public string XmlUDTList { get; private set; }

        #endregion

        #region Easy to read / use values

        /// <summary>
        /// Date / Time this Extract was ran
        /// </summary>
        public DateTime ExtractTime { get; private set; }

        #region Member Base Values

        /// <summary>
        /// Raw Value = Capture raw values because this XML structure is not gaurenteed
        /// </summary>
        public Dictionary<string, string> RawValues = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);

        /// <summary>
        /// Raw XML = Capture raw XML blocks because this XML structure is not gaurenteed
        /// </summary>
        public Dictionary<string, string> RawXML = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);

        /// <summary>
        /// Member VIP Status
        /// </summary>
        public bool VIP { get; private set; } = false;

        /// <summary>
        /// Member Date of Birth
        /// </summary>
        public DateTime ? BirthDate { get; private set; }

        /// <summary>
        /// Member's Current Effective date
        /// </summary>
        public DateTime ? EffectiveDate { get; private set; }

        /// <summary>
        /// Member's First Name
        /// </summary>
        public string FirstName { get; private set; }

        /// <summary>
        /// Member's Middle Name
        /// </summary>
        public string MiddleName { get; private set; }

        /// <summary>
        /// Member's Last Name
        /// </summary>
        public string LastName { get; private set; }

        /// <summary>
        /// Member's Gender - Long Name
        /// </summary>
        public string Gender { get; private set; }

        /// <summary>
        /// Member's Gender - 1 Letter Code
        /// </summary>
        public string GenderCode { get; private set; }

        /// <summary>
        /// Member's Unique ID as Assigned, not the HCC ID
        /// </summary>
        public string ID { get; private set; }

        /// <summary>
        /// Is this Member the Subscriber? True = Yes
        /// </summary>
        public bool IsSubscriber { get; private set; } = false;

        /// <summary>
        /// # of Dependents the Member has
        /// </summary>
        public int ? NumberOfDependents { get; private set; } = 0;

        /// <summary>
        /// Relation to Subscriber - Ex. 'Self' is the subscriber
        /// </summary>
        public string RelationshipToSubscriber { get; private set; }

        /// <summary>
        /// Smoking Status ID Code - This maybe depreciated due to misspelling
        /// </summary>
        public string SomkingStatus { get; private set; }

        /// <summary>
        /// Smoking Status ID Code
        /// </summary>
        public string SmokingStatus { get; private set; }

        /// <summary>
        /// Social Security #
        /// </summary>
        public string SSN { get; private set; }

        /// <summary>
        /// Parent Subscriber ID as Assigned, not the HCC ID
        /// </summary>
        public string SubscriberID { get; private set; }

        /// <summary>
        /// Subscriber System ID - usually the HCC ID
        /// </summary>
        public string SubscriberSystemID { get; private set; }

        /// <summary>
        /// Subscription ID - Ussually 2 characters less then the SubscriberID and ID
        /// </summary>
        public string SubscriptionID { get; private set; }

        /// <summary>
        /// Subscription System ID - Usually the Subscriber's HCC ID
        /// </summary>
        public string SubscriptionSystemID { get; private set; }

        /// <summary>
        /// System ID - Member's HCC ID
        /// </summary>
        public string SystemID { get; private set; }

        /// <summary>
        /// Version Effective Date - Last date this member was changed -
        /// 1/1/1800 means this member has never been changed since it was set up
        /// </summary>
        public DateTime ? VersionEffectiveDate { get; private set; }

        /// <summary>
        /// Exception experienced during parsing
        /// </summary>
        public Exception ParseException { get; set; }

        #endregion

        #region Current Plans

        /// <summary>
        /// Current plans
        /// </summary>
        public List<PlanInformation> CurrentPlans { get; private set; }

        /// <summary>
        /// Individual plan information
        /// </summary>
        public class PlanInformation
        {
            /// <summary>
            /// Coverage Tier - Plan text value like 'Subscriber' for top tier
            /// </summary>
            public string CoverageTier { get; set; }

            /// <summary>
            /// Plan Start Date
            /// </summary>
            public DateTime ? StartDate { get; set; }

            /// <summary>
            /// Plan End Date
            /// </summary>
            public DateTime ? EndDate { get; set; }

            /// <summary>
            /// Plan ID - This should be used as a tag
            /// </summary>
            public string PlanId { get; set; }

            /// <summary>
            /// Descriptive Name of the Plan
            /// </summary>
            public string PlanName { get; set; }

            /// <summary>
            /// Product Information
            /// </summary>
            public Product Product { get; set; }

            /// <summary>
            /// List of Benefit Networks this plan is a part of
            /// </summary>
            public List<BenefitNetworkInformation> BenefitNetworkList { get; set; }

            /// <summary>
            /// Raw XML = Capture raw values because this XML structure is not gaurenteed
            /// </summary>
            public Dictionary<string, string> RawXML = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);

        }

        /// <summary>
        /// Plan Product Information 
        /// </summary>
        public class Product
        {
            /// <summary>
            /// Is this a Medicare
            /// </summary>
            public bool IsMedicare { get; set; } = false;

            /// <summary>
            /// Plan Type
            /// </summary>
            public string PlanType { get; set; }

            /// <summary>
            /// Product Name
            /// </summary>
            public string ProductName { get; set; }

            /// <summary>
            /// Raw values - used because this call is not consistant
            /// </summary>
            public Dictionary<string, string> RawValues = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);

        }

        /// <summary>
        /// Benefit Network List
        /// </summary>
        public class BenefitNetworkList
        {
            /// <summary>
            /// Benefit Network Information
            /// </summary>
            public List<BenefitNetworkInformation> BenefitNetworkInformation { get; set; }
        }

        /// <summary>
        /// Benefit Network Information
        /// </summary>
        public class BenefitNetworkInformation
        {
            /// <summary>
            /// Benefit Network Name
            /// </summary>
            public string BenefitNetworkName { get; set; }

            /// <summary>
            /// Raw values - used because this call is not consistant
            /// </summary>
            public Dictionary<string, string> RawValues = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);

        }

        #endregion

        #region Account

        /// <summary>
        /// List of Accounts
        /// </summary>
        public List<Account> Accounts { get; private set; }

        /// <summary>
        /// Account
        /// </summary>
        public class Account
        {
            /// <summary>
            /// Account Type
            /// </summary>
            public string AccountType { get; set; }

            /// <summary>
            /// Account Type Code
            /// </summary>
            public string AccountTypeCode { get; set; }

            /// <summary>
            /// Account ID
            /// </summary>
            public string ID { get; set; }

            /// <summary>
            /// Account Name
            /// </summary>
            public string Name { get; set; }

            /// <summary>
            /// Account Start Date
            /// </summary>
            public DateTime ? StartDate { get; set; }

            /// <summary>
            /// Account End Date
            /// </summary>
            public DateTime ? EndDate { get; set; }

            /// <summary>
            /// Account System ID
            /// </summary>
            public string SystemId { get; set; }

            /// <summary>
            /// Correspondence Addresses - There should only be 1
            /// </summary>
            public List<AddressValue> CorrespondenceAddresses { get; set; }

            /// <summary>
            /// Raw values - used because this call is not consistant
            /// </summary>
            public Dictionary<string, string> RawValues = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);

        }


        #endregion


        #region Hierarchial Account Information

        /// <summary>
        /// List of Hierarchial Account Information
        /// </summary>
        public List<HierarchialAccountInformation> HierarchialAccountInformationList { get; private set; }

        /// <summary>
        /// Hierarchial Account Information - There can be sub accounts inside sub accounts inside sub accounts
        /// </summary>
        public class HierarchialAccountInformation
        {
            /// <summary>
            /// Parent Account Information
            /// </summary>
            public string ParentAccountId { get; set; }

            /// <summary>
            /// Parent Account Type
            /// </summary>
            public string ParentAccountType { get; set; }

            /// <summary>
            /// Parent Account Type Code
            /// </summary>
            public string ParentAccountTypeCode { get; set; }

            /// <summary>
            /// Top Account ID
            /// </summary>
            public string TopAccountId { get; set; }

            /// <summary>
            /// Top Account Name
            /// </summary>
            public string TopAccountName { get; set; }

            /// <summary>
            /// Top Account Type
            /// </summary>
            public string TopAccountType { get; set; }

            /// <summary>
            /// Top Account Type Code
            /// </summary>
            public string TopAccountTypeCode { get; set; }

            /// <summary>
            /// Raw values - used because this call is not consistant
            /// </summary>
            public Dictionary<string, string> RawValues = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);

        }

        #endregion

        #region Address Lists

        /// <summary>
        /// List of Correspondence Addresses
        /// </summary>
        public List<AddressValue> CorrespondenceAddresses { get; private set; }

        /// List of Residential Addresses
        public List<AddressValue> ResidentialAddresses { get; private set; }

        /// <summary>
        /// Address Item
        /// </summary>
        public class AddressValue
        {
            /// <summary>
            /// Street Address
            /// </summary>
            public string Address { get; set; }

            /// <summary>
            /// Street Address 2
            /// </summary>
            public string Address2 { get; set; }

            /// <summary>
            /// Street Address 3
            /// </summary>
            public string Address3 { get; set; }

            /// <summary>
            /// City
            /// </summary>
            public string City { get; set; }

            /// <summary>
            /// 2 Letter Country Code
            /// </summary>
            public string CountryCode { get; set; }

            /// <summary>
            /// County Code
            /// </summary>
            public string CountyCode { get; set; }

            /// <summary>
            /// 2 Letter State Code
            /// </summary>
            public string StateCode { get; set; }

            /// <summary>
            /// 5-6 Character Zip Code
            /// </summary>
            public string ZipCode { get; set; }

            /// <summary>
            /// 4 Charactor Zip Code Extension
            /// </summary>
            public string ZipExtension { get; set; }

            /// <summary>
            /// Raw values - used because this call is not consistant
            /// </summary>
            public Dictionary<string, string> RawValues = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);
        }

        #endregion

        #region Subscribers Information

        /// <summary>
        /// List of Subscribers
        /// </summary>
        public List<SubscriberInformationItem> SubscribersInformation { get; private set; }

        /// <summary>
        /// Subscriber Information
        /// </summary>
        public class SubscriberInformationItem
        {
            /// <summary>
            /// Birth Date
            /// </summary>
            public DateTime ? BirthDate { get; set; }

            /// <summary>
            /// Subscriber First Name
            /// </summary>
            public string FirstName { get; set; }

            /// <summary>
            /// Subscriber Gender
            /// </summary>
            public string Gender { get; set; }

            /// <summary>
            /// Subscriber's 1 Letter Gender Code
            /// </summary>
            public string GenderCode { get; set; }

            /// <summary>
            /// Subscriber's System Generated ID #
            /// </summary>
            public string ID { get; set; }

            /// <summary>
            /// Subscriber's Last Name
            /// </summary>
            public string LastName { get; set; }

            /// <summary>
            /// Suberscriber's Middle Name / Intial
            /// </summary>
            public string MiddleName { get; set; }

            /// <summary>
            /// Subscriber's Social Security #
            /// </summary>
            public string SSN { get; set; }

            /// <summary>
            /// HCC ID
            /// </summary>
            public string SystemID { get; set; }

            /// <summary>
            /// Raw Value = Capture raw values because this XML structure is not gaurenteed
            /// </summary>
            public Dictionary<string, string> RawValues = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);

        }

        #endregion

        #region Provider Choice

        /// <summary>
        /// List of member's provider choices
        /// </summary>
        public List<ProviderChoice> CurrentProviderChoices = new List<ProviderChoice>();

        /// <summary>
        /// Individual member's provider choices
        /// </summary>
        public class ProviderChoice
        {
            /// <summary>
            /// Coice Type
            /// </summary>
            public string ChoiceType { get; set; }

            /// <summary>
            /// Provider Choice Start Date
            /// </summary>
            public DateTime ? StartDate { get; set; }

            /// <summary>
            /// Provider Choice End Date
            /// </summary>
            public DateTime ? EndDate { get; set; }

            /// <summary>
            /// Provider First Name
            /// </summary>
            public string FirstName { get; set; }

            /// <summary>
            /// Provider Last Name
            /// </summary>
            public string LastName { get; set; }

            /// <summary>
            /// Provider Middle Name or Initial
            /// </summary>
            public string MiddleName { get; set; }

            /// <summary>
            /// Provider Choice Reason
            /// </summary>
            public string ProviderChoiceReason { get; set; }

            /// <summary>
            /// Provider Choice Reason Code
            /// </summary>
            public string ProviderChoiceReasonCode { get; set; }

            /// <summary>
            /// Provider's System Generated ID #
            /// </summary>
            public string ProviderID { get; set; }

            /// <summary>
            /// Provider NPI
            /// </summary>
            public string ProviderNPI { get; set; }

            /// <summary>
            /// Long Role Name, see ChoiceType for Short Role Name
            /// </summary>
            public string ProviderRoleName { get; set; }

            /// <summary>
            /// Raw Value = Capture raw values because this XML structure is not gaurenteed
            /// </summary>
            public Dictionary<string, string> RawValues = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);

        }

        #endregion

        #region Other Ids

        /// <summary>
        /// Alternate / Other ID's
        /// </summary>
        public List<IdentificationNumber> OtherIDs = new List<IdentificationNumber>();

        /// <summary>
        /// Identification Number
        /// </summary>
        public class IdentificationNumber
        {
            /// <summary>
            /// ID Type Code
            /// </summary>
            public string IDTypeCode { get; set; }

            /// <summary>
            /// ID Number
            /// </summary>
            public string IDNumber { get; set; }

            /// <summary>
            /// Raw Value = Capture raw values because this XML structure is not gaurenteed
            /// </summary>
            public Dictionary<string, string> RawValues = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);

        }

        #endregion

        #region Phone numbers

        public List<PhoneNumber> PhoneNumbers;

        /// <summary>
        /// Parse phone number
        /// </summary>
        private void ParsePhoneNumbers()
        {
            // If key does not exist, stop processing
            if (!RawXML.ContainsKey("phoneNumbers"))
                return;

            // Setup list object
            PhoneNumbers = new List<PhoneNumber>();

            // Load document
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(RawXML["phoneNumbers"]);

            foreach(XmlNode node in doc.ChildNodes)
            {
                PhoneNumber indPhoneNumber = new PhoneNumber();
                indPhoneNumber.RawXML = node.InnerXml;
                foreach(XmlNode babyNode in node.ChildNodes)
                {
                    indPhoneNumber.ItemXML[babyNode.Name] = babyNode.InnerXml;
                    indPhoneNumber.OrganizationPhoneTypeCode = babyNode.Name == "organizationPhoneTypeCode" ? babyNode.OuterXml : indPhoneNumber.OrganizationPhoneTypeCode;
                    indPhoneNumber.PhoneAreaCode = babyNode.Name == "phoneAreaCode" ? babyNode.InnerText : indPhoneNumber.PhoneAreaCode;
                    indPhoneNumber.PhoneCountryCode = babyNode.Name == "phoneCountryCode" ? babyNode.InnerText : indPhoneNumber.PhoneCountryCode;
                    indPhoneNumber.PhoneExtensionNumber = babyNode.Name == "phoneExtensionNumber" ? babyNode.InnerText : indPhoneNumber.PhoneExtensionNumber;
                    indPhoneNumber.PhoneNbr = babyNode.Name == "phoneNumber" ? babyNode.InnerText : indPhoneNumber.PhoneNbr;
                }

                PhoneNumbers.Add(indPhoneNumber);

            }
        }

        public class PhoneNumber
        {

            /// <summary>
            /// Phone country code - default is '01'
            /// </summary>
            public string PhoneCountryCode { get; set; }

            /// <summary>
            /// 3 number area code
            /// </summary>
            public string PhoneAreaCode { get; set; }

            /// <summary>
            /// Phone #
            /// </summary>
            public string PhoneNbr { get; set; }

            /// <summary>
            /// Phone # extension
            /// </summary>
            public string PhoneExtensionNumber { get; set; }

            /// <summary>
            /// Individual phone type code set
            /// </summary>
            public string IndividualPhoneTypeCode { get; set; }

            /// <summary>
            /// Individual organization type code set
            /// </summary>
            public string OrganizationPhoneTypeCode { get; set; }

            /// <summary>
            /// Raw XML = Capture raw values because this XML structure is not gaurenteed
            /// </summary>
            public string RawXML { get; set; }

            /// <summary>
            /// Raw item values
            /// </summary>
            public Dictionary<string, string> ItemXML =
                new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);

        }


        #endregion Phone numbers

        #region udtList / UDT

        /// <summary>
        /// List of User Defined Terms
        /// </summary>
        List<UDTItem> UDTList = new List<UDTItem>();

        /// <summary>
        /// Indidividual User Defined Term
        /// </summary>
        public class UDTItem
        {
            /// <summary>
            /// User Defined Term Name
            /// </summary>
            public string UDTName { get; set; }

            /// <summary>
            /// User Defined Term Attributes - One term can have multiple attributes
            /// </summary>
            public List<AttributeItem> Attributes { get; set; }

            /// <summary>
            /// Indidvual User Defined Term Attribute
            /// </summary>
            public class AttributeItem
            {
                /// <summary>
                /// Attribute Name
                /// </summary>
                public string AttributeName { get; set; }

                /// <summary>
                /// Attribute Long Name
                /// </summary>
                public string LongName { get; set; }

                /// <summary>
                /// Attribute Value
                /// </summary>
                public string Value { get; set; }

                /// <summary>
                /// Attribute Value Short Name
                /// </summary>
                public string ValueShortName { get; set; }

                /// <summary>
                /// Raw Value = Capture raw values because this XML structure is not gaurenteed
                /// </summary>
                public Dictionary<string, string> RawXML = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);

            }

        }

        #endregion

        #endregion
    }
}

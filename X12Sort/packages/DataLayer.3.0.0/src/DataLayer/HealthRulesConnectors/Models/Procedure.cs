using DataLayer.Base.Models;
using System;

namespace DataLayer.HealthRulesConnectors.Models
{
    public class Procedure : IAuthorizationProcedure
    {
        public string DatabaseKey { get; set; }
        public string ProcedureType { get; set; }
        public string ProcedureName { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}

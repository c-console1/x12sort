namespace DataLayer.HealthRulesConnectors.Models
{
    /// <summary>
    /// Class to hold generic call data
    /// </summary>
    public class GenerateCallData
    {
        /// <summary>
        /// Object that will be used to make the connector call
        /// </summary>
        public ExternalCalls callObject { get; set; }

        /// <summary>
        /// Username
        /// </summary>
        public string username { get; set; }

        /// <summary>
        /// Password
        /// </summary>
        public string password { get; set; }

    }
}

using System.Collections.Generic;
using System.Xml;
using DataLayer.Base;
using DataLayer.HealthRulesConnectors.Enumerations;
using DataLayer.HealthRulesConnectors.Models;
using DataLayer.HealthRulesConnectors.Models.Requests;
using DataLayer.HealthRulesConnectors.Models.Responses;

namespace DataLayer.HealthRulesConnectors
{
    public class SubscriberRepository : BaseRepository<HealthRulesConnectorsDatabase>
    {
        internal SubscriberRepository(HealthRulesConnectorsDatabase database) : base(database)
        {

        }

        #region SubscriptionLookup

        /// <summary>
        /// Subscriber Lookup
        /// </summary>
        /// <param name="source">.NET Request object</param>
        /// <returns>XML Formatted string</returns>
        public string SubscriptionLookupText(SubscriptionLookupRequest source)
        {
            string result = "";

            if (source != null)
            {
                // Base SOAP call object
                GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

                ExternalCallsProperties conn = new ExternalCallsProperties()
                {
                    username = objSoap.username,
                    password = objSoap.password,
                    url = "SubscriptionLookup"
                };

                //List<string> forceJSONList = new List<string>() { "memberId" };

                result = objSoap.callObject.Get(conn, source.Get());
            }

            return result;
        }


        /// <summary>
        /// Subscriber Lookup
        /// </summary>
        /// <param name="source">.NET Request object</param>
        /// <returns>XML Document Response object</returns>
        public XmlDocument SubscriptionLookupXML(SubscriptionLookupRequest source)
        {
            string xml = SubscriptionLookupText(source);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            return doc;
        }


        /// <summary>
        /// Subscriber Lookup
        /// </summary>
        /// <param name="source">.NET Request object</param>
        /// <returns>.NET Response Object</returns>
        public SubscriptionLookupResponse.Response SubscriptionLookup(SubscriptionLookupRequest source)
        {
            SubscriptionLookupResponse.Response result = new SubscriptionLookupResponse.Response();

            if (source != null)
            {
                // Base SOAP call object
                GenerateCallData objSoap = Database.GenerateCall(ApiVersionNumber.V4);

                ExternalCallsProperties conn = new ExternalCallsProperties()
                {
                    username = objSoap.username,
                    password = objSoap.password,
                    url = "SubscriptionLookup"
                };

                List<string> forceJSONList = new List<string>() {
                        "subscriptionUDT", "udtListValueSet", "qualityControlReviewConclusion", "hccIdentifier",
                        "payeeBankAccountDateRange", "error"
                    };

                result = objSoap.callObject.GetXML
                    <SubscriptionLookupResponse.Response>(conn, source.Get(), forceJSONList);
            }

            return result;
        }

        #endregion SubscriptionLookup
    }
}

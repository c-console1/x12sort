using DataLayer.Base;
using DataLayer.Base.Models;
using DataLayer.CaseManagementAPI.Models;
using Flurl.Http;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;


namespace DataLayer.CaseManagementAPI
{
    public class AuthorizationRepository : BaseRepository<CaseManagementAPIDatabase>
    {
        internal AuthorizationRepository(CaseManagementAPIDatabase database) : base(database)
        {

        }
        public async Task<RestInpatientAuthorization> GetInpatientAuthorizationAsync(string authNbr)
        {
            string json = await GetAuthorizationAsync(authNbr);
            RestInpatientAuthorization authorization = JsonConvert.DeserializeObject<RestInpatientAuthorization>(json);
            return authorization;
        }

        public async Task<RestOutpatientAuthorization> GetOutpatientAuthorizationAsync(string authNbr)
        {
            string json = await GetAuthorizationAsync(authNbr);
            RestOutpatientAuthorization authorization = JsonConvert.DeserializeObject<RestOutpatientAuthorization>(json);
            return authorization;
        }

        public async Task<RestInpatientAuthorization> GetInpatientAuthorizationAsync(string memberNbr, string authNbr)
        {
            string json = await GetAuthorizationAsync(memberNbr, authNbr);
            RestInpatientAuthorization authorization = JsonConvert.DeserializeObject<RestInpatientAuthorization>(json);
            return authorization;
        }

        public async Task<RestOutpatientAuthorization> GetOutpatientAuthorizationAsync(string memberNbr, string authNbr)
        {
            string json = await GetAuthorizationAsync(memberNbr, authNbr);
            RestOutpatientAuthorization authorization = JsonConvert.DeserializeObject<RestOutpatientAuthorization>(json);
            return authorization;
        }

        internal async Task<string> GetAuthorizationAsync(string authNbr)
        {
            HttpResponseMessage response = await Database.Api
                .Request($"/members/DEFAULT/authorizations/{authNbr}?memberIdType=EXTERNAL&authorizationIdType=AUTHORIZATION_NUMBER")
                .GetAsync();

            return await response.Content.ReadAsStringAsync();
        }

        internal async Task<string> GetAuthorizationAsync(string memberNbr, string authNbr)
        {
            HttpResponseMessage response = await Database.Api
                .Request($"/members/{memberNbr}/authorizations/{authNbr}?memberIdType=EXTERNAL&authorizationIdType=AUTHORIZATION_NUMBER")
                .GetAsync();

            return await response.Content.ReadAsStringAsync();
        }
        //TODO: Add more methods for posting and requesting data
        //TODO: Add authorization search
    }
}

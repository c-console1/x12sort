using DataLayer.Base;
using System.Linq;

namespace DataLayer.CaseManagementAPI
{
    public class CaseManagementAPIDatabase : BaseDatabase
    {
        /// <summary>
        /// API executor to make actual API calls
        /// </summary>
        public ApiExec Api { get; private set; }
        public AuthorizationRepository Authorizations { get; private set; }

        internal CaseManagementAPIDatabase(params IExecutor[] executors) : base(executors)
        {
            Api = Executors.First(q => q.DatabaseGroup.Contains("CASEMANAGEMENTAPI")) as ApiExec;
            Api.Client.Headers["Content-Type"] = "application/json";
            Authorizations = new AuthorizationRepository(this);
        }
    }
}

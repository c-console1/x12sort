using DataLayer.Base.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace DataLayer.CaseManagementAPI.Models
{
    /// <summary>
    /// Outpatient utilization management authorization object
    /// </summary>
    public class RestOutpatientAuthorization : BaseCaseManagementAPIObject, IAuthorization
    {
        #region Members
        private List<RestOutpatientService> _authorizedServices;
        #endregion

        #region Metadata
        /// <summary>
        /// Version of the authorization
        /// </summary>
        [JsonProperty(PropertyName = "version")]
        public int AuthVersion { get; set; }

        /// <summary>
        /// Authorization number
        /// </summary>
        [JsonProperty(PropertyName = "authorizationNum")]
        public string AuthorizationNumber { get; set; }

        /// <summary>
        /// Header information of the authorization
        /// </summary>
        [JsonProperty(PropertyName = "authorizationHeader")]
        public RestAuthorizationHeader Header { get; set; }
        #endregion

        #region Services
        /// <summary>
        /// Collection of the service or services for the authorization
        /// </summary>
        [JsonProperty(PropertyName = "lineItems")]
        public List<RestOutpatientService> AuthorizedServices
        {
            get => _authorizedServices;
            set => _authorizedServices = value;
        }

        List<IAuthorizedService> IAuthorization.AuthorizedServices
        {
            get => _authorizedServices.Select(serv => (IAuthorizedService)serv).ToList();
            set => _authorizedServices = value.Cast<RestOutpatientService>().ToList();
        }
        #endregion
    }
}

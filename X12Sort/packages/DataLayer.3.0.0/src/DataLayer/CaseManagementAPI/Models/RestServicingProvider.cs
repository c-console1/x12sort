using Newtonsoft.Json;

namespace DataLayer.CaseManagementAPI.Models
{
    public class RestServicingProvider : BaseCaseManagementAPIObject
    {
        [JsonProperty(PropertyName = "servicingProvider")]
        public TCRestInnerProvider Provider { get; set; }

        [JsonProperty(PropertyName = "provider")]
        public TCRestInnerProvider Data { get; set; }

        /// <summary>
        /// Provider Name
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Provider NPI
        /// </summary>
        [JsonProperty(PropertyName = "npi")]
        public string NPI { get; set; }

        [JsonProperty(PropertyName = "servicingFacility")]
        public TCRestInnerProvider Facility { get; set; }
    }
}

using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace DataLayer.CaseManagementAPI.Models
{
    public class RestAuthorizationHeader : BaseCaseManagementAPIObject
    {
        /// <summary>
        /// Authorization number
        /// </summary>
        [JsonProperty(PropertyName = "authorizationNum")]
        public string AuthorizationNumber { get; set; }

        /// <summary>
        /// Member number. Casenet API does not return this in getAuthorization.
        /// </summary>
        [JsonIgnore]
        public string MemberNumber { get; set; }

        /// <summary>
        /// Denotes whether the authorization is open
        /// </summary>
        [JsonProperty(PropertyName = "isOpen")]
        public bool Open { get; set; }

        /// <summary>
        /// Overall status of the authorization (approved, partial approved, denied, etc)
        /// </summary>
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }

        /// <summary>
        /// Party from which the authorization was referred (PCP, self, specialist, etc)
        /// </summary>
        [JsonIgnore]
        public string ReferralSource { get; set; }

        /// <summary>
        /// Date of the authorization
        /// </summary>
        [JsonProperty(PropertyName = "stateDate")]
        public DateTime? StateDate { get; set; }

        /// <summary>
        /// Date the authorization was requested
        /// </summary>
        [JsonProperty(PropertyName = "requestDate")]
        public DateTime? RequestedDate { get; set; }

        /// <summary>
        /// Inpatient only, target admission date
        /// </summary>
        [JsonProperty(PropertyName = "targetAdmitDate")]
        public DateTime? TargetAdmitDate { get; set; }

        /// <summary>
        /// Inpatient only, target discharge date
        /// </summary>
        [JsonProperty(PropertyName = "targetDischargeDate")]
        public DateTime? TargetDischargeDate { get; set; }

        /// <summary>
        /// Inpatient only, actual discharge date
        /// </summary>
        [JsonProperty(PropertyName = "dischargeDate")]
        public DateTime? ActualDischargeDate { get; set; }

        /// <summary>
        /// Inpatient only, first admission date
        /// </summary>
        [JsonProperty(PropertyName = "firstAdmitDate")]
        public DateTime? FirstAdmitDate { get; set; }

        /// <summary>
        /// Input source of the authorization
        /// </summary>
        [JsonIgnore]
        public string InputSource { get; set; }

        /// <summary>
        /// Date the authorization was moved to the claims system
        /// </summary>
        [JsonProperty(PropertyName = "dateSentToClaims")]
        public DateTime? SentToClaims { get; set; }

        /// <summary>
        /// User defined elements to an authorization. Can be anything.
        /// </summary>
        [JsonProperty(PropertyName = "extendedAttributes")]
        public Dictionary<string, RestStringUDF> UserDefined { get; set; }

        /// <summary>
        /// Provider information for the provider whom requested the services
        /// </summary>
        [JsonProperty(PropertyName = "requestingProvider")]
        public RestRequestingProvider RequestingProvider { get; set; }

        #region Diagnosis
        /// <summary>
        /// Diagnosis information for the authorization
        /// </summary>
        [JsonProperty(PropertyName = "authorizationDiagnosis")]
        public RestAuthorizationDiagnosis AuthorizationDiagnosis { get; set; }
        #endregion
    }
}

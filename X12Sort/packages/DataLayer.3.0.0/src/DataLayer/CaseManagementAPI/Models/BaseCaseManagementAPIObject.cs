using DataLayer.Base.Models;

namespace DataLayer.CaseManagementAPI.Models
{
    public class BaseCaseManagementAPIObject : BaseDataObject
    {
        /// <summary>
        /// Standard Constructor, Should be called by all inheriting objects
        /// </summary>
        protected BaseCaseManagementAPIObject() : base()
        {
        }
    }
}

using DataLayer.Base.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.CaseManagementAPI.Models
{
    public class RestUDFValue : BaseCaseManagementAPIObject, IOptionValue
    {
        [JsonProperty(PropertyName = "id")]
        public string ID { get; set; }

        [JsonProperty(PropertyName = "label")]
        public string Label { get; set; }

        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }

        [JsonProperty(PropertyName = "disabled")]
        public bool Disabled { get; set; }
    }

    public class RestServiceType : BaseCaseManagementAPIObject, IOptionValue
    {
        [JsonProperty(PropertyName = "id")]
        public string ID { get; set; }

        [JsonProperty(PropertyName = "label")]
        public string Label { get; set; }

        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }

        [JsonProperty(PropertyName = "disabled")]
        public bool Disabled { get; set; }
    }

    public class RestLineItemType : BaseCaseManagementAPIObject, IOptionValue
    {
        [JsonProperty(PropertyName = "id")]
        public string ID { get; set; }

        [JsonProperty(PropertyName = "label")]
        public string Label { get; set; }

        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }

        [JsonProperty(PropertyName = "disabled")]
        public bool Disabled { get; set; }
    }

    public class RestPlaceOfService : BaseCaseManagementAPIObject, IOptionValue
    {
        [JsonProperty(PropertyName = "id")]
        public string ID { get; set; }

        [JsonProperty(PropertyName = "label")]
        public string Label { get; set; }

        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }

        [JsonProperty(PropertyName = "disabled")]
        public bool Disabled { get; set; }
    }

    public class RestUpdatedBy : BaseCaseManagementAPIObject
    {
        [JsonProperty(PropertyName = "id")]
        public string ID { get; set; }

        [JsonProperty(PropertyName = "firstname")]
        public string FirstName { get; set; }

        [JsonProperty(PropertyName = "lastname")]
        public string LastName { get; set; }

        [JsonProperty(PropertyName = "username")]
        public string UserName { get; set; }
    }

    public class RestStayLevel : BaseCaseManagementAPIObject, IOptionValue
    {
        [JsonProperty(PropertyName = "id")]
        public string ID { get; set; }

        [JsonProperty(PropertyName = "label")]
        public string Label { get; set; }

        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }

        [JsonProperty(PropertyName = "disabled")]
        public bool Disabled { get; set; }
    }

    public class RestLevelOfUrgency : BaseCaseManagementAPIObject, IOptionValue
    {
        [JsonProperty(PropertyName = "id")]
        public string ID { get; set; }

        [JsonProperty(PropertyName = "label")]
        public string Label { get; set; }

        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }

        [JsonProperty(PropertyName = "disabled")]
        public bool Disabled { get; set; }
    }

    public class RestUrgencyRequestedBy : BaseCaseManagementAPIObject, IOptionValue
    {
        [JsonProperty(PropertyName = "id")]
        public string ID { get; set; }

        [JsonProperty(PropertyName = "label")]
        public string Label { get; set; }

        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }

        [JsonProperty(PropertyName = "disabled")]
        public bool Disabled { get; set; }
    }

    public class RestExplanation : BaseCaseManagementAPIObject, IOptionValue
    {
        [JsonProperty(PropertyName = "id")]
        public string ID { get; set; }

        [JsonProperty(PropertyName = "label")]
        public string Label { get; set; }

        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }

        [JsonProperty(PropertyName = "disabled")]
        public bool Disabled { get; set; }
    }
    public class RestVarianceReason : BaseCaseManagementAPIObject, IOptionValue
    {
        [JsonProperty(PropertyName = "id")]
        public string ID { get; set; }

        [JsonProperty(PropertyName = "label")]
        public string Label { get; set; }

        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }

        [JsonProperty(PropertyName = "disabled")]
        public bool Disabled { get; set; }
    }

    public class RestMedNecessity : BaseCaseManagementAPIObject, IOptionValue
    {
        [JsonProperty(PropertyName = "id")]
        public string ID { get; set; }

        [JsonProperty(PropertyName = "label")]
        public string Label { get; set; }

        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }

        [JsonProperty(PropertyName = "disabled")]
        public bool Disabled { get; set; }
    }

    public class RestStayLevelMet : BaseCaseManagementAPIObject, IOptionValue
    {
        [JsonProperty(PropertyName = "id")]
        public string ID { get; set; }

        [JsonProperty(PropertyName = "label")]
        public string Label { get; set; }

        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }

        [JsonProperty(PropertyName = "disabled")]
        public bool Disabled { get; set; }
    }
}

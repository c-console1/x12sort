using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.CaseManagementAPI.Models
{
    public class RestUDFBase
    {
        [JsonProperty(PropertyName = "__class")]
        public string Class { get; set; }
    }
    public class RestStringUDF : RestUDFBase
    {
        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }
    }

    public class RestOptionValueUDF : RestUDFBase
    {
        [JsonProperty(PropertyName = "value")]
        public RestUDFValue Value { get; set; }
    }
}

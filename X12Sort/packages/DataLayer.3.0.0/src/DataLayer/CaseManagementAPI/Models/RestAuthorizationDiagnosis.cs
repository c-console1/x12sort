using DataLayer.Base.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace DataLayer.CaseManagementAPI.Models
{
    public class RestAuthorizationDiagnosis : BaseCaseManagementAPIObject
    {
        [JsonProperty(PropertyName = "primaryAdmittingDiagnosis")]
        public RestDiagnosis AdmittingDiagnosis { get; set; }
        [JsonProperty(PropertyName = "primaryDiagnosis")]
        public RestDiagnosis PrimaryDiagnosis { get; set; }
        [JsonProperty(PropertyName = "diagnosisCodeSet")]
        public List<RestDiagnosis> DiagnosisCodeSet { get; set; }
    }

    public class RestDiagnosis : BaseCaseManagementAPIObject, IAuthorizationDiagnosis
    {
        [JsonIgnore]
        public string Description { get; set; }
        [JsonProperty(PropertyName = "diagnosisCode")]
        public string Code { get; set; }
        [JsonProperty(PropertyName = "diagnosisName")]
        public string DiagnosisName { get; set; }
        [JsonIgnore]
        public string DiagnosisType { get; set; }
        [JsonProperty(PropertyName = "effectiveDate")]
        public DateTime? StartDate { get; set; }

        [JsonProperty(PropertyName = "terminationDate")]
        public DateTime? EndDate { get; set; }
    }
}

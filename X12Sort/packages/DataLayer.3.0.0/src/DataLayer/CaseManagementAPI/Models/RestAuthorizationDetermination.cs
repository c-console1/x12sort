using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.CaseManagementAPI.Models
{
    public class RestAuthorizationDetermination : BaseCaseManagementAPIObject
    {
        /// <summary>
        /// User defined elements to an authorization. Can be anything.
        /// </summary>
        [JsonProperty(PropertyName = "extendedAttributes")]
        public Dictionary<string, RestOptionValueUDF> UserDefined { get; set; }

        [JsonProperty(PropertyName = "determinationDueDate")]
        public DateTime DueDate { get; set; }

        [JsonProperty(PropertyName = "readyForClaims")]
        public bool ClaimReady { get; set; }

        [JsonProperty(PropertyName = "unitsToDetermine")]
        public int UnitsToDetermine { get; set; }

        [JsonProperty(PropertyName = "dateSentToClaims")]
        public DateTime DateSentToClaims { get; set; }

        [JsonProperty(PropertyName = "trackingNum")]
        public string TrackingNumber { get; set; }

        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }

        [JsonProperty(PropertyName = "currentDecisionList")]
        public List<RestAuthorizationDecision> CurrentDecisionList { get; set; }

        [JsonProperty(PropertyName = "archivedDecisionList")]
        public List<RestAuthorizationDecision> ArchivedDecisionList { get; set; }

        [JsonProperty(PropertyName = "nextReviewDate")]
        public DateTime NextReviewDate { get; set; }
    }

    public class RestAuthorizationDecision : BaseCaseManagementAPIObject
    {
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }

        [JsonProperty(PropertyName = "units")]
        public int Units { get; set; }

        [JsonProperty(PropertyName = "decisionDate")]
        public DateTime DecisionDate { get; set; }

        [JsonProperty(PropertyName = "isCurrent")]
        public bool IsCurrent { get; set; }

        [JsonProperty(PropertyName = "varianceReason")]
        public RestVarianceReason VarianceReason { get; set; }

        [JsonProperty(PropertyName = "medNecessity")]
        public RestMedNecessity medNecessity { get; set; }

        [JsonProperty(PropertyName = "stayLevelMet")]
        public RestStayLevelMet RestStayLevelMet { get; set; }

        [JsonProperty(PropertyName = "claimInformation")]
        public string ClaimInformation { get; set; }

        [JsonProperty(PropertyName = "explanation")]
        public RestExplanation Explanation { get; set; }

        [JsonProperty(PropertyName = "toDate")]
        public DateTime ToDate { get; set; }

        [JsonProperty(PropertyName = "fromDate")]
        public DateTime FromDate { get; set; }
    }

}

using DataLayer.Base.Models;
using Newtonsoft.Json;
using System;


namespace DataLayer.CaseManagementAPI.Models
{
    public class RestAdmission : BaseCaseManagementAPIObject, IAdmission
    {
        [JsonIgnore]
        public DateTime? StartDate { get; set; }
        [JsonIgnore]
        public DateTime? EndDate { get; set; }
        [JsonIgnore]
        public DateTime? AdmissionDate { get; set; }
        [JsonIgnore]
        public string AdmissionSource { get; set; }
        [JsonIgnore]
        public string AdmissionType { get; set; }
    }
}

using DataLayer.Base.Models;
using Newtonsoft.Json;

namespace DataLayer.CaseManagementAPI.Models
{
    /// <summary>
    /// Class to house requesting provider data
    /// </summary>
    public class RestRequestingProvider : BaseCaseManagementAPIObject
    {
        [JsonProperty(PropertyName = "requestingProvider")]
        public TCRestInnerProvider InnerProvider { get; set; }

        /// <summary>
        /// Name of contact at provider location
        /// </summary>
        [JsonProperty(PropertyName = "authSpecificContactName")]
        public string ProviderContactName { get; set; }

        /// <summary>
        /// Provider contact phone
        /// </summary>
        [JsonProperty(PropertyName = "authSpecificContactPhone")]
        public string ContactPhone { get; set; }
    }

    /// <summary>
    /// Class to house the inner provider details of the response object because the casenet API is stupid
    /// </summary>
    public class TCRestInnerProvider : BaseCaseManagementAPIObject, IAuthorizationProvider
    {
        /// <summary>
        /// Database key
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public string ID { get; set; }

        /// <summary>
        /// Provider external id
        /// </summary>
        [JsonProperty(PropertyName = "externalId")]
        public string ExternalNumber { get; set; }

        /// <summary>
        /// Identifier for provider
        /// </summary>
        [JsonProperty(PropertyName = "providerNumber")]
        public string Number { get; set; }

        /// <summary>
        /// Provider Name. Different schemas on the Casenet side require multiple properties for the name
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Provider Name. Different schemas on the Casenet side require multiple properties for the name
        /// </summary>
        [JsonProperty(PropertyName = "providerName")]
        public string SecondName { get; set; }

        /// <summary>
        /// Provider NPI
        /// </summary>
        [JsonProperty(PropertyName = "npi")]
        public string NPI { get; set; }

        /// <summary>
        /// Provider tin. Not in getAuthorization response
        /// </summary>
        [JsonIgnore]
        public string TaxID { get; set; }
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Configuration.Models
{
    /// <summary>
    /// API Authentication Types
    /// <para>
    /// This item is internal to the is probably not what you want unless you are working with an application's data layer configuration
    /// </para>
    /// </summary>
    public enum ApiAuthenticationType
    {
        /// <summary>
        /// Authentication not required
        /// </summary>
        None,
        /// <summary>
        /// Bearer authentication token 
        /// </summary>
        Token,
        /// <summary>
        /// Basic Authentication with user name and password
        /// </summary>
        Basic
    }
}

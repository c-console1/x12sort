using Oracle.ManagedDataAccess.Client;
using System;
using System.Data.Common;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;

namespace DataLayer.Configuration.Models
{
    /// <summary>
    /// Connection String Configuration Object
    /// </summary>
    /// <remarks>
    /// This internally used an <c> DbConnectionStringBuilder </c> to build a connection string. The <c> ProviderType </c> is used to determine what builder type to use.
    /// <para>
    /// This item is internal to the is probably not what you want unless you are working with an application's data layer configuration
    /// </para>
    /// </remarks>
    public class ConfigConnectionString : ConfigBaseItem, ILogicalConnection, ICrudFunc
    {
        private string _additionalOptions;
        private ProviderType _provider;
        private DbConnectionStringBuilder _builder { get; set; }

        /// <summary>
        /// Internal Unique ID for the ConnectionID 
        /// </summary>
        public Guid ConnectionID { get; set; }

        /// <summary>
        /// Internal Unique ID for the ConnectionStringID
        /// </summary>
        [Obsolete("Use `ConnectionID` instead")]
        internal Guid ConnectionStringID
        {
            get { return ConnectionID; }
            set { ConnectionID = value; }
        }

        /// <summary>
        /// The logical database that this connection string belongs
        /// </summary>
        public string DatabaseGroup { get; set; }

        /// <summary>
        /// Is this connection string the default for the logical database
        /// </summary>
        public bool IsDefault { get; set; }

        /// <summary>
        /// Human friendly name to refer to this connection string
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Type of connection required: Oracle, MS SQL, OLE,
        /// </summary>
        public ProviderType Provider
        {
            get { return _provider; }
            set
            {
                string cxnStr = _builder?.ConnectionString ?? "";
                DbConnectionStringBuilder newBuilder;
                switch (value)
                {
                    case ProviderType.Oracle:
                        newBuilder = new OracleConnectionStringBuilder(cxnStr);
                        break;

                    case ProviderType.Sql:
                        newBuilder = new SqlConnectionStringBuilder(cxnStr);
                        break;

                    default:
                        throw new NotSupportedException($"Provider Type: {Provider.ToString()} not defined");
                }
                _builder = newBuilder;
                _provider = value;
            }
        }

        /// <summary>
        /// The server that the database lives on
        /// </summary>
        public string DataSource
        {
            get
            {
                if (_builder.ContainsKey("Data Source"))
                {
                    return _builder["Data Source"].ToString();
                }
                else
                {
                    return "";
                }
            }
            set
            {
                _builder["Data Source"] = value;
            }
        }

        /// <summary>
        /// The database for the connection
        /// </summary>
        public string InitialCatalog
        {
            get
            {
                if (_builder.ContainsKey("Initial Catalog"))
                {
                    return _builder["Initial Catalog"].ToString();
                }
                else
                {
                    return "";
                }
            }
            set
            {
                _builder["Initial Catalog"] = (string.IsNullOrEmpty(value)) ? null : value;
            }
        }

        /// <summary>
        /// The user to connect as
        /// </summary>
        public string UserID
        {
            get
            {
                if (_builder.ContainsKey("User Id"))
                {
                    return _builder["User Id"].ToString();
                }
                else
                {
                    return "";
                }
            }
            set
            {
                _builder["User Id"] = value;
            }
        }

        /// <summary>
        /// Use the integrate security or windows login
        /// </summary>
        public bool IntegratedSecurity
        {
            get
            {
                if (_builder.ContainsKey("Integrated Security"))
                {
                    return Convert.ToBoolean(_builder["Integrated Security"]);
                }
                else
                {
                    return false;
                }
            }
            set
            {
                if (_provider == ProviderType.Sql)
                {
                    _builder["Integrated Security"] = value;
                }
            }
        }

        /// <summary>
        /// Any additional connection string settings: "MultipleActiveResultSets=true;TrustServerCertificate=true;"
        /// </summary>
        public string Additional
        {
            get
            {
                return _additionalOptions;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    _additionalOptions = value;
                    foreach (string setting in value.Split(';').Where(s => !string.IsNullOrEmpty(s)))
                    {
                        string[] props = setting.Split('=');
                        _builder[props[0]] = props[1];
                    }
                }
            }
        }

        /// <summary>
        /// Is this connection string referring to a deprecated system or login
        /// </summary>
        public bool IsDeprecated { get; set; }

        internal ConfigConnectionString()
        {
            Provider = ProviderType.Sql;
            ConnectionID = Guid.Empty;
            IsDefault = false;
            IsDeprecated = false;
        }

        internal DbConnectionStringBuilder Builder { get { return _builder; } }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString()
        {
            string cxn = DataSource ?? "(null)";
            if (!string.IsNullOrEmpty(InitialCatalog))
            {
                cxn += "/" + InitialCatalog;
            }
            string security = IntegratedSecurity ? "Integrated Security" : $"User='{UserID}'";
            return $"{Name}, Connection: {cxn}, Security: {security}";
        }

        /// <summary>
        /// Clones this connection string into a new <c>DbConnectionStringBuilder</c>
        /// </summary>
        /// <returns>A deep clone of a <c>DbConnectionStringBuilder</c></returns>
        public DbConnectionStringBuilder Clone()
        {
            string cxnStr = _builder?.ConnectionString ?? "";
            DbConnectionStringBuilder newBuilder;
            switch (Provider)
            {
                case ProviderType.Oracle:
                    newBuilder = new OracleConnectionStringBuilder(cxnStr);
                    break;

                case ProviderType.Sql:
                    newBuilder = new SqlConnectionStringBuilder(cxnStr);
                    break;

                default:
                    throw new NotSupportedException($"Provider Type: {Provider.ToString()} not defined");
            }
            return newBuilder;
        }

        /// <summary>
        /// Convert this object to an anonymous object for using as a parameter in query
        /// </summary>
        /// <remarks>
        /// Converts empty string to null for saving in the database
        /// </remarks>
        /// <returns> New object that can be used to save this object </returns>
        public override object ToParameterObject()
        {
            return new
            {
                Name,
                DatabaseGroup,
                Provider = Provider.ToString(),
                DataSource = string.IsNullOrEmpty(DataSource) ? null : DataSource,
                InitialCatalog = string.IsNullOrEmpty(InitialCatalog) ? null : InitialCatalog,
                UserID = string.IsNullOrEmpty(UserID) ? null : UserID,
                IntegratedSecurity,
                Additional = string.IsNullOrEmpty(Additional) ? null : Additional,
            };
        }

        /// <summary>
        /// SQL Query to create an new <c>ConfigConnectionStrings</c>
        /// </summary>
        /// <returns>Returns a parameterized sql string</returns>
        public string CreateQuery()
        {
            return @"
INSERT INTO [ConfigConnectionStrings]
    ([Name],[DatabaseGroup],[Provider],[DataSource],[InitialCatalog],[UserID],[IntegratedSecurity],[Additional])
VALUES
    (@Name, @DatabaseGroup, @Provider, @DataSource, @InitialCatalog, @UserID, @IntegratedSecurity, @Additional)
";
        }


        /// <summary>
        /// SQL Query to read a <c>ConfigConnectionStrings</c> Object
        /// </summary>
        /// <returns>Returns a parameterized sql string</returns>
        public string ReadQuery()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// SQL Query to update a <c>ConfigConnectionStrings</c> object
        /// </summary>
        /// <returns>Returns a parameterized sql string</returns>
        public string UpdateQuery()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// SQL Query to delete a <c>ConfigConnectionStrings</c> object
        /// </summary>
        /// <returns>Returns a parameterized sql string</returns>
        public string DeleteQuery()
        {
            throw new NotImplementedException();
        }
    }
}

using System;

namespace DataLayer.Configuration.Models
{
    /// <summary>
    /// Interface to represent a generic database configuration
    /// </summary>
    /// <remarks>
    /// This item is internal to the is probably not what you want unless you are working with an application's data layer configuration 
    /// </remarks>
    public interface ILogicalDatabase
    {
        /// <summary>
        /// Connection String object to use as the connection for this database
        /// </summary>
        ILogicalConnection Connection { get; }

        /// <summary>
        /// Internal Unique ID for the ConnectionStringID that this object references
        /// </summary>
        Guid ConnectionID { get; }

        /// <summary>
        /// Internal Unique ID for the Database
        /// </summary>
        int DatabaseID { get; }

        /// <summary>
        /// Internal Unique ID for the MachineApplication that this object belongs to
        /// </summary>
        int MachineApplicationID { get; }
        /// <summary>
        /// Database name, should match what is in the DataAccess object
        /// </summary>
        string Name { get; }
    }
}

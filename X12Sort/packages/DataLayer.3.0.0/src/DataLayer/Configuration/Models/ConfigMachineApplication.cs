using System;
using System.Collections.Generic;
using System.Linq;

namespace DataLayer.Configuration.Models
{
    /// <summary>
    /// An application configuration, dependent upon the machine
    /// </summary>
    /// <remarks>
    /// <para>
    /// This item is internal to the is probably not what you want unless you are working with an application's data layer configuration
    /// </para>
    /// </remarks>
    public class ConfigMachineApplication : ConfigBaseItem
    {
        private List<ConfigApiDatabase> _apiDatabases;
        private List<ConfigDatabase> _databases;
        private List<ConfigAppVar> _variables;

        /// <summary>
        /// Standard Constructor
        /// </summary>
        public ConfigMachineApplication()
        {
            MachineApplicationID = -1;
            _databases = new List<ConfigDatabase>();
            _apiDatabases = new List<ConfigApiDatabase>();
            _variables = new List<ConfigAppVar>();
        }

        /// <summary>
        /// Internal Unique ID for the MachineApplication
        /// </summary>
        public int MachineApplicationID { get; internal set; }

        /// <summary>
        /// The Config Machine ID for this application
        /// </summary>
        public int MachineID { get; internal set; }

        /// <summary>
        /// The Config Machine for this application
        /// </summary>
        public ConfigMachine Machine { get; set; }

        /// <summary>
        /// Name for this application, should be a AlphaNumeric
        /// </summary>
        public string ApplicationName { get; set; }

        /// <summary>
        /// What type of application: Batch job, Web App, Config Template
        /// </summary>
        public string ApplicationType { get; set; }

        /// <summary>
        /// Human description of the application
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Is this app a template
        /// </summary>
        /// <value>Compares application type to "Template"</value>
        public bool IsTemplate { get { return ApplicationType == "Template"; } }

        /// <summary>
        /// List of Config Databases used by this application (only SQL Databases)
        /// </summary>
        public IEnumerable<ConfigDatabase> Databases
        {
            get { return _databases; }
            internal set { _databases = value.ToList(); }
        }

        /// <summary>
        /// List of Config Databases used by this application (only Api Databases)
        /// </summary>
        public IEnumerable<ConfigApiDatabase> ApiDatabases
        {
            get { return _apiDatabases; }
            internal set { _apiDatabases = value.ToList(); }
        }

        /// <summary>
        /// List of Config ILogical Databases used by this application
        /// </summary>
        internal IEnumerable<ILogicalDatabase> LogicalDatabases
        {
            get
            {
                foreach (var db in _databases) { yield return db as ILogicalDatabase; }
                foreach (var db in _apiDatabases) { yield return db as ILogicalDatabase; }
            }
        }

        /// <summary>
        /// List of Config AppVar used by this application
        /// </summary>
        public IEnumerable<ConfigAppVar> Variables { get { return _variables; } internal set { _variables = value.ToList(); } }

        /// <summary>
        /// Convert this object to an anonymous object for using as a parameter in query
        /// </summary>
        /// <returns> New object that can be used to save this object </returns>
        public override object ToParameterObject()
        {
            return new
            {
                MachineApplicationID,
                ApplicationName,
                ApplicationType,
                Description,
                MachineID,
            };
        }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString()
        {
            return $"{ApplicationName}, {ApplicationType} [{Description}]";
        }

        /// <summary>
        /// Gets a subset of the machine/application pair information
        /// </summary>
        /// <returns>Subset of configuration</returns>
        internal MachineApplicationInfo GetMachineApplicationInfo()
        {
            return new MachineApplicationInfo()
            {
                MachineApplicationID = MachineApplicationID,
                MachineID = Machine.MachineID,
                MachineName = Machine.MachineName,
                MachineDescription = Machine.Description,
                MachineIsProduction = Machine.IsProduction,
                ApplicationName = ApplicationName,
                ApplicationType = ApplicationType,
                ApplicationDescription = Description,
                Variables = Variables.ToDictionary(v => v.Name, v => v.Value)
            };
        }
    }
}

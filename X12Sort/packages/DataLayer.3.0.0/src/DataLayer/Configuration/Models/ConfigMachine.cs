using System;

namespace DataLayer.Configuration.Models
{
    /// <summary>
    /// The Machine Configuration Object
    /// </summary>
    /// <remarks>
    /// <para>
    /// This item is internal to the is probably not what you want unless you are working with an application's data layer configuration
    /// </para>
    /// </remarks>
    public class ConfigMachine : ConfigBaseItem
    {
        /// <summary>
        /// Standard Constructor
        /// </summary>
        public ConfigMachine()
        {
            MachineID = -1;
        }

        /// <summary>
        /// Internal Unique ID for the Machine
        /// </summary>
        public int MachineID { get; internal set; }

        /// <summary>
        /// Machine Name, Should be what is obtained from `Environment.MachineName`
        /// </summary>
        public string MachineName { get; set; }

        /// <summary>
        /// Human description of the machine, i.e. Local development machine for Joseph Livecchi
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Flag Indicating if the is a Production Run, from the Configuration Database
        /// </summary>
        public bool IsProduction { get; set; }

        /// <summary>
        /// Does the object have IDs from the database
        /// </summary>
        public bool IsLoaded()
        {
            return MachineID > -1;
        }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns> A string that represents the current object.</returns>
        public override string ToString()
        {
            string prod = IsProduction ? "**PRODUCTION**" : "";
            return $"{MachineName} [{Description}] {prod}";
        }

        /// <summary>
        /// Convert this object to an anonymous object for using as a parameter in query
        /// </summary>
        /// <returns> New object that can be used to save this object </returns>
        public override object ToParameterObject()
        {
            return new
            {
                MachineID,
                MachineName
            };
        }
    }
}

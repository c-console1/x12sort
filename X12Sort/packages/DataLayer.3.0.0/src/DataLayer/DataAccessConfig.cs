using System;
using System.Collections.Generic;
using System.Linq;

namespace DataLayer
{
    /// <summary>
    /// Users configuration for the DataAccess class. All user configurable settings should be placed in this class so they can be load/saved.
    /// </summary>
    /// <seealso cref="DataAccessConfigBuilder"/>
    /// <seealso cref="DbExec.Create"/>
    public class DataAccessConfig
    {
        /// <summary>
        /// List of potential environment templates that can be applied
        /// </summary>
        public static List<string> EnvironmentTemplateNames
        {
            get
            {
                return new List<string>()
                {
                    "DL_DEV",
                    "DEV",
                    "QA",
                    "PROD"
                };
            }
        }

        /// <summary>
        /// This is the full connection string to the AppConfig Database
        /// </summary>
        public string AppConfigConnectionString { get; set; } = "";

        /// <summary>
        /// This is the base URL for the HealthEdge PIK
        /// </summary>
        public string HealthEdgePIKBaseRESTURL { get; set; } = "";

        /// <summary>
        /// This is the number of days the token is registered for in use in the HealthEdge PIK
        /// </summary>
        public int HealthEdgePIKTokenLifeDays { get; set; } = 0;

        /// <summary>
        /// Dictionary of connection string that dam might can also use
        /// </summary>
        /// <remarks>
        /// This is designed to allow dam to be able to different connections for one off commands
        /// </remarks>
        public Dictionary<string, string> Connections { get; set; } = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

        /// <summary>
        /// This property will toggle the swallowing of logging database errors
        /// </summary>
        /// <remarks>
        /// **This is primary for production and only for the logging database**
        /// <para>
        /// We want to allow jobs to continue running in the case where the logging database goes down or has issues. This prevents the logging database from being 100% mission critical and breaking *everything* if it is broke
        /// </para>
        /// <para>
        /// If this is set to true, any exceptions are logged to the console, but it will not be thrown further up the stack.
        /// If set to false, any exception logged to console and not handled
        /// </para>
        /// <para> 
        /// **This value is read at configuration load and not reloaded**
        /// </para>
        /// </remarks>
        public bool ThrowLoggingDatabaseExceptions { get; set; } = true;

        /// <summary>
        /// Apply an environment template to the DAM configuration
        /// </summary>
        /// <param name="environmentTemplateName">Name of template to apply</param>
        /// <returns>Whether the template application was successful</returns>
        public bool ApplyEnvironmentTemplate(string environmentTemplateName)
        {
            switch (environmentTemplateName)
            {
                case "DL_DEV":
                    AppConfigConnectionString = "Data Source=PHSDBTEST16;Initial Catalog=PHC_AppConfig_Dev;Integrated Security=SSPI";
                    HealthEdgePIKBaseRESTURL = "http://100.112.32.203:8080/";
                    HealthEdgePIKTokenLifeDays = 1;
                    Connections = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
                    ThrowLoggingDatabaseExceptions = true;
                    break;
                case "DEV":
                    AppConfigConnectionString = "Data Source=PHSDBTEST16;Initial Catalog=PHC_AppConfig;Integrated Security=SSPI";
                    HealthEdgePIKBaseRESTURL = "http://100.112.32.203:8080/";
                    HealthEdgePIKTokenLifeDays = 1;
                    Connections = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
                    ThrowLoggingDatabaseExceptions = true;
                    break;
                case "QA":
                    AppConfigConnectionString = "Data Source=PHSDBTEST16;Initial Catalog=PHC_AppConfig;Integrated Security=SSPI";
                    HealthEdgePIKBaseRESTURL = "http://100.112.32.203:8080/";
                    HealthEdgePIKTokenLifeDays = 1;
                    Connections = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
                    ThrowLoggingDatabaseExceptions = true;
                    break;
                case "PROD":
                    AppConfigConnectionString = "Data Source=PHSSQL1415;Initial Catalog=PHC_AppConfig;Integrated Security=SSPI";
                    HealthEdgePIKBaseRESTURL = "http://100.112.32.203:8080/";
                    HealthEdgePIKTokenLifeDays = 7;
                    Connections = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
                    ThrowLoggingDatabaseExceptions = true;
                    break;
                default:
                    return false;
            }
            return true;
        }

        public override string ToString()
        {
            string connections = string.Join(Environment.NewLine, Connections.Keys.Select(q => $"    '{q} = '{Connections[q]}'"));

            return $@"
* {nameof(AppConfigConnectionString)} = '{AppConfigConnectionString}'
* {nameof(HealthEdgePIKBaseRESTURL)} = '{HealthEdgePIKBaseRESTURL}'
* {nameof(HealthEdgePIKTokenLifeDays)} = '{HealthEdgePIKTokenLifeDays}'
* {nameof(Connections)} = {{
{connections}
  }}
* {nameof(ThrowLoggingDatabaseExceptions)} = {ThrowLoggingDatabaseExceptions}
";
        }
    }
}

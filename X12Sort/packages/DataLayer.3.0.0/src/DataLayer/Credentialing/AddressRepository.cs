using DataLayer.Base;
using DataLayer.Credentialing.Models;
using DataLayer.StaticUtilities;
using USAddress;

namespace DataLayer.Credentialing
{
	public class AddressRepository : BaseRepository<CredentialingDatabase>
	{
		internal AddressRepository(CredentialingDatabase database) : base(database)
		{

		}

		/// <summary>
		/// Gets an address by database key
		/// </summary>
		/// <param name="addressKey">Key to get address for</param>
		/// <returns></returns>
		public Address GetAddress(string addressKey)
		{
			string sql = @"select address_k as DatabaseKey, entity_k as EntityKey, rtrim(addressline1) as Address1, rtrim(addressline2) as Address2, rtrim(city) as City, rtrim(county) as County,
				state as State, substring(rtrim(zipcode), 1, 5) as Zip, phone as Phone, fax as Fax
				from visualcactus.addresses a
				where 1=1
				and a.address_k = @addressKey
				and a.active = 1";

			Address result = Database.QueryFirstOrDefault<Address>(sql, new
			{
				addressKey = addressKey
			});

			return result;
		}

		/// <summary>
		/// Gets address matching input line 1, line 2, city, state zip, phone, fax
		/// </summary>
		/// <param name="address1">Address line 1 to match</param>
		/// <param name="address2">Address line 2 to match</param>
		/// <param name="city">City to match</param>
		/// <param name="state">State to match</param>
		/// <param name="zip">Zip code to match</param>
		/// <param name="phone">Phone number to match</param>
		/// <param name="fax">Fax number to match</param>
		/// <returns></returns>
		public Address GetAddress(string address1, string address2, string city, string state, string zip, string phone, string fax)
		{
			if (address1 == null)
				address1 = "";
			if (address2 == null)
				address2 = "";
			if ( city == null)
				 city = "";
			if ( state == null)
				 state = "";
			if ( zip == null)
				 zip = "";
			if ( phone == null)
				 phone = "";
			if ( fax == null)
				 fax = "";

			string sql = @"select address_k as DatabaseKey, entity_k as EntityKey, rtrim(addressline1) as Address1, rtrim(addressline2) as Address2, rtrim(city) as City, rtrim(county) as County,
				state as State, substring(rtrim(zipcode), 1, 5) as Zip, phone as Phone, fax as Fax
				from visualcactus.addresses a
				where 1=1
				and a.addressline1 = @address1
				and a.addressline2 = @address2
				and a.city = @city
				and a.state = @state
				and a.zipcode = @zip
				and a.phone = @phone
				and a.fax = @fax
				and a.entity_k in ('      para', '       all')
				and a.active = 1";

			Address result = Database.QueryFirstOrDefault<Address>(sql, new
			{
				address1 = address1.Trim(),
				address2 = address2.Trim(),
				city = city.Trim(),
				state = state.Trim(),
				zip = zip.Trim(),
				phone = phone.Trim(),
				fax = fax.Trim()
			});

			return result;
		}

		/// <summary>
		/// Gets address matching input line 1, city, state zip, phone, fax
		/// </summary>
		/// <param name="address1">Address line 1 to match</param>
		/// <param name="city">City to match</param>
		/// <param name="state">State to match</param>
		/// <param name="zip">Zip code to match</param>
		/// <param name="phone">Phone number to match</param>
		/// <param name="fax">Fax number to match</param>
		/// <returns></returns>
		public Address GetAddressNoLine2(string address1, string city, string state, string zip, string phone, string fax)
		{
			if (address1 == null)
				address1 = "";
			if (city == null)
				city = "";
			if (state == null)
				state = "";
			if (zip == null)
				zip = "";
			if (phone == null)
				phone = "";
			if (fax == null)
				fax = "";

			string sql = @"select address_k as DatabaseKey, entity_k as EntityKey, rtrim(addressline1) as Address1, rtrim(addressline2) as Address2, rtrim(city) as City, rtrim(county) as County,
				state as State, substring(rtrim(zipcode), 1, 5) as Zip, phone as Phone, fax as Fax
				from visualcactus.addresses a
				where 1=1
				and a.addressline1 = @address1
				and a.city = @city
				and a.state = @state
				and a.zipcode = @zip
				and a.phone = @phone
				and a.fax = @fax
				and a.entity_k in ('      para', '       all')
				and a.active = 1";

			Address result = Database.QueryFirstOrDefault<Address>(sql, new
			{
				address1 = address1.Trim(),
				city = city.Trim(),
				state = state.Trim(),
				zip = zip.Trim(),
				phone = phone.Trim(),
				fax = fax.Trim()
			});

			return result;
		}

		/// <summary>
		/// Gets address matching input line 1, line 2, city, state zip, phone
		/// </summary>
		/// <param name="address1">Address line 1 to match</param>
		/// <param name="address2">Address line 2 to match</param>
		/// <param name="city">City to match</param>
		/// <param name="state">State to match</param>
		/// <param name="zip">Zip code to match</param>
		/// <param name="phone">Phone number to match</param>
		/// <returns></returns>
		public Address GetAddressNoFax(string address1, string address2, string city, string state, string zip, string phone)
		{
			if (address1 == null)
				address1 = "";
			if (address2 == null)
				address2 = "";
			if (city == null)
				city = "";
			if (state == null)
				state = "";
			if (zip == null)
				zip = "";
			if (phone == null)
				phone = "";

			string sql = @"select address_k as DatabaseKey, entity_k as EntityKey, rtrim(addressline1) as Address1, rtrim(addressline2) as Address2, rtrim(city) as City, rtrim(county) as County,
				state as State, substring(rtrim(zipcode), 1, 5) as Zip, phone as Phone
				from visualcactus.addresses a
				where 1=1
				and a.addressline1 = @address1
				and a.addressline2 = @address2
				and a.city = @city
				and a.state = @state
				and a.zipcode = @zip
				and a.phone = @phone
				and a.entity_k in ('      para', '       all')
				and a.active = 1";

			Address result = Database.QueryFirstOrDefault<Address>(sql, new
			{
				address1 = address1.Trim(),
				address2 = address2.Trim(),
				city = city.Trim(),
				state = state.Trim(),
				zip = zip.Trim(),
				phone = phone.Trim()
			});

			return result;
		}

		/// <summary>
		/// Gets address matching input line 1, line 2, city, state zip, fax
		/// </summary>
		/// <param name="address1">Address line 1 to match</param>
		/// <param name="address2">Address line 2 to match</param>
		/// <param name="city">City to match</param>
		/// <param name="state">State to match</param>
		/// <param name="zip">Zip code to match</param>
		/// <param name="fax">Fax number to match</param>
		/// <returns></returns>
		public Address GetAddressNoPhone(string address1, string address2, string city, string state, string zip, string fax)
		{
			if (address1 == null)
				address1 = "";
			if (address2 == null)
				address2 = "";
			if (city == null)
				city = "";
			if (state == null)
				state = "";
			if (zip == null)
				zip = "";
			if (fax == null)
				fax = "";

			string sql = @"select address_k as DatabaseKey, entity_k as EntityKey, rtrim(addressline1) as Address1, rtrim(addressline2) as Address2, rtrim(city) as City, rtrim(county) as County,
				state as State, substring(rtrim(zipcode), 1, 5) as Zip, fax as Fax
				from visualcactus.addresses a
				where 1=1
				and a.addressline1 = @address1
				and a.addressline2 = @address2
				and a.city = @city
				and a.state = @state
				and a.zipcode = @zip
				and a.fax = @fax
				and a.entity_k in ('      para', '       all')
				and a.active = 1";

			Address result = Database.QueryFirstOrDefault<Address>(sql, new
			{
				address1 = address1.Trim(),
				address2 = address2.Trim(),
				city = city.Trim(),
				state = state.Trim(),
				zip = zip.Trim(),
				fax = fax.Trim()
			});

			return result;
		}

		/// <summary>
		/// Parses an address and attemts to format to a standard format.
		/// "1901 indian wood Circle Maumee oh 43537" -> "1901 Indian Wood Cir Maumee, OH 43537"
		/// </summary>
		/// <param name="input">Address to format</param>
		/// <returns></returns>
		public static Address ParseAddress(string input)
		{
			AddressParseResult parserResult = StaticUtilities.AddressManipulation.Parse(input);
			Address result = new Address();

			if (parserResult != null)
			{
				if (!string.IsNullOrEmpty(parserResult.Street))
					result.Address1 = StringManipulation.ToTitleCase(StringManipulation.StripMultipleWhitespace(parserResult.Number + " " + parserResult.Predirectional + " " + parserResult.Street + " " + parserResult.Suffix + " " + parserResult.Postdirectional));
				else
					result.Address1 = StringManipulation.ToTitleCase(StringManipulation.StripMultipleWhitespace(parserResult.StreetLine));
				result.Address2 = StringManipulation.ToTitleCase(StringManipulation.StripMultipleWhitespace(parserResult.SecondaryUnit + " " + parserResult.SecondaryNumber));
				result.City = StringManipulation.ToTitleCase(parserResult.City);
				result.State = parserResult.State;
				if (parserResult.Zip.Length > 5)
					result.ZipCode = parserResult.Zip.Substring(0, 5);
				else
					result.ZipCode = parserResult.Zip;
			}

			return result;
		}

		/// <summary>
		/// Gets next primary key for the addresses table.
		/// </summary>
		/// <returns></returns>
		public string GetNextPrimaryKey()
		{
			return Database.Utilities.GetNextPrimaryKey("AD", "ADDRESSES", "ADDRESS_K");
		}

		/// <summary>
		/// Gets a default physical address from cactus using rules established by credentialing
		/// </summary>
		/// <param name="npi">Provider npi to get address for</param>
		/// <param name="taxId">Provider or group tax id to get address for</param>
		/// <returns></returns>
		public Address GetDefaultPhysicalAddress(string npi, string taxId)
		{
			StaticUtilities.Extensions.ArgumentValidationExtensions.ThrowIfNullOrEmpty(npi, nameof(npi));
			StaticUtilities.Extensions.ArgumentValidationExtensions.ThrowIfNullOrEmpty(taxId, nameof(taxId));

			string sql = @"
				select address1, address2, city, state, zip, phone, fax, priority from(
					--provider using tax id on provider address
					select a.addressline1 as address1, a.addressline2 as address2, a.city, a.state, a.zipcode as zip, a.phone, a.fax,
					case when rt.description = 'Primary Address' then 1
						when rt.description = 'Alternate Address' then 2
						when rt.description = 'Primary Address - Do No Publish' then 3
						when rt.description = 'Alternate Address - Do Not Publish' then 4
						when rt.description = 'Prim-Part-time Less than 20 hours' then 5
						when rt.description = 'Alt-Part-time Less than 20 hours' then 6
						when rt.description = 'Prim Ind Provider ONLY-Hosp/Fac Based Address locations' then 7
						when rt.description = 'Alt Ind Provider ONLY-Hosp/Fac Based Address locations' then 8
						when rt.description = 'Baptist Hlth - Primary Address Do Not Publish' then 9
						when rt.description = 'Health 4 - Primary Address Do Not Publish' then 10
						when rt.description = 'Baptist Hlth - Alternate Address Do Not Publish' then 11
						when rt.description = 'Health 4 - Alternate Address Do Not Publish' then 12
						when rt.description = 'Primary Address use on Facility ONLY' then 12
						when rt.description = 'Facility ONLY - Primary Address use on Facility ONLY' then 13
						when rt.description = 'Facility ONLY - Alternate Address use on Facility ONLY' then 14
						when rt.description = 'Alternate Clinic Address Advantage ONLY' then 15
						when rt.description = 'Alternate Address use on Facility ONLY' then 16
					end as priority
					from visualcactus.providers p
					join visualcactus.provideraddresses pa on p.provider_k = pa.provider_k
					join visualcactus.addresses a on pa.address_k = a.address_k
					join visualcactus.reftable rt on pa.addresstype_rtk = rt.reftable_k
					where 1 = 1
					and p.npi = @npi
					and pa.taxidnumber = @taxId
					and pa.entity_k in('      para', '       all')
					and rt.description in('Primary Address', 'Alternate Address', 'Primary Address - Do No Publish', 'Alternate Address - Do Not Publish', 'Prim-Part-time Less than 20 hours', 'Alt-Part-time Less than 20 hours', 'Prim Ind Provider ONLY-Hosp/Fac Based Address locations', 'Alt Ind Provider ONLY-Hosp/Fac Based Address locations', 'Baptist Hlth - Primary Address Do Not Publish', 'Health 4 - Primary Address Do Not Publish', 'Baptist Hlth - Alternate Address Do Not Publish', 'Health 4 - Alternate Address Do Not Publish', 'Primary Address use on Facility ONLY', 'Facility ONLY - Primary Address use on Facility ONLY', 'Facility ONLY - Alternate Address use on Facility ONLY', 'Alternate Clinic Address Advantage ONLY', 'Alternate Address use on Facility ONLY')
					and a.active = 1
				union
					--provider using tax id on address
					select a.addressline1 as address1, a.addressline2 as address2, a.city, a.state, a.zipcode as zip, a.phone, a.fax,
					case when rt.description = 'Primary Address' then 1
						when rt.description = 'Alternate Address' then 2
						when rt.description = 'Primary Address - Do No Publish' then 3
						when rt.description = 'Alternate Address - Do Not Publish' then 4
						when rt.description = 'Prim-Part-time Less than 20 hours' then 5
						when rt.description = 'Alt-Part-time Less than 20 hours' then 6
						when rt.description = 'Prim Ind Provider ONLY-Hosp/Fac Based Address locations' then 7
						when rt.description = 'Alt Ind Provider ONLY-Hosp/Fac Based Address locations' then 8
						when rt.description = 'Baptist Hlth - Primary Address Do Not Publish' then 9
						when rt.description = 'Health 4 - Primary Address Do Not Publish' then 10
						when rt.description = 'Baptist Hlth - Alternate Address Do Not Publish' then 11
						when rt.description = 'Health 4 - Alternate Address Do Not Publish' then 12
						when rt.description = 'Primary Address use on Facility ONLY' then 12
						when rt.description = 'Facility ONLY - Primary Address use on Facility ONLY' then 13
						when rt.description = 'Facility ONLY - Alternate Address use on Facility ONLY' then 14
						when rt.description = 'Alternate Clinic Address Advantage ONLY' then 15
						when rt.description = 'Alternate Address use on Facility ONLY' then 16
					end as priority
					from visualcactus.providers p
					join visualcactus.provideraddresses pa on p.provider_k = pa.provider_k
					join visualcactus.addresses a on pa.address_k = a.address_k
					join visualcactus.reftable rt on pa.addresstype_rtk = rt.reftable_k
					where 1=1
					and p.npi = @npi
					and a.taxid = @taxId
					and pa.entity_k in('      para','       all')
					and rt.description in('Primary Address', 'Alternate Address', 'Primary Address - Do No Publish', 'Alternate Address - Do Not Publish', 'Prim-Part-time Less than 20 hours', 'Alt-Part-time Less than 20 hours', 'Prim Ind Provider ONLY-Hosp/Fac Based Address locations', 'Alt Ind Provider ONLY-Hosp/Fac Based Address locations', 'Baptist Hlth - Primary Address Do Not Publish', 'Health 4 - Primary Address Do Not Publish', 'Baptist Hlth - Alternate Address Do Not Publish', 'Health 4 - Alternate Address Do Not Publish', 'Primary Address use on Facility ONLY', 'Facility ONLY - Primary Address use on Facility ONLY', 'Facility ONLY - Alternate Address use on Facility ONLY', 'Alternate Clinic Address Advantage ONLY', 'Alternate Address use on Facility ONLY')
					and a.active = 1
				union
					--group using tax id on group
					select a.addressline1 as address1, a.addressline2 as address2, a.city, a.state, a.zipcode as zip, a.phone, a.fax,
					case when rt.description = 'Primary Address' then 1
						when rt.description = 'Alternate Address' then 2
						when rt.description = 'Primary Address - Do No Publish' then 3
						when rt.description = 'Alternate Address - Do Not Publish' then 4
						when rt.description = 'Prim-Part-time Less than 20 hours' then 5
						when rt.description = 'Alt-Part-time Less than 20 hours' then 6
						when rt.description = 'Prim Ind Provider ONLY-Hosp/Fac Based Address locations' then 7
						when rt.description = 'Alt Ind Provider ONLY-Hosp/Fac Based Address locations' then 8
						when rt.description = 'Baptist Hlth - Primary Address Do Not Publish' then 9
						when rt.description = 'Health 4 - Primary Address Do Not Publish' then 10
						when rt.description = 'Baptist Hlth - Alternate Address Do Not Publish' then 11
						when rt.description = 'Health 4 - Alternate Address Do Not Publish' then 12
						when rt.description = 'Primary Address use on Facility ONLY' then 12
						when rt.description = 'Facility ONLY - Primary Address use on Facility ONLY' then 13
						when rt.description = 'Facility ONLY - Alternate Address use on Facility ONLY' then 14
						when rt.description = 'Alternate Clinic Address Advantage ONLY' then 15
						when rt.description = 'Alternate Address use on Facility ONLY' then 16
					end as priority
					from visualcactus.providers p
					join visualcactus.provideraddresses pa on p.provider_k = pa.provider_k
					join visualcactus.groupsprovidersaddresses gpa on pa.provideraddress_k = gpa.provideraddress_k
					join visualcactus.groupaddress ga on gpa.groupaddress_k = ga.groupaddress_k
					join visualcactus.groups g on ga.group_k = g.group_k
					join visualcactus.addresses a on pa.address_k = a.address_k
					join visualcactus.reftable rt on pa.addresstype_rtk = rt.reftable_k
					where 1 = 1
					and p.npi = @npi
					and g.taxid = @taxId
					and pa.entity_k in('      para','       all')
					and rt.description in('Primary Address', 'Alternate Address', 'Primary Address - Do No Publish', 'Alternate Address - Do Not Publish', 'Prim-Part-time Less than 20 hours', 'Alt-Part-time Less than 20 hours', 'Prim Ind Provider ONLY-Hosp/Fac Based Address locations', 'Alt Ind Provider ONLY-Hosp/Fac Based Address locations', 'Baptist Hlth - Primary Address Do Not Publish', 'Health 4 - Primary Address Do Not Publish', 'Baptist Hlth - Alternate Address Do Not Publish', 'Health 4 - Alternate Address Do Not Publish', 'Primary Address use on Facility ONLY', 'Facility ONLY - Primary Address use on Facility ONLY', 'Facility ONLY - Alternate Address use on Facility ONLY', 'Alternate Clinic Address Advantage ONLY', 'Alternate Address use on Facility ONLY')
					and a.active = 1
				union
					--group using tax id on provider address via entity assignment
					select a.addressline1 as address1, a.addressline2 as address2, a.city, a.state, a.zipcode as zip, a.phone, a.fax,
					case when rt.description = 'Primary Address' then 1
						when rt.description = 'Alternate Address' then 2
						when rt.description = 'Primary Address - Do No Publish' then 3
						when rt.description = 'Alternate Address - Do Not Publish' then 4
						when rt.description = 'Prim-Part-time Less than 20 hours' then 5
						when rt.description = 'Alt-Part-time Less than 20 hours' then 6
						when rt.description = 'Prim Ind Provider ONLY-Hosp/Fac Based Address locations' then 7
						when rt.description = 'Alt Ind Provider ONLY-Hosp/Fac Based Address locations' then 8
						when rt.description = 'Baptist Hlth - Primary Address Do Not Publish' then 9
						when rt.description = 'Health 4 - Primary Address Do Not Publish' then 10
						when rt.description = 'Baptist Hlth - Alternate Address Do Not Publish' then 11
						when rt.description = 'Health 4 - Alternate Address Do Not Publish' then 12
						when rt.description = 'Primary Address use on Facility ONLY' then 12
						when rt.description = 'Facility ONLY - Primary Address use on Facility ONLY' then 13
						when rt.description = 'Facility ONLY - Alternate Address use on Facility ONLY' then 14
						when rt.description = 'Alternate Clinic Address Advantage ONLY' then 15
						when rt.description = 'Alternate Address use on Facility ONLY' then 16
					end as priority
					from visualcactus.providers p
					join visualcactus.entityassignments ea on p.provider_k = ea.provider_k
					join visualcactus.eaaddresses eaa on ea.ea_k = eaa.ea_k
					join visualcactus.provideraddresses pa on eaa.provideraddress_k = pa.provideraddress_k
					join visualcactus.addresses a on pa.address_k = a.address_k
					join visualcactus.reftable rt on pa.addresstype_rtk = rt.reftable_k
					where 1=1
					and p.npi = @npi
					and pa.taxidnumber = @taxId
					and ea.entity_k in('      para','       all')
					and rt.description in('Primary Address', 'Alternate Address', 'Primary Address - Do No Publish', 'Alternate Address - Do Not Publish', 'Prim-Part-time Less than 20 hours', 'Alt-Part-time Less than 20 hours', 'Prim Ind Provider ONLY-Hosp/Fac Based Address locations', 'Alt Ind Provider ONLY-Hosp/Fac Based Address locations', 'Baptist Hlth - Primary Address Do Not Publish', 'Health 4 - Primary Address Do Not Publish', 'Baptist Hlth - Alternate Address Do Not Publish', 'Health 4 - Alternate Address Do Not Publish', 'Primary Address use on Facility ONLY', 'Facility ONLY - Primary Address use on Facility ONLY', 'Facility ONLY - Alternate Address use on Facility ONLY', 'Alternate Clinic Address Advantage ONLY', 'Alternate Address use on Facility ONLY')
					and a.active = 1
					and ea.recordtype='a'
				) x
				where x.priority is not null
				order by priority";

			Address result = Database.QueryFirstOrDefault<Address>(sql, new
			{
				npi = npi,
				taxId = taxId
			});

			return result;
		}

		#region utility functions
		/// <summary>
		/// Translates zip code to county name. 43551 -> Wood
		/// </summary>
		/// <param name="zipCode">5 digit zip code to translate</param>
		/// <returns></returns>
		public string GetCountyFromZipCode(string zipCode)
		{
			if (string.IsNullOrEmpty(zipCode))
				zipCode = "";

			string sql = "select county from visualcactus.zipcodes where zipcode = @zip";
			return Database.ExecuteScalar<string>(sql, new { zip = zipCode.Substring(0, 5) });
		}

		#endregion

	}
}


using DataLayer.Base;
using DataLayer.Credentialing.Enumerations;
using DataLayer.Credentialing.Models;
using DataLayer.StaticUtilities.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataLayer.Credentialing
{
    public class ProviderRepository : BaseRepository<CredentialingDatabase>
    {
        const bool DEFAULT_ERROR_ON_MULTIPLE = true;

        internal ProviderRepository(CredentialingDatabase database) : base(database)
        {

        }

        ///////////////////////////////////////////////////////////////////////////////////////////

        #region Public Provider Methods

        #region Get Provider(s) By ID
        /// <summary>
        /// Get a single provider from Cactus by ID
        /// </summary>
        /// <param name="id">Provider ID</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <param name="errorOnMultiple">Whether the method should generate an error when multiple records are returned</param>
        /// <returns>Single provider record</returns>
        public Provider GetProviderByID(string id, IEnumerable<ProviderComponent> includedComponents = null, bool errorOnMultiple = DEFAULT_ERROR_ON_MULTIPLE)
        {
            return GetProvider(
                new Dictionary<string, object>()
                {
                    { "id", id }
                },
                includedComponents,
                errorOnMultiple
                );
        }

        /// <summary>
        /// Get a collection of providers from Cactus using a list of IDs
        /// </summary>
        /// <param name="ids">Collection of provider IDs</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Collection of provider records</returns>
        public IEnumerable<Provider> GetProvidersByIDs(IEnumerable<string> ids, IEnumerable<ProviderComponent> includedComponents = null)
        {
            return GetProviders(
                new Dictionary<string, object>()
                {
                    { "ids", ids }
                },
                includedComponents
                );
        }

        ///// <summary>
        ///// Get a data reader for providers from Cactus using a list of IDs
        ///// </summary>
        ///// <param name="ids">Collection of provider IDs</param>
        ///// <param name="includedComponents">List of components to include in the result</param>
        ///// <returns>Data reader for provider records</returns>
        public ProviderDataReader GetProviderDataReaderByIDs(IEnumerable<string> ids, IEnumerable<ProviderComponent> includedComponents = null)
        {
            return GetProviderDataReader(
                new Dictionary<string, object>()
                {
                    { "ids", ids }
                },
                includedComponents
                );
        }
        #endregion

        #region Get Provider(s) By Name
        /// <summary>
        /// Get a single provider from Cactus by facility name
        /// </summary>
        /// <param name="facilityName">Provider facility name</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <param name="errorOnMultiple">Whether the method should generate an error when multiple records are returned</param>
        /// <returns>Single provider record</returns>
        public Provider GetProviderByName(string facilityName, IEnumerable<ProviderComponent> includedComponents = null, bool errorOnMultiple = DEFAULT_ERROR_ON_MULTIPLE)
        {
            return GetProvider(
                new Dictionary<string, object>()
                {
                    { "facilityName", facilityName }
                },
                includedComponents,
                errorOnMultiple
                );
        }

        /// <summary>
        /// Get a single provider from Cactus by first name and last name
        /// </summary>
        /// <param name="firstName">Provider first name</param>
        /// <param name="lastName">Provider last name</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <param name="errorOnMultiple">Whether the method should generate an error when multiple records are returned</param>
        /// <returns>Single provider record</returns>
        public Provider GetProviderByName(string firstName, string lastName, IEnumerable<ProviderComponent> includedComponents = null, bool errorOnMultiple = DEFAULT_ERROR_ON_MULTIPLE)
        {
            return GetProvider(
                new Dictionary<string, object>()
                {
                    { "firstName", firstName },
                    { "lastName", lastName }
                },
                includedComponents,
                errorOnMultiple
                );
        }

        /// <summary>
        /// Get a collection of providers from Cactus by facility name
        /// </summary>
        /// <param name="facilityName">Provider facility name</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Collection of provider records</returns>
        public IEnumerable<Provider> GetProvidersByName(string facilityName, IEnumerable<ProviderComponent> includedComponents = null)
        {
            return GetProviders(
                new Dictionary<string, object>()
                {
                    { "facilityName", facilityName }
                },
                includedComponents
                );
        }

        /// <summary>
        /// Get a collection of providers from Cactus by first name and last name
        /// </summary>
        /// <param name="firstName">Provider first name</param>
        /// <param name="lastName">Provider last name</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Collection of provider records</returns>
        public IEnumerable<Provider> GetProvidersByName(string firstName, string lastName, IEnumerable<ProviderComponent> includedComponents = null)
        {
            return GetProviders(
                new Dictionary<string, object>()
                {
                    { "firstName", firstName },
                    { "lastName", lastName }
                },
                includedComponents
                );
        }
        #endregion

        #region Get Provider(s) By Name Like
        /// <summary>
        /// Get a single provider from Cactus by facility name using like comparison
        /// </summary>
        /// <param name="facilityName">Provider facility name</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <param name="errorOnMultiple">Whether the method should generate an error when multiple records are returned</param>
        /// <returns>Single provider record</returns>
        public Provider GetProviderByNameLike(string facilityName, IEnumerable<ProviderComponent> includedComponents = null, bool errorOnMultiple = DEFAULT_ERROR_ON_MULTIPLE)
        {
            return GetProvider(
                new Dictionary<string, object>()
                {
                    { "facilityNameLike", facilityName }
                },
                includedComponents,
                errorOnMultiple
                );
        }

        /// <summary>
        /// Get a single provider from Cactus by first name and last name using like comparison
        /// </summary>
        /// <param name="firstName">Provider first name</param>
        /// <param name="lastName">Provider last name</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <param name="errorOnMultiple">Whether the method should generate an error when multiple records are returned</param>
        /// <returns>Single provider record</returns>
        public Provider GetProviderByNameLike(string firstName, string lastName, IEnumerable<ProviderComponent> includedComponents = null, bool errorOnMultiple = DEFAULT_ERROR_ON_MULTIPLE)
        {
            return GetProvider(
                new Dictionary<string, object>()
                {
                    { "firstNameLike", firstName },
                    { "lastNameLike", lastName }
                },
                includedComponents,
                errorOnMultiple
                );
        }

        /// <summary>
        /// Get a collection of providers from Cactus by facility name using like comparison
        /// </summary>
        /// <param name="facilityName">Provider facility name</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Collection of provider records</returns>
        public IEnumerable<Provider> GetProvidersByNameLike(string facilityName, IEnumerable<ProviderComponent> includedComponents = null)
        {
            return GetProviders(
                new Dictionary<string, object>()
                {
                    { "facilityNameLike", facilityName }
                },
                includedComponents
                );
        }

        /// <summary>
        /// Get a collection of providers from Cactus by first name and last name using like comparison
        /// </summary>
        /// <param name="firstName">Provider first name</param>
        /// <param name="lastName">Provider last name</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Collection of provider records</returns>
        public IEnumerable<Provider> GetProvidersByNameLike(string firstName, string lastName, IEnumerable<ProviderComponent> includedComponents = null)
        {
            return GetProviders(
                new Dictionary<string, object>()
                {
                    { "firstNameLike", firstName },
                    { "lastNameLike", lastName }
                },
                includedComponents
                );
        }
        #endregion

        #region Get Provider(s) By NPI
        /// <summary>
        /// Get a single provider from Cactus by NPI
        /// </summary>
        /// <param name="npi">Provider NPI</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <param name="errorOnMultiple">Whether the method should generate an error when multiple records are returned</param>
        /// <returns>Single provider record</returns>
        public Provider GetProviderByNPI(string npi, IEnumerable<ProviderComponent> includedComponents = null, bool errorOnMultiple = DEFAULT_ERROR_ON_MULTIPLE)
        {
            return GetProvider(
                new Dictionary<string, object>()
                {
                    { "npi", npi }
                },
                includedComponents,
                errorOnMultiple
                );
        }

        /// <summary>
        /// Get a collection of providers from Cactus using a list of NPIs
        /// </summary>
        /// <param name="npis">Collection of provider NPIs</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Collection of provider records</returns>
        public IEnumerable<Provider> GetProvidersByNPIs(IEnumerable<string> npis, IEnumerable<ProviderComponent> includedComponents = null)
        {
            return GetProviders(
                new Dictionary<string, object>()
                {
                    { "npis", npis }
                },
                includedComponents
                );
        }

        /// <summary>
        /// Get a data reader for providers from Cactus using a list of NPIs
        /// </summary>
        /// <param name="npis">Collection of provider NPIs</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Data reader for provider records</returns>
        public ProviderDataReader GetProviderDataReaderByNPIs(IEnumerable<string> npis, IEnumerable<ProviderComponent> includedComponents = null)
        {
            return GetProviderDataReader(
                new Dictionary<string, object>()
                {
                    { "npis", npis }
                },
                includedComponents
                );
        }
        #endregion

        #region Get Providers Unfiltered
        /// <summary>
        /// Gets a collection of providers from Cactus, with no filtering applied
        /// </summary>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Collection of provider records</returns>
        public IEnumerable<Provider> GetProvidersUnfiltered(IEnumerable<ProviderComponent> includedComponents = null)
        {
            return GetProviders(null, includedComponents);
        }

        /// <summary>
        /// Gets a data reader for providers from Cactus, with no filtering applied
        /// </summary>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Data reader for provider records</returns>
        public ProviderDataReader GetProviderDataReaderUnfiltered(IEnumerable<ProviderComponent> includedComponents = null)
        {
            return GetProviderDataReader(null, includedComponents);
        }
        #endregion

        #endregion

        ///////////////////////////////////////////////////////////////////////////////////////////

        #region Public Provider Address Methods


        #region Get Addresses by Provider ID
        /// <summary>
        /// Get a collection of provider addresses from Cactus by a provider ID
        /// </summary>
        /// <param name="id">Provider ID</param>
        /// <returns>Collection of provider address records</returns>
        public IEnumerable<Address> GetProviderAddressesByID(string id)
        {
            return GetProviderAddresses(
                new Dictionary<string, object>()
                {
                    { "id", id }
                });
        }

        /// <summary>
        /// Get a collection of provider addresses from Cactus using a list of IDs
        /// </summary>
        /// <param name="ids">Collection of provider IDs</param>
        /// <returns>Collection of provider address records</returns>
        public IEnumerable<Address> GetProviderAddressesByID(IEnumerable<string> ids)
        {
            return GetProviderAddresses(
                new Dictionary<string, object>()
                {
                    { "ids", ids }
                });
        }
        #endregion

        #region Get Addresses by Provider NPI
        /// <summary>
        /// Get a collection of provider addresses from Cactus by a provider NPI
        /// </summary>
        /// <param name="npi">Provider NPI</param>
        /// <returns>Collection of provider address records</returns>
        public IEnumerable<Address> GetProviderAddressesByNPI(string npi)
        {
            return GetProviderAddresses(
                new Dictionary<string, object>()
                {
                    { "npi", npi }
                });
        }
        #endregion

        #region Get Provider Addresses By Group Database Key
        /// <summary>
        /// Get a collection of provider addresses from Cactus by group database key
        /// </summary>
        /// <param name="grpDatabaseKey">Provider group database key</param>
        /// <returns>Collection of provider address records</returns>
        //public IEnumerable<Address> GetProviderAddressesByGroupDatabaseKey(string grpDatabaseKey)
        //{
        //    return GetProviderAddresses(
        //        new Dictionary<string, object>()
        //        {
        //            { "grpDatabaseKey", grpDatabaseKey }
        //        });
        //}

        /// <summary>
        /// Get a collection of provider addresses from Cactus using a list of group database keys
        /// </summary>
        /// <param name="grpDatabaseKeys">Provider group database keys</param>
        /// <returns>Collection of provider address records</returns>
        //public IEnumerable<Address> GetProviderAddressesByGroupDatabaseKeys(IEnumerable<string> grpDatabaseKeys)
        //{
        //    return GetProviderGroupAddresses(
        //        new Dictionary<string, object>()
        //        {
        //            { "grpDatabaseKeys", grpDatabaseKeys }
        //        });
        //}
        #endregion


        #endregion

        ///////////////////////////////////////////////////////////////////////////////////////////

        #region Public Provider Board Methods


        #region Get Boards by Provider ID
        /// <summary>
        /// Get a collection of provider boards from Cactus by a provider ID
        /// </summary>
        /// <param name="id">Provider ID</param>
        /// <returns>Collection of provider board records</returns>
        public IEnumerable<ProviderBoard> GetProviderBoardsByID(string id)
        {
            return GetProviderBoards(
                new Dictionary<string, object>()
                {
                    { "id", id }
                });
        }

        /// <summary>
        /// Get a collection of provider boards from Cactus using a list of IDs
        /// </summary>
        /// <param name="ids">Collection of provider IDs</param>
        /// <returns>Collection of provider board records</returns>
        public IEnumerable<ProviderBoard> GetProviderBoardsByID(IEnumerable<string> ids)
        {
            return GetProviderBoards(
                new Dictionary<string, object>()
                {
                    { "ids", ids }
                });
        }
        #endregion

        #region Get Boards by Provider NPI
        /// <summary>
        /// Get a collection of provider boards from Cactus by a provider NPI
        /// </summary>
        /// <param name="npi">Provider NPI</param>
        /// <returns>Collection of provider board records</returns>
        public IEnumerable<ProviderBoard> GetProviderBoardsByNPI(string npi)
        {
            return GetProviderBoards(
                new Dictionary<string, object>()
                {
                    { "npi", npi }
                });
        }
        #endregion


        #endregion

        ///////////////////////////////////////////////////////////////////////////////////////////

        #region Public Provider Education Methods


        #region Get Education by Provider ID
        /// <summary>
        /// Get a collection of provider education from Cactus by a provider ID
        /// </summary>
        /// <param name="id">Provider ID</param>
        /// <returns>Collection of provider education records</returns>
        public IEnumerable<ProviderEducation> GetProviderEducationByID(string id)
        {
            return GetProviderEducation(
                new Dictionary<string, object>()
                {
                    { "id", id }
                });
        }

        /// <summary>
        /// Get a collection of provider education from Cactus using a list of IDs
        /// </summary>
        /// <param name="ids">Collection of provider IDs</param>
        /// <returns>Collection of provider education records</returns>
        public IEnumerable<ProviderEducation> GetProviderEducationByID(IEnumerable<string> ids)
        {
            return GetProviderEducation(
                new Dictionary<string, object>()
                {
                    { "ids", ids }
                });
        }
        #endregion

        #region Get Education by Provider NPI
        /// <summary>
        /// Get a collection of provider education from Cactus by a provider NPI
        /// </summary>
        /// <param name="npi">Provider NPI</param>
        /// <returns>Collection of provider education records</returns>
        public IEnumerable<ProviderEducation> GetProviderEducationByNPI(string npi)
        {
            return GetProviderEducation(
                new Dictionary<string, object>()
                {
                    { "npi", npi }
                });
        }
        #endregion


        #endregion

        ///////////////////////////////////////////////////////////////////////////////////////////

        #region Public Provider Entity Assignment Methods


        #region Get Entity Assignments by Provider ID
        /// <summary>
        /// Get a collection of provider entity assignments from Cactus by a provider ID
        /// </summary>
        /// <param name="id">Provider ID</param>
        /// <returns>Collection of provider entity assignment records</returns>
        public IEnumerable<ProviderEntityAssignment> GetProviderEntityAssignmentsByID(string id)
        {
            return GetProviderEntityAssignments(
                new Dictionary<string, object>()
                {
                    { "id", id }
                });
        }

        /// <summary>
        /// Get a collection of provider entity assignments from Cactus using a list of IDs
        /// </summary>
        /// <param name="ids">Collection of provider IDs</param>
        /// <returns>Collection of provider entity assignment records</returns>
        public IEnumerable<ProviderEntityAssignment> GetProviderEntityAssignmentsByID(IEnumerable<string> ids)
        {
            return GetProviderEntityAssignments(
                new Dictionary<string, object>()
                {
                    { "ids", ids }
                });
        }
        #endregion

        #region Get Entity Assignments by Provider NPI
        /// <summary>
        /// Get a collection of provider entity assignments from Cactus by a provider NPI
        /// </summary>
        /// <param name="npi">Provider NPI</param>
        /// <returns>Collection of provider entity assignment records</returns>
        public IEnumerable<ProviderEntityAssignment> GetProviderEntityAssignmentsByNPI(string npi)
        {
            return GetProviderEntityAssignments(
                new Dictionary<string, object>()
                {
                    { "npi", npi }
                });
        }
        #endregion


        #endregion

        ///////////////////////////////////////////////////////////////////////////////////////////

        #region Public Provider Language Methods


        #region Get Languages by Provider ID
        /// <summary>
        /// Get a collection of provider languages from Cactus by a provider ID
        /// </summary>
        /// <param name="id">Provider ID</param>
        /// <returns>Collection of provider language records</returns>
        public IEnumerable<ProviderLanguage> GetProviderLanguagesByID(string id)
        {
            return GetProviderLanguages(
                new Dictionary<string, object>()
                {
                    { "id", id }
                });
        }

        /// <summary>
        /// Get a collection of provider languages from Cactus using a list of IDs
        /// </summary>
        /// <param name="ids">Collection of provider IDs</param>
        /// <returns>Collection of provider language records</returns>
        public IEnumerable<ProviderLanguage> GetProviderLanguagesByID(IEnumerable<string> ids)
        {
            return GetProviderLanguages(
                new Dictionary<string, object>()
                {
                    { "ids", ids }
                });
        }
        #endregion

        #region Get Language by Provider NPI
        /// <summary>
        /// Get a collection of provider languages from Cactus by a provider NPI
        /// </summary>
        /// <param name="npi">Provider NPI</param>
        /// <returns>Collection of provider language records</returns>
        public IEnumerable<ProviderLanguage> GetProviderLanguagesByNPI(string npi)
        {
            return GetProviderLanguages(
                new Dictionary<string, object>()
                {
                    { "npi", npi }
                });
        }
        #endregion


        #endregion

        ///////////////////////////////////////////////////////////////////////////////////////////

        #region Public Provider License Methods


        #region Get Licenses by Provider ID
        /// <summary>
        /// Get a collection of provider licenses from Cactus by a provider ID
        /// </summary>
        /// <param name="id">Provider ID</param>
        /// <returns>Collection of provider license records</returns>
        public IEnumerable<ProviderLicense> GetProviderLicensesByID(string id)
        {
            return GetProviderLicenses(
                new Dictionary<string, object>()
                {
                    { "id", id }
                });
        }

        /// <summary>
        /// Get a collection of provider licenses from Cactus using a list of IDs
        /// </summary>
        /// <param name="ids">Collection of provider IDs</param>
        /// <returns>Collection of provider license records</returns>
        public IEnumerable<ProviderLicense> GetProviderLicensesByID(IEnumerable<string> ids)
        {
            return GetProviderLicenses(
                new Dictionary<string, object>()
                {
                    { "ids", ids }
                });
        }
        #endregion

        #region Get Licenses by Provider NPI
        /// <summary>
        /// Get a collection of provider licenses from Cactus by a provider NPI
        /// </summary>
        /// <param name="npi">Provider NPI</param>
        /// <returns>Collection of provider license records</returns>
        public IEnumerable<ProviderLicense> GetProviderLicensesByNPI(string npi)
        {
            return GetProviderLicenses(
                new Dictionary<string, object>()
                {
                    { "npi", npi }
                });
        }
        #endregion


        #endregion

        ///////////////////////////////////////////////////////////////////////////////////////////

        #region Public Provider Specialty Methods


        #region Get Specialties by Provider ID
        /// <summary>
        /// Get a collection of provider specialties from Cactus by a provider ID
        /// </summary>
        /// <param name="id">Provider ID</param>
        /// <returns>Collection of provider specialty records</returns>
        public IEnumerable<ProviderSpecialty> GetProviderSpecialtiesByID(string id)
        {
            return GetProviderSpecialties(
                new Dictionary<string, object>()
                {
                    { "id", id }
                });
        }

        /// <summary>
        /// Get a collection of provider specialties from Cactus using a list of IDs
        /// </summary>
        /// <param name="ids">Collection of provider IDs</param>
        /// <returns>Collection of provider specialty records</returns>
        public IEnumerable<ProviderSpecialty> GetProviderSpecialtiesByID(IEnumerable<string> ids)
        {
            return GetProviderSpecialties(
                new Dictionary<string, object>()
                {
                    { "ids", ids }
                });
        }
        #endregion

        #region Get Addresses by Provider NPI
        /// <summary>
        /// Get a collection of provider specialties from Cactus by a provider NPI
        /// </summary>
        /// <param name="npi">Provider NPI</param>
        /// <returns>Collection of provider specialty records</returns>
        public IEnumerable<ProviderSpecialty> GetProviderSpecialtiesByNPI(string npi)
        {
            return GetProviderSpecialties(
                new Dictionary<string, object>()
                {
                    { "npi", npi }
                });
        }
        #endregion


        #endregion

        ///////////////////////////////////////////////////////////////////////////////////////////

        #region Internal Provider Component Methods

        /// <summary>
        /// Add components to a list of providers
        /// </summary>
        /// <param name="providers">List of providers to add components to</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        internal void AddComponentsToProviders(IEnumerable<Provider> providers, IEnumerable<ProviderComponent> includedComponents)
        {
            if (includedComponents != null)
            {
                IEnumerable<string> ids = providers.Select(p => p.ProviderNumber);
                foreach (ProviderComponent component in includedComponents)
                    switch (component)
                    {
                        case ProviderComponent.Addresses:
                            IEnumerable<Address> addresses = Database.Providers.GetProviderAddressesByID(ids);
                            foreach (Provider provider in providers)
                                provider.Addresses = addresses.Where(a => a.ForeignDatabaseKey == provider.DatabaseKey);
                            break;
                        case ProviderComponent.Boards:
                            IEnumerable<ProviderBoard> boards = Database.Providers.GetProviderBoardsByID(ids);
                            foreach (Provider provider in providers)
                                provider.Boards = boards.Where(b => b.ForeignDatabaseKey == provider.DatabaseKey);
                            break;
                        case ProviderComponent.Education:
                            IEnumerable<ProviderEducation> education = Database.Providers.GetProviderEducationByID(ids);
                            foreach (Provider provider in providers)
                                provider.Education = education.Where(e => e.ForeignDatabaseKey == provider.DatabaseKey);
                            break;
                        case ProviderComponent.EntityAssignments:
                            IEnumerable<ProviderEntityAssignment> entityAssignments = Database.Providers.GetProviderEntityAssignmentsByID(ids);
                            foreach (Provider provider in providers)
                                provider.EntityAssignments = entityAssignments.Where(e => e.ForeignDatabaseKey == provider.DatabaseKey);
                            break;
                        case ProviderComponent.Groups:
                            IEnumerable<Group> groups = Database.Groups.GetGroupsByProviderID(ids);
                            foreach (Provider provider in providers)
                                provider.Groups = groups.Where(g => g.ForeignDatabaseKey == provider.DatabaseKey);
                            break;
                        case ProviderComponent.Languages:
                            IEnumerable<ProviderLanguage> languages = Database.Providers.GetProviderLanguagesByID(ids);
                            foreach (Provider provider in providers)
                                provider.Languages = languages.Where(l => l.ForeignDatabaseKey == provider.DatabaseKey);
                            break;
                        case ProviderComponent.Licenses:
                            IEnumerable<ProviderLicense> licenses = Database.Providers.GetProviderLicensesByID(ids);
                            foreach (Provider provider in providers)
                                provider.Licenses = licenses.Where(l => l.ForeignDatabaseKey == provider.DatabaseKey);
                            break;
                        case ProviderComponent.Specialties:
                            IEnumerable<ProviderSpecialty> specialties = Database.Providers.GetProviderSpecialtiesByID(ids);
                            foreach (Provider provider in providers)
                                provider.Specialties = specialties.Where(s => s.ForeignDatabaseKey == provider.DatabaseKey);
                            break;
                        default:
                            throw new ArgumentException($"The included component is not valid: {component}", nameof(includedComponents));
                    }
            }
        }

        #endregion

        ///////////////////////////////////////////////////////////////////////////////////////////

        #region Private Methods
        #region Get Provider(s) - Base Methods
        /// <summary>
        /// Get a single provider from Cactus
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <param name="errorOnMultiple">Whether the method should generate an error when multiple records are returned</param>
        /// <returns>Single provider record</returns>
        private Provider GetProvider(Dictionary<string, object> criteria, IEnumerable<ProviderComponent> includedComponents, bool errorOnMultiple)
        {
            IEnumerable<Provider> providers = GetProviders(criteria, includedComponents);
            if (providers.Count() > 1 && errorOnMultiple)
                throw new Exception($"The query returned multiple results: {providers.Count()} providers found");
            else if (providers.Any())
                return null;
            else
                return providers.First();
        }

        /// <summary>
        /// Get a collection of providers from Cactus
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Collection of provider records</returns>
        private IEnumerable<Provider> GetProviders(Dictionary<string, object> criteria, IEnumerable<ProviderComponent> includedComponents)
        {
            // Build provider query
            (string sql, object param, Dictionary<string, IEnumerable<object>> filterTables) = BuildProviderQuery(criteria);

            // Get base provider information
            IEnumerable<Provider> providers = Database.QueryWithFilters<Provider>(sql, param, filterTables);

            // Include other components
            AddComponentsToProviders(providers, includedComponents);

            return providers;
        }

        /// <summary>
        /// Get a data reader for providers from Cactus
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Provider data reader for EDW providers</returns>
        private ProviderDataReader GetProviderDataReader(Dictionary<string, object> criteria, IEnumerable<ProviderComponent> includedComponents)
        {
            // Build provider query
            (string sql, object param, Dictionary<string, IEnumerable<object>> filterTables) = BuildProviderQuery(criteria);

            // Get data reader
            return new ProviderDataReader(Database, sql, param, filterTables, includedComponents);
        }

        /// <summary>
        /// Generates the SQL and parameters necessary to get provider data from Cactus using the specified criteria
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <returns>
        /// 1 - SQL string to get provider base data from EDW
        /// 2 - param object for Dapper query parameter substitution
        /// 3 - list of tables to filter by with table contents
        /// </returns>
        private (string sql, object param, Dictionary<string, IEnumerable<object>> filterTables) BuildProviderQuery(Dictionary<string, object> criteria)
        {
            StringBuilder sql = new StringBuilder();
            Dictionary<string, object> param = new Dictionary<string, object>();
            Dictionary<string, IEnumerable<object>> filterTables = new Dictionary<string, IEnumerable<object>>();

            // Build sql to get base provider information
            sql.AppendLine($@"
                select * from (

                select p.provider_k as DatabaseKey, p.id as ProviderNumber, rtrim(p.firstname) as firstname, rtrim(p.middlename) as middlename, rtrim(p.lastname) as lastnamne,
                rtrim(p.suffix) as suffix, p.dateofbirth as BirthDate, rtrim(p.longname) as institutionname, p.ssn, p.upin, p.npi,
                case when p.individualinstitutionflag = 1 then 'true'
                    else 'false'
                    end as isindividual,
                row_number() over (order by provider_k asc) as rowNum
                from visualcactus.providers p
                where 1=1

                ) prov
            ");

            #region Add limiting criteria

            #region Add join statements
            if (criteria != null)
            {
                string tableName;
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "ids":
                            tableName = Database.GenerateTempTableName($"providers_{key}");
                            filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                            sql.AppendLine($"  join {tableName} tmp_{key} on rtrim(prov.[databasekey]) = tmp_{key}.[value]");
                            break;
                        case "npis":
                            tableName = Database.GenerateTempTableName($"providers_{key}");
                            filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                            sql.AppendLine($"  join {tableName} tmp_{key} on rtrim(prov.[npi]) = tmp_{key}.[value]");
                            break;
                    }
                }
            }
            #endregion

            #region Add row number limiter
            sql.AppendLine(" where prov.rowNum between @minRowNum and @maxRowNum");

            // Add min and max row numbers values (will be overwritten if this isdestined for a data reader)
            param.Add("minRowNum", 0);
            param.Add("maxRowNum", int.MaxValue);
            #endregion

            #region Add where clauses
            if (criteria != null)
            {
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "id":
                            param.Add(key, criteria[key]);
                            sql.AppendLine("   and rtrim(prov.providernumber) = @id");
                            break;
                        case "facilityName":
                            param.Add(key, criteria[key]);
                            sql.AppendLine("   and rtrim(prov.institutionname) = @facilityName");
                            break;
                        case "firstName":
                            if (!string.IsNullOrWhiteSpace((string)criteria[key]))
                            {
                                param.Add(key, criteria[key]);
                                sql.AppendLine("   and rtrim(prov.firstname) = @firstName");
                            }
                            break;
                        case "lastName":
                            if (!string.IsNullOrWhiteSpace((string)criteria[key]))
                            {
                                param.Add(key, criteria[key]);
                                sql.AppendLine("   and rtrim(prov.lastname) = @lastName");
                            }
                            break;
                        case "facilityNameLike":
                            param.Add(key, criteria[key]);
                            sql.AppendLine("   and rtrim(prov.institutionname) like @facilityNameLike");
                            break;
                        case "firstNameLike":
                            if (!string.IsNullOrWhiteSpace((string)criteria[key]))
                            {
                                param.Add(key, criteria[key]);
                                sql.AppendLine("   and rtrim(prov.firstname) like @firstNameLike");
                            }
                            break;
                        case "lastNameLike":
                            if (!string.IsNullOrWhiteSpace((string)criteria[key]))
                            {
                                param.Add(key, criteria[key]);
                                sql.AppendLine("   and rtrim(prov.lastname) like @lastNameLike");
                            }
                            break;
                        case "npi":
                            param.Add(key, criteria[key]);
                            sql.AppendLine("   and rtrim(prov.npi) = @npi");
                            break;
                    }
                }
            }
            #endregion

            #endregion

            /// 1 - SQL string to get provider base data from EDW
            /// 2 - param object for Dapper query parameter substitution
            /// 3 - list of tables to filter by with table contents
            return (sql.ToString(), param.ToQueryParamObject(), filterTables);
        }
        #endregion

        #region Get Provider Addresses - Base Method
        /// <summary>
        /// Get a collection of provider addresses from Cactus
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <returns>Collection of provider address records</returns>
        private IEnumerable<Address> GetProviderAddresses(Dictionary<string, object> criteria)
        {
            StringBuilder sql = new StringBuilder();
            Dictionary<string, object> param = new Dictionary<string, object>();
            Dictionary<string, IEnumerable<object>> filterTables = new Dictionary<string, IEnumerable<object>>();

            if (criteria == null)
                criteria = new Dictionary<string, object>();

            // Build sql to get provider addresses
            sql.AppendLine(@"select pa.provideraddress_k as DatabaseKey, pa.provider_k as ForeignDatabaseKey, rtrim(rt.description) as type,
				rtrim(a.addressline1) as address1, rtrim(a.addressline2) as address2, rtrim(a.city) as city, rtrim(a.state) as state, a.zipcode, a.phone, a.fax, a.taxid,
                a.latitude, a.longitude
				from visualcactus.provideraddresses pa
                join visualcactus.providers p on pa.provider_k = p.provider_k
				join visualcactus.reftable rt on pa.addresstype_rtk = rt.reftable_k
				join visualcactus.addresses a on pa.address_k=a.address_k
            ");

            #region Add limiting criteria

            #region Add join statements
            if (criteria != null)
            {
                string tableName;
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "ids":
                            tableName = Database.GenerateTempTableName($"provaddresses_{key}");
                            filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                            sql.AppendLine($"  join {tableName} tmp_{key} on rtrim(p.id) = tmp_{key}.[value]");
                            break;
                    }
                }
            }
            #endregion

            sql.AppendLine(@"
				where 1 = 1
				and pa.entity_k in ('      para', '       all')
				and pa.active = 1");

            #region Add where clauses
            if (criteria != null)
            {
                foreach (string key in criteria.Keys)
                    switch (key)
                    {
                        case "id":
                            param.Add(key, criteria[key]);
                            sql.AppendLine("   and rtrim(p.id) = @id");
                            break;
                        case "npi":
                            param.Add(key, criteria[key]);
                            sql.AppendLine("   and rtrim(p.npi) = @npi");
                            break;
                    }
            }
            #endregion

            #endregion

            // Get provider addresses
            return Database.QueryWithFilters<Address>(sql.ToString(), param.ToQueryParamObject(), filterTables);
        }

        #endregion

        #region Get Provider Board - Base Method
        /// <summary>
        /// Get a collection of provider boards from Cactus
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <returns>Collection of provider board records</returns>
        private IEnumerable<ProviderBoard> GetProviderBoards(Dictionary<string, object> criteria)
        {
            StringBuilder sql = new StringBuilder();
            Dictionary<string, object> param = new Dictionary<string, object>();
            Dictionary<string, IEnumerable<object>> filterTables = new Dictionary<string, IEnumerable<object>>();

            // Build sql to get provider boards
            sql.AppendLine(@"
                select pb.providerboard_k as DatabaseKey, pb.provider_k as ForeignDatabaseKey, rtrim(rt.description) as Description
				from visualcactus.providers p 
                join visualcactus.providerboards pb on p.provider_k = pb.provider_k
				join visualcactus.reftable rt on pb.board_rtk = rt.reftable_k");

            #region Add limiting criteria

            #region Add join statements
            if (criteria != null)
            {
                string tableName;
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "ids":
                            tableName = Database.GenerateTempTableName($"provboards_{key}");
                            filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                            sql.AppendLine($"  join {tableName} tmp_{key} on rtrim(p.id) = tmp_{key}.[value]");
                            break;
                    }
                }
            }
            #endregion

            sql.AppendLine(@"
    	        where 1 = 1
				and pb.entity_k in ('      para', '       all')");

            #region Add where clauses
            if (criteria != null)
            {
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "id":
                            param.Add(key, criteria[key]);
                            sql.AppendLine("   and rtrim(p.id) = @id");
                            break;
                        case "npi":
                            param.Add(key, criteria[key]);
                            sql.AppendLine("   and rtrim(p.npi) = @npi");
                            break;
                    }
                }
            }
            #endregion

            #endregion

            // Get provider boards
            return Database.QueryWithFilters<ProviderBoard>(sql.ToString(), param.ToQueryParamObject(), filterTables);
        }
        #endregion

        #region Get Provider Education - Base Method
        /// <summary>
        /// Get a collection of provider education from Cactus
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <returns>Collection of provider education records</returns>
        private IEnumerable<ProviderEducation> GetProviderEducation(Dictionary<string, object> criteria)
        {
            StringBuilder sql = new StringBuilder();
            Dictionary<string, object> param = new Dictionary<string, object>();
            Dictionary<string, IEnumerable<object>> filterTables = new Dictionary<string, IEnumerable<object>>();

            // Build sql to get provider education
            sql.AppendLine(@"
                select pe.providereducation_k as DatabaseKey, pe.provider_k as ForeignDatabaseKey, pe.degree_rtk as DegreeKey, rtrim(rt.description) as Description,
        		rtrim(rt2.description) as Specialty, rtrim(rt3.description) as Program, rtrim(i.name) as Institution,
                pe.startdate, pe.finishdate as enddate
        		from visualcactus.providers p
                join visualcactus.providereducation pe on p.provider_k = pe.provider_k
        		join visualcactus.reftable rt on pe.degree_rtk = rt.reftable_k
        		join visualcactus.reftable rt2 on pe.specialty_rtk = rt2.reftable_k
                join visualcactus.reftable rt3 on pe.program_rtk = rt3.reftable_k
        		join visualcactus.institutions i on pe.institution_k = i.institution_k");

            #region Add limiting criteria

            #region Add join statements
            if (criteria != null)
            {
                string tableName;
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "ids":
                            tableName = Database.GenerateTempTableName($"proveducation_{key}");
                            filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                            sql.AppendLine($"  join {tableName} tmp_{key} on rtrim(p.id) = tmp_{key}.[value]");
                            break;
                    }
                }
            }
            #endregion

            sql.AppendLine(@"
        		where 1 = 1
        		and pe.entity_k in ('      para', '       all')");

            #region Add where clauses
            if (criteria != null)
            {
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "id":
                            param.Add(key, criteria[key]);
                            sql.AppendLine("   and rtrim(p.id) = @id");
                            break;
                        case "npi":
                            param.Add(key, criteria[key]);
                            sql.AppendLine("   and rtrim(p.npi) = @npi");
                            break;
                    }
                }
            }
            #endregion

            #endregion

            // Get provider education
            return Database.QueryWithFilters<ProviderEducation>(sql.ToString(), param.ToQueryParamObject(), filterTables);
        }
        #endregion

        #region Get Provider Entity Assignment - Base Method
        /// <summary>
        /// Get a collection of provider entity assignments from Cactus
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <returns>Collection of provider entity assignment records</returns>
        private IEnumerable<ProviderEntityAssignment> GetProviderEntityAssignments(Dictionary<string, object> criteria)
        {
            StringBuilder sql = new StringBuilder();
            Dictionary<string, object> param = new Dictionary<string, object>();
            Dictionary<string, IEnumerable<object>> filterTables = new Dictionary<string, IEnumerable<object>>();

            // Build sql to get provider entity assignments
            sql.AppendLine(@"
                select ea.ea_k as DatabaseKey, ea.provider_k as ForeignDatabaseKey, rtrim(ea.recordtype) as RecordType, 
                rtrim(rt.description) as assignment, rtrim(rt2.description) as status, rtrim(rt3.description) as category,
                ea.originalappointmentdate as startdate, ea.termination_date as enddate
                from visualcactus.providers p
                join visualcactus.entityassignments ea on p.provider_k = ea.provider_k
                left join visualcactus.reftable rt on ea.assignment_rtk = rt.reftable_k
                left join visualcactus.reftable rt2 on ea.status_rtk = rt2.reftable_k
                left join visualcactus.reftable rt3 on ea.category_rtk = rt3.reftable_k
                    ");

            #region Add limiting criteria

            #region Add join statements
            if (criteria != null)
            {
                string tableName;
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "ids":
                            tableName = Database.GenerateTempTableName($"proveass_{key}");
                            filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                            sql.AppendLine($"  join {tableName} tmp_{key} on rtrim(p.id) = tmp_{key}.[value]");
                            break;
                    }
                }
            }
            #endregion

            sql.AppendLine(@"
                where 1 = 1
                and ea.entity_k = '      para'");

            #region Add where clauses
            if (criteria != null)
            {
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "id":
                            param.Add(key, criteria[key]);
                            sql.AppendLine("   and rtrim(p.id) = @id");
                            break;
                        case "npi":
                            param.Add(key, criteria[key]);
                            sql.AppendLine("   and rtrim(p.npi) = @npi");
                            break;
                    }
                }
            }
            #endregion

            #endregion

            // Get provider entity assignments
            return Database.QueryWithFilters<ProviderEntityAssignment>(sql.ToString(), param.ToQueryParamObject(), filterTables);
        }
        #endregion

        #region Get Provider Language - Base Method
        /// <summary>
        /// Get a collection of provider languages from Cactus
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <returns>Collection of provider language records</returns>
        private IEnumerable<ProviderLanguage> GetProviderLanguages(Dictionary<string, object> criteria)
        {
            StringBuilder sql = new StringBuilder();
            Dictionary<string, object> param = new Dictionary<string, object>();
            Dictionary<string, IEnumerable<object>> filterTables = new Dictionary<string, IEnumerable<object>>();

            // Build sql to get provider languages
            sql.AppendLine(@"
                select pl.providerlanguage_k as DatabaseKey, pl.provider_k as foreigndatabasekey, pl.language_rtk as LanguageKey, rtrim(rt.description) as Description
        		from visualcactus.providers p
                join visualcactus.providerlanguages pl on p.provider_k = pl.provider_k
        		join visualcactus.reftable rt on pl.language_rtk = rt.reftable_k");

            #region Add limiting criteria

            #region Add join statements
            if (criteria != null)
            {
                string tableName;
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "ids":
                            tableName = Database.GenerateTempTableName($"provlanguages_{key}");
                            filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                            sql.AppendLine($"  join {tableName} tmp_{key} on rtrim(p.id) = tmp_{key}.[value]");
                            break;
                    }
                }
            }
            #endregion

            sql.AppendLine(" where 1=1");

            #region Add where clauses
            if (criteria != null)
            {
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "id":
                            param.Add(key, criteria[key]);
                            sql.AppendLine("   and rtrim(p.id) = @id");
                            break;
                        case "npi":
                            param.Add(key, criteria[key]);
                            sql.AppendLine("   and rtrim(p.npi) = @npi");
                            break;
                    }
                }
            }
            #endregion

            #endregion

            // Get provider languages
            return Database.QueryWithFilters<ProviderLanguage>(sql.ToString(), param.ToQueryParamObject(), filterTables);
        }
        #endregion

        #region Get Provider License - Base Method
        /// <summary>
        /// Get a collection of provider licenses from Cactus
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <returns>Collection of provider license records</returns>
        private IEnumerable<ProviderLicense> GetProviderLicenses(Dictionary<string, object> criteria)
        {
            StringBuilder sql = new StringBuilder();
            Dictionary<string, object> param = new Dictionary<string, object>();
            Dictionary<string, IEnumerable<object>> filterTables = new Dictionary<string, IEnumerable<object>>();

            // Build sql to get provider licenses
            sql.AppendLine(@"
                select pl.providerlicense_k as DatabaseKey, pl.provider_k as ForeignDatabaseKey, rtrim(rt.description) as Description, rtrim(i.name) as InstitutionName,
                rtrim(rt2.description) as status, rtrim(pl.licensenumber) as licensenumber, rtrim(pl.state) as licensestate,
                pl.awardeddate as startdate, pl.expirationdate as enddate
        		from visualcactus.providers p
                join visualcactus.providerlicenses pl on p.provider_k = pl.provider_k
        		join visualcactus.reftable rt on pl.license_rtk = rt.reftable_k
				join visualcactus.reftable rt2 on pl.status_rtk = rt2.reftable_k
        		join visualcactus.institutions i on pl.institution_k = i.institution_k");

            #region Add limiting criteria

            #region Add join statements
            if (criteria != null)
            {
                string tableName;
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "ids":
                            tableName = Database.GenerateTempTableName($"provlicenses_{key}");
                            filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                            sql.AppendLine($"  join {tableName} tmp_{key} on rtrim(p.id) = tmp_{key}.[value]");
                            break;
                    }
                }
            }
            #endregion

            sql.AppendLine(@"
        		where 1 = 1
        		and pl.entity_k in ('      para', '       all')");

            #region Add where clauses
            if (criteria != null)
            {
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "id":
                            param.Add(key, criteria[key]);
                            sql.AppendLine("   and rtrim(p.id) = @id");
                            break;
                        case "npi":
                            param.Add(key, criteria[key]);
                            sql.AppendLine("   and rtrim(p.npi) = @npi");
                            break;
                    }
                }
            }
            #endregion

            #endregion

            // Get provider licenses
            return Database.QueryWithFilters<ProviderLicense>(sql.ToString(), param.ToQueryParamObject(), filterTables);
        }
        #endregion

        #region Get Provider Specialty - Base Method
        /// <summary>
        /// Get a collection of provider specialties from Cactus
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <returns>Collection of provider specialty records</returns>
        private IEnumerable<ProviderSpecialty> GetProviderSpecialties(Dictionary<string, object> criteria)
        {
            StringBuilder sql = new StringBuilder();
            Dictionary<string, object> param = new Dictionary<string, object>();
            Dictionary<string, IEnumerable<object>> filterTables = new Dictionary<string, IEnumerable<object>>();

            // Build sql to get provider specialties
            sql.AppendLine(@"
                select ps.providerspecialty_k as DatabaseKey, ps.provider_k as ForeignDatabaseKey, ps.specialty_rtk as SpecialtyKey, rtrim(rt2.shortdescription) as Code, rtrim(rt.description) as Description
                from visualcactus.providers p
        		join visualcactus.providerspecialties ps on p.provider_k = ps.provider_k
        		join visualcactus.reftable rt on ps.specialty_rtk = rt.reftable_k
                left join visualcactus.reftable rt2 on rt.foriegn_k = rt2.reftable_k");

            #region Add limiting criteria

            #region Add join statements
            if (criteria != null)
            {
                string tableName;
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "ids":
                            tableName = Database.GenerateTempTableName($"provspecs_{key}");
                            filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                            sql.AppendLine($"  join {tableName} tmp_{key} on rtrim(p.id) = tmp_{key}.[value]");
                            break;
                    }
                }
            }
            #endregion

            sql.AppendLine(@"
        		where 1 = 1
        		and ps.entity_k in ('      para', '       all')
        		and ps.active = 1");

            #region Add where clauses
            if (criteria != null)
            {
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "id":
                            param.Add(key, criteria[key]);
                            sql.AppendLine("   and rtrim(p.id) = @id");
                            break;
                        case "npi":
                            param.Add(key, criteria[key]);
                            sql.AppendLine("   and rtrim(p.npi) = @npi");
                            break;
                    }
                }
            }
            #endregion

            #endregion

            // Get provider specialties
            return Database.QueryWithFilters<ProviderSpecialty>(sql.ToString(), param.ToQueryParamObject(), filterTables);
        }
        #endregion
        #endregion

    }
}

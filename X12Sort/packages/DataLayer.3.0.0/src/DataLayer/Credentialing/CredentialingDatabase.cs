using DataLayer.Base;
using System;

namespace DataLayer.Credentialing
{
    /// <summary>
    /// Logical Database for the Credentialing System
    /// </summary>
    /// <remarks>
    /// Many tables in cactus have a column named entity_k. When this column is present a query should include:
    /// and entity_k in('      para', '       all')
    /// This will filter out data that is not paramounts or shared between all promedica divisions.
    /// </remarks>
    public class CredentialingDatabase : BaseDatabase
    {
        public AddressRepository Addresses { get; private set; }
        public GroupRepository Groups { get; private set; }
        public ProviderRepository Providers { get; private set; }
        public UtilityRepository Utilities { get; private set; }

        internal CredentialingDatabase(MachineApplicationInfo info, params IExecutor[] executors) : base(info, executors)
        {
            Addresses = new AddressRepository(this);
            Groups = new GroupRepository(this);
            Providers = new ProviderRepository(this);
            Utilities = new UtilityRepository(this);
        }
    }
}

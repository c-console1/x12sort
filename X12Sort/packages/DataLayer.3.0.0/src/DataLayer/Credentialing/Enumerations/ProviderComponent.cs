namespace DataLayer.Credentialing.Enumerations
{
    public enum ProviderComponent : int
    {
        Addresses = 1,
        Boards = 2,
        EntityAssignments = 3,
        Groups = 4,
        Languages = 5,
        Licenses = 6,
        Education = 7,
        Specialties = 8
    }
}

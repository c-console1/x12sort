using DataLayer.Base.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Credentialing.Models
{
    /// <summary>
    /// This is a class for cactus provider education
    /// </summary>
    public class ProviderEducation : BaseCredentialingObject, IStartEnd, IForeignBaseDataObject
    {
        /// <summary>
        /// Foreign key relating to the parent provider record
        /// </summary>
        public string ForeignDatabaseKey { get; set; }
        /// <summary>
        /// Degree primary key
        /// </summary>
        public string DegreeKey { get; set; }
        /// <summary>
        /// Degree description
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Degree specialty
        /// </summary>
        public string Specialty { get; set; }
        /// <summary>
        /// Degree program
        /// </summary>
        public string Program { get; set; }
        /// <summary>
        /// Educational institution
        /// </summary>
        public string Institution { get; set; }
        /// <summary>
        /// Education start date
        /// </summary>
        public DateTime? StartDate { get; set; }
        /// <summary>
        /// Education end date
        /// </summary>
        public DateTime? EndDate { get; set; }
    }

}

using DataLayer.Base.Models;
using System.Collections.Generic;

namespace DataLayer.Credentialing.Models
{
    /// <summary>
    /// This is a class for cactus provider groups
    /// </summary>
    public class Group : BaseCredentialingObject, IProviderGroup, IForeignBaseDataObject
    {
        /// <summary>
        /// Foreign key used for connecting this group to provider records
        /// </summary>
        public string ForeignDatabaseKey { get; set; }
        /// <summary>
        /// Provider group number
        /// </summary>
        public string GroupNumber { get; set; }
        /// <summary>
        /// Provider group name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Provider group npi
        /// </summary>
        public string NPI { get; set; }
        /// <summary>
        /// Provider group tax id
        /// </summary>
        public string TaxID { get; set; }
        /// <summary>
        /// List of providers belonging to this group
        /// </summary>
        public IEnumerable<Provider> Providers { get; set; }
        /// <summary>
        /// List of addresses belonging to this group
        /// </summary>
        public IEnumerable<Address> Addresses { get; set; }

    }
}

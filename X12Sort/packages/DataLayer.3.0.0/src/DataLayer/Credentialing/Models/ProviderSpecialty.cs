using DataLayer.Base.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Credentialing.Models
{
    /// <summary>
    /// This is a class for cactus provider specialties
    /// </summary>
    public class ProviderSpecialty : BaseCredentialingObject, ICode, IStartEnd, IForeignBaseDataObject
    {
        /// <summary>
        /// Foreign key relating to the parent provider record
        /// </summary>
        public string ForeignDatabaseKey { get; set; }
        /// <summary>
        /// Specialty code
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// Specialty description
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Specialty start date
        /// </summary>
        public DateTime? StartDate { get; set; }
        /// <summary>
        /// Specialty end date
        /// </summary>
        public DateTime? EndDate { get; set; }
    }

}

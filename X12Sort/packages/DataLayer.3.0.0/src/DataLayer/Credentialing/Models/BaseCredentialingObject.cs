using DataLayer.Base.Enumerations;
using DataLayer.Base.Models;

namespace DataLayer.Credentialing.Models
{
    /// <summary>
    /// Base object for all Credentialing Objects
    /// </summary>
    public abstract class BaseCredentialingObject : BaseDataObject
    {
        public override DataSource Source { get => DataSource.Cactus; }

        /// <summary>
        /// Standard Constructor, Should be called by all inheriting objects
        /// </summary>
        protected BaseCredentialingObject() : base()
        {
        }
    }
}

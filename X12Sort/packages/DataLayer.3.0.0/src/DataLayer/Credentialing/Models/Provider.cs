using DataLayer.Base.Enumerations;
using DataLayer.Base.Models;
using System;
using System.Collections.Generic;

namespace DataLayer.Credentialing.Models
{
    /// <summary>
    /// This is a class for cactus providers
    /// </summary>
    public class Provider : BaseCredentialingObject, IProvider
    {
        /// <summary>
        /// The company wide number for a provider, should be the same in all systems
        /// </summary>
        public string ProviderNumber { get; set; }
        /// <summary>
        /// Provider's first name
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Provider's middle name
        /// </summary>
        public string MiddleName { get; set; }
        /// <summary>
        /// Provider's last name
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Provider's name suffix
        /// </summary>
        public string Suffix { get; set; }
        /// <summary>
        /// Provider's degrees
        /// </summary>
        public string Degrees { get; set; }
        /// <summary>
        /// Required by IProvider. This field does not exist in cactus.
        /// </summary>
        public string InstitutionName { get; set; }
        /// <summary>
        /// Is this provider an individual?
        /// </summary>
        public bool IsIndividual { get; set; }
        /// <summary>
        /// Is this provider an institution?
        /// </summary>
        public bool IsInstitution
        {
            get
            {
                return !IsIndividual;
            }
        }
        /// <summary>
        /// Provider's medicare number
        /// </summary>
        public string MedicareNumber { get; set; }
        /// <summary>
        /// Provider's medicaid number
        /// </summary>
        public string MedicaidNumber { get; set; }
        /// <summary>
        /// Provider's ssn
        /// </summary>
        public string SSN { get; set; }
        /// <summary>
        /// Provider's upin
        /// </summary>
        public string UPIN { get; set; }
        /// <summary>
        /// Provider's npi
        /// </summary>
        public string NPI { get; set; }
        /// <summary>
        /// Provider's tax id
        /// </summary>
        public string TaxID { get; set; }
        /// <summary>
        /// Provider's birth date
        /// </summary>
        public DateTime? BirthDate { get; set; }
        /// <summary>
        /// Provider's gender
        /// </summary>
        public Sex Sex { get; set; }
        /// <summary>
        /// List of addresses owned by this provider
        /// </summary>
        public IEnumerable<Address> Addresses { get; set; }
        /// <summary>
        /// List of boards certifying this provider
        /// </summary>
        public IEnumerable<ProviderBoard> Boards { get; set; }
        /// <summary>
        /// List of provider contracts
        /// </summary>
        public IEnumerable<ProviderEntityAssignment> EntityAssignments { get; set; }
        /// <summary>
        /// List of provider groups this provider is a member of
        /// </summary>
        public IEnumerable<Group> Groups { get; set; }
        /// <summary>
        /// List of languages spoken by this provider
        /// </summary>
        public IEnumerable<ProviderLanguage> Languages { get; set; }
        /// <summary>
        /// List of licenses owned by this provider
        /// </summary>
        public IEnumerable<ProviderLicense> Licenses { get; set; }
        /// <summary>
        /// List of this provider's education
        /// </summary>
        public IEnumerable<ProviderEducation> Education { get; set; }
        /// <summary>
        /// List of this provider's specialties
        /// </summary>
        public IEnumerable<ProviderSpecialty> Specialties { get; set; }

    }
}

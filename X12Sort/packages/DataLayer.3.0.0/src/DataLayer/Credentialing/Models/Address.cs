using DataLayer.Base.Models;
using System.Collections.Generic;

namespace DataLayer.Credentialing.Models
{
    /// <summary>
    /// This is a class for cactus addresses
    /// </summary>
    public class Address : BaseCredentialingObject, IAddress, ICoordiantes, IForeignBaseDataObject
    {
        /// <summary>
        /// Foreign key relating to the parent provider record
        /// </summary>
        public string ForeignDatabaseKey { get; set; }
        /// <summary>
        /// Address name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Address line 1
        /// </summary>
        public string Address1 { get; set; }
        /// <summary>
        /// Address line 2
        /// </summary>
        public string Address2 { get; set; }
        /// <summary>
        /// City
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// County
        /// </summary>
        public string County { get; set; }
        /// <summary>
        /// State
        /// </summary>
        public USState State { get; set; }
        /// <summary>
        /// Zip code
        /// </summary>
        public ZipCode ZipCode { get; set; }
        /// <summary>
        /// Country
        /// </summary>
        public Country Country { get; set; }
        /// <summary>
        /// Phone number
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// Fax number
        /// </summary>
        public string Fax { get; set; }
        /// <summary>
        /// Address type (billing, mailing, etc)
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// Address longitude (w083 40.8871)
        /// </summary>
        public decimal? Longitude { get; set; }
        /// <summary>
        /// Address Latitude (n41 35.0056)
        /// </summary>
        public decimal? Latitude { get; set; }

        public Address()
        {
        }

        public Address(string address1, string address2, string city, string county, string state, string zip, string phone, string fax)
        {
            Address1 = address1;
            if (address2 == null)
                Address2 = "";
            else
                Address2 = address2;
            City = city;
            County = county;
            State = USState.FromString(state);
            ZipCode = ZipCode.FromString(zip);
            Phone = phone;
            Fax = fax;
        }

        public static bool operator ==(Address obj1, Address obj2)
        {
            if (ReferenceEquals(obj1, null) && ReferenceEquals(obj2, null))
                return true;

            if (ReferenceEquals(obj1, null) || ReferenceEquals(obj2, null))
                return false;

            return obj1.DatabaseKey == obj2.DatabaseKey
                && obj1.Address1 == obj2.Address1
                && obj1.Address2 == obj2.Address2
                && obj1.City == obj2.City
                && obj1.County == obj2.County
                && obj1.State == obj2.State
                && obj1.ZipCode == obj2.ZipCode
                && obj1.Phone == obj2.Phone
                && obj1.Fax == obj2.Fax;
        }

        public static bool operator !=(Address obj1, Address obj2)
        {
            return !(obj1 == obj2);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            Address address = obj as Address;

            return DatabaseKey == address.DatabaseKey
                && Address1 == address.Address1
                && Address2 == address.Address2
                && City == address.City
                && County == address.County
                && State == address.State
                && ZipCode == address.ZipCode
                && Phone == address.Phone
                && Fax == address.Fax;
        }

        public override int GetHashCode()
        {
            //get hash code theory: https://blogs.msdn.microsoft.com/ericlippert/2011/02/28/guidelines-and-rules-for-gethashcode/
            //implementation idea borrowed from: https://rehansaeed.com/gethashcode-made-easy/
            unchecked
            {
                int hashCode = 17;
                hashCode = (hashCode * 23) + (DatabaseKey == null ? 0 : DatabaseKey.GetHashCode());
                hashCode = (hashCode * 23) + (Address1 == null ? 0 : Address1.GetHashCode());
                hashCode = (hashCode * 23) + (Address2 == null ? 0 : Address2.GetHashCode());
                hashCode = (hashCode * 23) + (City == null ? 0 : City.GetHashCode());
                hashCode = (hashCode * 23) + (County == null ? 0 : County.GetHashCode());
                hashCode = (hashCode * 23) + (State == null ? 0 : State.GetHashCode());
                hashCode = (hashCode * 23) + (ZipCode == null ? 0 : ZipCode.GetHashCode());
                hashCode = (hashCode * 23) + (Phone == null ? 0 : Phone.GetHashCode());
                hashCode = (hashCode * 23) + (Fax == null ? 0 : Fax.GetHashCode());

                return hashCode;
            }
        }

    }
}

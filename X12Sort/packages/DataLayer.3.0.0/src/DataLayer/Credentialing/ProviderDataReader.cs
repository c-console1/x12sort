using System;
using System.Collections.Generic;
using System.Data;
using DataLayer.Base;
using DataLayer.Credentialing.Enumerations;
using DataLayer.Credentialing.Models;

namespace DataLayer.Credentialing
{
    public class ProviderDataReader : BaseDataReader<CredentialingDatabase, Provider>
    {
        #region Constants/variables
        private IEnumerable<ProviderComponent> _includedComponents;
        #endregion

        #region Constructors
        /// <summary>
        /// Standard Constructor
        /// </summary>
        /// <param name="database">Reference to the database where the data is coming from</param>
        /// <param name="sql">SQL string to get data from the database</param>
        /// <param name="filterTables">List of temp filter table names with corresponding lists of values</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        internal ProviderDataReader(CredentialingDatabase database, string sql, object param, Dictionary<string, IEnumerable<object>> filterTables, IEnumerable<ProviderComponent> includedComponents = null)
            : base(database, sql, param, filterTables)
        {
            _includedComponents = includedComponents;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Load records list with the next set of records
        /// </summary>
        /// <param name="recordCount">Maximum number of records to load into record list</param>
        /// <returns>True if more records have been loaded to Records; False if no records are remaining in the data reader</returns>
        public override bool NextResultSet(int recordCount = DefaultResultSetSize, int? commandTimeOut = null)
        {
            // Call base method
            // Return false if no more records were found in the data reader
            if (!base.NextResultSet(recordCount, commandTimeOut)) return false;

            // Include other components
            Database.Providers.AddComponentsToProviders(Records, _includedComponents);

            return true;
        }
        #endregion
    }
}

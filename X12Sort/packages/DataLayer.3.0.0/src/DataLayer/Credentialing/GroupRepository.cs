using DataLayer.Base;
using DataLayer.Credentialing.Enumerations;
using DataLayer.Credentialing.Models;
using DataLayer.StaticUtilities.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataLayer.Credentialing
{
    public class GroupRepository : BaseRepository<CredentialingDatabase>
    {
        const bool DEFAULT_ERROR_ON_MULTIPLE = true;

        internal GroupRepository(CredentialingDatabase database) : base(database)
        {

        }

        ///////////////////////////////////////////////////////////////////////////////////////////

        #region Public Group Methods

        #region Get groups by group id
        /// <summary>
        /// Get groups from Cactus by provider ID
        /// </summary>
        /// <param name="ids">Group ID</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>List of group records</returns>
        public IEnumerable<Group> GetGroupsByProviderID(IEnumerable<string> ids, IEnumerable<GroupComponent> includedComponents = null)
        {
            return GetGroups(
                new Dictionary<string, object>()
                {
                    { "ids", ids }
                },
                includedComponents
                );
        }
        #endregion

        #region Get group(s) by name
        /// <summary>
        /// Get group from Cactus by name
        /// </summary>
        /// <param name="groupName">Group name</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <param name="errorOnMultiple">Wether the method should generate an error when multiple records are returned</param>
        /// <returns>Single group record</returns>
        public Group GetGroupByName(string groupName, IEnumerable<GroupComponent> includedComponents = null, bool errorOnMultiple = DEFAULT_ERROR_ON_MULTIPLE)
        {
            {
                return GetGroup(
                    new Dictionary<string, object>()
                    {
                       { "groupName", groupName }
                    },
                    includedComponents,
                    errorOnMultiple
                    );
            }
        }

        /// <summary>
        /// Get a collection of groups from Cactus by name
        /// </summary>
        /// <param name="groupName">Group name</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Single group record</returns>
        public IEnumerable<Group> GetGroupsByName(string groupName, IEnumerable<GroupComponent> includedComponents = null)
        {
            {
                return GetGroups(
                    new Dictionary<string, object>()
                    {
                       { "groupName", groupName }
                    },
                    includedComponents
                    );
            }
        }
        #endregion

        #region Get group(s) by name like
        /// <summary>
        /// Get group from Cactus by name using like comparison
        /// </summary>
        /// <param name="groupName">Group name</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <param name="errorOnMultiple">Wether the method should generate an error when multiple records are returned</param>
        /// <returns>Single group record</returns>
        public Group GetGroupsByNameLike(string groupName, IEnumerable<GroupComponent> includedComponents = null, bool errorOnMultiple = DEFAULT_ERROR_ON_MULTIPLE)
        {
            return GetGroup(
                new Dictionary<string, object>()
                {
                    { "groupNameLike", groupName }
                },
                includedComponents,
                errorOnMultiple
                );
        }

        /// <summary>
        /// Get a collection of groups from Cactus by name using like comparison
        /// </summary>
        /// <param name="groupName">Group name</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <param name="errorOnMultiple">Wether the method should generate an error when multiple records are returned</param>
        /// <returns>Single group record</returns>
        public IEnumerable<Group> GetGroupsByNameLike(string groupName, IEnumerable<GroupComponent> includedComponents = null)
        {
            return GetGroups(
                new Dictionary<string, object>()
                {
                    { "groupNameLike", groupName }
                },
                includedComponents
                );
        }
        #endregion

        #endregion

        ///////////////////////////////////////////////////////////////////////////////////////////

        #region Public Group Address Methods
        /// <summary>
        /// Get a collection of group addresses from Cactus by group ID
        /// </summary>
        /// <param name="ids">Group ID</param>
        /// <returns>Collection of group address records</returns>
        public IEnumerable<Address> GetGroupAddressesByID(IEnumerable<string> ids)
        {
            return GetAddresses(
                new Dictionary<string, object>(){
                    { "ids", ids }
                });
        }
        #endregion

        ///////////////////////////////////////////////////////////////////////////////////////////

        #region InternalgroupComponent Methods

        /// <summary>
        /// Add components to a list of groups
        /// </summary>
        /// <param name="groups">List groups to add components to</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        internal void AddComponentsToGroups(IEnumerable<Group> groups, IEnumerable<GroupComponent> includedComponents)
        {
            if (includedComponents != null)
            {
                IEnumerable<string> grpDatabaseKeys = groups.Select(g => g.DatabaseKey);
                foreach (GroupComponent component in includedComponents)
                    switch (component)
                    {
                        case GroupComponent.Addresses:
                            IEnumerable<Address> addresses = Database.Groups.GetGroupAddressesByID(grpDatabaseKeys);
                            foreach (Group group in groups)
                                group.Addresses = addresses.Where(a => a.ForeignDatabaseKey == group.DatabaseKey);
                            break;
                        default:
                            throw new ArgumentException($"The included component is not valid: {component}", nameof(includedComponents));
                    }
            }
        }

        #endregion

        ///////////////////////////////////////////////////////////////////////////////////////////

        #region Private Methods

        #region GetGroup(s) - Base Methods
        /// <summary>
        /// Get a collection of groups from Cactus
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Collection ofgrouprecords</returns>

        /// <summary>
        /// Get a single group from Cactus
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <param name="errorOnMultiple">Whether the method should generate an error when multiple records are returned</param>
        /// <returns>Single group record</returns>
        private Group GetGroup(Dictionary<string, object> criteria, IEnumerable<GroupComponent> includedComponents, bool errorOnMultiple)
        {
            IEnumerable<Group> groups = GetGroups(criteria, includedComponents);
            if (groups.Count() > 1 && errorOnMultiple)
                throw new Exception($"The query returned multiple results: {groups.Count()} providers found");
            else if (groups.Any())
                return null;
            else
                return groups.First();
        }

        /// <summary>
        /// Gets a collection of groups from Cactus
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Collection of group records</returns>
        private IEnumerable<Group> GetGroups(Dictionary<string, object> criteria, IEnumerable<GroupComponent> includedComponents)
        {
            // Build group query
            (string sql, object param, Dictionary<string, IEnumerable<object>> filterTables) = BuildGroupQuery(criteria);

            // Get group addresses
            IEnumerable<Group> groups = Database.QueryWithFilters<Group>(sql, param, filterTables);

            // Include other components
            AddComponentsToGroups(groups, includedComponents);

            return groups;
        }

        /// <summary>
        /// Get a data reader for groups from Cactus
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <param name="includedComponents">List of components to include in the result</param>
        /// <returns>Group data reader for Cactus groups</returns>
        private GroupDataReader GetProviderDataReader(Dictionary<string, object> criteria, IEnumerable<GroupComponent> includedComponents)
        {
            // Build group query
            (string sql, object param, Dictionary<string, IEnumerable<object>> filterTables) = BuildGroupQuery(criteria);

            // Get data reader
            return new GroupDataReader(Database, sql, param, filterTables, includedComponents);
        }

        /// <summary>
                 /// Generates the SQL and parameters necessary to get group data from Cactus using the specified criteria
                 /// </summary>
                 /// <param name="criteria">Limiting criteria for the query</param>
                 /// <returns>
                 /// 1 - SQL string to get group base data from Cactus
                 /// 2 - param object for Dapper query parameter substitution
                 /// 3 - list of tables to filter by with table contents
                 /// </returns>
        private (string sql, object param, Dictionary<string, IEnumerable<object>> filterTables) BuildGroupQuery(Dictionary<string, object> criteria)
        {
            StringBuilder sql = new StringBuilder();
            Dictionary<string, object> param = new Dictionary<string, object>();
            Dictionary<string, IEnumerable<object>> filterTables = new Dictionary<string, IEnumerable<object>>();

            // Build sql to get base group information
            sql.AppendLine($@"
                select * from(
                    select distinct g.group_k as DatabaseKey, pa.provider_k as ForeignDatabaseKey, p.id as provid, rtrim(g.groupname) as Name, rtrim(g.npi), rtrim(g.taxid),
                    row_number() over (order by g.group_k asc) as rowNum
                    from visualcactus.providers p
                    join visualcactus.provideraddresses pa on p.provider_k = pa.provider_k
                    join visualcactus.groupsprovidersaddresses gpa on pa.provideraddress_k = gpa.provideraddress_k
                    join visualcactus.groupaddress ga on gpa.groupaddress_k = ga.groupaddress_k
                    join visualcactus.groups g on ga.group_k = g.group_k
                    where 1=1
                    and pa.entity_k in('      para','       all')
                    and ga.entity_k in('      para','       all')
                    and g.entity_k in('      para','       all')
                    and pa.active = 1
                    and ga.active = 1
                    and g.active = 1
                ) grp");

            #region Add limiting criteria

            #region Add join statements
            if (criteria != null)
            {
                string tableName;
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "ids":
                            tableName = Database.GenerateTempTableName($"groupsprovidersaddresses_{key}");
                            filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                            sql.AppendLine($"  join {tableName} tmp_{key} on rtrim(grp.provid) = tmp_{key}.[value]");
                            break;
                    }
                }
            }
            #endregion


            #region Add row number limiter
            sql.AppendLine(" where grp.rowNum between @minRowNum and @maxRowNum");

            // Add min and max row numbers values (will be overwritten if this is destined for a data reader)
            param.Add("minRowNum", 0);
            param.Add("maxRowNum", int.MaxValue);
            #endregion

            #region Add where clauses
            if (criteria != null)
            {
                foreach (string key in criteria.Keys)
                    switch (key)
                    {
                        case "groupName":
                            param.Add(key, criteria[key]);
                            sql.AppendLine("   and rtrim(grp.name) = @groupName");
                            break;
                        case "groupNameLike":
                            param.Add(key, criteria[key]);
                            sql.AppendLine("   and rtrim(grp.name) like @groupName");
                            break;
                    }
            }
            #endregion

            #endregion

            /// 1 - SQL string to getgroupbase data from Cactus
            /// 2 - param object for Dapper query parameter substitution
            /// 3 - list of tables to filter by with table contents
            return (sql.ToString(), param.ToQueryParamObject(), filterTables);
        }
        #endregion

        #region Get Group Addresses - Base Method
        /// <summary>
        /// Get a collection of group addresses from the Cactus
        /// </summary>
        /// <param name="criteria">Limiting criteria for the query</param>
        /// <returns>Collection ofgroupaddress records</returns>
        private IEnumerable<Address> GetAddresses(Dictionary<string, object> criteria)
        {
            StringBuilder sql = new StringBuilder();
            Dictionary<string, object> param = new Dictionary<string, object>();
            Dictionary<string, IEnumerable<object>> filterTables = new Dictionary<string, IEnumerable<object>>();

            if (criteria == null)
                criteria = new Dictionary<string, object>();

            // Build sql to getgroupaddresses
            sql.AppendLine(@"
                select ga.groupaddress_k as DatabaseKey, ga.group_k as ForeignDatabaseKey, rtrim(rt.description) as type,
				rtrim(a.addressline1) as address1, rtrim(a.addressline2) as address2, rtrim(a.city) as city, rtrim(a.state) as state, a.zipcode, a.phone, a.fax, a.taxid,
				a.latitude, a.longitude
				from visualcactus.groupaddress ga
				join visualcactus.reftable rt on ga.addresstype_rtk = rt.reftable_k
				join visualcactus.addresses a on ga.address_k = a.address_k");

            #region Add limiting criteria

            #region Add join statements
            if (criteria != null)
            {
                string tableName;
                foreach (string key in criteria.Keys)
                {
                    switch (key)
                    {
                        case "ids":
                            tableName = Database.GenerateTempTableName($"groupaddresses_{key}");
                            filterTables.Add(tableName, (IEnumerable<object>)criteria[key]);
                            sql.AppendLine($"  join {tableName} tmp_{key} on rtrim(ga.group_k) = tmp_{key}.[value]");
                            break;
                    }
                }
            }
            #endregion

            sql.AppendLine(@"
				where 1 = 1
				and ga.entity_k in ('      para', '       all')
				and ga.active = 1");

            #region Add where clauses
            #endregion

            #endregion

            // Getgroupaddresses
            return Database.QueryWithFilters<Address>(sql.ToString(), param.ToQueryParamObject(), filterTables);
        }

        #endregion

        #endregion

    }
}

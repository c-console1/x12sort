using System;
using System.Collections.Generic;
using System.Text;

/// <summary>
/// These are named based on the PIK REST API documentation, PLEASE DO NOT FIX THE CASE ON NAMES!
/// See documents in http://sps.promedica.org/it/pcsi/Shared%20Documents/Project%20Planning/HealthEdge%20Documentation/Portal%20Integration%20Kit/Sample%20JSON%20Files
/// </summary>

namespace DataLayer.HealthEdgePIK.Models
{
    public class DemographicChange
    {
        public string asOfDate { get; set; }
        public string subscriberId { get; set; }
        public List<DemographicMember> member { get; set; }
    }

    public class DemographicMember
    {
        public string hccIdentifier { get; set; }
        public DemographicIndividual individual { get; set; }
        public DemographicRaceOrEthnicity raceOrEthnicity { get; set; }
    }

    public class DemographicIndividual
    {
        public string taxIdentificationNumber { get; set; }
        public string genderCode { get; set; }
        public string maritalStatusCode { get; set; }
        public string birthDate { get; set; }
        public DemographicPrimaryName primaryName { get; set; }
        public DemographicLanguages languages { get; set; }
    }

    public class DemographicPrimaryName
    {
        public string lastName { get; set; }
        public string firstName { get; set; }
        public string middleName { get; set; }
    }

    public class DemographicLanguages
    {
        public List<DemographicLanguage> language { get; set; }
    }

    public class DemographicLanguage
    {
        public string primaryLanguage { get; set; }
        public string nativeLanguage { get; set; }
        public DemographicLanguageDomainCode languageDomainCode { get; set; }
    }

    public class DemographicLanguageDomainCode
    {
        public string codeEntry { get; set; }
    }

    public class DemographicRaceOrEthnicity
    {
        public List<DemographicRaceOrEthnicityCodes> raceOrEthnicityCodes { get; set; }
    }

    public class DemographicRaceOrEthnicityCodes
    {
        public string codeEntry { get; set; }
    }
}

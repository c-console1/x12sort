using System;
using System.Collections.Generic;
using System.Text;

/// <summary>
/// These are named based on the PIK REST API documentation, PLEASE DO NOT FIX THE CASE ON NAMES!
/// See documents in http://sps.promedica.org/it/pcsi/Shared%20Documents/Project%20Planning/HealthEdge%20Documentation/Portal%20Integration%20Kit/Sample%20JSON%20Files
/// </summary>

namespace DataLayer.HealthEdgePIK.Models
{
    public class TokenBody
    {
        public int lifeTime { get; set; }
        public string apiKey { get; set; }
    }
}

using System;
using System.Collections.Generic;
using System.Text;

/// <summary>
/// These are named based on the PIK REST API documentation, PLEASE DO NOT FIX THE CASE ON NAMES!
/// See documents in http://sps.promedica.org/it/pcsi/Shared%20Documents/Project%20Planning/HealthEdge%20Documentation/Portal%20Integration%20Kit/Sample%20JSON%20Files
/// </summary>

namespace DataLayer.HealthEdgePIK.Models
{
    public class BenefitPlanEnquiry
    {
        public string externalBenefitPlanId { get; set; }
        public benefitIndividual individual { get; set; }
        public string coverageLevelCode { get; set; }
        public string benefitInformationCode { get; set; }
        public string benefitBegin { get; set; }
        public string quantityQualifier { get; set; }
        public string timePeriodQualifier { get; set; }
        public string businessName { get; set; }
        public string networkName { get; set; }
        public string inPlanNetworkIndicator { get; set; }
        public string benefitIdentifier { get; set; }
        public string planName { get; set; }
        public string benNotes { get; set; }
        public benefitNetworkDefinitionDetails networkDefinitionDetails { get; set; }
        public string planCoverageDescription { get; set; }
        public string employerGroup { get; set; }
        public string isHighDeductiblePlan { get; set; }
        public string fundingAmountBasis { get; set; }
        public string tierCode { get; set; }
        public string benefitEnd { get; set; }
        public List<benefitPlanComponents> benefitPlanComponents { get; set; }
        public string serviceTypeCode { get; set; }
        public string employerGroupName { get; set; }
    }

    public class benefitIndividual
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string middleName { get; set; }
        public string relationship { get; set; }
    }

    public class benefitNetworkDefinitionDetails
    {
        public string componentName { get; set; }
    }

    public class benefitPlanComponents
    {
        public int weight { get; set; }
        public string componentName { get; set; }
    }
}

using System;
using System.Collections.Generic;
using System.Text;

/// <summary>
/// These are named based on the PIK REST API documentation, PLEASE DO NOT FIX THE CASE ON NAMES!
/// See documents in http://sps.promedica.org/it/pcsi/Shared%20Documents/Project%20Planning/HealthEdge%20Documentation/Portal%20Integration%20Kit/Sample%20JSON%20Files
/// </summary>

namespace DataLayer.HealthEdgePIK.Models
{
    public class EligibilityEnquiry
    {
        public string lastName { get; set; }
        public string zipCode { get; set; }
        public string physiciantitle { get; set; }
        public string epsdtflag { get; set; }
        public string relationshipToSubscriber { get; set; }
        public string city { get; set; }
        public string carriername { get; set; }
        public string prefix { get; set; }
        public string contractstatuscode { get; set; }
        public string physicianSpecialty { get; set; }
        public string planName { get; set; }
        public string pcpfromdate { get; set; }
        public string suffix { get; set; }
        public string sourcesystem { get; set; }
        public string ssn { get; set; }
        public string division { get; set; }
        public int elgtodate { get; set; }
        public string provider { get; set; }
        public string physicianfirstname { get; set; }
        public string member { get; set; }
        public string networkname { get; set; }
        public string state { get; set; }
        public string sexcode { get; set; }
        public string employergroup { get; set; }
        public string employergroupname { get; set; }
        public string business { get; set; }
        public string address2 { get; set; }
        public string memberidprefix { get; set; }
        public string address1 { get; set; }
        public string contractstatusdescription { get; set; }
        public string medicade { get; set; }
        public string birthDate { get; set; }
        public string healthplan { get; set; }
        public string physicianNPI { get; set; }
        public string elgfromdate { get; set; }
        public string firstName { get; set; }
        public string primarysubscriber { get; set; }
        public string carrier { get; set; }
        public string phoneNumber { get; set; }
        public string physicianmiddlename { get; set; }
        public string physicianlastname { get; set; }
        public string businessname { get; set; }
        public string middleName { get; set; }
        public string pcptodate { get; set; }
        public string facility { get; set; }
        public string medicare { get; set; }
    }
}

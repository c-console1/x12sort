using System;
using System.Collections.Generic;
using System.Text;

/// <summary>
/// These are named based on the PIK REST API documentation, PLEASE DO NOT FIX THE CASE ON NAMES!
/// See documents in http://sps.promedica.org/it/pcsi/Shared%20Documents/Project%20Planning/HealthEdge%20Documentation/Portal%20Integration%20Kit/Sample%20JSON%20Files
/// </summary>

namespace DataLayer.HealthEdgePIK.Models
{
    public class AddressChange
    {
        public string asOfDate { get; set; }
        public string subscriberId { get; set; }
        public List<AddressMember> member { get; set; }
    }

    public class AddressMember
    {
        public string hccIdentifier { get; set; }
        public PhysicalAddress physicalAddress { get; set; }
        public CorrespondenceAddress correspondenceAddress { get; set; }
    }

    public class PhysicalAddress
    {
        public List<MemberPhysicalAddress> memberPhysicalAddress { get; set; }
    }

    public class MemberPhysicalAddress
    {
        public string emailAddress { get; set; }
        public AddressInfo addressInfo { get; set; }
        public AddressTypeCode addressTypeCode { get; set; }
    }

    public class AddressInfo
    {
        public PostalAddress postalAddress { get; set; }
        public AddressPhoneList addressPhoneList { get; set; }
    }

    public class PostalAddress
    {
        public string address { get; set; }
        public string address2 { get; set; }
        public string address3 { get; set; }
        public string stateCode { get; set; }
        public string zipCode { get; set; }
        public string zipExtensionCode { get; set; }
        public string cityName { get; set; }
        public string countyCode { get; set; }
        public string countryCode { get; set; }
        public string longitude { get; set; }
        public string latitude { get; set; }
        public string ignoreAddressCheck { get; set; }
        public string undeliverableAddress { get; set; }
    }

    public class AddressPhoneList
    {
        public List<TelephoneNumber> telephoneNumber { get; set; }
    }

    public class TelephoneNumber
    {
        public string phoneCountryCode { get; set; }
        public string phoneAreaCode { get; set; }
        public string phoneNumber { get; set; }
        public string phoneExtensionNumber { get; set; }
        public IndividualPhoneTypeCode individualPhoneTypeCode { get; set; }
        public string organizationPhoneTypeCode { get; set; }
    }

    public class IndividualPhoneTypeCode
    {
        public string codeSetName { get; set; }
        public string codeEntry { get; set; }
        public string shortName { get; set; }
    }

    public class AddressTypeCode
    {
        public string codeSetName { get; set; }
        public string codeEntry { get; set; }
        public string shortName { get; set; }
    }

    public class CorrespondenceAddress
    {
        public string emailAddress { get; set; }
        public PostalAddress postalAddress { get; set; }
    }
}

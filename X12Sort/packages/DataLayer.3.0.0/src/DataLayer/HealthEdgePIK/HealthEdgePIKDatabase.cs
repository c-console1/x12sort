using DataLayer.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.HealthEdgePIK
{
    public class HealthEdgePIKDatabase : BaseDatabase
    {
        public HealthEdgePIKRepository PIKRepository { get; private set; }

        internal HealthEdgePIKDatabase(IDbExec dbExec) : base(dbExec)
        {
            PIKRepository = new HealthEdgePIKRepository(this);
        }

        internal HealthEdgePIKDatabase(params IExecutor[] executors) : base(executors)
        {
            PIKRepository = new HealthEdgePIKRepository(this);
        }
    }
}

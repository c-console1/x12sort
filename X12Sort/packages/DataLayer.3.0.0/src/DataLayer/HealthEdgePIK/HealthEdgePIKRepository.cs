using DataLayer.Base;
using DataLayer.HealthEdgePIK.Models;
using Flurl;
using Flurl.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;

namespace DataLayer.HealthEdgePIK
{
    public class HealthEdgePIKRepository : BaseRepository<HealthEdgePIKDatabase>
    {
        private readonly string _apiKey;
        private readonly string _baseURL;
        private PIKSecurityDatabase _pikSecurityDatabase;
        private readonly HealthEdgeCrypto _healthedgeCrypto;

        internal HealthEdgePIKRepository(HealthEdgePIKDatabase database) : base(database)
        {
            _pikSecurityDatabase = new PIKSecurityDatabase();
            _healthedgeCrypto = new HealthEdgeCrypto(_pikSecurityDatabase);
            _apiKey = _pikSecurityDatabase.GetKey((int)SecurityKeyEnum.APIKey);
            _baseURL = DataAccessConfigBuilder.Instance.Config.HealthEdgePIKBaseRESTURL;
        }

        /// <summary>
        /// Creates a Token for HealthEdge REST calls
        /// </summary>
        /// <returns></returns>
        public string TokenCreation()
        {
            TokenBody TokenBody = new TokenBody
            {
                lifeTime = DataAccessConfigBuilder.Instance.Config.HealthEdgePIKTokenLifeDays,
                apiKey = _apiKey
            };
            string[] Segments = new string[]
            {
                "auth",
                "token",
                "create"
            };

            return _baseURL.AppendPathSegments(Segments).PostJsonAsync(TokenBody).ReceiveString().Result;
        }

        /// <summary>
        /// Refreahes a Token for HealthEdge REST calls
        /// </summary>
        /// <returns></returns>
        public string TokenRefresh()
        {

            TokenBody TokenBody = new TokenBody
            {
                lifeTime = DataAccessConfigBuilder.Instance.Config.HealthEdgePIKTokenLifeDays,
                apiKey = _apiKey
            };
            string[] Segments = new string[]
            {
                "auth",
                "token",
                "refresh"
            };
            string Token = _baseURL.AppendPathSegments(Segments).PostJsonAsync(TokenBody).ReceiveString().Result;
            _pikSecurityDatabase.SetKey((int)SecurityKeyEnum.Token, Token, "Token");

            return Token;
        }

        /// <summary>
        /// This will get HealthEdge member eligibility fromt the EligibilityEnquiry REST call
        /// </summary>
        /// <param name="MemberID"></param>
        /// <returns></returns>
        public EligibilityEnquiry EligibilityEnquiry(string MemberID)
        {
            string Token = GetToken();
            string[] Segments = new string[]
            {
                "rti",
                "fetch",
                "eligibility"
            };
            HttpResponseMessage HttpResponseMessage = _baseURL.AppendPathSegments(Segments).SetQueryParam("memberId", MemberID).WithHeader("Authorization", Token).GetAsync().Result;
            string Signature = HttpResponseMessage.Headers.GetValues("X-Healthedge-Signature").FirstOrDefault();
            string DecryptedResponse = _healthedgeCrypto.DecryptResponse(HttpResponseMessage.Content.ReadAsStringAsync().Result, Signature);
            EligibilityEnquiry EligibilityEnquiry = JsonConvert.DeserializeObject<EligibilityEnquiry>(DecryptedResponse);

            return EligibilityEnquiry;
        }

        /// <summary>
        /// This will get HealthEdge member benefit plan information fromt the BenefitPlanEnquiry REST call
        /// </summary>
        /// <param name="MemberID"></param>
        /// <returns></returns>
        public List<BenefitPlanEnquiry> BenefitPlanEnquiry(string MemberID)
        {
            string Token = GetToken();
            string[] Segments = new string[]
            {
                "rti",
                "fetch",
                "benefitPlans"
            };
            HttpResponseMessage HttpResponseMessage = _baseURL.AppendPathSegments(Segments).SetQueryParam("memberId", MemberID).WithHeader("Authorization", Token).GetAsync().Result;
            string Signature = HttpResponseMessage.Headers.GetValues("X-Healthedge-Signature").FirstOrDefault();
            string DecryptedResponse = _healthedgeCrypto.DecryptResponse(HttpResponseMessage.Content.ReadAsStringAsync().Result, Signature);
            List<BenefitPlanEnquiry> BenefitPlanEnquiries = JsonConvert.DeserializeObject<List<BenefitPlanEnquiry>>(DecryptedResponse);

            return BenefitPlanEnquiries;
        }

        /// <summary>
        /// This wiill make sure the correct token will be returned to REST calls and will create and refresh as needed
        /// </summary>
        /// <returns></returns>
        private string GetToken()
        {
            string Token;

            if (_pikSecurityDatabase.GetKey((int)SecurityKeyEnum.Token) == null)
            {
                Token = TokenCreation();

                if (Token == "Token already created for the customer PARAMOUNT with app Paramount Data Layer")
                {
                    Token = TokenRefresh();
                }
                else
                {
                    _pikSecurityDatabase.SetKey((int)SecurityKeyEnum.Token, Token, "Token");
                }
            }
            else if (_pikSecurityDatabase.GetKeyLastUpdatedDate((int)SecurityKeyEnum.Token) <= DateTime.Now.AddDays(-DataAccessConfigBuilder.Instance.Config.HealthEdgePIKTokenLifeDays))
            {
                Token = TokenRefresh();
            }
            else
            {
                Token = _pikSecurityDatabase.GetKey((int)SecurityKeyEnum.Token);
            }

            if (Token == null)
            {
                throw new Exception("PIK Token Invalid");
            }

            return Token;
        }
    }
}

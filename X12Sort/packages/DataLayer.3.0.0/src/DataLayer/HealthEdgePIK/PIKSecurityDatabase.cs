using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Dapper;

namespace DataLayer.HealthEdgePIK
{
    public class PIKSecurityDatabase:IDisposable
    {
        private readonly IDbConnection _appConfigConnection;

        public PIKSecurityDatabase()
        {
            _appConfigConnection = new SqlConnection(DataAccessConfigBuilder.Instance.Config.AppConfigConnectionString);
        }

        /// <summary>
        /// Gets a key from the HealthEdge PIK key database
        /// </summary>
        /// <param name="PIKSecurityKeyID"></param>
        /// <returns></returns>
        public string GetKey(int PIKSecurityKeyID)
        {
            string SQLQuery = @"
            EXECUTE AS USER = 'intrabit'
            SELECT [das].fn_GetPIKSecurityKey(@PIKSecurityKeyID);
            REVERT;
            ";

            _appConfigConnection.Open();
            string Key = _appConfigConnection.QueryFirstOrDefault<string>(SQLQuery, new { PIKSecurityKeyID });
            _appConfigConnection.Close();

            return Key;
        }

        /// <summary>
        /// Saves a key to the HealthEdge PIK key database
        /// </summary>
        /// <param name="PIKSecurityKeyID"></param>
        /// <param name="Key"></param>
        /// <param name="Name"></param>
        public void SetKey(int PIKSecurityKeyID, string Key, string Name)
        {
            _appConfigConnection.Open();
            _appConfigConnection.QueryFirstOrDefault<string>("[das].sp_SetPIKSecurityKey", new { KeyID = PIKSecurityKeyID, Key, Name }, commandType: CommandType.StoredProcedure);
            _appConfigConnection.Close();
        }

        /// <summary>
        /// Gets a key's last updated date from the HealthEdge PIK key database
        /// </summary>
        /// <param name="PIKSecurityKeyID"></param>
        /// <returns></returns>
        public DateTime GetKeyLastUpdatedDate(int PIKSecurityKeyID)
        {
            string SQLQuery = @"
            SELECT CASE 
                    WHEN UpdatedOn IS NULL
                        THEN CreatedOn
                    ELSE UpdatedON
                    END AS LastUpdatedDate
            FROM [das].[PIKSecurityKeys]
            WHERE PIKSecurityKeyID = @PIKSecurityKeyID;
            ";

            _appConfigConnection.Open();
            DateTime LastUpdatedDate = _appConfigConnection.QueryFirstOrDefault<DateTime>(SQLQuery, new { PIKSecurityKeyID });
            _appConfigConnection.Close();

            return LastUpdatedDate;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    //dispose managed state (managed objects).
                    _appConfigConnection.Dispose();
                }

                //free unmanaged resources (unmanaged objects) and override a finalizer below.
                //set large fields to null.

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
             GC.SuppressFinalize(this);
        }
        #endregion
    }
}

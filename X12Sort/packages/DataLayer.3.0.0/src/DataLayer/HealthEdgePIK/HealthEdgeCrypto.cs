using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace DataLayer.HealthEdgePIK
{
    public class HealthEdgeCrypto
    {
        private byte[] _key;
        private PIKSecurityDatabase _pikSecurityDatabase;

        public HealthEdgeCrypto(PIKSecurityDatabase DataBaseSettings)
        {
            if (DataBaseSettings != null)
            {
                _pikSecurityDatabase = DataBaseSettings;
                _key = HexStringToByteArray(_pikSecurityDatabase.GetKey((int)SecurityKeyEnum.PublicKey));
            }
            else
                throw new Exception("DataBaseSettings cannot be null");
        }

        /// <summary>
        /// Logic to decrypt a HealthEdge JSON response
        /// </summary>
        /// <param name="EncryptedResponse"></param>
        /// <param name="Signature"></param>
        /// <returns></returns>
        public string DecryptResponse(string EncryptedResponse, string Signature)
        {
            byte[] ResponseBytes = Convert.FromBase64String(EncryptedResponse);
            byte[] IV = new byte[16];
            Array.Copy(ResponseBytes, 0, IV, 0, IV.Length);
            byte[] CipherText = new byte[ResponseBytes.Length - 16];
            Array.Copy(ResponseBytes, 16, CipherText, 0, CipherText.Length);

            using AesManaged AesManaged = new AesManaged
            {
                KeySize = 256,
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7,
                Key = _key,
                IV = IV
            };
            
            ICryptoTransform CryptorTransform = AesManaged.CreateDecryptor(AesManaged.Key, AesManaged.IV);
            
            using MemoryStream MemoryStream = new MemoryStream(CipherText);
            using CryptoStream CryptoStream = new CryptoStream(MemoryStream, CryptorTransform, CryptoStreamMode.Read);
            using StreamReader StreamReader = new StreamReader(CryptoStream);
            
            string DecryptedText = StreamReader.ReadToEnd();


            if (!VerifySignature(DecryptedText, Signature))
            {
                Exception Exception = new Exception("RESPONSE SIGNATURE IS NOT VALID!!!");
                throw Exception;
            }

            return DecryptedText;
        }

        /// <summary>
        /// Logic to encrypt a HealthEdge JSON payload
        /// </summary>
        /// <param name="Payload"></param>
        /// <returns></returns>
        public string EncryptPayload(string Payload)
        {
            byte[] PayloadBytes = Encoding.UTF8.GetBytes(Payload);
            byte[] EncryptedPayload;
            byte[] IV = new byte[16];
            Array.Copy(PayloadBytes, 0, IV, 0, IV.Length);

            AesManaged AesManaged = new AesManaged
            {
                KeySize = 256,
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7,
                Key = _key,
                IV = IV
            };

            using (ICryptoTransform CryptoTransform = AesManaged.CreateEncryptor())
            {
                EncryptedPayload = CryptoTransform.TransformFinalBlock(PayloadBytes, 0, PayloadBytes.Length);
            }

            AesManaged.Dispose();

            return Convert.ToBase64String(EncryptedPayload);
        }

        /// <summary>
        /// This will convert a Hex String to a Byte Array (no build in .NET fuction to do this)
        /// </summary>
        /// <param name="StringToConvert"></param>
        /// <returns></returns>
        private static byte[] HexStringToByteArray(string StringToConvert)
        {
            byte[] StringToConvertBytes = new byte[StringToConvert.Length / 2];

            for (int i = 0; i < StringToConvert.Length; i += 2)
            {
                StringToConvertBytes[i / 2] = Convert.ToByte(StringToConvert.Substring(i, 2), 16);
            }

            return StringToConvertBytes;
        }

        /// <summary>
        /// This will verify the signature of a HealthEdge response
        /// </summary>
        /// <param name="PlainTextResult"></param>
        /// <param name="Signature"></param>
        /// <returns></returns>
        private bool VerifySignature(string PlainTextResult, string Signature)
        {
            HMACSHA256 HMACSHA256 = new HMACSHA256();

            byte[] Key = HexStringToByteArray(_pikSecurityDatabase.GetKey((int)SecurityKeyEnum.SignatureKey));
            byte[] PlainTextResultBytes = Encoding.UTF8.GetBytes(PlainTextResult);

            HMACSHA256.Key = Key;
            byte[] ByteHash = HMACSHA256.ComputeHash(PlainTextResultBytes);

            string SignatureFromResult = Convert.ToBase64String(ByteHash);

            HMACSHA256.Dispose();

            return SignatureFromResult == Signature;
        }
    }
}

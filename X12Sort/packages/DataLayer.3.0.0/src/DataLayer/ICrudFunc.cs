﻿namespace DataLayer
{
    /// <summary>
    /// This interface has functions that return the SQL for CRUD operations for this object
    /// </summary>
    public interface ICrudFunc
    {
        /// <summary>
        /// SQL Query to create an object
        /// </summary>
        /// <returns>Returns a parameterized sql string</returns>
        string CreateQuery();

        /// <summary>
        /// SQL Query to read an object
        /// </summary>
        /// <returns>Returns a parameterized sql string</returns>
        string ReadQuery();

        /// <summary>
        /// SQL Query to update an object
        /// </summary>
        /// <returns>Returns a parameterized sql string</returns>
        string UpdateQuery();

        /// <summary>
        /// SQL Query to delete an object
        /// </summary>
        /// <returns>Returns a parameterized sql string</returns>
        string DeleteQuery();
    }
}

using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;


namespace DataLayer.StaticUtilities
{
    public static class Extension
    {
        public static IEnumerable<DataRow> AsEnumerable(this DataTable table)
        {
            if (table != null)
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    yield return table.Rows[i];
                }
        }
    }


    public static class PivotData
    {
        // code and concepts from: https://techbrij.com/pivot-c-array-datatable-convert-column-to-row-linq

        public static string Separator { get; set; }

        /// <summary>
        /// Take a 3 string column separated list and return a pivoted
        /// separated text list.  This assumes a unique value for all 3
        /// columns
        /// </summary>
        /// <param name="text">Source Text</param>
        /// <param name="separator">Character separating the columns</param>
        /// <returns>Data Table from a separated value string</returns>
        public static DataTable SeparatedTextPivotDataTable(string text, string separator)
        {
            // Create the data table
            DataTable table = new DataTable();
            table.Columns.Add(new DataColumn("Index", typeof(string)));
            table.Columns.Add(new DataColumn("TopRowText", typeof(string)));
            table.Columns.Add(new DataColumn("MainData", typeof(string)));

            // Split the source string into rows and fill the table
            if (text != null)
            {
                string[] arrOutput = text.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
                for (int i = 0; i < arrOutput.Length; i++)
                {
                    string line = arrOutput[i].TrimEnd();
                    if (!string.IsNullOrWhiteSpace(line))
                    {
                        string[] rowText = line.Split(new[] { separator }, StringSplitOptions.None);

                        DataRow row = table.NewRow();
                        row[0] = rowText[0];

                        if (rowText.Length > 1)
                            row[1] = rowText[1];
                        else
                            row[1] = string.Empty;

                        if (rowText.Length > 2)
                            row[2] = rowText[2];
                        else
                            row[2] = string.Empty;

                        table.Rows.Add(row);

                    }

                }
            }

            table.AcceptChanges();

            // Convert the data table into dynamic named array
            var data = table.AsEnumerable().Select(x => new
            {
                Index = x["Index"],
                TopRowText = x["TopRowText"],
                MainData = x["MainData"]
            }).OrderBy(x => x.Index);

            // Pivot the above array back into a data table
            DataTable t2 = data.ToPivotTable(
                item => item.TopRowText,
                item => item.Index,
                items => items.Any() ? items.Select(ss => ss.MainData).First() : ""
                );

            // Some secondary functions need a table name to work
            // this just assigns one
            t2.TableName = "PivotTable";

            table.Dispose();

            return t2;
        }

        /// <summary>
        /// Take a 3 string column separated list and return a pivoted
        /// separated text list.  This assumes a unique value for all 3
        /// columns
        /// </summary>
        /// <param name="text">Source Text</param>
        /// <param name="separator">Character separating the columns</param>
        /// <returns>Pivoted string with separated values</returns>
        public static string SeparatedTextPivot(string text, char separator)
        {

            return SeparatedTextPivot(text, separator.ToString(), string.Empty);

        }

        /// <summary>
        /// Take a 3 string column separated list and return a pivoted
        /// separated text list.  This assumes a unique value for all 3
        /// columns
        /// </summary>
        /// <param name="text">Source Text</param>
        /// <param name="separator">String separating the columns</param>
        /// <param name="newSeparator">What string will be used to separate the columns in the returned string.
        /// If one is not supplied then use the separator that split the string</param>
        /// <returns>Pivoted string with separated values</returns>
        public static string SeparatedTextPivot(string text, string separator, string newSeparator = "")
        {
            // Set the separator for joining the string together
            string finalSeparator = String.IsNullOrEmpty(newSeparator) ? separator : newSeparator;

            // Pivot the above array back into a data table
            DataTable t2 = SeparatedTextPivotDataTable(text, separator);

            // Build the string to match the original
            StringBuilder sb = new StringBuilder();
            for (var i = 0; i < t2.Columns.Count; i++)
            {
                sb.Append(t2.Columns[i].ColumnName);
                if (i < t2.Columns.Count - 1)
                    sb.Append(finalSeparator);
            }

            foreach (DataRow line in t2.Rows)
            {

                sb.AppendLine();
                for (var i = 0; i < t2.Columns.Count; i++)
                {
                    sb.Append(line[i]);
                    if (i < t2.Columns.Count - 1)
                        sb.Append(finalSeparator);
                }
            }

            t2.Dispose();

            return sb.ToString();
        }

        /// <summary>
        /// Convert a data table to a List of dictionary objects
        /// </summary>
        /// <param name="dt">Data table</param>
        /// <returns>List&lt;IDictionary&lt;string,object&gt;&gt; object</returns>
        public static List<dynamic> ToDynamicList(this DataTable dt)
        {
            var list = new List<dynamic>();

            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    dynamic dyn = new ExpandoObject();
                    list.Add(dyn);
                    foreach (DataColumn column in dt.Columns)
                    {
                        var dic = (IDictionary<string, object>)dyn;
                        dic[column.ColumnName] = row[column];
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Pivot Array
        /// </summary>
        /// <param name="source"></param>
        /// <param name="columnSelector"></param>
        /// <param name="rowSelector"></param>
        /// <param name="dataSelector"></param>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TColumn"></typeparam>
        /// <typeparam name="TRow"></typeparam>
        /// <typeparam name="TData"></typeparam>
        /// <returns></returns>
        public static dynamic[] ToPivotArray<T, TColumn, TRow, TData>(
            this IEnumerable<T> source,
            Func<T, TColumn> columnSelector,
            Expression<Func<T, TRow>> rowSelector,
            Func<IEnumerable<T>, TData> dataSelector)
        {

            var arr = new List<object>();
            var cols = new List<string>();

            if (rowSelector != null)
            {
                String rowName = ((MemberExpression)rowSelector.Body).Member.Name;
                var columns = source.Select(columnSelector).Distinct();

                cols = (new[] { rowName }).Concat(columns.Select(x => x.ToString())).ToList();


                var rows = source.GroupBy(rowSelector.Compile())
                                 .Select(rowGroup => new
                                 {
                                     Key = rowGroup.Key,
                                     Values = columns.GroupJoin(
                                         rowGroup,
                                         c => c,
                                         r => columnSelector(r),
                                         (c, columnGroup) => dataSelector(columnGroup))
                                 }).ToArray();


                foreach (var row in rows)
                {
                    var items = row.Values.Cast<object>().ToList();
                    items.Insert(0, row.Key);
                    var obj = GetAnonymousObject(cols, items);
                    arr.Add(obj);
                }
            }

            return arr.ToArray();
        }

        /// <summary>
        /// Get the columns of an dymanic array object
        /// </summary>
        /// <param name="columns"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        private static dynamic GetAnonymousObject(IEnumerable<string> columns, IEnumerable<object> values)
        {
            IDictionary<string, object> eo = new ExpandoObject() as IDictionary<string, object>;
            int i;
            for (i = 0; i < columns.Count(); i++)
            {
                eo.Add(columns.ElementAt<string>(i), values.ElementAt<object>(i));
            }
            return eo;
        }

        /// <summary>
        /// Pivot the Dictionary object
        /// </summary>
        /// <param name="source"></param>
        /// <param name="columnSelector"></param>
        /// <param name="rowSelector"></param>
        /// <param name="dataSelector"></param>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TColumn"></typeparam>
        /// <typeparam name="TRow"></typeparam>
        /// <typeparam name="TData"></typeparam>
        /// <returns></returns>
        public static DataTable ToPivotTable<T, TColumn, TRow, TData>(
            this IEnumerable<T> source,
            Func<T, TColumn> columnSelector,
            Expression<Func<T, TRow>> rowSelector,
            Func<IEnumerable<T>, TData> dataSelector)
        {
            DataTable table = new DataTable();

            if (rowSelector != null)
            {
                var rowName = ((MemberExpression)rowSelector.Body).Member.Name;
                table.Columns.Add(new DataColumn(rowName));
                var columns = source.Select(columnSelector).Distinct();

                foreach (var column in columns)
                    table.Columns.Add(new DataColumn(column.ToString()));

                var rows = source.GroupBy(rowSelector.Compile())
                                 .Select(rowGroup => new
                                 {
                                     Key = rowGroup.Key,
                                     Values = columns.GroupJoin(
                                         rowGroup,
                                         c => c,
                                         r => columnSelector(r),
                                         (c, columnGroup) => dataSelector(columnGroup))
                                 });

                foreach (var row in rows)
                {
                    var dataRow = table.NewRow();
                    var items = row.Values.Cast<object>().ToList();
                    items.Insert(0, row.Key);
                    dataRow.ItemArray = items.ToArray();
                    table.Rows.Add(dataRow);
                }
            }

            return table;
        }

    }

}

using DataLayer.Configuration.Models;
using DataLayer.StaticUtilities.Extensions;
using NLog;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data.Common;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace DataLayer.StaticUtilities
{
    /// <summary>
    /// A collection of static functions for working connection strings
    /// </summary>
    public static class ConnectionStrings
    {
        /// <summary>
        /// Logger instance
        /// </summary>
        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Gets a connection string from a XML configuration file.
        /// </summary>
        /// <param name="name">     Connection String Name </param>
        /// <param name="fileName"> Configuration file that is in macchine.config format </param>
        /// <returns>  </returns>
        public static string Get(string name, string fileName)
        {
            name.ThrowIfNullOrEmpty(nameof(name));
            fileName.ThrowIfNullOrEmpty(nameof(fileName));

            logger.Debug($"Looking for Connection String: '{name}' in file: '{fileName}'");

            if (!File.Exists(fileName))
            {
                throw new FileNotFoundException($"Configuration File not Found! File: {fileName}");
            }

            try
            {
                XDocument config = XDocument.Load(fileName);
                XElement node = config.Root.Element("connectionStrings").Elements("add").Single(n => n.Attribute("name").Value == name);
                string cxnString = node.Attribute("connectionString").Value;
                if (string.IsNullOrEmpty(cxnString))
                {
                    throw new InvalidOperationException($"Connection String is Empty!: {name} = '{cxnString}'");
                }
                logger.Debug($"Found '{name}':'{cxnString}'");
                return cxnString;
            }
            catch
            {
                throw new InvalidOperationException($"Configuration for {name} not found in {fileName}.");
            }
        }

        /// <summary>
        /// Converts a connection string to a connection builder object
        /// </summary>
        /// <param name="provider">         The type of connection string </param>
        /// <param name="connectionString"> The full connection string </param>
        /// <returns>  </returns>
        public static DbConnectionStringBuilder CreateBuilder(ProviderType provider, string connectionString)
        {
            connectionString.ThrowIfNullOrEmpty(nameof(connectionString));
            DbConnectionStringBuilder builder = null;
            switch (provider)
            {
                case ProviderType.Sql: builder = new SqlConnectionStringBuilder(connectionString); break;
                case ProviderType.Oracle: builder = new OracleConnectionStringBuilder(connectionString); break;
                default: throw new InvalidOperationException("Connection string provider is not recognized");
            }
            return builder;
        }

        /// <summary>
        /// Creates an empty ConnectionString builder
        /// </summary>
        /// <param name="provider"> The type of connection string </param>
        /// <returns> A new connection string builder  for the given provider initialized with the given connection string</returns>
        /// <example>
        /// <code>
        /// <![CDATA[
        /// DbConnectionStringBuilder cxnStrBuilder = ConnectionStrings.CreateBuilder(ProviderType.Oracle, "Data Source=TESTA;User ID=amiuser1;")
        /// ConfigConnectionString configCxnStr = cxnStrBuilder.ToConfigConnectionString("AmysisTestA", "BILLING");
        /// ]]>
        /// </code>
        /// </example>
        public static DbConnectionStringBuilder CreateBuilder(ProviderType provider)
        {
            DbConnectionStringBuilder builder = null;
            switch (provider)
            {
                case ProviderType.Sql: builder = new SqlConnectionStringBuilder(); break;
                case ProviderType.Oracle: builder = new OracleConnectionStringBuilder(); break;
                default: throw new InvalidOperationException("Connection string provider is not recognized");
            }
            return builder;
        }

        /// <summary>
        /// Get the database provider type
        /// </summary>
        /// <param name="builder"> A connection string builder object </param>
        /// <returns> Returns the provider type, aka what type of database </returns>
        public static ProviderType GetProviderType(this DbConnectionStringBuilder builder)
        {
            builder.ThrowIfNull(nameof(builder));
            if (builder is SqlConnectionStringBuilder)
            {
                return ProviderType.Sql;
            }
            if (builder is OracleConnectionStringBuilder)
            {
                return ProviderType.Oracle;
            }
            throw new InvalidOperationException("Unknown Provider Type");
        }

        /// <summary>
        /// Create a New <c> ConfigConnectionString </c> Object
        /// </summary>
        /// <param name="builder">       A connection string builder that has a connection string </param>
        /// <param name="name">          The name of the <c> ConfigConnectionString </c> Object </param>
        /// <param name="databaseGroup"> The logical database name that this connection string connects to </param>
        /// <returns> A new <c> ConfigConnectionString </c> object built from the connection string builder </returns>
        /// <remarks> The password is dropped if it was given </remarks>
        /// <example>
        /// <code>
        /// <![CDATA[
        /// DbConnectionStringBuilder cxnStrBuilder = ConnectionStrings.CreateBuilder(ProviderType.Oracle, "Data Source=TESTA;User ID=amiuser1;")
        /// ConfigConnectionString configCxnStr = cxnStrBuilder.ToConfigConnectionString("AmysisTestA", "BILLING");
        /// ]]>
        /// </code>
        /// </example>
        public static ConfigConnectionString ToConfigConnectionString(this DbConnectionStringBuilder builder, string name, string databaseGroup)
        {
            builder.ThrowIfNull(nameof(builder));
            name.ThrowIfNull(nameof(name));
            databaseGroup.ThrowIfNullOrEmpty(nameof(databaseGroup));

            ConfigConnectionString cxnStrObj = new ConfigConnectionString()
            {
                Name = name.Sanitize(),
                DatabaseGroup = databaseGroup.Sanitize().ToUpper(),
                Provider = builder.GetProviderType()
            };
            DbConnectionStringBuilder additional = CreateBuilder(cxnStrObj.Provider);

            foreach (string key in builder.Keys)
            {
                switch (key.ToLower())
                {
                    case "initial catalog": cxnStrObj.InitialCatalog = builder[key].ToString(); break;
                    case "integrated security": cxnStrObj.IntegratedSecurity = Convert.ToBoolean(builder[key]); break;
                    case "data source": cxnStrObj.DataSource = builder[key].ToString(); break;
                    case "user id": cxnStrObj.UserID = builder[key].ToString(); break;
                    case "password": break;
                    default:
                        if (!string.IsNullOrEmpty(builder[key].ToString()) && additional[key].ToString() != builder[key].ToString())
                        {
                            additional[key] = builder[key];
                        }
                        break;
                }
            }

            cxnStrObj.Additional = additional.ConnectionString;
            return cxnStrObj;
        }
    }
}

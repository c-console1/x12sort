using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace DataLayer.StaticUtilities
{
    /// <summary>
    /// Make running basic queries on XML strings
    /// </summary>
    public static class XMLQueries
    {
        /// <summary>
        /// Find the targeted parent/grandparent node of a supplied node
        /// </summary>
        /// <param name="childNode">Child node to look for</param>
        /// <param name="tagParentName">Local name of the parent node</param>
        /// <returns></returns>
        public static XmlNode XMLFindChildNodeFirst(XmlNode parentNode, string tagChildName)
        {
            // File the target parent node
            XmlNodeList childNodes = parentNode.ChildNodes;

            foreach (XmlNode subChildNode in childNodes)
            {
                if (subChildNode.LocalName == tagChildName)
                    return subChildNode;

                return XMLFindChildNodeFirst(subChildNode, tagChildName);
            }

            return null;

        }


        /// <summary>
        /// Get all the tags that match a tag's name
        /// </summary>
        /// <param name="xml">Source XML string</param>
        /// <returns></returns>
        public static XmlNodeList XMLFindTagsString(string xml, string tagName)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            // Load all empty nodes into a list
            XmlNodeList nodeMatch = doc.SelectNodes(".//" + tagName);
            return nodeMatch;

        }

        /// <summary>
        /// Find empty tags within a XML string
        /// </summary>
        /// <param name="xml">XML to parse</param>
        /// <param name="tagEmpty">Target tag to search for</param>
        /// <param name="tagParent">Parent tag of the target tag</param>
        /// <returns></returns>
        public static string XMLFindEmptyTagsString(string xml, string tagEmpty, string tagParent)
        {
            // Load all empty nodes into a list
            XmlNodeList nodeMatch = XMLFindTagsString(xml, tagEmpty);

            StringBuilder sb = new StringBuilder();

            // loop through all the matching nodes
            foreach (XmlNode indNode in nodeMatch)
            {
                if (indNode.InnerText.Trim().Length == 0)
                {
                    // File the target parent node
                    XmlNode parentNode = indNode.ParentNode;
                    while (parentNode.Name != "#document" && parentNode.LocalName != tagParent)
                    {
                        parentNode = parentNode.ParentNode;
                    }

                    sb.AppendLine(XMLSerialization.PrintXML(parentNode.OuterXml));

                }

            }

            return sb.ToString();

        }

        /// <summary>
        /// Find a parent node by name
        /// </summary>
        /// <param name="source">Source node of a larger XML document</param>
        /// <param name="parentName">Target parent node name to look for</param>
        /// <returns>XML node that matches the target name</returns>
        public static XmlNode XmlFindParent(XmlNode source, string parentName)
        {
            XmlNode output = source;
            while (output.LocalName != "#document" && output.LocalName != parentName)
            {
                output = output.ParentNode;
            }

            return output;
        }

        /// <summary>
        /// Find the first child node that matches the name 
        /// </summary>
        /// <param name="source">Source node of a larger XML document</param>
        /// <param name="childName">Target child node to find</param>
        /// <returns></returns>
        public static XmlNode XmlFindChild(XmlNode source, string childName)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(source.OuterXml);
            XmlNodeList nodeMatch = doc.SelectNodes(".//" + childName);

            if (nodeMatch == null)
                return null;

            if (nodeMatch.Count == 0)
                return null;

            return nodeMatch[0];
        }

        /// <summary>
        /// Find the many child nodes that matches the name from XML string
        /// </summary>
        /// <param name="source">XML source string</param>
        /// <param name="childName">Target child tag</param>
        /// <returns>XmlNodeList of child node found</returns>
        public static XmlNodeList XmlFindChildManyString(string source, string childName)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(source);
            XmlNodeList nodeMatch = doc.SelectNodes(".//" + childName);

            if (nodeMatch == null)
                return null;

            return nodeMatch;

        }

        /// <summary>
        /// Find the first child node that matches the name 
        /// </summary>
        /// <param name="source">XML Node object</param>
        /// <param name="childName">Target child tag</param>
        /// <returns>XmlNodeList</returns>
        public static XmlNodeList XmlFindChildMany(XmlNode source, string childName)
        {
            return XmlFindChildManyString(source.OuterXml, childName);
        }

        /// <summary>
        /// Find a a childe node by thier local name (ignores namespace prefixes)
        /// </summary>
        /// <param name="source"></param>
        /// <param name="childName"></param>
        /// <returns></returns>
        public static XmlNodeList XmlFindChilLocal(XmlNode source, string childName)
        {
            return XmlFindChildManyString(source.OuterXml, childName);
        }

        /// <summary>
        /// Simulate XPath queries to get around issues with SOAP namespaces
        /// </summary>
        /// <param name="doc">XmlDocument document</param>
        /// <param name="caseSensetive">Is this case sentive on tag names?</param>
        /// <param name="branchNames">Individual branch</param>
        /// <returns></returns>
        public static List<XmlNode> XmlFindMockXpath(XmlDocument doc, bool caseSensetive, params string[] branchNames)
        {

            List<XmlNode> output = new List<XmlNode>();
            output.Add(doc.ChildNodes[0].ParentNode);

            foreach (string item in branchNames)
            {
                List<XmlNode> tempOutput = new List<XmlNode>();

                // Go through the parenet node(s)
                foreach (XmlNode currentNode in output)
                {
                    // Go through the child node to find match
                    foreach (XmlNode childNode in currentNode.ChildNodes)
                    {
                        // Case sensetive match found, add children
                        if (caseSensetive && childNode.LocalName == item)
                            tempOutput.Add(childNode);

                        // Case insensetive match found, add children
                        if (!caseSensetive && childNode.LocalName.ToUpper() == item.ToUpper())
                            tempOutput.Add(childNode);
                    }
                }

                // Reset the output values
                output = tempOutput;
            }

            return output;
        }


        /// <summary>
        /// Simulate XPath queries to get around issues with SOAP namespaces
        /// </summary>
        /// <param name="xml">XML string</param>
        /// <param name="caseSensetive">Is this case sentive on tag names?</param>
        /// <param name="branchNames">Individual branch</param>
        /// <returns></returns>
        public static List<XmlNode> XmlFindMockXpath(string xml, bool caseSensetive, params string[] branchNames)
        {

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            return XmlFindMockXpath(doc, caseSensetive, branchNames);
        }

    }


}

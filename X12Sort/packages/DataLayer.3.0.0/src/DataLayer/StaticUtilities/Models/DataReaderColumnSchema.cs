using System;

namespace DataLayer.StaticUtilities.Models
{
    /// <summary>
    /// A class that repersents a data reader column's schemas
    /// </summary>
    /// <remarks>
    /// Values takesn from here: https://docs.microsoft.com/en-us/dotnet/api/system.data.datatablereader.getschematable?view=netframework-4.8
    /// </remarks>
    public class DataReaderColumnSchema : IEquatable<DataReaderColumnSchema>
    {
        /// <summary>
        /// The name of the column as it appears in the DataTable.
        /// </summary>
        public string ColumnName { get; set; }

        /// <summary>
        ///  The ordinal of the column
        /// </summary>
        public int ColumnOrdinal { get; set; }

        /// <summary>
        /// -1 if the ColumnSize (or MaxLength) property of the DataColumn cannot be determined or is not relevant;
        /// otherwise, 0 or a positive integer that contains the MaxLength value.
        /// </summary>
        public int ColumnSize { get; set; }

        /// <summary>
        /// The underlying type of the column.
        /// </summary>
        public Type DataType { get; set; }

        /// <summary>
        /// true if the AllowDbNull constraint is set to true for the column; otherwise, false.
        /// </summary>
        public bool AllowDBNull { get; set; }

        /// <summary>
        /// Pretty printing of the object
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string nullStr = AllowDBNull ? "'NULL'" : "";
            return $"{ColumnName} [{DataType}({ColumnSize})] {nullStr }";
        }

        /// <summary>
        /// Not equals comparison operator
        /// </summary>
        /// <param name="obj1"></param>
        /// <param name="obj2"></param>
        /// <returns></returns>
        public static bool operator !=(DataReaderColumnSchema obj1, DataReaderColumnSchema obj2)
        {
            return !(obj1 == obj2);
        }

        /// <summary>
        /// Equals comparison operator
        /// </summary>
        /// <param name="obj1"></param>
        /// <param name="obj2"></param>
        /// <returns></returns>
        public static bool operator ==(DataReaderColumnSchema obj1, DataReaderColumnSchema obj2)
        {
            if (ReferenceEquals(obj1, obj2))
            {
                return true;
            }

            if (ReferenceEquals(obj1, null) || ReferenceEquals(obj2, null))
            {
                return false;
            }

            bool columnNameMatch = (obj1.ColumnName ?? "").ToLower() == (obj2.ColumnName ?? "").ToLower();
            bool columnOrdinalMatch = obj1.ColumnOrdinal == obj2.ColumnOrdinal;
            bool columnSizeMatch = obj1.ColumnSize == obj2.ColumnSize;
            bool dataTypeMatch = obj1.DataType == obj2.DataType;
            bool allowDBNullMatch = obj1.AllowDBNull == obj2.AllowDBNull;

            return columnNameMatch && columnOrdinalMatch && columnSizeMatch && dataTypeMatch && allowDBNullMatch;
        }

        /// <summary>
        /// Compares two Schema obeject to see if they are the same
        /// </summary>
        /// <param name="other">The other schema object</param>
        /// <returns></returns>
        public bool Equals(DataReaderColumnSchema other)
        {
             return (this == other);
        }

        /// <summary>
        /// Compares two Schema obeject to see if they are the same
        /// </summary>
        /// <param name="other">The other schema object</param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is DataReaderColumnSchema))
                return false;

            DataReaderColumnSchema other = obj as DataReaderColumnSchema;

            return (this == other);
        }

        /// <summary>
        /// Gets the hash code for the current object
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            //get hash code theory: https://blogs.msdn.microsoft.com/ericlippert/2011/02/28/guidelines-and-rules-for-gethashcode/
            //implementation idea borrowed from: https://rehansaeed.com/gethashcode-made-easy/
            unchecked
            {
                int hashCode = 17;
                hashCode = (hashCode * 23) + (ColumnName == null ? 0 : ColumnName.GetHashCode());
                hashCode = (hashCode * 23) + (ColumnOrdinal.GetHashCode());
                hashCode = (hashCode * 23) + (ColumnSize.GetHashCode());
                hashCode = (hashCode * 23) + (DataType == null ? 0 : DataType.GetHashCode());
                hashCode = (hashCode * 23) + (AllowDBNull.GetHashCode());

                return hashCode;
            }
        }
    }
}

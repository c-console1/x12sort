using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.StaticUtilities.Models
{
    /// <summary>
    /// Object that hold a zip state name and range
    /// of zip codes
    /// </summary>
    public class StateFromZipcode
    {

        /// <summary>
        /// Initialize object
        /// </summary>
        /// <param name="state2Letter"></param>
        /// <param name="stateLong"></param>
        /// <param name="zipLow"></param>
        /// <param name="zipHigh"></param>
        public StateFromZipcode(
            string state2Letter, string stateLong, string zipLow, string zipHigh)
        {
            State2Letter = state2Letter;
            StateLong = stateLong;
            ZipLow = int.Parse(zipLow);
            ZipHigh = int.Parse(zipHigh);
        }

        /// <summary>
        /// 2 letter state code
        /// </summary>
        public string State2Letter { get; protected set; }

        /// <summary>
        /// State name
        /// </summary>
        public string StateLong { get; protected set; }

        /// <summary>
        /// Lowest zip code in range
        /// </summary>
        public int ZipLow { get; protected set; }

        /// <summary>
        /// Highest zip code by range
        /// </summary>
        public int ZipHigh { get; protected set; }

    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.StaticUtilities.Models
{

    /// <summary>
    /// Country and State information
    /// </summary>
    public class CountryStateAbbr
    {
        /// <summary>
        /// Instantiate object
        /// </summary>
        /// <param name="countryCode">2 letter coutry code</param>
        /// <param name="stateAbbr">2 letter state code</param>
        /// <param name="stateName">Verbose state name</param>
        /// <param name="isTerritory">Is this a territory</param>
        /// <param name="altStateName">Is this a territory</param>
        public CountryStateAbbr(string countryCode, string stateAbbr, string stateName, bool isTerritory,
            List<string> altStateName)
        {
            // Set Country information
            DataLayer.Base.Models.Country country = DataLayer.Base.Models.Country.FromString(countryCode);
            CountryName = country.Name;
            CountryAbbr = country.Code;
            CountryAbbr3 = country.Code3;

            // Set State information
            StateAbbr = stateAbbr;
            StateName = stateName;
            IsTerritory = isTerritory;
            AltStateName = altStateName;
        }

        /// <summary>
        /// Instantiate object
        /// </summary>
        /// <param name="countryCode">2 letter coutry code</param>
        /// <param name="stateAbbr">2 letter state code</param>
        /// <param name="stateName">Verbose state name</param>
        /// <param name="isTerritory">Is this a territory</param>
        public CountryStateAbbr(string countryCode, string stateAbbr, string stateName, bool isTerritory)
        {
            // Set Country information
            DataLayer.Base.Models.Country country = DataLayer.Base.Models.Country.FromString(countryCode);
            CountryName = country.Name;
            CountryAbbr = country.Code;
            CountryAbbr3 = country.Code3;

            // Set State information
            StateAbbr = stateAbbr;
            StateName = stateName;
            IsTerritory = isTerritory;
            AltStateName = new List<string>();
        }

        /// <summary>
        /// Country name
        /// </summary>
        public string CountryName { get; protected set; }

        /// <summary>
        /// 2 letter abbreviation
        /// </summary>
        public string CountryAbbr { get; protected set; }

        /// <summary>
        /// 3 letter abbreviation
        /// </summary>
        public string CountryAbbr3 { get; protected set; }

        /// <summary>
        /// 2 letter state abbreviation
        /// </summary>
        public string StateAbbr { get; protected set; }

        /// <summary>
        /// State full name
        /// </summary>
        public string StateName { get; protected set; }

        /// <summary>
        /// Alternative state name / spelling
        /// </summary>
        public List<string> AltStateName { get; protected set; }

        /// <summary>
        /// Is this state / providence a Territory?
        /// </summary>
        public bool IsTerritory { get; protected set; }

    }
}

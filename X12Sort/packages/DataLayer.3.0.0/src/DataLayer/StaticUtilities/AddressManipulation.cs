using DataLayer.Base.Models;
using DataLayer.StaticUtilities.Models;
using System;
using System.Linq;
using System.Collections.Generic;
using USAddress;

namespace DataLayer.StaticUtilities
{
    public static class AddressManipulation
    {
        /// <summary>
        /// List of zip code ranges and the states
        /// associated with them
        /// </summary>
        internal static List<StateFromZipcode> stateFromZipcodes;

        /// <summary>
        /// Address parser object
        /// </summary>
        private static AddressParser _parser;

        /// <summary>
        /// Address parser
        /// </summary>
        internal static AddressParser Parser
        {
            get
            {
                if (_parser == null)
                {
                    _parser = new AddressParser();
                }

                return _parser;
            }
        }

        /// <summary>
        /// List of queryable object by Country and State
        /// </summary>
        /// <param name="includeUSStates">True, include US states</param>
        /// <param name="includeUSTerritories">True, include US territories</param>
        /// <param name="includeCanada">True, include Canadian provinces</param>
        /// <returns>List of state and conutry information</returns>
        public static List<Models.CountryStateAbbr> CountryStates(bool includeUSStates, bool includeUSTerritories, bool includeCanada)
        {
            List<Models.CountryStateAbbr> output = new List<Models.CountryStateAbbr>();

            // US states that NOT territories
            if (includeUSStates)
            {
                output.Add(new Models.CountryStateAbbr("US", "AL", "Alabama", false));
                output.Add(new Models.CountryStateAbbr("US", "AK", "Alaska", false));
                output.Add(new Models.CountryStateAbbr("US", "AZ", "Arizona", false));
                output.Add(new Models.CountryStateAbbr("US", "AR", "Arkansas", false));
                output.Add(new Models.CountryStateAbbr("US", "CA", "California", false));
                output.Add(new Models.CountryStateAbbr("US", "CO", "Colorado", false));
                output.Add(new Models.CountryStateAbbr("US", "CT", "Connecticut", false));
                output.Add(new Models.CountryStateAbbr("US", "DE", "Delaware", false));
                output.Add(new Models.CountryStateAbbr("US", "FL", "Florida", false));
                output.Add(new Models.CountryStateAbbr("US", "GA", "Georgia", false));
                output.Add(new Models.CountryStateAbbr("US", "HI", "Hawaii", false));
                output.Add(new Models.CountryStateAbbr("US", "ID", "Idaho", false));
                output.Add(new Models.CountryStateAbbr("US", "IL", "Illinois", false));
                output.Add(new Models.CountryStateAbbr("US", "IN", "Indiana", false));
                output.Add(new Models.CountryStateAbbr("US", "IA", "Iowa", false));
                output.Add(new Models.CountryStateAbbr("US", "KS", "Kansas", false));
                output.Add(new Models.CountryStateAbbr("US", "KY", "Kentucky", false));
                output.Add(new Models.CountryStateAbbr("US", "LA", "Louisiana", false));
                output.Add(new Models.CountryStateAbbr("US", "ME", "Maine", false));
                output.Add(new Models.CountryStateAbbr("US", "MD", "Maryland", false));
                output.Add(new Models.CountryStateAbbr("US", "MA", "Massachusetts", false));
                output.Add(new Models.CountryStateAbbr("US", "MI", "Michigan", false));
                output.Add(new Models.CountryStateAbbr("US", "MN", "Minnesota", false));
                output.Add(new Models.CountryStateAbbr("US", "MS", "Mississippi", false));
                output.Add(new Models.CountryStateAbbr("US", "MO", "Missouri", false));
                output.Add(new Models.CountryStateAbbr("US", "MT", "Montana", false));
                output.Add(new Models.CountryStateAbbr("US", "NE", "Nebraska", false));
                output.Add(new Models.CountryStateAbbr("US", "NV", "Nevada", false));
                output.Add(new Models.CountryStateAbbr("US", "NH", "New Hampshire", false));
                output.Add(new Models.CountryStateAbbr("US", "NJ", "New Jersey", false));
                output.Add(new Models.CountryStateAbbr("US", "NM", "New Mexico", false));
                output.Add(new Models.CountryStateAbbr("US", "NY", "New York", false));
                output.Add(new Models.CountryStateAbbr("US", "NC", "North Carolina", false));
                output.Add(new Models.CountryStateAbbr("US", "ND", "North Dakota", false));
                output.Add(new Models.CountryStateAbbr("US", "OH", "Ohio", false));
                output.Add(new Models.CountryStateAbbr("US", "OK", "Oklahoma", false));
                output.Add(new Models.CountryStateAbbr("US", "OR", "Oregon", false));
                output.Add(new Models.CountryStateAbbr("US", "PA", "Pennsylvania", false));
                output.Add(new Models.CountryStateAbbr("US", "RI", "Rhode Island", false));
                output.Add(new Models.CountryStateAbbr("US", "SC", "South Carolina", false));
                output.Add(new Models.CountryStateAbbr("US", "SD", "South Dakota", false));
                output.Add(new Models.CountryStateAbbr("US", "TN", "Tennessee", false));
                output.Add(new Models.CountryStateAbbr("US", "TX", "Texas", false));
                output.Add(new Models.CountryStateAbbr("US", "UT", "Utah", false));
                output.Add(new Models.CountryStateAbbr("US", "VT", "Vermont", false));
                output.Add(new Models.CountryStateAbbr("US", "VA", "Virginia", false));
                output.Add(new Models.CountryStateAbbr("US", "WA", "Washington", false));
                output.Add(new Models.CountryStateAbbr("US", "WV", "West Virginia", false));
                output.Add(new Models.CountryStateAbbr("US", "WI", "Wisconsin", false));
                output.Add(new Models.CountryStateAbbr("US", "WY", "Wyoming", false));
            }

            // US territories
            if (includeUSTerritories)
            {
                output.Add(new Models.CountryStateAbbr("US", "AS", "American Samoa", true,
                    new List<string>() { "Amerika Sāmoa", "Amelika Sāmoa", "Sāmoa Amelika" }));
                output.Add(new Models.CountryStateAbbr("US", "GU", "Guam", true,
                    new List<string>() { "Guåhån" }));
                output.Add(new Models.CountryStateAbbr("US", "MP", "Northern Mariana Islands", true,
                    new List<string>() { "Commonwealth of the Northern Mariana Islands", "Chamorro",
                    "Sankattan Siha Na Islas Mariånas"}));
                output.Add(new Models.CountryStateAbbr("US", "PR", "Puerto Rico", true,
                    new List<string>() { "Commonwealth of Puerto Rico", "Estado Libre Asociado de Puerto Rico",
                    "Free Associated State of Puerto Rico"}));
                output.Add(new Models.CountryStateAbbr("US", "VI", "Virgin Islands", true,
                    new List<string>() { "Islas Vírgenes" }));
                output.Add(new Models.CountryStateAbbr("US", "DC", "Washington, D.C.", true,
                    new List<string>() { "District of Columbia", "D.C., Washington", "Washington, DC",
                    "Washington DC"}));
            }

            // Canadian provinces
            if (includeCanada)
            {
                output.Add(new Models.CountryStateAbbr("CA", "AB", "Alberta", false));
                output.Add(new Models.CountryStateAbbr("CA", "BC", "British Columbia", false));
                output.Add(new Models.CountryStateAbbr("CA", "MB", "Manitoba", false));
                output.Add(new Models.CountryStateAbbr("CA", "NB", "New Brunswick", false));
                output.Add(new Models.CountryStateAbbr("CA", "NL", "Newfoundland and Labrador", false,
                    new List<string>() { "Newfoundland", "Labrador" }));
                output.Add(new Models.CountryStateAbbr("CA", "NT", "Northwest Territories", false));
                output.Add(new Models.CountryStateAbbr("CA", "NS", "Nova Scotia", false));
                output.Add(new Models.CountryStateAbbr("CA", "NU", "Nunavut", false));
                output.Add(new Models.CountryStateAbbr("CA", "ON", "Ontario", false));
                output.Add(new Models.CountryStateAbbr("CA", "PE", "Prince Edward Island", false));
                output.Add(new Models.CountryStateAbbr("CA", "QC", "Quebec", false,
                    new List<string>() { "Québec" }));
                output.Add(new Models.CountryStateAbbr("CA", "SK", "Saskatchewan", false));
                output.Add(new Models.CountryStateAbbr("CA", "YT", "Yukon", false));
            }

            return output;

        }

        /// <summary>
        /// Parses an address and attempts to format to a standard format.
        /// "1901 indian wood Circle Maumee oh 43537" -> "1901 Indian Wood Cir Maumee, OH 43537"
        /// </summary>
        /// <param name="input">Address to format</param>
        /// <remarks>
        /// Code consuming the results of this funciton will need AddressParser by Jim Counts from NuGet
        /// https://github.com/jamesrcounts/usaddress
        /// </remarks>
        /// <returns></returns>
        public static AddressParseResult Parse(string input)
        {
            AddressParseResult parserResult = _parser.ParseAddress(input);
            return parserResult;
        }

        /// <summary>
        /// Converts a two character abbreviated state to full length. Case insensitive.
        /// OH -> Ohio
        /// </summary>
        /// <param name="abbreviation">State to expand</param>
        /// <param name="includeUSStates">Include US states, defualt yes</param>
        /// <param name="includeUSTerritories">Include US territories", defualt no</param>
        /// <param name="includeCanada">include Canadian provinces, defualt yes</param>
        /// <returns>Verbose state names</returns>
        public static string StateAbbreviationToVerbose(string abbreviation,
            bool includeUSStates = true, bool includeUSTerritories = false, bool includeCanada = true)
        {
            // Empty query
            if (string.IsNullOrWhiteSpace(abbreviation))
                return "";

            // Load state list
            List<Models.CountryStateAbbr> states =
                CountryStates(includeUSStates, includeUSTerritories, includeCanada);

            // Query state list
            Models.CountryStateAbbr indState = states.Where(
                x => x.StateAbbr.ToUpper() == abbreviation.ToUpper().Trim()
                ).FirstOrDefault();

            // No results, was a verbose name passed instead?
            // Query for standard state name
            if (indState == null)
                indState = states.Where(
                x => x.StateName.ToUpper() == abbreviation.ToUpper().Trim()
                ).FirstOrDefault();

            // No results, was a alternate verbose name passed instead?
            // Query for alternate standard state name
            // No results, try alternative state names
            if (indState == null)
            {
                indState = states.Where(x => x.AltStateName.Any(
                    y => y.ToUpper().Trim() == abbreviation.ToUpper().Trim()
                    )).FirstOrDefault();
            }

            // No results, return blank name
            if (indState == null)
                return "";

            // Value found, return results
            return indState.StateName;
        }

        /// <summary>
        /// Converts a full length state state to its two character abbreviation. Case insensitive.
        /// Ohio -> OH
        /// </summary>
        /// <param name="verbose">State to abbreviate</param>
        /// <param name="includeUSStates">Include US states, defualt yes</param>
        /// <param name="includeUSTerritories">Include US territories", defualt no</param>
        /// <param name="includeCanada">include Canadian provinces, defualt yes</param>
        /// <returns></returns>
        public static string StateVerboseToAbbreviation(string verbose,
            bool includeUSStates = true, bool includeUSTerritories = false, bool includeCanada = true)
        {
            // Empty query
            if (string.IsNullOrWhiteSpace(verbose))
                return "";

            // Load state list
            List<Models.CountryStateAbbr> states =
                CountryStates(includeUSStates, includeUSTerritories,
                includeCanada);

            // Query state list
            Models.CountryStateAbbr indState = states.Where(
                x => x.StateName.ToUpper() == verbose.ToUpper().Trim()
                ).FirstOrDefault();

            // No results, try alternative state names
            if (indState == null)
            {
                indState = states.Where(x => x.AltStateName.Any(
                    y => y.ToUpper().Trim() == verbose.ToUpper().Trim()
                    )).FirstOrDefault();
            }

            // Was the abbreviation passed by accident?
            // No results, try abbreviation
            if (indState == null)
                indState = states.Where(
                    x => x.StateAbbr.ToUpper() == verbose.ToUpper().Trim()
                    ).FirstOrDefault();

            // No results, return blank
            if (indState == null)
                return "";

            // Value found, return results
            return indState.StateAbbr;
        }

        /// <summary>
        /// Is valid abbreviation
        /// </summary>
        /// <param name="abbreviation">Abbreviate to evaluate</param>
        /// <param name="includeUSStates">Include US states, defualt yes</param>
        /// <param name="includeUSTerritories">Include US territories", defualt no</param>
        /// <param name="includeCanada">include Canadian provinces, defualt yes</param>
        /// <returns></returns>
        public static bool IsValidAbbreviation(string abbreviation,
            bool includeUSStates = true, bool includeUSTerritories = false, bool includeCanada = true)
        {
            // No possible matches
            if (!includeUSStates && !includeUSTerritories && !includeCanada)
                return false;

            // Empty query
            if (string.IsNullOrWhiteSpace(abbreviation))
                return false;

            // Load state list
            List<Models.CountryStateAbbr> states =
                CountryStates(includeUSStates, includeUSTerritories, includeCanada);

            // Clean up test value
            abbreviation = abbreviation.Trim().ToUpper();

            // Loop through returned values            
            foreach (Models.CountryStateAbbr item in states) {
                if (item.StateAbbr == abbreviation)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Get list of possible states for zip code
        /// </summary>
        /// <param name="zipCodeString">Zip code</param>
        /// <returns>list of possible states</returns>
        public static IEnumerable<StateFromZipcode> FindStateByZipCode(
            string zipCodeString)
        {
            // Empty value
            if (string.IsNullOrWhiteSpace(zipCodeString))
                throw new ArgumentNullException(zipCodeString);

            // Invalid int
            int zipcode;
            if(int.TryParse(zipCodeString, out zipcode))
                throw new ArgumentException(zipCodeString);

            FillstateFromZipcodes();

            return stateFromZipcodes.Where(x =>
                zipcode >= x.ZipLow && zipcode <= x.ZipHigh);
        }

        /// <summary>
        /// Get list of possible states for zip code
        /// </summary>
        /// <param name="zipcode">Zip code</param>
        /// <returns>list of possible states</returns>
        public static IEnumerable<StateFromZipcode> FindStateByZipCode(
            int zipcode)
        {
            FillstateFromZipcodes();
            return stateFromZipcodes.Where(x =>
                zipcode >= x.ZipLow && zipcode <= x.ZipHigh);
        }

        /// <summary>
        /// One time fill of 'stateFromZipcodes' object
        /// </summary>
        private static void FillstateFromZipcodes()
        {
            if (stateFromZipcodes == null)
            {
                stateFromZipcodes = new List<StateFromZipcode>();
                stateFromZipcodes.Add(new StateFromZipcode("AL", "Alabama", "35004", "36925"));
                stateFromZipcodes.Add(new StateFromZipcode("AK", "Alaska", "99501", "99950"));
                stateFromZipcodes.Add(new StateFromZipcode("AZ", "Arizona", "85001", "86556"));
                stateFromZipcodes.Add(new StateFromZipcode("AR", "Arkansas", "71601", "72959"));
                stateFromZipcodes.Add(new StateFromZipcode("AR", "Arkansas", "75502", "75502"));
                stateFromZipcodes.Add(new StateFromZipcode("CA", "California", "90001", "96162"));
                stateFromZipcodes.Add(new StateFromZipcode("CO", "Colorado", "80001", "81658"));
                stateFromZipcodes.Add(new StateFromZipcode("CT", "Connecticut", "6001", "6389"));
                stateFromZipcodes.Add(new StateFromZipcode("CT", "Connecticut", "6401", "6928"));
                stateFromZipcodes.Add(new StateFromZipcode("DE", "Delaware", "19701", "19980"));
                stateFromZipcodes.Add(new StateFromZipcode("DC", "District of Columbia", "20001", "20039"));
                stateFromZipcodes.Add(new StateFromZipcode("DC", "District of Columbia", "20042", "20599"));
                stateFromZipcodes.Add(new StateFromZipcode("DC", "District of Columbia", "20799", "20799"));
                stateFromZipcodes.Add(new StateFromZipcode("FL", "Florida", "32004", "34997"));
                stateFromZipcodes.Add(new StateFromZipcode("GA", "Georgia", "30001", "31999"));
                stateFromZipcodes.Add(new StateFromZipcode("GA", "Georgia", "39901", "39901"));
                stateFromZipcodes.Add(new StateFromZipcode("HI", "Hawaii", "96701", "96898"));
                stateFromZipcodes.Add(new StateFromZipcode("ID", "Idaho", "83201", "83876"));
                stateFromZipcodes.Add(new StateFromZipcode("IL", "Illinois", "60001", "62999"));
                stateFromZipcodes.Add(new StateFromZipcode("IN", "Indiana", "46001", "47997"));
                stateFromZipcodes.Add(new StateFromZipcode("IA", "Iowa", "50001", "52809"));
                stateFromZipcodes.Add(new StateFromZipcode("IA", "Iowa", "68119", "68120"));
                stateFromZipcodes.Add(new StateFromZipcode("KS", "Kansas", "66002", "67954"));
                stateFromZipcodes.Add(new StateFromZipcode("KY", "Kentucky", "40003", "42788"));
                stateFromZipcodes.Add(new StateFromZipcode("LA", "Louisiana", "70001", "71232"));
                stateFromZipcodes.Add(new StateFromZipcode("LA", "Louisiana", "71234", "71497"));
                stateFromZipcodes.Add(new StateFromZipcode("ME", "Maine", "3901", "4992"));
                stateFromZipcodes.Add(new StateFromZipcode("MD", "Maryland", "20331", "20331"));
                stateFromZipcodes.Add(new StateFromZipcode("MD", "Maryland", "20335", "20797"));
                stateFromZipcodes.Add(new StateFromZipcode("MD", "Maryland", "20812", "21930"));
                stateFromZipcodes.Add(new StateFromZipcode("MA", "Massachusetts", "1001", "2791"));
                stateFromZipcodes.Add(new StateFromZipcode("MA", "Massachusetts", "5501", "5544"));
                stateFromZipcodes.Add(new StateFromZipcode("MI", "Michigan", "48001", "49971"));
                stateFromZipcodes.Add(new StateFromZipcode("MN", "Minnesota", "55001", "56763"));
                stateFromZipcodes.Add(new StateFromZipcode("MS", "Mississippi", "38601", "39776"));
                stateFromZipcodes.Add(new StateFromZipcode("MS", "Mississippi", "71233", "71233"));
                stateFromZipcodes.Add(new StateFromZipcode("MO", "Missouri", "63001", "65899"));
                stateFromZipcodes.Add(new StateFromZipcode("MT", "Montana", "59001", "59937"));
                stateFromZipcodes.Add(new StateFromZipcode("NE", "Nebraska", "68001", "68118"));
                stateFromZipcodes.Add(new StateFromZipcode("NE", "Nebraska", "68122", "69367"));
                stateFromZipcodes.Add(new StateFromZipcode("NV", "Nevada", "88901", "89883"));
                stateFromZipcodes.Add(new StateFromZipcode("NH", "New Hampshire", "3031", "3897"));
                stateFromZipcodes.Add(new StateFromZipcode("NJ", "New Jersey", "7001", "8989"));
                stateFromZipcodes.Add(new StateFromZipcode("NM", "New Mexico", "87001", "88441"));
                stateFromZipcodes.Add(new StateFromZipcode("NY", "New York", "6390", "6390"));
                stateFromZipcodes.Add(new StateFromZipcode("NY", "New York", "10001", "14975"));
                stateFromZipcodes.Add(new StateFromZipcode("NC", "North Carolina", "27006", "28909"));
                stateFromZipcodes.Add(new StateFromZipcode("ND", "North Dakota", "58001", "58856"));
                stateFromZipcodes.Add(new StateFromZipcode("OH", "Ohio", "43001", "45999"));
                stateFromZipcodes.Add(new StateFromZipcode("OK", "Oklahoma", "73001", "73199"));
                stateFromZipcodes.Add(new StateFromZipcode("OK", "Oklahoma", "73401", "74966"));
                stateFromZipcodes.Add(new StateFromZipcode("OR", "Oregon", "97001", "97920"));
                stateFromZipcodes.Add(new StateFromZipcode("PA", "Pennsylvania", "15001", "19640"));
                stateFromZipcodes.Add(new StateFromZipcode("RI", "Rhode Island", "2801", "2940"));
                stateFromZipcodes.Add(new StateFromZipcode("SC", "South Carolina", "29001", "29948"));
                stateFromZipcodes.Add(new StateFromZipcode("SD", "South Dakota", "57001", "57799"));
                stateFromZipcodes.Add(new StateFromZipcode("TN", "Tennessee", "37010", "38589"));
                stateFromZipcodes.Add(new StateFromZipcode("TX", "Texas", "73301", "73301"));
                stateFromZipcodes.Add(new StateFromZipcode("TX", "Texas", "75001", "75501"));
                stateFromZipcodes.Add(new StateFromZipcode("TX", "Texas", "75503", "79999"));
                stateFromZipcodes.Add(new StateFromZipcode("TX", "Texas", "88510", "88589"));
                stateFromZipcodes.Add(new StateFromZipcode("UT", "Utah", "84001", "84784"));
                stateFromZipcodes.Add(new StateFromZipcode("VT", "Vermont", "5001", "5495"));
                stateFromZipcodes.Add(new StateFromZipcode("VT", "Vermont", "5601", "5907"));
                stateFromZipcodes.Add(new StateFromZipcode("VA", "Virginia", "20040", "20167"));
                stateFromZipcodes.Add(new StateFromZipcode("VA", "Virginia", "22001", "24658"));
                stateFromZipcodes.Add(new StateFromZipcode("WA", "Washington", "98001", "99403"));
                stateFromZipcodes.Add(new StateFromZipcode("WV", "West Virginia", "24701", "26886"));
                stateFromZipcodes.Add(new StateFromZipcode("WI", "Wisconsin", "53001", "54990"));
                stateFromZipcodes.Add(new StateFromZipcode("WY", "Wyoming", "82001", "83128"));

                // Territories
                stateFromZipcodes.Add(new StateFromZipcode("AS", "American Samoa", "96799", "96799"));
                stateFromZipcodes.Add(new StateFromZipcode("GU", "Guam", "96910", "96910"));
                stateFromZipcodes.Add(new StateFromZipcode("GU", "Guam", "96912", "96913"));
                stateFromZipcodes.Add(new StateFromZipcode("GU", "Guam", "96915", "96917"));
                stateFromZipcodes.Add(new StateFromZipcode("GU", "Guam", "96919", "96919"));
                stateFromZipcodes.Add(new StateFromZipcode("GU", "Guam", "96921", "96921"));
                stateFromZipcodes.Add(new StateFromZipcode("GU", "Guam", "96923", "96923"));
                stateFromZipcodes.Add(new StateFromZipcode("GU", "Guam", "96928", "96929"));
                stateFromZipcodes.Add(new StateFromZipcode("GU", "Guam", "96931", "96932"));
                stateFromZipcodes.Add(new StateFromZipcode("PW", "Palau", "96939", "96940"));
                stateFromZipcodes.Add(new StateFromZipcode("FM", "Federated States Of Micronesia", "96942", "96944"));
                stateFromZipcodes.Add(new StateFromZipcode("MP", "Commonwealth Of The Northern Mariana Islands", "96950", "96952"));
                stateFromZipcodes.Add(new StateFromZipcode("MH", "Republic Of The Marshall Islands", "96960", "96960"));
                stateFromZipcodes.Add(new StateFromZipcode("MH", "Republic Of The Marshall Islands", "96970", "96970"));
                stateFromZipcodes.Add(new StateFromZipcode("FM", "Federated States Of Micronesia", "96941", "96941"));
            }
        }


    }
}

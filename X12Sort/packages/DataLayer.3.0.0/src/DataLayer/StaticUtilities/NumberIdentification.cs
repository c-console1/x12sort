using DataLayer.Base.Models;
using DataLayer.Billing.Models;
using DataLayer.StaticUtilities.Extensions;
using System.Text.RegularExpressions;

namespace DataLayer.StaticUtilities
{
    /// <summary>
    /// Static functions to help with number identification.
    /// </summary>
    public static class NumberIdentification
    {
        /// <summary>
        /// Regex to validate social security numbers, doesn't include the "-"
        /// </summary>
        /// <remarks> Regex Reference: https://www.codeproject.com/Articles/651609/Validating-Social-Security-Numbers-through-Regular </remarks>
        /// <value> Constant string to be used as a Regex Expression </value>
        public const string SocialSecurityRegex = @"^(?!219099999|078051120)(?!666|000|9\d{2})\d{3}(?!00)\d{2}(?!0{4})\d{4}$";

        /// <summary>
        /// Regex to validate Health Insurance Claim Number (HICN), doesn't include the "-"
        /// </summary>
        /// <remarks> Regex Reference: http://regexlib.com/REDetails.aspx?regexp_id=3643 </remarks>
        /// <value> Constant string to be used as a Regex Expression </value>
        public const string HealthInsuranceClaimRegex = @"^(00[1-9]|0[1-9][0-9]|[1-7][0-9][0-9]|7[0-7][0-2]|77[0-2])(\d{6})(A|B([1-9]?|[ADGHJKLNPQRTWY])|C([1-9]|[A-Z])|D([1-9]?|[ACDGHJKLMNPQRSTVWXYZ])|E([1-9]?|[ABCDFGHJKM])|F([1-8])|J([1-4])|K([1-9]|[ABCDEFGHJLM])|T([ABCDEFGHJKLMNPQRSTUVWXYZ2]?)|M|W([1-9]?|[BCFGJRT]))$";

        /// <summary>
        /// Regex to validate Health Insurance Claim Number issued by RRB, doesn't include the "-"
        /// </summary>
        /// <remarks> Regex Reference: https://www.cms.gov/Regulations-and-Guidance/Guidance/Manuals/Downloads/ge101c02.pdf </remarks>
        /// <value> Constant string to be used as a Regex Expression </value>
        public const string RailroadRetirementBoardRegex = @"^(A|CA|H|JA|MA|MH|PA|PD|PH|WA|WCA|WCD|WCH|WD|WH)(?!219099999|078051120)(?!666|000|9\d{2})\d{3}(?!00)\d{2}(?!0{4})\d{4}$";

        /// <summary>
        /// Regex to validate Medicare Beneficiary Identifier (MBI), doesn't include the "-"
        /// </summary>
        /// <remarks> Regex Reference: https://stackoverflow.com/a/47683670 </remarks>
        /// <value> Constant string to be used as a Regex Expression </value>
        public const string MedicareBeneficiaryIdentifierRegex = @"^\d(?![SLOIBZ])[A-Z]\d|(?![SLOIBZ])[A-Z]\d(?![SLOIBZ])[A-Z]\d|(?![SLOIBZ])[A-Z]\d(?![SLOIBZ])[A-Z](?![SLOIBZ])[A-Z]\d\d$";

        /// <summary>
        /// Regex to validate Paramount member
        /// </summary>
        /// <value> Constant string to be used as a Regex Expression </value>
        public const string ParamountMemberRegex = @"^[A-z][A-z0-9]{2}\d{8}$";

        /// <summary>
        /// Regex to validate Paramount contract
        /// </summary>
        /// <value> Constant string to be used as a Regex Expression </value>
        public const string ParamountContractRegex = @"^[A-z][A-z0-9]{2}\d{6}$";

        /// <summary>
        /// Regex to validate MBI, doesn't include the "-"
        /// </summary>
        /// <remarks> Format from Matt Elder </remarks>
        /// <value> Constant string to be used as a Regex Expression </value>
        public const string CaseNumberRegex = @"^\d{7,10}$";

        /// <summary>
        /// Regex to validate Medicaid Numbers (MMIS)
        /// </summary>
        /// <remarks> Format from Matt Elder </remarks>
        /// <value> Constant string to be used as a Regex Expression </value>
        public const string MedicaidRegex = @"^\d{12}$";

        /// <summary>
        /// Get the type of Id the given string is
        /// </summary>
        /// <param name="id">String that is an id of some kind</param>
        /// <remarks> Internally this does a lot of regex matching, which is not optimize so it may be slow on large repetitions</remarks>
        /// <returns>The Identifier Type if a match was found or Unknown if it wasn't</returns>
        public static MemberIdentifierType GetIdType(string id)
        {
            if (IsMedicareBeneficiary(id))
            {
                return MemberIdentifierType.MBI;
            }
            if (IsHealthInsuranceClaim(id))
            {
                return MemberIdentifierType.HICN;
            }
            if (IsRailroadRetirementClaim(id))
            {
                return MemberIdentifierType.RRB;
            }
            if (IsSocialSecurity(id))
            {
                return MemberIdentifierType.SSN;
            }
            if (IsParamountMember(id))
            {
                return MemberIdentifierType.Paramount;
            }
            if (IsParamountContract(id))
            {
                return MemberIdentifierType.Contract;
            }
            if (IsCaseNumber(id))
            {
                return MemberIdentifierType.CaseNumber;
            }
            if (IsMedicaid(id))
            {
                return MemberIdentifierType.Medicaid;
            }
            return MemberIdentifierType.Unknown;
        }

        #region Is<type> functions

        /// <summary>
        /// Returns true if the number is a valid social security number
        /// </summary>
        /// <param name="id"> The string number you are trying to validate </param>
        /// <returns>True if the given string match the expected format, else false</returns>
        public static bool IsSocialSecurity(string id)
        {
            id.ThrowIfNullOrEmpty(nameof(id));
            return Regex.IsMatch(id.Trim().Replace("-", "").ToUpper(), SocialSecurityRegex);
        }

        /// <summary>
        /// Returns true if the number is a valid HCIN number
        /// </summary>
        /// <param name="id"> The string number you are trying to validate </param>
        /// <returns>True if the given string match the expected format, else false</returns>
        public static bool IsHealthInsuranceClaim(string id)
        {
            id.ThrowIfNullOrEmpty(nameof(id));
            return Regex.IsMatch(id.Trim().Replace("-", "").ToUpper(), HealthInsuranceClaimRegex);
        }

        /// <summary>
        /// Returns true if the number is a valid RRB HCIN number
        /// </summary>
        /// <param name="id"> The string number you are trying to validate </param>
        /// <returns>True if the given string match the expected format, else false</returns>
        public static bool IsRailroadRetirementClaim(string id)
        {
            id.ThrowIfNullOrEmpty(nameof(id));
            return Regex.IsMatch(id.Trim().Replace("-", "").ToUpper(), RailroadRetirementBoardRegex);
        }

        /// <summary>
        /// Returns true if the number is a valid MBI number
        /// </summary>
        /// <param name="id"> The string number you are trying to validate </param>
        /// <returns>True if the given string match the expected format, else false</returns>
        public static bool IsMedicareBeneficiary(string id)
        {
            id.ThrowIfNullOrEmpty(nameof(id));
            return Regex.IsMatch(id.Trim().Replace("-", "").ToUpper(), MedicareBeneficiaryIdentifierRegex);
        }

        /// <summary>
        /// Returns true if the number is a valid Paramount number
        /// </summary>
        /// <param name="id"> The string number you are trying to validate </param>
        /// <returns>True if the given string match the expected format, else false</returns>
        public static bool IsParamountMember(string id)
        {
            id.ThrowIfNullOrEmpty(nameof(id));
            return Regex.IsMatch(id.Trim().Replace("-", "").ToUpper(), ParamountMemberRegex);
        }

        /// <summary>
        /// Returns true if the number is a valid Paramount contract
        /// </summary>
        /// <param name="id"> The string number you are trying to validate </param>
        /// <returns>True if the given string match the expected format, else false</returns>
        public static bool IsParamountContract(string id)
        {
            id.ThrowIfNullOrEmpty(nameof(id));
            return Regex.IsMatch(id.Trim().Replace("-", "").ToUpper(), ParamountContractRegex);
        }

        /// <summary>
        /// Returns true if the number is a valid Medicaid case number
        /// </summary>
        /// <param name="id"> The string number you are trying to validate </param>
        /// <returns>True if the given string match the expected format, else false</returns>
        public static bool IsCaseNumber(string id)
        {
            id.ThrowIfNullOrEmpty(nameof(id));
            return Regex.IsMatch(id.Trim().Replace("-", "").ToUpper(), CaseNumberRegex);
        }

        /// <summary>
        /// Returns true if the number is a valid Medicaid number
        /// </summary>
        /// <param name="id"> The string number you are trying to validate </param>
        /// <returns>True if the given string match the expected format, else false</returns>
        public static bool IsMedicaid(string id)
        {
            id.ThrowIfNullOrEmpty(nameof(id));
            return Regex.IsMatch(id.Trim().Replace("-", "").ToUpper(), MedicaidRegex);
        }

        #endregion Is<type> functions
    }
}

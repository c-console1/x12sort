using Dapper;
using DataLayer.Base;
using DataLayer.StaticUtilities.Extensions;
using DataLayer.StaticUtilities.Models;
using NLog;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DataLayer.StaticUtilities
{
    /// <summary>
    /// This class contains the Bulk Copy Extension Methods for `IDbExec`
    /// </summary>
    public static class BulkCopyExtension
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        #region Schema Functions

        /// <summary>
        /// Returns a enumerable list of column metadata form the query
        /// </summary>
        /// <param name="dbExec">The database executor to use</param>
        /// <param name="query">A SQL query to get the column metadata for</param>
        /// <returns></returns>
        public static IEnumerable<DataReaderColumnSchema> GetSchemaFromQuery(this IDbExec dbExec, string query)
        {
            IEnumerable<DataReaderColumnSchema> result = null;

            if (dbExec != null && query != null)
            {
                // if the query contails a space, than it should be wrapped in () as a subquery
                if (query.Contains(" "))
                {
                    query = $"({query})";
                }

                string firstRowQuery;
                switch (dbExec.Provider)
                {
                    case ProviderType.Oracle:
                        firstRowQuery = $"SELECT * FROM {query} WHERE ROWNUM = 1"; break;
                    case ProviderType.Sql:
                        firstRowQuery = $"SELECT TOP 1 * FROM {query}"; break;
                    default:
                        throw new InvalidOperationException("Provider type not supported");
                }

                using (IDataReader reader = dbExec.ExecuteReader(firstRowQuery))
                {
                    result = reader.GetSchemaList();
                }
            }

            return result;
        }

        /// <summary>
        /// Returns a enumerable list of column metadata form the query
        /// </summary>
        /// <param name="database">The logical database to use</param>
        /// <param name="query">A SQL query to get the column metadata for</param>
        /// <returns></returns>
        public static IEnumerable<DataReaderColumnSchema> GetSchemaFromQuery(this BaseDatabase database, string query)
        {
            if (database != null)
                return database.DbExec.GetSchemaFromQuery(query);
            else
                return null;
        }

        /// <summary>
        /// Returns a enumerable list of column metadata for the data reader
        /// </summary>
        /// <param name="reader">An `IDataReader` to get the schema for</param>
        /// <returns></returns>
        public static IEnumerable<DataReaderColumnSchema> GetSchemaList(this IDataReader reader)
        {
            reader.ThrowIfNull(nameof(reader));
            return reader.GetSchemaTable().CreateDataReader().Parse<DataReaderColumnSchema>();
        }

        #endregion Schema Functions

        #region Bulk Copy Functions

        /// <summary>
        /// Execute BulkCopy Insert functions for either SQL Server or Oracle, based on set provider type.
        /// </summary>
        /// <param name="dbExec">Database Executor to Extend</param>
        /// <param name="reader">Data Reader from insert query</param>
        /// <param name="tableName">Name of table to insert data from data reader</param>
        /// <param name="batchSize">Default 10,000. Number of inserts to process per batch.</param>
        /// <param name="useColumnMapping">If set to true, matches columns from database reader output to target table. If set to false matching is positional between source and target.</param>
        //public static long ExecuteBulkCopy(this IDbExec dbExec, IDataReader reader, string tableName, IEnumerable<ColumnMapInfo> columnMapping = null, int batchSize = 10000)
        public static long ExecuteBulkCopy(this IDbExec dbExec, IDataReader reader, string tableName, int batchSize = 10000, bool useColumnMapping = false)
        {
            _logger.Info($"Starting BulkCopy transfer into {tableName}");
            long rowCount = 0;

            if (dbExec != null && reader != null)
            {
                switch (dbExec.Provider)
                {
                    case ProviderType.Sql:
                        rowCount = _doSqlBulkCopy(dbExec, reader, tableName, batchSize, useColumnMapping);
                        break;

                    case ProviderType.Oracle:
                        rowCount = _doOracleBulkCopy(dbExec, reader, tableName, batchSize);
                        break;

                    default:
                        throw new InvalidOperationException("Provider Type not supported!");
                }
            }

            _logger.Info($"Inserted {rowCount} row(s)");
            return rowCount;
        }

        /// <summary>
        /// Execute BulkCopy Insert functions for either SQL Server or Oracle, based on set provider type.
        /// </summary>
        /// <param name="db">Base Database object to Extend</param>
        /// <param name="reader">Data Reader from insert query</param>
        /// <param name="tableName">Name of table to insert data from data reader</param>
        /// <param name="batchSize">Default 10,000. Number of inserts to process per batch.</param>
        /// <param name="useColumnMapping">If set to true, matches columns from database reader output to target table. If set to false matching is positional between source and target.</param>
        public static long ExecuteBulkCopy(this BaseDatabase db, IDataReader reader, string tableName, int batchSize = 10000, bool useColumnMapping = false)
        {
            if (db != null)
                return db.DbExec.ExecuteBulkCopy(reader, tableName, batchSize, useColumnMapping);
            else
                return -1;
        }

        /// <summary>
        /// Performs a bulk copy into the targetTable using a data reader for the source data
        /// </summary>
        /// <param name="dbExec">Database Executor to use</param>
        /// <param name="reader">IDataReader object for the source data</param>
        /// <param name="tableName">Name of target table</param>
        /// <param name="batchSize">Size of each individual bulk copy batch</param>
        /// <param name="useColumnMapping">If set to true, matches columns from database reader output to target table. If set to false matching is positional between source and target.</param>
        private static long _doSqlBulkCopy(IDbExec dbExec, IDataReader reader, string tableName, int batchSize, bool useColumnMapping)
        {
            long beginCount = dbExec.ExecuteScalar<long>($"SELECT COUNT(*) FROM {tableName}");
            SqlConnection conn = (SqlConnection)(dbExec as DbExec).Connection;
            using SqlBulkCopy bulkCopy = new SqlBulkCopy(conn) { DestinationTableName = tableName, BatchSize = batchSize };

            if (useColumnMapping)
            {
                // Check if destinationTableName has schema name
                string schema = null;
                string[] split = bulkCopy.DestinationTableName.Split(new[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                if (split.Length > 1)
                {
                    schema = split[0];
                    tableName = split[1];
                }

                // Get source column names
                IEnumerable<string> sourceColumnNames = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName);

                // Get destination column names
                IEnumerable<string> destColumnNames = conn.GetSchema("Columns", new[] { null, schema, tableName, null })
                    .Rows.Cast<DataRow>().Select(x => x["COLUMN_NAME"].ToString());

                // get common elements from both column names, ignore case
                IEnumerable<string> columnCollection = Enumerable.Intersect(destColumnNames, sourceColumnNames, StringComparer.OrdinalIgnoreCase);
                foreach (string columnName in columnCollection)
                    bulkCopy.ColumnMappings.Add(columnName, columnName);
            }

            bulkCopy.WriteToServer(reader);
            bulkCopy.Close();

            long endCount = dbExec.ExecuteScalar<long>($"SELECT COUNT(*) FROM {bulkCopy.DestinationTableName}");
            return endCount - beginCount;
        }

        /// <summary>
        /// Performs a bulk copy into the targetTable using a data reader for the source data
        /// </summary>
        /// <param name="dbExec">Database Executor to use</param>
        /// <param name="reader">IDataReader object for the source data</param>
        /// <param name="tableName">Name of target table</param>
        /// <param name="batchSize">Size of each individual bulk copy batch</param>
        /// <returns>Total number of rows inserted</returns>
        private static long _doOracleBulkCopy(IDbExec dbExec, IDataReader reader, string tableName, int batchSize)
        {
            List<DataReaderColumnSchema> validColumns = reader.GetSchemaList().ToList();

            List<object[]> rowBatch = new List<object[]>(batchSize);

            // Build binding statement based on column names
            StringBuilder bindingStatement = new StringBuilder();
            bindingStatement.Append("INSERT INTO ").Append(tableName).Append("(")
                .Append(string.Join(",", validColumns.Select(q => q.ColumnName)))
                .Append(") values (")
                .Append(string.Join(",", validColumns.Select(q => $":{q.ColumnName}")))
                .Append(")");

            IDbCommand cmd = dbExec.CreateCommand();
            cmd.CommandText = bindingStatement.ToString();
            if (cmd is OracleCommand)
            {
                (cmd as OracleCommand).ArrayBindCount = batchSize;
            }

            Dictionary<int, List<object>> transposedValues = new Dictionary<int, List<object>>();
            validColumns.ForEach(q => transposedValues[q.ColumnOrdinal] = new List<object>());

            // Perform bulk copy
            long rowCount = 0;
            int currentCount = 0;
            try
            {
                while (reader.Read())
                {
                    object[] row = new object[reader.FieldCount];
                    reader.GetValues(row);
                    rowBatch.Add(row);

                    // transpose values to make arrays of the values
                    foreach (var item in transposedValues)
                    {
                        transposedValues[item.Key].Add(row[item.Key]);
                    }

                    currentCount++;
                    // Check to see if row batch has reached max size, if so run the upload
                    if (currentCount >= batchSize)
                    {
                        foreach (var item in validColumns)
                        {
                            IDbDataParameter p = cmd.CreateParameter();
                            p.ParameterName = $":{item.ColumnName}";
                            if (p is OracleParameter)
                            {
                                (p as OracleParameter).OracleDbType = TypeConverter.ToOracleDbType(item.DataType);
                            }
                            else
                            {
                                p.DbType = TypeConverter.ToDbType(item.DataType);
                            }
                            p.Direction = ParameterDirection.Input;
                            p.Value = transposedValues[item.ColumnOrdinal].ToArray();
                            cmd.Parameters.Add(p);
                        }

                        rowCount += cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                        currentCount = 0;
                    }
                }

                // Send remaining data to bulk copy
                if (currentCount > 0)
                {
                    if (cmd is OracleCommand)
                    {
                        (cmd as OracleCommand).ArrayBindCount = rowBatch.Count;
                    }

                    foreach (var item in validColumns)
                    {
                        IDbDataParameter p = cmd.CreateParameter();
                        p.ParameterName = $":{item.ColumnName}";
                        if (p is OracleParameter)
                        {
                            (p as OracleParameter).OracleDbType = TypeConverter.ToOracleDbType(item.DataType);
                        }
                        else
                        {
                            p.DbType = TypeConverter.ToDbType(item.DataType);
                        }
                        p.Direction = ParameterDirection.Input;
                        p.Value = transposedValues[item.ColumnOrdinal].ToArray();
                        cmd.Parameters.Add(p);
                    }

                    rowCount += cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                }
            }
            catch (Exception e)
            {
                _logger.Warn(e, "Exception when executing BulkCopy (Oracle)");
                throw;
            }

            return rowCount;
        }

        #endregion Bulk Copy Functions
    }
}

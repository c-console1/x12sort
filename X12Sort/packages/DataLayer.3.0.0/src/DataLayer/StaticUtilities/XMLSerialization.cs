using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace DataLayer.StaticUtilities
{
    public static class XMLSerialization
    {
        #region BuildRequestObject methods

        /// <summary>
        /// Build a request object XML
        /// </summary>
        /// <param name="requestSource">Source object to</param>
        /// <param name="template">XML template ussaly pulled from the 'resRequests.resx' resource file</param>
        /// <param name="customFunctionPre">Lamda function for customized cleanup BEFORE the template is applied</param>
        /// <param name="customFunctionPost">Lamda function for customized cleanup AFTER the template is applied</param>
        public static string BuildRequestObject(object requestSource, string template, Func<string, string> customFunctionPre, Func<string, string> customFunctionPost)
        {
            if (requestSource == null)
                throw new ArgumentNullException(nameof(requestSource));

            if (template == null)
                throw new ArgumentNullException(nameof(template));

            if (customFunctionPre == null)
                throw new ArgumentNullException(nameof(customFunctionPre));

            if (customFunctionPost == null)
                throw new ArgumentNullException(nameof(customFunctionPost));

            // Build request
            // * Wrap the raw serialized XML with a temproary or '<junk}' tag so a multiple root XML object errot is not the
            //   thrown on XML clean-up
            // * Run the XML cleanup routine
            // * Remove the temproary or 'junk' tag
            string output = "<junk>" + XMLSerialization.Object2XML(requestSource) + "</junk>";
            output = XMLSerialization.RemoveEmptyTags(output);
            output = output.Replace("<junk>", "").Replace("</junk>", "");

            // * Run a cumstom function for special changes like adding custom attrubutes to a tag
            if (customFunctionPre != null)
                output = customFunctionPre(output);

            // * Get a standard template loaded from the 'resRequests.resx' resource file
            output = string.Format(template, output); // Merge generated XML into template

            // * Run a cumstom function for special changes like special values into the header tag
            //   This should be rarely needed
            output = customFunctionPost(output);

            // * Return request objects
            return output;
        }

        /// <summary>
        /// Build a request object XML
        /// </summary>
        /// <param name="requestSource">Source object to</param>
        /// <param name="template">XML template ussaly pulled from the 'resRequests.resx' resource file</param>
        /// <param name="customFunctionPre">Lamda function for customized cleanup BEFORE the template is applied</param>
        public static string BuildRequestObject(object requestSource, string template, Func<string, string> customFunctionPre)
        {
            // Mirror the main method except the 'customFunctionPre' lamda function
            return BuildRequestObject(requestSource, template, customFunctionPre, x => x);
        }

        /// <summary>
        /// Build a request object XML
        /// </summary>
        /// <param name="requestSource">Source object to</param>
        /// <param name="template">XML template ussaly pulled from the 'resRequests.resx' resource file</param>
        public static string BuildRequestObject(object requestSource, string template)
        {
            // Mirror the main method except both the 'customFunctionPre' and 'customFunctionPost' lamda functions
            return BuildRequestObject(requestSource, template, x => x, y => y);
        }

        #endregion BuildRequestObject methods

        /// <summary>
        /// Deserialize object from string
        /// </summary>
        /// <param name="objectData">XML serialized data</param>
        /// <param name="type">Object data type</param>
        /// <returns>.NET object, null </returns>
        public static object XmlDeserializeFromString(this string objectData, Type type)
        {
            if (objectData == null)
                throw new ArgumentNullException(nameof(objectData));

            if (type == null)
                throw new ArgumentNullException(nameof(type));

            XmlSerializer serializer = new XmlSerializer(type);
            object result;

            using (TextReader reader = new StringReader(objectData))
            {
                result = serializer.Deserialize(reader);
            }

            return result;
        }

        /// <summary>
        /// Serialize an object to XML via Newton JSON
        /// </summary>
        /// <param name="source">Source .NET objects</param>
        /// <returns>XML string</returns>
        public static string Object2XML(object source)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));

            string json = JsonConvert.SerializeObject(source, Newtonsoft.Json.Formatting.Indented);

            json = "{mainXmlBody : [ " + json + " ] }";

            XmlDocument doc = (XmlDocument)JsonConvert.DeserializeXmlNode(json);
            string output = doc.InnerXml;
            output = PrintXML(output);
            output = output.Replace("<mainXmlBody>", "").Replace("</mainXmlBody>", "");
            return output;
        }

        /// <summary>
        /// Convert XML to JSON so you can use http://json2csharp.com to C# response classes
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static string XML2JSON(string xml, List<string> tagsToForceArrays)
        {
            if (xml == null)
                throw new ArgumentNullException(nameof(xml));

            if (tagsToForceArrays == null)
                throw new ArgumentNullException(nameof(tagsToForceArrays));

            List<string> badValues = new List<string>();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            // Remove attributes and get a list of prefixes to remove
            // these generate XML columns that may break Newtonsoft JSON deserialization,
            // Exspicial;y if they in include name spaces.
            foreach (XmlElement el in doc.SelectNodes(".//*"))
            {
                el.Attributes.RemoveAll();
            }

            // Remove all name spaces that will break deserialization in Newtonsoft JSON
            // Name spaces will break
            xml = RemoveAllNamespaces(xml);
            doc.LoadXml(xml);

            // Allow forced arrays in document
            // Newtonsoft JSON does not deserialize a list on IEnumeration when an XML only has 1 value
            // in what should be an array.  This is the work around. Ex.
            // <a> <b>1</b> <b>2</b> </a> - Will deserialize as an IEnumerable object
            // <a> <b>1</b> </a> - Will deserialize as a singlar object
            // <a> <b xmlns:json='http://james.newtonking.com/projects/json'>1</b> </a> - Will force to be a IEnumerable object
            xml = doc.InnerXml;
            int posOfFirstSpace = xml.IndexOf('>');
            xml = xml.Substring(0, posOfFirstSpace) + " xmlns:json='http://james.newtonking.com/projects/json'" +
                xml.Substring(posOfFirstSpace);

            if (tagsToForceArrays != null)
            {
                tagsToForceArrays.ForEach(x => xml = xml.Replace(
                    string.Format("<{0}>", x), string.Format("<{0} json:Array='true'>", x)
                    ));
            }

            doc.LoadXml(xml);

            // Serialize the object
            string json = JsonConvert.SerializeXmlNode(doc, Newtonsoft.Json.Formatting.Indented);
            return json;
        }

        /// <summary>
        /// Remove all attributes from an XML string
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static string RemoveAllAttributes(string xml)
        {
            if (xml == null)
                throw new ArgumentNullException(nameof(xml));

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            // Remove attributes and get a list of prefixes to remove
            // these generate XML columns that may break Newtonsoft JSON deserialization,
            // Exspicial;y if they in include name spaces.
            foreach (XmlElement el in doc.SelectNodes(".//*"))
            {
                el.Attributes.RemoveAll();
            }
            return doc.InnerXml;
        }

        /// <summary>
        /// Implemented based on interface, not part of algorithm
        /// </summary>
        /// <param name="xmlDocument"></param>
        /// <returns></returns>
        public static string RemoveAllNamespaces(string xmlDocument)
        {
            if (xmlDocument == null)
                throw new ArgumentNullException(nameof(xmlDocument));

            XElement xmlDocumentWithoutNs = RemoveAllNamespaces(XElement.Parse(xmlDocument));

            return xmlDocumentWithoutNs.ToString();
        }

        /// <summary>
        /// Core recursion function
        /// </summary>
        /// <param name="xmlDocument"></param>
        /// <returns></returns>
        private static XElement RemoveAllNamespaces(XElement xmlDocument)
        {
            if (xmlDocument == null)
                throw new ArgumentNullException(nameof(xmlDocument));

            if (!xmlDocument.HasElements)
            {
                XElement xElement = new XElement(xmlDocument.Name.LocalName);
                xElement.Value = xmlDocument.Value;

                foreach (XAttribute attribute in xmlDocument.Attributes())
                    xElement.Add(attribute);

                return xElement;
            }
            return new XElement(xmlDocument.Name.LocalName, xmlDocument.Elements().Select(el => RemoveAllNamespaces(el)));
        }

        /// <summary>
        /// Remove empty tags from XML
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static string RemoveEmptyTags(string xml)
        {
            if (xml == null)
                throw new ArgumentNullException(nameof(xml));

            XElement doc = XElement.Parse(xml);
            doc.Descendants().Where(e => string.IsNullOrEmpty(e.Value)).Remove();
            return PrintXML(doc.ToString());
        }

        /// <summary>
        /// Format XML to be human readable
        /// </summary>
        /// <param name="xml">XML String source</param>
        /// <returns>Print / Easy to read XML string</returns>
        public static string PrintXML(string xml)
        {
            if (xml == null)
                throw new ArgumentNullException(nameof(xml));

            string result = "";

            MemoryStream mStream = new MemoryStream();
            XmlTextWriter xmlTextWriter = new XmlTextWriter(mStream, Encoding.Unicode);
            XmlDocument document = new XmlDocument();

            try
            {
                // Load the XmlDocument with the XML.
                document.LoadXml(xml);

                xmlTextWriter.Formatting = System.Xml.Formatting.Indented;

                // Write the XML into a formatting XmlTextWriter
                document.WriteContentTo(xmlTextWriter);
                xmlTextWriter.Flush();
                mStream.Flush();

                // Have to rewind the MemoryStream in order to read
                // its contents.
                mStream.Position = 0;

                // Read MemoryStream contents into a StreamReader.
                StreamReader sReader = new StreamReader(mStream);

                // Extract the text from the StreamReader.
                string formattedXml = sReader.ReadToEnd();

                sReader.Dispose();
                result = formattedXml;
            }
            catch (XmlException)
            {
                // Handle the exception
            }

            mStream.Close();
            mStream.Dispose();
            try {
                xmlTextWriter.Close();
                xmlTextWriter.Dispose();
            } catch (Exception) { }

            return result;
        }
    }
}

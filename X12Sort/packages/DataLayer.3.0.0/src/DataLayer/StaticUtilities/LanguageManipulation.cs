﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.StaticUtilities
{
    public class LanguageManipulation
    {
        /// <summary>
        /// Converts an iso 639-1 language abbreviation to full length. Case insensitive.
        /// en -> English
        /// </summary>
        /// <param name="abbreviation">Language to expand</param>
        /// <returns></returns>
        public static string LanguageAbbreviationToVerbose(string abbreviation)
        {
            //codes from: https://www.w3schools.com/tags/ref_language_codes.asp

            if (!string.IsNullOrEmpty(abbreviation))
            {
                switch (abbreviation.ToLower())
                {
                    case "ab": return "Abkhazian";
                    case "aa": return "Afar";
                    case "af": return "Afrikaans";
                    case "ak": return "Akan";
                    case "sq": return "Albanian";
                    case "am": return "Amharic";
                    case "ar": return "Arabic";
                    case "an": return "Aragonese";
                    case "hy": return "Armenian";
                    case "as": return "Assamese";
                    case "av": return "Avaric";
                    case "ae": return "Avestan";
                    case "ay": return "Aymara";
                    case "az": return "Azerbaijani";
                    case "bm": return "Bambara";
                    case "ba": return "Bashkir";
                    case "eu": return "Basque";
                    case "be": return "Belarusian";
                    case "bn": return "Bengali (Bangla)";
                    case "bh": return "Bihari";
                    case "bi": return "Bislama";
                    case "bs": return "Bosnian";
                    case "br": return "Breton";
                    case "bg": return "Bulgarian";
                    case "my": return "Burmese";
                    case "ca": return "Catalan";
                    case "ch": return "Chamorro";
                    case "ce": return "Chechen";
                    case "ny": return "Chichewa, Chewa, Nyanja";
                    case "zh": return "Chinese";
                    case "zh-Hans": return "Chinese (Simplified)";
                    case "zh-Hant": return "Chinese (Traditional)";
                    case "cv": return "Chuvash";
                    case "kw": return "Cornish";
                    case "co": return "Corsican";
                    case "cr": return "Cree";
                    case "hr": return "Croatian";
                    case "cs": return "Czech";
                    case "da": return "Danish";
                    case "dv": return "Divehi, Dhivehi, Maldivian";
                    case "nl": return "Dutch";
                    case "dz": return "Dzongkha";
                    case "en": return "English";
                    case "eo": return "Esperanto";
                    case "et": return "Estonian";
                    case "ee": return "Ewe";
                    case "fo": return "Faroese";
                    case "fj": return "Fijian";
                    case "fi": return "Finnish";
                    case "fr": return "French";
                    case "ff": return "Fula, Fulah, Pulaar, Pular";
                    case "gl": return "Galician";
                    case "gd": return "Gaelic (Scottish)";
                    case "ka": return "Georgian";
                    case "de": return "German";
                    case "el": return "Greek";
                    case "kl": return "Greenlandic";
                    case "gn": return "Guarani";
                    case "gu": return "Gujarati";
                    case "ht": return "Haitian Creole";
                    case "ha": return "Hausa";
                    case "he": return "Hebrew";
                    case "hz": return "Herero";
                    case "hi": return "Hindi";
                    case "ho": return "Hiri Motu";
                    case "hu": return "Hungarian";
                    case "is": return "Icelandic";
                    case "io": return "Ido";
                    case "ig": return "Igbo";
                    case "id": return "Indonesian";
                    case "ia": return "Interlingua";
                    case "ie": return "Interlingue";
                    case "iu": return "Inuktitut";
                    case "ik": return "Inupiak";
                    case "ga": return "Irish";
                    case "it": return "Italian";
                    case "ja": return "Japanese";
                    case "jv": return "Javanese";
                    case "kn": return "Kannada";
                    case "kr": return "Kanuri";
                    case "ks": return "Kashmiri";
                    case "kk": return "Kazakh";
                    case "km": return "Khmer";
                    case "ki": return "Kikuyu";
                    case "rw": return "Kinyarwanda (Rwanda)";
                    case "rn": return "Kirundi";
                    case "ky": return "Kyrgyz";
                    case "kv": return "Komi";
                    case "kg": return "Kongo";
                    case "ko": return "Korean";
                    case "ku": return "Kurdish";
                    case "kj": return "Kwanyama";
                    case "lo": return "Lao";
                    case "la": return "Latin";
                    case "lv": return "Latvian (Lettish)";
                    case "li": return "Limburgish ( Limburger)";
                    case "ln": return "Lingala";
                    case "lt": return "Lithuanian";
                    case "lu": return "Luga-Katanga";
                    case "lg": return "Luganda, Ganda";
                    case "lb": return "Luxembourgish";
                    case "gv": return "Manx";
                    case "mk": return "Macedonian";
                    case "mg": return "Malagasy";
                    case "ms": return "Malay";
                    case "ml": return "Malayalam";
                    case "mt": return "Maltese";
                    case "mi": return "Maori";
                    case "mr": return "Marathi";
                    case "mh": return "Marshallese";
                    case "mo": return "Moldavian";
                    case "mn": return "Mongolian";
                    case "na": return "Nauru";
                    case "nv": return "Navajo";
                    case "ng": return "Ndonga";
                    case "nd": return "Northern Ndebele";
                    case "ne": return "Nepali";
                    case "no": return "Norwegian";
                    case "nb": return "Norwegian bokmål";
                    case "nn": return "Norwegian nynorsk";
                    case "ii": return "Nuosu";
                    case "oc": return "Occitan";
                    case "oj": return "Ojibwe";
                    case "cu": return "Old Church Slavonic, Old Bulgarian";
                    case "or": return "Oriya";
                    case "om": return "Oromo (Afaan Oromo)";
                    case "os": return "Ossetian";
                    case "pi": return "Pāli";
                    case "ps": return "Pashto, Pushto";
                    case "fa": return "Persian (Farsi)";
                    case "pl": return "Polish";
                    case "pt": return "Portuguese";
                    case "pa": return "Punjabi (Eastern)";
                    case "qu": return "Quechua";
                    case "rm": return "Romansh";
                    case "ro": return "Romanian";
                    case "ru": return "Russian";
                    case "se": return "Sami";
                    case "sm": return "Samoan";
                    case "sg": return "Sango";
                    case "sa": return "Sanskrit";
                    case "sr": return "Serbian";
                    case "sh": return "Serbo-Croatian";
                    case "st": return "Sesotho";
                    case "tn": return "Setswana";
                    case "sn": return "Shona";
                    case "sd": return "Sindhi";
                    case "si": return "Sinhalese";
                    case "sk": return "Slovak";
                    case "sl": return "Slovenian";
                    case "so": return "Somali";
                    case "nr": return "Southern Ndebele";
                    case "es": return "Spanish";
                    case "su": return "Sundanese";
                    case "sw": return "Swahili (Kiswahili)";
                    case "ss": return "Swati";
                    case "sv": return "Swedish";
                    case "tl": return "Tagalog";
                    case "ty": return "Tahitian";
                    case "tg": return "Tajik";
                    case "ta": return "Tamil";
                    case "tt": return "Tatar";
                    case "te": return "Telugu";
                    case "th": return "Thai";
                    case "bo": return "Tibetan";
                    case "ti": return "Tigrinya";
                    case "to": return "Tonga";
                    case "ts": return "Tsonga";
                    case "tr": return "Turkish";
                    case "tk": return "Turkmen";
                    case "tw": return "Twi";
                    case "ug": return "Uyghur";
                    case "uk": return "Ukrainian";
                    case "ur": return "Urdu";
                    case "uz": return "Uzbek";
                    case "ve": return "Venda";
                    case "vi": return "Vietnamese";
                    case "vo": return "Volapük";
                    case "wa": return "Wallon";
                    case "cy": return "Welsh";
                    case "wo": return "Wolof";
                    case "fy": return "Western Frisian";
                    case "xh": return "Xhosa";
                    case "yi": return "Yiddish";
                    case "yo": return "Yoruba";
                    case "za": return "Zhuang, Chuang";
                    case "zu": return "Zulu";
                }
            }

            return "";
        }

        /// <summary>
        /// Converts a language to its iso 639-1 abbreviation. Case insensitive.
        /// English -> en
        /// </summary>
        /// <param name="verbose">Language to abbreviate</param>
        /// <returns></returns>
        public static string LanguageVerboseToAbbreviation(string verbose)
        {
            //codes from: https://www.w3schools.com/tags/ref_language_codes.asp

            if (!string.IsNullOrEmpty(verbose))
            {
                switch (verbose.ToLower())
                {
                    case "abkhazian": return "ab";
                    case "afar": return "aa";
                    case "afrikaans": return "af";
                    case "akan": return "ak";
                    case "albanian": return "sq";
                    case "amharic": return "am";
                    case "arabic": return "ar";
                    case "aragonese": return "an";
                    case "armenian": return "hy";
                    case "assamese": return "as";
                    case "avaric": return "av";
                    case "avestan": return "ae";
                    case "aymara": return "ay";
                    case "azerbaijani": return "az";
                    case "bambara": return "bm";
                    case "bashkir": return "ba";
                    case "basque": return "eu";
                    case "belarusian": return "be";
                    case "bengali (bangla)": return "bn";
                    case "bihari": return "bh";
                    case "bislama": return "bi";
                    case "bosnian": return "bs";
                    case "breton": return "br";
                    case "bulgarian": return "bg";
                    case "burmese": return "my";
                    case "catalan": return "ca";
                    case "chamorro": return "ch";
                    case "chechen": return "ce";
                    case "chichewa, chewa, nyanja": return "ny";
                    case "chinese": return "zh";
                    case "chinese (simplified)": return "zh-Hans";
                    case "chinese (traditional)": return "zh-Hant";
                    case "chuvash": return "cv";
                    case "cornish": return "kw";
                    case "corsican": return "co";
                    case "cree": return "cr";
                    case "croatian": return "hr";
                    case "czech": return "cs";
                    case "danish": return "da";
                    case "divehi, dhivehi, maldivian": return "dv";
                    case "dutch": return "nl";
                    case "dzongkha": return "dz";
                    case "english": return "en";
                    case "esperanto": return "eo";
                    case "estonian": return "et";
                    case "ewe": return "ee";
                    case "faroese": return "fo";
                    case "fijian": return "fj";
                    case "finnish": return "fi";
                    case "french": return "fr";
                    case "fula, fulah, pulaar, pular": return "ff";
                    case "galician": return "gl";
                    case "gaelic (scottish)": return "gd";
                    case "gaelic (manx)": return "gv";
                    case "georgian": return "ka";
                    case "german": return "de";
                    case "greek": return "el";
                    case "greenlandic": return "kl";
                    case "guarani": return "gn";
                    case "gujarati": return "gu";
                    case "haitian creole": return "ht";
                    case "hausa": return "ha";
                    case "hebrew": return "he";
                    case "herero": return "hz";
                    case "hindi": return "hi";
                    case "hiri motu": return "ho";
                    case "hungarian": return "hu";
                    case "icelandic": return "is";
                    case "ido": return "io";
                    case "igbo": return "ig";
                    case "indonesian": return "id";
                    case "interlingua": return "ia";
                    case "interlingue": return "ie";
                    case "inuktitut": return "iu";
                    case "inupiak": return "ik";
                    case "irish": return "ga";
                    case "italian": return "it";
                    case "japanese": return "ja";
                    case "javanese": return "jv";
                    case "kalaallisut, greenlandic": return "kl";
                    case "kannada": return "kn";
                    case "kanuri": return "kr";
                    case "kashmiri": return "ks";
                    case "kazakh": return "kk";
                    case "khmer": return "km";
                    case "kikuyu": return "ki";
                    case "kinyarwanda (rwanda)": return "rw";
                    case "kirundi": return "rn";
                    case "kyrgyz": return "ky";
                    case "komi": return "kv";
                    case "kongo": return "kg";
                    case "korean": return "ko";
                    case "kurdish": return "ku";
                    case "kwanyama": return "kj";
                    case "lao": return "lo";
                    case "latin": return "la";
                    case "latvian (lettish)": return "lv";
                    case "limburgish ( limburger)": return "li";
                    case "lingala": return "ln";
                    case "lithuanian": return "lt";
                    case "luga-katanga": return "lu";
                    case "luganda, ganda": return "lg";
                    case "luxembourgish": return "lb";
                    case "manx": return "gv";
                    case "macedonian": return "mk";
                    case "malagasy": return "mg";
                    case "malay": return "ms";
                    case "malayalam": return "ml";
                    case "maltese": return "mt";
                    case "maori": return "mi";
                    case "marathi": return "mr";
                    case "marshallese": return "mh";
                    case "moldavian": return "mo";
                    case "mongolian": return "mn";
                    case "nauru": return "na";
                    case "navajo": return "nv";
                    case "ndonga": return "ng";
                    case "northern ndebele": return "nd";
                    case "nepali": return "ne";
                    case "norwegian": return "no";
                    case "norwegian bokmål": return "nb";
                    case "norwegian nynorsk": return "nn";
                    case "nuosu": return "ii";
                    case "occitan": return "oc";
                    case "ojibwe": return "oj";
                    case "old church slavonic, old bulgarian": return "cu";
                    case "oriya": return "or";
                    case "oromo (afaan oromo)": return "om";
                    case "ossetian": return "os";
                    case "pāli": return "pi";
                    case "pashto, pushto": return "ps";
                    case "persian (farsi)": return "fa";
                    case "polish": return "pl";
                    case "portuguese": return "pt";
                    case "punjabi (eastern)": return "pa";
                    case "quechua": return "qu";
                    case "romansh": return "rm";
                    case "romanian": return "ro";
                    case "russian": return "ru";
                    case "sami": return "se";
                    case "samoan": return "sm";
                    case "sango": return "sg";
                    case "sanskrit": return "sa";
                    case "serbian": return "sr";
                    case "serbo-croatian": return "sh";
                    case "sesotho": return "st";
                    case "setswana": return "tn";
                    case "shona": return "sn";
                    case "sichuan yi": return "ii";
                    case "sindhi": return "sd";
                    case "sinhalese": return "si";
                    case "siswati": return "ss";
                    case "slovak": return "sk";
                    case "slovenian": return "sl";
                    case "somali": return "so";
                    case "southern ndebele": return "nr";
                    case "spanish": return "es";
                    case "sundanese": return "su";
                    case "swahili (kiswahili)": return "sw";
                    case "swati": return "ss";
                    case "swedish": return "sv";
                    case "tagalog": return "tl";
                    case "tahitian": return "ty";
                    case "tajik": return "tg";
                    case "tamil": return "ta";
                    case "tatar": return "tt";
                    case "telugu": return "te";
                    case "thai": return "th";
                    case "tibetan": return "bo";
                    case "tigrinya": return "ti";
                    case "tonga": return "to";
                    case "tsonga": return "ts";
                    case "turkish": return "tr";
                    case "turkmen": return "tk";
                    case "twi": return "tw";
                    case "uyghur": return "ug";
                    case "ukrainian": return "uk";
                    case "urdu": return "ur";
                    case "uzbek": return "uz";
                    case "venda": return "ve";
                    case "vietnamese": return "vi";
                    case "volapük": return "vo";
                    case "wallon": return "wa";
                    case "welsh": return "cy";
                    case "wolof": return "wo";
                    case "western frisian": return "fy";
                    case "xhosa": return "xh";
                    case "yiddish": return "yi";
                    case "yoruba": return "yo";
                    case "zhuang, chuang": return "za";
                    case "zulu": return "zu";
                }
            }

            return "";
        }

        /// <summary>
        /// Converts a hub language to its trucare abbreviation. Case insensitive.
        /// English -> en
        /// </summary>
        /// <param name="verbose"></param>
        /// <returns></returns>
        public static string HubVerboseToCareManagementAbbreviation(string verbose)
        {
            if (!string.IsNullOrEmpty(verbose))
            {
                switch (verbose.ToLower())
                {
                    case "albanian": return "SQ";
                    case "amharic": return "AM";
                    case "arabic": return "FA";
                    case "chinese": return "CA";
                    case "czchekoslovakia": return "CS";
                    case "dutch": return "NL";
                    case "english": return "EN";
                    case "filipino": return "FI";
                    case "french": return "FR";
                    case "german": return "GE";
                    case "greek": return "GK";
                    case "gujarati": return "GJ";
                    case "hausa": return "HA";
                    case "hebrew": return "HB";
                    case "hindi": return "HN";
                    case "hungarian": return "HU";
                    case "icelandic": return "IS";
                    case "indian": return "ID";
                    case "iranian": return "FA";
                    case "italian": return "IT";
                    case "japanese": return "JA";
                    case "kannada": return "KN";
                    case "kashmiri": return "KS";
                    case "korean": return "KO";
                    case "latin": return "LN";
                    case "lithuanian": return "LT";
                    case "malayalam": return "ML";
                    case "mandarin": return "MD";
                    case "niger-congo": return "BN";
                    case "persian": return "PE";
                    case "polish": return "PO";
                    case "portuguese": return "PG";
                    case "punjabi": return "PJ";
                    case "romanian": return "RO";
                    case "russian": return "RU";
                    case "serbian": return "SR";
                    case "sign language": return "AS";
                    case "spanish": return "SP";
                    case "tagalog": return "TG";
                    case "tamil": return "TA";
                    case "telugu": return "TE";
                    case "tok pisin": return "TP";
                    case "turkish": return "TK";
                    case "ukrainian": return "UK";
                    case "urdu": return "UR";
                    case "vietnamese": return "VT";
                    case "yiddish": return "YD";
                    case "yoruba": return "YO";
                    case "zhuang": return "ZA";
                    case "zulu": return "ZU";
                }
            }

            return "";
        }

    }
}
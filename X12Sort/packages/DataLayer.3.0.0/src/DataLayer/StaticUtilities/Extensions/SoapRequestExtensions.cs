using NLog;
using System;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Xml.XPath;

namespace DataLayer.StaticUtilities.Extensions
{
    /// <summary>
    /// Extension functions that provide SOAP XML conversions functions
    /// </summary>
    public static class SoapRequestExtensions
    {
        /// <summary>
        /// The constant SOAP envelope 
        /// </summary>
        public const string SoapEnvelope = @"<?xml version='1.0' encoding='UTF-8'?><S:Envelope xmlns:S=""http://schemas.xmlsoap.org/soap/envelope/""><S:Body></S:Body></S:Envelope>";

        private static readonly ILogger Logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Created a XML SOAP envelope from a `ISoapRequest` that can be POSTed to a HTTP endpoint
        /// </summary>
        /// <typeparam name="TRequest">The soap request body without SOAP envelope</typeparam>
        /// <typeparam name="TResponse">The soap response body without SOAP envelope</typeparam>
        /// <param name="request">The request object </param>
        /// <seealso cref="SoapRequest{TRequest, TResponse}"/>
        /// <returns></returns>
        public static XmlDocument MakeEnvelope<TRequest, TResponse>(this ISoapRequest<TRequest, TResponse> request) where TRequest : class where TResponse : class
        {
            // create the envelope XML doc
            XmlDocument envelope = new XmlDocument();
            envelope.LoadXml(SoapEnvelope);

            // Convert the request body into an XML document
            XmlDocument bodyDoc = new XmlDocument();
            if (request != null)
                using (XmlWriter writer = bodyDoc.CreateNavigator().AppendChild())
                {
                    new XmlSerializer(typeof(TRequest)).Serialize(writer, request.Body);
                }

            // append the XML body into the envelope
            if (request != null)
            {
                XPathNavigator bodyNode = envelope.CreateNavigator().SelectSingleNode("/S:Envelope/S:Body", request.NamespaceManager);
                XmlNodeReader reader = new XmlNodeReader(bodyDoc);

                bodyNode.AppendChild(reader);

                reader.Dispose();
            }

            Logger.Debug($"Serialized SOAP body: {Environment.NewLine}{envelope.OuterXml}");
            return envelope;
        }

        /// <summary>
        /// Converts XML SOAP envelope to response object
        /// </summary>
        /// <typeparam name="TRequest">The soap request body without SOAP envelope</typeparam>
        /// <typeparam name="TResponse">The soap response body without SOAP envelope</typeparam>
        /// <param name="request">The request object</param>
        /// <param name="response">The XDocument of the HTTP body</param>
        /// <seealso cref="SoapRequest{TRequest, TResponse}"/>
        /// <seealso cref="Flurl.Http.Xml.HttpResponseMessageExtensions.ReceiveXDocument(System.Threading.Tasks.Task{System.Net.Http.HttpResponseMessage})"/>
        /// <returns></returns>
        public static TResponse ReadEnvelope<TRequest, TResponse>(this ISoapRequest<TRequest, TResponse> request, XDocument response) where TRequest : class where TResponse : class
        {
            if (request == null)
                return null;

            var bodyNode = response.XPathSelectElement("/S:Envelope/S:Body/*", request.NamespaceManager);
            Logger.Debug($"Reading SOAP body: {Environment.NewLine}{bodyNode.ToString()}");
            XmlSerializer serializer = new XmlSerializer(typeof(TResponse));
            return (TResponse)serializer.Deserialize(bodyNode.CreateReader());
        }
    }
}

using DataLayer.Base.Models;

namespace DataLayer.StaticUtilities.Extensions
{
    public static class IAddressExtensions
    {
        /// <summary>
        /// Formats the address as a string
        /// </summary>
        /// <param name="addr">Address to format</param>
        /// <param name="includeName">Whether name field from address should be included or not</param>
        /// <returns>Formatted address string</returns>
        public static string Format(this IAddress addr, bool includeName = false)
        {
            string name = (addr != null && includeName) ? addr.Name + ", " : "";
            string address1 = (addr == null || string.IsNullOrEmpty(addr.Address1)) ? "" : $" {addr.Address1}";
            string address2 = (addr == null || string.IsNullOrEmpty(addr.Address2)) ? "" : $" {addr.Address2}";
            string city = (addr == null || string.IsNullOrEmpty(addr.City)) ? "" : $" {addr.City}";
            string countryStr = (addr == null || addr.Country == Country.UnitedStates) ? "" : $", {addr.Country.Code3}";
            string stateStr = (addr == null || addr.State == USState.Unknown) ? "" : $" {addr.State.Code}";
            string zip = (addr == null || string.IsNullOrEmpty(addr.ZipCode)) ? "" : $"{addr.ZipCode}";

            return $"{name}{address1}{address2}, {city}{stateStr} {zip}{countryStr}";
        }

        /// <summary>
        /// Computes if addresses are similar using levenshtein distance. Only compares address info, not meta info
        /// </summary>
        /// <param name="addr">this address object</param>
        /// <param name="other">another address object</param>
        /// <param name="percent">Percent difference allowable</param>
        /// <returns></returns>
        public static bool IsSimilar(this IAddress addr, IAddress other, double percent = 0.9)
        {
            if (addr == null  || other == null)
            {
                return false;
            }

            if (addr.Equals(other))
            {
                return true;
            }

            string myFull = addr.Format(includeName: false).ToUpper();
            string otherFull = other.Format(includeName: false).ToUpper();

            double numChars = myFull.Length;
            double distance = myFull.LevenshteinDistance(otherFull);

            return (distance / numChars) < (1 - percent);
        }

        /// <summary>
        /// Determines if an address object has any address information
        /// </summary>
        public static bool IsEmpty(this IAddress addr)
        {
            if (addr == null)
                return true;

            return string.IsNullOrEmpty(addr.Address1)
                && string.IsNullOrEmpty(addr.City)
                && addr.State == USState.Unknown
                && addr.ZipCode == ZipCode.Unknown;
        }
    }
}

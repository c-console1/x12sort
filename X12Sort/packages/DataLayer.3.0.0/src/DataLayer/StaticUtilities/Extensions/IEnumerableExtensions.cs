using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace DataLayer.StaticUtilities.Extensions
{
    public static class IEnumerableExtensions
    {
        private const int DEFAULT_BATCH_SIZE = 1000;

        /// <summary>
        /// Converts a tree of items into a flat of all nodes
        /// </summary>
        /// <param name="root">The starting object to flatten</param>
        /// <remarks>Flatten objects starting with the given object and then the children, operating recursively </remarks>
        /// <returns></returns>
        public static IEnumerable<T> Flatten<T>(this T root, Func<T, IEnumerable<T>> childrenSelector) where T : class
        {
            root.ThrowIfNull(nameof(root));
            childrenSelector.ThrowIfNull(nameof(childrenSelector));

            yield return root;
            foreach (T flatItem in childrenSelector(root).Flatten(childrenSelector))
            {
                yield return flatItem;

            }
        }

        /// <summary>
        /// Converts a enumeration of trees into a flat list of all nodes
        /// </summary>
        /// <param name="root">The starting object to flatten</param>
        /// <remarks>Flatten objects starting with the given object and then the children, operating recursively </remarks>
        /// <returns></returns>
        public static IEnumerable<T> Flatten<T>(this IEnumerable<T> root, Func<T, IEnumerable<T>> childrenSelector) where T : class
        {
            root.ThrowIfNull(nameof(root));
            childrenSelector.ThrowIfNull(nameof(childrenSelector));

            foreach (T item in root)
            {
                foreach (T flatItem in item.Flatten(childrenSelector))
                {
                    yield return flatItem;
                }
            }
        }

        /// <summary>
        /// Gets a batch from an IEnumerable
        /// </summary>
        /// <remarks>https://stackoverflow.com/a/15414468</remarks>
        /// <param name="root">Original IEnumerable</param>
        /// <param name="batchNumber">Batch number to get (0-based)</param>
        /// <param name="batchSize">Batch size</param>
        /// <returns>Single batch of `batchSize` from `root`</returns>
        public static IEnumerable<T> GetBatch<T>(this IEnumerable<T> root, int batchNumber, int batchSize = DEFAULT_BATCH_SIZE)
        {
            return root.Skip(batchNumber * batchSize).Take(batchSize);
        }

        /// <summary>
        /// Gets the total number of batches based on the batch size
        /// </summary>
        /// <param name="root">Original IEnumerable</param>
        /// <param name="batchSize">Batch size</param>
        /// <returns>Total batch count</returns>
        public static int GetBatchCount<T>(this IEnumerable<T> root, int batchSize = DEFAULT_BATCH_SIZE)
        {
            return (int)Math.Ceiling(root.Count() / (double)batchSize);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <remarks>https://blog.differentpla.net/blog/2011/01/30/converting-ienumerablelesstgreater-to-idatareader/</remarks>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="fieldCount"></param>
        /// <param name="getValue"></param>
        /// <returns></returns>
        public static IDataReader AsDataReader<T>(this IEnumerable<T> source, int fieldCount, Func<T, int, object> getValue)
        {
            if (source != null)
                return EnumerableDataReader.Create(source, fieldCount, getValue);
            else
                return null;
        }
    }
}

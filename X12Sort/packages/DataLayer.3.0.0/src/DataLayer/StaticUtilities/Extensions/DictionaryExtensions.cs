using System.Collections.Generic;
using System.Dynamic;

namespace DataLayer.StaticUtilities.Extensions
{
    public static class DictionaryExtensions
    {
        /// <summary>
        /// Format a dictionary into a Dapper parameter object
        /// </summary>
        /// <remarks>https://stackoverflow.com/a/7596697</remarks>
        /// <param name="dict">Dictionary being extended</param>
        /// <returns>Object formatted as a Dapper paramter object</returns>
        public static object ToQueryParamObject(this Dictionary<string, object> dict)
        {
            var eo = new ExpandoObject();
            var eoColl = (ICollection<KeyValuePair<string, object>>)eo;

            if (dict != null)
                foreach (var kvp in dict)
                {
                    eoColl.Add(kvp);
                }

            dynamic eoDynamic = eo;

            return eoDynamic;
        }
    }
}

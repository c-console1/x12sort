using Newtonsoft.Json.Converters;
using System;
using System.Globalization;
using System.Linq;

namespace DataLayer.StaticUtilities.Extensions
{
    /// <summary>
    /// Collection of Static Functions for DateTime Operations
    /// </summary>
    public static class DateTimeExtensions
    {
        /// <summary>
        /// Adds week days to a date
        /// </summary>
        /// <param name="date">Date to modify</param>
        /// <param name="days">Number of days to add</param>
        /// <returns>Modified date with days added</returns>
        public static DateTime AddWeekDays(this DateTime date, int days)
        {
            int inc = days > 0 ? 1 : -1;
            int daysAbs = Math.Abs(days);

            while (daysAbs > 0)
            {
                date = date.AddDays(inc);
                if (date.IsWeekday()) daysAbs -= 1;
            }
            return date;
        }

        /// <summary>
        /// Determine if <paramref name="self"/> is less than (-1), greater than (1), or equal (0) to <paramref name="date"/>.
        /// </summary>
        /// <param name="self">First date to compare</param>
        /// <param name="date">Second date to compare</param>
        /// <returns>0 = dates are equal; -1 = date1 is less than date2; 1 = date1 is greater than date2</returns>
        public static int Compare(this DateTime? self, DateTime? date)
        {
            if (self == date) return 0;
            if (self == null) return -1;
            if (date == null) return 1;
            return self < date ? -1 : self > date ? 1 : 0;
        }
        /// <summary>
        /// Determine if <paramref name="self"/> is less than (-1), greater than (1), or equal (0) to <paramref name="date"/>.
        /// </summary>
        /// <param name="self">First date to compare</param>
        /// <param name="date">Second date to compare</param>
        /// <returns>0 = dates are equal; -1 = date1 is less than date2; 1 = date1 is greater than date2</returns>
        public static int Compare(this DateTime? self, DateTime date)
        {
            if (self == date) return 0;
            if (self == null) return -1;
            return self < date ? -1 : self > date ? 1 : 0;
        }

        /// <summary>
        /// Determine if <paramref name="self"/> is less than (-1), greater than (1), or equal (0) to <paramref name="date"/>.
        /// </summary>
        /// <param name="self">First date to compare</param>
        /// <param name="date">Second date to compare</param>
        /// <returns>0 = dates are equal; -1 = date1 is less than date2; 1 = date1 is greater than date2</returns>
        public static int Compare(this DateTime self, DateTime date)
        {
            if (self == date) return 0;
            return self < date ? -1 : self > date ? 1 : 0;
        }
        /// <summary>
        /// Determine if <paramref name="self"/> is less than (-1), greater than (1), or equal (0) to <paramref name="date"/>.
        /// </summary>
        /// <param name="self">First date to compare</param>
        /// <param name="date">Second date to compare</param>
        /// <returns>0 = dates are equal; -1 = date1 is less than date2; 1 = date1 is greater than date2</returns>
        public static int Compare(this DateTime self, DateTime? date)
        {
            if (self == date) return 0;
            if (date == null) return 1;
            return self < date ? -1 : self > date ? 1 : 0;
        }


        /// <summary>
        /// Determines whether a date is a weekday or not
        /// </summary>
        /// <param name="date">Date to analyze</param>
        /// <returns>True: date is a weekday; False: date is NOT a weekday</returns>
        public static bool IsWeekday(this DateTime date)
        {
            return !date.IsWeekend();
        }

        /// <summary>
        /// Determines whether a date is a weekend or not
        /// </summary>
        /// <param name="date">Date to analyze</param>
        /// <returns>True: date is a weekend; False: date is NOT a weekend</returns>
        public static bool IsWeekend(this DateTime date)
        {
            return date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday;
        }

        /// <summary>
        /// Removes all hours, mins, and sec, return object with just the date
        /// </summary>
        /// <param name="date">Date to process</param>
        /// <returns>new DateTime object with just the date and not time elements</returns>
        public static DateTime TrimTime(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day);
        }

        /// <summary>
        /// Removes all hours, mins, and sec, return object with just the date
        /// </summary>
        /// <param name="date">Date to process</param>
        /// <returns>new DateTime object with just the date and not time elements</returns>
        public static DateTime? TrimTime(this DateTime? date)
        {
            return date.HasValue ? (DateTime?)new DateTime(date.Value.Year, date.Value.Month, date.Value.Day) : null;
        }
    }

    /// <summary>
    /// Date/time converter to use during JSON conversion
    /// </summary>
    public class DateFormatConverter : IsoDateTimeConverter
    {
        /// <summary>
        /// Date/time converter method to use during JSON conversion
        /// </summary>
        /// <param name="format">Format of date/time object</param>
        public DateFormatConverter(string format)
        {
            DateTimeFormat = format;
        }
    }
}

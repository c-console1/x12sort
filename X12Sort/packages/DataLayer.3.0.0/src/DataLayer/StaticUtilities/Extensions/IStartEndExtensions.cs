using DataLayer.Base.Models;
using System;

namespace DataLayer.StaticUtilities.Extensions
{
    /// <summary>
    /// Collection of Static Functions for IStartEnd Operations
    /// </summary>
    public static class IStartEndExtensions
    {
        /// <summary>
        /// Determine whether passed <paramref name="range"/> is completely within the current range.
        /// </summary>
        /// <param name="self">Current date range</param>
        /// <param name="range">Passed date range</param>
        /// <param name="exculsive">If true, the <paramref name="range"/> must be between but not equal to start or end dates.</param>
        /// <returns>Whether passed date range is completely within the current date range</returns>
        public static bool Contains(this IStartEnd self, IStartEnd range, bool exculsive = false)
        {
            int startCompareLimit = 0;
            int endCompareLimit = 0;
            if (exculsive)
            {
                startCompareLimit = -1;
                endCompareLimit = 1;
            }

            if (range.IsValid() && self.IsValid())
            {
                return self.StartDate.Compare(range.StartDate) <= startCompareLimit && self.EndDate.Compare(range.EndDate) >= endCompareLimit;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Determine whether passed <paramref name="date"/> is completely within the current range.
        /// </summary>
        /// <param name="self">Current date range</param>
        /// <param name="date">Passed date range</param>
        /// <param name="exculsive">If true, the <paramref name="date"/> must be between but not equal to start or end dates.</param>
        /// <returns>Whether passed date range is completely within the current date range</returns>
        public static bool Contains(this IStartEnd self, DateTime date, bool exculsive = false)
        {
            int startCompareLimit = 0;
            int endCompareLimit = 0;
            if (exculsive)
            {
                startCompareLimit = -1;
                endCompareLimit = 1;
            }
            if (self.IsValid())
            {
                return self.StartDate.Compare(date) <= startCompareLimit && self.EndDate.Compare(date) >= endCompareLimit;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Determine whether date ranges are equal
        /// </summary>
        /// <param name="self">Current date range</param>
        /// <param name="range">Date range passed for comparison</param>
        /// <returns>Whether date range is equal to other date range passed</returns>
        public static bool EqualTo(this IStartEnd self, IStartEnd range)
        {
            if (self.IsValid() && range.IsValid())
            {
                return (self.StartDate == range.StartDate) && (self.EndDate == range.EndDate);
            }
            else
            {
                return false;
            }
        }
         
        /// <summary>
        /// Determine whether passed date range intersects current date range at all.
        /// </summary>
        /// <param name="self">Current date range</param>
        /// <param name="range">Passed date range</param>
        /// <returns>Whether passed date range intersects current date range at all</returns>
        public static bool Intersects(this IStartEnd self, IStartEnd range)
        {
            if (range.IsValid() && self.IsValid())
            {
                return range.EndDate.Compare(self.StartDate) >= 0 && range.StartDate.Compare(self.EndDate) <= 0;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Determines whether passed <paramref name="range"/> is contiguous (next to) with current date range. Comparison based on date only, time is truncated
        /// </summary>
        /// <param name="self">Current date range</param>
        /// <param name="range">Passed date range</param>
        /// <returns>Whether passed date range is contiguous with current date range</returns>
        public static bool IsContiguous(this IStartEnd self, IStartEnd range)
        {
            if (self.IsValid() && range.IsValid())
            {
                // flip the start and end dates since we are comparing borders
                DateTime startBorder = self.EndDate.TrimTime().Value.AddDays(1);
                DateTime endBorder = self.StartDate.TrimTime().Value.AddDays(-1);
                return (startBorder == range.StartDate.TrimTime()) || (endBorder == range.EndDate.TrimTime());
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Determines whether date range is valid (i.e. start date should be less than or equal to end date)
        /// </summary>
        /// <param name="self">Current date range</param>
        /// <param name="allowNull">If false, Start and End must not be null. If true, allows null values</param>
        /// <returns>Whether date range is valid</returns>
        public static bool IsValid([ValidatedNotNull] this IStartEnd self, bool allowNull = false)
        {
            if (self == null)
            {
                return false;
            }
            if (self.StartDate == null || self.EndDate == null)
            {
                return allowNull;
            }
            return self.StartDate < self.EndDate;
        }

        /// <summary>
        /// Gets a simple start/end range from any object implementing `IStartEnd`
        /// </summary>
        /// <param name="self">Current date range</param>
        /// <returns>Simple start/end range</returns>
        public static StartEndRange GetRange(this IStartEnd self)
        {
            return (StartEndRange)self;
        }
    }
}

using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;

namespace DataLayer.StaticUtilities
{
    /// <summary>
    /// A collection of static helper with Oracle database operations
    /// </summary>
    public static class TypeConverter
    {
        public static Dictionary<Type, DbType> DbTypeMap { get; } = new Dictionary<Type, DbType>()
        {
            [typeof(byte)] = DbType.Byte,
            [typeof(sbyte)] = DbType.SByte,
            [typeof(short)] = DbType.Int16,
            [typeof(ushort)] = DbType.UInt16,
            [typeof(int)] = DbType.Int32,
            [typeof(uint)] = DbType.UInt32,
            [typeof(long)] = DbType.Int64,
            [typeof(ulong)] = DbType.UInt64,
            [typeof(float)] = DbType.Single,
            [typeof(double)] = DbType.Double,
            [typeof(decimal)] = DbType.Decimal,
            [typeof(bool)] = DbType.Boolean,
            [typeof(string)] = DbType.String,
            [typeof(char)] = DbType.StringFixedLength,
            [typeof(Guid)] = DbType.Guid,
            [typeof(DateTime)] = DbType.DateTime,
            [typeof(DateTimeOffset)] = DbType.DateTimeOffset,
            [typeof(TimeSpan)] = DbType.Time,
            [typeof(byte[])] = DbType.Binary,
            [typeof(byte?)] = DbType.Byte,
            [typeof(sbyte?)] = DbType.SByte,
            [typeof(short?)] = DbType.Int16,
            [typeof(ushort?)] = DbType.UInt16,
            [typeof(int?)] = DbType.Int32,
            [typeof(uint?)] = DbType.UInt32,
            [typeof(long?)] = DbType.Int64,
            [typeof(ulong?)] = DbType.UInt64,
            [typeof(float?)] = DbType.Single,
            [typeof(double?)] = DbType.Double,
            [typeof(decimal?)] = DbType.Decimal,
            [typeof(bool?)] = DbType.Boolean,
            [typeof(char?)] = DbType.StringFixedLength,
            [typeof(Guid?)] = DbType.Guid,
            [typeof(DateTime?)] = DbType.DateTime,
            [typeof(DateTimeOffset?)] = DbType.DateTimeOffset,
            [typeof(TimeSpan?)] = DbType.Time,
            [typeof(object)] = DbType.Object
        };

        public static Dictionary<Type, SqlDbType> SqlTypeMap { get; } = new Dictionary<Type, SqlDbType>()
        {
            [typeof(byte)] = SqlDbType.Binary,
            [typeof(sbyte)] = SqlDbType.Binary,
            [typeof(short)] = SqlDbType.SmallInt,
            [typeof(ushort)] = SqlDbType.SmallInt,
            [typeof(int)] = SqlDbType.Int,
            [typeof(uint)] = SqlDbType.Int,
            [typeof(long)] = SqlDbType.BigInt,
            [typeof(ulong)] = SqlDbType.BigInt,
            [typeof(float)] = SqlDbType.Float,
            [typeof(double)] = SqlDbType.Decimal,
            [typeof(decimal)] = SqlDbType.Decimal,
            [typeof(bool)] = SqlDbType.Bit,
            [typeof(string)] = SqlDbType.VarChar,
            [typeof(char)] = SqlDbType.Char,
            [typeof(Guid)] = SqlDbType.UniqueIdentifier,
            [typeof(DateTime)] = SqlDbType.DateTime,
            [typeof(DateTimeOffset)] = SqlDbType.DateTimeOffset,
            [typeof(TimeSpan)] = SqlDbType.Timestamp,
            [typeof(byte[])] = SqlDbType.VarBinary,
            [typeof(byte?)] = SqlDbType.Binary,
            [typeof(sbyte?)] = SqlDbType.Binary,
            [typeof(short?)] = SqlDbType.SmallInt,
            [typeof(ushort?)] = SqlDbType.SmallInt,
            [typeof(int?)] = SqlDbType.Int,
            [typeof(uint?)] = SqlDbType.Int,
            [typeof(long?)] = SqlDbType.BigInt,
            [typeof(ulong?)] = SqlDbType.BigInt,
            [typeof(float?)] = SqlDbType.Float,
            [typeof(double?)] = SqlDbType.Decimal,
            [typeof(decimal?)] = SqlDbType.Decimal,
            [typeof(bool?)] = SqlDbType.Bit,
            [typeof(char?)] = SqlDbType.Char,
            [typeof(Guid?)] = SqlDbType.UniqueIdentifier,
            [typeof(DateTime?)] = SqlDbType.DateTime,
            [typeof(DateTimeOffset?)] = SqlDbType.DateTimeOffset,
            [typeof(TimeSpan?)] = SqlDbType.Timestamp,
            [typeof(object)] = SqlDbType.VarBinary
        };

        public static Dictionary<Type, OracleDbType> OracleTypeMap { get; } = new Dictionary<Type, OracleDbType>()
        {
            [typeof(byte)] = OracleDbType.Byte,
            [typeof(sbyte)] = OracleDbType.Byte,
            [typeof(short)] = OracleDbType.Int16,
            [typeof(ushort)] = OracleDbType.Int16,
            [typeof(int)] = OracleDbType.Int32,
            [typeof(uint)] = OracleDbType.Int32,
            [typeof(long)] = OracleDbType.Int64,
            [typeof(ulong)] = OracleDbType.Int64,
            [typeof(float)] = OracleDbType.Single,
            [typeof(double)] = OracleDbType.Double,
            [typeof(decimal)] = OracleDbType.Decimal,
            [typeof(bool)] = OracleDbType.Boolean,
            [typeof(string)] = OracleDbType.Varchar2,
            [typeof(char)] = OracleDbType.Char,
            [typeof(Guid)] = OracleDbType.Varchar2,
            [typeof(DateTime)] = OracleDbType.Date,
            [typeof(DateTimeOffset)] = OracleDbType.Date,
            [typeof(TimeSpan)] = OracleDbType.TimeStamp,
            [typeof(byte[])] = OracleDbType.Blob,
            [typeof(byte?)] = OracleDbType.Byte,
            [typeof(sbyte?)] = OracleDbType.Byte,
            [typeof(short?)] = OracleDbType.Int16,
            [typeof(ushort?)] = OracleDbType.Int16,
            [typeof(int?)] = OracleDbType.Int32,
            [typeof(uint?)] = OracleDbType.Int32,
            [typeof(long?)] = OracleDbType.Int64,
            [typeof(ulong?)] = OracleDbType.Int64,
            [typeof(float?)] = OracleDbType.Single,
            [typeof(double?)] = OracleDbType.Double,
            [typeof(decimal?)] = OracleDbType.Decimal,
            [typeof(bool?)] = OracleDbType.Boolean,
            [typeof(char?)] = OracleDbType.Varchar2,
            [typeof(Guid?)] = OracleDbType.Varchar2,
            [typeof(DateTime?)] = OracleDbType.Date,
            [typeof(DateTimeOffset?)] = OracleDbType.Date,
            [typeof(TimeSpan?)] = OracleDbType.TimeStamp,
            [typeof(object)] = OracleDbType.Blob
        };

        /// <summary>
        /// Translates a .NET type to an DbType
        /// </summary>
        /// <param name="type">.NET type to translate</param>
        /// <remarks>
        /// This list of conversion is from the dapper library
        /// </remarks>
        public static DbType ToDbType(Type type)
        {
            return DbTypeMap[type];
        }

        /// <summary>
        /// Translates a .NET type to a SqlDbType
        /// </summary>
        /// <param name="type">.NET type to translate</param>
        public static SqlDbType ToSqlDbType(Type type)
        {
            return SqlTypeMap[type];
        }

        /// <summary>
        /// Translates a .NET type to an OracleDbType
        /// </summary>
        /// <param name="type">.NET type to translate</param>
        /// <returns></returns>
        public static OracleDbType ToOracleDbType(Type type)
        {
            return OracleTypeMap[type];
        }
    }
}

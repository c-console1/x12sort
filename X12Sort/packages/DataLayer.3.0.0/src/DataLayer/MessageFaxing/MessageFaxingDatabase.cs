using DataLayer.Base;
using System;

namespace DataLayer.MessageFaxing
{
    /// <summary>
    /// Logical Database for the Messaging/Faxing System
    /// </summary>
    public class MessageFaxingDatabase : BaseDatabase
    {
        public MessagingRepository Messaging { get; private set; }

        internal MessageFaxingDatabase(IDbExec dbExec) : base(dbExec)
        {
            Messaging = new MessagingRepository(this);
        }

        internal MessageFaxingDatabase(params IExecutor[] executors) : base(executors)
        {
            Messaging = new MessagingRepository(this);
        }
    }

}

using DataLayer.Base;

namespace DataLayer.MessageFaxing
{
	public class MessagingRepository : BaseRepository<MessageFaxingDatabase>
	{
		internal MessagingRepository(MessageFaxingDatabase database) : base(database)
		{

		}
	}
}

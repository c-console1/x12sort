using DataLayer.Base;
using System;

namespace DataLayer.ProviderHub
{
    public class ProviderHubDatabase : BaseDatabase
    {
        public HubRepository Hub { get; private set; }

        public ProviderHubDatabase(IDbExec dbExec) : base(dbExec)
        {
            Hub = new HubRepository(this);
        }

        internal ProviderHubDatabase(params IExecutor[] executors) : base(executors)
        {
            Hub = new HubRepository(this);
        }


    }
}

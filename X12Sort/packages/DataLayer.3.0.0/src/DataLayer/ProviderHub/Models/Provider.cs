﻿using System.Collections.Generic;

namespace DataLayer.ProviderHub.Models
{
    public class Provider : BaseHubObject
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Degree { get; set; }
        public string Sex { get; set; }
        public string InstitutionName { get; set; }
        public string NPI { get; set; }
        public string TaxID { get; set; }
        public string MedicareNumber { get; set; }
        public string MedicaidNumber { get; set; }
        public IEnumerable<ProviderBoard> Boards { get; set; }
        public IEnumerable<ProviderLanguage> Languages { get; set; }
    }
}

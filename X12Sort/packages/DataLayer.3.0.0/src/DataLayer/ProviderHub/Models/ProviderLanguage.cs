﻿using System.Collections.Generic;

namespace DataLayer.ProviderHub.Models
{
    public class ProviderLanguage : BaseHubObject
    {
        public string Description { get; set; }
    }
}

using DataLayer.Base.Enumerations;
using DataLayer.Base.Models;

namespace DataLayer.ProviderHub.Models
{
    public abstract class BaseHubObject : BaseDataObject
    {

        public override DataSource Source { get => DataSource.Other; }

        protected BaseHubObject() : base()
        {
        }
    }
}

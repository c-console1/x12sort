﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using DataLayer.StaticUtilities;
using DataLayer.StaticUtilities.Extensions;

namespace PHCMessaging
{
    public static class Email
    {
        private const char EMAIL_DELIMITER = ';';
        private const string DEFAULT_FROM = "PHCITNotifications@Promedica.org";
        private const string DEFAULT_SUBJECT = "Paramount IT Notification";
        private const string DEFAULT_HOST = "mail.phsi.promedica.org";
        private const int DEFAULT_PORT = 25;
        private const bool DEFAULT_HTML = false;
        private const MailPriority DEFAULT_PRIORITY = MailPriority.Normal;

        /// <summary>
        /// Send an email using SMTP
        /// </summary>
        /// <param name="body">Text of the email body of SMTP email; if it is HTML, set bodyIsHTML parameter to true</param>
        /// <param name="emailTo">Delimited list of email addresses to send to</param>
        /// <param name="emailFrom">Address to send STMP email from</param>
        /// <param name="emailCC">Delimited list of email addresses to cc</param>
        /// <param name="emailBCC">Delimited list of email addresses to bcc</param>
        /// <param name="subject">Subject line</param>
        /// <param name="emailHost">SMTP email server host address</param>
        /// <param name="emailPort">SMTP email server port</param>
        /// <param name="attachments">List of attachments; note that if the attachments are too large, the email delivery might fail</param>
        /// <param name="bodyIsHTML">Determines whether the body should be interpreted as HTML</param>
        /// <param name="priority">Email priority (high, normal, or low)</param>
        /// <returns>True = email was successfully sent; False = email was not sent and the error was printed to the console</returns>
        public static bool Send(
            string body,
            string emailTo,
            string emailFrom = DEFAULT_FROM,
            string emailCC = null,
            string emailBCC = null,
            string subject = DEFAULT_SUBJECT,
            string emailHost = DEFAULT_HOST,
            int emailPort = DEFAULT_PORT,
            IEnumerable<Attachment> attachments = null,
            bool bodyIsHTML = DEFAULT_HTML,
            MailPriority priority = DEFAULT_PRIORITY)
        {
            try
            {
                // Validate parameters
                if (emailTo == null) throw new ArgumentNullException("emailTo");
                if (emailFrom == null) throw new ArgumentNullException("emailFrom");
                if (!emailFrom.IsValidEmailAddress()) throw new ArgumentException($"Invalid from email address: '{emailFrom}'");
                if (subject == null) throw new ArgumentNullException("subject");
                if (emailHost == null) throw new ArgumentNullException("emailHost");
                if (emailPort < 1 || emailPort > 65535) throw new ArgumentException("Email port number must be between 1 and 65535");

                // Build message
                SmtpClient client = new SmtpClient(emailHost, emailPort);
                MailMessage message = new MailMessage();
                message.From = new MailAddress(emailFrom);
                message.Subject = subject;
                message.Body = body;
                message.IsBodyHtml = bodyIsHTML;
                message.Priority = priority;
                
                // Add to email addresses
                IEnumerable<string> toList = emailTo.Split(new char[] { EMAIL_DELIMITER }, StringSplitOptions.RemoveEmptyEntries).ToList();
                if (toList.Count() == 0) throw new ArgumentException("Must define at least 1 to email address");
                foreach (var toAddress in toList)
                {
                    if (!toAddress.IsValidEmailAddress()) throw new ArgumentException($"Invalid to email address: '{toAddress}'");
                    message.To.Add(new MailAddress(toAddress));
                }

                // Add CC email addresses
                IEnumerable<string> ccList = (string.IsNullOrWhiteSpace(emailCC) ? new string[] { } : emailCC.Split(new char[] { EMAIL_DELIMITER }, StringSplitOptions.RemoveEmptyEntries)).ToList();
                foreach (var ccAddress in ccList)
                {
                    if (!ccAddress.IsValidEmailAddress()) throw new ArgumentException($"Invalid cc email address: '{ccAddress}'");
                    message.CC.Add(new MailAddress(ccAddress));
                }

                // Add BCC email addresses
                IEnumerable<string> bccList = (string.IsNullOrWhiteSpace(emailBCC) ? new string[] { } : emailBCC.Split(new char[] { EMAIL_DELIMITER }, StringSplitOptions.RemoveEmptyEntries)).ToList();
                foreach (var bccAddress in bccList)
                {
                    if (!bccAddress.IsValidEmailAddress()) throw new ArgumentException($"Invalid bcc email address: '{bccAddress}'");
                    message.Bcc.Add(new MailAddress(bccAddress));
                }

                // Add attachments
                if (attachments != null)
                    foreach (var attachment in attachments)
                        message.Attachments.Add(attachment);

                // Send email
                client.Send(message);
                return true;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
                Console.Error.WriteLine(ex.StackTrace);
                return false;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using DataLayer;
using DataLayer.MessageFaxing;

namespace PHCMessaging
{
    public static class EmailQueue
    {
        private const char EMAIL_DELIMITER = ';';
        private const string DEFAULT_FROM = "PHCITNotifications@Promedica.org";
        private const string DEFAULT_SUBJECT = "Paramount IT Notification";
        private const string DEFAULT_BODY_TYPE = "html";
        private const string DEFAULT_SEND_TYPE = "fax";
        private const string DEFAULT_TAG = "NOT_SET";
        private const string DEFAULT_PROFILE_KEY = "";

        private static DataAccess _data;

        private static DataAccess Data
        {
            get
            {
                if (_data == null)
                {
                    _data = new DataAccess(DbExec.Create());
                    _data.LoadApplication("EXTRACT", "StdExtract", false);
                }
                return _data;
            }
        }

        /// <summary>
        /// Add an email to the MessageFaxing queue
        /// </summary>
        /// <param name="body">Text of the email body; by default, it is interpreted as an HTML email</param>
        /// <param name="emailTo">Delimited list of email addresses to send to</param>
        /// <param name="emailFrom">Address to send Exchange email from</param>
        /// <param name="subject">Subject line</param>
        /// <param name="attachmentPaths">List of paths to attachments; the paths must be accessible to this method</param>
        /// <param name="bodyType">Determines how the body should be interpreted (default is HTML)</param>
        /// <param name="sendType">???</param>
        /// <param name="tag">???</param>
        /// <param name="profileKey">???</param>
        public static void Add(
            string body,
            string emailTo,
            string emailFrom = DEFAULT_FROM,
            string subject = DEFAULT_SUBJECT,
            IEnumerable<string> attachmentPaths = null,
            string bodyType = DEFAULT_BODY_TYPE,
            string sendType = DEFAULT_SEND_TYPE,
            string tag = DEFAULT_TAG,
            string profileKey = DEFAULT_PROFILE_KEY)
        {
            Add(
                Data,
                body,
                emailTo,
                emailFrom: emailFrom,
                subject: subject,
                attachmentPaths: attachmentPaths,
                bodyType: bodyType,
                sendType: sendType,
                tag: tag,
                profileKey: profileKey);
        }

        /// <summary>
        /// Add an email to the MessageFaxing queue using a preloaded data access object
        /// </summary>
        /// <param name="dataAccess">Preloaded data access object</param>
        /// <param name="body">Text of the email body; by default, it is interpreted as an HTML email</param>
        /// <param name="emailTo">Delimited list of email addresses to send to</param>
        /// <param name="emailFrom">Address to send Exchange email from</param>
        /// <param name="subject">Subject line</param>
        /// <param name="attachmentPaths">List of paths to attachments; the paths must be accessible to this method</param>
        /// <param name="bodyType">Determines how the body should be interpreted (default is HTML)</param>
        /// <param name="sendType">???</param>
        /// <param name="tag">???</param>
        /// <param name="profileKey">???</param>
        public static void Add(
            DataAccess dataAccess,
            string body,
            string emailTo,
            string emailFrom = DEFAULT_FROM,
            string subject = DEFAULT_SUBJECT,
            IEnumerable<string> attachmentPaths = null,
            string bodyType = DEFAULT_BODY_TYPE,
            string sendType = DEFAULT_SEND_TYPE,
            string tag = DEFAULT_TAG,
            string profileKey = DEFAULT_PROFILE_KEY)
        {
            // TODO: implement the email queue method
            throw new NotImplementedException("This has not been implemented yet.");
        }
    }
}

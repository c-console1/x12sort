﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Threading.Tasks;
using DataLayer;
using System.Data;

namespace X12Sort
{
    public static class Settings
    {
        private static string _solutionName;
        public static string SiteDestRootIn { get; set; }
        public static List<string> CRSites { get; set; }
        public static string TestEnv { get; set; }
        public static DataAccess Mount { get; set; }
        public static string EmailBody { get; set; }
        public static string EmailSubject { get; set; }
        public static string EmailFrom { get; set; }   
        public static string EmailTo { get; set; }
        public static string TypeDestDir { get; set; } 
        public static string TypeSourceDir { get; set; }
        public static Dictionary<string, string> TransactionTypes { get; set; }
        public static string SiteDestRootOut { get; set; }
        public static string SiteSourceDir { get; set; }
        public static Dictionary<string, string> ISAValues { get; set; }
        public static string ArchiveDir { get; set; }
        public static string SolutionName { get { return _solutionName; } }
        public static void InitializeApplication(string[] args)
        {
            _solutionName = Path.GetFileName(Process.GetCurrentProcess().MainModule.FileName).Split('.')[0];
            // create data access mount and load application using solution name
            Mount = new DataAccess(DbExec.Create());
            Mount.LoadApplication(Environment.MachineName, _solutionName, logInstance: true);
            // Create Log folder and file            
            if (GetDamSetting("LogFolder") == null) throw new Exception("'LogFolder' setting must be defined in DAM configuration.");
            string logFolder = GetDamSetting("LogFolder");
            string logFile = $"{_solutionName}_{DateTime.Now.ToString("yyyyMMddHHmmss")}.log";
            Log.Configuration.FilePath = Path.Combine(logFolder, logFile);
            Log.Write($"Log Folder = \"{logFolder}\"");
            Log.Write($"Log File = \"{logFile}\"");            
            if (!Directory.Exists(logFolder))
            {
                Log.Write($"Temp folder does not exist, creating folder...");
                Directory.CreateDirectory(logFolder);
            }
            Log.Write($"Machine name = \"{Mount.Configuration.Current.Machine.MachineName}\"");
            // Log DAM variables
            Log.Write("Display DAM variables...");
            foreach (var variable in Mount.Configuration.Current.Variables)
            {
                Log.Write($"{variable.Name} = \"{variable.Value}\"");
            }
        }
        public static string GetDamSetting(string key) 
        { 
            return Mount.Configuration.Current.Variables.First(q => q.Name == key).Value;
        }
    }
}

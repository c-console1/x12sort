﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataLayer;
using System.Data;
using System.IO;
using System.Diagnostics;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;

namespace X12Sort
{
    public class SiteSort
    {
        public SiteSort()
        {
            Log.Write("--------------------- BEGIN SITE SORT PROCESS ---------------------"); 
            NonStandardISA("In");
            NonStandardISA("Out");
            StandardISA("In");
            StandardISA("Out");
            Log.Write("--------------------- END SITE SORT PROCESS ----------------------");
            Log.CloseLogFile();
        }
        private static void NonStandardISA(string direction)
        {
            string fileGetter = direction + "\\*";
            string SourceDir = Path.Combine(Settings.GetDamSetting("SiteSourceDir"), fileGetter);
            string OutDestDir = Path.Combine(Settings.GetDamSetting("SiteDestRootOut"), direction);
            string InDestDir = Path.Combine(Settings.GetDamSetting("SiteDestRootIn"), direction);
            string ISAValuesRaw = Settings.GetDamSetting("ISAValues");
            string[] ISAArray = ISAValuesRaw.Split(';');
            Dictionary<string, string> ISAValues = new Dictionary<string, string>();
            foreach (string s in ISAArray)
            {
                string[] temparray = s.Split(',');
                ISAValues.Add(temparray[0], temparray[1]);
            }
            string[] inputFiles =
            Directory.GetFiles(Path.GetDirectoryName(SourceDir), Path.GetFileName(SourceDir));
            if (inputFiles.Length > 0)
            {
                Log.Write($"Running Non-Standard ISA site sort logic for direction {direction}");
                if (direction == "In")
                {
                    foreach (string filename in inputFiles)
                    {
                        using (FileStream fs = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite))
                        {
                            using (StreamReader reader = new StreamReader(fs))
                            {
                                string ISA = GetISAData(filename, reader);
                                string ISA06 = ISA.Substring(35, 15);
                                if (ISAValues.TryGetValue(ISA06, out string value))
                                {
                                    Log.Write($"{ISA06} translates to {value}");
                                    string name = Path.GetFileName(filename);
                                    string DestDir = Path.Combine(InDestDir, value);
                                    fs.Dispose();
                                    Log.Write($"{name} moved to <{DestDir}>");
                                    File.Move(filename, Path.Combine(DestDir, name));
                                }
                            }
                        }
                    }
                }
                else
                {
                    foreach (string filename in inputFiles)
                    {
                        using (FileStream fs = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite))
                        {
                            using (StreamReader reader = new StreamReader(fs))
                            {
                                string ISA = GetISAData(filename, reader);
                                string ISA08 = ISA.Substring(54, 15);
                                if (ISAValues.TryGetValue(ISA08, out string value))
                                {
                                    Log.Write($"{ISA08} translates to {value}");
                                    string name = Path.GetFileName(filename);
                                    string DestDir = Path.Combine(OutDestDir, value);
                                    fs.Dispose();
                                    Log.Write($"{name} moved to <{DestDir}>");
                                    File.Move(filename, Path.Combine(DestDir, name));
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                Log.Write("No files, moving on");
            }
        }
        private static void StandardISA(string direction)
        {
            string fileGetter = direction + "\\*";
            string SourceDir = Path.Combine(Settings.GetDamSetting("SiteSourceDir"), fileGetter);
            string OutDestDir = Path.Combine(Settings.GetDamSetting("SiteDestRootOut"), direction);
            string InDestDir = Path.Combine(Settings.GetDamSetting("SiteDestRootIn"), direction);
            string[] inputFiles =
            Directory.GetFiles(Path.GetDirectoryName(SourceDir), Path.GetFileName(SourceDir));
            if (inputFiles.Length > 0)
            {
                Log.Write($"Running Standard ISA site sort logic for direction {direction}");
                if (direction == "In")
                {
                    foreach (string filename in inputFiles)
                    {
                        using (FileStream fs = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite))
                        {
                            using (StreamReader reader = new StreamReader(fs))
                            {
                                string ISA = GetISAData(filename, reader);
                                string ISA06 = ISA.Substring(35, 15);
                                ISA06 = ISA06.Trim();
                                Log.Write($"ISA06 of <{filename}> is <{ISA06}>");
                                string name = Path.GetFileName(filename);
                                string DestDir = Path.Combine(InDestDir, ISA06);
                                fs.Dispose();
                                Log.Write($"{name} moved to <{DestDir}>");
                                File.Move(filename, Path.Combine(DestDir, name));
                            }
                        }
                    }
                }
                else
                {
                    foreach (string filename in inputFiles)
                    {
                        using (FileStream fs = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite))
                        {
                            using (StreamReader reader = new StreamReader(fs))
                            {
                                string ISA = GetISAData(filename, reader);
                                string ISA08 = ISA.Substring(54, 15);
                                ISA08 = ISA08.Trim();
                                Log.Write($"ISA08 of <{filename}> is <{ISA08}>");
                                string name = Path.GetFileName(filename);
                                string DestDir = Path.Combine(OutDestDir, ISA08);
                                fs.Dispose();
                                Log.Write($"{name} moved to <{DestDir}>");
                                File.Move(filename, Path.Combine(DestDir, name));
                            }
                        }
                    }
                }
                Log.Write("Site sort program complete");
            }
        }
        private static void ClearReader(StreamReader reader)
        {
            reader.DiscardBufferedData();
            reader.BaseStream.Seek(0, System.IO.SeekOrigin.Begin);
        }
        static string GetISAData(string file, StreamReader reader)
        {
            ClearReader(reader);
            char[] c = null;
            c = new char[106];
            reader.Read(c, 0, c.Length);
            StringBuilder builder = new StringBuilder();
            foreach (char ch in c)
            {
                builder.Append(ch);
            }
            string ISA = builder.ToString();
            ClearReader(reader);
            return ISA;
        }
    }
}

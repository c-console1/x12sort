# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2020-07-16
### Added 
- C# rewrite of Powershell jobs that sort X12 files
- Two main classes, Type and Sort, called by parameters
- Type sort sorts files that need to be sent to Axiom by transaction type, unzips, ensures proper formatting, etc
- Site sort sorts returned files from Axiom that need to go to trading partners or return to Amisys
